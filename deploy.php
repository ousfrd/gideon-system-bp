<?php
namespace Deployer;

require_once './vendor/autoload.php';
require 'recipe/laravel.php';

use Symfony\Component\Dotenv\Dotenv;


$dotenvPath = __DIR__ . '/.env.deploy';
if (is_file($dotenvPath)) {
	$dotenv = new Dotenv();
	$dotenv->load($dotenvPath);
}

$repo = getenv('DEPLOY_REPO') ?? '';
$host = getenv('DEPLOY_HOST') ?? 'localhost';
$deployUser = getenv('DEPLOY_USER') ?? '';
$deployIdentityFile = getenv('DEPLOY_IDENTITY_FILE') ?? '';
$deployPath = getenv('DEPLOY_PATH') ?? '';

// Project name
set('application', 'Gideon System');

// Project repository
set('repository', $repo);

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);

// Hosts
host($host)
	->user($deployUser)
	->identityFile($deployIdentityFile)
    ->set('deploy_path', $deployPath);

// Tasks
task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

