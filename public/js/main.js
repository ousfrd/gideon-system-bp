function initMenu() {
    $('#menu ul').hide();
    $('#menu ul').children('.current').parent().show();
    //$('#menu ul:first').show();
    
    
    $('#menu li a').click(
      function() {
        var checkElement = $(this).next();
        if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
          checkElement.slideUp('normal');
          return false;
        }
        if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
          // $('#menu ul:visible').slideUp('normal');
          checkElement.slideDown('normal');
          return false;
        }
      }
    );
}
    
function showWaitingLoader(){
  $('#wait-loader').show();
}

$(document).ready(function(){
  /** 
   *  delete report hide columns cookie to prevent too large cookie sent to server 
   *  cause Bad request error. Error message: Your browser sent a request that this server could not understand.
   *  Size of a request header field exceeds server limit.
  */
  (function(){
    if (Cookies) {
      const cookieNames = Object.keys(Cookies.get());
      for (name of cookieNames) {
        if (name.endsWith('columns_hider-cookie')) {
          Cookies.remove(name);
        }
      }
    }
  })();

	$(".fm").animatedModal({
		modalTarget:'animatedModal',
		top:54,
		animatedIn:'slideInRight',
		animatedOut:'slideOutRight',
		animationDuration:'.6s',
		color:'#fff',
		afterOpen:function(){ $('select[multiple="multiple"]').multiselect();}
	});
	 
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	})
	
	$(".cbox").colorbox({
		"fixed":true,
		"overlayClose":false,
		"maxHeight":"90%",
		"close":'<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
	});
	$(".cbox-inline").colorbox({inline:true,
		"fixed":true,
		"overlayClose":false,
		 width:"70%",
		 "maxHeight":"90%",
		"close":'<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
	});
	
	 $('select[multiple="multiple"]').multiselect();

   $('.header-dropdown').hover(function() {
      $('.header-dropdown .dropdown-menu').show();
   }, function() {
      $('.header-dropdown .dropdown-menu').hide();
   });


    $('#select-c').change(function(){
    var v = $(this).val()

    if(v != '') {
      var a = $("<a>").attr("target","_blank").attr("href",v)
      a.click()
    }

    return false
  })
   
   $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");
      });

   $("#menu-toggle-2").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled-2");
          $('#menu ul').hide();
          Cookies.set('sitemenuClosed', $("#wrapper").hasClass("toggled-2"));
         
   });

   initMenu();


  $('.wait-loading').click(function(){
    $('#wait-loader').show();
  });

  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });

  $('.go-to-review-link').on('click', function() {
    let link = $(this).prev('.review_link').data('value');
    if (link && link.length > 0) {
      window.open(link, '_blank');
    }
  });

  $( window ).resize(function() {
    changeTableHeight();
  });

  function changeTableHeight() {
    const height = window.innerHeight - $('.navbar').outerHeight(true) - 45 - 25;
    $('.nayjest-grid table').height(height);
  }

  changeTableHeight();


})

$(function() { 
    // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // save the latest tab; use cookies if you like 'em better:
        localStorage.setItem('lastTab', $(this).attr('href'));
    });

    // go to the latest tab, if it exists:
    var lastTab = localStorage.getItem('lastTab');
  
    if (lastTab) {
        $('[href="' + lastTab + '"]').tab('show');
    }

    //$('#alerts').load("{{route('global.alerts')}}")

    $('.table-striped tbody [data-toggle="popover"]').popover({html:true,trigger:'hover'})


    $('input[data-toggle="toggle"]').change(function() {
      $.ajax({
        method: $(this).data('method') || 'POST',
        url: $(this).data('url'),
        data: {
          value: $(this).prop('checked') ? 1 : 0,
          pk: $(this).attr('data-pk'),
          name: $(this).attr('id')
        }
      }).done(function() {

      }).fail(function(error) {
        alert(error);
      });
      
      return false
    })
});