<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('API\V1')->prefix('v1')->group(function() {
	Route::get('/accounts', 'AccountController@indexAction');
	Route::get('/imports', 'ImportController@indexAction');
	Route::get('/disburses', 'DisburseController@indexAction');
	Route::post('/disburses', 'DisburseController@createAction');
	Route::get('/products/overviews', 'ProductController@getProductOverviewsAction');
});
