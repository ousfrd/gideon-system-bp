<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Auth::routes();

Route::get('/users', 'UserController@indexAction')->name("users")->middleware('can:users');
Route::any('/user/new', 'UserController@addAction')->name('user.add')->middleware('can:user.add');
Route::any('/user/edit/{id}', 'UserController@editAction')->name('user.edit')->middleware('can:user.edit');
Route::get('/user/change-password', 'UserController@changePassword')->name('user.change-password')->middleware('can:user.change-password');
Route::post('/user/do-schange-password', 'UserController@doChangePassword')->name('user.do-change-password');

Route::get('/user/{id}', 'UserController@viewAction')->name('user.view')->middleware(['auth', 'can:user.view,id']);
Route::any('/settings', 'UserController@settingsAction')->name('user.settings');

Route::get('/accounts', 'AccountController@indexAction')->name('accounts')->middleware('can:accounts');
Route::any('/account/new', 'AccountController@addAction')->name('account.add')->middleware('can:account.add');
Route::any('/account/edit/{id}', 'AccountController@editAction')->name('account.edit')->middleware('can:account.edit');
Route::any('/account/delete/{id}', 'AccountController@deleteAction')->name('account.delete')->middleware('can:account.delete');
Route::any('/account/view/{id}', 'AccountController@viewAction')->name('account.view')->middleware('can:account.view');
Route::any('/account/checkmws/{id}', 'AccountController@checkmwsAction')->name('account.checkmws')->middleware('can:account.checkmws');
Route::any('/account/advertising_api_auth/{id}', 'AccountController@advertisingapiAuthAction')->name('account.advertising_api_auth')->middleware('can:account.advertising_api_auth');
Route::any('/account/advertising_api_token', 'AccountController@advertisingapiTokenAction')->name('account.advertising_api_token')->middleware('can:account.advertising_api_token');
Route::post('/accounts/ajaxsave', 'AccountController@ajaxsaveAction')->name('account.ajaxsave')->middleware('can:account.edit');

Route::get('/products', 'ProductController@indexAction')->name('products')->middleware('can:products');
Route::get('/brands', 'BrandController@indexAction')->name('brands')->middleware('can:brands');
Route::delete('/brands/{id}', 'BrandController@destroy')->name('brand.delete')->middleware('can:brand.delete');
Route::get('/brand/{brand}', 'BrandController@productsAction')->name('brand.products')->middleware('can:brand.products');
Route::any('/product/new', 'ProductController@addAction')->name('product.add')->middleware('can:product.add');
Route::any('/product/edit/{id}', 'ProductController@editAction')->name('product.edit')->middleware('can:product.edit');
Route::any('/product/delete/{id}', 'ProductController@deleteAction')->name('product.delete')->middleware('can:product.delete');
Route::get('/product/view/{id}/{country?}', 'ProductController@viewAction')->name('product.view')->middleware('can:product.view');
Route::any('/product/ajaxsave', 'ProductController@ajaxsaveAction')->name('product.ajaxsave')->middleware('can:product.ajaxsave');
Route::get('/product/low-inventory', 'ProductController@lowInventoryAction')->name('products.low')->middleware('can:products.low');
Route::get('/product/low-sales', 'ProductController@lowSalesAction')->name('products.lowsales')->middleware('can:products.lowsales');
Route::get('/product/newreleases', 'ProductController@indexNewReleasesAction')->name('products.newreleases')->middleware('can:products.newreleases');
Route::get('/product/newreleases/create', 'ProductController@createNewReleases')->name('products.newreleases.create')->middleware('can:products.newreleases.create');
Route::any('/product/newreleases/save', 'ProductController@newReleasesSaveAction')->name('products.createnewreleases.save')->middleware('products.createnewreleases.save');


Route::get('/listings', 'ListingController@indexAction')->name('listings')->middleware('can:listings');
Route::any('/listing/ajaxsave', 'ListingController@ajaxsaveAction')->name('listing.ajaxsave')->middleware('can:listing.ajaxsave');

Route::get('/listing/excess-inventory', 'ListingController@excessInventoryAction')->name('listings.excess')->middleware('can:listings.excess');
Route::get('/listing/low-inventory', 'ListingController@lowInventoryAction')->name('listings.low')->middleware('can:listings.low');
Route::get('/listing/low-sales', 'ListingController@lowSalesAction')->name('listings.lowsales')->middleware('can:listings.lowsales');

Route::post('/listing/{id}/update-field', 'ListingController@updateField')->name('listing.update-field')->middleware('can:listing.update-field');
Route::get('/listing/{id}/{country?}', 'ListingController@viewAction')->name('listing.view')->middleware('can:listing.view');

Route::any('/listing/expense/add/{id?}', 'ListingController@addExpenseAction')->name('listing.addexpense')->middleware('can:listing.addexpense');

Route::any('/listing/expense/delete/{id}', 'ListingController@deleteExpenseAction')->name('listing.deleteexpense')->middleware('can:listing.deleteexpense');
Route::any('/listing/expense/edit/{id}', 'ListingController@editExpenseAction')->name('listing.editexpense')->middleware('can:listing.editexpense');
Route::post('/listing/insertcardstatus', 'ListingController@updateInsertCardStatus')->name('listing.updateInsertCardStatus')->middleware('can:manage-insertcard');

Route::any('/order/{id}', 'OrderController@viewAction')->name('order.view')->middleware('can:order.view');
Route::any('/report/call_backs/periodicGrossSales', 'ReportController@periodicGrossSales')->name('order.call_backs.periodicGrossSales')->middleware('can:order.call_backs.periodicGrossSales');
Route::any('/report/call_backs/v2/periodicGrossSales', 'ReportController@v2PeriodicGrossSales')->name('order.call_backs.periodicGrossSales.v2')->middleware('can:order.call_backs.periodicGrossSales.v2');;

Route::any('/report/call_backs/periodicBestSellers', 'ReportController@periodicBestSellers')->name('listing.call_backs.periodicBestSellers')->middleware('can:listing.call_backs.periodicBestSellers');
Route::any('/report/call_backs/v2periodicBestSellers', 'ReportController@v2PeriodicBestSellers')->name('listing.call_backs.v2periodicBestSellers')->middleware('can:listing.call_backs.v2periodicBestSellers');

Route::get('/csorders', 'OrderController@csordersAction')->name('csorders')->middleware('can:manage-outsource-orders');
Route::get('/orders', 'OrderController@ordersAction')->name('orders')->middleware('can:orders');
Route::any('/orders/ajaxsave', 'OrderController@ajaxsaveAction')->name('order.ajaxsave')->middleware('can:order.ajaxsave');
Route::get('/orders/{amazon_order_id}/order-items.json', 'OrderController@orderItems')->name('order.orderItems')->middleware('can:order.orderItems');

Route::get('/reports/lists', 'ReportController@listsAction')->name('reports.lists')->middleware('can:reports.lists');
Route::get('/reports/new', 'ReportController@newAction')->name('reports.new')->middleware('can:reports.new');
Route::any('/reports/generate', 'ReportController@ajaxReportAction')->name('report.generate')->middleware('can:report.generate');
Route::get('/reports/download/{id}', 'ReportController@downloadAction')->name('report.download')->middleware('can:report.download');
Route::get('/reports/productDailyProfit', 'ReportController@productDailyProfitAction')->name('report.productDailyProfit')->middleware('can:report.productDailyProfit');
Route::any('/reports/fetchProductDailyProfit', 'ReportController@fetchProductDailyProfitAction')->name('report.fetchdailyprofits')->middleware('can:report.fetchdailyprofits');
Route::any('/reports/fetchProductDailyProfitRows', 'ReportController@fetchProductDailyProfitRowsAction')->name('report.fetchdailyprofitsrows')->middleware('can:report.fetchdailyprofitsrows');
Route::get('/reports/monthlyProfit', 'ReportController@monthlyProfitAction')->name('report.monthlyProfit')->middleware('can:report.monthlyProfit');
Route::any('/reports/fetchMonthlyProfit', 'ReportController@fetchMonthlyProfitAction')->name('report.fetchmonthlyprofits')->middleware('can:report.fetchmonthlyprofits');
Route::any('/reports/updatePmGoal', 'ReportController@updatePmGoalAction')->name('report.updatePmGoal')->middleware('can:report.updatePmGoal');

Route::get('/reports/product-performance', 'ReportController@productPerformanceAction')->name('report.product-performance')->middleware('can:report.product-performance');
Route::get('/reports/disburse', 'ReportController@disburseAction')->name('report.disburse')->middleware('can:report.disbursement');
Route::get('/reports/disbursement', 'ReportController@disbursementAction')->name('report.disbursement')->middleware('can:report.disbursement');
Route::any('/reports/fetchDisbursement', 'ReportController@fetchDisbursementAction')->name('report.fetchdisbursement');
Route::any('/reports/fetchDisbursementRows', 'ReportController@fetchDisbursementRowsAction')->name('report.fetchdisbursementrows');
Route::any('/reports/fetchDisbursementbyAccount', 'ReportController@fetchDisbursementbyAccountAction')->name('report.fetchdisbursementbyaccount');
Route::any('/reports/fetchDisbursementbyAccountChart', 'ReportController@fetchDisbursementbyAccountChartAction')->name('report.fetchdisbursementbyaccountchart');
Route::any('/reports/fetchDisbursementbyDepositDate', 'ReportController@fetchDisbursementbyDepositDateAction')->name('report.fetchdisbursementbydepositdate');

Route::get('/email/lists', 'EmailController@indexAction')->name('email.lists');
Route::any('/email/templates', 'EmailController@templateAction')->name('email.templates');
Route::any('/email/templates/new', 'EmailController@addTemplateAction')->name('email.template.add');
Route::any('/email/templates/edit/{id}', 'EmailController@editTemplateAction')->name('email.template.edit');
Route::any('/email/templates/delete/{id}', 'EmailController@deleteTemplateAction')->name('email.template.delete');
Route::get('/email/view/{id}', 'EmailController@viewAction')->name('email.view');
Route::any('/email/templates/ajaxsave', 'EmailController@ajaxSaveTemplateAction')->name('email.template.ajaxsave');
Route::get('/email/reviewer_profiles', 'EmailController@profileAction')->name('email.profiles');

Route::get('/sponsored-ads', 'AdController@listAction')->name('sponsored-ads');

Route::any('/inbount-shipment/create-plan/{id?}', 'InboundShipmentController@createShipmentPlanAction')->name('inbound_shipment.create_plan');
Route::any('/inbount-shipment/delete/{id}', 'InboundShipmentController@deleteAction')->name('inbound_shipment.delete');


Route::any('/inbount-shipment/ajaxsave', 'InboundShipmentController@ajaxsaveAction')->name('shipment.ajaxsave');

Route::any('/inbount-shipments', 'InboundShipmentController@indexAction')->name('inbound_shipments');


Route::get('/utils/ajax/accounts', 'UtilController@ajaxAccountsAction')->name('utils.ajax.accounts')->middleware('can:utils.ajax.accounts');
Route::get('/utils/ajax/marketplaces/{account?}', 'UtilController@ajaxMarketplacesAction')->name('utils.ajax.marketplaces')->middleware('can:utils.ajax.marketplaces');
Route::get('/utils/ajax/products', 'UtilController@ajaxProductsAction')->name('utils.ajax.products')->middleware('can:utils.ajax.products');
Route::get('/utils/ajax/product-managers', 'UtilController@ajaxProductManagersAction')->name('utils.ajax.product-managers')->middleware('can:utils.ajax.product-managers');
Route::get('/utils/ajax/product-addcost', 'UtilController@ajaxProductAddcostAction')->name('utils.ajax.product-addcost')->middleware('can:utils.ajax.product-addcost');
Route::get('/utils/ajax/send-test-email', 'UtilController@ajaxSendTestEmailAction')->name('utils.ajax.send-test-email')->middleware('can:utils.ajax.send-test-email');
Route::post('/utils/ajax/insert-product-image', 'UtilController@insertProductImageAction')->name('utils.ajax.insert-product-image')->middleware('can:utils.ajax.insert-product-image');

Route::get('/alerts','IndexController@globalAlertsAction')->name('global.alerts');

Route::get('/search', 'IndexController@searchAction')->name('search')->middleware('can:search');


Route::get('/home', 'IndexController@indexAction')->name('dashboard');
Route::get('/', 'IndexController@indexAction')->name('home');
Route::get('/addcost', 'IndexController@addcostAction')->name('index.addcost')->middleware('can:index.addcost');
Route::get('/product_cost', 'IndexController@addProductCostAction')->name('index.addproductcost')->middleware('can:index.addproductcost');

Route::get('/ads', 'AdController@indexAction')->name('ad.index')->middleware('can:ad.index');
Route::get('/ads/used-analyze', 'AdController@analyzeAction')->name('ad.used-analyze')->middleware('can:ad.used-analyze');

Route::any('/ads/fetchrows', 'AdController@fetchRowsAction')->name('ad.fetchrows')->middleware('can:ad.fetchrows');
Route::any('/ads/fetchcampaigns', 'AdController@fetchCampaignsAction')->name('ad.fetchcampaigns')->middleware('can:ad.fetchcampaigns');
Route::any('/ads/fetchadgroups', 'AdController@fetchAdGroupsAction')->name('ad.fetchadgroups')->middleware('can:ad.fetchadgroups');
Route::any('/ads/fetchkeywords', 'AdController@fetchKeywordsAction')->name('ad.fetchkeywords')->middleware('can:ad.fetchkeywords');
Route::any('/ads/fetchproducts', 'AdController@fetchProductsAction')->name('ad.fetchproducts')->middleware('can:ad.fetchproducts');
Route::any('/ads/fetchsearchterms', 'AdController@fetchSearchtermsAction')->name('ad.fetchsearchterms')->middleware('can:ad.fetchsearchterms');
Route::any('/ads/updateCampaignState', 'AdController@updateCampaignStateAction')->name('ad.updateCampaignState')->middleware('can:ad.updateCampaignState');
Route::any('/ads/updateAutoSchedulerState', 'AdController@updateAutoSchedulerStateAction')->name('ad.updateAutoSchedulerState')->middleware('can:ad.updateAutoSchedulerState');
Route::any('/ads/updateCampaignDailyBudget', 'AdController@updateCampaignDailyBudgetAction')->name('ad.updateCampaignDailyBudget')->middleware('can:ad.updateCampaignDailyBudget');
Route::any('/ads/updateAdGroupDefaultBid', 'AdController@updateAdGroupDefaultBidAction')->name('ad.updateAdGroupDefaultBid')->middleware('can:ad.updateAdGroupDefaultBid');
Route::any('/ads/updateAdGroupsDefaultBid', 'AdController@updateAdGroupsDefaultBidAction')->name('ad.updateAdGroupsDefaultBid')->middleware('can:ad.updateAdGroupsDefaultBid');
Route::any('/ads/updateAdGroupState', 'AdController@updateAdGroupStateAction')->name('ad.updateAdGroupState')->middleware('can:ad.updateAdGroupState');
Route::any('/ads/updateKeywordBid', 'AdController@updateKeywordBidAction')->name('ad.updateKeywordBid')->middleware('can:ad.updateKeywordBid');
Route::any('/ads/updateKeywordState', 'AdController@updateKeywordStateAction')->name('ad.updateKeywordState')->middleware('can:ad.updateKeywordState');
Route::any('/ads/updateKeywordGroupBid', 'AdController@updateKeywordGroupBidAction')->name('ad.updateKeywordGroupBid')->middleware('can:ad.updateKeywordGroupBid');


Route::get('/ads/analyze', 'AdController@agAnalyzeAction')->name('ad.analyze');
Route::any('/ads/agfetchcampaigns', 'AdController@agFetchCampaignsAction')->name('ad.agfetchcampaigns');
Route::any('/ads/agfetchcampaignstotal', 'AdController@agFetchCampaignsTotalAction')->name('ad.agfetchcampaignstotal');
Route::any('/ads/agfetchadgroups', 'AdController@agFetchAdGroupsAction')->name('ad.agfetchadgroups');
Route::any('/ads/agfetchkeywords', 'AdController@agFetchKeywordsAction')->name('ad.agfetchkeywords');
Route::any('/ads/agfetchsearchterms', 'AdController@agFetchSearchtermsAction')->name('ad.agfetchsearchterms');
Route::any('/ads/agfetchproducts', 'AdController@agFetchProductsAction')->name('ad.agfetchproducts');


Route::get('/ads/manage', 'AdController@manageAction')->name('ad.manage');

Route::any('/ads/campaigns/new', 'AdController@createCampaignAction')->name('ad.campaigns.new');
Route::get('/ads/campaigns/{id}/view', 'AdController@viewCampaignAction')->name('ad.campaigns.view');
Route::any('/ads/campaigns/{id}/edit', 'AdController@editCampaignAction')->name('ad.campaigns.edit');
Route::any('/ads/campaigns/{id}/negative_keywords', 'AdController@campaignNegativeKeywordsAction')->name('ad.campaigns.negativeKeywords');
Route::any('/ads/campaigns/{id}/deleteNegativeKeywords', 'AdController@deleteCampaignNegativeKeywordsAction')->name('ad.campaigns.deleteCampaignNagativeKeywords');
Route::any('/ads/campaigns/{campaignId}/negativeKeywords/{keywordId}/delete', 'AdController@deleteCampaignNegativeKeywordAction')->name('ad.campaigns.deleteCampaignNagativeKeyword');
Route::any('/ads/campaigns/{id}/adgroups', 'AdController@campaignAdGroupsAction')->name('ad.campaigns.adgroups');
Route::any('/ads/campaigns/{id}/delete', 'AdController@deleteCampaign')->name('ad.campaigns.delete');




Route::any('/ads/campaigns/{id}/adgroups/new', 'AdController@createAdGroupAction')->name('ad.adgroups.new');
Route::get('/ads/campaigns/{id}/adgroups/{gid}/view', 'AdController@viewAdGroupAction')->name('ad.adgroups.view');
Route::any('/ads/campaigns/{id}/adgroups/{gid}/edit', 'AdController@editAdGroupAction')->name('ad.adgroups.edit');
Route::any('/ads/campaigns/{id}/adgroups/{gid}/negative_keywords', 'AdController@adGroupNegativeKeywordsAction')->name('ad.adgroups.negativeKeywords');
Route::any('/ads/campaigns/{id}/adgroups/{gid}/keywords', 'AdController@adGroupKeywordsAction')->name('ad.adgroups.keywords');

Route::get('/ads/rules', 'AdController@rulesAction')->name('ad.rules');
Route::any('/ads/rules/new', 'AdController@createRuleAction')->name('ad.rules.new');
Route::any('/ads/rules/{id}/edit', 'AdController@editRuleAction')->name('ad.rules.edit');
Route::any('/ads/rules/delete/{id}', 'AdController@deleteRuleAction')->name('ad.rules.delete');
Route::any('/ads/rules/fetchrules', 'AdController@fetchRulesAction')->name('ad.rules.fetch');

// Route::get('/ad/request-report/{id}', 'AdController@requestReportAction')->name('ad.request_report');
// Route::get('/ad/get-report/{id}', 'AdController@getReportAction')->name('ad.get_report');
// Route::get('/ad/sandbox/{id}', 'AdController@sandboxAction')->name('ad.sandbox');
// Route::get('/ad/campaigns/{id}', 'AdController@listCampaignsAction')->name('ad.list_campaigns');
// Route::get('/ad/snapshots/{id}/{recordType}', 'AdController@getSnapshotAction')->name('ad.get_snapshot');



/**
 * Oliver add Business Report
 */
Route::any('/business_reports', 'BusinessReportController@indexAction')->name('business.list')->middleware('business.list');
Route::get('/business_report/{asin}', 'BusinessReportController@viewAction')->name('business.report')->middleware('business.report');

Route::get('/redeems/dashboard', 'RedeemController@dashboard')->name('redeems.dashboard')->middleware('can:redeems.dashboard');
Route::any('/redeems/free-product', 'RedeemController@freeProductAction')->name('redeems.freeProduct')->middleware('can:redeems.freeProduct');
Route::get('/redeems/claim-review-campaign/{claim_review_campaign_id}/free-product', 'RedeemController@freeProductAction')->name('redeems.claimReviewCampaignFreeProduct');

Route::any('/redeems/giftcard', 'RedeemController@giftcardAction')->name('redeems.giftcard')->middleware('can:redeems.giftcard');
Route::get('/redeems/claim-review-campaign/{claim_review_campaign_id}/giftcard', '
@giftcardAction')->name('redeems.claimReviewCampaignGiftCard');

Route::any('/redeems/other', 'RedeemController@otherAction')->name('redeems.other')->middleware('can:redeems.other');;
Route::get('/redeems/claim-review-campaign/{claim_review_campaign_id}/other', 'RedeemController@otherAction')->name('redeems.claimReviewCampaignOther');


Route::any('/redeems/warehouse-request', 'RedeemController@warehouseRequestAction')->name('redeems.warehouseRequest');
Route::any('/redeems/outofstock-request', 'RedeemController@outofstockRequestAction')->name('redeems.outofstockRequest');


Route::any('/redeems/followup', 'RedeemController@followupAction')->name('redeems.followup')->middleware('can:redeems.followup');
Route::any('/redeems/refund', 'RedeemController@refundAction')->name('redeems.refund')->middleware('can:redeems.refund');
Route::any('/redeems/replacement', 'RedeemController@replacementAction')->name('redeems.replacement')->middleware('can:redeems.replacement');
Route::any('/redeem/ajaxsave', 'RedeemController@ajaxsaveAction')->name('redeem.ajaxsave')->middleware('can:redeem.ajaxsave');
Route::get('/redeems/delete-test', 'RedeemController@deleteTestData')->name('redeems.deleteTestData')->middleware('can:redeems.deleteTestData');
Route::post('/redeems/store', 'RedeemController@store')->name('redeems.store')->middleware('can:redeems.store');
Route::get('/redeems/{amazon_order_id}/check-exists', 'RedeemController@checkExists')->name('redeems.checkExists')->middleware('can:redeems.checkExists');

Route::get('/redeems/invite-reviews', 'RedeemController@inviteReviewRedeems')->name('redeems.list-invite-reviews')->middleware('can:redeems.list-invite-reviews');
Route::get('/redeems/invite-reviews-followup', 'RedeemController@inviteReviewFollowUp')->name('redeems.invite-reviews-followup')->middleware('can:redeems.invite-reviews-followup');
Route::get('/redeems/{id}/invite-review', 'RedeemController@inviteLetter')->name('redeems.invite-review')->middleware('can:redeems.invite-review');
Route::post('/redeems/{id}/send-invite-review-letter', 'RedeemController@sendInviteReviewLetter')->name('redeems.send-invite-review-letter')->middleware('can:redeems.send-invite-review-letter');
Route::get('/imports', 'ImportController@indexAction')->name('imports')->middleware('can:imports');
Route::get('/import_orders', 'ImportController@ordersImport')->name('import.orders')->middleware('can:import.orders');
Route::post('/import_orders_parse', 'ImportController@parseOrdersImport')->name('import.orders.parse')->middleware('can:import.orders.parse');
Route::post('/import_order_shipments_parse', 'ImportController@parseOrderShipmentsImport')->name('import.order_shipments.parse')->middleware('can:import.order_shipments.parse');

Route::get('/import_listings', 'ImportController@listingsImport')->name('import.listings')->middleware('can:import.listings');
Route::post('/import_listings_parse', 'ImportController@parseListingsImport')->name('import.listings.parse')->middleware('can:import.listings.parse');

Route::get('/import_inventory', 'ImportController@inventoryImport')->name('import.inventory')->middleware('can:import.inventory');
Route::post('/import_inventory_parse', 'ImportController@parseInventoryImport')->name('import.inventory.parse')->middleware('can:import.inventory.parse');

Route::get('/import_finances', 'ImportController@financesImport')->name('import.finances')->middleware('can:import.finances');
Route::post('/import_finances_parse', 'ImportController@parseFinancesImport')->name('import.finances.parse')->middleware('can:import.finances.parse');

Route::get('/import_ads', 'ImportController@adsImport')->name('import.ads')->middleware('can:import.ads');
Route::post('/import_ads_parse', 'ImportController@parseAdsImport')->name('import.ads.parse')->middleware('can:import.ads.parse');

Route::get('/import_campaigns', 'ImportController@campaignsImport')->name('import.campaigns')->middleware('can:import.campaigns');
Route::post('/import_campaigns_parse', 'ImportController@parseCampaignsImport')->name('import.campaigns.parse')->middleware('can:import.campaigns.parse');
Route::post('/import_searchterms_parse', 'ImportController@parseSearchTermsImport')->name('import.searchterms.parse')->middleware('can:import.searchterms.parse');

Route::get('/import_business', 'ImportController@businessImport')->name('import.business')->middleware('can:import.business');
Route::post('/import_business_parse', 'ImportController@parseBusinessImport')->name('import.business.parse')->middleware('can:import.business.parse');

Route::get('/import_stats', 'ImportController@ImportStats')->name('import.stats')->middleware('can:import.stats');
Route::any('/fetchImportStatsRows', 'ImportController@fetchImportStatsRowsAction')->name('import.fetchimportstatsrows')->middleware('can:import.fetchimportstatsrows');

Route::get('/marketing/order-list', 'MarketingController@listOrders')->name('marketing.listOrders')->middleware('can:marketing.listOrders');
Route::get('/marketing/order-shipment-list', 'MarketingController@listOrderShipments')->name('marketing.listOrderShipments')->middleware('can:marketing.listOrderShipments');
Route::post('/marketing/updateReviewStatus', 'MarketingController@updateReviewStatus')->name('marketing.updateReviewStatus')->middleware('can:marketing.updateReviewStatus');
Route::post('/marketing/updateCustomerInfo', 'MarketingController@updateCustomerInfo')->name('marketing.updateCustomerInfo')->middleware('can:marketing.updateCustomerInfo');

Route::get('/gift-card-template/asin/{asin}', 'GiftCardTemplateController@get')->name('gift-card-template.get')->middleware('can:gift-card-template.get');

Route::get('/managed-review-order/own-orders', 'ManagedReviewOrderController@ownOrders')->name('managed-review-order.own-orders')->middleware('can:managed-review-order.own-orders');
Route::get('/managed-review-order/check-review-orders', 'ManagedReviewOrderController@checkReviewOrders')->name('managed-review-order.check-review-orders')->middleware('can:managed-review-order.check-review-orders');
Route::get('/managed-review-order/need-review-orders', 'ManagedReviewOrderController@needReviewOrders')->name('managed-review-order.need-review-orders')->middleware('can:managed-review-order.need-review-orders');

Route::get('/customers/dashboard', 'CustomerController@dashboard')->name('customers.dashboard')->middleware('can:customers.dashboard');
Route::get('/customers/all/{country?}', 'CustomerController@all')->name('customers.all')->middleware('can:customers.all');
Route::get('/customers/repeat-buyers/{country?}', 'CustomerController@repeatBuyers')->name('customers.repeat-buyers')->middleware('can:customers.repeat-buyers');
Route::get('/customers/real-email-buyers/{country?}', 'CustomerController@realEmailBuyers')->name('customers.real-email-buyers')->middleware('can:customers.real-email-buyers');
Route::get('/customers/positive-reviewer/{country?}', 'CustomerController@positiveReviewer')->name('customers.positive-reviewer')->middleware('can:customers.positive-reviewer');
Route::get('/customers/negative-reviewer/{country?}', 'CustomerController@negativeReviewer')->name('customers.negative-reviewer')->middleware('can:customers.negative-reviewer');
Route::get('/customers/{country}/dashboard', 'CustomerController@countryDashboard')->name('customers.country-dashboard')->middleware('can:customers.country-dashboard');
Route::get('/customers/{id}', 'CustomerController@show')->name('customers.show')->middleware('can:customers.show');
Route::get('/customers/export/form', 'CustomerController@exportForm')->name('customers.export-form')->middleware('can:customers.export-form');
Route::post('/customers/export/generate', 'CustomerController@exportGenerate')->name('customers.export-generate')->middleware('can:customers.export-generate');
Route::get('/customers/export/history', 'CustomerController@exportHistory')->name('customers-export-history')->middleware('can:customers-export-history');

//!ATTENTION: Don't use Route::resources([]), it may break other parts. see AuthServiceProvider@getRoutes

Route::get('/claim-review-campaign/products/overview', 'ClaimReviewCampaignController@productsCampaignOverview')->name('claim-review-campaign.products-overview');
Route::get('/claim-review-campaign/update-product-statistic', 'ClaimReviewCampaignController@updateProductStatistic')->name('claim-review-campaign.update-product-statistic');
Route::get('/claim-review-campaign/filter-shipments', 'ClaimReviewCampaignController@filterShipments')->name('claim-review-campaign.filter-shipments')->middleware('can:claim-review-campaign.filter-shipments');
Route::post('/claim-review-campaign/filter-shipments', 'ClaimReviewCampaignController@filterShipments')->name('claim-review-campaign.filter-shipments')->middleware('can:claim-review-campaign.filter-shipments');
Route::get('/download/{file}', 'DownloadController@download')->name('download-file');

Route::resource('claim-review-campaign', 'ClaimReviewCampaignController');

Route::resource('gift-card-template', 'GiftCardTemplateController');
Route::resource('leave-review-task', 'LeaveReviewTaskController');
Route::resource('managed-review-order', 'ManagedReviewOrderController');
Route::resource('owned-buyer-cart-item', 'OwnedBuyerCartItemController');
Route::resource('divvy-transaction-record', 'DivvyTransactionRecordController');
Route::resource('clicksend-history', 'ClickSendHistoryController');

Route::resource('customers', 'CustomerController', ['except' => ['show']]);

Route::resource('customers.customer-notes', 'CustomerNoteController');

Route::resource('owned-buyer', 'OwnedBuyerController', ['except' => ['show']]);
Route::get('/owned-buyer/{id}', 'OwnedBuyerController@show')->name('owned-buyer.show');

Route::resource('product-cost', 'ProductCostController', ['only' => ['store', 'update', 'destroy']]);
Route::resource('product-shipping-cost', 'ProductShippingCostController', ['only' => ['store', 'update', 'destroy']]);
Route::resource('product-fbm-shipping-cost', 'ProductFbmShippingCostController', ['only' => ['store', 'update', 'destroy']]);


Route::post('/claim-review-campaign/{id}/ajaxUpdate', 'ClaimReviewCampaignController@ajaxUpdate')->name('claim-review-campaign.ajaxUpdate')->middleware('can:claim-review-campaign.ajaxUpdate');
Route::put('/claim-review-campaign/{id}/send', 'ClaimReviewCampaignController@send')->name('claim-review-campaign.send')->middleware('can:claim-review-campaign.send');
Route::post('/claim-review-campaign/{id}/clicksendConfig', 'ClaimReviewCampaignController@createClicksendConfig')->name('claim-review-campaign.createClicksendConfig')->middleware('can:claim-review-campaign.createClicksendConfig');
Route::post('/claim-review-campaign/{id}/config', 'ClaimReviewCampaignController@createConfig')->name('claim-review-campaign.createConfig')->middleware('can:claim-review-campaign.createConfig');
Route::get('/claim-review-campaign/{id}/review-clicksend', 'ClaimReviewCampaignController@clicksendReview')->name('claim-review-campaign.review-clicksend')->middleware('can:claim-review-campaign.review-clicksend');
Route::get('/claim-review-campaign/{id}/review', 'ClaimReviewCampaignController@clicksendReview')->name('claim-review-campaign.review')->middleware('can:claim-review-campaign.review');
Route::put('/claim-review-campaign/{id}/send-via-api', 'ClaimReviewCampaignController@sendViaApi')->name('claim-review-campaign.send-via-api')->middleware('can:claim-review-campaign.send-via-api');

Route::post('/owned_buyer/import', 'OwnedBuyerController@import')->name('owned_buyer.import')->middleware('can:owned_buyer.import');
Route::get('/owned_buyer/find-eligible-buyers/{asin?}', 'OwnedBuyerController@findEligibleBuyers')->name('owned_buyer.find-eligible-buyers')->middleware('can:owned_buyer.find-eligible-buyers');
Route::get('/owned_buyer/find-eligible-buyers/{asin}/last-order-before/{lastOrderBefore}/last-review-before/{lastReviewBefore}/reviewRateLessThan/{reviewRateLessThan}', 'OwnedBuyerController@findEligibleBuyers')->name('owned_buyer.find-eligible-buyers-with-parameters')->middleware('can:owned_buyer.find-eligible-buyers-with-parameters');
Route::post('/owned_buyer/do-find-eligible-buyers', 'OwnedBuyerController@doFindEligibleBuyers')->name('owned_buyer.do-find-eligible-buyers')->middleware('can:owned_buyer.do-find-eligible-buyers');
Route::post('/owned_buyer/updateStatus/{id}', 'OwnedBuyerController@updateStatus')->name('owned-buyer.update-status')->middleware('can:owned-buyer.update-status');
Route::get('/owned_buyer/prime-buyers', 'OwnedBuyerController@primeBuyers')->name('owned-buyer.prime-buyers')->middleware('can:owned-buyer.prime-buyers');
Route::get('/owned_buyer/new-buyers', 'OwnedBuyerController@newBuyers')->name('owned-buyer.new-buyers')->middleware('can:owned-buyer.new-buyers');
Route::post('/owned_buyer/{id}/add-payment-method', 'OwnedBuyerController@addPaymentMethod')->name('owned-buyer.add-payment-method')->middleware('can:owned-buyer.add-payment-method');
Route::post('/owned_buyer/{id}/update-payment-method', 'OwnedBuyerController@updatePaymentMethod')->name('owned-buyer.update-payment-method')->middleware('can:owned-buyer.update-payment-method');
Route::post('/divvy-transaction-record/import', 'DivvyTransactionRecordController@import')->name('divvy-transaction-record.import')->middleware('can:divvy-transaction-record.import');

Route::post('/products/{id}/update-field', 'ProductController@updateField')->name('product.update-field')->middleware('can:product.update-field');
Route::get('/products/{id}/review-overview', 'ProductController@reviewOverview')->name('product.review-overview')->middleware('can:product.review-overview');
Route::get('/products/{id}/sales-rank-history', 'ProductController@salesRankHistory')->name('product.sales-rank-history')->middleware('can:product.sales-rank-history');
Route::get('/product_categories', 'ProductCategoryController@index')->name('product-category.index');
Route::get('/product_categories/create', 'ProductCategoryController@create')->name('product-category.create');
Route::post('/product_categories/store', 'ProductCategoryController@store')->name('product-category.store');
Route::delete('/product_categories/{id}', 'ProductCategoryController@destroy')->name('product-category.delete');
Route::any('/product_categories/ajaxsave', 'ProductCategoryController@ajaxsaveAction')->name('product-category.ajaxsave');

Route::post('/product/update-info', 'ProductUpdateController@updateInfo')->name('product.update-info')->middleware('can:product.update-info');

Route::post('/product/update-sales-rank', 'SalesRankUpdateController@updateSalesRank')->name('product.update-sales-rank')->middleware('can:product.update-sales-rank');

// Route::post('/review/info', 'ReviewController@insertReview')->name('review.insert-review')->middleware('can:review.insert-review');
// Route::get('/reviews/{asin?}', 'ReviewController@indexAction')->name('reviews')->middleware('can:reviews');
Route::post('/review/info', 'ReviewController@insertReview')->name('review.insert-review');
Route::get('/reviews/{asin?}', 'ReviewController@indexAction')->name('reviews');

Route::post('/updateReviewCategory', 'ReviewController@updateReviewCategory')->name('updateReviewCategory')->middleware('can:updateReviewCategory');

Route::post('/review/updateReviewStatus', 'ReviewController@updateReviewStatus')->name('review.updateReviewStatus')->middleware('can:review.updateReviewStatus');
Route::post('/review/updateReviewFollowUpStatus', 'ReviewController@updateReviewFollowUpStatus')->name('review.updateReviewFollowUpStatus');

Route::get('/inventory-management/summary', 'InventoryManagementController@summary')->name('inventory-management.summary')->middleware('can:inventory-management.summary');

Route::resource('product-review-overview', 'ProductReviewOverviewController', ['only' => ['store']]);
Route::get('/product-review-overview/products', 'ProductReviewOverviewController@getProducts')->name('product-review-overview.products')->middleware('secureIpAccess');

Route::resource('invite-review-letter-template', 'InviteReviewLetterTemplateController')->middleware('auth');

Route::post('/warehouse-activities', 'WarehouseActivityController@store')->name('warehouse-activity.store')->middleware(['auth', 'can:warehouse-activity.store']);
Route::delete('/warehouse-activities/{id}', 'WarehouseActivityController@destroy')->name('warehouse-activity.destroy')->middleware(['auth', 'can:warehouse-activity.destroy']);