<?php

namespace App;

use Illuminate\Support\Facades\Log;

use App\Customer;
use Excel;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class CustomerExportor extends Model
{
    protected $guarded = ["id", "file"];

    public function exportFile() {
        $filters = [];
        $filters['seller_accounts'] = json_decode($this->seller_accounts);
        $filters['asins'] = json_decode($this->asins);
        $filters['has_real_email'] = $this->has_real_email;
        $filters['excludes_refund_orders'] = $this->excludes_refund_orders;
        $filters['is_repeated'] = $this->is_repeated;
        $filters['is_positive_reviewer'] = $this->is_positive_reviewer;
        $filters['is_negative_reviewer'] = $this->is_negative_reviewer;
        $filters['first_purchase_date'] = $this->first_purchase_date;
        $filename = self::exportByFilter($filters);
        $this->file = $filename;
        $this->save();
    }

    public function getSellerAccountsArrayAttribute() {
        if ($this->seller_accounts) {
            return json_decode($this->seller_accounts);
        } else {
            return null;
        }
    }

    public function getAsinsArrayAttribute() {
        if ($this->asins) {
            return json_decode($this->asins);
        } else {
            return null;
        }
    }

    public function getFileUrlAttribute() {
        if ($this->file) {
            return \Storage::disk(config('giftcard.storage'))->url($this->file);
        } else {
            return null;
        }
    }

    public static function exportByFilter($filters) {
        $customers = Customer::getCustomers($filters);
        $filename = "customers-".Carbon::now()->toIso8601String();
        $file = Excel::create($filename, function($excel) use($filters, $customers) {
            $excel->setTitle('Sellers Profit Report');
            $excel->setCreator('PL Inventory')
                    ->setCompany('KB');
            $excel->setDescription('KB PL Customer Export');
            $excel->sheet('Filters', function($sheet) use($filters, $customers) {
                $headerRowNum = 1;
                $dataRowNum = 2;
                $header = ['Sellers', 'Asins', 'Real Email', 'Excludes Refund Orders', 'IsRepeatBuyer', 'IsPositiveReviewer', 'IsNegativeReviewer', 'FirstOrderDate'];
                $sheet->row($headerRowNum, $header);
                $data = [];
                if (array_key_exists('seller_accounts', $filters) && !empty($filters['seller_accounts'])) {
                    $data[0] = join(" | ", $filters['seller_accounts']);
                } else {
                    $data[0] = "";
                }
                if (array_key_exists('asins', $filters) && !empty($filters['asins'])) {
                    $data[1] = join(" | ", $filters['asins']);
                } else {
                    $data[1] = "";
                }
                if (array_key_exists('has_real_email', $filters) && $filters['has_real_email']) {
                    $data[2] = "Yes";
                } else {
                    $data[2] = "No";
                }
                if (array_key_exists('excludes_refund_orders', $filters) && $filters['excludes_refund_orders']) {
                    $data[3] = "Yes";
                } else {
                    $data[3] = "No";
                }
                if (array_key_exists('is_repeated', $filters) && $filters['is_repeated']) {
                    $data[4] = "Yes";
                } else {
                    $data[4] = "Not Care";
                }
                if (array_key_exists('is_positive_reviewer', $filters) && $filters['is_positive_reviewer']) {
                    $data[5] = "Yes";
                } else {
                    $data[5] = "Not Care";
                }
                if (array_key_exists('is_negative_reviewer', $filters) && $filters['is_negative_reviewer']) {
                    $data[6] = "Yes";
                } else {
                    $data[6] = "Not Care";
                }
                if (array_key_exists('first_purchase_date', $filters) && $filters['first_purchase_date']) {
                    $data[7] = $filters['first_purchase_date'];
                } else {
                    $data[7] = "Not Care";
                }
                $sheet->row($dataRowNum, $data);
            });

            $excel->sheet('Customers', function($sheet) use($filters, $customers) {
                $rowNum = 1;
                $header = [
                    'name', 'email', 'amazon_buyer_email', 
                    'facebook_account', 'wechat_account', 'bought_orders', 'Products',
                    'bought_orders_count', 'refunded_orders', 'refunded_orders_count', 
                    'leave_possitive_reviews_count', 
                    'leave_negative_reviews_count', 'claim_reviews_count', 
                    'invite_reviews_count', 'redeems_count', 'city', 
                    'state', 'postal_code', 'country'
                ];
                $sheet->row($rowNum, $header);
                foreach($customers as $customer){
                    $rowNum++;
                    $productTile = [];
                    $orderNumbers = explode(',', $customer->bought_orders);
                    foreach($orderNumbers as $orderNumber){
                        $orderNumber = trim($orderNumber,"[]\"");
                        $titles = OrderItem::select('Title')->where('AmazonOrderId', '=', $orderNumber)->get();
                        foreach($titles as $title){
                            $productTile[] = $title->Title;
                        }
                    }
                    $titles = implode($productTile);
                    $data = [
                        $customer->name, $customer->email, $customer->amazon_buyer_email,
                        $customer->facebook_account, $customer->wechat_account, $customer->bought_orders, $titles,
                        $customer->bought_orders_count, $customer->refunded_orders, $customer->refunded_orders_count,
                        $customer->leave_possitive_reviews_count,
                        $customer->leave_negative_reviews_count, $customer->claim_reviews_count,
                        $customer->invite_reviews_count, $customer->redeems_count, $customer->city,
                        $customer->state, $customer->postal_code, $customer->country 
                    ];

                    try {
                        $sheet->row($rowNum, $data);
                    } catch (\Exception $e) {
                        Log::warning($data);
                        Log::warning($e->getMessage());
                    }
                }
            });
        })->string('xls');
        $storagePath = 'customer-exportor/'.$filename.'.xls';
        \Storage::disk(config('giftcard.storage'))->put($storagePath, $file, 'public');
        return $storagePath;
    }

    public static function mostRecent($num = 100) {
        return self::orderBy('id', 'desc')->limit($num)->get();
    }

    public static function boot()
    {
        parent::boot();
        self::saving(function($model) {
            if ($model->isDirty('seller_accounts') && $model->seller_accounts && is_array($model->seller_accounts)) {
                $model->seller_accounts = json_encode($model->seller_accounts);
            }
            if ($model->isDirty('asins') && $model->asins && is_array($model->asins)) {
                $model->asins = json_encode($model->asins);
            }
        });

    }
}
