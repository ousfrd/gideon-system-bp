<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use App\Traits\MassInsertOrUpdate;

class AsinWatcher extends Model
{
	
	use MassInsertOrUpdate;

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'asin_watcher';

}

