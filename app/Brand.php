<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Listing;
use App\Product;
use DB;

class Brand extends Model
{
    //
    protected $table = 'brands';
	protected $fillable = ['name'];
	
	static function insertBrand($name) {
		$brand = Brand::where('name', $name)->get();
		$brand_new = ['name' => $name];
		if ($brand -> isEmpty()) {
			$newBrand = new Brand($brand_new);
			$newBrand->save();
		};
	}

	static function sellersOfBrandCount($name) {
		$sellersOfBrandCount = DB::table('seller_listings')
				   ->select('seller_id')
				   ->join('products', 'seller_listings.asin', '=', 'products.asin')
				   ->where('products.brand', $name)
				   ->groupBy('seller_id')
				   ->pluck('seller_id')
				   ->count();
		return $sellersOfBrandCount;
	}

	static function productsOfBrandCount($name) {
		$productsOfBrandCount = Product::where('brand', $name)->count();
		return $productsOfBrandCount;
	}

	static function updateBrandOfAllProduct() {
		$brands = Product::select('brand')->distinct()->whereNotNull('brand')->get();
		foreach ($brands as $brand) {
			self::insertBrand($brand->brand);
		}
	}
}
