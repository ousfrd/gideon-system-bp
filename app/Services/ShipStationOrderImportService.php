<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Services;

use Illuminate\Support\Facades\Log;
use \ShipStation;
use \GuzzleHttp;
use App\Account;
use App\Order;
use App\OrderItem;
use App\Listing;
use App\Product;
use App\Import;
use App\ManagedReviewOrder;
use App\ProductStatistic;
use App\Helpers\ShipStationOrderConverter;
use ShipStation\Model\ListOrderswithparametersresponse;


class ShipStationOrderImportService
{
	public $orderConverter;
	protected $config;
	protected $client;
	protected $contentType = 'application/json';

	public function __construct($user, $password, $retry_enabled, $max_retry)
	{
		$this->orderConverter = new ShipStationOrderConverter();

		$this->config = ShipStation\Configuration::getDefaultConfiguration()
            ->setUsername($user)
            ->setPassword($password);

        $this->client = new GuzzleHttp\Client(array(
        	'curl' => array(CURLOPT_SSL_VERIFYPEER => false, CURLOPT_SSL_VERIFYHOST => false),
        	'allow_redirects' => false,
        	'cookies' => true,
        	'verify' => false,

        	'retry_enabled' => $retry_enabled,
        	'max_retry_attempts' => $max_retry,
        	'retry_after_header' => 'X-Rate-Limit-Reset',
        ));
	}

	public function refreshStores()
	{
		$storeApi = new ShipStation\Api\StoresApi($this->client, $this->config);
		$refreshStoreRequest = new ShipStation\Model\RefreshStorerequest();
		try {
			$resp = $storeApi->storesRefreshstoreByStoreIdAndRefreshDatePost(
				$this->contentType, $refreshStoreRequest);
		} catch (ShipStation\ApiException $e) {
			$message = sprintf(
				'[RefreshShipStationStoresFailed] message: %s, headers: %s, body: %s',
				$e->getMessage(), $e->getResponseHeaders(), $e->getResponseBody());
			Log::critical($message);
			$resp = $e;
		} catch (Exception $e) {
			Log::error($e->getMessage());
			$resp = NULL;
		}

		return $resp;
	}

	public function importOrders($orderDateStart = NULL)
	{
		$tz = new \DateTimeZone('America/Los_Angeles');
		$interval = new \DateInterval('P5D');

		$ordersApi = new ShipStation\Api\OrdersApi($this->client, $this->config);
		$storeApi = new ShipStation\Api\StoresApi($this->client, $this->config);
		$dftParams = [
			'customerName' => NULL,
			'itemKeyword' => NULL,
			'createDateStart' => NULL,
			'createDateEnd' => NULL,
			'modifyDateStart' => NULL,
			'modifyDateEnd' => NULL,
			'orderDateStart' => NULL,
			'orderDateEnd' => NULL,
			'orderNumber' => NULL,
			'orderStatus' => NULL,
			'paymentDateStart' => NULL,
			'paymentDateEnd' => NULL,
			'storeId' => NULL,
			'$sortBy' => NULL,
			'sortDir' => NULL,
			'page' => 1,
			'pageSize' => 500
		];
		try {
			$stores = $storeApi->storesGet();
			$fromDateAll = $toDateAll = NULL;
			foreach ($stores as $store) {
				if (stripos($store->getMarketplaceName(), 'amazon') === FALSE) {
					$message = sprintf(
						'[SkipNonAmazonStore] StoreID: %s, StoreName: %s',
						$store->getStoreId(), $store->getStoreName()
					);
					Log::info($message);
					continue;
				}

				$fromDate = $toDate = NULL;
				$succeedCnt = $failedCnt = 0;

				$page = 1;
				$pageSize = 500;
				if (!$orderDateStart) {
					$orderDate = ShipStationOrderConverter::toDateTime($store->getRefreshDate());
					$orderDate->sub($interval);
					$orderDateStart = $orderDate->format('Y-m-d\TH:i:s');
				}

				$message = sprintf(
					'Importing ShipStation Orders after %s - [Store %s - %s]...',
					$orderDateStart, $store->getStoreId(), $store->getStoreName());
				Log::info($message);

				while (true) {
					$params = array_merge($dftParams, [
						'orderDateStart' => $orderDateStart,
						'storeId' => $store->getStoreId(),
						'page' => $page,
					]);
					$resp = call_user_func_array(array($ordersApi, 'ordersGet'), $params);
					$result = $this->importShipStationOrders($store, $resp);

					$succeedCnt += $result['succeed'];
					$failedCnt += $result['failed'];

					if ($fromDate === NULL || $fromDate > $result['fromDate']) {
						$fromDate = $result['fromDate'];
					}
					if ($toDate === NULL || $toDate < $result['toDate']) {
						$toDate = $result['toDate'];
					}

					$page++;
					if ($resp->getPages() <= $page) {
						break;
					}
				}

				$this->updateImportStatus($store);

		        if ($fromDate && $toDate) {
					$this->updateSellerStatistics(
						$store, $fromDate->format('Y-m-d'), $toDate->format('Y-m-d'));

			        if ($fromDateAll === NULL || $fromDateAll > $fromDate) {
			        	$fromDateAll = $fromDate;
			        }
			        if ($toDateAll === NULL || $toDateAll < $toDate) {
			        	$toDateAll = $toDate;
			        }
		        }

				$message = sprintf(
					'Importing ShipStation Orders after %s Finished - [Store %s - %s], [Succeed: %s, Failed: %s]',
					$orderDateStart, $store->getStoreId(), $store->getStoreName(), $succeedCnt, $failedCnt);
				Log::info($message);
			}
		    ManagedReviewOrder::syncSelfOrdersWithOrderItem();
            ProductStatistic::buildStatistics(
            	$fromDateAll->format('Y-m-d'), $toDateAll->format('Y-m-d'));
		} catch (ShipStation\ApiException $e) {
			$headers = [];
			foreach ($e->getResponseHeaders() as $header) {
				array_push($headers, implode(': ', $header));
			}

			$message = sprintf(
				'[ImportShipStationOrdersFailed] message: %s, headers: %s, body: %s',
				$e->getMessage(), implode(';', $headers), $e->getResponseBody());
			Log::critical($message);
		} catch (Exception $e) {
			Log::error($e->getMessage());
		}
	}

	public function updateImportStatus($shipStationStore)
	{
		$account = $this->getAccount($shipStationStore->getStoreName());
		if ($account) {
			$import = new Import([
				'seller_id' => $account->seller_id,
				'country' => ShipStationOrderConverter::getCountryFromStore($shipStationStore),
				'type' => 'ShipStationOrders',
				'status' => Import::getImportedStatus(),
			]);
			$import->created_at = time();
			$import->updated_at = time();
			$import->save();
		}
	}

	public function updateSellerStatistics($shipStationStore, $fromDate, $toDate)
	{
		$account = $this->getAccount($shipStationStore->getStoreName());
		if ($account) {
    		ProductStatistic::buildStatisticsForSeller($account, $fromDate, $toDate);
		}
	}

	public function importShipStationOrders($store, ListOrderswithparametersresponse $resp)
	{
		$result = [
			'fromDate' => NULL,
			'toDate' => NULL,
			'total' => 0,
			'succeed' => 0,
			'failed' => 0
		];

		foreach ($resp->getOrders() as $shipStationOrder) {
			$order = $this->orderConverter->convert($store, $shipStationOrder);

			$result['total'] += 1;
			try {
				$imported = $this->importOrder($order);
				if (!$imported) {
					$result['failed'] += 1;
					continue;
				}

				$purchaseDate = \DateTime::createFromFormat(
					\DateTimeInterface::RFC3339, $order['PurchaseDate']);
				if ($result['fromDate'] === NULL || $result['fromDate'] > $purchaseDate) {
					$result['fromDate'] = $purchaseDate;
				}
				if ($result['toDate'] === NULL || $result['toDate'] < $purchaseDate) {
					$result['toDate'] = $purchaseDate;
				}

				$result['succeed'] += 1;
			} catch (Exception $e) {
				$message = sprintf(
					'[ImportShipStationOrderError] message: %s, order: %s',
					$e->getMessage(), $order);
				Log::error($message);

				$result['failed'] += 1;
			}
		}

		return $result;
	}

	public function importOrder(array $order)
	{
		$result = FALSE;

		$items = $order['items'];
		unset($order['items']);

		$account = $this->getAccount($order['seller_id']);
		if ($account === NULL) {
			$message = sprintf(
				'[OrderImportError] Reason: "Account Missing", StoreName: %s, OrderID: %s',
				$order['seller_id'], $order['AmazonOrderId']
			);
			Log::warning($message);

			return $result;
		}

		$order['seller_id'] = $account->seller_id;
		// If order items do not exist, order would not be imported
		$listingExists = FALSE;
		foreach ($items as $item) {
			$item['seller_id'] = $order['seller_id'];
			$listing = $this->getListing($item['seller_id'], $item['SellerSKU']);
			if ($listing === NULL) {
				$message = sprintf(
					'[OrderImportError] Reason: "Listing Missing", SellerId: %s, OrderID: %s, SellerSKU: %s',
					$item['seller_id'], $item['AmazonOrderId'], $item['SellerSKU']
				);
				Log::warning($message);
				continue;
			}

			$listingExists = TRUE;

			$item['Currency'] = $order['Currency'];

			// Listing related data
			$item['ASIN'] = $listing->asin;

            $product = Product::where([
            	['asin', '=', $item['ASIN']],
            	['country', '=', $item['country']]
            ])->first();
            if($product !== NULL) {
                $item['cost'] = $product->cost;
                $item['shipping_cost'] = $product->shipping_cost;
            }

            // Ignore additional fees, should update elsewhere
            if (FALSE) {
	            $item['giftwrap_fee'] = 0;
	            $item['item_promo'] = 0;
	            $item['shipping_promo'] = 0;
	            $item['total_promo'] = -1 * ($item['item_promo'] + $item['shipping_promo']);
            }

            $orderItem = OrderItem::where([
            	['seller_id', '=', $item['seller_id']],
            	['AmazonOrderId', '=', $item['AmazonOrderId']],
            	['SellerSKU', '=', $item['SellerSKU']],
            ])->first();
            if ($orderItem === NULL) {
            	$orderItem = new OrderItem();
            }
            if ($orderItem->fba_fee !== NULL && $orderItem->fba_fee != 0) {
            	unset($item['fba_fee']);
            }
            if ($orderItem->commission !== NULL && $orderItem->commission != 0) {
            	unset($item['commission']);
            }
            $orderItem->fill($item);

            $saved = $orderItem->save();
            if (!$saved) {
            	Log::warning('[OrderItemSaveError] AmazonOrderId: ' . $orderItem->AmazonOrderId);
            }
        }

        if ($listingExists) {
			$dbOrder = Order::where([
				['seller_id', '=', $order['seller_id']],
				['AmazonOrderId', '=', $order['AmazonOrderId']],
			])->first();
			if ($dbOrder === NULL) {
				$dbOrder = new Order();
				$dbOrder->fill($order);
			} else {
				// Only update OrderStatus/LastUpdateDate/FulfillmentChannel
				$dbOrder->OrderStatus = $order['OrderStatus'];
				// if ($dbOrder->PurchaseDate != $order['PurchaseDate']) {
				// 	Log::debug('[UpdateOrderPurchaseDate] AmazonOrderId: ' . $order['AmazonOrderId'] . ', PurchaseDate: ' . $order['PurchaseDate']);
				// 	$dbOrder->PurchaseDate = $order['PurchaseDate'];
				// }
				if ($order['LastUpdateDate']) {
					$dbOrder->LastUpdateDate = $order['LastUpdateDate'];
				}
				// $dbOrder->FulfillmentChannel = $order['FulfillmentChannel'];
				if ($dbOrder->SalesChannel != $order['SalesChannel']) {
					$dbOrder->SalesChannel = $order['SalesChannel'];
				}
			}

        	$saved = $dbOrder->save();
            if ($saved) {
				$orderRecord = Order::where([
					['seller_id', '=', $order['seller_id']],
					['AmazonOrderId', '=', $order['AmazonOrderId']],
				])->first();
            	Log::debug('[OrderSaved] AmazonOrderId: ' . $orderRecord->AmazonOrderId . ', PurchaseDate: ' . $orderRecord->PurchaseDate);
            } else{
            	Log::warning('[OrderSaveError] AmazonOrderId: ' . $order['AmazonOrderId']);
            }

        	$result = TRUE;
        }

        return $result;
	}

	public function getAccountCode($storeName)
	{
		preg_match('/(?P<code>^\d+)/', $storeName, $matches);
		if (isset($matches['code'])) {
			$code = $matches['code'];
		} else {
			$code = str_pad(strval(intval($storeName)), 3, '0');
		}

		return $code;
	}

	public function getAccount($storeName)
	{
		$accountCode = $this->getAccountCode($storeName);
		return Account::where('code', '=', $accountCode)->first();
	}

	public function getListing($sellerId, $sku)
	{
		return Listing::where('seller_id', '=', $sellerId)
			->whereRaw('UPPER(`sku`) LIKE ?', [strtoupper($sku)])
			->first();
	}
}
