<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;

class Report extends Model
{
    //
    protected $table = 'reports';

    protected $guarded = ['id', 'created_at', 'updated_at'];

     
    function user() {
        return $this->belongsTo('App\User', 'created_by');
    }
}
