<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use App\Traits\MassInsertOrUpdate;

class AdCampaignsReport extends Model
{
	use MassInsertOrUpdate;
	
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'ad_campaigns_reports';

	public $timestamps = false;
	
	public function account(){
		return $this->belongsTo('App\Account','seller_id','seller_id');
	}
	
	public function ad_campaign(){
		return $this->belongsTo('App\AdCampaign','campaignId','campaignId');
	}

	public static function get_beginning() {
		$first_record = AdCampaignsReport::orderBy('date', 'asc')->first();
		if($first_record) {
			return $first_record->date;
		} else {
			return "2010-01-20";
		}
	}
}
	