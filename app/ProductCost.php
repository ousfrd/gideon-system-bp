<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use Carbon\Carbon;

class ProductCost extends Model
{
    protected $fillable = ['product_id', 'start_date'];

    protected $appends = array('future', 'link');
    
    public function product() {
        return $this->belongsTo('App\Product');
    }

    public function getFutureAttribute() {
        if (Carbon::parse($this->start_date)->greaterThan(Carbon::today())) {
            return true;
        } else {
            return false;
        }
        
    }

    public function getLinkAttribute() {
        return route('product-cost.destroy', $this);
    }

    // it's only called when populate the table first time
    public static function fillInitialData() {
        $products = Product::all();
        foreach ($products as $product) {
            $costs = $product->productCosts;
            if ($costs->isEmpty()) {
                $cost = new ProductCost();
                $cost->product_id = $product->id;
                $cost->cost_per_unit = $product->cost;
                $cost->start_date = '2010-01-01';
                $cost->save();
            }
        } 
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function($model) {
            $costs = ProductCost::where('product_id', $model->product_id);
            $costs->update(['end_date' => $model->start_date]);
        });

        self::saved(function($model) {
            $product = $model->product;
            $product->cost = $model->cost_per_unit;
            $product->save();
        });

        self::deleting(function($model) {
            $cost = ProductCost::where('id', '!=', $model->id)->where('product_id', $model->product_id)->orderBy('start_date', 'desc')->first();
            if ($cost) {
                $cost->end_date = null;
                $cost->save();
            } 
        });
    }
}
