<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\OwnedBuyer;
use App\OwnedBuyerCartItem;
use App\LeaveReviewTask;
use Carbon\Carbon;
use App\OrderItemMarketing;
use DB;
use App\Traits\ReviewHandlable;
use App\Helpers\DataHelper;

class ManagedReviewOrder extends Model
{
    use ReviewHandlable;
    
    const INVITE_REVIEW_ORDER = 'INVITE_REVIEW_ORDER';
    const OWNED_BUYER_SELF_ORDER = 'OWNED_BUYER_SELF_ORDER';
    const OWNED_BUYER_OTHER_ORDER = 'OWNED_BUYER_OTHER_ORDER';
    const OWNED_BUYER_NOT_KNOWN_ORDER = 'OWNED_BUYER_NOT_KNOWN_ORDER';

    private static $ownedBuyers = [];

    protected $fillable = ['amazon_order_id']; 

    public function orderShipment() {
        return $this->belongsTo('App\Shipment', 'amazon_order_id', 'amazon_order_id');
    }

    public function leaveReviewTask() {
        if ($this->asin && $this->left_review_at) {
            return LeaveReviewTask::where("asin", $this->asin)
                            ->where("created_at", "<=", $this->left_review_at)
                            ->where("deadline", ">=", $this->left_review_at)
                            ->first();
        } else {
            return null;
        }
    }

    public static $types = [
        self::INVITE_REVIEW_ORDER, 
        self::OWNED_BUYER_SELF_ORDER, 
        self::OWNED_BUYER_OTHER_ORDER,
        self::OWNED_BUYER_NOT_KNOWN_ORDER
    ];

    public function ownedBuyer() {
        return $this->belongsTo('App\OwnedBuyer');
    }

    public static $status = [
        "ADDED_TO_CART" => 1,
        "PLACED_ORDER" => 2,
        "SYNCED" => 3,
        "INVITE_REVIEW_ORDER_NOT_FOUND" => 20,
        "SELF_ORDER_NOT_FOUND" => 21,
        "ORDER_COST_INCORRECT" => 22,
    ];

    public static function fillOrdersInfo() {
        $orders = self::all();
        foreach ($orders  as $order) {
            $order->fillInfo();
        }
    }

    public static function gridQuery() {
        $query = self::select('managed_review_orders.*', 'owned_buyers.id as owned_buyer_id', 'owned_buyers.nickname', 'owned_buyers.device_name', 'owned_buyers.browser', 'users.name')
                        ->leftJoin('users', 'managed_review_orders.created_by', '=', 'users.id')
                        ->leftJoin('owned_buyers', 'owned_buyers.id', '=', 'managed_review_orders.owned_buyer_id');
        return $query;
    }

    public static function selfOrdersQuery() {
        $query = self::gridQuery();
        $query->where('managed_review_orders.order_type', self::OWNED_BUYER_SELF_ORDER);
        return $query;
    }

    public static function checkReviewOrderQuery() {
        $query = self::gridQuery();
        $query->whereNull('managed_review_orders.review_link')->whereNotNull('managed_review_orders.review_title');
        return $query;
    }

    public static function needReviewOrderQuery() {
        $query = self::gridQuery();
        $query->where('managed_review_orders.need_review', true)->whereNull('managed_review_orders.review_title');
        return $query;
    }

    public static function gridQueryForTask($leaveReviewTask) {
        $asin = $leaveReviewTask->asin;
        $createdAt = $leaveReviewTask->created_at->toDateTimeString();
        $deadline = $leaveReviewTask->deadline;
        $query = self::select('managed_review_orders.*', 'owned_buyers.id as owned_buyer_id', 'owned_buyers.nickname', 'owned_buyers.device_name', 'owned_buyers.browser')
                        ->leftJoin('owned_buyers', 'owned_buyers.id', '=', 'managed_review_orders.owned_buyer_id')
                        ->where('asin', $asin)
                        ->where('order_type', ManagedReviewOrder::OWNED_BUYER_SELF_ORDER)
                        ->whereBetween('left_review_at', [$createdAt, $deadline]);
        return $query;
    }

    public static function gridQueryByBueryId($buyer_id) {
        $query = self::select('managed_review_orders.*', 'seller_accounts.id as seller_account_id', 'seller_accounts.name', 'seller_accounts.code', 'seller_accounts.seller_id')
                        ->leftJoin('seller_accounts', 'seller_accounts.seller_id', '=', 'managed_review_orders.seller_id')
                        ->where('owned_buyer_id', $buyer_id);
        return $query;
    }



    public function fillInfo() {
        $orderShipment = $this->orderShipment;
        if ($orderShipment) {
            $this->order_type = self::OWNED_BUYER_SELF_ORDER;
            $this->order_cost = $orderShipment->item_price + 
                                $orderShipment->item_tax + 
                                $orderShipment->shipping_price + 
                                $orderShipment->shipping_tax + 
                                $orderShipment->gift_wrap_price + 
                                $orderShipment->gift_wrap_tax + 
                                $orderShipment->ship_promotion_discount + 
                                $orderShipment->item_promotion_discount;
            $this->estimated_delivery_date = $orderShipment->estimated_arrival_date;
            $this->seller_id = $orderShipment->seller_id;
            $this->asin = $orderShipment->asin;
            $this->order_date = $orderShipment->purchase_date; 
            
            // mark this order as processed by self review team
            $orderItemMarketing = OrderItemMarketing::firstOrNew(['oim_amazon_order_id'=> $orderShipment->amazon_order_id]);
            if (empty($orderItemMarketing->oim_sku)) {
                $orderItemMarketing->oim_sku = $orderShipment->sku;
            }
			$orderItemMarketing->self_review = true;
            $orderItemMarketing->processed = true;
            $orderItemMarketing->asin = $orderShipment->asin;
            $orderItemMarketing->buyer_id = $orderShipment->buyer_email;
            $orderItemMarketing->seller_id = $orderShipment->seller_id;
            $orderItemMarketing->save();
        } else {
            if(empty($this->order_type)) {$this->order_type = self::OWNED_BUYER_OTHER_ORDER;}
            if ($this->order_type == self::OWNED_BUYER_SELF_ORDER) {
                OrderItemMarketing::createForSelfReview($this->amazon_order_id);
            }
        }
        $buyer = empty($this->owned_buyer_id) ? $this->findBuyer($orderShipment) : OwnedBuyer::find($this->owned_buyer_id);
        if (!empty($buyer)) {
            $this->owned_buyer_id = $buyer->id;
            $this->created_by = $buyer->created_by;
        }
        $this->save();
    }

    private function findBuyer($orderShipment){
        if (empty($orderShipment)) return null;
        
        if (count(self::$ownedBuyers) == 0 ) {
            self::$ownedBuyers = OwnedBuyer::with('shippingAddresses')->select('id', 'name')->get();
        }
        foreach (self::$ownedBuyers as $buyer) {
            $name = preg_replace('/\s/', '', strtolower($buyer->name));
            $recipent = preg_replace('/\s/' , '', strtolower($orderShipment->recipient_name));
            if ($name == $recipent) {
                echo "name: " .$name. ", recipent:" . $recipent ." \r\n";
                $zip = substr(trim($orderShipment->ship_postal_code), 0, 5);
                $shippingAddresses = $buyer->shippingAddresses;
                foreach ($shippingAddresses as $shippingAddress) {
                    $ship_postal_code = substr(trim($shippingAddress->ship_postal_code), 0, 5);
                    if ($ship_postal_code == $zip) {
                        return $buyer;
                    }
                }
            }
        }
        return null;
    }

    public static function importOrders($filePath) {
        $handle = fopen($filePath, "r");
        $rowsCount = 0;
        while ( ($row = fgetcsv($handle) ) !== FALSE ) {
            if ($rowsCount > 0) {
                $buyerId = 0;
                if (preg_match('/^\d+$/', $row[0])) {
                    $buyerId = $row[0];
                } else if (preg_match('/.*@.*\.\w+/', $row[0])) {
                    $email = $row[0];
                    $buyer = OwnedBuyer::where("email", trim($email))->first();
                    if ($buyer) {
                        $buyerId = $buyer->id;
                    }
                    echo $email . " " . $buyerId . " ; ";
                }
                $orderNumbers = array_slice($row, 1);
                foreach ($orderNumbers as $orderNumber) {
                    if (!empty(trim($orderNumber))) {
                        $order = ManagedReviewOrder::firstOrNew(['amazon_order_id' => trim($orderNumber)]);
                        if (empty($order->owned_buyer_id)) {$order->owned_buyer_id = $buyerId;}
                        $order->order_date = "2000-01-01";
                        $order->save();
                    }
                  }
            }
            $rowsCount++;
        }
    }

    /**
     * CSV file format
     * Account Nickname,buyer email,buyer phone,order id,order total,order-date,has-review
     *  Account Nickname: required
     *  email: optional if phone present
     *  phone: optional if email present
     *  order id: required, amazon order number
     *  order total: required
     *  order-date: optional (format: YYYY-MM-DD)
     *  has-review: optional, 1 means left review; empty or 0 means not yet.
     */
    public static function importBuyerOrders($filePath) {
        $handle = fopen($filePath, "r");
        $rowsCount = 0;
        while ( ($row = fgetcsv($handle) ) !== FALSE ) {
            if ($rowsCount > 0 && count($row) >= 5) {
                [$nickName, $email, $phone, $orderNumber, $orderTotal, $orderDate, $hasReview, $refunded] = $row;
                // if (!empty($refunded) || empty($orderTotal)) {
                //     echo "Invalid Row" . PHP_EOL;
                //     var_dump($row);
                //     continue;
                // }
                $buyer = OwnedBuyer::where("nickname", trim($nickName))->first();
                if (empty($buyer)) {
                    if ($email) {
                        $buyer = OwnedBuyer::where("email", trim($email))->first();
                    }
                    if (empty($buyer) && $phone) {
                        $phone = preg_replace('/\D/', '', trim($phone));
                        $buyer = OwnedBuyer::where("phone", $phone)->first();
                    }
                    if (empty($buyer)) {
                        echo "buyer not found for row" . PHP_EOL;
                        var_dump($row);
                        continue;
                    }
                }
                $order = new ManagedReviewOrder();
                if (empty(trim($orderNumber))) {
                    $orderNumber = "123-1234567-1234567";
                    $order->amazon_order_id = $orderNumber;
                } else {
                    $order = ManagedReviewOrder::firstOrNew(['amazon_order_id' => trim($orderNumber)]);
                }
                if (!empty($refunded)) {
                    $order->status = -1; //refunded
                }
                $order->owned_buyer_id = $buyer->id;
                $order->order_cost = $orderTotal;
                $order->order_date = empty($orderDate) ? "2000-01-01" : $orderDate;
                if ($hasReview) {
                    $order->review_title = "NOT KNOWN, BUT HAS TITLE";
                    $order->review_content = "NOT KNOWN, BUT HAS CONTENT";
                    $order->review_link = "NOT KNOWN, BUT HAS LINK";
                    $order->left_review_at = Carbon::parse($order->order_date)->addDays(15)->format('Y-m-d H:i:s');
                    $order->left_review_by = $buyer->created_by;
                }
                $order->save();
            }
            $rowsCount++;
        }
    }

    public static function syncSelfOrders() {
        $notSyncedOrders = self::where('order_type', self::OWNED_BUYER_SELF_ORDER)->whereNull('asin')->get();
        foreach ( $notSyncedOrders as $order) {
            $order->fillInfo();
        }
    }

    public static function syncSelfOrdersWithOrderItem() {
        $notSyncedOrders = self::where('order_type', self::OWNED_BUYER_SELF_ORDER)->whereNull('asin')->get();
        foreach ($notSyncedOrders as $order) {
            $orderItem = OrderItem::where('AmazonOrderId', $order->amazon_order_id)->first();
            if ($orderItem) {
                $order->seller_id = $orderItem->seller_id;
                $order->asin = $orderItem->ASIN;
                $order->save();
            }
        }
        
    }

    // delete duplicated orders placed on the same day with same amount by the same buyer.
    public static function deleteDuplicatedOrders() {
        while (true) {
            $duplicatedOrders = ManagedReviewOrder::select('id', 'owned_buyer_id', 'order_date', 'order_cost', DB::raw('count("id")'))
                                                ->groupBy('owned_buyer_id', 'order_date', 'order_cost')
                                                ->havingRaw('count("id") > ?', [1])
                                                ->get();
            if (count($duplicatedOrders) > 0) {
                foreach ($duplicatedOrders as $order) {
                    $order->delete();
                }
            } else {
                break;
            }
        }
        
    }

    public static function getSelfReviewStatistics($fromDate, $toDate) {
        $result = [];

        $selfReviews = self::select('managed_review_orders.*', 'order_items.country')
            ->join('order_items', 'managed_review_orders.amazon_order_id', '=', 'order_items.AmazonOrderId')
            ->where([
                ['order_date', '>=', $fromDate],
                ['order_date', '<=', $toDate]
            ])->get();
        $selfReviewsByProduct = DataHelper::groupByVal($selfReviews, function ($selfReview) {
            return sprintf('%s-%s', $selfReview->country, $selfReview->asin);
        });
        foreach ($selfReviewsByProduct as $k => $selfReviewsGroup) {
            $result[$k] = [
                'selfReviewCnt' => count($selfReviewsGroup),
                'reviewedSelfReviewCnt' => 0
            ];
        }

        $fromDatetime = \Carbon\Carbon::createFromFormat(
            'Y-m-d', $fromDate, config('app.timezone'))->startOfDay()->format('Y-m-d H:i:s');
        $toDatetime = \Carbon\Carbon::createFromFormat(
            'Y-m-d', $toDate, config('app.timezone'))->endOfDay()->format('Y-m-d H:i:s');
        $selfReviews = self::select('managed_review_orders.*', 'order_items.country')
            ->join('order_items', 'managed_review_orders.amazon_order_id', '=', 'order_items.AmazonOrderId')
            ->where([
                ['left_review_at', '>=', $fromDatetime],
                ['left_review_at', '<=', $toDatetime]
            ])->get();
        $selfReviewsByProduct = DataHelper::groupByVal($selfReviews, function ($selfReview) {
            return sprintf('%s-%s', $selfReview->country, $selfReview->asin);
        });
        foreach ($selfReviewsByProduct as $k => $selfReviewsGroup) {
            if (array_key_exists($k, $result)) {
                $result[$k]['reviewedSelfReviewCnt'] = count($selfReviewsGroup);
            } else {
                $result[$k] = [
                    'selfReviewCnt' => 0,
                    'reviewedSelfReviewCnt' => count($selfReviewsGroup),
                ];
            }
        }

        return $result;
    }

    public static function getDefaultSelfReviewStatistic() {
        return [
            'selfReviewCnt' => 0,
            'reviewedSelfReviewCnt' => 0
        ];
    }

    public static function boot()
    {
        parent::boot();

        self::created(function($model) {
            $model->fillInfo();
            
            if (empty($model->asin) && $model->order_type == self::OWNED_BUYER_SELF_ORDER) {
                $ownedBuyer = $model->ownedBuyer;
                $ownedBuyer->not_synced_order_id = $model->amazon_order_id;
                $ownedBuyer->save();
            }
            if ($ownedBuyer = $model->ownedBuyer) {
                $ownedBuyer->updateOrderStatistics();
            }
            $cartItems = OwnedBuyerCartItem::where("owned_buyer_id", $model->owned_buyer_id)->get();
            $cartItems->map(function ($cartItem) {
                $cartItem->delete();
            });
        });

        self::saving(function($model) {
            if (empty($model->need_review)) {
                $model->need_review = false;
            }
            if ($model->isDirty('review_title')) {
                if (empty($model->review_title)) {
                    $model->left_review_at = null;
                } else {
                    if (empty($model->left_review_at)) {
                        $model->left_review_at = date('Y-m-d H:i:s');
                    } 
                }
            }
            if (empty($model->amazon_order_id)) {
                $model->amazon_order_id = "123-1234567-1234567";
            }
            if ($model->isDirty('review_link') && $model->review_link && empty($model->left_review_at)) {
                $model->left_review_at = date('Y-m-d H:i:s');
            }
            if (empty($model->review_link) && empty($model->review_title) && empty($model->review_content)) {
                $model->left_review_at = null;
            }
        });

        self::saved(function($model) {
            if (empty($model->getOriginal('asin')) && $model->asin) {
                $ownedBuyer = $model->ownedBuyer;
                if ($ownedBuyer->not_synced_order_id == $model->amazon_order_id) {
                    $ownedBuyer->not_synced_order_id = null;
                    $ownedBuyer->save();
                }
            }
            if ($model->owned_buyer_id && $model->owned_buyer_id > 0) {
                $ownedBuyer = $model->ownedBuyer;
                if ($model->isDirty('owned_buyer_id'))  {
                    if (empty($ownedBuyer->last_placed_order_at) || $ownedBuyer->last_placed_order_at < $model->order_date) {
                        $ownedBuyer->last_placed_order_at = $model->order_date;
                    }
                    $shipment = $model->orderShipment;
                    if ($shipment) {
                        if ($ownedBuyer->last_placed_order_at < $shipment->purchase_date) {
                            $ownedBuyer->last_placed_order_at = $shipment->purchase_date;
                        }
                    }
                    $ownedBuyer->save();
                }
                if ($model->left_review_at || $model->getOriginal('left_review_at')) {
                    $original = $model->getOriginal();
                    $leftReviewAt = empty($original['left_review_at']) ? $model->left_review_at : $original['left_review_at'];
                    $leaveReviewTask = LeaveReviewTask::where("asin", $model->asin)
                                    ->where("created_at", "<=", $leftReviewAt)
                                    ->where("deadline", ">=", $leftReviewAt)
                                    ->first();
                    if ($leaveReviewTask) {
                        $leaveReviewTask->updateOwnedBuyerReview();
                    }
                }

                if ($model->isDirty('review_link')) {
                    $ownedBuyer->updateReviewInfo();
                }

                if ($model->isDirty('order_cost') && $model->status != -1) {
                    $originalOrderCost = empty($model->getOriginal('order_cost')) ? 0 : $model->getOriginal('order_cost');
                    $ownedBuyer = $model->ownedBuyer;
                    $payment = $ownedBuyer->paymentMethods->first();
                    if ($payment) {
                        $payment->total_spent = $payment->total_spent - ($originalOrderCost - $model->order_cost);
                        $payment->available_balance = $payment->available_balance + ($originalOrderCost - $model->order_cost);
                        $payment->save();
                    }
                }
            }
        });

        self::deleting(function($model) {
            
        });

        self::deleted(function($model) {
            if ($model->owned_buyer_id && $model->owned_buyer_id > 0) {
                $ownedBuyer = $model->ownedBuyer;
                if ($ownedBuyer) {
                    $ownedBuyer->updateReviewInfo();
                }
            }
            if ($ownedBuyer = $model->ownedBuyer) {
                if ($ownedBuyer->not_synced_order_id == $model->amazon_order_id) {
                    $ownedBuyer->not_synced_order_id = null;
                    $ownedBuyer->save();
                }
                $ownedBuyer->updateOrderStatistics();
                
                $payment = $ownedBuyer->paymentMethods->first();
                if ($payment) {
                    $payment->total_spent = $payment->total_spent - $model->order_cost;
                    $payment->available_balance = $payment->available_balance + $model->order_cost;
                    $payment->save();
                }
            }
            if ($model->isDirty('left_review_at')) {
                $original = $model->getOriginal();
                $leaveReviewTask = LeaveReviewTask::where("asin", $original['asin'])
                                    ->where("created_at", "<=", $original['left_review_at'])
                                    ->where("deadline", ">=", $original['left_review_at'])
                                    ->first();
                if ($leaveReviewTask) {
                    $leaveReviewTask->updateOwnedBuyerReview();
                }
            }
        });
    }
}
