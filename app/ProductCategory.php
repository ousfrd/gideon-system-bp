<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    //
    protected $table = 'product_categories';
    protected $fillable = ['id', 'category'];

    public function products()
    {
        return $this->hasMany('App\Product', 'category_id');
    }

    function productList() {
    	$list = [];
    	foreach ($this->products as $p) {
    		$list[] = $p->asin;
    	}
    	
    	return $list;
    }

    public static function saveCategory($category){
        $category = ProductCategory::create(['category' => $category]);
    }

    public static function updateCategory($id, $new_category){
        $category = ProductCategory::find($id);
        $category->category = $new_category;
        $category->save();
    }

    public static function deleteCategory($id){
        $category = ProductCategory::find($id);
        $category->delete();
    }

    public static function findByName($category){
        return self::where('category',$category)->firstOrFail();
    }
}
