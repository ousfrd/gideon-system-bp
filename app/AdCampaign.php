<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use App\Traits\MassInsertOrUpdate;
use AmazonAdvertisingApi\Client;

class AdCampaign extends Model
{
	use MassInsertOrUpdate;
	
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'ad_campaigns';
	
	public function account(){
		return $this->belongsTo('App\Account','seller_id','seller_id');
	}

	public function ad_groups() {
		return $this->hasMany('App\AdGroup','campaignId','campaignId');
	}

	public function ad_campaigns_reports() {
		return $this->hasMany('App\AdCampaignsReport','campaignId','campaignId');
	}
	
	public function ad_reports() {
		return $this->hasMany('App\AdReport','campaignId','campaignId');
	}

	public static function get_daily_data($params) {
		$data = [];

		// if select adGroup, query ad_groups_reports table
		if(isset($params['adGroupId']) && $params['adGroupId']) {	

			$query = AdGroup::join('ad_groups_reports', 'ad_groups.adGroupId', '=', 'ad_groups_reports.adGroupId');

			$query->select(DB::raw('date, SUM(attributedSales1d) - SUM(attributedSales1d_diff) as total_sales, 
	                     	SUM(clicks) - SUM(clicks_diff) as total_clicks, 
	                     	SUM(impressions) - SUM(impressions_diff) as total_impressions, 
	                     	SUM(cost * exchange_rate) - SUM(cost_diff * exchange_rate) as total_cost,
	                     	SUM(attributedConversions1d) - SUM(attributedConversions1d_diff) as total_orders'));

			if($params['seller_id'] && $params['seller_id'] != 'all') {
				$query = $query->where('ad_groups.seller_id', $params['seller_id']);
			}

			if($params['country'] && $params['country'] != 'all') {
				$query->where('ad_groups.country', $params['country']);
			}

			if(isset($params['start_date']) && isset($params['end_date'])) {	
				$query->whereBetween('date', [$params['start_date'], $params['end_date']]);
			}

			if(isset($params['adGroupId']) && $params['adGroupId']) {	
				$query->where('ad_groups.adGroupId', $params['adGroupId']);
			}
			
			$query_data = $query->groupBy('date')->orderBy('date', 'asc')->get();


		} else {

			$query = AdCampaign::join('ad_campaigns_reports', 'ad_campaigns.campaignId', '=', 'ad_campaigns_reports.campaignId');

			$query->select(DB::raw('date, SUM(attributedSales1d) - SUM(attributedSales1d_diff) as total_sales, 
	                     	SUM(clicks) - SUM(clicks_diff) as total_clicks, 
	                     	SUM(impressions) - SUM(impressions_diff) as total_impressions, 
	                     	SUM(cost * exchange_rate) - SUM(cost_diff * exchange_rate) as total_cost,
	                     	SUM(attributedConversions1d) - SUM(attributedConversions1d_diff) as total_orders'));

			if($params['seller_id'] && $params['seller_id'] != 'all') {
				$query = $query->where('ad_campaigns.seller_id', $params['seller_id']);
			}

			if($params['country'] && $params['country'] != 'all') {
				$query->where('ad_campaigns.country', $params['country']);
			}

			if(isset($params['start_date']) && isset($params['end_date'])) {	
				$query->whereBetween('date', [$params['start_date'], $params['end_date']]);
			}

			if(isset($params['query']) && $params['query']) {	
				$query->where('ad_campaigns.name', 'like', "%".$params['query']."%");
			}

			if(isset($params['campaignIds']) && $params['campaignIds']) {	
				$query->whereIn('ad_campaigns.campaignId', json_decode($params['campaignIds']));
			}


			$query_data = $query->groupBy('date')->orderBy('date', 'asc')->get();


		}


			foreach ($query_data as $key => $value) {

				$data[$value->date]['total_sales'] = $value->total_sales;
				$data[$value->date]['total_clicks'] = $value->total_clicks;
				$data[$value->date]['total_impressions'] = $value->total_impressions;
				$data[$value->date]['total_cost'] = round($value->total_cost,2);
				$data[$value->date]['total_orders'] = $value->total_orders;
				$data[$value->date]['acos'] = (isset($value->total_sales) && $value->total_sales > 0) ? round(($value->total_cost / $value->total_sales) * 100, 2) : "0.0";
				$data[$value->date]['cpc'] = (isset($value->total_clicks) && $value->total_clicks > 0) ? round($value->total_cost / $value->total_clicks, 2) : 0;
				$data[$value->date]['ctr'] = (isset($value->total_impressions) && $value->total_impressions > 0) ? round(($value->total_clicks / $value->total_impressions) * 100, 2) : 0;
				$data[$value->date]['cr'] = (isset($value->total_clicks) && $value->total_clicks > 0) ? round(($value->total_orders / $value->total_clicks) * 100, 2) : 0;
			}

			return $data;

	}

	public static function getCampaigns($params) {

		$dataProvider = AdCampaign::join('ad_campaigns_reports', 'ad_campaigns.campaignId', '=', 'ad_campaigns_reports.campaignId');

		$campaignManagers = DB::table('product_ads')->leftJoin('seller_listings', function($join)
			              {
                             $join->on('product_ads.sku', '=', 'seller_listings.sku');
                             $join->on('product_ads.country', '=', 'seller_listings.country');
                         })->leftJoin('product_users', function($join)
			             {
                            $join->on('seller_listings.product_id', '=', 'product_users.product_id');
                         })->leftJoin('users', function($join)
			              {
                             $join->on('product_users.user_id', '=', 'users.id');
                         })->select('product_ads.campaignId', 'users.name')->groupBy('campaignId')->get();

        $managers = [];
        foreach($campaignManagers as $manager) {
        	$managers[$manager->campaignId] = $manager->name;
        }


		if(isset($params['seller_id']) && $params['seller_id'] && $params['seller_id'] != 'all') {
			$dataProvider->where('ad_campaigns.seller_id', $params['seller_id']);
		}

		if(isset($params['country']) && $params['country'] && $params['country'] != 'all') {	
			$dataProvider->where('ad_campaigns.country', $params['country']);
		}

		if(isset($params['start_date']) && isset($params['end_date'])) {	
			$dataProvider->whereBetween('ad_campaigns_reports.date', [$params['start_date'], $params['end_date']]);
		}

		if(isset($params['query']) && $params['query']) {	
			$dataProvider->where('ad_campaigns.name', 'like', "%".$params['query']."%");
		}

		if(isset($params['campaignId']) && $params['campaignId']) {	
			$dataProvider->where('ad_campaigns.campaignId', $params['campaignId']);
		}

		if(isset($params['campaignIds']) && $params['campaignIds']) {	
			if(count(json_decode($params['campaignIds'])) == 1) {
				$dataProvider->where('ad_campaigns.campaignId', json_decode($params['campaignIds'])[0]);
			} elseif(count(json_decode($params['campaignIds'])) > 1) {
				$dataProvider->whereIn('ad_campaigns.campaignId', json_decode($params['campaignIds']));
			}
		}

		if(isset($params['campaignState']) && $params['campaignState'] && $params['campaignState'] != 'all') {	
			$dataProvider->where('ad_campaigns.state', $params['campaignState']);
		}

		if(isset($params['campaignTargetingType']) && $params['campaignTargetingType'] && $params['campaignTargetingType'] != 'all') {	
			$dataProvider->where('ad_campaigns.targetingType', $params['campaignTargetingType']);
		}

		$dataProvider->select(DB::raw('ad_campaigns.*, ad_campaigns_reports.campaignId, SUM(ad_campaigns_reports.clicks) - SUM(ad_campaigns_reports.clicks_diff) as total_clicks, SUM(ad_campaigns_reports.impressions) - SUM(ad_campaigns_reports.impressions_diff) as total_impressions, ROUND(SUM(ad_campaigns_reports.cost * ad_campaigns_reports.exchange_rate) - SUM(ad_campaigns_reports.cost_diff * ad_campaigns_reports.exchange_rate), 2) as total_cost, SUM(ad_campaigns_reports.attributedSales1d) - SUM(ad_campaigns_reports.attributedSales1d_diff) as total_sales, SUM(ad_campaigns_reports.attributedConversions1d) - SUM(ad_campaigns_reports.attributedConversions1d_diff) as total_orders'));

		$dataProvider->groupBy('ad_campaigns_reports.campaignId');


		$data = $dataProvider->get();
		$seller_accounts = Account::all();
		foreach($seller_accounts as $account) {
			$accounts[$account->seller_id] = [$account->name, $account->code];
		}

		$servingStatus = [
        	'CAMPAIGN_STATUS_ENABLED'=> 'Delivering', 
        	'CAMPAIGN_PAUSED'=> 'Paused', 
        	'CAMPAIGN_OUT_OF_BUDGET'=> 'Out of budget',
        	'CAMPAIGN_ARCHIVED'=> 'Archived',
        	'ENDED' => 'Ended',
        	'ADVERTISER_PAYMENT_FAILURE' => 'Advertiser Payment Failure',
    	];
		foreach ($data as $key => $value) {
			$value->dailyBudget = (float)$value->dailyBudget;
			$value->total_clicks = (float)$value->total_clicks;
			$value->total_impressions = (float)$value->total_impressions;
			$value->total_cost = (float)$value->total_cost;
			$value->total_sales = (float)$value->total_sales;
			$value->total_orders = (float)$value->total_orders;
			
			$value->account = $accounts[$value->seller_id][0] . " (" . $accounts[$value->seller_id][1] . ")";
			$value->acos = (isset($value->total_sales) && $value->total_sales > 0) ? round(($value->total_cost / $value->total_sales) * 100, 2) : "0.0";
			$value->cpc = (isset($value->total_clicks) && $value->total_clicks > 0) ? round($value->total_cost / $value->total_clicks, 2) : 0;
			$value->ctr = (isset($value->total_impressions) && $value->total_impressions > 0) ? round(($value->total_clicks / $value->total_impressions) * 100, 2) : 0;
			$value->cr = (isset($value->total_clicks) && $value->total_clicks > 0) ? round(($value->total_orders / $value->total_clicks) * 100, 2) : 0;
			$value->start_date = date("d/m/Y", ($value->creationDate / 1000));

			$value->product_manager = isset($managers[$value->campaignId]) ? $managers[$value->campaignId] : '';
			$value->status = $servingStatus[$value->servingStatus];
		}


		return $data;

	}

	public static function getCampaignsTotal($params) {

		$dataProvider = AdCampaign::join('ad_campaigns_reports', 'ad_campaigns.campaignId', '=', 'ad_campaigns_reports.campaignId');


		if(isset($params['seller_id']) && $params['seller_id'] && $params['seller_id'] != 'all') {
			$dataProvider->where('ad_campaigns.seller_id', $params['seller_id']);
		}

		if(isset($params['country']) && $params['country'] && $params['country'] != 'all') {	
			$dataProvider->where('ad_campaigns.country', $params['country']);
		}

		if(isset($params['start_date']) && isset($params['end_date'])) {	
			$dataProvider->whereBetween('ad_campaigns_reports.date', [$params['start_date'], $params['end_date']]);
		}

		if(isset($params['campaignId']) && $params['campaignId']) {	
			$dataProvider->where('ad_campaigns.campaignId', $params['campaignId']);
		}

		if(isset($params['campaignIds']) && $params['campaignIds']) {	
			if(count(json_decode($params['campaignIds'])) == 1) {
				$dataProvider->where('ad_campaigns.campaignId', json_decode($params['campaignIds'])[0]);
			} elseif(count(json_decode($params['campaignIds'])) > 1) {
				$dataProvider->whereIn('ad_campaigns.campaignId', json_decode($params['campaignIds']));
			}
		}

		$dataProvider->select(DB::raw('SUM(ad_campaigns_reports.clicks) - SUM(ad_campaigns_reports.clicks_diff) as total_clicks, SUM(ad_campaigns_reports.impressions) - SUM(ad_campaigns_reports.impressions_diff) as total_impressions, ROUND(SUM(ad_campaigns_reports.cost * ad_campaigns_reports.exchange_rate) - SUM(ad_campaigns_reports.cost_diff * ad_campaigns_reports.exchange_rate), 2) as total_cost, SUM(ad_campaigns_reports.attributedSales1d) - SUM(ad_campaigns_reports.attributedSales1d_diff) as total_sales, SUM(ad_campaigns_reports.attributedConversions1d) - SUM(ad_campaigns_reports.attributedConversions1d_diff) as total_orders'));

		$data = $dataProvider->get();

		foreach ($data as $key => $value) {
			$value->acos = (isset($value->total_sales) && $value->total_sales > 0) ? round(($value->total_cost / $value->total_sales) * 100, 2) : "0.0";
			$value->cpc = (isset($value->total_clicks) && $value->total_clicks > 0) ? round($value->total_cost / $value->total_clicks, 2) : 0;
			$value->ctr = (isset($value->total_impressions) && $value->total_impressions > 0) ? round(($value->total_clicks / $value->total_impressions) * 100, 2) : 0;
			$value->cr = (isset($value->total_clicks) && $value->total_clicks > 0) ? round(($value->total_orders / $value->total_clicks) * 100, 2) : 0;
		}

		$budgetQuery = "SELECT SUM(ad_campaigns.dailyBudget) as dailyBudget FROM ad_campaigns WHERE campaignId IN (SELECT DISTINCT campaignId FROM ad_campaigns_reports WHERE ad_campaigns_reports.date BETWEEN '" . $params['start_date'] . "' AND '" . $params['end_date'] . "')";

		if(isset($params['seller_id']) && $params['seller_id'] && $params['seller_id'] != 'all') {
			$budgetQuery .= " AND ad_campaigns.seller_id = '" . $params['seller_id'] . "'";
		}
		if(isset($params['country']) && $params['country'] && $params['country'] != 'all') {	
			$budgetQuery .= " AND ad_campaigns.country = '" . $params['country'] . "'";
		}
		if(isset($params['campaignIds']) && $params['campaignIds']) {	
			if(count(json_decode($params['campaignIds'])) == 1) {
				$budgetQuery .= " AND ad_campaigns.campaignId = '" . json_decode($params['campaignIds'])[0] . "'";
			} elseif(count(json_decode($params['campaignIds'])) > 1) {
				$budgetQuery .= " AND ad_campaigns.campaignId IN (" . implode(",", json_decode($params['campaignIds'])) . ")";
			}
		}

		$result = DB::select(DB::raw($budgetQuery));

		$data[0]->dailyBudget = $result[0]->dailyBudget;

		return $data;

	}

	public function initClient($account) {

		$refreshToken = $account->adv_refresh_token;
		if($account->region == "US") {
			$region = "na";
		} elseif ($account->region == "UK") {
			$region = "eu";
		} else {
			return;
		}

		$config = array(
		    "clientId" => config('app.lwa_client_id'),
		    "clientSecret" => config('app.lwa_client_secret'),
		    "refreshToken" => $refreshToken,
		    "region" => $region,
		    "sandbox" => false,
	  	);
		
		$client = new Client($config);

		return $client;
	}	

	public function updateCampaignStateAction($campaignId, $adData){
		
		$campaign = AdCampaign::where('campaignId', $campaignId)->first();

		$account = $campaign->account;

		if(empty($account->adv_refresh_token)) {
			session()->flash('msg', 'Account did not register Amazon Advertising API.');
			return;	
		}

		$client = $this->initClient($account);


		$client->doRefreshToken();
		
		if(!empty($client)) {

			if(empty($account->adv_profile_ids)) {
				$account->updateProfiles($client);
			}
			
			// if not request in url then get first from profiles 
			$profile_ids = json_decode($account->adv_profile_ids);
			$country = $campaign->country ? $campaign->country : $profile_ids[0]->countryCode;
			$profileId = (string)$account->getProfileByCountry($country)->profileId;
			$client->profileId = $profileId;
			$response = $client->updateCampaigns($adData);
					

		}


		if($response['success']) {// if response if sucess

			//echo $campaign->state = $new_state;

			if($adData[0]['state'] == "enabled"){
			   $servingStatus = "CAMPAIGN_STATUS_ENABLED";
			}else if($adData[0]['state'] == "paused"){
			   $servingStatus = "CAMPAIGN_PAUSED";
			}else{
				
			}
			$campaign->servingStatus = $servingStatus;
			echo $campaign->state = $adData[0]['state'];

		    $campaign->save();
	   }	

		return $response;	

	}

	public static function getCampaignsByRule($rule){

		//check conditions
		$conditions = json_decode($rule->conditions,true);
		$rule_query = DB::table("ad_campaigns");


		$rule_query->join("ad_reports", "ad_reports.campaignId", "=", "ad_campaigns.campaignId");
		if($conditions['targetingType']) { $rule_query->where('targetingType', $conditions['targetingType']);}
		if($conditions['seller_id']) { $rule_query->where('ad_campaigns.seller_id', $conditions['seller_id']);}
		if($conditions['state']) { $rule_query->where('state', $conditions['state']);}                
		if($conditions['containedword']) { $rule_query->where('name', 'like', "%".$conditions['containedword']."%");}
		if($conditions['asin']) { $rule_query->where('ad_reports.asin', $conditions['asin'] );}

		$rule_query->where('ad_campaigns.auto_scheduler', "enabled" );

		$rule_query->groupBy("ad_campaigns.campaignId");

		$campaigns = $rule_query->get();// Test

		return $campaigns;

	}

}
	