<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
class Inventory extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'product_inventory';

	public $timestamps = false;
	
	function product() {
		return $this->belongsTo('App\Product','asin','asin')->where('country', $this->country);
	}
	
	public static function boot()
  {
		parent::boot();
		self::saved(function($model) {
			if ($model->isDirty('total_quantity')) {
				if ($model->product) {
					$model->product->calculateInventoryTotalQty();
				}
			}
		});
	}
	
}

