<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\ClaimReviewCampaign;

class GiftCardTemplate extends Model
{
    protected $fillable = ['asin', 'amount', 'version', 'front_design', 'back_design', 'name', 'note'];

    protected $appends = array('front_design_url', 'back_design_url', 'attachment1_url', 'attachment2_url');

    public static $templateTypes = [
        "post_card" => "Post Card",
        "letter" => "Letter"
    ];
    
    public function getFrontDesignUrlAttribute()
    {
        if ($this->front_design) {
            return GiftCardTemplate::getUrl($this->front_design);
        } else {
            return "/images/noImageUploaded.png";
        }
    }

    public function getBackDesignUrlAttribute()
    {
        if ($this->back_design) { 
            return GiftCardTemplate::getUrl($this->back_design);
        } else {
            return "/images/noImageUploaded.png";
        }
    }

    public function getAttachment1UrlAttribute()
    {
        return GiftCardTemplate::getUrl($this->attachment1);
    }

    public function getAttachment2UrlAttribute()
    {
        return GiftCardTemplate::getUrl($this->attachment2);
    }

    public function getAttachment1PathAttribute()
    {
        $fileName = $this->attachment1;
        if (empty($fileName)) {
            return NULL;
        }

        self::downloadToLocal($fileName);
        if (Storage::disk('local')->exists($fileName)) {
            return Storage::disk('local')->path($fileName);
        }

        return NULL;
    }

    public function getAttachment2PathAttribute()
    {
        $fileName = $this->attachment2;
        if (empty($fileName)) {
            return NULL;
        }

        self::downloadToLocal($fileName);
        if (Storage::disk('local')->exists($fileName)) {
            return Storage::disk('local')->path($fileName);
        }

        return NULL;
    }

    public static function boot()
    {
        parent::boot();

        self::deleting(function($model) {
            ClaimReviewCampaign::where("gift_card_template_id", $model->id)->update(['gift_card_template_id' => null]);
        });

        self::deleted(function($model) {
            Storage::disk(config('giftcard.storage'))->delete($model->front_design);
            Storage::disk(config('giftcard.storage'))->delete($model->back_design);
        });
    }

    public static function getUrl($filename) {
        return Storage::disk(config('giftcard.storage'))->url($filename);
    }

    public static function getGiftCardStorage() {
        return config('giftcard.storage');
    }

    public static function isLocal() {
        return self::getGiftCardStorage() == 'local';
    }

    public static function getLocalDirPrefix() {
        return 'giftcard';
    }

    public static function downloadToLocal($fileName) {
        if (self::isLocal()) {
            return TRUE;
        }

        $localDisk = Storage::disk('local');
        $giftcardStorage = Storage::disk(self::getGiftCardStorage());
        $downloadRequired = (!$localDisk->exists($fileName) || 
            $localDisk->lastModified($fileName) < $giftcardStorage->lastModified($fileName));
        if ($downloadRequired) {
            $localDisk->put($fileName, $giftcardStorage->get($fileName));
        }

        return $localDisk->exists($fileName);
    }
}
