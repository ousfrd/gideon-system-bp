<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;

class BusinessReport extends Model
{
    //
    protected $table = 'business_reports';

    public $timestamps = false;

    protected $guarded = ['id'];

    function merchant(){
        return $this->belongsTo('App\Account','seller_id','seller_id');
    }

}
