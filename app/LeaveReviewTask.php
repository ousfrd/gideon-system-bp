<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ManagedReviewOrder;
use App\LeaveReviewSubTask;

class LeaveReviewTask extends Model
{
    protected $fillable = ['name', 'asin', 'total_review_goal', 'owned_buyer_review_goal', 'invite_review_goal', 'deadline'];

    public function subTasks() {
        return $this->hasMany('App\LeaveReviewSubTask');
    }

    public function ownedBuyerReviewsQuery() {
        $asin = $this->asin;
        $createdAt = $this->created_at->toDateTimeString();
        $deadline = $this->deadline;
        return ManagedReviewOrder::where('asin', $asin)
                                    ->where('order_type', ManagedReviewOrder::OWNED_BUYER_SELF_ORDER)
                                    ->whereBetween('left_review_at', [$createdAt, $deadline]);
    }

    public function ownedBuyerReviews() {
        return $this->ownedBuyerReviewsQuery()->get();
    }

    public function updateOwnedBuyerReview() {
        $ownedBuyerReviews = $this->ownedBuyerReviews();
        $reviewsByUserId = collect($ownedBuyerReviews->toArray())->groupBy('left_review_by')->toArray();
        foreach ($this->subTasks as $subTask) {
            $progress = array_key_exists($subTask->user_id, $reviewsByUserId) ? count($reviewsByUserId[$subTask->user_id]) : 0;
            $subTask->progress = $progress;
            $subTask->save();
        }
        $this->owned_buyer_approved_reviews_count = count($ownedBuyerReviews);
        $this->save();
    }

    public function fillSubTaskJson() {
        $subTasks = $this->subTasks;
        if ($subTasks && count($subTasks) > 0) {
            $subTasksJson = $this->subTasks->toJson();
            $this->sub_tasks_json = $subTasksJson;
        }
    }

    public static function gridQuery() {
        $query = self::with('subTasks')->orderBy('id', 'desc');
        return $query;
    }

    public static function boot()
    {
        parent::boot();

        self::saving(function($model) {
            $model->fillSubTaskJson();
        });
    }

    
}
