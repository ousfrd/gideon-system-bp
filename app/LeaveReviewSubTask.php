<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveReviewSubTask extends Model
{
    protected $fillable = ['leave_review_task_id', 'user_id', 'goal'];
}
