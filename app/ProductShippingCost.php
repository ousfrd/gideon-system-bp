<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use Carbon\Carbon;

class ProductShippingCost extends Model
{
    protected $fillable = ['product_id', 'start_date'];

    protected $appends = array('future', 'link');
    
    public function product() {
        return $this->belongsTo('App\Product');
    }

    public function getFutureAttribute() {
        if (Carbon::parse($this->start_date)->greaterThan(Carbon::today())) {
            return true;
        } else {
            return false;
        }
        
    }

    public function getLinkAttribute() {
        return route('product-shipping-cost.destroy', $this);
    }

    public static function fillInitialData() {
        $products = Product::all();
        foreach ($products as $product) {
            $costs = $product->productShippingCosts;
            if ($costs->isEmpty()) {
                $cost = new ProductShippingCost();
                $cost->product_id = $product->id;
                $cost->cost_per_unit = $product->shipping_cost;
                $cost->start_date = '2010-01-01';
                $cost->save();
            }
        } 
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function($model) {
            $costs = self::where('product_id', $model->product_id);
            $costs->update(['end_date' => $model->start_date]);
        });

        self::saved(function($model) {
            $product = $model->product;
            $product->shipping_cost = $model->cost_per_unit;
            $product->save();
        });

        self::deleting(function($model) {
            $cost = self::where('id', '!=', $model->id)->where('product_id', $model->product_id)->orderBy('start_date', 'desc')->first();
            if ($cost) {
                $cost->end_date = null;
                $cost->save();
            } 
        });
    }
}
