<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesRankUpdate extends Model
{
    //
    protected $table = 'sales_ranks';
    protected $fillable = ['asin','sales_ranks','date','rating', 'country', 'category'];
    public $timestamps = false;

    static function insertSalesRank($asin, $sales_ranks, $date, $rating) {
        $sales_rank = SalesRankUpdate::insert(['asin' => $asin, 'sales_ranks' => $sales_ranks, 'date' => $date, 'rating' => $rating]);
    }
    
    static function updateSalesRank($asin, $sales_ranks, $date, $rating) {
        $sales_rank = SalesRankUpdate::where(['asin' => $asin, 'date' => $date])->update(['sales_ranks' => $sales_ranks, 'rating' => $rating]);
    }
    
    static function addSalesRank($asin, $country, $category, $salesRank, $date = null) {
        if (empty($date)) { $date = date('Y-m-d'); }
        $sales_rank = SalesRankUpdate::firstOrNew(
            [
                'asin' => $asin,
                'country' => $country,
                'date' => $date,
                'category' => $category,
            ]
        );
        $sales_rank->sales_rank = $salesRank;
        $sales_rank->save();
    }
}
