<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use GuzzleHttp\Client as GuzzleClient;
class Stannp extends Model
{
    //
    // private static $stannp_api = env('STANNP_API');
    private static $stannp_api = null;

    public static function getBalance() {
        $stannp_api = env('STANNP_API');
        if (empty($stannp_api)) {
            throw new Exception('Stannp Api is empty'); 
        }
        $http = new GuzzleClient();
        $url = "https://us.stannp.com/api/v1/recipients/validate?api_key=".$stannp_api;
        $response = $http->get($url);
        $res = json_decode( $response->getBody(), true);
        return $res["data"]["balance"];
    }

    public static function addressValidate($shipment) {
        $stannp_api = env('STANNP_API');
        $url = "https://us.stannp.com/api/v1/recipients/validate?api_key=".$stannp_api;
        $data = array(
            'address1' => $shipment->ship_address_1,
            'address2' => $shipment->ship_address_2,
            'city' => $shipment->ship_city,
            'state' => $shipment->ship_state,
            'zipcode' => $shipment->ship_postal_code,
            'country' => $shipment->ship_country
        );

        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return $result;
    }

    public static function createCampaign($name, $type) {
        $stannp_api = env('STANNP_API');
        $url = "https://us.stannp.com/api/v1/campaigns/draft?api_key=".$stannp_api;
        $data = array(
            'name' => $name,
            'type' => $type,
            'what_recipients' => "valid"
        );

        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return $result["data"];
    }

    public static function getCampaignCost($id) {
        $stannp_api = env('STANNP_API');
        $url = "https://us.stannp.com/api/v1/campaigns/cost/?api_key=".$stannp_api;
        $data = array(
            'id' => $id
        );

        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return $result;
    }

    public static function send($claimReviewCampaign, $testMode = false) {
        $stannp_api = env('STANNP_API');
        $claimReviewCampaign->is_sending = true;
        $claimReviewCampaign->save();
        
        $template = $claimReviewCampaign->giftCardTemplate;
    
        if ($template && $template->attachment1) {
            $shipments = $claimReviewCampaign->getClickReviewSendOrderShipments()->get();
            $post_recipients = [];

            $claimReviewCampaign->balance_before = self::getBalance();
            if ($template->template_type == "letter") {
                $new_campaign_id = self::createCampaign("");
            }
            $counter = 0;
            $orderItemMarketingsArray = [];
            foreach ($shipments as $shipment) {
                $oim = $shipment->order_item_marketing;
                if ($oim->is_sent) {
                    continue;
                }
                if (!self::addressValidate($shipment)) {
                    continue;
                }
                $postdata = http_build_query(
                    array(
                        'group_id' => "1",
                        'on_duplicate' => "update" ,
                        'firstname' => "Steve",
                        'lastname' => "Parish",
                        'address1' => $shipment->ship_address_1,
                        'address2' => $shipment->ship_address_2,
                        'city' => $shipment->ship_city,
                        'state' => $shipment->ship_state,
                        'zipcode' => $shipment->ship_postal_code,
                        'country' => $shipment->ship_country
                    )
                );
                $opts = array('http' => array(
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/x-www-form-urlencoded',
                    'content' => $postdata
                )
                );
                $context  = stream_context_create($opts);
                $result = file_get_contents("https://us.stannp.com/api/v1/recipients/new?api_key=".$stannp_api, false, $context);
                $response = json_decode($result,true);
                $orderItemMarketingsArray[] = $oim;
            }
            $orderItemMarketingsCollection = collect($orderItemMarketingsArray);

            if ($response['http_code'] >= 200 && $response['http_code'] < 300) {
            $recipients = $response['data']['recipients'];
            $claimReviewCampaign->actual_sent_amount = $counter;
 
            $claimReviewCampaign->balance_after = $accountGetResult["data"]["balance"];
            $price = ((float)$claimReviewCampaign->balance_before - (float)$claimReviewCampaign->balance_after) / $claimReviewCampaign->actual_sent_amount;
            $claimReviewCampaign->unit_price = $price;
            $claimReviewCampaign->total_cost = (float)$claimReviewCampaign->balance_before - (float)$claimReviewCampaign->balance_after;
            $claimReviewCampaign->sent_at = date('Y-m-d H:i:s');
            $claimReviewCampaign->save();
            foreach ($recipients as $recipient) {
                $oim = $orderItemMarketingsCollection->first(function ($value, $key) use($recipient) {
                return $value->click_send_scheduled_send_at == $recipient['schedule'];
                });
                $oim->is_sent = true;
                $oim->click_send_response = json_encode($recipient);
                $oim->save();
            }
            
            } else {
                throw new Exception($result);
            }
        }
    }
}
