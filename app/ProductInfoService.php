<?php

namespace App;

use Illuminate\Support\Facades\Log;
use AmazonProductLoader\ProductDetailTaskSender;
use AmazonProductLoader\ProductCategoriesTaskSender;
use AmazonProductLoader\ProductLoader;
use AmazonProductLoader\ReviewTaskSender;
use AmazonProductLoader\ProductDetailWebTaskSender;
use App\Product;
use App\Review;
use App\SalesRankUpdate;

class ProductInfoService {

  /**
   * @params $asin array(string)
   */
  static function requestInfo($asins, $marketplace) {
    $config = self::_getConfig($marketplace);
    $sender = new ProductDetailTaskSender(
                        $config['broker_host'], 
                        $config['broker_user'], 
                        $config['broker_password'], 
                        $config['broker_vhost'], 
                        $config['broker_exchange'] , 
                        $config['broker_routing_key']
                      );
    $sender->sendTask($asins, $marketplace);
  }

  /**
   * @params $marketplace ['us', 'ca', 'uk', ...]
   */
  static function getInfo($asins, $marketplace) {
    $config = self::_getConfig($marketplace);
    $loader = new ProductLoader(
                  $config['product_service_host'], 
                  $config['product_service_port'], 
                  $config['product_service_user'], 
                  $config['product_service_password']
                );
    $products = $loader->getProducts($asins, $marketplace);
    return $products;
  }

  static function requestCategory($asins, $marketplace) {
    $config = self::_getConfig($marketplace);
    $sender = new ProductCategoriesTaskSender(
                    $config['broker_host'], 
                    $config['broker_user'], 
                    $config['broker_password'], 
                    $config['broker_vhost'], 
                    $config['broker_exchange_category'], 
                    $config['broker_routing_key_category']
                  );
    foreach ($asins as $asin) {
      $sender->sendTask($asin, $marketplace);
    }
  }

  static function getCategory($asins, $marketplace) {
    $config = self::_getConfig($marketplace);
    $loader = new ProductLoader(
      $config['product_service_host'], 
      $config['product_service_port'], 
      $config['product_service_user'], 
      $config['product_service_password']
    );
    $productCategories = $loader->getProductCategories($asins, $marketplace);
    return $productCategories;
  }

  static function requestCategoryForAllProducts() {
    
  }

  public static function requestRating($asins, $marketplace) {
    $ratingConfig = self::_getRatingConfig();

    $sender = new ProductDetailWebTaskSender(
      $ratingConfig['host'], $ratingConfig['port'],
      $ratingConfig['password'], $ratingConfig['database']);

    $sender->sendTask($asins, $marketplace);
  }

  public static function requestRatingForAllProducts() {
    $products = Product::select('id', 'asin', 'country')->where([
      ['status', '>=', 0],
      ['discontinued', '<=', 0]
    ])->get();
    $countryProducts = $products->groupBy('country');
    $countryProducts->each(function ($products, $country) {
      $marketplace = strtolower($country);
      $asins = $products->map(function($item, $index) {
        return $item->asin;
      })->all();

      self::requestRating($asins, $marketplace);
    });
  }

  public static function getRatingForAllProducts() {
    $products = Product::select('id', 'asin', 'country')->where([
      ['status', '>=', 0],
      ['discontinued', '<=', 0]
    ])->get();
    $countryProducts = $products->groupBy('country');
    $countryProducts->each(function ($products, $country) {
      $marketplace = strtolower($country);
      $asins = $products->map(function($item, $index) {
        return $item->asin;
      })->all();
      $productOverviews = self::getRatings($asins, $marketplace);
      foreach ($productOverviews as $asin => $productOverview) {
        if (empty($productOverview['review'])) {
          continue;
        }

        $review = $productOverview['review'];

        if (isset($productOverview['time'])) {
          $date = date('Y-m-d', strtotime($productOverview['time']));
        } else {
          $date = date('Y-m-d');
        }

        $rating = Rating::firstOrNew([
          'country' => $country,
          'asin' => $asin,
          'date' => $date
        ]);
        if (empty($rating)) {
          $rating = new Rating();
        }

        $record = [
          'country' => $country,
          'asin' => $asin,
          'date' => $date,
          'rating_count' => $review['review_count'] ?? 0,
          'rating' => $review['review_star'] ?? 0,
        ];

        $proportion = $review['proportion'] ?? [];
        foreach ($proportion as $item) {
          $k = sprintf('star%s_rating', $item['star']);
          $record[$k] = $item['proportion'];
        }
        $rating->fill($record);

        $rating->save();

        $product = Product::where([
          ['country', '=', $country],
          ['asin', '=', $asin]
        ])->first();
        if ($product) {
          $product->review_quantity = $rating->rating_count;
          $product->review_rating = $rating->rating;
          $product->save();
        }
      }
    });
  }

  public static function getRatings($asins, $marketplace) {
    $config = self::_getConfig($marketplace);
    $loader = new ProductLoader(
      $config['product_service_host'], 
      $config['product_service_port'], 
      $config['product_service_user'], 
      $config['product_service_password']
    );

    return $loader->getProductOverview($asins, $marketplace);
  }

  public static function requestReview($asins, $marketplace) {
    $ratingConfig = self::_getRatingConfig();

    $sender = new ReviewTaskSender(
      $ratingConfig['host'], $ratingConfig['port'],
      $ratingConfig['password'], $ratingConfig['database']);

    $sender->sendTask($asins, $marketplace);
  }

  public static function requestReviewForAllProducts() {
    $products = Product::select('id', 'asin', 'country')->where([
      ['status', '>=', 0],
      ['discontinued', '<=', 0]
    ])->get();
    $countryProducts = $products->groupBy('country');
    $countryProducts->each(function ($products, $country) {
      $marketplace = strtolower($country);
      $asins = $products->map(function($item, $index) {
        return $item->asin;
      })->all();

      self::requestReview($asins, $marketplace);
    });
  }

  public static function getReviewForAllProducts() {
    $config = self::_getConfig($marketplace);
    $loader = new ProductLoader(
      $config['product_service_host'], 
      $config['product_service_port'], 
      $config['product_service_user'], 
      $config['product_service_password']
    );

    $products = Product::select('id', 'asin', 'country')->where([
      ['status', '>=', 0],
      ['discontinued', '<=', 0]
    ])->get();
    foreach ($products as $product) {
      $asin = $product->asin;
      $reviews = $loader->getProductReviews([$asin]);
      $reviews = $reviews[$asin] ?? [];
      $indexedReviews = [];
      foreach ($reviews as $review) {
        $indexedReviews[$review['review_id']] = $review;
      }

      $unexistingReviews = array_merge([], $indexedReviews);
      $amazonReviewIds = array_keys($indexedReviews);
      $existingReviews = Review::whereIn('review_id', $amazonReviewIds)->get();
      foreach ($existingReviews as $review) {
        unset($unexistingReviews[$review['review_id']]);

        $reviewNew = $indexedReviews[$review['review_id']];
        if ($reviewNew['review_rating'] == $review->review_rating) {
          continue;
        }

        $review->review_rating = $reviewNew['review_rating'];
        $review->save();
      }

      if (count($unexistingReviews) <= 0) {
        continue;
      }

      foreach (array_chunk($unexistingReviews, 100) as $reviewsGroup) {
        $reviewsInfo = array_map(function ($review) {
          return [
            'review_id' => $review['review_id'],
            'country' => $review['country'],
            'asin' => $review['asin'],
            'review_date' => date('Y-m-d', strtotime($review['review_date'])),
            'profile_name' => $review['profile_name'],
            'review_title' => $review['review_title'],
            'review_text' => $review['review_text'],
            'review_rating' => $review['review_rating'],
            'review_link' => $review['review_link'],
            'verified_purchase' => $review['verified_purchase'],
            'review_exist' => 1,
            'image' => $review['image'] ? 'YES' : 'NO',
            'video' => $review['video'] ? 'YES' : 'NO'
          ];
        }, $reviewsGroup);
        DB::table('reviews')->insert($reviewsInfo);
      }
    }
  }

  static function requestInfoForAllProducts() {
    $products = Product::select('id', 'asin', 'country')->where('status', '>=', 0)->get();
    $countryProducts = $products->groupBy('country');
    $countryProducts->each(function ($products, $country) {
      $marketplace = strtolower($country);
      $asins = $products->map(function($item, $index) {
        return $item->asin;
      })->all();
      ProductInfoService::requestInfo($asins, $marketplace);
    });
  }

  static function getInfoForAllProducts() {
    $products = Product::select('id', 'asin', 'country')->where('status', '>=', 0)->get();
    $countryProducts = $products->groupBy('country');
    $countryProducts->each(function ($products, $country) {
      $marketplace = strtolower($country);
      $allAsins = $products->map(function($item, $index) {
        return $item->asin;
      })->all();
      $asinProducts = $products->keyBy('asin');
      $asinsArray = array_chunk($allAsins, 10);
      foreach ($asinsArray as $asins) {
        $infos = ProductInfoService::getInfo($asins, $marketplace);
        foreach ($infos as $asin => $info) {
          $product = $asinProducts[$asin];
          self::_updateProduct($product, $info);
        }
      }
    });
  }

  private static function _updateProduct($product, $response) {
    if ($response && $response['matching_product']) {
      $matchingProduct = json_decode($response['matching_product']);
      $attributes = $matchingProduct->ItemAttributes;
      $salesRankings = $matchingProduct->SalesRankings;
      if ($attributes) {
        self::_updateProductAttributes($product, $attributes);
      }
      if ($salesRankings) {
        self::_updateSalesRank($product, $salesRankings);
      }
    }
  }

  private static function _updateProductAttributes($product, $attributes) {
    $product->brand = $attributes->Brand;
    if ($attributes->SmallImage) { $product->small_image = $attributes->SmallImage->URL; }
    if ($attributes->PackageDimensions) {
      $weight = $attributes->PackageDimensions->Weight;
      $height = $attributes->PackageDimensions->Height;
      $length = $attributes->PackageDimensions->Length;
      $width = $attributes->PackageDimensions->Width;

      if ($weight){ $product->weight = $weight->value; }
      if ($height){ $product->height = $height->value; }
      if ($length){ $product->length = $length->value; }
      if ($width) { $product->width = $width->value;}
    }
    $product->save();
  }

  private static function _updateSalesRank($product, $salesRankings) {
    $asin = $product->asin;
    $country = strtoupper($product->country);
    $date = date('Y-m-d');
    $salesRankingChoosen = FALSE;
    foreach( $salesRankings as $salesRanking ) {
      if ($salesRanking) {
        $category = $salesRanking->ProductCategoryId;
        $rank = $salesRanking->Rank;
        SalesRankUpdate::addSalesRank($asin, $country, $category, $rank, $date);

        if (preg_match('/[a-zA-Z]+/' , $category) && !$salesRankingChoosen) {
          $product->sales_rank = $rank;
          $product->sales_rank_date = $date;
          $product->save();

          $salesRankingChoosen = TRUE;
        }
      }
    }
  }

  private static function _getConfig($marketplace) {
    $config = [
      "broker_host" => env('BROKER_HOST', 'localhost'),
      "broker_user" => env('BROKER_USER', 'guest'),
      "broker_password" => env('BROKER_PASSWORD', 'guest'),
      "broker_vhost" => env('BROKER_VHOST', '/'),
      "broker_exchange" => env('BROKER_EXCHANGE_PREFIX', '') . strtoupper($marketplace),
      "broker_routing_key" => env('BROKER_ROUTING_KEY_PREFIX', '') . strtoupper($marketplace),
      "broker_exchange_category" => env('BROKER_EXCHANGE_CATEGORIES_PREFIX', '') . strtoupper($marketplace),
      "broker_routing_key_category" => env('BROKER_ROUTING_KEY_CATEGORIES_PREFIX', '') . strtoupper($marketplace),
      "product_service_host" => env('PRODUCT_SERVICE_HOST'),
      "product_service_port" => env('PRODUCT_SERVICE_PORT'),
      "product_service_user" => env('PRODUCT_SERVICE_USER'),
      "product_service_password" => env('PRODUCT_SERVICE_PASSWORD')
    ];
    return $config;
  }

  public static function _getRatingConfig() {
    return [
      'host' => env('SCRAPER_HOST', 'localhost'),
      'port' => env('SCRAPER_PORT', 6379),
      'password' => env('SCRAPER_PASSWORD', ''),
      'database' => env('SCRAPER_DATABASE', 0)
    ];
  }

  
}