<?php

namespace App;

use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use Illuminate\Support\Facades\Auth;
use Shipment;
use App\Brand;
use App\Order;
use App\OrderItem;
use App\ProductStatistic;
use App\Customer;
use App\SalesRankUpdate;
use App\Helpers\DataHelper;

class Listing extends Model
{

	/**
	 * qty = active_inventory->fulfillable_quantity; Inventory Report afn-fulfillable-quantity field
	 */

	 /**
		 * listing status: there is status field in amazon all listing report file.
		 * 1: Active (listing report file)
		 * 0: Inactive (listing report file)
		 * -1: Removed (no longer appears in listing report file)
		 * -2: exists in listing report, but not either Active nor Inactive
	  */


	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'seller_listings';
	
	//protected $fillable = ['asin','code','country','keywords','title','product_group','old_asin','type'];
	public $timestamps = false;
	
	protected static $amzLinks = [
			'US'=>'https://www.amazon.com/',
			'CA'=>'https://www.amazon.ca/',
			'MX'=>'https://www.amazon.com.mx/',
			'UK'=>'https://www.amazon.co.uk/',
			'DE'=>'https://www.amazon.de/',
			'ES'=>'https://www.amazon.es/',
			'FR'=>'https://www.amazon.fr/',
			'IT'=>'https://www.amazon.it/',
			'JP'=>'https://www.amazon.co.jp/',
	];
	
	
// 	function users() {
// 		return $this->belongsToMany('App\User', 'seller_listing_users',  'listing_id','user_id');
// 	}
	static function updateInsertCardStatus($sku, $insert_card) {
		$listing = Listing::where('sku', $sku)->update(['insert_card' => $insert_card]);
        return $listing;
	}

	
	function inventory() {
		return $this->hasOne('App\Inventory','listing_id','id');
	}

	function active_inventory() {
		return $this->hasOne('App\Inventory','listing_id','id')->where('item_condition', 'New')->where('status',1);
	}

	function product() {
		return $this->belongsTo('App\Product','product_id','id');
	}
	
	function merchant(){
		return $this->belongsTo('App\Account','seller_id','seller_id');
	}
	
	function orderItems() {
		return $this->hasMany('App\OrderItem','SellerSku','sku');
	}
	
	function expenses() {
		return $this->hasMany('App\ListingExpense','listing_id','id');
	}
	
	function inbound_shipments() {
		return $this->hasMany('App\InboundShipment','seller_sku','sku')->where('shipment_status','<>','DELETED')->orderBy('date_updated','desc');
	}
	
	function health_report() {
		return $this->hasOne('App\ListingHealthReport','listing_id','id')->orderBy('snapshot_date','desc');
	}
	
	function amzLink() {
		return self::$amzLinks[strtoupper($this->country)].'dp/'.$this->asin;
	}
	
	function ad_report($start_date, $end_date) {
		return AdReport::reportsDateRange($start_date, $end_date, ['sku'=>$this->sku]);
	}
// 	A2梳子：假设目前已有库存2000，日均销量110，工厂生产周期30天
// 	总补货周期=30+14+5=49
// 	安全库存＝日均销量＊（总的补货周期＋5）
// 	安全库存=110*（49+5）=5940

// 	非高峰期备货采购量＝日均销量＊备货周期-已有库存（程序读取amazon总库存+采购录入已经在生产和在运输途中的库存）
// 	高峰期备货采购量=日均销量＊备货周期*3（采购可以手动录入数字）
// 	最佳采购日期＝今日日期＋库存还能支撑天数－总补货周期
	
	function totalQty() {
		return $this->total_qty?$this->total_qty:$this->qty;
	}
	
	function getManufactureDays() {
		return $this->peak_season_mode? $this->ps_manufacture_days:$this->manufacture_days;
	}
	
	function getShippingDays() {
		return $this->peak_season_mode? $this->ps_cn_to_amz_shipping_days:$this->cn_to_amz_shipping_days;
	}
	
	/**
	 * 总补货周期＝深圳提交工厂采购到收到货的总时长（采购手动录入）＋CN到Amazon仓库运输时间（采购手动录入）（默认14天，特别产品视具体情况而定）＋5
	 * @return number
	 */
	function totalDayToReinstock(){
		return $this->getManufactureDays()+ $this->getShippingDays() + 5;
	}
	
	/**
	 * 日均销量=7天日均销量*70%+14天日均销量*20%+21天日均销量*10%
	 * @return number
	 */
	function dailyAverageOrders() {
		return $this->daily_ave_orders;
		// return  round($this->last_7_days_orders *0.7/7 + $this->last_14_days_orders *0.2/14+ $this->last_21_days_orders *0.1/21,1);
	}
	
	/**
	 * 安全库存＝日均销量＊（总的补货周期＋5）
	 * @return number
	 */
	function safeStockQty(){
		return ceil($this->dailyAverageOrders() * ($this->totalDayToReinstock() + 5));
	}
	
	
	
	
	function replenishNeeded() {
		return $this->safeStockQty() > $this->totalQty();
	}
	
	/**
	 * 非高峰期备货采购量＝日均销量＊备货周期-已有库存（程序读取amazon总库存+采购录入已经在生产和在运输途中的库存）
	 * 高峰期备货采购量=日均销量＊备货周期*3（采购可以手动录入数字）
	 * @return number
	 */
	function recommendedReplenishmentQty() {
		
		if ($this->peak_season_mode){
			$qty = $this->dailyAverageOrders() * $this->totalDayToReinstock() * 3;
		} else {
			$qty = $this->dailyAverageOrders() * $this->totalDayToReinstock() - $this->totalQty();
		}
		
		return ceil($qty);
		
	}
	
	/**
	 * 最佳采购日期＝今日日期＋库存还能支撑天数－总补货周期
	 * @return string
	 */
	function recommendedReplenishmentDate() {
		
		if($this->daysToReplenish() === 'N/A') {
			return 'N/A';
		}
		
		if($this->daysToReplenish() > 0) {
			return date('Y-m-d',strtotime('+ '.($this->daysToOutofStock()-$this->totalDayToReinstock()).' days'));
		}
		else{
			return 'TODAY';
		}
	}
	/**
	 * 库存还能支撑天数
	 * @return string|mixed
	 */
	function daysToOutofStock() {
		if ($this->dailyAverageOrders() == 0) {
			return 'N/A';
		}
		//var_dump($this->total_qty);
		return max(0,ceil(($this->totalQty()) / $this->dailyAverageOrders()));
	}
	
	/**
	 * 库存还能支撑天数－总补货周期
	 * @return string|mixed
	 */
	function daysToReplenish() {
		if ($this->dailyAverageOrders() == 0) {
			return 'N/A';
		}
		return max(0,ceil(($this->totalQty() - $this->safeStockQty()) / $this->dailyAverageOrders()));
	}
	
	static function lowOrOutOfStockingCount() {
		$query = self::join('products','products.id','seller_listings.product_id')
		->join('product_inventory','seller_listings.id','product_inventory.listing_id')
		->where('product_inventory.id','>',0)
		->where('seller_listings.fulfillment_channel','Amazon')
		->whereRaw(DB::Raw('seller_listings.total_qty/(seller_listings.daily_ave_orders) - (seller_listings.manufacture_days+seller_listings.cn_to_amz_shipping_days+5) <= 7'))
		->where('products.status',1)
		->where('products.discontinued',0);
		
		
		
		if(Auth::user()->role == "product manager") {
			$query->whereIn('seller_listings.asin',Auth::user()->productList());
		}elseif(auth()->user()->role == "seller account manager") {
			$query->whereIn('seller_listings.seller_id',auth()->user()->accountList());
		}
		
		$count = $query->count();
		
		return $count;
	}
	
	function orderReports($start_date,$end_date) {
		
		return Order::orderReportsByDate($start_date, $end_date,['sku'=>$this->sku,'country'=>$this->country]);
		
		//return ['totals'=>$totals,'counts'=>$counts,'total_orders'=>$total_orders,'total_gross'=>$total_gross];
		
		$timezoneOffset = \DT::timezoneUTCOffset();
		
		$query = OrderItem::with(['order'])
		->where('SellerSku',$this->sku)
		->where('country',$this->country)
		->join('orders', 'orders.AmazonOrderId', '=', 'order_items.AmazonOrderId')
		
		->whereBetween('PurchaseDate',[\DT::convertToUTC(date('Y-m-d 00:00:00',strtotime($start_date))),\DT::convertToUTC(date('Y-m-d 23:59:59',strtotime($end_date)))]);
		
		$query->whereIn('OrderStatus',['Shipped','Unshipped','Pending']);
		
		
		//var_dump([\DT::convertToUTC(date('Y-m-d 00:00:00',strtotime($start_date))),\DT::convertToUTC(date('Y-m-d 00:00:00',strtotime($end_date)+24*60*60))]);
		
		if($start_date == $end_date) {
			$query->select(DB::raw('(HOUR(PurchaseDate)+'.$timezoneOffset.') as date'), DB::raw('count(*) as total'));
		} else {
			$query->select(DB::raw('DATE(DATE_SUB(PurchaseDate,INTERVAL -'.$timezoneOffset.' HOUR)) as date'), DB::raw('count(*) as total'));
		}
		
// 		echo $query->toSql();
		$counts = $query
		->groupBy('date')
		->pluck('total','date')->toArray();
		
		
		if($start_date == $end_date) {
			$query->select(DB::raw('(HOUR(PurchaseDate)+'.$timezoneOffset.') as date'), DB::raw('sum(ItemPrice) as total'));
		} else {
			$query->select(DB::raw('DATE(DATE_SUB(PurchaseDate,INTERVAL -'.$timezoneOffset.' HOUR)) as date'), DB::raw('sum(ItemPrice) as total'));
		}
		
		$totals = $query->groupBy('date')->pluck('total','date')->toArray();
		
		$total_gross = 0;
		$total_orders = 0;
		
		
		if($start_date == $end_date) {
			for ($hour = 0; $hour < date('H'); $hour++) {
				if(!isset($counts[$hour]) || empty($counts[$hour])) {
					$counts[$hour] = 0;
				}else{
					$counts[$hour] = round($counts[$hour],2);
				}
				if(!isset($totals[$hour]) || empty($totals[$hour]) ) {
					$totals[$hour] = 0;
				}else{
					$totals[$hour] = round($totals[$hour],2);
				}
				
				$total_gross += $totals[$hour];
				$total_orders += $counts[$hour];
			}
		} else {
			for($date = $start_date;$date <= $end_date;$date = date("Y-m-d",strtotime("$date +1 day"))) {
				if(!isset($counts[$date]) || empty($counts[$date])) {
					$counts[$date] = 0;
				}else{
					$counts[$date] = round($counts[$date],2);
				}
				if(!isset($totals[$date]) || empty($totals[$date]) ) {
					$totals[$date] = 0;
				}else{
					$totals[$date] = round($totals[$date],2);
				}
				
				$total_gross += $totals[$date];
				$total_orders += $counts[$date];
			}
		}
		ksort($counts);
		ksort($totals);
		
		return ['totals'=>$totals,'counts'=>$counts,'total_orders'=>$total_orders,'total_gross'=>$total_gross];
	}
	
	function inventoryHistory($start_date,$end_date) {
		$query = DailyInventoryReport::where('sku',$this->sku)->where('country',$this->country)
		->whereBetween('date',[date('Y-m-d 00:00:00',strtotime($start_date)),date('Y-m-d 23:59:59',strtotime($end_date))]);
		
		$cols = [ 
				'date',
				DB::raw ( 'sum(fulfillable_quantity) as fulfillable_quantity' ),
				DB::raw ( 'sum(total_quantity) as total_quantity' ),
				DB::raw ( 'sum(inbound_working_quantity) as inbound_working_quantity '),
				DB::raw ( 'sum(inbound_shipped_quantity) as inbound_shipped_quantity' ),
				DB::raw ( 'sum(inbound_receiving_quantity) as inbound_receiving_quantity' ),
				DB::raw ( 'sum(reserved_quantity) as reserved_quantity' ),
				DB::raw ( 'sum(unsellable_quantity) as unsellable_quantity' )
		];
		$query->select($cols)->groupBy('date')->orderBy('date','asc');

		$rows = $query->get()->toArray();
		$totals = [];
		foreach ($rows as $row) {
			$totals[$row['date']] = $row;
		}
		
		for($date= $start_date;$date <= $end_date;$date = date("Y-m-d",strtotime("$date +1 day"))) {
			if(!isset($totals[$date]) || empty($totals[$date]) ) {
				$totals[$date] = ['fulfillable_quantity'=>0,
						'total_quantity'=>0,
						'inbound_working_quantity'=>0,
						'inbound_shipped_quantity'=>0,
						'inbound_receiving_quantity'=>0,
						'reserved_quantity'=>0,
						'unsellable_quantity'=>0
				];
			}
		}
		
		ksort($totals);
		
		return $totals;
		// 		return ['totals'=>$totals,'counts'=>$counts,'total_orders'=>$total_orders,'total_gross'=>$total_gross];
	}

	function calculateDailyAveOrders() {
		$sku = $this->sku;
		$orderItems = OrderItem::select("AmazonOrderId", "QuantityOrdered")->where("SellerSKU", $sku)->get();
		if (count($orderItems) == 0) { return 0; }
		$amazonOrderIds = [];
		$quantityShipped = 0;
		foreach($orderItems as $orderItem) {
			$amazonOrderIds[] = $orderItem->AmazonOrderId;
			$quantityShipped += $orderItem->QuantityOrdered;
		}
		$minDate = Order::where("OrderStatus", "!=", "Cancelled")->whereIn("AmazonOrderId", $amazonOrderIds)->min("PurchaseDate");
		if (!$minDate) { return 0; }
		$days = floor((time() - strtotime($minDate))/(24*3600));
		$dailyAveOrders = $quantityShipped / $days;
		return $dailyAveOrders;
	}

	static function calculateDailyAveOrdersForAll() {
		$listings = Listing::all();
		foreach($listings as $listing) {
			$aveOrders = $listing->calculateDailyAveOrders();
			$listing->daily_ave_orders = $aveOrders;
			$listing->save();
		}
		Log::info("Listing::updateLifecalculateDailyAveOrdersForAlltimeStatisticsForAll run at : " . date("Y-m-d H:i:s"));
	}

	function getOrdersQuantityTillNow($daysAgo) {
		$time = strtotime("-{$daysAgo} days");
		$dateComponents = getdate($time);
		$newTime = mktime(0, 0, 0, $dateComponents["mon"], $dateComponents["mday"], $dateComponents['year']);
		$date = date('Y-m-d H:i:s', $newTime);
		$query = OrderItem::join( 'orders', function($join) {
			$join->on('orders.AmazonOrderId', '=', 'order_items.AmazonOrderId');
		});
		$query->where('SellerSKU', $this->sku);
		$query->where('orders.OrderStatus', 'Shipped');
		$query->where('orders.PurchaseDate', '>=', $date);
		$orders = $query->sum('QuantityOrdered');
		return $orders ? $orders : 0;
	}

	static function calculateOrdersQuantityForAll() {
		$listings = Listing::all();
		$fields = [
			'last_7_days_orders' => 7,
			'last_14_days_orders' => 14,
			'last_21_days_orders' => 21,
			'last_30_days_orders' => 30,
			'today_so_far_orders' => 0,
			'this_month_orders' => getdate(time())['mday']
		];
		foreach ( $listings as $listing ) {
			foreach ($fields as $field => $daysAgo) {
				$ordersQuantity = $listing->getOrdersQuantityTillNow($daysAgo);
				$listing->$field = $ordersQuantity;
			}
			$listing->save();
		}
		Log::info("Listing::calculateOrdersQuantityForAll run at : " . date("Y-m-d H:i:s"));
	}

	function updateSalesPrice() {
		$orderShipment = Shipment::where("sku", $this->sku)->orderBy('id', 'desc')->first();
		if ( $orderShipment ) {
			$salesPrice = $orderShipment->item_price / $orderShipment->quantity_shipped;
			$this->sale_price = $salesPrice * $orderShipment->exchange_rate;
			$this->save();
		}
	}

	static function UpdateSalesPriceForAll(){
		$listings = Listing::select("id", "sku", "price", "price_usd")->get();
		foreach ($listings as $listing) {
			$listing->updateSalesPrice();
		}
	}

	public static function boot()
    {
        parent::boot();

        self::saving(function($model) {
					if (!$model->ai_ads_open) {
						$model->ai_ads_commission_rate = 0;
					}
					if ($model->ai_ads_commission_rate) {
						$model->ai_ads_commission_rate = floatval($model->ai_ads_commission_rate);
					}
					$model->updated_at = date("Y-m-d H:i:s");
				});

				self::saved(function($model) {
					$product = $model->product;
					if ($product) {
						$listings = $product->listings;
						if ($listings) {
							$warehouse_qty = 0;
							foreach ($listings as $listing) {
								if ($listing->status == -1 or $listing->merchant->status != 'Active') {
									continue;
								}
								$warehouse_qty += $listing->afn_warehouse_quantity;
							}
							$product->warehouse_qty = $warehouse_qty;
							$product->save();
						}
					}
				});
		}

	public function markRemoved() {
		$this->status = -1;
		$this->save();
	}

	public function getInsertCardReviewCost() {
		return $this->insert_card_review_cost;
	}

	public function remove() {
		$sellerId = $this->seller_id;
		$country = $this->country;
        $asin = $this->asin;
        $sku = $this->sku;

        // Delete Orders
        $orderItems = OrderItem::where([
            ['seller_id', '=', $sellerId],
            ['ASIN', '=', $asin],
            ['SellerSKU', '=', $sku]
        ])->get();
        $amazonOrderIds = $orderItems->pluck('AmazonOrderId')->unique()->values()->all();
        if ($amazonOrderIds) {
            $message = sprintf(
                '[DeleteOrders] %s', implode(', ', $amazonOrderIds));
            Log::info($message);
        }

        foreach ($orderItems as $orderItem) {
            $orderItem->delete();
        }

        foreach ($amazonOrderIds as $amazonOrderId) {
            $order = Order::where('AmazonOrderId', $amazonOrderId)->first();
            if ($order) {
                $order->delete();
            }
        }

        // Delete product statistics
        ProductStatistic::where('listing_id', $this->id)->delete();

        // Delete sales ranks
        SalesRankUpdate::where([
        	['asin', '=', $asin],
        	['country', '=', $country]
        ])->delete();

        // Delete product and brand
        if ($this->product_id) {
            $product = Product::find($this->product_id);
            if ($product) {
                $brandName = $product->brand;
                $brand = Brand::where('name', $brandName)->first();
                if ($brand) {
                    $message = sprintf(
                        '[DeleteBrand] %s', $brandName);
                    Log::info($message);

                    $brand->delete();
                }

                $name = empty($product->alias) ? $product->name : $product->alias;
                $message = sprintf('[DeleteProduct] %s', $name);
                Log::info($message);

                $product->delete();
            }
        }

        $message = sprintf(
            '[DeleteListing] SellerID: %s, ASIN: %s, SKU: %s',
            $sellerId, $asin, $sku
        );
        Log::info($message);

        $this->delete();
	}

	public function getOrders($fromDate, $toDate = NULL, $includeShipments = TRUE, $includeMarketing = TRUE) {
		$startDate = \Carbon\Carbon::createFromFormat(
			'Y-m-d', $fromDate, config('app.timezone'))->startOfDay()->setTimezone('UTC');
		$fromDate = $startDate->format('Y-m-d H:i:s');
		$queryCond = [
			['order_items.ASIN', '=', $this->asin],
			['order_items.SellerSKU', '=', $this->sku],
			['orders.PurchaseDate', '>=', $fromDate]
		];
		if ($toDate) {
			$endDate = \Carbon\Carbon::createFromFormat(
				'Y-m-d', $toDate, config('app.timezone'))->endOfDay()->setTimezone('UTC');
			$toDate = $endDate->format('Y-m-d H:i:s');
			$queryCond[] = ['orders.PurchaseDate', '<=', $toDate];
		}

		// $fields = [
		// 	'orders.id as order_id', 'order_items.id as order_item_id',
		// 	'orders.seller_id', 'orders.AmazonOrderId', 'order_items.ASIN',
		// 	'order_items.SellerSKU', 'orders.OrderStatus', 'orders.PurchaseDate',
		// 	'orders.SalesChannel', 'orders.FulfillmentChannel',
		// 	'order_shipments.id as order_shipment_id', 'order_shipments.sku',
		// 	'order_shipments.buyer_email', 'order_shipments.buyer_name',
		// 	'order_shipments.recipient_name', 'order_shipments.ship_address_1',
		// 	'order_shipments.ship_address_2', 'order_shipments.ship_city',
		// 	'order_shipments.ship_state', 'order_shipments.ship_postal_code',
		// 	'order_shipments.ship_country',  'order_shipments.shipment_date',
		// 	'order_item_marketings.id as order_item_marketing_id',
		// 	'order_item_marketings.processed', 'order_item_marketings.is_sent',
		// 	'order_item_marketings.claim_review_id',
		// 	'order_item_marketings.invite_review',
		// 	'order_item_marketings.self_review',
		// 	'order_item_marketings.email_promotion_review'
		// ];
		$fields = [
			'orders.id as order_id', 'order_items.id as order_item_id',
			'orders.seller_id', 'orders.AmazonOrderId', 'order_items.ASIN',
			'order_items.SellerSKU', 'orders.OrderStatus', 'orders.PurchaseDate',
			'orders.SalesChannel', 'orders.FulfillmentChannel',
		];
		$query = Order::select($fields)->where($queryCond)
			->join('order_items', 'order_items.AmazonOrderId', '=', 'orders.AmazonOrderId');

		$withCond = [];
		if ($includeShipments) {
			$withCond[] = 'shipment';
			// $query = $query->leftJoin(
			// 	'order_shipments', 'orders.AmazonOrderId',
			// 	'=', 'order_shipments.amazon_order_id');
		}

		if ($includeMarketing) {
			$withCond[] = 'orderItemMarketing';
			// $query = $query->leftJoin(
			// 	'order_item_marketings', 'orders.AmazonOrderId',
			// 	'=', 'order_item_marketings.oim_amazon_order_id'
			// );
		}

		if ($withCond) {
			$query = $query->with($withCond);
		}

		return $query->get();
	}

	public function buildClaimReviewStatistics($fromDate, $toDate = NULL) {
		$claimReviewStatistics = [];

		$blockListedCustomers = Customer::select('amazon_buyer_email')
			->where('blocklisted', TRUE)->get()->pluck('amazon_buyer_email')->all();
		$orders = $this->getOrders($fromDate, $toDate);
		$groupedOrders = DataHelper::groupByDate($orders, 'PurchaseDate', config('app.timezone'));
		foreach ($groupedOrders as $date => $ordersByDate) {
			$orderCount = count($ordersByDate);

			$shippedOrders = [];
			$pendingOrderCount = 0;
			$returnedOrderCount = 0;
			$partialReturnedOrderCount = 0;
			$shippingOrderCount = 0;
			$unshippedOrderCount = 0;
			foreach ($ordersByDate as $order) {
				$status = $order->OrderStatus;
				if ($status == 'Pending') {
					$pendingOrderCount += 1;
				} else if ($status == 'Returned') {
					$returnedOrderCount += 1;
				} else if ($status == 'Partial Returned') {
					$partialReturnedOrderCount += 1;
				} else if ($status == 'Shipping') {
					$shippingOrderCount += 1;
				} else if ($status == 'Unshipped') {
					$unshippedOrderCount += 1;
				}

				if (strtolower($status) == 'shipped') {
					$shippedOrders[$order->AmazonOrderId] = $order;
				}
			}
			$shippedOrderCount = count($shippedOrders);

			if ($this->insert_card > 0) {
				$innerPageOrderCount = $orderCount;
				$missingShipmentCount = 0;
				$toClaimReviewCount = 0;
				$claimReviewSentCount = 0;
				$claimReviewToSendCount = 0;
			} else {
				$innerPageOrderCount = 0;
				$missingShipmentCount = 0;
				$toClaimReviewCount = 0;
				$claimReviewSentCount = 0;
				$claimReviewToSendCount = 0;

				foreach ($shippedOrders as $amazonOrderId => $order) {
					if (empty($order->shipment)) {
						$missingShipmentCount += 1;
					}

					$shouldClaimReview = strtoupper(substr($amazonOrderId, 0, 1)) != "S";
					$shouldClaimReview = $order->FulfillmentChannel == 'AFN' && $shouldClaimReview;
					if ($order->shipment && in_array($order->shipment->buyer_email, $blockListedCustomers)) {
						$shouldClaimReview = FALSE;
					}
					if ($order->orderItemMarketing) {
						if ($order->orderItemMarketing->self_review) {
							$shouldClaimReview = FALSE;
						}
					}

					if (!$shouldClaimReview) {
						continue;
					}

					$toClaimReviewCount += 1;

					if ($order->orderItemMarketing) {
						if ($order->orderItemMarketing->is_sent) {
							$claimReviewSentCount += 1;
						} else {
							$claimReviewToSendCount += 1;
						}
					} else {
						$claimReviewToSendCount += 1;
					}
				}
			}

			if ($claimReviewToSendCount > 0) {
				$claimReviewToSendStartDate = $date;
			}

			$claimReviewStatistics[$date] = (object) [
				'orderCount' => $orderCount,
				'pendingOrderCount' => $pendingOrderCount,
				'shippedOrderCount' => $shippedOrderCount,
				'returnedOrderCount' => $returnedOrderCount,
				'partialReturnedOrderCount' => $partialReturnedOrderCount,
				'shippingOrderCount' => $shippingOrderCount,
				'unshippedOrderCount' => $unshippedOrderCount,
				'innerPageOrderCount' => $innerPageOrderCount,
				'missingShipmentCount' => $missingShipmentCount,
				'toClaimReviewCount' => $toClaimReviewCount,
				'claimReviewSentCount' => $claimReviewSentCount,
				'claimReviewToSendCount' => $claimReviewToSendCount
			];
		}

		return $claimReviewStatistics;
	}

	public static function getDefaultClaimReviewStatistic() {
		return (object) [
			'orderCount' => 0,
			'pendingOrderCount' => 0,
			'shippedOrderCount' => 0,
			'returnedOrderCount' => 0,
			'partialReturnedOrderCount' => 0,
			'shippingOrderCount' => 0,
			'unshippedOrderCount' => 0,
			'innerPageOrderCount' => 0,
			'missingShipmentCount' => 0,
			'toClaimReviewCount' => 0,
			'claimReviewSentCount' => 0,
			'claimReviewToSendCount' => 0
		];
	}
}


