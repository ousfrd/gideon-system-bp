<?php
namespace App;

use Illuminate\Support\Facades\Log;
use App\ProductStatistic;
use App\OtherFee;

class Profit {

  /**
     * $params 
     *  'account_id', [get profit by account]
     *  'listing', id: integer [get profit by listing]
     *  'product', id: integer [get profit by product]
     *  'marketplace' [get profit by marketplace]
     *  'country', [alias for marketplace]
     *  'products', [get profit by pm]
     */
  static function get($fromDate, $toDate, $params = []) {
    $defaultParams = [
      'marketplace' => null,
      'country' => null,
      'listing_id' => null,
      'product_id' => null
    ];
    $params = array_merge($defaultParams, $params);
    if ($params['marketplace'] && $params['marketplace'] != 'all') {
      $params['country'] = config('app.marketplaceCountry')[$params['marketplace']];
    }
    if ($params['country'] &&  !$params['marketplace']) {
      $params['marketplace'] = config('app.marketplaces')[$params['country']];
    }
    if (isset($params['old']) && $params['old']) {
      $productStatistic = ProductStatistic::getStatistics($fromDate, $toDate, $params);
    } else {
      $params['from_date'] = $fromDate;
      $params['to_date'] = $toDate;
      $productStatistic = ProductStatistic::getProductStatisticReport($params);
    }
    $productStatistic->{'other_fee'} = 0;
    if (!$params['listing_id'] && !$params['product_id'] && !$params['products']) {
      $otherFees = OtherFee::amountByDate($fromDate, $toDate, $params);
      $productStatistic->{'other_fee'} = $otherFees;
    }
    return $productStatistic;

  }
}