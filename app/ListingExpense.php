<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
class ListingExpense extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'listing_expenses';
	
	public function listing(){
		return $this->belongsTo('App\Listing','listing_id','id');
	}
	
	static function orderReportsByDate($start_date, $end_date,$params){
		
		$query = self::whereBetween('posted_date',[date('Y-m-d',strtotime($start_date)),date('Y-m-d',strtotime($end_date)+24*60*60)]);
		$query = self::buildQuery($query,$params);
		
		$query->select(DB::raw('sum(amount) as total'));
		
		return $query->first()->total;
		
	}
	
	
	static function buildQuery($query,$params=[]) {
		
		if(!empty($params)) {
// 			var_dump($params);
			$query->whereHas('listing',function($q) use ($params) {
				if(isset($params['account_id'])) {
					$q->where('seller_id',$params['account_id']);
				}
				
				if(isset($params['fulfillment_type'])) {
					switch (strtolower($params['fulfillment_type'])){
						case 'afn':
							$q->where('fulfillment_channel','Amazon');
							break;
						case 'mfn':
							$q->where('fulfillment_channel','Merchant');
							break;
					}
				}
				
				if(isset($params['sku']) && !empty($params['sku'])) {
					$q->where('sku',$params['sku']);
				}
				
				if(isset($params['country']) && !empty($params['country'])) {
					$q->where('country',$params['country']);
				}
				
				if(isset($params['asin']) && !empty($params['asin'])) {
					//$q->whereHas('listing',function($q) use ($params){
						$q->where('asin',$params['asin']);
					//});
				}
				
				if( isset($params['products']) && !empty($params['products']) ) {
					//$q->whereHas('listing',function($q) use ($params){
						$q->whereIn('asin',$params['products']);
					//});
						
				}
				
			});
	
		}
		
		
		
		
		
		
		
		return $query;
	}
	
}
	