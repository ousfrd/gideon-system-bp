<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
class OtherFee extends Model
{
    protected $table = 'other_fees';

    public $timestamps = false;
    
    public function merchant(){
		return $this->belongsTo('App\Account','seller_id','seller_id');
	}
	
	
	
	static function amountByDate($start_date, $end_date,$params){
		
		$query = self::whereBetween('posted_date',[\DT::convertToUTC(date('Y-m-d 00:00:00',strtotime($start_date))),\DT::convertToUTC(date('Y-m-d 23:59:59',strtotime($end_date)))]);
		$query->where('fee_type', '<>', 'ReserveDebit')->where('fee_type', '<>', 'ReserveCredit');
		$query = self::buildQuery($query,$params);
		
		$query->select(DB::raw('sum(fee_amount*exchange_rate) as total'));
		//echo $query->toSql();
		
		return 0-$query->first()->total;
		
	}

	static function getFeeDetailByDateRange($start_date, $end_date, $params) {

		$query = self::whereBetween('posted_date',[\DT::convertToUTC(date('Y-m-d 00:00:00',strtotime($start_date))),\DT::convertToUTC(date('Y-m-d 23:59:59',strtotime($end_date)))]);

		$query = self::buildQuery($query,$params);

		return $query->get();
	}


	static function buildQuery($query,$params=[]) {

		if(isset($params['account_id']) && !empty($params['account_id']) && $params['account_id']!='all') {
			
			$query->whereHas('merchant',function($q) use ($params) {
					$q->where('seller_id',$params['account_id']);
			});
		}
		
		if(isset($params['marketplace']) && !empty($params['marketplace']) && $params['marketplace']!='all') {
			$country = array_search($params['marketplace'], config('app.marketplaces'));
			
			if(array_key_exists($country, config('app.countriesCurrency')) ) {
				$currency = config('app.countriesCurrency')[$country];
				$query->where('currency',$currency);
			}
			
		}
		
		return $query;
	}

}
