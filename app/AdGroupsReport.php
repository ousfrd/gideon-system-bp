<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use App\Traits\MassInsertOrUpdate;

class AdGroupsReport extends Model
{
	use MassInsertOrUpdate;
	
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'ad_groups_reports';

	public $timestamps = false;
	
	
	public function account(){
		return $this->belongsTo('App\Account','seller_id','seller_id');
	}
	
	public function ad_group(){
		return $this->belongsTo('App\AdGroup','adGroupId','adGroupId');
	}
}
	