<?php

namespace App;

use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use App\Account;
use App\Listing;
use App\Order;
use App\OrderItem;
use App\AdReport;
use App\Product;
use App\UserProduct;
use App\ProductCategory;
use App\ProductOverview;
use DB;
use Carbon\Carbon;
use App\Helpers\DataHelper;

class ProductStatistic extends Model
{
    public static $orderAggregationMapping = [
        'orders' => 'total_orders',
        'units' => 'total_units',
        'gross_sales' => 'total_gross',
        'refer_fee' => 'commission',
        'shipping' => 'shipping_fee',
        'refund' => 'total_refund',
        'refunded_units' => 'refund_count',
        'promo_orders' => 'promo_count',
        'promo_value' => 'total_promo',
        'other_fee' => 'other_fee',
        'tax' => 'tax',
        'vat_fee' => 'vat_fee',
        'giftwrap_fee' => 'giftwrap_fee',
        'insert_card_orders' => 'insert_card_orders',
        'pending_orders' => 'pending_orders'
    ];
    public static $bestSellingAggregationMapping = [
        'total_units' => 'total_units',
        'total_units_promo' => 'total_units_promo',
        'total_orders' => 'total_orders',
        'total_gross' => 'total_gross_sales',
        'total_gross_promo' => 'total_gross_promo',
        'shipping_fee' => 'total_shipping_fee',
        'tax' => 'total_tax',
        'commission' => 'total_refer_fee',
        'fba_fee' => 'fba_fee',
        'total_promo' => 'total_promo_value',
        'promo_count' => 'total_promo_orders',
        'total_refund' => 'total_refund',
        'refund_count' => 'total_refunded_units',
        'total_cost' => 'total_cost',
        'giftwrap_fee' => 'total_giftwrap_fee',
        'other_fee' => 'total_other_fee',
        'vat_fee' => 'total_vat_fee',
        'ppc_count' => 'ad_orders',
        'ads_sales' => 'ads_sales',
        'adExpenses' => 'ads_cost',
        'total_revenue' => 'total_revenue',
        'fba_revenue' => 'fba_revenue',
        'fba_insert_card_orders' => 'fba_insert_card_orders',
        'insert_card_reviews' => 'insert_card_reviews',
        'fbm_revenue' => 'fbm_revenue',
        'profit' => 'total_profit',
    ];
    public static $orderReportAggregationMapping = [
        'total_refund' => 'total_refund',
        'refund_count' => 'total_refunded_units',
        'total_orders' => 'total_orders',
        'total_units' => 'total_units',
        'shipping_fee' => 'total_shipping_fee',
        'total_gross' => 'total_gross_sales',
        'total_gross_promo' => 'total_gross_promo',
        'tax' => 'total_tax',
        'commission' => 'total_refer_fee',
        'fba_fee' => 'fba_fee',
        'vat_fee' => 'total_vat_fee',
        'total_promo' => 'total_promo_value',
        'promo_count' => 'total_promo_orders',
        'total_units_promo' => 'total_units_promo',
        'total_cost' => 'total_cost',
        'giftwrap_fee' => 'total_giftwrap_fee',
        'other_fee' => 'total_other_fee',
        'revenue' => 'total_revenue',
        'fba_insert_card_orders' => 'fba_insert_card_orders',
        'insert_card_reviews' => 'insert_card_reviews',
        'profit' => 'total_profit',
    ];
    public static $productDailyProfitAggregationMapping = [
        'profit' => 'total_profit',
        'orders' => 'total_orders',
        'total_gross' => 'total_gross_sales',
        'refund' => 'total_refund',
        'product_cost' => 'product_cost',
        'ad_expenses' => 'ads_cost',
        'total_cost' => 'total_cost'
    ];

    protected $guarded = ['id'];

    function product() {
        return $this->belongsTo('App\Product');
    }

    function getMarketplaceAttribute() {
        return config('app.marketplaces')[$this->country];
    }
    /**
     * $params 
     *  'account_id',
     *  'listing_id', id: integer
     *  'product_id', id: integer
     *  'country'
     *  'products'
     */
    static function getStatistics($fromDate, $toDate, $params) {
        $cols = [ 
            DB::raw ( 'sum(fba_units) as fba_units' ),
            DB::raw ( 'sum(fbm_units) as fbm_units' ),
            DB::raw ( 'sum(total_units) as total_units' ),

            DB::raw ( 'sum(fba_orders) as fba_orders' ),
            DB::raw ( 'sum(fbm_orders) as fbm_orders' ),
            DB::raw ( 'sum(total_orders) as total_orders' ),

            DB::raw ( 'sum(total_gross_sales) as total_gross_sales' ),
            DB::raw ( 'sum(total_revenue) as total_revenue'),
            DB::raw ( 'sum(total_profit) as total_profit' ),
            DB::raw ( 'sum(total_shipping) as total_shipping_fee '),
            DB::raw ( 'sum(total_tax) as total_tax' ),
            DB::raw ( 'sum(total_promo_value) as total_promo_value' ),
            DB::raw ( 'sum(fba_fee) as fba_fee' ),
            DB::raw ( 'sum(total_vat_fee) as total_vat_fee' ),
            DB::raw ( 'sum(total_refer_fee) as total_refer_fee' ),
            DB::raw ( 'sum(total_refund) as total_refund' ),

            DB::raw ( 'sum(fba_refunded_units) as fba_refunded_units'),
            DB::raw ( 'sum(fbm_refunded_units) as fbm_refunded_units'),
            DB::raw ( 'sum(total_refunded_units) as total_refunded_units'),

            DB::raw ( 'sum(fba_promo_orders) as fba_promo_orders' ),
            DB::raw ( 'sum(fbm_promo_orders) as fbm_promo_orders' ),
            DB::raw ( 'sum(total_promo_orders) as total_promo_orders' ),

            DB::raw ( 'sum(total_cost) as total_cost' ),
            DB::raw ( 'sum(total_giftwrap_fee) as total_giftwrap_fee' ),
            DB::raw ( 'sum(total_other_fee) as total_other_fee' ),
            DB::raw ( 'sum(organic_orders) as organic_orders' ),
            DB::raw ( 'sum(ads_cost) as ads_cost' ),
            DB::raw ( 'sum(ad_orders) as ad_orders' ),
            DB::raw ( 'sum(insert_card_reviews) as insert_card_reviews' ),
            DB::raw ( 'sum(fba_insert_card_orders) as fba_insert_card_orders' ),
        ];
        $query = self::select($cols);
        $query->where('date', '>=', $fromDate);
        $query->where('date', '<=', $toDate);

        if (array_key_exists('account_id', $params) && $params['account_id'] && $params['account_id'] != 'all') {
            $query->where('seller_id', $params['account_id']);
        }
        if (array_key_exists('listing_id', $params) && $params['listing_id']) {
            $query->where('listing_id', $params['listing_id']);
        }
        if (array_key_exists('product_id', $params) && $params['product_id']) {
            $query->where('product_id', $params['product_id']);
        }
        if (array_key_exists('country', $params) && $params['country'] && $params['country'] != 'all') {
            $query->where('country', $params['country']);
        }
        if (array_key_exists('products', $params) && $params['products']) {
            $query->whereIn('product_id', $params['products']);
        }
        return $query->first();
    }

    public static function getProductStatisticReport($params) {
        $productStatistics = self::getProductStatistics($params);
        return (object) self::generateProductStatisticReport($productStatistics);
    }

    public static function generateProductStatisticReport($productStatistics) {
        return self::aggregateProductStatistics($productStatistics);
    }

    public static function aggregateProductStatistics($productStatistics) {
        return [
            'fba_units' => DataHelper::aggregate($productStatistics, 'fba_units', 'sum'),
            'fba_insert_card_orders' => DataHelper::aggregate($productStatistics, 'fba_insert_card_orders', 'sum'),
            'insert_card_reviews' => DataHelper::aggregate($productStatistics, 'insert_card_reviews', 'sum'),
            'fbm_units' => DataHelper::aggregate($productStatistics, 'fbm_units', 'sum'),
            'total_units' => DataHelper::aggregate($productStatistics, 'total_units', 'sum'),
            'fba_orders' => DataHelper::aggregate($productStatistics, 'fba_orders', 'sum'),
            'fbm_orders' => DataHelper::aggregate($productStatistics, 'fbm_orders', 'sum'),
            'total_orders' => DataHelper::aggregate($productStatistics, 'total_orders', 'sum'),
            'total_gross_sales' => DataHelper::aggregate(
                $productStatistics, 'total_gross_sales', 'sum'),
            'total_revenue' => DataHelper::aggregate($productStatistics, 'total_revenue', 'sum'),
            'fba_revenue' => DataHelper::aggregate($productStatistics, 'fba_revenue', 'sum'),
            'fbm_revenue' => DataHelper::aggregate($productStatistics, 'fbm_revenue', 'sum'),
            'total_profit' => DataHelper::aggregate($productStatistics, 'total_profit', 'sum'),
            'total_shipping_fee' => DataHelper::aggregate($productStatistics, 'total_shipping', 'sum'),
            'total_tax' => DataHelper::aggregate($productStatistics, 'total_tax', 'sum'),
            'total_promo_value' => DataHelper::aggregate($productStatistics, 'total_promo_value', 'sum'),
            'fba_fee' => DataHelper::aggregate($productStatistics, 'fba_fee', 'sum'),
            'total_vat_fee' => DataHelper::aggregate($productStatistics, 'total_vat_fee', 'sum'),
            'total_refer_fee' => DataHelper::aggregate(
                $productStatistics, 'total_refer_fee', 'sum'),
            'total_refund' => DataHelper::aggregate($productStatistics, 'total_refund', 'sum'),
            'fba_refunded_units' => DataHelper::aggregate(
                $productStatistics, 'fba_refunded_units', 'sum'),
            'fbm_refunded_units' => DataHelper::aggregate(
                $productStatistics, 'fbm_refunded_units', 'sum'),
            'total_refunded_units' => DataHelper::aggregate(
                $productStatistics, 'total_refunded_units', 'sum'),
            'fba_promo_orders' => DataHelper::aggregate(
                $productStatistics, 'fba_promo_orders', 'sum'),
            'fbm_promo_orders' => DataHelper::aggregate(
                $productStatistics, 'fbm_promo_orders', 'sum'),
            'total_promo_orders' => DataHelper::aggregate(
                $productStatistics, 'total_promo_orders', 'sum'),
            'total_cost' => DataHelper::aggregate($productStatistics, 'total_cost', 'sum'),
            'total_giftwrap_fee' => DataHelper::aggregate(
                $productStatistics, 'total_giftwrap_fee', 'sum'),
            'total_other_fee' => DataHelper::aggregate(
                $productStatistics, 'total_other_fee', 'sum'),
            'organic_orders' => DataHelper::aggregate($productStatistics, 'organic_orders', 'sum'),
            'ads_cost' => DataHelper::aggregate($productStatistics, 'ads_cost', 'sum'),
            'ads_sales' => DataHelper::aggregate($productStatistics, 'ads_sales', 'sum'),
            'ad_orders' => DataHelper::aggregate($productStatistics, 'ad_orders', 'sum'),
            'fba_pending_orders' => DataHelper::aggregate($productStatistics, 'fba_pending_orders', 'sum'),
            'fbm_pending_orders' => DataHelper::aggregate($productStatistics, 'fbm_pending_orders', 'sum'),
            'total_units_promo' => DataHelper::aggregate($productStatistics, 'total_units_promo', 'sum'),
            'total_gross_promo' => DataHelper::aggregate($productStatistics, 'total_gross_promo', 'sum'),
            'product_cost' => DataHelper::aggregate($productStatistics, function($ps) {
                return $ps->product_unit_cost * $ps->total_units;
            }, 'sum'),
        ];
    }

    public static function updateInsertCardReviews($order) {
        $purchaseDate = Order::select('PurchaseDate')->where('AmazonOrderId', $order)->first();
    
        $date = substr($purchaseDate->PurchaseDate, 0, 10);
        $sku = OrderItem::select('SellerSKU')->where('AmazonOrderId', $order)->first()->SellerSKU;
        $asin = OrderItem::select('ASIN')->where('AmazonOrderId', $order)->first()->ASIN;
        ProductStatistic::where('asin', $asin)->where('sku', $sku)->where('date', $date)->increment('insert_card_reviews', 1);
        $product = ProductStatistic::where('asin', $asin)->where('sku', $sku)->where('date', $date)->get();
  
    }

    public static function getProductStatistics($params) {
        return self::getQueryBuilder($params)->get();
    }

    public static function getQueryBuilder($params) {
        $queryConds = self::getQuery($params);
        $normalQueryConds = array_filter($queryConds, function($queryCond) {
            return strtolower($queryCond[1]) != 'in';
        });
        $query = self::where($normalQueryConds);

        $inQueryConds = array_filter($queryConds, function($queryCond) {
            return strtolower($queryCond[1]) == 'in';
        });
        if ($inQueryConds) {
            foreach ($inQueryConds as $inQueryCond) {
                $query = $query->whereIn($inQueryCond[0], $inQueryCond[2]);
            }
        }
        $notInQueryConds = array_filter($queryConds, function($queryCond) {
            return strtolower($queryCond[1]) == 'not in';
        });
        if ($notInQueryConds) {
            foreach ($notInQueryConds as $notInQueryCond) {
                $query = $query->whereNotIn($notInQueryCond[0], $notInQueryCond[2]);
            }
        }
        return $query;
    }

    public static function getQuery($params) {
        $query = [];

        $psObj = new ProductStatistic();
        $psTable = $psObj->getTable();

        if (isset($params['account_id']) && !empty($params['account_id']) && strtolower($params['account_id']) != 'all') {
            array_push($query, [$psTable . '.seller_id', '=', $params['account_id']]);
        }

        if (isset($params['listing_id']) && !empty($params['listing_id'])) {
            array_push($query, [$psTable . '.listing_id', '=', $params['listing_id']]);
        }

        if (isset($params['product_id']) && !empty($params['product_id'])) {
            array_push($query, [$psTable . '.product_id', '=', $params['product_id']]);
        }

        if (isset($params['products']) && !empty($params['products'])) {
            array_push($query, [$psTable . '.product_id', 'in', $params['products']]);
        }

        if (isset($params['country']) && !empty($params['country']) && strtolower($params['country']) != 'all') {
            array_push($query, [$psTable . '.country', '=', $params['country']]);
        }

        if (isset($params['asins']) && !empty($params['asins'])) {
            array_push($query, [$psTable . '.asin', 'in', $params['asins']]);
        }

        if (isset($params['asin']) && !empty($params['asin'])) {
            array_push($query, [$psTable . '.asin', '=', $params['asin']]);
        }

        if (isset($params['sku']) && !empty($params['sku'])) {
            array_push($query, [$psTable . '.sku', '=', $params['sku']]);
        }

        // 2020-09-18
        if (isset($params['from_date']) && !empty($params['from_date'])) {
            array_push($query, [$psTable . '.date', '>=', $params['from_date']]);
        }

        if (isset($params['to_date']) && !empty($params['to_date'])) {
            array_push($query, [$psTable . '.date', '<=', $params['to_date']]);
        }

        return $query;
    }

    static function getBestSellers($fromDate, $toDate) {
        $cols = [
            'product_id',
            'country',
            'asin',
            DB::raw ( 'sum(fba_units) as fba_units' ),
            DB::raw ( 'sum(fbm_units) as fbm_units' ),
            DB::raw ( 'sum(total_units) as total_units' ),

            DB::raw ( 'sum(fba_orders) as fba_orders' ),
            DB::raw ( 'sum(fbm_orders) as fbm_orders' ),
            DB::raw ( 'sum(total_orders) as total_orders' ),

            DB::raw ( 'sum(total_gross_sales) as total_gross_sales' ),
            DB::raw ( 'sum(total_revenue) as total_revenue'),
            DB::raw ( 'sum(total_profit) as total_profit' ),
            DB::raw ( 'sum(total_shipping) as total_shipping_fee '),
            DB::raw ( 'sum(total_tax) as total_tax' ),
            DB::raw ( 'sum(total_promo_value) as total_promo_value' ),
            DB::raw ( 'sum(fba_fee) as fba_fee' ),
            DB::raw ( 'sum(total_vat_fee) as total_vat_fee' ),
            DB::raw ( 'sum(total_refer_fee) as total_refer_fee' ),
            DB::raw ( 'sum(total_refund) as total_refund' ),

            DB::raw ( 'sum(fba_refunded_units) as fba_refunded_units'),
            DB::raw ( 'sum(fbm_refunded_units) as fbm_refunded_units'),
            DB::raw ( 'sum(total_refunded_units) as total_refunded_units'),

            DB::raw ( 'sum(fba_promo_orders) as fba_promo_orders' ),
            DB::raw ( 'sum(fbm_promo_orders) as fbm_promo_orders' ),
            DB::raw ( 'sum(total_promo_orders) as total_promo_orders' ),

            DB::raw ( 'sum(total_cost) as total_cost' ),
            DB::raw ( 'sum(total_giftwrap_fee) as total_giftwrap_fee' ),
            DB::raw ( 'sum(total_other_fee) as total_other_fee' ),
            DB::raw ( 'sum(organic_orders) as organic_orders' ),
            DB::raw ( 'sum(ads_cost) as ads_cost' ),
            DB::raw ( 'sum(fba_insert_card_orders) as fba_insert_card_orders' ),
            DB::raw ( 'sum(insert_card_reviews) as insert_card_reviews' ),
            DB::raw ( 'sum(ad_orders) as ad_orders' ),
        ];
        $query = ProductStatistic::with('product')->select($cols);
        $query->where('date', '>=', $fromDate);
        $query->where('date', '<=', $toDate);

        $query->groupBy('product_id');
        $query->orderBy('total_profit', 'desc');

        return $query->get();
    }

    static function buildStatistics($fromDate, $toDate) {
        $listings = Listing::select('id', 'seller_id', 'asin', 'sku', 'country', 'product_id')->where('status', '>=', 0)->get();
        foreach ($listings as $listing) {
            self::buildStatisticsForListing($listing, $fromDate, $toDate);
        }
    }

    static function buildStatisticsForProduct($product, $fromDate, $toDate) {
        $listings = $product->listings;
        foreach ($listings as $listing) {
            self::buildStatisticsForListing($listing, $fromDate, $toDate);
        }
    }

    static function buildStatisticsForSeller($seller, $fromDate, $toDate) {
        $listings = $seller->listings;
        foreach ($listings as $listing) {
            self::buildStatisticsForListing($listing, $fromDate, $toDate);
        }
    }

    static function buildStatisticsForLast2Days() {
        $fromDate = (new Carbon())->subDays(2)->toDateString();
        $toDate = (new Carbon('tomorrow'))->toDateString();
        self::buildStatistics($fromDate, $toDate);
    }

    static function buildStatisticsForLastWeek() {
        $fromDate = (new Carbon())->subDays(7)->toDateString();
        $toDate = (new Carbon('tomorrow'))->toDateString();
        self::buildStatistics($fromDate, $toDate);
    }

    static function buildStatisticsForLastMonth() {
        $fromDate = (new Carbon())->subDays(30)->toDateString();
        $toDate = (new Carbon('tomorrow'))->toDateString();
        self::buildStatistics($fromDate, $toDate);
    }


    static function getAdsDataForListing($listing, $fromDate, $toDate) {
        $query = AdReport::whereBetween ('date', [$fromDate, $toDate] )
                            ->select ( 
                                DB::raw('sum(attributedConversions7d) as ad_orders' ),
                                DB::raw('round(sum(cost*exchange_rate), 2) as ad_cost')
                            )
                            ->where('seller_id', $listing->seller_id)
                            ->where('sku', $listing->sku);
        return $query->first();
    }

    public static function getBestSellingProducts($params) {
        $result = [];

        $aggregation = [];
        $productStatistics = self::getProductStatistics($params);
        $groupedProductStatistics = DataHelper::groupByVal($productStatistics, 'asin');
        foreach ($groupedProductStatistics as $asin => $productStatisticsByProduct) {
            $groupedPs = DataHelper::groupByVal($productStatisticsByProduct, function($ps) {
                return strtoupper($ps->country);
            });
            foreach ($groupedPs as $country => $psByProductCountry) {
                $data = self::aggregateProductStatistics($psByProductCountry);
                $aggregationEntry = ['ASIN' => $asin, 'country' => $country];
                foreach (self::$bestSellingAggregationMapping as $k => $v) {
                    $aggregationEntry[$k] = $data[$v];
                }
                $aggregationEntry['total_units_full'] = $aggregationEntry['total_units'] - $aggregationEntry['total_units_promo'];
                $aggregationEntry['total_gross_full'] = $aggregationEntry['total_gross'] - $aggregationEntry['total_gross_promo'];
                $payout = $aggregationEntry['total_revenue'] - $aggregationEntry['adExpenses'] - $data['total_tax'];
                $aggregationEntry['payout'] = round($payout, 2);
                $aggregationEntry['profit'] = $aggregationEntry['payout'] - $aggregationEntry['total_cost'];
                $aggregationEntry['gross_sales'] = $aggregationEntry['total_gross'] + $aggregationEntry['shipping_fee'];

                $aggregation[$country . '-' . $asin] = $aggregationEntry;
            }
        }

        uasort($aggregation, function($a, $b) {
            if ($a['profit'] == $b['profit']) {
                return 0 ;
            }

            return ($a['profit'] < $b['profit']) ? 1 : -1;
        });
        return $aggregation;
    }

    public static function getProductPerformance($params) {
        $bestSellingProductStatistics = ProductStatistic::getBestSellingProducts($params);

        $records = [];
        foreach ($bestSellingProductStatistics as $productStatistic) {
            $productStatistic = (object) $productStatistic;

            $record = [];
            $record['marketplace'] = $productStatistic->country;
            $record['asin'] = $productStatistic->ASIN;
            $cost = $productStatistic->total_cost;
            $adExpenses = $productStatistic->adExpenses;
            if ($cost > 0) {
                $record['roi'] = round($productStatistic->profit / $cost * 100, 2);
                $record['roi2'] = round($productStatistic->profit / ($cost + $adExpenses) * 100, 2);
            } else {
                $record['roi'] = 'N/A';
                $record['roi2'] = 'N/A';
            }
            $record['profit'] = $productStatistic->profit;
            $record['payout'] = $productStatistic->payout;
            $record['total_revenue'] = $productStatistic->total_revenue;
            $record['total_cost'] = $productStatistic->total_cost;
            $record['total_gross'] = $productStatistic->total_gross;
            $record['shipping_fee'] = $productStatistic->shipping_fee;
            $record['vat_fee'] = $productStatistic->vat_fee;
            $record['fba_fee'] = $productStatistic->fba_fee;
            $record['fba_insert_card_orders'] = $productStatistic->fba_insert_card_orders;
            $record['insert_card_reviews'] = $productStatistic->insert_card_reviews;
            $record['total_refund'] = $productStatistic->total_refund;
            $record['tax'] = $productStatistic->tax;
            $record['commission'] = $productStatistic->commission;
            $record['giftwrap_fee'] = $productStatistic->giftwrap_fee;
            $record['other_fee'] = $productStatistic->other_fee;
            $record['adExpenses'] = $productStatistic->adExpenses;
            $record['total_orders'] = $productStatistic->total_orders;
            $record['total_units'] = $productStatistic->total_units;
            $record['refund_count'] = $productStatistic->refund_count;
            $record['ppc_count'] = $productStatistic->ppc_count;
            $record['ads_sales'] = $productStatistic->ads_sales;

            $k = sprintf('%s-%s', $record['marketplace'], $record['asin']);
            $records[$k] = $record;
        }

        return $records;
    }

    public static function getDefaultProductPerformance() {
        return [
            'roi' => 0,
            'roi2' => 0,
            'profit' => 0,
            'payout' => 0,
            'total_revenue' => 0,
            'total_cost' => 0,
            'total_gross' => 0,
            'shipping_fee' => 0,
            'vat_fee' => 0,
            'fba_fee' => 0,
            'fba_insert_card_orders' => 0,
            'insert_card_reviews' => 0,
            'total_refund' => 0,
            'tax' => 0,
            'commission' => 0,
            'giftwrap_fee' => 0,
            'other_fee' => 0,
            'adExpenses' => 0,
            'total_orders' => 0,
            'total_units' => 0,
            'refund_count' => 0,
            'ppc_count' => 0,
            'ads_sales' => 0
        ];
    }

    public static function getProductOverviews($params) {
        $fromDateStr = $params['from_date'];
        $toDateStr = $params['to_date'];
        $toDate = \Carbon\Carbon::createFromFormat('Y-m-d', $toDateStr);
        $last7FromDate = $toDate->copy()->subDays(7)->startOfDay();
        $last7FromDateStr = $last7FromDate->format('Y-m-d');

        $bestSellingProductStatistics = ProductStatistic::getBestSellingProducts($params);

        $productOverviews = [];
        $records = [];
        foreach ($bestSellingProductStatistics as $productStatistic) {
            $productStatistic = (object) $productStatistic;

            $record = [];
            $record['marketplace'] = $productStatistic->country;
            $record['asin'] = $productStatistic->ASIN;
            $cost = $productStatistic->total_cost;
            $adExpenses = $productStatistic->adExpenses;

            $record['profit'] = $productStatistic->profit;
            $record['payout'] = $productStatistic->payout;
            $record['adExpenses'] = $productStatistic->adExpenses;
            $record['total_orders'] = $productStatistic->total_orders;
            $record['total_units'] = $productStatistic->total_units;
            $record['ppc_count'] = $productStatistic->ppc_count;
            $record['ads_sales'] = $productStatistic->ads_sales;

            if ($record['profit'] != 0) {
                $adProfitPercentage = round($record['adExpenses'] / $record['profit'] * 100, 2);
            } else {
                $adProfitPercentage = 'N/A';
            }
            $record['adProfitPercentage'] = $adProfitPercentage;

            if (isset($record['ads_sales']) && $record['ads_sales'] > 0) {
                $acos = round($record['adExpenses'] / $record['ads_sales'] * 100, 2);
            } else {
                $acos = 'N/A';
            }
            $record['acos'] = $acos;

            $k = sprintf('%s-%s', $record['marketplace'], $record['asin']);
            $records[$k] = $record;
        }

        $productClaimReviewStatistics = ClaimReviewCampaign::getClaimReviewStatistics($fromDateStr, $toDateStr);

        $redeemStatistics = Redeem::getRedeemStatistics($fromDateStr, $toDateStr);

        $selfReviewStatistics = ManagedReviewOrder::getSelfReviewStatistics($fromDateStr, $toDateStr);

        $productPerformances = self::getProductPerformance([
            'from_date' => $last7FromDateStr,
            'to_date' => $toDateStr
        ]);

        $activeAccounts = [];
        foreach (Account::getSortedAccounts() as $account) {
            if (strtolower($account->status) != 'active') {
                continue;
            }

            $activeAccounts[$account->seller_id] = $account;
        }
        $productCategories = [];
        foreach (ProductCategory::all() as $category) {
            $productCategories[$category->id] = $category->category;
        }
        $products = Product::with(['users', 'listings'])->where([
            ['discontinued', '=', FALSE],
            ['status', '>', 0],
        ])->get();
        foreach ($products as $product) {
            $productInfo = $product->attributesToArray();
            $user = $product->users->first();
            if ($user) {
                $productInfo['PM'] = $user->name;
            } else {
                $productInfo['PM'] = 'N/A';
            }

            $sellerAccounts = [];
            foreach ($product->listings as $listing) {
                if ($listing->status < 0) {
                    continue;
                }

                if (array_key_exists($listing->seller_id, $activeAccounts)) {
                    $sellerAccounts[] = $activeAccounts[$listing->seller_id]->code;
                }
            }
            $sellerAccounts = array_unique($sellerAccounts);
            $productInfo['sellerAccounts'] = implode(',', $sellerAccounts);

            $hasInnerPage = $product->listings->contains(function ($listing) {
                return $listing->insert_card > 0;
            });
            $productInfo['hasInnerPage'] = $hasInnerPage ? 'YES' : 'NO';

            $productInfo['serialNum'] = str_pad($product->id, 5, '0', STR_PAD_LEFT);

            $productInfo['statusText'] = $product->status > 0 ? 'Active' : 'Inactive';
            $productInfo['productName'] = $product->alias ?? $product->name;
            $productInfo['productGroup'] = $product->product_group ?? '';
            $productInfo['reviewRating'] = $product->review_rating ?? 0;
            $productInfo['reviewQty'] = $product->review_quantity ?? 0;
            if ($product->category_id == NULL) {
                $productInfo['category'] = '';
            } else {
                if (array_key_exists($product->category_id, $productCategories)) {
                    $productInfo['category'] = $productCategories[$product->category_id];
                } else {
                    $productInfo['category'] = '';
                }
            }
            $productInfo['shouldClaimReview'] = $product->should_claim_review ? 'YES' : 'NO';
            $productInfo['shouldInviteReview'] = $product->should_invite_review ? 'YES' : 'NO';
            $productInfo['negtiveReviewRemove'] = $product->negtive_review_remove ? 'YES' : 'NO';
            $productInfo['selfReview'] = $product->self_review ? 'YES' : 'NO';

            $productInfo['fulfillableQuantity'] = 0;
            $productInfo['unsellableQuantity'] = 0;
            $productInfo['reservedQuantity'] = 0;
            $productInfo['warehouseQuantity'] = 0;
            $productInfo['inboundWorkingQuantity'] = 0;
            $productInfo['inboundShippedQuantity'] = 0;
            $productInfo['inboundReceivingQuantity'] = 0;
            $productInfo['totalQuantity'] = 0;
            foreach ($product->listings as $listing) {
                if ($listing->status < 0) {
                    continue;
                }

                if (!array_key_exists($listing->seller_id, $activeAccounts)) {
                    continue;
                }

                $productInfo['fulfillableQuantity'] += $listing->afn_fulfillable_quantity;
                $productInfo['unsellableQuantity'] += $listing->afn_unsellable_quantity;
                $productInfo['reservedQuantity'] += $listing->afn_reserved_quantity;
                $productInfo['warehouseQuantity'] += $listing->afn_warehouse_quantity;
                $productInfo['inboundWorkingQuantity'] += $listing->afn_inbound_working_quantity;
                $productInfo['inboundShippedQuantity'] += $listing->afn_inbound_shipped_quantity;
                $productInfo['inboundReceivingQuantity'] += $listing->afn_inbound_receiving_quantity;
                $productInfo['totalQuantity'] += $listing->afn_total_quantity;
            }
            $productInfo['amazonQty'] = $productInfo['fulfillableQuantity'] + $productInfo['reservedQuantity'] + $productInfo['inboundReceivingQuantity'];

            $k = sprintf('%s-%s', $product->country, $product->asin);
            if (array_key_exists($k, $records)) {
                $record = $records[$k];
            } else {
                $record = ProductStatistic::getDefaultProductPerformance();
            }

            if (array_key_exists($k, $productPerformances)) {
                $productPerformance = $productPerformances[$k];
                $productInfo['last7DaysProfit'] = $productPerformance['profit'];
                $productInfo['last7DaysUnits'] = $productPerformance['total_units'];
            } else {
                $productInfo['last7DaysProfit'] = 0;
                $productInfo['last7DaysUnits'] = 0;
            }

            if (array_key_exists($k, $redeemStatistics)) {
                $redeemStatistic = $redeemStatistics[$k];
            } else {
                $redeemStatistic = Redeem::getDefaultRedeemStatistic();
            }

            if (array_key_exists($k, $productClaimReviewStatistics)) {
                $claimReviewStatistic = $productClaimReviewStatistics[$k];
            } else {
                $claimReviewStatistic = ClaimReviewCampaign::getDeafultClaimReviewStatistic();
            }

            if (array_key_exists($k, $selfReviewStatistics)) {
                $selfReviewStatistic = $selfReviewStatistics[$k];
            } else {
                $selfReviewStatistic = ManagedReviewOrder::getDefaultSelfReviewStatistic();
            }

            $productOverviews[] = (object) array_merge($productInfo, $record, $redeemStatistic, $claimReviewStatistic, $selfReviewStatistic);
        }

        Log::debug($productOverviews);

        return $productOverviews;
    }

    public static function buildProductOverviews($fromDate, $toDate = NULL) {
        if (empty($toDate)) {
            $toDate = \Carbon\Carbon::now()->format('Y-m-d');
        }

        $params = [
            'from_date' => $fromDate,
            'to_date' => $toDate
        ];
        $aggregation = [];
        $productStatistics = self::getDailyProductStatistics($params);
        foreach ($productStatistics as $date => $productStatisticsByDate) {
            $productStatisticsByProduct = DataHelper::groupByVal($productStatisticsByDate, function ($statistic) {
                return sprintf('%s-%s', $statistic->country, $statistic->asin);
            });

            foreach ($productStatisticsByProduct as $k => $statistics) {
                if (empty($statistics)) {
                    continue;
                }

                list($marketplace, $asin) = explode('-', $k);

                $data = self::aggregateProductStatistics($statistics);
                $totalRevenue = $data['total_revenue'];
                $totalCost = $data['total_cost'];
                $totalGross = $data['total_gross_sales'];
                $shippingFee = $data['total_shipping_fee'];
                $vatFee = $data['total_vat_fee'];
                $fbaFee = $data['fba_fee'];
                $totalRefund = $data['total_refund'];
                $tax = $data['total_tax'];
                $commission = $data['total_refer_fee'];
                $giftwrapFee = $data['total_giftwrap_fee'];
                $otherFee = $data['total_other_fee'];
                $profit = $data['total_profit'];
                $adExpenses = $data['ads_cost'];
                $adSales = $data['ads_sales'];
                $totalOrders = $data['total_orders'];
                $totalUnits = $data['total_units'];
                $promoUnits = $data['total_units_promo'];
                $ppcCount = $data['ad_orders'];
                $payout = round($totalRevenue - $adExpenses - $tax, 2);

                $productOverview = ProductOverview::firstOrNew([
                    'marketplace' => $marketplace,
                    'asin' => $asin,
                    'date' => $date
                ]);

                $productOverview->fill([
                    'total_revenue' => $totalRevenue,
                    'total_cost' => $totalCost,
                    'total_gross' => $totalGross,
                    'shipping_fee' => $shippingFee,
                    'vat_fee' => $vatFee,
                    'fba_fee' => $fbaFee,
                    'total_refund' => $totalRefund,
                    'tax' => $tax,
                    'commission' => $commission,
                    'giftwrap_fee' => $giftwrapFee,
                    'other_fee' => $otherFee,
                    'profit' => $profit,
                    'payout' => $payout,
                    'ad_expenses' => $adExpenses,
                    'ads_sales' => $adSales,
                    'total_orders' => $totalOrders,
                    'total_units' => $totalUnits,
                    'promo_units' => $promoUnits,
                    'ppc_count' => $ppcCount,
                ]);
                $productOverview->save();
            }
        }
    }

    public static function getHourlyOrderReports($orderParams) {
        $query = Order::getQuery($orderParams);
        $orders = Order::getOrders($query);

        return Order::generateOrderReports($orders, 'hourly');
    }

    public static function getDailyOrderReports($productStatisticParams) {
        $aggregation = [];
        $productStatistics = self::getProductStatistics($productStatisticParams);
        $groupedProductStatistics = DataHelper::groupByVal($productStatistics, 'date');
        foreach ($groupedProductStatistics as $date => $productStatisticsByDate) {
            $data = self::aggregateProductStatistics($productStatisticsByDate);
            $aggregationEntry = [];
            foreach (self::$orderReportAggregationMapping as $k => $v) {
                $aggregationEntry[$k] = $data[$v];
            }
            $aggregationEntry['total_units_full'] = $aggregationEntry['total_units'] - $aggregationEntry['total_units_promo'];
            $aggregationEntry['total_gross_full'] = $aggregationEntry['total_gross'] - $aggregationEntry['total_gross_promo'];
            $aggregationEntry['pending_orders'] = $data['fba_pending_orders'] + $data['fbm_pending_orders'];

            $aggregation[$date] = (object)$aggregationEntry;
        }

        if (isset($productStatisticParams['from_date'])) {
            $startDate = \Carbon\Carbon::createFromFormat(
                'Y-m-d', $productStatisticParams['from_date']);
        } else {
            $startDate = \Carbon\Carbon::now();
        }

        if (isset($productStatisticParams['to_date'])) {
            $endDate = \Carbon\Carbon::createFromFormat(
                'Y-m-d', $productStatisticParams['to_date']);
        } else {
            $endDate = \Carbon\Carbon::now();
        }
        $endDate = $endDate->startOfDay();
        for ($date = $startDate->copy()->startOfDay(); $date->lte($endDate); $date->addDay()) {
            $dateKey = $date->format(DataHelper::DATE_METRICS['day']);
            if (!isset($aggregation[$dateKey]) || empty($aggregation[$dateKey])) {
                $aggregation[$dateKey] = Order::getDefaultOrderReport();
            }
        }

        return $aggregation;
    }

    public static function getProductDailyProfit($productStatisticParams) {
        $aggregation = [];
        $productStatistics = self::getDailyProductStatistics($productStatisticParams);
        foreach ($productStatistics as $date => $productStatisticsByDate) {
            if (empty($productStatisticsByDate)) {
                $aggregation[$date] = self::getDefaultProductDailyProfit();
                continue;
            }

            $data = self::aggregateProductStatistics($productStatisticsByDate);
            $aggregationEntry = [];
            foreach (self::$productDailyProfitAggregationMapping as $k => $v) {
                if (is_float($data[$v])) {
                    $aggregationEntry[$k] = round($data[$v], 2);
                } else {
                    $aggregationEntry[$k] = $data[$v];
                }
            }
            $aggregationEntry['payout'] = round(
                $data['total_revenue'] - $data['ads_cost'] - $data['total_tax'], 2);
            $aggregationEntry['total_sessions'] = 0;
            $aggregationEntry['unit_session_percentage'] = 0;

            $aggregation[$date] = $aggregationEntry;
        }

        return $aggregation;
    }

    public static function getPmMonthlyProfit($productStatisticParams) {
        // users contains all pms have products
        $users = [];
        // userProducts contains product's user information
        $userProducts = [];

        $userProductRecords = UserProduct::select(
            DB::raw('product_users.user_id, product_users.product_id, users.name, users.group'))
            ->leftJoin('users', function($join) {
                $join->on('users.id', '=', 'product_users.user_id');
            })->get()->toArray();
        foreach ($userProductRecords as $userProductRecord) {
            $user = [
                'info' => [
                    'id' => $userProductRecord['user_id'],
                    'name' => $userProductRecord['name'],
                    'group' => $userProductRecord['group'],
                ],
            ];

            $users[$userProductRecord['user_id']] = $user;
            $userProducts[$userProductRecord['product_id']] = $user['info'];
        }

        $aggregation = [];
        $productStatistics = self::getDailyProductStatistics($productStatisticParams);
        foreach ($productStatistics as $date => $productStatisticsByDate) {
            $aggregation[$date] = [];

            $userProductStatistics = [];
            foreach ($productStatisticsByDate as $ps) {
                $productId = $ps->product_id;
                if (!array_key_exists($productId, $userProducts)) {
                    continue;
                }

                $userInfo = $userProducts[$productId];
                if (!array_key_exists($userInfo['id'], $userProductStatistics)) {
                    $userProductStatistics[$userInfo['id']] = [];
                }
                array_push($userProductStatistics[$userInfo['id']], $ps);
            }
            // $productStatisticsByDateProduct = DataHelper::groupByVal(
            //     $productStatisticsByDate, 'product_id');
            // foreach ($productStatisticsByDateProduct as $productId => $groupedProductStatistics) {
            //     if (!array_key_exists($productId, $userProducts)) {
            //         continue;
            //     }

            //     $userInfo = $userProducts[$productId];
            //     foreach ($groupedProductStatistics as $ps) {
            //         array_push($users[$userInfo['id']]['productStatistics'], $ps);
            //     }
            // }

            foreach ($users as $userId => $userData) {
                if (!array_key_exists($userId, $userProductStatistics) || empty($userProductStatistics[$userId])) {
                    array_push(
                        $aggregation[$date],
                        array_merge(
                            ['user' => $userData['info']],
                            self::getDefaultPmMonthlyProfit()
                        )
                    );

                    continue;
                }

                $aggregationEntry = [
                    'user' => $userData['info']
                ];
                $data = self::aggregateProductStatistics($userProductStatistics[$userId]);
                foreach (self::$productDailyProfitAggregationMapping as $k => $v) {
                    if (is_float($data[$v])) {
                        $aggregationEntry[$k] = round($data[$v], 2);
                    } else {
                        $aggregationEntry[$k] = $data[$v];
                    }
                }
                $aggregationEntry['payout'] = round(
                    $data['total_revenue'] - $data['ads_cost'] - $data['total_cost'], 2);

                array_push($aggregation[$date], $aggregationEntry);
            }
        }

        $usersInfo = array_map(function($user) {
            return $user['info'];
        }, $users);

        return array($usersInfo, $aggregation);
    }

    public static function getDailyProductStatistics($productStatisticParams) {
        $productStatistics = self::getProductStatistics($productStatisticParams);
        $groupedProductStatistics = DataHelper::groupByVal($productStatistics, 'date');

        if (isset($productStatisticParams['from_date'])) {
            $startDate = \Carbon\Carbon::createFromFormat(
                'Y-m-d', $productStatisticParams['from_date']);
        } else {
            $startDate = \Carbon\Carbon::now();
        }

        if (isset($productStatisticParams['to_date'])) {
            $endDate = \Carbon\Carbon::createFromFormat(
                'Y-m-d', $productStatisticParams['to_date']);
        } else {
            $endDate = \Carbon\Carbon::now();
        }
        $endDate = $endDate->endOfDay();
        for ($date = $startDate->copy()->startOfDay(); $date->lte($endDate); $date->addDay()) {
            $dateKey = $date->format(DataHelper::DATE_METRICS['day']);
            if (!isset($groupedProductStatistics[$dateKey]) || empty($groupedProductStatistics[$dateKey])) {
                $groupedProductStatistics[$dateKey] = [];
            }
        }

        return $groupedProductStatistics;
    }

    static function buildStatisticsForListing($listing, $fromDate, $toDate) {
        if (TRUE) {
            $productId = $listing->product_id;
            $listingId = $listing->id;
            $sellerId = $listing->seller_id;
            $asin = $listing->asin;
            $sku = $listing->sku;

            $startDate = \Carbon\Carbon::parse(
                $fromDate, config('app.timezone'))->startOfDay()->setTimezone('UTC');
            $endDate = \Carbon\Carbon::parse(
                $toDate, config('app.timezone'))->endOfDay()->setTimezone('UTC');
            $orderStatuses = array_filter(Order::ORDER_STATUS, function($v) {
                return $v != 'Cancelled';
            });
            $queryCond = [
                'account_id' => $sellerId,
                'marketplace' => $listing->country,
                'asin' => $asin,
                'sku' => $sku,
                'statuses' => $orderStatuses,
                'purchase_date' => [
                    'start_date' => $startDate->format('Y-m-d H:i:s'),
                    'end_date' => $endDate->format('Y-m-d H:i:s')
                ],
            ];
            $query = Order::getQuery($queryCond);
            $orders = Order::getOrders($query);
            $groupedOrders = DataHelper::groupByVal($orders, 'FulfillmentChannel');
            if (empty($orders) || empty($groupedOrders)) {
                $message = sprintf(
                        '[NoOrderForQuery] Query: %s, Orders: %s, GroupedOrders: %s',
                        json_encode($queryCond), json_encode($orders),
                        json_encode($groupedOrders));
                Log::debug($message);
            } else {
                $message = sprintf(
                    '[OrdersByDate] Start: %s, End: %s, ASIN: %s, SKU: %s, Orders: %s',
                    $queryCond['purchase_date']['start_date'],
                    $queryCond['purchase_date']['end_date'],
                    $listing->asin, $listing->sku, json_encode($groupedOrders));
                Log::debug($message);
            }

            if (array_key_exists('AFN', $groupedOrders)) {
                $FBAOrdersStatistics = self::aggregateFBAOrders($groupedOrders['AFN']);
            } else {
                $FBAOrdersStatistics = [];
            }
            if (array_key_exists('MFN', $groupedOrders)) {
                $FBMOrdersStatistics = self::aggregateFBMOrders($groupedOrders['MFN']);
            } else {
                $FBMOrdersStatistics = [];
            }

            $dates = array_merge(
                array_keys($FBAOrdersStatistics), array_keys($FBMOrdersStatistics));
            foreach ($dates as $date) {
                if (array_key_exists($date, $FBAOrdersStatistics)) {
                    $fbaAggregation = $FBAOrdersStatistics[$date];
                } else {
                    $fbaAggregation = self::getDefaultFBAAggregation();
                }

                if (array_key_exists($date, $FBMOrdersStatistics)) {
                    $fbmAggregation = $FBMOrdersStatistics[$date];
                } else {
                    $fbmAggregation = self::getDefaultFBMAggregation();
                }

                $adData = AdReport::getAggregatedAdReports([
                    'seller_id' => $sellerId,
                    'asin' => $asin,
                    'sku' => $sku,
                    'date' => $date
                ]);

                $product = Product::find($productId);
                if ($product) {
                    $productCost = $product->productCostInDate($date);
                    $shippingCost = $product->shippingCostInDate($date);
                    $fbmShippingCost = $product->fbmShippingCostInDate($date);
                } else {
                    $productCost = 0;
                    $shippingCost = 0;
                    $fbmShippingCost = 0;
                }
                $productUnitCost = empty($productCost) ? 0 : $productCost->cost_per_unit;
                $unitShipToAmzCost = empty($shippingCost) ? 0 : $shippingCost->cost_per_unit;
                $fbmUnitShippingCost = empty($fbmShippingCost) ? 0 : $fbmShippingCost->cost_per_unit;

                $baseData = [
                    'asin' => $listing->asin,
                    'sku' => $listing->sku,
                    'country' => $listing->country,
                    'seller_id' => $listing->seller_id,
                    'ad_orders' => $adData['orders'],
                    'ads_cost' => $adData['cost'],
                    'ads_sales' => $adData['sales'],
                    'product_unit_cost' => $productUnitCost,
                    'unit_ship_to_amz_cost' => $unitShipToAmzCost,
                    'fbm_unit_shipping_cost' => $fbmUnitShippingCost
                ];
                $psData = array_merge($baseData, $fbaAggregation, $fbmAggregation);

                $ps = ProductStatistic::firstOrNew([
                    'product_id' => $productId,
                    'listing_id' => $listingId,
                    'date' => $date
                ]);
                $ps->fill($psData);
                $ps->save();
            }
        } else {
            $marketplace = ucfirst(config ( 'app.marketplaces' )[$listing->country]);
            $records = OrderItem::join('orders', 'orders.AmazonOrderId', '=', 'order_items.AmazonOrderId')
                        ->select(DB::raw('orders.OrderStatus, orders.PurchaseDate, orders.SalesChannel, orders.FulfillmentChannel, order_items.*'))
                        ->where('order_items.seller_id', $listing->seller_id)
                        ->where('order_items.ASIN', $listing->asin)
                        ->where('order_items.SellerSKU', $listing->sku)
                        ->whereBetween('orders.PurchaseDate', [
                            \DT::convertToUTC ( date ( 'Y-m-d 00:00:00', strtotime ( $fromDate ) ) ),
                            \DT::convertToUTC ( date ( 'Y-m-d 23:59:59', strtotime ( $toDate ) ) ) ]
                        )
                        ->where('orders.SalesChannel', $marketplace)
                        ->where('orders.OrderStatus', '!=', 'Cancelled')
                        ->get();
            $orderItemsByDate = $records->groupBy(function ($item, $key) {
                return Carbon::parse($item->PurchaseDate, 'UTC')->tz(config('timezone'))->format('Y-m-d');
            });
            foreach ($orderItemsByDate as $date=>$orderItems) {
                $fba_gross_sales = $fbm_gross_sales = 0;
                $fba_shipping = $fbm_shipping = 0;
                $fba_refer_fee = $fbm_refer_fee = 0;
                $fba_units = $fbm_units = 0;
                $fba_orders = $fbm_orders = 0;
                $fba_refunded_units = $fbm_refunded_units = 0;
                $fba_shipping = $fbm_shipping = 0;
                $fba_promo_orders = $fbm_promo_orders = 0;
                $fba_promo_value = $fbm_promo_value = 0;
                $fba_other_fee = $fbm_other_fee = 0;
                $fba_tax = $fbm_tax = 0;
                $fba_vat_fee = $fbm_vat_fee = 0;
                $fba_refund = $fbm_refund = 0;
                $fba_giftwrap_fee = $fbm_giftwrap_fee = 0;
                $fba_orders_collect = collect([]);
                $fbm_orders_collect = collect([]);
                $fba_fee = 0;
                
                $promo_units = 0;

                foreach ($orderItems as $orderItem) {
                    if ($orderItem->FulfillmentChannel == 'AFN') {
                        $fba_orders_collect->push($orderItem->AmazonOrderId);
                        $fba_units += $orderItem->QuantityOrdered;
                        if ($orderItem->refund_item_quantity) {
                            $fba_refunded_units += $orderItem->refund_item_quantity;
                        }
                        $fba_gross_sales += ($orderItem->ItemPrice + $orderItem->chargeback) * $orderItem->exchange_rate;
                        $fba_shipping += $orderItem->ShippingPrice * $orderItem->exchange_rate;
                        $fba_fee += $orderItem->fba_fee * $orderItem->exchange_rate;
                        $fba_refer_fee += $orderItem->commission * $orderItem->exchange_rate;
                        $fba_promo_value += $orderItem->total_promo * $orderItem->exchange_rate;
                        $fba_other_fee += $orderItem->other_fee * $orderItem->exchange_rate;
                        $fba_tax += $orderItem->tax * $orderItem->exchange_rate;
                        $fba_vat_fee += $orderItem->vat_fee * $orderItem->exchange_rate;
                        $orderItem->total_promo != 0 ? $fba_promo_orders += 1 : '';
                        $fba_refund += $orderItem->total_refund;
                        $fba_giftwrap_fee += $orderItem->giftwrap_fee * $orderItem->exchange_rate;
                    }
                    if ($orderItem->FulfillmentChannel == 'MFN') {
                        $fbm_orders_collect->push($orderItem->AmazonOrderId);
                        $fbm_units += $orderItem->QuantityOrdered;
                        if ($orderItem->refund_item_quantity) {
                            $fbm_refunded_units += $orderItem->refund_item_quantity;
                        }
                        $fbm_gross_sales += ($orderItem->ItemPrice + $orderItem->chargeback) * $orderItem->exchange_rate;
                        $fbm_shipping += $orderItem->ShippingPrice * $orderItem->exchange_rate;
                        $fbm_refer_fee += $orderItem->commission * $orderItem->exchange_rate;
                        $fbm_promo_value += $orderItem->total_promo * $orderItem->exchange_rate;
                        $fbm_other_fee += $orderItem->other_fee * $orderItem->exchange_rate;
                        $fbm_tax += $orderItem->tax * $orderItem->exchange_rate;
                        $fbm_vat_fee += $orderItem->vat_fee * $orderItem->exchange_rate;
                        $orderItem->total_promo != 0 ? $fbm_promo_orders += 1 : '';
                        $fbm_giftwrap_fee += $orderItem->giftwrap_fee * $orderItem->exchange_rate;
                        $fbm_refund += $orderItem->total_refund;
                    }

                }
                $fba_orders = $fba_orders_collect->unique()->count();
                $fbm_orders = $fbm_orders_collect->unique()->count();

                $product = Product::find($listing->product_id);
                $ps = ProductStatistic::firstOrNew(['product_id' => $product->id, 'listing_id' => $listing->id, 'date' => $date]);
                $adData = self::getAdsDataForListing($listing, $date, $date);
                $ad_orders = empty($adData->ad_orders) ? 0 : $adData->ad_orders;
                $ad_cost = empty($adData->ad_cost) ? 0 : $adData->ad_cost;
                $ps->fill([
                    'asin' => $listing->asin,
                    'sku' => $listing->sku,
                    'country' => $listing->country,
                    'ad_orders' =>  $ad_orders,
                    'ads_cost' =>  $ad_cost,
                    'seller_id' =>  $listing->seller_id,
                    'fba_orders' =>  $fba_orders,
                    'fbm_orders' =>  $fbm_orders,
                    'fba_units' =>  $fba_units,
                    'fbm_units' =>  $fbm_units,
                    'fba_gross_sales' =>  $fba_gross_sales,
                    'fbm_gross_sales' =>  $fbm_gross_sales,
                    'fba_refer_fee' => $fba_refer_fee,
                    'fbm_refer_fee' => $fbm_refer_fee,
                    'fba_shipping' =>  $fba_shipping,
                    'fbm_shipping' =>  $fbm_shipping,
                    'fba_refund' => $fba_refund,
                    'fbm_refund' => $fbm_refund,
                    'fba_refunded_units' =>  $fba_refunded_units,
                    'fbm_refunded_units' =>  $fbm_refunded_units,
                    'fba_promo_orders' => $fba_promo_orders,
                    'fbm_promo_orders' => $fbm_promo_orders,
                    'fba_promo_value' => $fba_promo_value,
                    'fbm_promo_value' => $fbm_promo_value,
                    'fba_other_fee' => $fba_other_fee,
                    'fbm_other_fee' => $fbm_other_fee,
                    'fba_tax' => $fba_tax,
                    'fbm_tax' => $fbm_tax,
                    'fba_vat_fee' => $fba_vat_fee,
                    'fbm_vat_fee' => $fbm_vat_fee,
                    'fba_giftwrap_fee' => $fba_giftwrap_fee,
                    'fbm_giftwrap_fee' => $fbm_giftwrap_fee
                ]);
                

                $productCost = $product->productCostInDate($date);
                $shippingCost = $product->shippingCostInDate($date);
                $fbmShippingCost = $product->fbmShippingCostInDate($date);
                $ps->product_unit_cost = empty($productCost) ? 0 : $productCost->cost_per_unit;
                $ps->unit_ship_to_amz_cost = empty($shippingCost) ? 0 : $shippingCost->cost_per_unit;
                $ps->fbm_unit_shipping_cost = empty($fbmShippingCost) ? 0 : $fbmShippingCost->cost_per_unit;
                $ps->fba_fee = $fba_fee;
                $ps->save();
            }
        }
    }

    public static function aggregateFBAOrders($orders) {
        $result = [];
        $ordersByDate = DataHelper::groupByDate($orders, 'PurchaseDate', config('app.timezone'));
        foreach ($ordersByDate as $date => $orderItems) {
            $resultEntry = [];
            foreach (self::aggregateOrders($orderItems) as $k => $v) {
                $resultEntry['fba_' . $k] = $v;
            }
            $resultEntry['fba_fee'] = DataHelper::aggregate($orderItems, function($orderItem) {
                return $orderItem->fba_fee * $orderItem->exchange_rate;
            }, 'sum');

            $result[$date] = $resultEntry;
        }
        return $result;
    }

    public static function aggregateFBMOrders($orders) {
        $result = [];
        $ordersByDate = DataHelper::groupByDate($orders, 'PurchaseDate', config('app.timezone'));
        foreach ($ordersByDate as $date => $orderItems) {
            $resultEntry = [];
            foreach (self::aggregateOrders($orderItems) as $k => $v) {
                $resultEntry['fbm_' . $k] = $v;
            }
            $result[$date] = $resultEntry;
        }

        return $result;
    }

    public static function aggregateOrders($orderItems) {
        $result = [];
        $aggregation = Order::aggregateOrders($orderItems);
        foreach (self::$orderAggregationMapping as $k => $v) {
            $result[$k] = $aggregation[$v];
        }

        return $result;
    }

    public static function getDefaultFBAAggregation() {
        return [
            'fba_orders' => 0,
            'fba_units' => 0,
            'fba_gross_sales' => 0,
            'fba_refer_fee' => 0,
            'fba_shipping' => 0,
            'fba_refund' => 0,
            'fba_refunded_units' => 0,
            'fba_promo_orders' => 0,
            'fba_promo_value' => 0,
            'fba_other_fee' => 0,
            'fba_tax' => 0,
            'fba_vat_fee' => 0,
            'fba_giftwrap_fee' => 0,
            'fba_pending_orders' => 0
        ];
    }

    public static function getDefaultFBMAggregation() {
        return [
            'fbm_orders' => 0,
            'fbm_units' => 0,
            'fbm_gross_sales' => 0,
            'fbm_refer_fee' => 0,
            'fbm_shipping' => 0,
            'fbm_refund' => 0,
            'fbm_refunded_units' => 0,
            'fbm_promo_orders' => 0,
            'fbm_promo_value' => 0,
            'fbm_other_fee' => 0,
            'fbm_tax' => 0,
            'fbm_vat_fee' => 0,
            'fbm_giftwrap_fee' => 0,
            'fbm_pending_orders' => 0
        ];
    }

    public static function getDefaultProductDailyProfit() {
        return [
            'profit' => 0,
            'orders' => 0,
            'total_gross' => 0,
            'refund' => 0,
            'payout' => 0,
            'product_cost' => 0,
            'ad_expenses' => 0,
            'total_cost' => 0,
            'total_sessions' => 0,
            'unit_session_percentage' => 0,
        ];
    }

    public static function getDefaultPmMonthlyProfit() {
        return [
            'profit' => 0,
            'orders' => 0,
            'total_gross' => 0,
            'refund' => 0,
            'payout' => 0,
            'product_cost' => 0,
            'ad_expenses' => 0,
            'total_cost' => 0,
        ];
    }

    public static function processStatistic($model) {
        $model->total_units = $model->fba_units + $model->fbm_units;
        $model->total_gross_sales = $model->fba_gross_sales + $model->fbm_gross_sales;
        $model->total_shipping = $model->fba_shipping + $model->fbm_shipping;
        $model->total_refer_fee = $model->fba_refer_fee + $model->fbm_refer_fee;
        $model->total_orders = $model->fba_orders + $model->fbm_orders;
        $model->total_refunded_units = $model->fba_refunded_units + $model->fbm_refunded_units;
        $model->total_shipping = $model->fba_shipping + $model->fbm_shipping;
        $model->total_orders = $model->fba_orders + $model->fbm_orders;
        $model->organic_orders = $model->total_orders - $model->ad_orders;
        $model->total_promo_orders = $model->fba_promo_orders + $model->fbm_promo_orders;
        $model->total_promo_value = $model->fba_promo_value + $model->fbm_promo_value;
        $model->total_other_fee = $model->fba_other_fee + $model->fbm_other_fee;
        $model->total_tax = $model->fba_tax + $model->fbm_tax;
        $model->total_vat_fee = $model->fba_vat_fee + $model->fbm_vat_fee;
        $model->total_refund = $model->fba_refund + $model->fbm_refund;

        $model->fba_revenue = (
            $model->fba_gross_sales + 
            $model->fba_shipping + 
            $model->fba_vat_fee + 
            $model->fba_fee + 
            $model->fba_refer_fee + 
            $model->fba_promo_value + 
            $model->fba_refund + 
            $model->fba_tax + 
            $model->fba_giftwrap_fee + 
            $model->fba_other_fee
        );
        $model->fbm_revenue = (
            $model->fbm_gross_sales +
            $model->fbm_shipping + 
            $model->fbm_vat_fee + 
            $model->fbm_refer_fee + 
            $model->fbm_promo_value + 
            $model->fbm_refund + 
            $model->fbm_tax + 
            $model->fbm_giftwrap_fee + 
            $model->fbm_other_fee
        );

        $model->total_revenue = $model->fba_revenue + $model->fbm_revenue;
        $model->fba_cost = $model->fba_units * ($model->product_unit_cost + $model->unit_ship_to_amz_cost);
        $model->fbm_cost = $model->fbm_units * ($model->product_unit_cost + $model->unit_ship_to_amz_cost + $model->fbm_unit_shipping_cost);
        $model->total_cost = $model->fba_cost + $model->fbm_cost;
        $model->total_profit = (
            $model->total_revenue - 
            $model->ads_cost - 
            $model->total_cost - 
            $model->total_tax
        );
        $model->total_giftwrap_fee = $model->fba_giftwrap_fee + $model->fbm_giftwrap_fee;
    }

    public static function boot()
    {
        parent::boot();

        self::saving(function($model) { 
            self::processStatistic($model);
        });
    }
}

