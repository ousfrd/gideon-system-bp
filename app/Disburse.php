<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\DataHelper;


class Disburse extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'disburses';

	protected $fillable = [
		'amount', 'total_balance', 'unavailable_balance', 'request_transfer_available',
		'instant_transfer_balance', 'disburse_date', 'seller_id', 'marketplace',
		'message'
	];

	public function account() {
		return $this->belongsTo('App\Account', 'seller_id', 'seller_id');
	}

	public static function getDisburses($params) {
		$queryCond = self::getQuery($params);
		return self::getDisbursesFromQuery($queryCond);
	}

	public static function getQuery($params) {
		$query = [];

		if (isset($params['seller_id']) && !empty($params['seller_id'])) {
			$query[] = ['seller_id', '=', $params['seller_id']];
		}

		if (isset($params['marketplace']) && !empty($params['marketplace'])) {
			$query[] = ['marketplace', '=', strtoupper($params['marketplace'])];
		}

		$now = \Carbon\Carbon::now('UTC');
		$fromDate = NULL;
		$toDate = NULL;
		if (isset($params['date']) && !empty($params['date'])) {
			try {
				$fromDate = $toDate = \Carbon\Carbon::createFromFormat(
					'Y-m-d', $params['date']);
			} catch (\Exception $e) {
				$fromDate = $toDate = $now;
			}
		} else {
			if (isset($params['startDate']) && !empty($params['startDate'])) {
				try {
					$fromDate = \Carbon\Carbon::createFromFormat('Y-m-d', $params['startDate']);
				} catch (\Exception $e) {
					$fromDate = $now;
				}
			}
			if (isset($params['endDate']) && !empty($params['endDate'])) {
				try {
					$toDate = \Carbon\Carbon::createFromFormat('Y-m-d', $params['endDate']);
				} catch (\Exception $e) {
					$toDate = $now;
				}
			}

			if (empty($fromDate)) {
				$fromDate = $now;
			}
			if (empty($toDate)) {
				$toDate = $now;
			}
		}
		$fromDateStr = $fromDate->copy()->startOfDay()->format('Y-m-d H:i:s');
		$toDateStr = $toDate->copy()->endOfDay()->format('Y-m-d H:i:s');
		$query[] = ['disburse_date', '>=', $fromDateStr];
		$query[] = ['disburse_date', '<=', $toDateStr];

		return $query;
	}

	public static function getDisbursesFromQuery($query) {
		return self::where($query)->get();
	}

	public static function audit($disburses) {
		$result = [];

		$groupedDisburses = DataHelper::groupByVal($disburses, function ($disburse) {
			return sprintf('%s-%s', $disburse->seller_id, $disburse->marketplace);
		});
		foreach ($groupedDisburses as $key => $disbursesBySeller) {
			$amount = DataHelper::aggregate(
				$disbursesBySeller, 'amount', 'sum');

			usort($disbursesBySeller, function ($a, $b) {
				return strtotime($a->disburse_date) - strtotime($b->disburse_date);
			});
			$disburse = end($disbursesBySeller);
			$entry = (object) [
				'code' => $disburse->account->code,
				'name' => $disburse->account->name,
				'sellerId' => $disburse->seller_id,
				'marketplace' => $disburse->marketplace,
				'amount' => $amount,
				'totalBalance' => $disburse->total_balance,
				'unavailableBalance' => $disburse->unavailable_balance,
				'instantTransferBalance' => $disburse->instant_transfer_balance,
				'requestTransferAvailable' => $disburse->request_transfer_available,
			];

			$result[] = $entry;
		}

		usort($result, function ($a, $b) {
			return intval($a->code) - intval($b->code);
		});

		return $result;
	}
}
