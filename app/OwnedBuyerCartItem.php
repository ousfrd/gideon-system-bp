<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OwnedBuyerCartItem extends Model
{
    protected $fillable = ['asin', 'owned_buyer_id'];

    public function ownedBuyer() 
    {
        return $this->belongsTo('App\OwnedBuyer');
    }

    public static function gridQuery()
    {
        $query = self::select(
            'owned_buyer_cart_items.*', 
            'owned_buyers.nickname', 
            'owned_buyers.device_name', 
            'owned_buyers.email'
            )->join('owned_buyers', 'owned_buyer_cart_items.owned_buyer_id', '=', 'owned_buyers.id');
        return $query;
    }

    public static function boot()
    {
        parent::boot();

        self::created(function($model) {
            $model->ownedBuyer->increment('cart_items_count');
        });

        self::deleted(function($model) {
            $model->ownedBuyer->decrement('cart_items_count');
        });
    }
}
