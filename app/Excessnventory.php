<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
class Excessnventory extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'product_excess_inventory';
	
	
	function listing() {
		return $this->belongsTo('App\Listing','listing_id','id');
	}
	
	function merchant(){
		return $this->belongsTo('App\Account','seller_id','seller_id');
	}
	
	static function total() {
		$query = self::where('date','>',date('Y-m-d',strtotime('-1 days')));
		
		if(auth()->user()->role == "product manager") {
			$query->whereIn('asin',auth()->user()->productList());
		}elseif(auth()->user()->role == "seller account manager") {
			$query->whereIn('seller_id',auth()->user()->accountList());
		}
		
		$count = $query->count();
		
		return $count;
	}
	
	static function listByUser(User $user) {
		$query = self::where('date','>=',date('Y-m-d',strtotime('-1 days')));
		
		if(in_array($user->role,["product manager","admin"] )) {
			$query->whereIn('asin',$user->productList());
		}elseif($user->role == "seller account manager") {
			$query->whereIn('seller_id',$user->accountList());
		}
		
		return $query->get();
		
	}
}

