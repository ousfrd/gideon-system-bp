<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DivvyTransactionRecord;
use App\ManagedReviewOrder;
use Carbon\Carbon;

class OwnedBuyerPaymentMethod extends Model
{
    const MONTHLYRESETBALANCE = 100;

    protected $fillable = ['owned_buyer_id', 'card_number', 'bank_name', 'available_balance'];

    public function getCardLast4Digits() {
        $cardNumber = $this->card_number;
        if ($cardNumber && preg_match('/\d{4}\s*$/', $cardNumber)) {
            return substr(trim($cardNumber), -4);
        } else {
            return null;
        }
    }

    public static function updateCostFromTransactionRecord(){
        $records = DivvyTransactionRecord::selectRaw('card_last_4, sum(amount_in_number) as cost')
                                ->groupBy('card_last_4')
                                ->pluck('cost', 'card_last_4');
        foreach($records as $last4 => $cost) {
            $paymentMethod = OwnedBuyerPaymentMethod::where('last_4_digits', $last4)->first();
            if ($paymentMethod) {
                $paymentMethod->total_spent = $cost;
                $paymentMethod->save();
            }
        }
        
    }

    public static function resetAvailableBalance() {
        OwnedBuyerPaymentMethod::query()->update(['available_balance' => self::MONTHLYRESETBALANCE]);
    }

    public static function recalculateAvailableBalance() {
        $paymentMethods = OwnedBuyerPaymentMethod::all();
        $startDate = (new Carbon('first day of this month'))->format('Y-m-d');
        foreach ($paymentMethods as $payment) {
            $ownedBuyerId = $payment->owned_buyer_id;
            $orderTotal = ManagedReviewOrder::where('owned_buyer_id',  $ownedBuyerId)->where('order_date', '>', $startDate)->sum('order_cost');
            $available = self::MONTHLYRESETBALANCE - $orderTotal;
            $payment->available_balance = $available;
            $payment->save();
        }
    }

    public static function boot()
    {
        parent::boot();

        self::saving(function($model) {
            if ($model->isDirty("card_number")) {
                $model->last_4_digits = $model->getCardLast4Digits();
            }
        });
    }

}
