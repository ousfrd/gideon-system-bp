<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
class InventoryHistory extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'product_inventory_history';

  public $timestamps = true;
  
  private $inventoryAttrNames = [
    "afn_warehouse_quantity" => "warehouse_quantity",
    "afn_fulfillable_quantity" => "fulfillable_quantity",
    "afn_unsellable_quantity" => "unsellable_quantity",
    "afn_reserved_quantity" => "reserved_quantity",
    "afn_inbound_working_quantity" => "inbound_working_quantity",
    "afn_inbound_shipped_quantity" => "inbound_shipped_quantity",
    "afn_inbound_receiving_quantity" => "inbound_receiving_quantity",
    "mfn_listing_exists" => "mfn_listing_exists",
    "afn_listing_exists" => "afn_listing_exists",
    "mfn_fulfillable_quantity" => "mfn_fulfillable_quantity",
    "afn_total_quantity" => "total_quantity"
  ];
	
	function product() {
		return $this->belongsTo('App\Product','product_id','id');
  }
  
  function listing() {
    return $this->belongsTo('App\Listing','listing_id','id');
  }
  
  static function saveFromInventory($inventory) {
    $attributes = $inventory->getAttributes();
    $attributes["id"] = null;
    $currentDate = date("Y-m-d");
    $inventoryHistory = InventoryHistory::where("import_date",$currentDate)
                                          ->where("sku", $inventory->sku)
                                          ->where("asin", $inventory->asin)
                                          ->where("item_condition", $inventory->item_condition)
                                          ->first();
    if ($inventoryHistory === null) {
      $inventoryHistory = new InventoryHistory();
      $inventoryHistory->import_date = $currentDate;
    }
    foreach ($attributes as $field => $value) {
      if ($field != "id") {
        $inventoryHistory->$field = $value;
      }
    }
    $inventoryHistory->save();
  }

  static function saveFromListing($listing) {
    $currentDate = date("Y-m-d");
    $inventoryHistory = InventoryHistory::where("listing_id", $listing->listing_id)
                                          ->where("import_date", $currentDate)
                                          ->first();
    if ($inventoryHistory === null) {
      $inventoryHistory = new InventoryHistory();
      $inventoryHistory->product_id = $listing->product_id;
      $inventoryHistory->listing_id = $listing->id;
      $inventoryHistory->sku = $listing->sku;
      $inventoryHistory->asin = $listing->asin;
      $inventoryHistory->country = $listing->country;
      $inventoryHistory->seller_id = $listing->seller_id;
      $inventoryHistory->item_condition = $listing->item_condition;
      $inventoryHistory->price = $listing->sale_price;
      $inventoryHistory->import_date = $currentDate;
    }
    foreach ($inventoryHistory->inventoryAttrNames as $listingAttr => $inventoryHistoryAttr ) {
      $inventoryHistory->$inventoryHistoryAttr = $listing->$listingAttr;
    }
    $inventoryHistory->save();
  }
	
}

