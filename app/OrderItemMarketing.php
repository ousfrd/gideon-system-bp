<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

use App\Shipment;
use App\Traits\ReviewHandlable;


class OrderItemMarketing extends Model
{
    use ReviewHandlable;
    
    const STATUS_FIELDS = ["invite_review", "self_review", "email_promotion_review"];
    
    const SELF_BOUGHT_ORDER_SKU_PLACEHOLDER = "SELF_BOUGHT_ORDER_SKU_PLACEHOLDER";

    protected $fillable = ['oim_sku', 'oim_amazon_order_id', 'self_review', 'is_sent'];

    public function orderShipment() {
        return $this->belongsTo('App\Shipment', 'oim_amazon_order_id', 'amazon_order_id');
    }

    public function claimReviewCampaign() {
        return $this->belongsTo('App\ClaimReviewCampaign', 'claim_review_id');
    }

    public static function updateStatus($sku, $amazonOrderId, $field, $value) 
    {
        $orderItemMarketing = OrderItemMarketing::firstOrNew(["oim_sku" => $sku, "oim_amazon_order_id" => $amazonOrderId]);
        $orderItemMarketing->$field = $value;
        $orderItemMarketing->save();
    }

    public static function updateField($sku, $amazonOrderId, $field, $value) 
    {
        $orderItemMarketing = OrderItemMarketing::firstOrNew(["oim_sku" => $sku, "oim_amazon_order_id" => $amazonOrderId]);
        $orderItemMarketing->$field = $value;
        $orderItemMarketing->save();
    }

    public static function createForSelfReview($orderId, $sku = null)
    {
        $oims = OrderItemMarketing::where('oim_amazon_order_id', $orderId)->get();
        if (count($oims) > 0) {
            foreach ($oims as $oim) {
                $oim->self_review = true;
                $oim->save();
            }
        } else {
            $oim = new OrderItemMarketing();
            $oim->oim_amazon_order_id = $orderId;
            $oim->self_review = true;
            if (empty($sku)) {
                $oim->oim_sku = self::SELF_BOUGHT_ORDER_SKU_PLACEHOLDER;
                $oim->save();
            } else {
                $oim->syncWithShipment();
            }
        }
    }

    public static function syncSelfBoughtOrder()
    {
        $orders = OrderItemMarketing::where('oim_sku', self::SELF_BOUGHT_ORDER_SKU_PLACEHOLDER)->get();
        foreach ($orders as $order) {
            $order->syncWithShipment();
        }
    }

    public static function syncAll() {
        $orders = OrderItemMarketing::whereNull('buyer_id')->orWhereNull('order_date')->get();
        foreach ($orders as $order) {
            $order->syncWithShipment(false);
        }
    }

    public function syncWithShipment($selfReview = true)
    {
        $shipment = Shipment::where("amazon_order_id", $this->oim_amazon_order_id)->first();
        if ($shipment) {
            $this->oim_sku = $shipment->sku;
            $this->asin = $shipment->asin;
            $this->buyer_id = $shipment->buyer_email;
            $this->seller_id = $shipment->seller_id;
            if ($selfReview) {
                $this->self_review = $selfReview;
            }
            $this->order_date = $shipment->purchase_date;
            $this->save();
        }
    }

    public static function boot()
    {
        parent::boot();
        self::saving(function($model) {
            if ($model->isDirty('review_link')) {
                $orginal = $model->getOriginal('review_link');
                if (empty($orginal) && $model->review_link) {
                    $model->left_review_at = date('Y-m-d H:i:s');
                }
                if ($orginal && empty($model->review_link)) {
                    $model->left_review_at = null;
                }
            }
            if (
                $model->invite_review || 
                $model->self_review || 
                $model->email_promotion_review || 
                $model->insert_card_review ||
                $model->claim_review_id 
                ) {
                    $model->processed = true;
                    if (    
                        $model->isDirty('invite_review') || 
                        $model->isDirty('self_review') || 
                        $model->isDirty('email_promotion_review') ||
                        $model->isDirty('insert_card_review') ||
                        $model->isDirty('claim_review_id')
                    ) {
                        $model->operator_id = Auth::id();
                    }
                
            } else {
                $model->processed = false;
                $model->operator_id = null;
            }
        });
    }
}
