<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClaimReviewReturnAddress extends Model
{
    protected $guarded = ['id'];

    public static function getByPlatform($platform) {
    	return self::where('platform', $platform)->get();
    }

    public function format() {
    	return sprintf(
    		'%s<br />%s<br />%s, %s, %s',
    		$this->address_name, $this->address_line_1, $this->address_city,
    		$this->address_state, $this->address_postal_code
    	);
    }

    public function formatOneLine() {
    	return sprintf(
    		'%s, %s, %s, %s, %s',
    		$this->address_name, $this->address_line_1, $this->address_city,
    		$this->address_state, $this->address_postal_code
    	);
    }
}
