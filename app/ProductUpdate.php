<?php

namespace App;
use App\Product;
use Illuminate\Database\Eloquent\Model;

class ProductUpdate extends Model
{
    //
    protected $table = 'products';
    protected $fillable = ['asin','code','country','keywords','title','product_group','old_asin','type','brand','small_image','weight', 'sales_ranks'];
    static function updateInfo($asin, $brand, $small_image, $weight, $sales_ranks) {
		Product::where('asin', $asin)->update(['brand' => $brand, 'small_image' => $small_image, 'weight' => $weight, 'sales_ranks' => $sales_ranks]);
	}
}
