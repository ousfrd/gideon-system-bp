<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;
use Storage;

class Import extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'imports';

	protected $fillable = ['seller_id','type','filename','status','country','report_date', 'method'];

	const REPORT_TYPE_MAPPING = [
		'advertising_report' => 'Ads',
		'FBA_inventory_report' => 'FBA Inventory',
		'finance_report' => 'Finances',
		'listings_report' => 'Listings',
		'order_report' => 'Orders',
		'FBA_shipment_report' => 'OrderShipments'
	];

	function account() {
		return $this->belongsTo ( 'App\Account', 'seller_id', 'seller_id' );
	}

	public static function getImportedStatus()
	{
		return 'imported';
	}

	public static function deleteFiles() {
		$fromDate = Carbon::now()->subDays(30)->format('Y-m-d');
		$toDate = Carbon::now()->subDays(2)->format('Y-m-d');
		$imports = Import::whereBetween('created_at', [$fromDate, $toDate])->get();
		$imports->each(function($import) {
			$file = $import->filename;
			$exists = Storage::disk('dropbox')->exists($file);
			if ($exists) {
				Storage::disk('dropbox')->delete($file);
			}
		});
	}

	public static function getImports($params) {
		$queryCond = self::getQuery($params);
		return self::getImportsFromQuery($queryCond);
	}

	public static function getQuery($params) {
		$query = [['status', '=', 'imported']];

		if (isset($params['seller_id']) && !empty($params['seller_id'])) {
			$query[] = ['seller_id', '=', $params['seller_id']];
		}

		$reportType = NULL;
		if (isset($params['report_type']) && !empty($params['report_type'])) {
			$reportType = $params['report_type'];
			if (array_key_exists($reportType, self::REPORT_TYPE_MAPPING)) {
				$query[] = ['type', '=', self::REPORT_TYPE_MAPPING[$reportType]];
			}
		}

		if (isset($params['marketplace']) && !empty($params['marketplace'])) {
			if (!in_array($reportType, self::getReportTypesWithoutCountry())) {
				$query[] = ['country', '=', strtoupper($params['marketplace'])];
			}
		}

		if (isset($params['date']) && !empty($params['date'])) {
			try {
				$date = \Carbon\Carbon::createFromFormat('Y-m-d', $params['date']);
			} catch (Exception $e) {
				$date = \Carbon\Carbon::now('UTC');
			}
		} else {
			$date = \Carbon\Carbon::now('UTC');
		}
		$fromDateStr = $date->copy()->startOfDay()->format('Y-m-d H:i:s');
		$toDateStr = $date->copy()->endOfDay()->format('Y-m-d H:i:s');
		$query[] = ['created_at', '>=', $fromDateStr];
		$query[] = ['created_at', '<=', $toDateStr];

		return $query;
	}

	public static function getImportsFromQuery($query) {
		return Import::where($query)->get();
	}

	public static function getReportTypesWithoutCountry() {
		return ['order_report', 'FBA_shipment_report'];
	}
}

