<?php
namespace App\Traits;
use App\Helpers\ReviewHelper;

trait ReviewHandlable {
  public static function extractReviewIdForAll() {
    $models = parent::whereNotNull('review_link')->get();
    $models->each(function($model) {
        $model->review_id = ReviewHelper::extractId($model->review_link);
        $model->save();
    });
  }

  public static function bootReviewHandlable() {
    static::saving(function($model) {
      if ($model->isDirty('review_link')) {
          if ($model->review_link) {
              $model->review_id = ReviewHelper::extractId($model->review_link);
          } else {
              $model->review_id = null;
          }
      }
  });
  }
}

?>