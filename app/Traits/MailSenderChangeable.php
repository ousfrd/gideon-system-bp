<?php
namespace App\Traits;

trait MailSenderChangeable
{
    /**
     * @param array $settings
     */
    public function changeMailSender($settings)
    {
        $mailTransport = app()->make('mailer')->getSwiftMailer()->getTransport();
        if ($mailTransport instanceof \Swift_SmtpTransport) {
            /** @var \Swift_SmtpTransport $mailTransport */
            $mailTransport->setUsername($settings['username']);
            $mailTransport->setPassword($settings['password']);
            $mailTransport->setHost($settings['host']);
            $mailTransport->setPort($settings['port']);
        }
    }
}
