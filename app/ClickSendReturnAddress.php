<?php

namespace App;
use ClickSendClient;

use Illuminate\Database\Eloquent\Model;

class ClickSendReturnAddress extends Model
{
    protected $guarded = ['id'];

    public static function sync() {
        $addresses = ClickSendClient::getReturnAddresses();
        if (count($addresses) > 0 ) {
            self::truncate();
            foreach ($addresses as $address) {
                $model = new ClickSendReturnAddress($address);
                $model->save();
            }
        }
    }

    public function format() {
        return $this->address_name . "<br>" . 
               $this->address_line_1 . "<br>" .
               $this->address_city . "," . $this->address_state . "," . $this->address_postal_code; 
    }
}
