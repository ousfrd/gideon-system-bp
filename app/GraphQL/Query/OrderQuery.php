<?php
namespace App\GraphQL\Query;
use App\Order;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class OrderQuery extends Query
{
    protected $attributes = [
        'name' => 'Order Query',
        'description' => 'A query of order'
    ];

    public function type()
    {
        // result of query with pagination laravel
        return GraphQL::type('order');
    }

    // arguments to filter query
    public function args()
    {
        return [
            'amazon_order_id' => [
                'name' => 'amazon_order_id',
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, SelectFields $fields)
    {

        if (isset($args['amazon_order_id'])) {

            $order = Order::with($fields->getRelations())
                ->where('AmazonOrderId',$args['amazon_order_id'])
                ->where('OrderStatus', 'Shipped')
                ->select($fields->getSelect())
                ->first();
   
            return $order;  

        } else {
            return null;
        }
    }
}