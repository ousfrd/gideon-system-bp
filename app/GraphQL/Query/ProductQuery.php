<?php
namespace App\GraphQL\Query;
use App\Product;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class ProductQuery extends Query
{
    protected $attributes = [
        'name' => 'Product Query',
        'description' => 'A query of product'
    ];

    public function type()
    {
        // result of query with pagination laravel
        return GraphQL::type('product');
    }

    // arguments to filter query
    public function args()
    {
        return [
            'asin' => [
                'name' => 'asin',
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, SelectFields $fields)
    {

        if (isset($args['asin'])) {

            $product = Product::with($fields->getRelations())
                ->where('asin',$args['asin'])
                ->where('country', 'US')
                ->select($fields->getSelect())
                ->first();
   
            return $product;  

        } else {
            return null;
        }
    }
}