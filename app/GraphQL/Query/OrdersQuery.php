<?php
namespace App\GraphQL\Query;
use App\Order;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class OrdersQuery extends Query
{
    protected $attributes = [
        'name' => 'Orders Query',
        'description' => 'A query of orders'
    ];
    public function type()
    {
        // result of query with pagination laravel
        return Type::listOf(GraphQL::type('order'));
    }
    
    // arguments to filter query
    public function args()
    {
        return [
            'amazon_order_id' => [
                'name' => 'amazon_order_id',
                'type' => Type::string()
            ]
        ];
    }

    public function resolve($root, $args, SelectFields $fields)
    {
        if (isset($args['amazon_order_id'])) {
            $select = $fields->getSelect();
            $with = $fields->getRelations();
            
            $orders = Order::select($select)->with($with)->where('AmazonOrderId',$args['amazon_order_id']);
            
            return $orders->get();
        } else {
            return null;
        }
    }
}