<?php
namespace App\GraphQL\Query;
use App\Product;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class ProductsQuery extends Query
{
    protected $attributes = [
        'name' => 'Products Query',
        'description' => 'A query of products'
    ];

    public function type()
    {
        // result of query with pagination laravel
        return Type::listOf(GraphQL::type('product'));
    }

    // arguments to filter query
    public function args()
    {
        return [
        ];
    }

    public function resolve($root, $args, SelectFields $fields)
    {

            $products = Product::with($fields->getRelations())
                ->where('is_newrelease', 1)
                ->select($fields->getSelect())
                ->get();
   
            return $products;  

    }
}