<?php

namespace App\GraphQL\Mutation;

use App\Import;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;    

class AddImportMutation extends Mutation
{
    protected $attributes = [
        'name' => 'AddImport'
    ];

    public function type()
    {
        return GraphQL::type('import');
    }

    public function args()
    {
        return [
            'seller_id' => ['name' => 'seller_id', 'type' => Type::nonNull(Type::string())],
            'type' => ['name' => 'type', 'type' => Type::string()],
            'filename' => ['name' => 'filename', 'type' => Type::string()],
            'country' => ['name' => 'country', 'type' => Type::string()],
            'report_date' => ['name' => 'report_date', 'type' => Type::string()],
            'method' => ['name' => 'method', 'type' => Type::string()],
        ];
    }

    public function resolve($root, $args)
    {
        $import = Import::create($args);
        if(!$import) {
            return null;
        }

        return $import;
    }
}