<?php

namespace App\GraphQL\Mutation;

use App\Redeem;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;    

class AddRedeemMutation extends Mutation
{
    protected $attributes = [
        'name' => 'AddRedeem'
    ];

    public function type()
    {
        return GraphQL::type('redeem');
    }

    public function args()
    {
        return [
            'requestDate' => ['name' => 'requestDate', 'type' => Type::nonNull(Type::string())],
            'seller_id' => ['name' => 'seller_id', 'type' => Type::nonNull(Type::string())],
            'AmazonOrderId' => ['name' => 'AmazonOrderId', 'type' => Type::nonNull(Type::string())],
            'asin' => ['name' => 'asin', 'type' => Type::string()],
            'country' => ['name' => 'country', 'type' => Type::string()],
            'source' => ['name' => 'source', 'type' => Type::string()],
            'amount' => ['name' => 'amount', 'type' => Type::string()],
            'usingTime' => ['name' => 'usingTime', 'type' => Type::nonNull(Type::string())],
            'star' => ['name' => 'star', 'type' => Type::nonNull(Type::int())],
            'how_to_help' => ['name' => 'how_to_help', 'type' => Type::string()],
            'new_address' => ['name' => 'new_address', 'type' => Type::string()],
            'name' => ['name' => 'name', 'type' => Type::nonNull(Type::string())],
            'email' => ['name' => 'email', 'type' => Type::nonNull(Type::string())],
            'newsletter' => ['name' => 'newsletter', 'type' => Type::boolean()],
            'attachments' => ['name' => 'attachments', 'type' => Type::string()],
        ];
    }

    public function resolve($root, $args)
    {
        $redeem = Redeem::create($args);
        if(!$redeem) {
            return null;
        }

        return $redeem;
    }
}