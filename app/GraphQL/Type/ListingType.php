<?php
namespace App\GraphQL\Type;
use App\Listing;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ListingType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Listing',
        'description' => 'A type',
        'model' => Listing::class, // define model for users type
    ];
    
    // define field of type
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the listing'
            ],
            'asin' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The listing asin'
            ],
            'country' => [
                'type' => Type::string(),
                'description' => 'The listing country'
            ],
            'sku' => [
                'type' => Type::string(),
                'description' => 'The listing sku'
            ],
            'price' => [
                'type' => Type::Float(),
                'description' => 'The price of listing'
            ],
            'sale_price' => [
                'type' => Type::Float(),
                'description' => 'The sale price of listing'
            ],
            'status' => [
                'type' => Type::Boolean(),
                'description' => 'The status of listing'
            ]
        ];
    }
}