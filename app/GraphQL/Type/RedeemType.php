<?php
namespace App\GraphQL\Type;
use App\Redeem;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class RedeemType extends GraphQLType
{
    protected $attributes = [
        'name' => 'OrderRedeem',
        'description' => 'A type',
        'model' => Redeem::class, // define model for users type
    ];
    
    // define field of type
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the order item'
            ],
            'seller_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The amazon order seller id'
            ],
            'AmazonOrderId' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The amazon order id'
            ],
            'asin' => [
                'type' => Type::string(),
                'description' => 'The asin'
            ],
            'requestDate' => [
                'type' => Type::string(),
                'description' => 'The requested date'
            ]
        ];
    }
}