<?php
namespace App\GraphQL\Type;
use App\AsinWatcher;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class AsinWatcherType extends GraphQLType
{
    protected $attributes = [
        'name' => 'AsinWatcher',
        'description' => 'A type',
        'model' => AsinWatcher::class, // define model for users type
    ];
    
    // define field of type
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the asin watcher'
            ],
            'asin' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The product asin'
            ],
            'country' => [
                'type' => Type::string(),
                'description' => 'The product country'
            ],
            'date' => [
                'type' => Type::string(),
                'description' => 'The asin watcher date'
            ],
            'price' => [
                'type' => Type::Float(),
                'description' => 'The price of product'
            ]
        ];
    }
}