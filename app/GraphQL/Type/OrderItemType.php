<?php
namespace App\GraphQL\Type;
use App\OrderItem;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class OrderItemType extends GraphQLType
{
    protected $attributes = [
        'name' => 'OrderItem',
        'description' => 'A type',
        'model' => OrderItem::class, // define model for users type
    ];
    
    // define field of type
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the order item'
            ],
            'AmazonOrderId' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The amazon order id'
            ],
            'ASIN' => [
                'type' => Type::string(),
                'description' => 'The asin'
            ],
            'country' => [
                'type' => Type::string(),
                'description' => 'The country'
            ],
            'ItemPriceUSD' => [
                'type' => Type::Float(),
                'description' => 'The item price'
            ],
        ];
    }
}