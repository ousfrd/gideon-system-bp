<?php
namespace App\GraphQL\Type;
use App\Order;
use App\OrderItemMarketing;
use App\ClaimReviewCampaign;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class OrderType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Order',
        'description' => 'A type',
        'model' => Order::class, // define model for users type
    ];
    
    // define field of type
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the order'
            ],
            'AmazonOrderId' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The amazon order id'
            ],
            'seller_id' => [
                'type' => Type::string(),
                'description' => 'The seller id of the order'
            ],
            'BuyerName' => [
                'type' => Type::string(),
                'description' => 'The buyer name of the order'
            ],
            'BuyerEmail' => [
                'type' => Type::string(),
                'description' => 'The buyer email of the order'
            ],
            'items' => [
                'type' => Type::listOf(GraphQL::type('order_item')),
                'description' => 'The order items of the order'
            ],
            'redeem' => [
                'type' => GraphQL::type('redeem'),
                'description' => 'The redeem of the order'
            ],
            'reward' => [
                'type' => Type::int(),
                'description' => 'reward for this order',
                'selectable'=> false,
                'resolve' => function($root, $args) {
                    $orderItemMarketing = $root->orderItemMarketing;
                    if ($orderItemMarketing && $campaign = $orderItemMarketing->claimReviewCampaign) {
                        return $campaign->cashback;
                    } else {
                        return null;
                    }
                }
            ]
        ];
    }
}