<?php
namespace App\GraphQL\Type;
use App\Product;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Product',
        'description' => 'A type',
        'model' => Product::class, // define model for users type
    ];
    
    // define field of type
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the product'
            ],
            'asin' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The product asin'
            ],
            'country' => [
                'type' => Type::string(),
                'description' => 'The product country'
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The product name'
            ],
            'small_image' => [
                'type' => Type::string(),
                'description' => 'The product image'
            ],
            'is_newrelease' => [
                'type' => Type::boolean(),
                'description' => 'If the product is new release'
            ],
            'active_listings' => [
                'type' => Type::listOf(GraphQL::type('listing')),
                'description' => 'The active listings of the product'
            ],
            'asin_watchers' => [
                'type' => Type::listOf(GraphQL::type('asin_watcher')),
                'description' => 'The asin watchers of the product'
            ]
        ];
    }
}