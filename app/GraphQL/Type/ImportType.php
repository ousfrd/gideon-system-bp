<?php
namespace App\GraphQL\Type;
use App\Import;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ImportType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Import',
        'description' => 'A type',
        'model' => Import::class, // define model for users type
    ];
    
    // define field of type
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the import'
            ],
            'seller_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The amazon order seller id'
            ],
            'type' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The type of import'
            ],
            'filename' => [
                'type' => Type::string(),
                'description' => 'The filename'
            ],
            'country' => [
                'type' => Type::string(),
                'description' => 'The country'
            ],
            'report_date' => [
                'type' => Type::string(),
                'description' => 'report date'
            ],
            'method' => [
                'type' => Type::string(),
                'description' => 'report method'
            ]

        ];
    }
}