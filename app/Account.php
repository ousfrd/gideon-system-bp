<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use AmazonAdvertisingApi\Client;
use App\Traits\MassInsertOrUpdate;
use App\Helpers\CurrencyHelper;

class Account extends Model
{

	use MassInsertOrUpdate;

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'seller_accounts';

	protected $fillable = ['name', 'code', 'seller_id', 'region'];
	
	//protected $fillable = ['asin','code','country','keywords','title','product_group','old_asin'];
// 	public $timestamps = false;
	
	protected $amzLinks = [
			'US'=>'https://www.amazon.com/',
			'CA'=>'https://www.amazon.ca/',
			'MX'=>'https://www.amazon.com.mx/',
			'UK'=>'https://www.amazon.co.uk/',
			'DE'=>'https://www.amazon.de/',
			'ES'=>'https://www.amazon.es/',
			'FR'=>'https://www.amazon.fr/',
			'IT'=>'https://www.amazon.it/',
			'JP'=>'https://www.amazon.co.jp/',
			'AU'=>'https://www.amazon.com.au/',
	];

	public function disburses() {
		return $this->hasMany('App\Disburse', 'seller_id', 'seller_id');
	}
	
	public static function getSortedAccounts()
	{
		return self::orderBy('status', 'asc')->orderBy('code', 'asc')->get();
	}

	public static function getAccountBySellerId($sellerId) {
		return self::where('seller_id', $sellerId)->first();
	}
	
	function schedules() {
		return $this->hasMany('App\PurchaseSchedule','seller_account','code');
	}
	
	function users() {
		return $this->belongsToMany('App\User', 'seller_account_users',  'seller_account_id','user_id');
	}

	function listings() {
		return $this->hasMany('App\Listing','seller_id','seller_id');
	}

	function managerList() {
		return $this->users()->pluck('users.name','users.id')->toArray();
	}
	
	function adReportRequests() {
		return $this->hasMany('App\AdReportRequest','seller_id','seller_id');
	}	

	function adSnapshots() {
		return $this->hasMany('App\AdSnapshot','seller_id','seller_id');
	}	

	function adCampaigns() {
		return $this->hasMany('App\AdCampaign','seller_id','seller_id');
	}	

	function adGroups() {
		return $this->hasMany('App\AdGroup','seller_id','seller_id');
	}	

	function productAds() {
		return $this->hasMany('App\ProductAd','seller_id','seller_id');
	}	

	function adKeywords() {
		return $this->hasMany('App\AdKeyword','seller_id','seller_id');
	}	

	function redeems() {
		return $this->hasMany('App\Redeem','seller_id','seller_id');
	}

	function settlements() {
		return $this->hasMany('App\Settlement', 'account_code', 'code');
	}
	
	function amazonLink() {
		return $this->amzLinks[strtoupper($this->country)].'gp/aag/main?&seller='.$this->seller_id;
	}

	function getAmzSellerLinksAttribute() {
		$result = [];
		if ($this->marketplaces) {
			$marketplaces =  explode(',', $this->marketplaces);
			foreach ($marketplaces as $marketplace) {
				$link = $this->amzLinks[$marketplace].'sp?seller=' . $this->seller_id;
				$result[$marketplace] = $link;
			}
		}
		return $result;
		
	}



	static function groups() {
		return self::distinct()->get(['group'])->pluck('group','group')->toArray();
	}

	function getProfileByCountry($country) {
		$profile_ids = json_decode($this->adv_profile_ids, false, 512, JSON_BIGINT_AS_STRING);
		if(empty($profile_ids)) {
			return "";
		}
		foreach ($profile_ids as $profile) {
			if($country == $profile->countryCode) {
				return $profile;
			}
		}
	}
	
	function updateProfiles($client) {
		// get profile lists
		try{
			$client->doRefreshToken();
			$res = $client->listProfiles();
		} catch( \Exception $e ) {
			var_dump($e->getMessage());
			return $this->adv_profile_ids;
		}

		if($res['success']) {
			$profiles = json_decode($res['response'], false, 512, JSON_BIGINT_AS_STRING);

			$seller_profiles = [];
			// in case one seller account has many country and profile id
			foreach ($profiles as $profile) {
				$profileId = $profile->profileId;
				$countryCode = $profile->countryCode;
				$currencyCode = $profile->currencyCode;
				$dailyBudget = isset($profile->dailyBudget) ? $profile->dailyBudget : 999999999;
				$timezone = $profile->timezone;
				$marketplaceId = $profile->accountInfo->marketplaceStringId;
				$seller_id = $profile->accountInfo->id;

				$seller_profiles[$seller_id][] = compact('profileId', 'countryCode', 'currencyCode', 'dailyBudget', 'timezone', 'marketplaceId', 'seller_id');
			}

			// save seller profile ids
			foreach ($seller_profiles as $id => $list) {
				if($id == $this->seller_id) {
					$adv_profile_ids = json_encode($list);
					break;				
				}
			}
		}

		if(isset($adv_profile_ids)) {
			$this->adv_profile_ids = $adv_profile_ids;
			$this->save();

			return $adv_profile_ids;
		} else {
			return $this->adv_profile_ids;
		}

	}
	
	function adRequestReport($reportDate, $recordType = 'productAds', $campaignType = 'sp')
	{

		$refreshToken = $this->adv_refresh_token;
		if(empty($refreshToken)) {
			return;
		}

		$client = $this->initClient();
		if(empty($client)) {
			return;
		}
		
		// $adv_profile_ids = $this->updateProfiles($client);
		$adv_profile_ids = $this->adv_profile_ids;
		if(empty($adv_profile_ids)) {
			$adv_profile_ids = $this->updateProfiles($client);
		}

		if($recordType == 'productAds') {
			$metrics = "campaignName,campaignId,adGroupName,adGroupId,currency,asin,sku,";
		} elseif ($recordType == 'campaigns') {
			$metrics = "campaignName,campaignId,campaignStatus,campaignBudget,";
		} elseif ($recordType == 'adGroups') {
			$metrics = "campaignName,campaignId,adGroupName,adGroupId,";
		} elseif ($recordType == 'keywords') {
			$metrics = "campaignName,campaignId,adGroupName,adGroupId,keywordId,keywordText,matchType,";
		} elseif($recordType == 'searchterms') {
			$metrics = "campaignName,campaignId,adGroupName,adGroupId,keywordId,keywordText,matchType,";
		} elseif($recordType == 'targets') {
			$metrics = "campaignName,campaignId,adGroupName,adGroupId,";
		}

		$metrics .= "impressions,clicks,cost,attributedConversions1d,attributedConversions7d,attributedConversions14d,attributedConversions30d,attributedConversions1dSameSKU,attributedConversions7dSameSKU,attributedConversions14dSameSKU,attributedConversions30dSameSKU,attributedUnitsOrdered1d,attributedUnitsOrdered7d,attributedUnitsOrdered14d,attributedUnitsOrdered30d,attributedSales1d,attributedSales7d,attributedSales14d,attributedSales30d,attributedSales1dSameSKU,attributedSales7dSameSKU,attributedSales14dSameSKU,attributedSales30dSameSKU";

		$params = array("reportDate" => $reportDate, "metrics" => $metrics);

		$segment = "";
		if($recordType == 'searchterms' || $recordType == 'targets') {
			$segment = "query";
			$params['segment'] = $segment;
		}
		
		$profile_ids = json_decode($adv_profile_ids, false, 512, JSON_BIGINT_AS_STRING);
		if(empty($profile_ids)) {
			return;
		}
		
		// get all profiles belongs to this seller
		foreach ($profile_ids as $profile) {

			$client->profileId = $profile->profileId;
			$country = $profile->countryCode;

			if($recordType == 'searchterms') {
				$recordTypeRequest = 'keywords';
			} else {
				$recordTypeRequest = $recordType;
			}
			$res = $client->requestReport($recordTypeRequest, $params, $campaignType);

			if($res['success'] && json_decode($res['response'], false, 512, JSON_BIGINT_AS_STRING)->status != "FAILURE") {

				$reportId = json_decode($res['response'], false, 512, JSON_BIGINT_AS_STRING)->reportId;
				
				if($reportId) {
					try{
						// save ad report request id to avoid repeat request
						$ad_report_request = AdReportRequest::where('seller_id', $this->seller_id)->where('country', $country)->where('recordType', $recordType)->where('reportDate', $reportDate)->first();

						if(!is_null($ad_report_request)) {
							$ad_report_request->metrics = $metrics;
							$ad_report_request->reportId = $reportId;
							$ad_report_request->save();
						} else {
							$ad_report_request = new AdReportRequest;
							$ad_report_request->seller_id = $this->seller_id;
							$ad_report_request->country = $country;
							$ad_report_request->recordType = $recordType;
							$ad_report_request->reportDate = $reportDate;
							$ad_report_request->segment = $segment;
							$ad_report_request->metrics = $metrics;
							$ad_report_request->reportId = $reportId;

							$ad_report_request->save();
						}


						echo "Success Request ".$recordType." Report for account: " . $this->seller_id . ", country: " . $country . ", date: ". $reportDate . "\n";
					} catch (\Exception $e ) {
						echo "Failed to Request ".$recordType." Report for account: " . $this->seller_id . ", country: " . $country . ", date: " . $reportDate . "\n";
						echo $e->getMessage();
					}
				}
			}

		}

	}

	public function getAdReportByDate($reportDate, $recordType = 'productAds') {

		$report_requests = $this->adReportRequests->where('recordType', $recordType)->where('reportDate', $reportDate);
		foreach($report_requests as $report_request) {
			$reportId = $report_request->reportId;
			$country = $report_request->country;
			$seller_id = $report_request->seller_id;
			$date = $report_request->reportDate;

			$profile = $this->getProfileByCountry($country);
			$client = $this->initClient();
			if(empty($client)) {
				return;
			}

			if(!$profile) {
				$this->updateProfiles($client);
				$profile = $this->getProfileByCountry($country);
			}
			
			if(!$profile) {
				return;
			}
			$client->profileId = $profile->profileId;
			$currency = $profile->currencyCode;

			$res = $client->getReport($reportId);
			$data = [];
			if(is_array(json_decode($res['response'], false, 512, JSON_BIGINT_AS_STRING))) {
				foreach(json_decode($res['response'], false, 512, JSON_BIGINT_AS_STRING) as $ad) {

					$row_data = [];
					// $row_data['adId'] = $adId;
					// $row_data['spend'] = $ad->cost;
					$row_data['currency'] = $currency;
					$row_data['seller_id'] = $seller_id;
					$row_data['country'] = $country;
					$row_data['date'] = $date;
					
	        		if(!isset($ad->impressions) or (empty($ad->impressions) and empty($ad->clicks))) {
	        			continue;
	        		}

					if($currency == 'USD') {
		            	$row_data['exchange_rate'] = 1;
					}
		        	else {
		        		$row_data['exchange_rate'] = CurrencyHelper::get_exchange_rate($date,$currency,'USD');
		        	}

		        	if($recordType == 'productAds') {
		        		$row_data['adId'] = $ad->adId;

						$metrics = "campaignName,campaignId,adGroupName,adGroupId,currency,asin,sku,";
					} elseif($recordType == 'campaigns') {
						$metrics = "campaignName,campaignId,campaignStatus,campaignBudget,";
					} elseif ($recordType == 'adGroups') {
						$metrics = "campaignName,campaignId,adGroupName,adGroupId,";
					} elseif ($recordType == 'keywords') {
						$metrics = "campaignName,campaignId,adGroupName,adGroupId,keywordId,keywordText,matchType,";
					} elseif($recordType == 'searchterms') {
						if(isset($ad->query)) {
							$row_data['query'] = $ad->query;
						} else {
							continue;
						}

						$metrics = "campaignName,campaignId,adGroupName,adGroupId,keywordId,keywordText,matchType,";
					} elseif($recordType == 'targets') {
						if(isset($ad->query)) {
							$row_data['query'] = $ad->query;
						} else {
							continue;
						}

						$metrics = "campaignName,campaignId,adGroupName,adGroupId,";
					}

					$metrics .= "impressions,clicks,cost,attributedConversions1d,attributedConversions7d,attributedConversions14d,attributedConversions30d,attributedConversions1dSameSKU,attributedConversions7dSameSKU,attributedConversions14dSameSKU,attributedConversions30dSameSKU,attributedUnitsOrdered1d,attributedUnitsOrdered7d,attributedUnitsOrdered14d,attributedUnitsOrdered30d,attributedSales1d,attributedSales7d,attributedSales14d,attributedSales30d,attributedSales1dSameSKU,attributedSales7dSameSKU,attributedSales14dSameSKU,attributedSales30dSameSKU";

					foreach(explode(",", $metrics) as $key) {
						$row_data[$key] = isset($ad->$key) ? $ad->$key : null;
					}

					if($recordType == 'productAds' and empty($row_data['sku'])) {
						$adRes = $client->getProductAd($row_data['adId']);
						$productAds = json_decode($adRes['response'], false, 512, JSON_BIGINT_AS_STRING);

						$row_data['sku'] = $productAds->sku;
						if(isset($productAds->asin)) {
							$row_data['asin'] = $productAds->asin;
						} else {
							$listing = Listing::where('seller_id', $seller_id)->where('sku', $productAds->sku)->where('country', $country)->first();

							if(!is_null($listing)) {
								$row_data['asin'] = $listing->asin;
							} else {
								$row_data['asin'] = '';
							}
						}
						
						
					}

					if($recordType == 'targets') {
						$row_data['keywordId'] = 1;
					}

		        	$data[]= $row_data;
				}
			}

			if(!empty($data)) {
		        try{
		        	if($recordType == 'campaigns') {
		        		$result = AdCampaignsReport::insertOrUpdate($data);
		        	} elseif ($recordType == 'adGroups') {
		        		$result = AdGroupsReport::insertOrUpdate($data);
		        	} elseif ($recordType == 'productAds') {
		        		$result = AdReport::insertOrUpdate($data);
		        	} elseif ($recordType == 'keywords') {
		        		$result = AdKeywordsReport::insertOrUpdate($data);
		        	} elseif ($recordType == 'searchterms' || $recordType == 'targets') {
		        		$result = AdSearchtermsReport::insertOrUpdate($data);
		        	}


		        	echo "Success get ".$recordType." Report for account: " . $seller_id . ", country: " . $country . ", date: ". $date . "\n";
		        } catch( \Exception $e ) {
					session()->flash('msg',$e->getMessage());
					echo "Failed to get ".$recordType." Report for account: " . $seller_id . ", country: " . $country . ", date: ". $date . "\n";
				}			
			}

		}

	}

	public function adRequestSnapshot($recordType, $stateFilter = "enabled,paused,archived", $campaignType = "sponsoredProducts") {
		$refreshToken = $this->adv_refresh_token;
		if(empty($refreshToken)) {
			return;
		}

		$client = $this->initClient();
		if(empty($client)) {
			return;
		}

		$adv_profile_ids = $this->adv_profile_ids;
		if(empty($adv_profile_ids)) {
			$adv_profile_ids = $this->updateProfiles($client);
		}

		$profile_ids = json_decode($adv_profile_ids, false, 512, JSON_BIGINT_AS_STRING);
		// get all profiles belongs to this seller
		foreach ($profile_ids as $profile) {
			$client->profileId = $profile->profileId;
			$country = $profile->countryCode;

			$res = $client->requestSnapshot( $recordType, 
				array("stateFilter" => $stateFilter,
		          "campaignType" => $campaignType));

			if($res['success'] && json_decode($res['response'], false, 512, JSON_BIGINT_AS_STRING)->status != "FAILURE") {

				$snapshotId = json_decode($res['response'], false, 512, JSON_BIGINT_AS_STRING)->snapshotId;
				
				if($snapshotId) {
					try{
						// only keep one snapshot record in db
						$ad_snapshot = AdSnapshot::where('seller_id', $this->seller_id)->where('country', $country)->where('recordType', $recordType)->where('stateFilter', $stateFilter)->first();

						if(!is_null($ad_snapshot)) {
							$ad_snapshot->snapshotId = $snapshotId;
							$ad_snapshot->save();
						} else {
							$ad_snapshot = new AdSnapshot;
							$ad_snapshot->seller_id = $this->seller_id;
							$ad_snapshot->country = $country;
							$ad_snapshot->snapshotId = $snapshotId;
							$ad_snapshot->recordType = $recordType;
							$ad_snapshot->campaignType = $campaignType;
							$ad_snapshot->stateFilter = $stateFilter;
							$ad_snapshot->save();
						}

						
						echo "Success Reqeust Ad Snapshot for account: " . $this->seller_id . ", country: " . $country . "\n";
					} catch (\Exception $e ) {
						echo "Failed to Reqeust Ad Snapshot for account: " . $this->seller_id . ", country: " . $country . "\n";
						echo $e->getMessage();
					}
				}
			}

		}	
	}

	public function getSnapshot($recordType, $stateFilter = "enabled,paused,archived") {
		$snapshot = $this->adSnapshots->where('recordType', $recordType)->where('stateFilter', $stateFilter)->first();
		if($snapshot) {
				$snapshotId = $snapshot->snapshotId;
				$country = $snapshot->country;
				$seller_id = $snapshot->seller_id;

				$profile = $this->getProfileByCountry($country);
				$client = $this->initClient();
				if(empty($client)) {
					return;
				}

				if(!$profile) {
					$this->updateProfiles($client);
					$profile = $this->getProfileByCountry($country);
				}
				$client->profileId = $profile->profileId;

				$res = $client->getSnapshot($snapshotId);
				if($res['success'] && $res['code'] == 200) {
					$data = [];
					$result_rows = json_decode($res['response'], false, 512, JSON_BIGINT_AS_STRING);

					if(is_array($result_rows) && count($result_rows) > 0) {
						foreach($result_rows as $row) {
							$row_data = [];

							$row_data['seller_id'] = $seller_id;
							$row_data['country'] = $country;

							if($recordType == 'campaigns') {
								$row_data['campaignId'] = $row->campaignId;
								$row_data['name'] = $row->name;
								$row_data['campaignType'] = $row->campaignType;
								$row_data['targetingType'] = $row->targetingType;
								$row_data['premiumBidAdjustment'] = $row->premiumBidAdjustment;
								$row_data['dailyBudget'] = $row->dailyBudget;
								$row_data['startDate'] = $row->startDate;
								$row_data['state'] = $row->state;
							} elseif ($recordType == 'adGroups') {
								$row_data['adGroupId'] = $row->adGroupId;
								$row_data['name'] = $row->name;
								$row_data['campaignId'] = $row->campaignId;
								$row_data['defaultBid'] = $row->defaultBid;
								$row_data['state'] = $row->state;						
							} elseif ($recordType == 'productAds') {
								$row_data['adId'] = $row->adId;
								$row_data['adGroupId'] = $row->adGroupId;
								$row_data['campaignId'] = $row->campaignId;
								$row_data['sku'] = $row->sku;
								$row_data['state'] = $row->state;						
							} elseif ($recordType == 'keywords' || $recordType == 'negativeKeywords') {
								$row_data['keywordId'] = $row->keywordId;
								if(isset($row->adGroupId)) {
									$row_data['adGroupId'] = $row->adGroupId;
								} else {
									$row_data['adGroupId'] = null;
								}
								$row_data['campaignId'] = $row->campaignId;
								$row_data['keywordText'] = $row->keywordText;
								$row_data['matchType'] = $row->matchType;
								$row_data['state'] = $row->state;	
								if(isset($row->bid)) {
									$row_data['bid'] = $row->bid;
								} else {
									$row_data['bid'] = null;
								}
							}

							$row_data['updated_at'] = date('Y-m-d H:i:s');
				        	$data[]= $row_data;
						}

						

						try{
							if($recordType == 'campaigns') {
				        		$result = AdCampaign::insertOrUpdate($data);
				        	} elseif ($recordType == 'adGroups') {
				        		$result = AdGroup::insertOrUpdate($data);
				        	} elseif ($recordType == 'productAds') {
				        		$result = ProductAd::insertOrUpdate($data);
				        	} elseif ($recordType == 'keywords' || $recordType == 'negativeKeywords') {
				        		$result = AdKeyword::insertOrUpdate($data);
				        	}

				        	echo "Success get ".$recordType." for account: " . $seller_id . ", country: " . $country . "\n";
				        } catch( \Exception $e ) {
							session()->flash('msg',$e->getMessage());
							var_dump($e->getMessage());
							echo "Failed to get ".$recordType." for account: " . $seller_id . ", country: " . $country . "\n";
						}
					}
				}

		}
		

	}



	public function updateProductAds($recordType, $stateFilter = "enabled,paused,archived") {
		$refreshToken = $this->adv_refresh_token;
		if(empty($refreshToken)) {
			return;
		}

		$client = $this->initClient();
		if(empty($client)) {
			return;
		}

		$adv_profile_ids = $this->adv_profile_ids;
		if(empty($adv_profile_ids)) {
			try{
				$adv_profile_ids = $this->updateProfiles($client);
			} catch( \Exception $e ) {
				var_dump($e->getMessage());
			}
		}

		$profile_ids = json_decode($adv_profile_ids, false, 512, JSON_BIGINT_AS_STRING);
		if(empty($profile_ids)) {
			return;
		}
		
		// get all profiles belongs to this seller
		foreach ($profile_ids as $profile) {
			$client->profileId = $profile->profileId;
			$country = $profile->countryCode;
			$seller_id = $this->seller_id;
			$startIndex = 0;
			$count = 5000;

			switch($recordType) {
				case 'campaigns': 
					$retry = 0;
					while(true) {
						$res = $client->listCampaignsEx(array("stateFilter" => $stateFilter, "startIndex" => $startIndex, "count" => $count));
						if($res['success']) {
							$result_rows =json_decode($res['response'], false, 512, JSON_BIGINT_AS_STRING);
							if(is_array($result_rows) && count($result_rows) > 0) {

								$this->insertDB($result_rows, $recordType, $seller_id, $country, $startIndex);

								if(count($result_rows) < 5000) {
									break;
								} else {
									$startIndex += 5000;
								}
							} else {
								break;
							}

						} else {
							if($retry >= 5) {
								break;
							} else {
								continue;
							}
						}
					}

					break;
				case 'adGroups':
					$retry = 0;
					while(true) {
						$res = $client->listAdGroupsEx(array("stateFilter" => $stateFilter, "startIndex" => $startIndex, "count" => $count));
						if($res['success']) {
							$result_rows =json_decode($res['response'], false, 512, JSON_BIGINT_AS_STRING);
							if(is_array($result_rows) && count($result_rows) > 0) {

								$this->insertDB($result_rows, $recordType, $seller_id, $country, $startIndex);

								if(count($result_rows) < 5000) {
									break;
								} else {
									$startIndex += 5000;
								}
							} else {
								break;
							}

						} else {
							if($retry >= 5) {
								break;
							} else {
								continue;
							}
						}
					}

					break;
				case 'keywords':
					$retry = 0;
					while(true) {
						$res = $client->listBiddableKeywordsEx(array("stateFilter" => $stateFilter, "startIndex" => $startIndex, "count" => $count));
						if($res['success']) {
							$result_rows =json_decode($res['response'], false, 512, JSON_BIGINT_AS_STRING);
							if(is_array($result_rows) && count($result_rows) > 0) {

								$this->insertDB($result_rows, $recordType, $seller_id, $country, $startIndex);

								if(count($result_rows) < 5000) {
									break;
								} else {
									$startIndex += 5000;
								}
							} else {
								break;
							}

						} else {
							if($retry >= 5) {
								break;
							} else {
								continue;
							}
						}
					}

					break;
				case 'negativeKeywords':
					$retry = 0;
					while(true) {
						$retry += 1;
						$res = $client->listNegativeKeywordsEx(array("stateFilter" => $stateFilter, "startIndex" => $startIndex, "count" => $count));
						if($res['success']) {
							$result_rows =json_decode($res['response'], false, 512, JSON_BIGINT_AS_STRING);
							if(is_array($result_rows) && count($result_rows) > 0) {

								$this->insertDB($result_rows, $recordType, $seller_id, $country, $startIndex);

								if(count($result_rows) < 5000) {
									break;
								} else {
									$startIndex += 5000;
								}
							} else {
								break;
							}

						} else {
							if($retry >= 5) {
								break;
							} else {
								continue;
							}
						}
					}

					break;
				case 'campaignNegativeKeywords':
					$retry = 0;
					while(true) {
						$retry += 1;
						$res = $client->listCampaignNegativeKeywordsEx(array("stateFilter" => $stateFilter, "startIndex" => $startIndex, "count" => $count));
						if($res['success']) {
							$result_rows =json_decode($res['response'], false, 512, JSON_BIGINT_AS_STRING);
							if(is_array($result_rows) && count($result_rows) > 0) {

								$this->insertDB($result_rows, $recordType, $seller_id, $country, $startIndex);

								if(count($result_rows) < 5000) {
									break;
								} else {
									$startIndex += 5000;
								}
							} else {
								break;
							}

						} else {
							if($retry >= 5) {
								break;
							} else {
								continue;
							}
						}
					}

					break;
				case 'productAds':
					$retry = 0;
					while(true) {
						$res = $client->listProductAdsEx(array("stateFilter" => $stateFilter, "startIndex" => $startIndex, "count" => $count));
						if($res['success']) {
							$result_rows =json_decode($res['response'], false, 512, JSON_BIGINT_AS_STRING);
							if(is_array($result_rows) && count($result_rows) > 0) {

								$this->insertDB($result_rows, $recordType, $seller_id, $country, $startIndex);

								if(count($result_rows) < 5000) {
									break;
								} else {
									$startIndex += 5000;
								}
							} else {
								break;
							}

						} else {
							if($retry >= 5) {
								break;
							} else {
								continue;
							}
						}
					}

					break;
			}


		}	
	}


	public function insertDB($result_rows, $recordType, $seller_id, $country, $startIndex) {
		$data = [];
		if(is_array($result_rows) && count($result_rows) > 0) {
			foreach($result_rows as $row){

				$row_data['seller_id'] = $seller_id;
				$row_data['country'] = $country;

				if($recordType == 'campaigns') {
					$row_data['campaignId'] = $row->campaignId;
					$row_data['name'] = $row->name;
					$row_data['campaignType'] = $row->campaignType;
					$row_data['targetingType'] = $row->targetingType;
					$row_data['premiumBidAdjustment'] = $row->premiumBidAdjustment;
					if(isset($row->dailyBudget)) {
						$row_data['dailyBudget'] = $row->dailyBudget;
					} else {
						$row_data['dailyBudget'] = null;
					}
					
					$row_data['startDate'] = $row->startDate;
					if(isset($row->endDate)) {
						$row_data['endDate'] = $row->endDate;
					} else {
						$row_data['endDate'] = null;
					}
					$row_data['state'] = $row->state;
					if(isset($row->placement)) {
						$row_data['placement'] = $row->placement;
					} else {
						$row_data['placement'] = null;
					}
					$row_data['servingStatus'] = $row->servingStatus;
					$row_data['creationDate'] = $row->creationDate;
					$row_data['lastUpdatedDate'] = $row->lastUpdatedDate;
				} elseif ($recordType == 'adGroups') {
					$row_data['adGroupId'] = $row->adGroupId;
					$row_data['name'] = $row->name;
					$row_data['campaignId'] = $row->campaignId;
					$row_data['defaultBid'] = $row->defaultBid;
					$row_data['state'] = $row->state;	
					$row_data['servingStatus'] = $row->servingStatus;
					$row_data['creationDate'] = $row->creationDate;
					$row_data['lastUpdatedDate'] = $row->lastUpdatedDate;
				} elseif ($recordType == 'productAds') {
					$row_data['adId'] = $row->adId;
					$row_data['adGroupId'] = $row->adGroupId;
					$row_data['campaignId'] = $row->campaignId;
					$row_data['sku'] = $row->sku;
					$row_data['state'] = $row->state;
					$row_data['servingStatus'] = $row->servingStatus;
					$row_data['creationDate'] = $row->creationDate;
					$row_data['lastUpdatedDate'] = $row->lastUpdatedDate;						
				} elseif ($recordType == 'keywords' || $recordType == 'negativeKeywords' || $recordType == 'campaignNegativeKeywords') {
					$row_data['keywordId'] = $row->keywordId;
					if(isset($row->adGroupId)) {
						$row_data['adGroupId'] = $row->adGroupId;
					} else {
						$row_data['adGroupId'] = null;
					}
					$row_data['campaignId'] = $row->campaignId;
					$row_data['keywordText'] = $row->keywordText;
					$row_data['matchType'] = $row->matchType;
					$row_data['state'] = $row->state;	
					if(isset($row->bid)) {
						$row_data['bid'] = $row->bid;
					} else {
						$row_data['bid'] = null;
					}
					if(isset($row->servingStatus)) {
						$row_data['servingStatus'] = $row->servingStatus;
					} else {
						$row_data['servingStatus'] = null;
					}
					$row_data['creationDate'] = $row->creationDate;
					$row_data['lastUpdatedDate'] = $row->lastUpdatedDate;
				}

				$row_data['updated_at'] = date('Y-m-d H:i:s');
		        $data[]= $row_data;
			}


			try{
				if($recordType == 'campaigns') {
	        		$result = AdCampaign::insertOrUpdate($data);
	        	} elseif ($recordType == 'adGroups') {
	        		$result = AdGroup::insertOrUpdate($data);
	        	} elseif ($recordType == 'productAds') {
	        		$result = ProductAd::insertOrUpdate($data);
	        	} elseif ($recordType == 'keywords' || $recordType == 'negativeKeywords' || $recordType == 'campaignNegativeKeywords') {
	        		$result = AdKeyword::insertOrUpdate($data);
	        	}

	        	echo "Success update ".$recordType." from " . $startIndex . " for account: " . $seller_id . ", country: " . $country . "\n";
	        } catch( \Exception $e ) {
				session()->flash('msg',$e->getMessage());
				var_dump($e->getMessage());
				echo "Failed to update ".$recordType." from " . $startIndex . " for account: " . $seller_id . ", country: " . $country . "\n";
			}


		}

	}


	public function initClient() {

		$refreshToken = $this->adv_refresh_token;
		if($this->region == "US") {
			$region = "na";
		} elseif ($this->region == "UK") {
			$region = "eu";
		} else {
			return;
		}

		$config = array(
		    "clientId" => config('app.lwa_client_id'),
		    "clientSecret" => config('app.lwa_client_secret'),
		    "refreshToken" => $refreshToken,
		    "region" => $region,
		    "sandbox" => false,
	  	);

		$client = new Client($config);

		return $client;
	}

	static public function nameIdArray() {
		return self::select('name', 'seller_id', 'code')
								->get()
								->keyBy('seller_id')
								->map(function($account) {
									return $account->code . " - " . $account->name;
								})->toArray();
	}

	public static function boot() {
		parent::boot();

		self::saving(
			function($account){
				if ($account->status != 'Active') {
					Listing::where('seller_id', $account->seller_id)->update(['status' => 0]);
				} else {
					Listing::where('seller_id', $account->seller_id)->update(['status' => 1]);
				}
			}
		);
	}
}

