<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Order;
use App\Grids\OrderGrid;
use Formatter;
use Grids;

use App\OrderItem;

use Input;
use Validator;
use App\Account;
use Illuminate\Support\Facades\Auth;
use App\ListingExpense;
use App\AdPayment;
use App\SellerDeal;
use App\SellerCoupon;
use App\OtherFee;
use App\AdReport;
use App\Listing;
use App\User;
use App\Review;
use App\Product;
use App\Report;
use App\ProductDailyProfit;
use App\Grids\ReportGrid;
use App\UserProduct;
use App\Inventory;
use App\PmGoal;
use App\Settlement;
use Carbon\Carbon;
use App\InventoryReportGenerator;
use App\Profit;
use App\ProductStatistic;
use App\Helpers\DataHelper;
use App\Disburse;
use App\Grids\DisburseGrid;
use App\Grids\ProductPerformanceGrid;
use Oefenweb\Statistics\Statistics;
use DB;

use Excel;

class ReportController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->middleware('auth');
	}
	
	
	
	
	function periodicGrossSales(Request $request){
		$start_date = $request->get('start_date',date('Y-m-d',strtotime('-6 days')));
		$end_date = $request->get('end_date',date('Y-m-d'));
		$account_id = $request->get('account_id','');
		$marketplace = $request->get('marketplace','all');
		$data_range = $request->get('data_range');
		// $fulfillment_type = $request->get('fulfillment_type','');
		$fulfillment_type = 'afn';
		$sku = $request->get('sku','');
		$asin = $request->get('asin','');
		$country = $request->get('country','');
		
		if(auth()->user()->role == 'product manager assistant'){
			$products = auth()->user()->productList();
		}else{
			$products = [];
		}
		
		$params = ['account_id'=>$account_id,'marketplace'=>$marketplace,'fulfillment_type'=>$fulfillment_type,'asin'=>$asin,'sku'=>$sku,'country'=>$country,'products'=>$products];
		
		
		$orderReport = Order::orderReportsByDate($start_date, $end_date,$params,False);
		
		$otherExpenses =  ListingExpense::orderReportsByDate($start_date, $end_date,$params);
		
		// $reviewExpenses =  Review::totalByDateRange($start_date, $end_date,$params);
		
		$adExpenses = AdReport::totalByDateRange($start_date, $end_date, $params);

		$product = null;
		if ($asin) {
			$product = Product::where('asin', $asin)->first();
			$claimReviewCount = $product->claimReviewOrderCount($start_date, $end_date);
			$inviteReviewCount = $product->inviteReviewOrderCount($start_date, $end_date);
			$selfReviewCount = $product->selfReviewOrderCount($start_date, $end_date);
			$product->claimReviewCount = $claimReviewCount;
			$product->inviteReviewCount = $inviteReviewCount;
			$product->selfReviewCount = $selfReviewCount;
		}
		if(empty($sku) && empty($asin) && empty($products)) {
			$dealExpenses = SellerDeal::amountByDate($start_date, $end_date,$params);
			
			$couponExpenses = SellerCoupon::amountByDate($start_date, $end_date, $params);

			$otherFees = OtherFee::amountByDate($start_date, $end_date, $params);
		} else {
			$dealExpenses = $couponExpenses = $otherFees = null;

		}
		
 		// var_dump($params,$orderReport->toArray());
		
		return view('parts.boxes.box',[
				'account_id'=>$account_id,'start_date'=>$start_date,'end_date'=>$end_date,'marketplace'=>$marketplace,
				'orders'=>$orderReport,'data_range'=>$data_range,'otherExpenses'=>$otherExpenses,
				'adExpenses'=>$adExpenses, 'dealExpenses'=>$dealExpenses, 'couponExpenses'=>$couponExpenses, 'otherFees'=>$otherFees,
				'product' => $product
			]);
	}

	function v2PeriodicGrossSales(Request $request) {
		$start_date = $request->get('start_date',date('Y-m-d',strtotime('-6 days')));
		$end_date = $request->get('end_date',date('Y-m-d'));
		$account_id = $request->get('account_id','');
		$marketplace = $request->get('marketplace','all');
		$data_range = $request->get('data_range');
		$product_id = $request->get('product','');
		$listing_id = $request->get('listing', '');
		$country = $request->get('country','');
		$user_id = $request->get('user_id', '');
		$product = null;
		$products = [];
		if ($product_id) { 
			$product = Product::find($product_id);
			$claimReviewCount = $product->claimReviewOrderCount($start_date, $end_date);
			$inviteReviewCount = $product->inviteReviewOrderCount($start_date, $end_date);
			$selfReviewCount = $product->selfReviewOrderCount($start_date, $end_date);
			$product->claimReviewCount = $claimReviewCount;
			$product->inviteReviewCount = $inviteReviewCount;
			$product->selfReviewCount = $selfReviewCount;
		}
		if ($user_id) {
			$user = User::find($user_id);
			$products = $user->productIds();
		}
		// if(auth()->user()->role == 'product manager assistant'){
		// 	$products = auth()->user()->productList();
		// }else{
		// 	$products = [];
		// }
		$params = [
			'account_id'=>$account_id,
			'marketplace'=>$marketplace,
			'from_date'=>$start_date,
			'to_date'=>$end_date,
			'product_id'=>$product_id,
			'listing_id'=>$listing_id,
			'country'=>$country,
			'product' => $product,
			'products'=>$products,
			'old' => isset($params['old']) ?? 0,
		];
		$profit = Profit::get($start_date, $end_date, $params);
		return view('parts.boxes.v2box', [
			'profit' => $profit,
			'data_range'=> $data_range,
			'start_date'=> $start_date,
			'end_date' => $end_date,
			'product' => $product
		]);
		
	}

	function periodicBestSellers(Request $request){

		$start_date = $request->get('start_date',date('Y-m-d'));
		$end_date = $request->get('end_date',date('Y-m-d'));
		$date_range = $request->get('date_range','Today');
		$account_id = $request->get('account_id');
		$marketplace = $request->get('marketplace');
		$fulfillment_type = $request->get('fulfillment_type');
		$asin = $request->get('asin');

		if(auth()->user()->role == 'product manager assistant'){
			$products = auth()->user()->productList();
		} else {
			$products = [];
		}

		$offset = $request->get('offset', 0);

		$params = ['account_id'=>$account_id,'marketplace'=>$marketplace,'fulfillment_type'=>$fulfillment_type,'asin'=>$asin,'products'=>$products];

		$bestsellers = Order::bestSellersByDate($start_date, $end_date, $params, True, $offset, 24);
		$refunds = Order::refundsByAsins($start_date, $end_date, $params);
		$refund_counts = Order::refundCountByAsins($start_date, $end_date, $params);
		$ppc_counts = Order::ppcCountByAsins($start_date, $end_date, $params);
		$asins = array_pluck($bestsellers, 'ASIN');
		$asin_params = ['account_id'=>$account_id,'marketplace'=>$marketplace,'fulfillment_type'=>$fulfillment_type,'asins'=>$asins];
		$ad_expenses = AdReport::totalByDateRangeAsins($start_date, $end_date, $asin_params);
		$products = Product::whereIn('asin', $asins)->get();

		foreach ($bestsellers as $bestseller) {

			$bestseller->marketplace = config('app.marketplaces')[$bestseller->region];

			foreach($products as $product) {
				if($product->asin == $bestseller->ASIN and $product->country == $bestseller->region) {
					$bestseller->product_id = $product->id;
					$bestseller->product_name = $product->name;
					$bestseller->ai_ads_open = $product->ai_ads_open;
					$bestseller->ai_ads_commission_rate = $product->ai_ads_commission_rate;
					$bestseller->amazon_link = config('app.amzLinks')[strtoupper($product->country)].'dp/'.$product->asin;
				}
			}

			foreach($ad_expenses as $ad_expense) {
				if($ad_expense->asin == $bestseller->ASIN) {
					$bestseller->adExpenses = $ad_expense->total;
				}
			}

			$bestseller->total_refund = $bestseller->refund_count = 0;
			
			foreach ($refunds as $refund) {
				if($bestseller->ASIN == $refund->asin and $bestseller->region == $refund->region) {
					$bestseller->total_refund = $refund->total_refund;
				}
			}

			foreach ($refund_counts as $refund_count) {
				if($bestseller->ASIN == $refund_count->asin and $bestseller->region == $refund_count->region) {
					$bestseller->refund_count = $refund_count->refund_count;
				}
			}
			
			foreach ($ppc_counts as $ppc_count) {
				if($bestseller->ASIN == $ppc_count->asin) {
					$bestseller->ppc_count = $ppc_count->ppc_count;
					$bestseller->ads_sales = $ppc_count->ads_sales;
				}
			}

			$bestseller->gross_sales = $bestseller->total_gross+$bestseller->shipping_fee;
			$bestseller->payout = $bestseller->total_gross+$bestseller->shipping_fee+$bestseller->vat_fee+$bestseller->fba_fee+$bestseller->commission+$bestseller->total_promo+$bestseller->total_refund-$bestseller->adExpenses;

			$bestseller->profit = $bestseller->payout - $bestseller->total_cost;
		}
		
		
		return view('parts.boxes.bestsellers',[
				'bestsellers'=> $bestsellers
		]);	

	}

	function v2PeriodicBestSellers(Request $request){

		$start_date = $request->get('start_date',date('Y-m-d'));
		$end_date = $request->get('end_date',date('Y-m-d'));

		if(auth()->user()->role == 'product manager assistant'){
			$products = auth()->user()->productList();
		} else {
			$products = [];
		}

		$bestsellers = ProductStatistic::getBestSellers($start_date, $end_date);
		
		return view('parts.boxes.v2bestsellers',[
				'bestsellers'=> $bestsellers
		]);	

	}
	
	public function listsAction(Request $request) {

		$title = "Download Report";

		$grid = ReportGrid::grid();

		$reports = Report::all();

		return view('report.index', ['grid' => $grid, "title"=>$title]);
	}

	public function newAction(Request $request) {

		$start_date = $request->get('start_date',date('Y-m-d'));
		$end_date = $request->get('end_date',date('Y-m-d'));
		$date_range = $request->get('date_range','Today');

		$accounts = Account::all();
		$marketplaces = config( 'app.marketplaces' );
		
		$data = [
			'start_date'=>$start_date,'end_date'=>$end_date,'date_range'=>$date_range,
			'accounts'=>$accounts,
			'marketplaces'=>$marketplaces
		];

		return view('report.new', $data);
	}


	public function downloadAction(Request $request,$id) {
		$filename = Report::find($id)->filename;

		return response()->download(storage_path('reports/') . $filename);

	}

	public function productDailyProfitAction(Request $request) {

		$title = "Product Daily Profit";
		
		$accounts = Account::select('name', 'seller_id', 'code')->where('status', 'Active')->get();
		$accounts = $accounts->map(function ($account) {
		    return (object)['name' => $account->name, 'seller_id' => $account->seller_id, 'code' => $account->code];
		});
		$marketplaces = config( 'app.marketplaces' );
		$beginning = ProductDailyProfit::get_beginning();

		return view('report.product_daily_profit', compact('title', 'accounts', 'marketplaces', 'beginning'));
	}

	public function monthlyProfitAction(Request $request) {

		$title = "PM Monthly Profit";

		return view('report.monthly_profit', compact('title'));
	}

	public function fetchMonthlyProfitAction(Request $request) {
		$type = $request->get('type');
		$months = [
			1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr',
			5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug',
			9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec'
		];
		$year = $request->get('year');
		$monthNum = $request->get('month');
		$month = $months[$monthNum];
		$startDate = \Carbon\Carbon::createFromDate($year, $monthNum, 1);
		$endDate = $startDate->copy()->endOfMonth();
		$startDateStr = $startDate->format('Y-m-d');
		$endDateStr = $endDate->format('Y-m-d');
		$queryParams = [
			'from_date' => $startDateStr,
			'to_date' => $endDateStr
		];

		$data = [
			'columns' => [["type" => "date", "id" => "x", "label" => "x"]],
			'rows' => [],
			'aggrows' => [],
			'aggtotal' => []
		];

		list($users, $statistics) = ProductStatistic::getPmMonthlyProfit($queryParams);
		foreach ($users as $user) {
			array_push($data['columns'], [
				'type' => 'number', 'id' => $user['name'], 'label' => $user['name']
			]);
		}

		$pmGoals = PmGoal::whereIn('user_id', array_keys($users))->where([
			['year', '=', $year],
			['month', '=', $monthNum]
		])->get()->groupBy('user_id')->toArray();

		$days = $endDate->diff($startDate)->days;
		$profitRate = $days / $endDate->day;

		$statisticsByPm = [];
		foreach ($statistics as $date => $statisticsByDate) {
			array_push(
				$data['rows'],
				array_merge([$date], array_column($statisticsByDate, $type))
			);

			foreach ($statisticsByDate as $statistic) {
				$userId = $statistic['user']['id'];
				if (!array_key_exists($userId, $statisticsByPm)) {
					$statisticsByPm[$userId] = [];
				}
				array_push($statisticsByPm[$userId], (object)$statistic);
			}
		}

		foreach ($statisticsByPm as $userId => $statistics) {
			if (array_key_exists($userId, $pmGoals)) {
				$userGoal = current($pmGoals[$userId]);
				if (array_key_exists('goal', $userGoal)) {
					if ($userGoal['goal']) {
						$pmGoal = $userGoal['goal'];
					} else {
						$pmGoal = 50000;
					}
				} else {
					$pmGoal = 50000;
				}
			} else {
				$pmGoal = 50000;
			}

			$profit = round(DataHelper::aggregate($statistics, 'profit'), 2);
			$userAggregation = [
				'uid' => $userId,
				'group' => $users[$userId]['group'],
				'pm' => $users[$userId]['name'],
				'goal' => $pmGoal,
				'profit' => $profit,
				'orders' => DataHelper::aggregate($statistics, 'orders'),
				'ad_expenses' => round(DataHelper::aggregate($statistics, 'ad_expenses'), 2),
				'refund' => round(DataHelper::aggregate($statistics, 'refund'), 2),
				'payout' => round(DataHelper::aggregate($statistics, 'payout'), 2),
				'finish_rate' => sprintf('%s%%', round($profit / $pmGoal * 100, 3)),
				'diff' => $profit - $pmGoal,
				'expect_profit' => round($pmGoal * $profitRate, 2)
			];
			array_push($data['aggrows'], $userAggregation);
		}
		$totalCols = [
			'goal', 'profit', 'orders', 'ad_expenses', 'refund', 'payout',
			'diff', 'expect_profit'
		];
		foreach ($totalCols as $k) {
			$data['aggtotal'][$k] = round(Statistics::sum(array_column($data['aggrows'], $k)), 2);
		}
		$data['aggtotal']['finish_rate'] = sprintf(
			'%s%%',
			round($data['aggtotal']['profit'] / $data['aggtotal']['goal'] * 100, 3)
		);

		// dd($result);
		// return response()->json($result);
		return response()->json($data);

	}

	public function updatePmGoalAction(Request $request) {
		$uid = $request->get('uid');
		$year = $request->get('year');
		$month = $request->get('month');

		$goal = $request->get('goal');

		$pm_goal = PmGoal::firstOrNew(array('user_id' => $uid, 'year' => $year, 'month' => $month));
		$pm_goal->goal = (int)$goal;
		$pm_goal->save();


		return response()->json(True);
	}


	public function fetchProductDailyProfitAction(Request $request) {
		$account = $request->get('account', 'all');
		$marketplace = $request->get('marketplace', 'all');
		$startDate = \Carbon\Carbon::parse($request->get('startDate'))->format('Y-m-d');
		$endDate = \Carbon\Carbon::parse($request->input('endDate'))->format('Y-m-d');
		$asin_params = $request->input('asin_params');

		$params = [
			'account_id' => $account,
			'country' => strtoupper($marketplace),
			'from_date' => $startDate,
			'to_date' => $endDate
		];
		if (auth()->user()->role == "product manager assistant") {
			$params['asins'] = auth()->user()->productList();
		}

		$productDailyProfits = ProductStatistic::getProductDailyProfit($params);
		$result = [];
		$columns = [
			'profit', 'orders', 'total_gross', 'refund', 'payout', 'product_cost',
			'ad_expenses', 'total_cost', 'total_sessions', 'unit_session_percentage'
		];
		foreach ($productDailyProfits as $date => $productDailyProfit) {
			$entry = [$date];
			foreach ($columns as $column) {
				array_push($entry, $productDailyProfit[$column]);
			}
			array_push($result, $entry);
		}

		return response()->json($result);
	}


	public function fetchProductDailyProfitRowsAction(Request $request) {
		$account = $request->input('account');
		$marketplace = $request->input('marketplace');
		$start_date = date('Y-m-d', strtotime($request->input('startDate')));
		$end_date = date('Y-m-d', strtotime($request->input('endDate')));
		$asin_params = $request->input('asin_params');

		if ($end_date == date('Y-m-d')) {
			try{
                Product::updateProductDailyProfitByDate($end_date, $end_date);
            } catch(\Exception $e) {
                var_dump($e->getMessage());
            }
		}

		$query = ProductStatistic::leftJoin('products', function($join) {
			$join->on('products.asin', '=', 'product_statistics.asin');
			$join->on('products.country', '=', 'product_statistics.country');
		});
		$query->whereBetween('date', [$start_date, $end_date]);

		if ($account && $account != 'all') {
			$query->where('product_statistics.seller_id', $account);
		}

		if ($marketplace && $marketplace != 'all') {
			$query->where('product_statistics.country', $marketplace);
		}

		if ($asin_params && !empty($asin_params)) {
			$values = array_map(function (array $value) {
	            return "('" . implode($value, "', '") . "')"; 
	        }, json_decode($asin_params));

			$query->whereRaw(
	            '(' . implode(['product_statistics.asin', 'product_statistics.country'], ', ') . ') in (' . implode($values, ', ') . ')'
	        );
		}

		if (auth()->user()->role == "product manager assistant") {
			$query->whereIn('product_statistics.asin', auth()->user()->productList());
		}

		$query->select(DB::raw('product_statistics.asin, product_statistics.country, product_statistics.date, products.name, products.small_image, products.id as product_id, products.warehouse_qty as warehouse_qty, products.discontinued, SUM(product_statistics.total_orders) as orders, SUM(product_statistics.total_profit) as profit, SUM(total_gross_sales) as total_gross, SUM(ads_cost) as ad_expenses, SUM(total_refund) as refund'));

		$query->groupBy(
			'product_statistics.date', 'product_statistics.asin',
			'product_statistics.country');
		$query->orderBy('profit', 'desc');

		$rows = $query->get();

		$daily_profit_data = [];
		foreach ($rows as $row) {
			$daily_profit_data[$row->asin][$row->country][$row->date] = $row;
		}

		$product_managers = [];
		$managers_query = UserProduct::leftJoin('users', function ($join) {
			$join->on('users.id', '=', 'product_users.user_id');
		});
		$managers_query->select(DB::raw('product_users.product_id, users.name'));
		$managers = $managers_query->get();
		foreach ($managers as $manager) {
			if (!isset($product_managers[$manager->product_id])) {
				$product_managers[$manager->product_id] = [];
			}
			$product_managers[$manager->product_id][] = $manager->name;
		}

		$seller_inventory = $seller_inventory_suspended = [];
		$inventory = DB::select("SELECT seller_accounts.name, seller_accounts.code, seller_accounts.status, distinct_inventory.asin, distinct_inventory.country, distinct_inventory.seller_id, SUM(distinct_inventory.fulfillable_quantity) AS fulfillable_qty, SUM(distinct_inventory.reserved_quantity) AS reserved_qty FROM (SELECT product_id, asin, seller_id, status, country, warehouse_quantity, fulfillable_quantity, unsellable_quantity, reserved_quantity, total_quantity FROM product_inventory UNION SELECT product_id, asin, seller_id, status, country, warehouse_quantity, fulfillable_quantity, unsellable_quantity, reserved_quantity, total_quantity FROM product_inventory) AS distinct_inventory LEFT JOIN seller_accounts on distinct_inventory.seller_id = seller_accounts.seller_id WHERE distinct_inventory.status = 1 GROUP BY distinct_inventory.asin, distinct_inventory.country, distinct_inventory.seller_id");
		foreach ($inventory as $inv) {
			if ($inv->status == 'Active') {
				$seller_inventory[$inv->asin . '-' . $inv->country][$inv->seller_id] = ['name'=>$inv->name, 'code'=>$inv->code, 'fulfillable_qty'=>$inv->fulfillable_qty, 'reserved_qty'=>$inv->reserved_qty];
			} else if ($inv->status == 'Suspended' || $inv->status == 'Blocked') {
				$seller_inventory_suspended[$inv->asin . '-' . $inv->country][$inv->seller_id] = ['name'=>$inv->name, 'code'=>$inv->code, 'fulfillable_qty'=>$inv->fulfillable_qty, 'reserved_qty'=>$inv->reserved_qty];
			}
		}

		$result['rowData'] = [];
		$result['topRowData'] = [];
		$toprow = [
			'image' => '',
			'asin' => '',
			'name' => '',
			'total_qty', 'total_qty_suspended', 'total_orders', 'total_profit',
			'total_ad_expenses', 'total_refund', 'total_sessions',
			'unit_session_percentage'
		];

		foreach ($daily_profit_data as $asin => $value) {
			foreach ($value as $country => $row) {
				$region = config('app.countriesRegion')[$country];
				$firstrow = reset($row);
				if ($firstrow->country) {
					$amazon_link = Product::amzLink($asin, $firstrow->country);
				} else {
					$amazon_link = Product::amzLink($asin, $country);
				}
				$newrow['amz_link'] = $amazon_link;
				$newrow['product_link'] = route(
					"product.view", ["id" => $firstrow->product_id]);
				$newrow['name'] = $firstrow->name;
				$newrow['asin'] = $firstrow->asin;
				$newrow['country'] = $firstrow->country;
				$newrow['product_id'] = $firstrow->product_id;
				$newrow['discontinued'] = $firstrow->discontinued;
				$newrow['small_image'] = $firstrow->small_image;
				if (isset($product_managers[$firstrow->product_id])) {
					$newrow['manager'] = implode(
						', ', $product_managers[$firstrow->product_id]);
				} else {
					$newrow['manager'] = '';
				}

				$total_qty = $total_qty_suspended = $total_orders = $total_profit = $total_ad_expenses = $total_refund = $total_sessions = $total_session_orders = 0;

				$date = $end_date;
				$i = 0;
				while ($date >= $start_date) {
					if (!isset($toprow['date' . $i . '_orders'])) {
						$toprow['date' . $i . '_orders'] = 0;
					}

					if (!isset($toprow['date' . $i . '_profit'])) {
						$toprow['date' . $i . '_profit'] = 0;
					}

					if (!isset($toprow['date' . $i . '_ad_expenses'])) {
						$toprow['date' . $i . '_ad_expenses'] = 0;
					}

					if (!isset($toprow['date' . $i . '_refund'])) {
						$toprow['date' . $i . '_refund'] = 0;
					}

					if (!isset($toprow['total_qty'])) {
						$toprow['total_qty'] = 0;
					}

					if (!isset($toprow['total_qty_suspended'])) {
						$toprow['total_qty_suspended'] = 0;
					}

					if (!isset($toprow['total_orders'])) {
						$toprow['total_orders'] = 0;
					}

					if (!isset($toprow['total_profit'])) {
						$toprow['total_profit'] = 0;
					}

					if (!isset($toprow['total_ad_expenses'])) {
						$toprow['total_ad_expenses'] = 0;
					}

					if (!isset($toprow['total_refund'])) {
						$toprow['total_refund'] = 0;
					}

					if (array_key_exists($date, $row)) {
						$newrow['date' . $i . '_orders'] = (float) $row[$date]->orders;
						$newrow['date' . $i . '_profit'] = (float) $row[$date]->profit;
						$newrow['date' . $i . '_ad_expenses'] = (float) $row[$date]->ad_expenses;
						$newrow['date' . $i . '_refund'] = (float) $row[$date]->refund;
						$toprow['date' . $i . '_orders'] += (float) $row[$date]->orders;
						$toprow['date' . $i . '_profit'] += (float) $row[$date]->profit;
						$toprow['date' . $i . '_ad_expenses'] += (float) $row[$date]->ad_expenses;
						$toprow['date' . $i . '_refund'] += (float) $row[$date]->refund;
					} else {
						$newrow['date' . $i . '_orders'] = 0;
						$newrow['date'.$i.'_profit'] = 0;
						$newrow['date'.$i.'_ad_expenses'] = 0;
						$newrow['date'.$i.'_refund'] = 0;

						$toprow['date'.$i.'_orders'] += 0;
						$toprow['date'.$i.'_profit'] += 0;
						$toprow['date'.$i.'_ad_expenses'] += 0;
						$toprow['date'.$i.'_refund'] += 0;
					}

					$total_orders += $newrow['date'.$i.'_orders'];
					$total_profit += $newrow['date'.$i.'_profit'];
					$total_ad_expenses += $newrow['date'.$i.'_ad_expenses'];
					$total_refund += $newrow['date'.$i.'_refund'];
					// $total_sessions += $newrow['date'.$i.'_total_sessions'];
					if( $date != date('Y-m-d') && $date != date('Y-m-d', strtotime('yesterday')) ) {
						$total_session_orders += $newrow['date'.$i.'_orders'];
					}

					$date = date('Y-m-d', strtotime($date . ' -1 day'));
					$i += 1;

				}

				
				$seller_qty_total = 0;
				$seller_qty_reserved_total = 0;
				$newrow['seller_qty'] = "";
				if(isset($seller_inventory[$asin . '-' . $region])) {
					foreach($seller_inventory[$asin . '-' . $region] as $seller_id => $seller_inv) {
						$newrow['seller_qty'] .= $seller_inv['name'] . " (".$seller_inv['code']."): " . $seller_inv['fulfillable_qty'] . " (". $seller_inv['reserved_qty'] .")<!--!>";
						$seller_qty_total += (int)$seller_inv['fulfillable_qty'];
						$seller_qty_reserved_total += (int)$seller_inv['reserved_qty'];
					}
				}

				$seller_qty_suspended_total = 0;
				$seller_qty_suspended_reserved_total = 0;
				$newrow['seller_qty_suspended'] = "";
				if(isset($seller_inventory_suspended[$asin . '-' . $region])) {
					foreach($seller_inventory_suspended[$asin . '-' . $region] as $seller_id => $seller_inv) {
						$newrow['seller_qty_suspended'] .= $seller_inv['name'] . " (".$seller_inv['code']."): " . $seller_inv['fulfillable_qty'] . " (". $seller_inv['reserved_qty'] .")<!--!>";
						$seller_qty_suspended_total += (int)$seller_inv['fulfillable_qty'];
						$seller_qty_suspended_reserved_total += (int)$seller_inv['reserved_qty'];
					}
				}
				
				$newrow['total_qty'] = $seller_qty_total;
				$newrow['total_reserved_qty'] = $seller_qty_reserved_total;
				$newrow['total_qty_suspended'] = $seller_qty_suspended_total;
				$newrow['total_reserved_qty_suspended'] = $seller_qty_suspended_reserved_total;

				$newrow['total_orders'] = round($total_orders, 2);
				$newrow['total_profit'] = round($total_profit, 2);
				$newrow['total_ad_expenses'] = round($total_ad_expenses, 2);
				$newrow['total_refund'] = round($total_refund, 2);
				// $newrow['total_sessions'] = round($total_sessions, 2);
				$newrow['total_session_orders'] = round($total_session_orders, 2);
				// $newrow['unit_session_percentage'] = $total_sessions ? round( ($total_session_orders / $total_sessions) * 100, 2) : 0;

				$toprow['total_qty'] += $seller_qty_total;
				$toprow['total_qty_suspended'] += $seller_qty_suspended_total;
				$toprow['total_orders'] += $newrow['total_orders'];
				$toprow['total_profit'] += $newrow['total_profit'];
				$toprow['total_ad_expenses'] += $newrow['total_ad_expenses'];
				$toprow['total_refund'] += $newrow['total_refund'];
				// $toprow['total_sessions'] += $newrow['total_sessions'];
				// $toprow['total_session_orders'] += $newrow['total_session_orders'];

				$newrow['profit_diff'] = round($newrow['date1_profit'] - $newrow['date2_profit'], 2);
				$newrow['orders_diff'] = round($newrow['date1_orders'] - $newrow['date2_orders'], 2);
				$toprow['profit_diff'] = round($toprow['date1_profit'] - $toprow['date2_profit'], 2);
				$toprow['orders_diff'] = round($toprow['date1_orders'] - $toprow['date2_orders'], 2);

				$result['rowData'][] = $newrow;
				
			}
		}

		$toprow['total_orders'] = round($toprow['total_orders'], 2);
		$toprow['total_profit'] = round($toprow['total_profit'], 2);
		$toprow['total_ad_expenses'] = round($toprow['total_ad_expenses'], 2);
		$toprow['total_refund'] = round($toprow['total_refund'], 2);
		// $toprow['total_sessions'] = round($toprow['total_sessions'], 2);
		// $toprow['unit_session_percentage'] = $toprow['total_sessions'] ? round( ($toprow['total_session_orders'] / $toprow['total_sessions']) * 100, 2) : 0;
		foreach($toprow as $k => $r) {
			if($r != '') {
				$toprow[$k] = round($r, 2);
			}
		}
		$toprow['total_qty'] = (string)$toprow['total_qty'];

		$result['topRowData'][] = $toprow;

		$result['columnDefs'] = [];
		$result['columnDefs'][] = ['headerName' => 'Product Info (Total Amount)', 'children' => [
			['headerName'=> 'Image', 'field'=> 'small_image', 'cellRenderer'=>'imageCellRenderer', 'width'=> 100, 'suppressFilter' => true, 'pinned' => 'left', 'lockPosition'=>true],
			['headerName'=> 'ASIN', 'field'=> 'asin', 'cellRenderer'=>'asinCellRenderer', 'width'=> 100, 'pinned' => 'left', 'lockPosition'=>true, 'floatingFilterComponentParams' => ['suppressFilterButton'=>true]],
			['headerName'=> 'Name', 'field'=> 'name', 'width'=> 120, 'pinned' => 'left', 'cellStyle'=>['white-space'=> 'normal'], 'lockPosition'=>true, 'floatingFilterComponentParams' => ['suppressFilterButton'=>true]],
			['headerName'=> 'Manager', 'field'=> 'manager', 'width'=> 80, 'pinned' => 'left', 'cellStyle'=>['white-space'=> 'normal'], 'lockPosition'=>true, 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'cellRenderer'=>'managerCellRenderer'],
			['headerName'=> 'Suspended Inventory', 'field'=> 'total_qty_suspended', 'pinned' => 'left', 'type'=> 'numericColumn', 'cellRenderer'=>'inventorySuspendedCellRenderer', 'width'=> 100, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true], 'lockPosition'=>true, 'columnGroupShow'=>'open'],
			['headerName'=> 'Total Orders', 'field'=> 'total_orders', 'pinned' => 'left', 'type'=> 'numericColumn', 'width'=> 110, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true], 'lockPosition'=>true, 'columnGroupShow'=>'open'],
			['headerName'=> 'Total Profit', 'field'=> 'total_profit', 'pinned' => 'left', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 110, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true], 'lockPosition'=>true, 'columnGroupShow'=>'open'],
			['headerName'=> 'Total Ad Expenses', 'field'=> 'total_ad_expenses', 'pinned' => 'left', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 110, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true], 'lockPosition'=>true, 'columnGroupShow'=>'open'],
			['headerName'=> 'Total Refund', 'field'=> 'total_refund', 'pinned' => 'left', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 110, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true], 'lockPosition'=>true, 'columnGroupShow'=>'open'],
			// ['headerName'=> 'Total Sessions', 'field'=> 'total_sessions', 'pinned' => 'left', 'type'=> 'numericColumn', 'width'=> 110, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true], 'lockPosition'=>true, 'columnGroupShow'=>'open'],
			// ['headerName'=> 'Session Percentage', 'field'=> 'unit_session_percentage', 'pinned' => 'left', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 110, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true], 'lockPosition'=>true, 'columnGroupShow'=>'open']
		]];
		// $result['columnDefs'][] = ['headerName'=> 'Image', 'field'=> 'image', 'cellRenderer'=>'imageCellRenderer', 'width'=> 100, 'suppressFilter' => true, 'pinned' => 'left', 'lockPosition'=>true];
		// $result['columnDefs'][] = ['headerName'=> 'ASIN', 'field'=> 'asin', 'cellRenderer'=>'asinCellRenderer', 'width'=> 100, 'pinned' => 'left', 'lockPosition'=>true];
		// $result['columnDefs'][] = ['headerName'=> 'Name', 'field'=> 'name', 'pinned' => 'left', 'rowDrag'=>true, 'cellStyle'=>['white-space'=> 'normal'], 'lockPosition'=>true];
		$result['columnDefs'][] = ['headerName'=>'Compare', 'children'=>[
			['headerName'=> 'Orders Diff', 'field'=> 'orders_diff', 'type'=> 'numericColumn', 'pinned' => 'left', 'width'=> 70, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true], 'suppressMenu'=>false, 'lockPosition'=>true],
			['headerName'=> 'Profit Diff', 'field'=> 'profit_diff', 'type'=> 'numericColumn', 'pinned' => 'left', 'cellRenderer'=>'priceCellRenderer', 'width'=> 70, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true], 'suppressMenu'=>false, 'lockPosition'=>true],
		]];

		$date = $end_date;
		$i = 0;
		while($date >= $start_date) {

			$result['columnDefs'][] = ['headerName'=>date('m/d', strtotime($date)), 'children'=>[
				['headerName'=> 'Order', 'field'=> 'date'.$i.'_orders', 'type'=> 'numericColumn', 'width'=> 80, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true], 'suppressMenu'=>false, 'lockPosition'=>true],
				['headerName'=> 'Profit', 'field'=> 'date'.$i.'_profit', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 80, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter','floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true], 'suppressMenu'=>false, 'lockPosition'=>true],
				
				['headerName'=> 'Ad Expenses', 'field'=> 'date'.$i.'_ad_expenses', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 80, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true], 'suppressMenu'=>false, 'lockPosition'=>true, 'columnGroupShow'=>'open'],
				['headerName'=> 'Refund', 'field'=> 'date'.$i.'_refund', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 80, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true], 'suppressMenu'=>false, 'lockPosition'=>true, 'columnGroupShow'=>'open'],
				// ['headerName'=> 'Sessions', 'field'=> 'date'.$i.'_total_sessions', 'type'=> 'numericColumn', 'width'=> 80, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true], 'suppressMenu'=>false, 'lockPosition'=>true, 'columnGroupShow'=>'open'],
				// ['headerName'=> 'Order Session Percentage', 'field'=> 'date'.$i.'_unit_session_percentage', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 80, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true], 'suppressMenu'=>false, 'lockPosition'=>true, 'columnGroupShow'=>'open'],
			]];

			// $result['columnDefs'][] = ['headerName'=> date('m/d', strtotime($date)) . ' Order', 'field'=> 'date'.$i.'_orders', 'type'=> 'numericColumn', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'filterParams' => ['inRangeInclusive'=>true], 'suppressMenu'=>false, 'lockPosition'=>true];
			// $result['columnDefs'][] = ['headerName'=> date('m/d', strtotime($date)) . ' Profit', 'field'=> 'date'.$i.'_profit', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'filterParams' => ['inRangeInclusive'=>true], 'suppressMenu'=>false, 'lockPosition'=>true];

			$date = date('Y-m-d', strtotime($date . ' -1 day'));	
			$i += 1;
		}

		return response()->json($result);

	}


	public function ajaxReportAction(Request $request) {

		$start_date = $request->get('start_date',date('Y-m-d'));
		$end_date = $request->get('end_date',date('Y-m-d'));
		$date_range = $request->get('date_range','Today');
		$seller_id = $request->get('account','');
		$marketplace = $request->get('marketplace','all');
		$fulfillment_type = $request->get('fulfillment_type','');
		$products = $request->get('products','');
		$user_id = auth()->user()->id;
		$type = $request->get('report_type','');
		$filename = $type . "-" . time() . "-" . $user_id;
		$file_type = $request->get('file_type','xls');

		$params = ['account_id'=>$seller_id,'marketplace'=>$marketplace,'fulfillment_type'=>$fulfillment_type];
		var_dump($params);

		if($products) {
			
			$asins = explode(",", $products);

			if(count($asins) > 1) {
				$params['products'] = $asins;
			} else {
				$params['asin'] = $asins;
			}
		}

		if($type == "Sellers Profit Report") {

			$orderReportBySeller = Order::orderReportsBySeller($start_date, $end_date,$params);

			$adExpensesByPayment = AdPayment::amountBySeller($start_date, $end_date,$params);
			$adExpensesByListing = AdReport::totalBySeller($start_date, $end_date, $params);

			$profit_data = [];
			foreach ($orderReportBySeller as $key => $value) {
				$seller_id = $value->seller_id;
				$profit_data[$seller_id]['total_units'] = $value->total_units;
				$profit_data[$seller_id]['total_orders'] = $value->total_orders;
				$profit_data[$seller_id]['total_gross'] = $value->total_gross;
				$profit_data[$seller_id]['tax'] = $value->tax;
				$profit_data[$seller_id]['shipping_fee'] = $value->shipping_fee;
				$profit_data[$seller_id]['vat_fee'] = $value->vat_fee;
				$profit_data[$seller_id]['fba_fee'] = $value->fba_fee;
				$profit_data[$seller_id]['commission'] = $value->commission;
				$profit_data[$seller_id]['total_promo'] = $value->total_promo;
				$profit_data[$seller_id]['promo_count'] = $value->promo_count;
				$profit_data[$seller_id]['total_cost'] = $value->total_cost;
				$profit_data[$seller_id]['total_refund'] = $value->total_refund;
			};

			foreach ($adExpensesByPayment as $key => $value) {
				$seller_id = $value->seller_id;
				$profit_data[$seller_id]['ad_expenses_by_payment'] = $value->total;
			};

			foreach ($adExpensesByListing as $key => $value) {
				$seller_id = $value->seller_id;
				$profit_data[$seller_id]['ad_expenses_by_listing'] = $value->total;
			};
			// // $dealExpenses = SellerDeal::amountByDate($start_date, $end_date,$params);
			// var_dump($profit_data);

			$exported = self::exportProfitBySellers($filename, $file_type, $start_date, $end_date, $profit_data);

		}


		if($type == "Products Profit Report") {
			# get best sellers
			$bestsellers = Order::bestSellersByDate($start_date, $end_date, $params);

			$asins = array_pluck($bestsellers, 'ASIN');
			$asin_params = ['account_id'=>$seller_id,'marketplace'=>$marketplace,'asins'=>$asins];
			$ad_expenses = AdReport::totalByDateRangeAsinCountry($start_date, $end_date, $asin_params);

			foreach ($bestsellers as $bestseller) {
				try {
					$product = Product::where('asin', $bestseller->ASIN)->where('country', $bestseller->region)->firstOrFail();
				} catch(\Exception $e){
					$product = null;
				}
				
				$bestseller->product = $product;
				$bestseller->marketplace = config('app.marketplaces')[$bestseller->region];


				foreach($ad_expenses as $ad_expense) {
					if($ad_expense->asin == $bestseller->ASIN and $bestseller->region == $ad_expense->country) {
						$bestseller->ad_expenses = $ad_expense->total;
						break;
					}
				}


				$asins[] = $bestseller->ASIN;
			}


			# get products spend ad yet no order
			$ads_nosell_products = AdReport::adsWithNoSellProductsByDate($start_date, $end_date, $params, $asins);

			// var_dump($filename);
			$exported = self::exportProfitByProducts($filename, $file_type, $start_date, $end_date, $bestsellers, $ads_nosell_products);

		}

		// if($type == "Orders Report") {

		// }

		if($type == "Ads Report") {

			$seller_ads = AdReport::sellerAdsByDateRange($start_date, $end_date, $params);
			$product_ads = AdReport::productAdsByDateRange($start_date, $end_date, $params);

			$exported = self::exportAdsReport($filename, $file_type, $start_date, $end_date, $seller_ads, $product_ads);

		}

		if($type == "Products Daily Profit Report") {
			$product_ads = AdReport::productAdsByDateRange($start_date, $end_date, $params);
			$product_profits = Order::orderReportsByDateRange($start_date, $end_date, $params);

			foreach($product_profits as $product_profit) {

				foreach ($product_ads as $product_ad) {
					if($product_profit->ASIN == $product_ad->asin and $product_profit->country == $product_ad->country and $product_profit->pdate == $product_ad->date and $product_profit->seller_id == $product_ad->seller_id) {
						$product_profit->ad_expenses = $product_ad->total;
						break;
					}
				}


				$product_profit->payout = $product_profit->total_gross+$product_profit->shipping_fee+$product_profit->vat_fee+$product_profit->fba_fee+$product_profit->commission+$product_profit->total_promo+$product_profit->total_refund;

				$product_profit->cost = $product_profit->total_cost+$product_profit->ad_expenses;

				$product_profit->profit = round($product_profit->payout - $product_profit->cost, 2);

			}

			$exported = self::exportDailyProfitReport($filename, $file_type, $start_date, $end_date, $product_profits);			
		}

		if ($type == "Listing Inventory Report") {
			$data = InventoryReportGenerator::prepareData($seller_id, $marketplace, $start_date);
			$exported = self::exportListingInventoryReport($filename, $file_type, $start_date, $data);
		}


		if($type == "Other Fees Report") {
			$otherFees = OtherFee::getFeeDetailByDateRange($start_date, $end_date, $params);
			$exported = self::exportOtherFeesReport($filename, $file_type, $start_date, $end_date, $otherFees);		
		}


		if($exported) {
			$report = Report::create([
					'type' => $type,
					'filename' => $filename.".".$file_type,
					'seller_id' => $params['account_id'],
					'start_date' => $start_date,
					'end_date'=>$end_date,
					'date_range'=>$date_range,
					'marketplace'=>$marketplace,
					'fulfillment_type'=>$fulfillment_type,
					'products'=>$products,
					'created_by'=>$user_id,
					'file_type'=>$file_type
			]);			
		}

	}


	private function exportProfitBySellers($filename, $file_type, $start_date, $end_date, $profit_data) {

		try{

			return Excel::create($filename, function($excel) use($start_date, $end_date, $profit_data) {

			    // Set the title
			    $excel->setTitle('Sellers Profit Report');

			    // Chain the setters
			    $excel->setCreator('PL Inventory')
			          ->setCompany('KB');

			    // Call them separately
			    $excel->setDescription('KB PL Inventory Profit Report File');


			    $excel->sheet('Sellers Profit', function($sheet) use($start_date, $end_date, $profit_data) {

			    	$row_num = 1;
			    	$header = ['Start Date', 'End Date', 'Seller ID', 'Seller Name', 'Gross Sales', 'Payout', 'Cost', 'Profit', 'Total Units', 'Total Orders', 'Total Gross', 'Tax', 'Shipping Fee', 'FBA Fee', 'Commission', 'Promo Count', 'Total Promo', 'Total Refund', 'Product Cost', 'Ad Expenses By Payment', 'Ad Expenses By Listing'];

			    	
			    	$sheet->row($row_num, $header);
			    	foreach ($profit_data as $seller_id => $value) {
			    		$ad_expenses_by_payment = isset($value['ad_expenses_by_payment']) ? 0-$value['ad_expenses_by_payment'] : 0;
			    		$ad_expenses_by_listing = isset($value['ad_expenses_by_listing']) ? $value['ad_expenses_by_listing'] : 0;

			    		$total_refund = isset($value['total_refund']) ? $value['total_refund'] : 0;
			    		$total_gross = isset($value['total_gross']) ? $value['total_gross'] : 0;
						$shipping_fee = isset($value['shipping_fee']) ? $value['shipping_fee'] : 0;					
			    		$tax = isset($value['tax']) ? $value['tax'] : 0;
						$commission = isset($value['commission']) ? $value['commission'] : 0;
						$vat_fee = isset($value['vat_fee']) ? $value['vat_fee'] : 0;
			    		$fba_fee = isset($value['fba_fee']) ? $value['fba_fee'] : 0;
			    		$total_promo = isset($value['total_promo']) ? $value['total_promo'] : 0;
			    		$total_cost = isset($value['total_cost']) ? $value['total_cost'] : 0;
			    		$total_units = isset($value['total_units']) ? $value['total_units'] : 0;
			    		$total_orders = isset($value['total_orders']) ? $value['total_orders'] : 0;
			    		$promo_count = isset($value['promo_count']) ? $value['promo_count'] : 0;

			    		// Sheet manipulation
			    		$gross_sales = $total_gross + $shipping_fee;
			    		$payout = $gross_sales + $commission + $vat_fee + $fba_fee + $total_promo + $total_refund;
			    		$cost = $total_cost + $ad_expenses_by_listing;
			    		$profit = $payout - $cost;

			    		$seller_name = Account::where('seller_id', $seller_id)->first()->name;

			    		$row = [$start_date, $end_date, $seller_id, $seller_name, number_format($gross_sales, 2), number_format($payout, 2), number_format($cost, 2), number_format($profit, 2), $total_units, $total_orders, number_format($total_gross, 2), number_format($tax, 2), number_format($shipping_fee, 2), number_format($vat_fee, 2), number_format($fba_fee, 2), number_format($commission, 2), $promo_count, number_format($total_promo, 2), number_format($total_refund, 2), number_format($total_cost, 2), number_format($ad_expenses_by_payment, 2), number_format($ad_expenses_by_listing, 2)];

			    		$row_num += 1;
			    		$sheet->row($row_num, $row);
			    	}

			    	

			    	# append empty rows
			    	// for($i = 1; $i < 10; $i++) {
			    	// 	$row_num += 1;
				    // 	$sheet->row($row_num, []);
				    // }




				});


			})->store($file_type, storage_path('reports'));

		}
        catch(Exception $e)
        {
        	var_dump($e);
            return false;
        }
	}

	private function exportAdsReport($filename, $file_type, $start_date, $end_date, $seller_ads, $product_ads) {
		try{

			return Excel::create($filename, function($excel) use($start_date, $end_date, $seller_ads, $product_ads) {

			    // Set the title
			    $excel->setTitle('Ads Report');

			    // Chain the setters
			    $excel->setCreator('PL Inventory')
			          ->setCompany('KB');

			    // Call them separately
			    $excel->setDescription('KB PL Inventory Ads Report File');


			    $excel->sheet('Seller Ads Report', function($sheet) use($start_date, $end_date, $seller_ads) {

			    	$row_num = 1;
			    	$header = ['Seller Name', 'Country'];
			    	$date = $start_date;
			    	while ($date <= $end_date) {
			    		$header[] = date('d-M', strtotime($date));
			    		$date = date('Y-m-d', strtotime("+1 day", strtotime($date)));
			    	}

			    	
			    	$sheet->row($row_num, $header);

			    	$all_rows = [];
			    	foreach ($seller_ads as $ad) {
			    		$all_rows[$ad->seller_id][$ad->country][$ad->date] = $ad->total;
			    	}


			    	$excel_rows = [];
			    	$sellers = array_unique(array_keys($all_rows));
			    	foreach($sellers as $seller) {
			    		$countris = array_unique(array_keys($all_rows[$seller]));
			    		foreach($countris as $country) {
			    			$seller_name = Account::where('seller_id', $seller)->first()->name;
			    			$excel_row = [$seller_name, $country];

			    			$date = $start_date;
					    	while ($date <= $end_date) {
					    		if(isset($all_rows[$seller][$country][$date])) {
					    			$excel_row[] = $all_rows[$seller][$country][$date];
					    		} else {
					    			$excel_row[] = 0;
					    		}
					    		
					    		$date = date('Y-m-d', strtotime("+1 day", strtotime($date)));
					    	}

			    			$excel_rows[] = $excel_row;
			    		}
			    	}



			    	foreach ($excel_rows as $row) {
			    		$row_num += 1;
			    		$sheet->row($row_num, $row);
			    	}




				});



				$excel->sheet('Product Ads Report', function($sheet) use($start_date, $end_date, $product_ads) {

			    	$row_num = 1;
			    	$header = ['Seller Name', 'ASIN', 'Country', 'Name'];
			    	$date = $start_date;
			    	while ($date <= $end_date) {
			    		$header[] = date('d-M', strtotime($date));
			    		$date = date('Y-m-d', strtotime("+1 day", strtotime($date)));
			    	}

			    	
			    	$sheet->row($row_num, $header);

			    	$all_rows = [];
			    	foreach ($product_ads as $ad) {
			    		$all_rows[$ad->seller_id][$ad->country][$ad->asin][$ad->date] = $ad->total;
			    	}


			    	$excel_rows = [];
			    	$sellers = array_unique(array_keys($all_rows));
			    	foreach($sellers as $seller) {
			    		$countris = array_unique(array_keys($all_rows[$seller]));
			    		foreach($countris as $country) {
			    			$asins = array_unique(array_keys($all_rows[$seller][$country]));

			    			foreach($asins as $asin) {
				    			$seller_name = Account::where('seller_id', $seller)->first()->name;
				    			$product_name = Listing::where('asin', $asin)->where('country', $country)->first() ? Listing::where('asin', $asin)->where('country', $country)->first()->item_name : '';
				    			$excel_row = [$seller_name, $asin, $country, $product_name];

				    			$date = $start_date;
						    	while ($date <= $end_date) {
						    		if(isset($all_rows[$seller][$country][$asin][$date])) {
						    			$excel_row[] = $all_rows[$seller][$country][$asin][$date];
						    		} else {
						    			$excel_row[] = 0;
						    		}
						    		
						    		$date = date('Y-m-d', strtotime("+1 day", strtotime($date)));
						    	}

				    			$excel_rows[] = $excel_row;
				    		}
			    		}
			    	}



			    	foreach ($excel_rows as $row) {
			    		$row_num += 1;
			    		$sheet->row($row_num, $row);
			    	}




				});



			})->store($file_type, storage_path('reports'));

		}
        catch(Exception $e)
        {
        	var_dump($e);
            return false;
        }	
	}


	private function exportDailyProfitReport($filename, $file_type, $start_date, $end_date, $product_profits) {
		try{

			return Excel::create($filename, function($excel) use($start_date, $end_date, $product_profits) {

			    // Set the title
			    $excel->setTitle('Daily Product Profit Report');

			    // Chain the setters
			    $excel->setCreator('PL Inventory')
			          ->setCompany('KB');

			    // Call them separately
			    $excel->setDescription('KB PL Inventory Daily Product Profit Report File');


				$excel->sheet('ASIN Profit', function($sheet) use($start_date, $end_date, $product_profits) {

			    	$row_num = 1;
			    	$header = ['Seller Name', 'ASIN', 'Country', 'Name'];
			    	$date = $start_date;
			    	while ($date <= $end_date) {
			    		$header[] = date('d-M', strtotime($date));
			    		$date = date('Y-m-d', strtotime("+1 day", strtotime($date)));
			    	}

			    	
			    	$sheet->row($row_num, $header);

			    	$all_rows = [];
			    	foreach ($product_profits as $product_profit) {
			    		$all_rows[$product_profit->seller_id][$product_profit->country][$product_profit->ASIN][$product_profit->pdate] = $product_profit->profit;
			    	}


			    	$excel_rows = [];
			    	$sellers = array_unique(array_keys($all_rows));
			    	foreach($sellers as $seller) {
			    		$countris = array_unique(array_keys($all_rows[$seller]));
			    		foreach($countris as $country) {
			    			$asins = array_unique(array_keys($all_rows[$seller][$country]));

			    			foreach($asins as $asin) {
				    			$seller_name = Account::where('seller_id', $seller)->first()->name;
				    			$product_name = Listing::where('asin', $asin)->where('country', $country)->first() ? Listing::where('asin', $asin)->where('country', $country)->first()->item_name : '';
				    			$excel_row = [$seller_name, $asin, $country, $product_name];

				    			$date = $start_date;
						    	while ($date <= $end_date) {
						    		if(isset($all_rows[$seller][$country][$asin][$date])) {
						    			$excel_row[] = $all_rows[$seller][$country][$asin][$date];
						    		} else {
						    			$excel_row[] = 0;
						    		}
						    		
						    		$date = date('Y-m-d', strtotime("+1 day", strtotime($date)));
						    	}

				    			$excel_rows[] = $excel_row;
				    		}
			    		}
			    	}



			    	foreach ($excel_rows as $row) {
			    		$row_num += 1;
			    		$sheet->row($row_num, $row);
			    	}


				});



				$excel->sheet('ASIN Order Quantity', function($sheet) use($start_date, $end_date, $product_profits) {

			    	$row_num = 1;
			    	$header = ['Seller Name', 'ASIN', 'Country', 'Name'];
			    	$date = $start_date;
			    	while ($date <= $end_date) {
			    		$header[] = date('d-M', strtotime($date));
			    		$date = date('Y-m-d', strtotime("+1 day", strtotime($date)));
			    	}

			    	
			    	$sheet->row($row_num, $header);

			    	$all_rows = [];
			    	foreach ($product_profits as $product_profit) {
			    		$all_rows[$product_profit->seller_id][$product_profit->country][$product_profit->ASIN][$product_profit->pdate] = $product_profit->total_orders;
			    	}


			    	$excel_rows = [];
			    	$sellers = array_unique(array_keys($all_rows));
			    	foreach($sellers as $seller) {
			    		$countris = array_unique(array_keys($all_rows[$seller]));
			    		foreach($countris as $country) {
			    			$asins = array_unique(array_keys($all_rows[$seller][$country]));

			    			foreach($asins as $asin) {
				    			$seller_name = Account::where('seller_id', $seller)->first()->name;
				    			$product_name = Listing::where('asin', $asin)->where('country', $country)->first() ? Listing::where('asin', $asin)->where('country', $country)->first()->item_name : '';
				    			$excel_row = [$seller_name, $asin, $country, $product_name];

				    			$date = $start_date;
						    	while ($date <= $end_date) {
						    		if(isset($all_rows[$seller][$country][$asin][$date])) {
						    			$excel_row[] = $all_rows[$seller][$country][$asin][$date];
						    		} else {
						    			$excel_row[] = 0;
						    		}
						    		
						    		$date = date('Y-m-d', strtotime("+1 day", strtotime($date)));
						    	}

				    			$excel_rows[] = $excel_row;
				    		}
			    		}
			    	}



			    	foreach ($excel_rows as $row) {
			    		$row_num += 1;
			    		$sheet->row($row_num, $row);
			    	}


				});


			})->store($file_type, storage_path('reports'));

		}
        catch(Exception $e)
        {
        	var_dump($e);
            return false;
        }			
	}


	private function exportProfitByGroups($filename, $file_type, $start_date, $end_date, $profit_data) {

		try{

			return Excel::create($filename, function($excel) use($start_date, $end_date, $profit_data) {

			    // Set the title
			    $excel->setTitle('Groups Profit Report');

			    // Chain the setters
			    $excel->setCreator('PL Inventory')
			          ->setCompany('KB');

			    // Call them separately
			    $excel->setDescription('KB PL Inventory Profit Report File');


			    $excel->sheet('Groups Profit', function($sheet) use($start_date, $end_date, $profit_data) {

			    	$row_num = 1;
			    	$header = ['Start Date', 'End Date', 'Group', 'Gross Sales', 'Payout', 'Cost', 'Profit', 'Total Units', 'Total Orders', 'Total Gross', 'Tax', 'Shipping Fee', 'FBA Fee', 'Commission', 'Promo Count', 'Total Promo', 'Total Refund', 'Product Cost', 'Ad Expenses'];

			    	
			    	$sheet->row($row_num, $header);
			    	foreach ($profit_data as $group => $value) {
			    		$ad_expenses = isset($value['ad_expenses']) ? $value['ad_expenses'] : 0;
			    		$total_refund = isset($value['total_refund']) ? $value['total_refund'] : 0;
			    		$total_gross = isset($value['total_gross']) ? $value['total_gross'] : 0;
			    		$shipping_fee = isset($value['shipping_fee']) ? $value['shipping_fee'] : 0;
			    		$tax = isset($value['tax']) ? $value['tax'] : 0;
						$commission = isset($value['commission']) ? $value['commission'] : 0;
						$vat_fee = isset($value['vat_fee']) ? $value['vat_fee'] : 0;
			    		$fba_fee = isset($value['fba_fee']) ? $value['fba_fee'] : 0;
			    		$total_promo = isset($value['total_promo']) ? $value['total_promo'] : 0;
			    		$total_cost = isset($value['total_cost']) ? $value['total_cost'] : 0;
			    		$total_units = isset($value['total_units']) ? $value['total_units'] : 0;
			    		$total_orders = isset($value['total_orders']) ? $value['total_orders'] : 0;
			    		$promo_count = isset($value['promo_count']) ? $value['promo_count'] : 0;

			    		// Sheet manipulation
			    		$gross_sales = $total_gross + $shipping_fee;
			    		$payout = $gross_sales + $commission + $vat_fee + $fba_fee +  $total_promo + $total_refund;
			    		$cost = $total_cost + $ad_expenses;
			    		$profit = $payout - $cost;

			    		$row = [$start_date, $end_date, $group, "$".number_format($gross_sales, 2), "$".number_format($payout, 2), "$".number_format($cost, 2), "$".number_format($profit, 2), $total_units, $total_orders, "$".number_format($total_gross, 2), "$".number_format($tax, 2), "$".number_format($shipping_fee, 2), "$".number_format($vat_fee, 2), "$".number_format($fba_fee, 2), "$".number_format($commission, 2), $promo_count, "$".number_format($total_promo, 2), "$".number_format($total_refund, 2), "$".number_format($total_cost, 2), "$".number_format($ad_expenses, 2)];

			    		$row_num += 1;
			    		$sheet->row($row_num, $row);
			    	}

			    	

			    	# append empty rows
			    	// for($i = 1; $i < 10; $i++) {
			    	// 	$row_num += 1;
				    // 	$sheet->row($row_num, []);
				    // }




				});


			})->store($file_type, storage_path('reports'));

		}
        catch(Exception $e)
        {
        	var_dump($e);
            return false;
        }
	}

	private function exportOtherFeesReport($filename, $file_type, $start_date, $end_date, $otherFees) {
		try {

			return Excel::create($filename, function($excel) use($start_date, $end_date, $otherFees) {
			    // Set the title
			    $excel->setTitle('Other Fees Detail Report');

			    // Chain the setters
			    $excel->setCreator('Josh')
			          ->setCompany('Seller Warrior');

			    // Call them separately
			    $excel->setDescription('Seller Warrior Other Fees Detail Report File');

			    $excel->sheet('Other Fees', function($sheet) use($start_date, $end_date, $otherFees) {

			    	$row_num = 1;
				    $header = ['Posted Date', 'Seller Name', 'Seller Code', 'Seller ID', 'Fee Reason', 'Fee Type', 'Fee Description', 'Fee Amount', 'Currency', 'Exchange Rate'];

				    $fee_descriptions = [
				    	"Commission" => "Referral Fees",
				    	"CouponClipFee" => "Coupon clip fee",
				    	"CouponRedemptionFee" => "Coupon redemption fee",
				    	"FixedClosingFee" => "Per-item fees for Individual Sellers",
				    	"FreshInboundTransportationFee" => "Fresh Inbound Transportation Fee",
				    	"HighVolumeListingFee" => "High-volume listing fee",
				    	"ImagingServicesFee" => "Amazon Imaging fee",
				    	"MFNPostageFee" => "Easy Ship Fee",
				    	"ReferralFee" => "Referral Fees",
				    	"RefundCommission" => "Refund Administration Fee",
				    	"SalesTaxCollectionFee" => "Tax Calculation Services Fees",
				    	"Subscription" => "Monthly subscription fee",
				    	"TextbookRentalBuyoutFee" => "Purchase of Rented Books",
				    	"TextbookRentalExtensionFee" => "Rental Extensions",
				    	"TextbookRentalServiceFee" => "Rental Book Service Fee",
				    	"VariableClosingFee" => "Closing Fees",
				    	"BubblewrapFee" => "FBA Prep Service Fees (Bubble Wrap)",
				    	"FBACustomerReturnPerOrderFee" => "Returns Processing Fee-Order Handling",
				    	"FBACustomerReturnPerUnitFee" => "Returns Processing Fee-Pick & Pack",
				    	"FBACustomerReturnWeightBasedFee" => "Returns Processing Fee-Weight Handling",
				    	"FBADisposalFee" => "Inventory Disposals",
				    	"FBAFulfillmentCODFee" => "Fee for cash on delivery",
				    	"FBAInboundConvenienceFee" => "Inventory Placement Service Fees",
				    	"FBAInboundDefectFee" => "Unplanned Prep Service Fees",
				    	"FBAInboundTransportationFee" => "FBA Amazon-Partnered Carrier Shipment Fee/ Inbound Transportation Charge",
				    	"FBAInboundTransportationProgramFee" => "FBA Inbound Transportation Program Fee",
				    	"FBALongTermStorageFee" => "FBA Long-Term Storage Fees",
				    	"FBAOverageFee" => "FBA Inventory Storage Overage Fee",
				    	"FBAPerOrderFulfillmentFee" => "FBA Per Order Fulfillment Fee",
				    	"FBAPerUnitFulfillmentFee" => "FBA Fulfillment Fees",
				    	"FBARemovalFee" => "Inventory Removals",
				    	"FBAStorageFee" => "FBA Inventory Storage Fee",
				    	"FBATransportationFee" => "Multi-Channel Fulfillment Weight Handling",
				    	"FBAWeightBasedFee" => "FBA Weight Based Fee",
				    	"FulfillmentFee" => "FBA Fulfillment Fees",
				    	"FulfillmentNetworkFee" => "Cross-Border Fulfillment Fee",
				    	"LabelingFee" => "FBA Label Service Fee",
				    	"OpaqueBaggingFee" => "FBA Prep Service Fees-Adult-Bagging (black or opaque)",
				    	"PolybaggingFee" => "FBA Prep Service Fees (Labeling)",
				    	"SSOFFulfillmentFee" => "Fulfillment Fee",
				    	"TapingFee" => "FBA Taping Fee",
				    	"TransportationFee" => "FBA transportation fee",
				    	"UnitFulfillmentFee" => "Unit Fulfillment Fee"
				    ];


				    $sheet->row($row_num, $header);
				    foreach( $otherFees as $otherFee) {
				    	$fee_description = array_key_exists($otherFee->fee_type, $fee_descriptions) ? $fee_descriptions[$otherFee->fee_type] : "";
				    	
				    	$row = [$otherFee->posted_date, $otherFee->merchant->name, $otherFee->merchant->code, $otherFee->seller_id, $otherFee->fee_reason, $otherFee->fee_type, $fee_description, $otherFee->fee_amount, $otherFee->currency, $otherFee->exchange_rate];

						$row_num += 1;
						$sheet->row($row_num, $row);
				    }

			    });

			})->store($file_type, storage_path('reports'));

		}
		catch(Exception $e)
		{
			var_dump($e);
            return false;
		}
	}

	private function exportProfitByProducts($filename, $file_type, $start_date, $end_date, $bestsellers, $ads_nosell_products) {

		try{

			return Excel::create($filename, function($excel) use($start_date, $end_date, $bestsellers, $ads_nosell_products) {

			    // Set the title
			    $excel->setTitle('Products Profit Report');

			    // Chain the setters
			    $excel->setCreator('PL Inventory')
			          ->setCompany('KB');

			    // Call them separately
			    $excel->setDescription('KB PL Inventory Profit Report File');


				$excel->sheet('Products Profit', function($sheet) use($start_date, $end_date, $bestsellers, $ads_nosell_products) {

					$row_num = 1;
				    $header = ['Start Date', 'End Date', 'ASIN', 'Region', 'Name', 'Gross Sales', 'Payout', 'Cost', 'Profit', 'Total Units', 'Total Orders', 'Total Gross', 'Tax', 'Shipping Fee', 'FBA Fee', 'Commission', 'Promo Count', 'Total Promo', 'Total Refund', 'Product Cost', 'Ad Expenses'];

				    $sheet->row($row_num, $header);
				    foreach( $bestsellers as $bestseller) {
			    		$ad_expenses = isset($bestseller->ad_expenses) ? $bestseller->ad_expenses : 0;
			    		$total_refund = isset($bestseller->total_refund) ? $bestseller->total_refund : 0;
			    		$total_gross = isset($bestseller->total_gross) ? $bestseller->total_gross : 0;
			    		$shipping_fee = isset($bestseller->shipping_fee) ? $bestseller->shipping_fee : 0;
			    		$tax = isset($bestseller->tax) ? $bestseller->tax : 0;
							$commission = isset($bestseller->commission) ? $bestseller->commission : 0;
							$vat_fee = isset($bestseller->vat_fee) ? $bestseller->vat_fee : 0;
			    		$fba_fee = isset($bestseller->fba_fee) ? $bestseller->fba_fee : 0;
			    		$total_promo = isset($bestseller->total_promo) ? $bestseller->total_promo : 0;
			    		$total_cost = isset($bestseller->total_cost) ? $bestseller->total_cost : 0;
			    		$total_units = isset($bestseller->total_units) ? $bestseller->total_units : 0;
			    		$total_orders = isset($bestseller->total_orders) ? $bestseller->total_orders : 0;
			    		$promo_count = isset($bestseller->promo_count) ? $bestseller->promo_count : 0;

			    		$asin = $bestseller->ASIN;
			    		$region = $bestseller->region;
			    		if($bestseller->product != null) {
				    		$name = $bestseller->product->name;
				    	}

				    	if(empty($name)) {
				    		$listing = Listing::where('asin', $asin)->where('region', $region)->first();
				    		if(empty($listing)) {
				    			$listing = Listing::where('asin', $asin)->first();
				    		}

				    		$name = $listing ? $listing->item_name : "";
				    	}
			    		

			    		$gross_sales = $total_gross + $shipping_fee;
			    		$payout = $gross_sales + $commission + $vat_fee + $fba_fee + $total_promo + $total_refund;
			    		$cost = $total_cost + $ad_expenses;
			    		$profit = $payout - $cost;


							$row = [$start_date, $end_date, $asin, $region, $name, number_format($gross_sales, 2), number_format($payout, 2), number_format($cost, 2), number_format($profit, 2), $total_units, $total_orders, number_format($total_gross, 2), number_format($tax, 2), number_format($shipping_fee, 2), number_format($vat_fee, 2), number_format($fba_fee, 2), number_format($commission, 2), $promo_count, number_format($total_promo, 2), number_format($total_refund, 2), number_format($total_cost, 2), number_format($ad_expenses, 2)];

							$row_num += 1;
							$sheet->row($row_num, $row);

				    }

				    foreach ($ads_nosell_products as $nosell_ad) {
			    		try {
								$product = Product::where('asin', $nosell_ad->asin)->where('country', $nosell_ad->country)->firstOrFail();
							} catch(\Exception $e){
								$product = null;
							}

							if($product) {
								$asin = $product->asin;
								$name = $product->name;
								$region = $product->country;
								$ad_expenses = $nosell_ad->total;

								$total_gross = $shipping_fee = $tax = 0;
								$gross_sales = $total_gross + $shipping_fee;
								$product_cost = 0;
								$total_refund = 0;
								$payout = $gross_sales + $total_refund;
								$cost = $ad_expenses + $product_cost;
								$profit = $payout - $cost;
						
								$row = [$start_date, $end_date, $asin, $region, $name, $gross_sales, number_format($payout, 2), number_format($cost, 2), number_format($profit, 2), 0, 0, number_format($total_gross, 2), number_format($tax, 2), number_format($shipping_fee, 2), 0, 0, 0, 0, number_format($total_refund, 2), number_format($product_cost, 2), number_format($ad_expenses, 2)];

								$row_num += 1;
								$sheet->row($row_num, $row);
							}	
				    }



				});


			})->store($file_type, storage_path('reports'));

		}
        catch(Exception $e)
        {
        	var_dump($e);
            return false;
        }

	}

	private function exportListingInventoryReport($filename, $file_type, $start_date, $data) {
		try {
			return Excel::create($filename, function($excel) use($start_date, $data) {
				// Set the title
				$excel->setTitle('Listing Inventory Report '. $start_date );

				// Chain the setters
				$excel->setCreator('SFPL Inventory')
							->setCompany('KB');

				// Call them separately
				$excel->setDescription('SFKB PL Listing Inventory Report File');

				$excel->sheet('Products Profit', function($sheet) use($start_date, $data) {

					$row_num = 1;
					$header = [
						"Account name",	"Store name",	"Item_name",	
						"Sku",	"Asin",	"Fulfillable_quantity",	"Unsellable_quantity",	
						"Reserved_quantity",	"Warehouse_quantity",	"Inbound_working_quantity",	
						"Inbound_shipped_quantity",	"Inbound_receiving_quantity",	"Total_quantity",	
						"Product cost",	"Shipping_cost",	"Selling price(USD)",	
						"Date",	"Last_updated_at"
					];
					$sheet->row($row_num, $header);
					$row_num++;
					foreach($data as $row) {
						$sheet->row($row_num, array_values($row));
						$row_num++;
					}
				});
			})->store($file_type, storage_path('reports'));
		} catch(Exception $e) {
			var_dump($e);
      return false;
		}
	}


	// Converts a number into a short version, eg: 1000 -> 1k
	// Based on: http://stackoverflow.com/a/4371114
	function number_format_short( $n, $precision = 2 ) {
		if ($n < 900) {
			// 0 - 900
			$n_format = number_format($n, $precision);
			$suffix = '';
		} else if ($n < 900000) {
			// 0.9k-850k
			$n_format = number_format($n / 1000, $precision);
			$suffix = 'K';
		} else if ($n < 900000000) {
			// 0.9m-850m
			$n_format = number_format($n / 1000000, $precision);
			$suffix = 'M';
		} else if ($n < 900000000000) {
			// 0.9b-850b
			$n_format = number_format($n / 1000000000, $precision);
			$suffix = 'B';
		} else {
			// 0.9t+
			$n_format = number_format($n / 1000000000000, $precision);
			$suffix = 'T';
		}
	// Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
	// Intentionally does not affect partials, eg "1.50" -> "1.50"
		if ( $precision > 0 ) {
			$dotzero = '.' . str_repeat( '0', $precision );
			$n_format = str_replace( $dotzero, '', $n_format );
		}
		return $n_format . $suffix;
	}

	private static function ChangeOrderbyTotalAmountUSD($a, $b)
	{

		if ($a["total_amount_usd"]==$b["total_amount_usd"]) return 0;

		return ($a["total_amount_usd"] > $b["total_amount_usd"]) ? -1:1;

			
	}

	private static function ChangeOrderbyDepositDate($a, $b)
	{

		if ($a["actual_deposit_date"]==$b["actual_deposit_date"]) return 0;

		return ($a["actual_deposit_date"] > $b["actual_deposit_date"]) ? -1:1;

			
	}	

	public function disburseAction(Request $request) {
		$title = 'Disburse Report';

        $sellerId = $request->get('account', 'all');
		$startDate = $request->get('start_date', date('Y-m-d'));
		$endDate = $request->get('end_date', date('Y-m-d'));
        $dateRange = $request->get('date_range', 'Today');
        $marketplace = $request->get('marketplace', 'all');
        $marketplaces = config('app.marketplaces');

        $accounts = Account::getSortedAccounts();
        $activeAccounts = $accounts->filter(function ($account) {
        	return $account->status == 'Active';
        });

        $params = [
        	'startDate' => $startDate,
        	'endDate' => $endDate,
        ];
        if (!empty($marketplace) && $marketplace != 'all') {
            $params['marketplace'] = $marketplace;
        }
        if (!empty($sellerId) && $sellerId != 'all') {
        	$params['seller_id'] = $sellerId;
        }

        $queryConds = Disburse::getQuery($params);
        $disburses = Disburse::with('account')->where($queryConds)->get();
        $disburseStatistics = collect(Disburse::audit($disburses))->sortByDesc('amount');
        $grid = DisburseGrid::grid($disburseStatistics)->render();

        return view('report.disburse', [
        	'title' => $title,
        	'marketplace' => $marketplace,
            'marketplaces' => $marketplaces,
            'account' => $sellerId,
            'accounts' => $activeAccounts,
        	'start_date' => $startDate,
        	'end_date' => $endDate,
            'date_range' => $dateRange,
            'grid' => $grid
        ]);
	}

	public function productPerformanceAction(Request $request) {
		$title = 'Product Performance Report';

        $sellerId = $request->get('account', 'all');
        $startDate = $request->get('start_date', date('Y-m-d', strtotime('-30 days')));
		$endDate = $request->get('end_date', date('Y-m-d'));
        $dateRange = $request->get('date_range', 'Last 30 Days');
        $marketplace = $request->get('marketplace', 'all');
        $marketplaces = config('app.marketplaces');

        $accounts = Account::getSortedAccounts();
        $activeAccounts = $accounts->filter(function ($account) {
        	return $account->status == 'Active';
        });

        $params = [
        	'from_date' => $startDate,
        	'to_date' => $endDate,
        ];
        if (!empty($marketplace) && $marketplace != 'all') {
            $params['country'] = $marketplace;
        }
        if (!empty($sellerId) && $sellerId != 'all') {
        	$params['account_id'] = $sellerId;
        }

        $productPerformancesWithInfo = [];
        $productPerformances = ProductStatistic::getProductPerformance($params);
		$products = Product::with(['users', 'listings'])->where([
            ['discontinued', '=', FALSE],
            ['status', '>', 0],
		])->get();
        foreach ($products as $product) {
            $productInfo = $product->attributesToArray();
            $productInfo['amzLink'] = $product->amazonLink();
            $user = $product->users->first();
            if ($user) {
            	$productInfo['PM'] = $user->name;
            } else {
            	$productInfo['PM'] = 'N/A';
            }

            $listingPriceSum = 0;
            $listingCnt = 0;
            foreach ($product->listings as $listing) {
            	if ($listing->status <= 0 || ($listing->price <= 0 && $listing->sale_price <= 0)) {
            		continue;
            	}

        		if ($listing->sale_price > 0) {
        			$listingPriceSum += $listing->sale_price;
        		} else {
        			$listingPriceSum += $listing->price;
        		}
        		$listingCnt += 1;
            }
            if ($listingCnt > 0) {
            	$productInfo['price'] = round($listingPriceSum / $listingCnt, 2);
            } else {
            	$productInfo['price'] = 'N/A';
            }

            $k = sprintf('%s-%s', $product->country, $product->asin);
            if (array_key_exists($k, $productPerformances)) {
                $productPerformance = $productPerformances[$k];
            } else {
                $productPerformance = ProductStatistic::getDefaultProductPerformance();
            }

            $productPerformancesWithInfo[] = (object) array_merge($productInfo, $productPerformance);
        }

        $productPerformances = collect($productPerformancesWithInfo)->sortByDesc('profit');
        $grid = ProductPerformanceGrid::grid($productPerformances)->render();

        return view('report.product_performance', [
        	'title' => $title,
        	'marketplace' => $marketplace,
            'marketplaces' => $marketplaces,
            'account' => $sellerId,
            'accounts' => $activeAccounts,
        	'start_date' => $startDate,
        	'end_date' => $endDate,
            'date_range' => $dateRange,
            'grid' => $grid
        ]);
	}

	public function disbursementAction(){

		$title = "Disbursement Report";
		
		$accounts = Account::select('name', 'seller_id', 'code')->where('status', 'Active')->get();
		$accounts = $accounts->map(function ($account) {
		    return (object)['name' => $account->name, 'seller_id' => $account->seller_id, 'code' => $account->code];
		});
		$marketplaces = config( 'app.marketplaces' );
		$beginning = Settlement::get_beginning();

		return view('report.disbursement_report', compact('title', 'accounts', 'marketplaces', 'beginning'));


	}

	public function fetchDisbursementRowsAction(Request $request) {


		$start_date = Carbon::parse($request->input('startDate'))->startOfDay();  //2016-09-29 00:00:00.000000
		$end_date = Carbon::parse($request->input('endDate'))->endOfDay(); //2016-09-29 23:59:59.000000		
		
		$deposit_start_date = Carbon::parse($request->input('depositstartDate'))->startOfDay();  //2016-09-29 00:00:00.000000
		$deposit_end_date = Carbon::parse($request->input('depositendDate'))->endOfDay(); //2016-09-29 23:59:59.000000		

		


		$query = Settlement::join( 'seller_accounts', function($join) {
			$join->on('settlements.account_code', '=', 'seller_accounts.code');
		});

		$query->selectRaw("CONVERT_TZ(settlement_start_date,'+00:00','-08:00') as settlement_start_date,
		CONVERT_TZ(settlement_end_date,'+00:00','-08:00') as settlement_end_date,
		CONVERT_TZ(deposit_date,'+00:00','-08:00') as deposit_date,settlements.actual_deposit_date,
		
		total_amount_usd,settlements.currency,region,account_code, settlements.seller_id, name, settlements.id");

		
		$query->where('total_amount_usd','>', 0);
		
		$query->having('settlement_start_date', '>=' , $start_date);
		$query->having('settlement_start_date', '<=' , $end_date);


			if($request->arrayValue){

				$selected_seller_ids = [];
				foreach($request->arrayValue as $val){
		
					$selected_seller_ids[] = $val['id'];
		
				}
		
				$query->whereIn('settlements.seller_id', $selected_seller_ids);

			}
		
			if($request->depositarrayValue){

				$selected_deposit_dates = [];
				foreach($request->depositarrayValue as $val){
		
					$selected_deposit_dates[] = $val['date'];						
				}
		
				$query->whereIn('actual_deposit_date', $selected_deposit_dates);

			}			



		$rows = $query->get ();
		$result = [];	
		$aggregated_amount = 0;
		$no = 1;
	
		foreach($rows as $row) {

			$newrow['name'] = $row['name'];
			$newrow['account_code'] = $row['account_code'];
			$newrow['settlement_start_date'] = $row['settlement_start_date'];
			$newrow['settlement_end_date'] = $row['settlement_end_date'];
			$newrow['deposit_date'] = $row['deposit_date'];
			$newrow['actual_deposit_date'] = $row['actual_deposit_date'];
			$newrow['total_amount_usd'] = $row['total_amount_usd'];
			$aggregated_amount = $aggregated_amount + $row['total_amount_usd'];
			$result['rowData'][] = $newrow;

	
		}
		
		if($result && count($result['rowData']) > 1  )  usort($result['rowData'], 'self::ChangeOrderbyDepositDate');
		
		$toprow['account_code'] = '';
		$toprow['settlement_start_date'] = '';
		$toprow['settlement_end_date'] = '';
		$toprow['deposit_date'] = '';
		$toprow['actual_deposit_date'] = '';
		$toprow['total_amount_usd'] = round($aggregated_amount,2);

		$result['topRowData'][] = $toprow;
		
		$title = "Disbursement Report";
		$result['columnDefs'][] = ['headerName'=> 'Account Name', 'field'=> 'name'];
		$result['columnDefs'][] = ['headerName'=> 'Account Code', 'field'=> 'account_code'];
		$result['columnDefs'][] = ['headerName'=> 'Settlement Start Date', 'field'=> 'settlement_start_date','valueFormatter'=>'if(moment(value).isValid()){ return moment(value).format("YYYY-M-D")}'];
		$result['columnDefs'][] = ['headerName'=> 'Settlement End Date', 'field'=> 'settlement_end_date','valueFormatter'=>'if(moment(value).isValid()){ return moment(value).format("YYYY-M-DD")}'];		
		$result['columnDefs'][] = ['headerName'=> 'Deposit Date', 'field'=> 'actual_deposit_date','valueFormatter'=>'if(moment(value).isValid()){ return moment(value).format("YYYY-M-DD")}'];
		$result['columnDefs'][] = ['headerName'=> 'Amount', 'field'=> 'total_amount_usd','valueFormatter'=>'"$" + Math.floor(value).toString().replace(/(\\d)(?=(\\d{3})+(?!\\d))/g, "$1,")'];


		return response()->json($result);
	}

	public function fetchDisbursementbyAccountAction(Request $request){
//		DB::enableQueryLog();


		$start_date = Carbon::parse($request->input('startDate'))->startOfDay();  //2016-09-29 00:00:00.000000
		$end_date = Carbon::parse($request->input('endDate'))->endOfDay(); //2016-09-29 23:59:59.000000		

		$deposit_start_date = Carbon::parse($request->input('depositstartDate'))->startOfDay();  //2016-09-29 00:00:00.000000
		$deposit_end_date = Carbon::parse($request->input('depositendDate'))->endOfDay(); //2016-09-29 23:59:59.000000		


		$query = Settlement::join( 'seller_accounts', function($join) {
			$join->on('settlements.account_code', '=', 'seller_accounts.code');
		});

		$query->selectRaw("CONVERT_TZ(settlement_start_date,'+00:00','-08:00') as settlement_start_date,
		CONVERT_TZ(settlement_end_date,'+00:00','-08:00') as settlement_end_date,
		CONVERT_TZ(deposit_date,'+00:00','-08:00') as deposit_date, settlements.actual_deposit_date,

		total_amount_usd,settlements.currency,region,account_code, settlements.seller_id, name, settlements.id");


		$query->where('total_amount_usd','>', 0);

		if($request->depositarrayValue){

			$selected_deposit_dates = [];
			foreach($request->depositarrayValue as $val){
	
				$selected_deposit_dates[] = $val['date'];						
			}
	
			//print_r($selected_deposit_dates);

			$query->whereIn('actual_deposit_date', $selected_deposit_dates);

		}	

		$query->orderBy('total_amount_usd', 'desc');

		$query->having('settlement_start_date', '>=' , $start_date);
		$query->having('settlement_start_date', '<=' , $end_date);


		$rows = $query->get ();

	//	dd(DB::getQueryLog());
		$result = [];
		$data = [];

		if(!$rows) return response()->json($data);

		foreach($rows as $row) {

			$data[$row->seller_id]['account_code'] = $row->account_code;
			$data[$row->seller_id]['seller_id'] = $row->seller_id;
			$data[$row->seller_id]['total_amount_usd'][] = $row->total_amount_usd;
			
		}

		
		foreach($data as $seller_id => $value){

			$result[$seller_id]['account_code'] = $data[$seller_id]['account_code'];
			$result[$seller_id]['seller_id'] = $data[$seller_id]['seller_id'];			
			$result[$seller_id]['total_amount_usd'] = array_sum($data[$seller_id]['total_amount_usd']);
			

		}

	
		if(count($result)>1)  usort($result, 'self::ChangeOrderbyTotalAmountUSD');

		$k=1;

		$data = [];

		if($result){
			foreach($result as $val){

				$data[] = ["id"=> $val['seller_id'], "name" =>  $val['account_code']."   (".( ReportController::number_format_short($val['total_amount_usd'])).")", "value"=>$val['seller_id']];
				

			}
		}

		//return total_amount_usd by seller id from settlement table
		return response()->json($data);

	}

	public function fetchDisbursementbyDepositDateAction(Request $request){
		

		$start_date = Carbon::parse($request->input('startDate'))->startOfDay();  //2016-09-29 00:00:00.000000
		$end_date = Carbon::parse($request->input('endDate'))->endOfDay(); //2016-09-29 23:59:59.000000		

		$deposit_start_date = Carbon::parse($request->input('depositstartDate'))->startOfDay();  //2016-09-29 00:00:00.000000
		$deposit_end_date = Carbon::parse($request->input('depositendDate'))->endOfDay(); //2016-09-29 23:59:59.000000		

		$arrayValue = $request->arrayValue;



		$query = Settlement::join( 'seller_accounts', function($join) {
			$join->on('settlements.account_code', '=', 'seller_accounts.code');
		});

		$query->selectRaw("(CONVERT_TZ(settlement_start_date,'+00:00','-08:00')) as settlement_start_date,
		(CONVERT_TZ(settlement_end_date,'+00:00','-08:00')) as settlement_end_date,
		(CONVERT_TZ(deposit_date,'+00:00','-08:00')) as deposit_date, settlements.actual_deposit_date, total_amount_usd,settlements.currency,region,account_code, settlements.seller_id, name, settlements.id");

		if($request->arrayValue){

			$selected_seller_ids = [];
			foreach($request->arrayValue as $val){
	
				$selected_seller_ids[] = $val['id'];
	
			}
	
			$query->whereIn('settlements.seller_id', $selected_seller_ids);

		}

		$query->where('total_amount_usd','>', 0);

		$query->orderBy('total_amount_usd', 'desc');

		$query->having('settlement_start_date', '>=' , $start_date);
		$query->having('settlement_start_date', '<=' , $end_date);


		$rows = $query->get ();

		//dd(DB::getQueryLog());
		$result = [];
		$data = [];

		if(!$rows) return response()->json($data);

		foreach($rows as $row) {

			$data[$row->actual_deposit_date]['account_code'] = $row->account_code;
			$data[$row->actual_deposit_date]['actual_deposit_date'] = $row->actual_deposit_date;
			$data[$row->actual_deposit_date]['total_amount_usd'][] = $row->total_amount_usd;
			
		}

		
		foreach($data as $actual_deposit_date => $value){

			$result[$actual_deposit_date]['account_code'] = $data[$actual_deposit_date]['account_code'];
			$result[$actual_deposit_date]['actual_deposit_date'] = $data[$actual_deposit_date]['actual_deposit_date'];			
			$result[$actual_deposit_date]['total_amount_usd'] = array_sum($data[$actual_deposit_date]['total_amount_usd']);
			

		}

		if(count($result)>1) usort($result, 'self::ChangeOrderbyDepositDate');
		

		$k=1;

		$data = [];

		foreach($result as $val){

			$data[] = ["date"=> $val['actual_deposit_date'], "deposit" =>  $val['actual_deposit_date']."   (".( ReportController::number_format_short($val['total_amount_usd'])).")", "value"=>$val['actual_deposit_date']];
			

		}

		//return total_amount_usd by seller id from settlement table
		return response()->json($data);


	}

	public function fetchDisbursementAction(Request $request) {


			$start_date = Carbon::parse($request->input('startDate'))->startOfDay();  //2016-09-29 00:00:00.000000
			$end_date = Carbon::parse($request->input('endDate'))->endOfDay(); //2016-09-29 23:59:59.000000		



		$deposit_dates = [];

		foreach($request->depositarrayValue as $value){

			$deposit_dates[] = $value['date'];

		}
		if($deposit_dates){

			$deposit_start_date = min($deposit_dates);
			$deposit_end_date = max($deposit_dates);
		}else{
			$deposit_start_date = null;
			$deposit_end_date = null;
		}
		

		$query = Settlement::join( 'seller_accounts', function($join) {
			$join->on('settlements.account_code', '=', 'seller_accounts.code');
		});

		$query->selectRaw("CONVERT_TZ(settlement_start_date,'+00:00','-08:00') as settlement_start_date,
		CONVERT_TZ(settlement_end_date,'+00:00','-08:00') as settlement_end_date,
		CONVERT_TZ(deposit_date,'+00:00','-08:00') as deposit_date, settlements.actual_deposit_date,

		total_amount_usd,settlements.currency,region,account_code, settlements.seller_id, name, settlements.id");

		$query->where('total_amount_usd','>', 0);

		$query->orderBy('actual_deposit_date', 'asc');
		
		$query->having('settlement_start_date', '>=' , $start_date);
		$query->having('settlement_start_date', '<=' , $end_date);
		
		if($deposit_start_date){

			$query->where('actual_deposit_date', '>=' , $deposit_start_date);
			$query->where('actual_deposit_date', '<=' , $deposit_end_date);
		}

		$rows = $query->get ();
		
		$result = [];
		$data = [];

		if(!$rows) return response()->json($data);

		foreach($rows as $row) {

			$data[$row->actual_deposit_date]['account_code'] = $row->account_code;
			$data[$row->actual_deposit_date]['actual_deposit_date'] = $row->actual_deposit_date;
			$data[$row->actual_deposit_date]['total_amount_usd'][] = $row->total_amount_usd;
			
		}

	
		foreach($data as $actual_deposit_date => $value){

			$result[$actual_deposit_date]['account_code'] = $data[$actual_deposit_date]['account_code'];
			$result[$actual_deposit_date]['actual_deposit_date'] = $data[$actual_deposit_date]['actual_deposit_date'];			
			$result[$actual_deposit_date]['total_amount_usd'] = array_sum($data[$actual_deposit_date]['total_amount_usd']);
			
		}


		//if(count($result)>1)  usort($result, 'self::ChangeOrderbyDepositDate');

		$k=1;

		$data = [];

		if($result){
			foreach($result as $val){

				$data[] = [$val['actual_deposit_date'], $val['total_amount_usd']];
				

			}
		}
	

		return response()->json($data);

	}


	public function fetchDisbursementbyAccountChartAction(Request $request){
		//		DB::enableQueryLog();
		
		
				$start_date = Carbon::parse($request->input('startDate'))->startOfDay();  //2016-09-29 00:00:00.000000
				$end_date = Carbon::parse($request->input('endDate'))->endOfDay(); //2016-09-29 23:59:59.000000		
		
				$deposit_start_date = Carbon::parse($request->input('depositstartDate'))->startOfDay();  //2016-09-29 00:00:00.000000
				$deposit_end_date = Carbon::parse($request->input('depositendDate'))->endOfDay(); //2016-09-29 23:59:59.000000		
		
		
				$query = Settlement::join( 'seller_accounts', function($join) {
					$join->on('settlements.account_code', '=', 'seller_accounts.code');
				});
		
				$query->selectRaw("CONVERT_TZ(settlement_start_date,'+00:00','-08:00') as settlement_start_date,
				CONVERT_TZ(settlement_end_date,'+00:00','-08:00') as settlement_end_date,
				CONVERT_TZ(deposit_date,'+00:00','-08:00') as deposit_date, settlements.actual_deposit_date,
		
				total_amount_usd,settlements.currency,region,account_code, settlements.seller_id, name, settlements.id");
		
		
				$query->where('total_amount_usd','>', 0);
		
				if($request->depositarrayValue){
		
					$selected_deposit_dates = [];
					foreach($request->depositarrayValue as $val){
			
						$selected_deposit_dates[] = $val['date'];						
					}
			
					//print_r($selected_deposit_dates);
		
					$query->whereIn('actual_deposit_date', $selected_deposit_dates);
		
				}	
		
				$query->orderBy('total_amount_usd', 'desc');
		
				$query->having('settlement_start_date', '>=' , $start_date);
				$query->having('settlement_start_date', '<=' , $end_date);
		
		
				$rows = $query->get ();
		
			//	dd(DB::getQueryLog());
				$result = [];
				$data = [];
		
				if(!$rows) return response()->json($data);
		
				foreach($rows as $row) {
		
					$data[$row->seller_id]['account_code'] = $row->account_code;
					$data[$row->seller_id]['seller_id'] = $row->seller_id;
					$data[$row->seller_id]['total_amount_usd'][] = $row->total_amount_usd;
					
				}
		
				
				foreach($data as $seller_id => $value){
		
					$result[$seller_id]['account_code'] = $data[$seller_id]['account_code'];
					$result[$seller_id]['seller_id'] = $data[$seller_id]['seller_id'];			
					$result[$seller_id]['total_amount_usd'] = array_sum($data[$seller_id]['total_amount_usd']);
					
		
				}
		
			
				if(count($result)>1)  usort($result, 'self::ChangeOrderbyTotalAmountUSD');
		
				$k=1;
		
				$data = [];
		
				if($result){
					foreach($result as $val){
		
						$data[] = [$val['account_code'], round($val['total_amount_usd'],2)];
						
		
					}
				}
		
				//return total_amount_usd by seller id from settlement table
				return response()->json($data);
		
			}	

	
}


