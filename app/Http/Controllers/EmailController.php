<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Grids\EmailGrid;
use App\Grids\EmailTemplateGrid;
use App\Email;
use App\EmailTemplate;
use App\Product;

use App\Grids\ReviewerProfileGrid;

use DB;

class EmailController extends Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->middleware('auth');
	}

    function indexAction(Request $request) {

    	$this->authorize('manage-emails');

    	$title = "Manage Emails";

		$grid = EmailGrid::grid()->render();

		return view('email.index', ['grid' => $grid, "title"=>$title]);
    }

    function templateAction(Request $request) {

    	$this->authorize('manage-emails');
    	
    	$title = "Manage Email Templates";

		$grid = EmailTemplateGrid::grid()->render();

		return view('email.templates', ['grid' => $grid, "title"=>$title]);
    }

    function addTemplateAction(Request $request) {
    	$this->authorize('manage-emails');

    	$title = "Add Email Template";

    	if( $data = $request->all()){
			
			$email_template = $this->create($request->all());

			$is_general = $data['general'];

    		if($is_general == 0 && isset($data['products'])) {
				foreach ( explode(",", $data['products']) as $pid)
				{
					$product = Product::find($pid);
					$product->need_review = 1;
					$product->save();

					$email_template->products()->attach($pid);
				}
			}

			return redirect()->route('email.templates');
		}

		$email_template = new EmailTemplate;
		return view('email.add_template', ['email_template' => $email_template]);

    }

    function editTemplateAction(Request $request, $template_id) {
    	$this->authorize('manage-emails');

    	$title = "Edit Email Template";

    	$email_template = EmailTemplate::where('id',$template_id)->firstOrFail();

    	if( $data = $request->all()){

			$email_template->type = $data['type'];
			$email_template->subject = $data['subject'];
			$email_template->message = $data['message'];
			$email_template->general = $data['general'];
			$email_template->active = $data['active'];
			$email_template->save();

			//remove all current products
			$email_template->products()->detach();

			$is_general = $data['general'];
    		if($is_general == 0 && isset($data['products'])) {
				foreach ( explode(",", $data['products']) as $pid)
				{
					$product = Product::find($pid);
					$product->need_review = 1;
					$product->save();

					$email_template->products()->attach($pid);
				}
			}

			session()->flash('msg','Email Template has been updated successfully.');
			return redirect()->route('email.templates');
		}

		$template = 'email.edit_template';
		return view($template, ['email_template' => $email_template]);

    }

	function deleteTemplateAction(Request $request, $template_id) {
		$template = EmailTemplate::where('id',$template_id)->firstOrFail();
		
		
		DB::transaction(function () use($template) {
			
			
			$template->delete();
			
		});
			
			session()->flash('msg',"Email Template has been deleted.");
			
			return redirect()->route("email.templates");
	}

	function ajaxSaveTemplateAction(Request $request) {
		$value = $request->get('value');
		list($type,$id) = explode('-', $request->get('pk'));
		
		try{
			EmailTemplate::where('id', $id)->update([$type => $value]);
				
		} catch(\Exception $e) {
			print $e->getMessage();
		}

	}

	public function profileAction(Request $request) {

		$title = "Manage Reviewer Profiles";

		$grid = ReviewerProfileGrid::grid();

		return view('email.profiles', ['grid' => $grid, "title"=>$title]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	protected function create(array $data)
	{
		return EmailTemplate::create([
				'type' => $data['type'],
				'subject' => $data['subject'],
				'message'=>$data['message'],
				'general'=>$data['general'],
		]);
	}

}
