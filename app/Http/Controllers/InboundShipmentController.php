<?php 
namespace App\Http\Controllers;
use App\User;
use App\Product;
use App\Listing;
use App\Http\Controllers\Controller;
use DB;
use Grids;
use Html;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Grids\ListingGrid;
use App\OrderItem;
use App\MWS\MWSClient;
use App\InboundShipment;
use App\Grids\InboundShipmentGrid;



class InboundShipmentController extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->middleware('auth');
	}
	
	public function indexAction(Request $request){
		$grid = InboundShipmentGrid::grid()->render();
		
		return view('shipments.index',['grid'=>$grid]);
		
	}
	
	public function createShipmentPlanAction(Request $request,$id=null) {
		
		
		if($id) {
			$listing = Listing::with(['product'])->where('id',$id)->firstOrFail();
			$sku = $listing->sku;
		} else {
			$listing = null;
		}
		
		if($data = $request->get('data')) {
			if(empty($id)) {
				$id = $data['id'];
			}
			
			
			
			$config = [
					'Seller_Id' => $listing->seller_id,
					'Marketplace_Id'=>config('app.marketplaceIds')[strtoupper($listing->country)],
					'Access_Key_ID' => config('app.mws.access_key'),
					'Secret_Access_Key' => config('app.mws.secret_key'),
					'MWSAuthToken' => $listing->merchant->mws_auth,
			];
			
	
			$client = new MWSClient($config);
			$shipFromAddress = $data['shipFromAddress'];
			
			$shipmentName = $data['shipment_name'];
			$qty = $data['qty'];
			//create plan
			$response = $client->createInboundShipmentPlan($sku,$qty,0,$shipFromAddress,$listing->country);

			if (!empty($response)) {
				
				$shipment_response = $client->createInboundShipment($sku,$qty,$response[0]["ShipmentId"],$shipmentName,$shipFromAddress,
						$response[0]["DestinationFulfillmentCenterId"],
						$response[0]["LabelPrepType"]);

				$inbound_shipment = New InboundShipment();
				$inbound_shipment->seller_id = $listing->seller_id;
				$inbound_shipment->shipment_id = $shipment_response;
				$inbound_shipment->shipment_name = $shipmentName;
				$inbound_shipment->shipment_status = $data['shipment_status'];
				$inbound_shipment->cost = $data['cost'];
				$inbound_shipment->shipment_cost = $data['shipment_cost'];
				
				$inbound_shipment->label_prep_type =  $response[0]["LabelPrepType"];
				$inbound_shipment->seller_sku = $sku;
				$inbound_shipment->quantity_shipped = $qty;
				$inbound_shipment->date_created = date('Y-m-d H:i:s');
				
				
				if($request->ajax()) {
					session()->flash('msg','Inbound shipment '.$inbound_shipment->shipment_id.' has been created successfully.');
					echo 1;
				}else{
					session()->flash('msg','Inbound shipment '.$inbound_shipment->shipment_id.' has been created successfully.');
					
					return redirect()->route('inbound_shipments');
				}
			}
		}
		
		return view('shipments.createplan',['listing'=>$listing]);
	}
	
	
	public function deleteAction(Request $request,$id) {
		$shipment = InboundShipment::with(['listing','merchant'])->where('id',$id)->firstOrFail();
		
		
		$config = [
				'Seller_Id' => $shipment->seller_id,
				'Marketplace_Id'=>config('app.marketplaceIds')[strtoupper($shipment->listing->country)],
				'Access_Key_ID' => config('app.mws.access_key'),
				'Secret_Access_Key' => config('app.mws.secret_key'),
				'MWSAuthToken' => $shipment->merchant->mws_auth,
		];
		
		
		$client = new MWSClient($config);
		
		//update plan
		$response = $client->updateInboundShipmentStatus($shipment->seller_sku,$shipment->quantity_shipped,$shipment->shipment_id,'DELETED');
		
		if (!empty($response)) {
			
			
			$shipment->shipment_status = 'DELETED';
			$shipment->save();
			session()->flash('msg','Inbound shipment '.$shipment->shipment_id.' has been set as DELETED successfully.');
			
			return redirect()->back();
		}
		
	}
	
	function ajaxsaveAction(Request $request) {
		$value = $request->get('value');
		list($type,$id) = explode('-', $request->get('pk'));
		
		
		
		// 		var_dump($type,$id);
		try{
			InboundShipment::where('id', $id)->update([$type => $value]);
			echo 1;
		}catch (\Exception $e) {
			print $e->getMessage();
		}
		
		
		
		
	}
	
}