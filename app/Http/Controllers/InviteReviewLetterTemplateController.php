<?php

namespace App\Http\Controllers;

use App\InviteReviewLetterTemplate;
use Illuminate\Http\Request;
use App\Grids\InviteLetterTemplateGrid;

class InviteReviewLetterTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grid = (new InviteLetterTemplateGrid())->generateGrid();
        return view('invite-review-letter-template.index', [
            'grid' => $grid
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('invite-review-letter-template.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $letterTemplate = new InviteReviewLetterTemplate($request->all());
        $letterTemplate->status = 1;
        $letterTemplate->save();
        return redirect()->route('invite-review-letter-template.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InviteReviewLetterTemplate  $inviteReviewLetterTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(InviteReviewLetterTemplate $inviteReviewLetterTemplate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InviteReviewLetterTemplate  $inviteReviewLetterTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(InviteReviewLetterTemplate $inviteReviewLetterTemplate)
    {
        return view('invite-review-letter-template.edit', [
            'inviteReviewLetterTemplate' => $inviteReviewLetterTemplate
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InviteReviewLetterTemplate  $inviteReviewLetterTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InviteReviewLetterTemplate $inviteReviewLetterTemplate)
    {
        $inviteReviewLetterTemplate->update($request->all());
        return redirect()->route('invite-review-letter-template.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InviteReviewLetterTemplate  $inviteReviewLetterTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(InviteReviewLetterTemplate $inviteReviewLetterTemplate)
    {
        $inviteReviewLetterTemplate->status = -1;
        $inviteReviewLetterTemplate->save();
        return response('OK', 200);
    }
}
