<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grids\BrandGrid;
use App\Grids\ProductsOfBrandGrid;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Brand;
class BrandController extends Controller
{

    /**
	 * Brands Page
	 * @param Request $request
	 * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
	 */
	public function indexAction(Request $request)
	{
		 
		
		$this->authorize('manage-products');
		
		//page title 
		$title = "Manage Products";
		 
		//brand grid
		$grid = BrandGrid::grid()->render();
	
		
		//product manager list, used for dropdown list when editing product mananger
		$managers = User::productManagerList('id-list');
		
		return view('brand.index', ['grid' => $grid,"title"=>$title,'managerSources'=>$managers]);
    }
    
	public function productsAction(Request $request)
	{
		 
		
		$this->authorize('manage-products');
		
		//page title 
		$title = "Manage Products";
		 
		//brand grid
		$grid = ProductsOfBrandGrid::grid($request->brand)->render();
	
		
		//product manager list, used for dropdown list when editing product mananger
		$managers = User::productManagerList('id-list');
		
		return view('product.index', ['grid' => $grid,"title"=>$title,'managerSources'=>$managers]);
	}

	public function destroy(Request $request)
    {   
        Brand::where('id', $request->id)->delete();
        return response("DELETED", 200);
    }
}
