<?php

namespace App\Http\Controllers;

use App\ProductReviewOverview;
use Illuminate\Http\Request;
use App\Product;

class ProductReviewOverviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $asin = $request->input('asin');
        $country = $request->input('country');
        $review_quantity = $request->input('reviews');
        $review_rating = $request->input('rating');
        $date = date('Y-m-d');
        ProductReviewOverview::add($asin, $country, $review_quantity, $review_rating, $date);
        return response('ok', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductReviewOverview  $productReviewOverview
     * @return \Illuminate\Http\Response
     */
    public function show(ProductReviewOverview $productReviewOverview)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductReviewOverview  $productReviewOverview
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductReviewOverview $productReviewOverview)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductReviewOverview  $productReviewOverview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductReviewOverview $productReviewOverview)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductReviewOverview  $productReviewOverview
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductReviewOverview $productReviewOverview)
    {
        //
    }

    function getProducts(Request $request) {
        $products = Product::select('asin','country')->where('status', '>=', 0)->get();
        $data = $products->map(function($item, $key) {
            return [$item->asin, $item->country];
        })->all();
        return response()->json($data);
    }
}
