<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Product;
use App\PurchaseRequest;
use App\PurchaseSchedule;
use App\Region;
use App\Account;
use App\Listing;
use App\User;
use App\Order;

class UtilController extends Controller {
	
	public function __construct() {
		$this->middleware ( 'auth' );
	}
	
	
	public function ajaxProductsAction(Request $request){
		
		$t =$request->get('t','id');
		
		$query = Product::select('id','name','asin','country')->orderBy('name','asc');
		
		if(auth()->user()->role == "product manager") {
			$query->whereHas('users',function($q){
				$q->where('user_id',Auth::user()->id);
			});
		}

		
		if($q = $request->get('q')) {
			$query->whereRaw('status = 1 and (asin like "'.$q.'%'.'" or name like "'.$q.'%'.'")');
			// $query->where(function ($query) {
   //              $query->where('asin','like',$q.'%')->orWhere('name','like',$q.'%');
   //          });
		}
		
		$data_query = $query;

		$total_count = count($query->get());

		if($page = $request->get('page')) {
			$offset = 15 * ($page - 1);
			$data_query->offset($offset)->limit(15);
		}

		$products = $data_query->get();
		
		$data = [];
		foreach ($products as $product) {
			if($t == 'asin') {
				$data[] = ['id'=>$product->asin,'text'=>$product->asin.' - '.$product->name,'short_text'=>$product->asin. ' - '.$product->country];
			} else {
				$data[] = ['id'=>$product->id,'text'=>$product->asin. ' - '.$product->country.' - '.$product->name,'short_text'=>$product->asin. ' - '.$product->country];
			}
			
		}
		
		return response()->json(['results'=>$data, 'total_count'=>$total_count]);
	}
	
	
	public function ajaxProductManagersAction(Request $request){
		
		$t =$request->get('t','id');
		
		$query = User::select('id','name')->whereIn('role',['product manager','admin'])->orderBy('name','asc');
		
		
		
		if($q = $request->get('q')) {
			$query->where('name','like','%'.$q.'%');
		}
		
		if($q = $request->get('term')) {
			$query->where('name','like','%'.$q.'%');
		}
		
		
		$users =  $query->get();
		
		
		$data = [];
		foreach ($users as $user) {
			
			$data[] = ['id'=>$user->id,'text'=>$user->name];
			
			
		}
		if($q = $request->get('term')) {
			echo json_encode($data);
		} else {
			echo json_encode(['results'=>$data]);
		}
		
	}
	
	public function ajaxProductAddcostAction(Request $request){
		
		if($id = $request->get('id')) {

			$cost = $request->get('cost');
			$shipping_cost = $request->get('shipping_cost');

			if((is_null($cost) || $cost === '') or (is_null($shipping_cost) || $shipping_cost === '')) {
				return response()->json(['success'=>False, 'error_msg'=>'Please input product cost and shipping cost.']);
			}

			if(!is_numeric($cost) or !is_numeric($shipping_cost)) {
				return response()->json(['success'=>False, 'error_msg'=>'Please input numeric value.']);
			}

			try {
				
				$product = Product::find($id);

				$product->cost = $cost;

				$product->shipping_cost = $shipping_cost;

				$product->save();
				
				return response()->json(['success'=>True, 'id'=>$id]);

			} catch (Exception $e) {
				
				return response()->json(['success'=>False, 'error_msg'=>$e->getMessage()]);

			}
			
		}
		
	
	}


	public function ajaxAccountsAction(Request $request){
		$query = Account::select('code','seller_id')->orderBy('code','asc');
		
		
		if($q = $request->get('q')) {
			$query->where('code','like','%'.$q.'%');
		}
		
	
		$accounts =  $query->get();
		
		$data = [
				['id'=>0,'text'=>'All Accounts']
		];
		
		foreach ($accounts as $account) {
			$data[] = ['id'=>$account->seller_id,'text'=>$account->code];
		}
		
		echo json_encode(['results'=>$data]);
	}
	
	public function ajaxMarketplacesAction(Request $request,$account=null) {
		
		$marketplaces = [['id'=>'all','text'=>'All Marketplaces']];
		
		
		if(!empty($account)) {
			//marketplaces
			$account = Account::where('seller_id',$account)->firstOrFail();
			
			foreach (explode(',', $account->marketplaces) as $c) {
				$mp = ucfirst(config('app.marketplaces')[strtoupper(trim($c))]);
				$marketplaces[] = ['id'=>$mp,'text'=>$mp];
			}
		}else if($asin = $request->get('asin')) {
			$countries = Product::where('asin',$asin)->select('country')->get()->toArray();
			
			foreach ($countries as $c) {
				
				$mp = ucfirst(config('app.marketplaces')[strtoupper($c['country'])]);
				$marketplaces[] = ['id'=>$mp,'text'=>$mp];
			}
			
		}else {
			foreach (config('app.marketplaces') as $mp) {
				$mp = ucfirst($mp);
				$marketplaces[] = ['id'=>$mp,'text'=>$mp];
			}
		}
		
		echo json_encode(['results'=>$marketplaces]);
	}


	public function ajaxSendTestEmailAction(Request $request) {

		if($template_id = $request->get('template_id')) {
			Order::ajaxSendTestEmail($template_id);
		}
	}

	public function insertProductImageAction(Request $request) {
		$product_id = $request->get('product_id');
		$image = $request->get('image');
		$destinationPath = public_path('images') . '/' . $product_id . '.jpg';
		if(file_put_contents($destinationPath, file_get_contents($image))){
			$product = Product::find($product_id);
			$product->small_image = url('/images/' . $product_id . '.jpg');
			$product->save();

			return response()->json(['success'=>1, 'product_image_url'=>$product->small_image]);
		} else {
			return response()->json(['success'=>0, 'error_msg'=>"Unable to save the file."]);
		}


	}
	
}