<?php 
namespace App\Http\Controllers;
use App\User;

use App\Http\Controllers\Controller;
use DB;
use Grids;
use Html;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Account;
use App\Product;
use App\Listing;
use App\Inventory;


use SellerWorks\Amazon\Credentials\Credentials as MWSCredentials;
use SellerWorks\Amazon\Orders\Client as MWSClient;
use App\Grids\AccountGrid;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client as GuzzleClient;


class AccountController extends Controller
{
	
	public function __construct()
	{
		$this->middleware('auth');
		parent::__construct();
		
	}
	
	
	/**
	 * Show the profile for the given user.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function indexAction(Request $request)
	{
		$this->authorize('manage-accounts');

		$title = "Manage Accounts";
		$input = $request->get("Accounts");
		$grid = AccountGrid::grid()->render();
			
		return view('account.index', ['grid' => $grid,"title"=>$title,'all_managers'=>User::accountManagerList()]);
	}
	
	
	
	
	
	function addAction(Request $request) {
		
		$this->authorize('manage-accounts');
		
		if( $data = $request->get("data")){
			// 			var_dump($data );die();
			try {
				DB::transaction(function () use($data) {
					$account = new Account;
// 					$account->code =$data['code'];
					$account->seller_id =$data['seller_id'];
					$account->region =$data['region'];
					$account->name=$data['name'];
					$account->code=$data['code'];
					$account->status = $data['status'];
					$account->mws_auth=$data['mws_auth'];
					if(isset($data['only_fba']) && $data['only_fba'] == "Yes") {
						$account->only_fba = 1;
					} else {
						$account->only_fba = 0;
					}
					$account->marketplaces=implode(",", $data['marketplaces']);
					$account->seller_email=$data['seller_email'];
					// $account->seller_email_password=$data['seller_email_password'];
					$account->smtp_host=$data['smtp_host'];
					$account->smtp_username=$data['smtp_username'];
					$account->smtp_password=$data['smtp_password'];
// 					$account->currency=$data['currency'];
					$account->disburse_enabled = $data['disburse_enabled'];
					$account->min_disburse_amount = $data['min_disburse_amount'];
					
					$account->save();
					
					
					if(isset($data['managers'])) {
						foreach ( $data['managers'] as $uid)
						{
							$account->users()->attach($uid);

							# attach manager to account's products
							// foreach($account->listings as $listing) {
							// 	$product = Product::find($listing->product_id);
							// 	$product->users()->syncWithoutDetaching($uid);
							// }
						}
					}
					
					
				});
					
					session()->flash('msg',"Account has been successfully created.");
					return 1;	
					//return redirect()->route("accounts");
					
			} catch ( \Exception $e ) {
				session()->flash('msg',$e->getMessage());
			}
		} else {
			$account = new Account;
		}
		
		return view('account.add', ['account' => $account,'all_managers'=>User::accountManagerList(),'account_managers'=>$account->users()->pluck('users.id')->toArray()]);
		
	}
	
	function editAction(Request $request, $pid) {
		
		
		$account = Account::where('id',$pid)->firstOrFail();
		
		$this->authorize('manage-account',$account);
		
		if( $data = $request->get("data")){
// 			var_dump($data );die();
			try {
				DB::transaction(function () use($data,$account) {
					
					$account->name=$data['name'];
					$account->code=$data['code'];
// 					$account->currency=$data['currency'];
					$account->status = $data['status'];
					if(isset($data['only_fba']) && $data['only_fba'] == "Yes") {
						$account->only_fba = 1;
					} else {
						$account->only_fba = 0;
					}
					$account->marketplaces=isset($data['marketplaces']) ? implode(",", $data['marketplaces']) : "";
					$account->mws_auth=$data['mws_auth'];
					$account->seller_email=$data['seller_email'];
					$account->smtp_host=$data['smtp_host'];
					$account->smtp_username=$data['smtp_username'];
					$account->smtp_password=$data['smtp_password'];
// 					$account->secret_key=$data['secret_key'];
					$account->disburse_enabled = $data['disburse_enabled'];
					$account->min_disburse_amount = $data['min_disburse_amount'];
					
					$account->save();
					
					// set inventory and listing status as 0 if account is suspended
					if($account->status != "Active") {
						try {
							Inventory::where('seller_id', $account->seller_id)->update(['status' => 0]);
							Listing::where('seller_id', $account->seller_id)->update(['status' => 0]);
						} catch (\Exception $e) {
							echo 'Caught exception: ',  $e->getMessage(), "\n";
						}
					}

					//remove all current before add new ones
					$account->users()->detach();
					
					if(isset($data['managers'])) {
						foreach ( $data['managers'] as $uid)
						{
							$account->users()->attach($uid);
							
							# attach manager to account's products
							// foreach($account->listings as $listing) {
							// 	$product = Product::find($listing->product_id);
							// 	$product->users()->syncWithoutDetaching($uid);
							// }
						}
					}
					
					
				});
					
					session()->flash('msg',"Account has been successfully updated.");
					
					return redirect()->route("accounts");
					
			} catch ( \Exception $e ) {
				session()->flash('msg',$e->getMessage());
			}
		}
		
		$view_template = $request->ajax()?'account.form.edit':'account.edit';
		return view($view_template, ['ajax'=>$request->ajax(),'account' => $account,'all_managers'=>User::accountManagerList(),'account_managers'=>$account->managerList()]);
		
	}
	
	
	function deleteAction(Request $request, $pid) {
		$account = Account::where('id',$pid)->firstOrFail();
		$this->authorize('manage-account',$account);
		
		
		DB::transaction(function () use($account) {
			
			
			
			$account->delete();
			
		});
			
			session()->flash('msg',"Account has been deleted.");
			
			return redirect()->route("accounts");
	}
	
	function checkmwsAction(Request $request, $pid) {
		$account = Account::where('id',$pid)->firstOrFail();
		$this->authorize('manage-account',$account);
		
		// Create the API client.
		$credentials = new MWSCredentials($account->seller_id, $account->access_key, $account->secret_key);
		$client = new MWSClient($credentials);
		
		// Send request to the API.
		$status = $client->GetServiceStatus();
		
		$account->mws_status = $status->Status;
		$account->save();
		// Output service level information.
		echo $status->Status;
		
	}
	
	function advertisingapiAuthAction(Request $request, $pid) {

		session(['adv_account_id' => $pid]);

		$goto_url = 'https://www.amazon.com/ap/oa?client_id='. config("app.lwa_client_id") . '&scope=cpc_advertising:campaign_management&response_type=code&redirect_uri=' . route( 'account.advertising_api_token');

		return redirect($goto_url);
	}

	function advertisingapiTokenAction(Request $request) {
		// $this->authorize('manage-account',$account);
		
		if( $auth_code = $request->get("code")){

			$client = new GuzzleClient(['base_uri' => 'https://api.amazon.com']);
			$res = $client->request('POST', '/auth/o2/token', [
	            'form_params' => [
	            	'grant_type'=>'authorization_code',
	                'code' => $auth_code,
	                'redirect_uri' => route('account.advertising_api_token'),
	                'client_id' => config("app.lwa_client_id"),
	                'client_secret' => config("app.lwa_client_secret"),
	            ]
	        ]);


			if($res->getStatusCode() == '200') {
				$content = json_decode($res->getBody());
				$access_token = $content->access_token;
				$refresh_token = $content->refresh_token;

				$account_id = session('adv_account_id');
				$account = Account::where('id',$account_id)->firstOrFail();
				$account->adv_refresh_token = $refresh_token;
				$account->adv_access_token = $access_token;

				$account->save();

				session()->flash('msg',"Account Adverising API is successfully registered.");
			
			} else {

				session()->flash('msg',"Account Adverising API registeration was failed.");

			}

			return redirect()->route("accounts");
			
			
		} else {
			session()->flash('msg',"No authorize code found in url.");
			
			return redirect()->route("accounts");
		}
		
	}

	function ajaxsaveAction(Request $request) {
		$value = $request->get('value');
		list($type, $id) = explode('-', $request->get('pk'));

		$account = Account::where('id', $id)->first();
		if ($account) {
			$account->$type = $value;
			$account->save();
		}
	}
}