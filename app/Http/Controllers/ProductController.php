<?php 
namespace App\Http\Controllers;
use App\User;
use App\Product;
use App\AdReport;
use App\NewRelease;
use App\Http\Controllers\Controller;
use DB;
use Grids;
use Html;
use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Grids\ProductGrid;
use App\Grids\ReviewListGrid;
use App\OrderItem;
use App\Grids\ListingGrid;
use App\Grids\NewReleaseGrid;
use App\Listing;
use App\Review;
use App\SalesRankUpdate;
use App\WarehouseActivity;
use App\ProductCategory;
use App\Helpers\ReviewHelper;

class ProductController extends Controller
{
	
	/**
	 * authencated user only
	 */
	public function __construct()
	{
		parent::__construct();
		$this->middleware('auth');
	}
	
	/**
	 * Product Listing Page
	 * @param Request $request
	 * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
	 */
	public function indexAction(Request $request)
	{
		 
		
		$this->authorize('manage-products');
		
		//page title 
		$title = "Manage Products";
		 
		//product grid
		$grid = ProductGrid::grid()->render();
	
		
		//product manager list, used for dropdown list when editing product mananger
		$managers=  User::productManagerList('id-list');
		$categories = ProductCategory::all();
		return view('product.index', ['grid' => $grid,"title"=>$title,'managerSources'=>$managers, 'categories'=>$categories]);
	}
	
	
	/**
	 * Low inventory product list page
	 * @param Request $request
	 * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
	 */
	public function lowInventoryAction(Request $request){
		
		$grid = ProductGrid::grid($request,['low'=>1])->render();
		
		$title = "Low Inventory Products";
		
		$managers=  User::productManagerList('id-list');
		
		return view('product.index',['grid'=>$grid,'title'=>$title,'managerSources'=>$managers]);
	}
	
	
	
	public function lowSalesAction(Request $request){
		
		$grid = ProductGrid::grid($request,['lowsales'=>1])->render();
		
		$title = "Low Sales Products";
		
		$managers=  User::productManagerList('id-list');
		
		return view('product.index',['grid'=>$grid,'title'=>$title,'managerSources'=>$managers]);
	}
	
	
	
	
	function viewAction(Request $request, $pid,$country=null) {
		$version = $request->get('version', 'v2');
		$box_route = $version == 'v1' ? route('order.call_backs.periodicGrossSales') : route('order.call_backs.periodicGrossSales.v2');
		$productId = $request->id;
		if (preg_match('/^\d+$/', $productId)) {
			$product = Product::find($productId);
		} else {
			$products = Product::select('*')->where('asin',$productId)->get();
			if ($products->count() == 1) {
				$product = $products->first();
			} else {
				return redirect(route('products', ['Products[filters][asin-like]' => $productId]));
			}
		}
		
		
		if(auth()->user()->role == 'product manager assistant') {
			$this->authorize('manage-product',$product);
		}

		$start_date = $request->get('start_date', date('Y-m-d'));
		$end_date = $request->get('end_date', date('Y-m-d'));
		$date_range= $request->get('date_range','Today');
		$params = ['product'=>$product->id,'asin'=>$product->asin,'country'=>$product->country];
		$adsSummary = AdReport::summaryByAsinAndCountry($start_date, $end_date, $product->asin, $product->country);
		$start_date_yesterday = date('Y-m-d',strtotime ("-1 day",strtotime ($start_date)));
		$end_date_yesterday = date('Y-m-d',strtotime("-1 day",strtotime ($end_date) ));

		$start_date_last_week = date('Y-m-d',strtotime ("-1 week",strtotime ($start_date)));
		$end_date_last_week = date('Y-m-d',strtotime("-1 week",strtotime ($end_date) ));

		$start_date_last_month = date('Y-m-d',strtotime ("-1 month",strtotime ($start_date)));
		$end_date_last_month = date('Y-m-d',strtotime("-1 month",strtotime ($end_date) ));

		$start_date_last_year = date('Y-m-d',strtotime ("-1 year",strtotime ($start_date)));
		$end_date_last_year = date('Y-m-d',strtotime ("-1 year",strtotime ($end_date)));


		$orderReport = $product->orderReports($start_date, $end_date);
		
// 		var_dump($orderReport);die();
		$orderReportYesterday = $orderReportLastWeek = $orderReportLastMonth = $orderReportLastYear = null;
		if($start_date == $end_date) {
			$orderReportYesterday = $product->orderReports($start_date_yesterday, $end_date_yesterday);
		}
		$orderReportLastWeek = $product->orderReports($start_date_last_week, $end_date_last_week);
		$orderReportLastMonth = $product->orderReports($start_date_last_month, $end_date_last_month);
		$orderReportLastYear = $product->orderReports($start_date_last_year, $end_date_last_year);
		
		
		$inventoryHistory = $product->inventoryHistory($start_date, $end_date);
		
		
		$qty_cols = [
				'afn_fulfillable_quantity'=>'Fulfillable Qty',
				'afn_unsellable_quantity'=>'Unsellable Qty',
				'afn_reserved_quantity'=>'Reserved Qty',
				'afn_warehouse_quantity'=>'Warehouse Total Qty',
				'afn_inbound_working_quantity'=>'Inbound Working Qty',
				'afn_inbound_shipped_quantity'=>'Inbound Shipped Qty',
				'afn_inbound_receiving_quantity'=>'Inbound Receiving Qty',
				'afn_researching_quantity' => 'Researching Qty',
				'afn_total_quantity'=>'Total Qty',
				
		];
		
		$qtys = [];
		foreach ($qty_cols as $k=>$l) {
			@$qtys[$l] = 0;
		}
		if($listings = $product->listings) {
			foreach ($listings as $listing) {
				if ($listing->status == -1 or $listing->merchant->status != 'Active') {
					continue;  
				}
				foreach ($qty_cols as $k=>$l) {
					@$qtys[$l] += $listing->$k;
				}
			}
		} 
		
		$managerSources = User::productManagerList('id-list');
		$managers = $product->users()->select('name')->pluck('name')->toArray();
		
		$listing_grid = ListingGrid::grid($request,['product_id'=>$product->id]);

		// $review_list_grid = ReviewListGrid::grid($request,['asin'=>$product->asin]);
		$reviews = Review::where('asin', $product->asin)->get();

		$categorizedReviewQuantity = Review::getCategorizedQuantity($product->asin);
		
		$warehouseActivities = WarehouseActivity::with('user')->where('product_id', $product->id)->orderBy('id', 'desc')->get();

		return view('product.view', ['product' => $product,'start_date'=>$start_date,'end_date'=>$end_date,'date_range'=>$date_range,'params'=>$params,
				'start_date_yesterday'=>$start_date_yesterday,'end_date_yesterday'=>$end_date_yesterday,
				'start_date_last_week'=>$start_date_last_week,'end_date_last_week'=>$end_date_last_week,
				'start_date_last_month'=>$start_date_last_month,'end_date_last_month'=>$end_date_last_month,
				'start_date_last_year'=>$start_date_last_year,'end_date_last_year'=>$end_date_last_year,
				'listing_grid'=>$listing_grid,
				'categorizedReviewQuantity'=>$categorizedReviewQuantity,
				'pid'=>$pid,
				'reviews'=>$reviews,
				'asin'=>$product->asin,
				// 'review_list_grid'=>$review_list_grid,
				'orders'=>$orderReport,
// 				'counts'=>$orderReport['counts'],'totals'=>$orderReport['totals'],
				'managerSources'=>$managerSources,'managers'=>$managers,
// 				'total_gross'=>$orderReport['total_gross'],'total_orders'=>$orderReport['total_orders'],
				'qtys'=>$qtys,
				'orderReportYesterday'=>$orderReportYesterday,
				'orderReportLastMonth'=>$orderReportLastMonth,'orderReportLastWeek'=>$orderReportLastWeek,
				'orderReportLastYear'=>$orderReportLastYear,
				'inventoryHistory'=>$inventoryHistory,
				'adsSummary'=>$adsSummary,
				'box_route'=>$box_route,
				'warehouseActivities' => $warehouseActivities
				
		]
		);
		
	}

	function reviewOverview(Request $request) {
		$id = $request->id;
		$product = Product::find($id);
		$fromDate = $request->input('from_date');
		$toDate = $request->input('to_date');
		$reviewOverview = $product->gerReviewOverview($fromDate, $toDate);
		return response()->json($reviewOverview);
	}

	function salesRankHistory(Request $request) {
		$id = $request->id;
		$product = Product::find($id);
		$fromDate = $request->input('from_date');
		$toDate = $request->input('to_date');
		$salesRank = $product->gerSalesRankHistory($fromDate, $toDate);
		return response()->json($salesRank);
	}

	function categorizedReviewQuantity(Request $request) {
		$id = $request->id;
		$product = Product::find($id);
		$categorizedReviewQuantity = Review::getCategorizedQuantity($product->asin);

	}
	
	
	/**
	 * ajax update product attributes
	 * @param Request $request
	 */
	function ajaxsaveAction(Request $request) {
		$value = $request->get('value');
		list($type,$id) = explode('-', $request->get('pk'));
		
		
		try{
			
			if($type == 'manager') {
				$product = Product::where('id',$id)->first();
				$product->users()->detach();
				
				//if(isset($data['products'])) {
				// foreach ( $value as $uid)
				// {
				// 	if(!is_int($uid)) {
				// 		$uid = User::where('name',$uid)->first()->id;
				// 	}
				// 	$product->users()->attach($uid);
				// }
				//}

				if($value != "None") {
					$uid = User::where('name',$value)->first()->id;
					$product->users()->attach($uid);
				}

			} else if ($type == 'category_id') {
				if($value != "None") {
					$category_id = ProductCategory::where('category',$value)->first()->id;
					Product::where('id', $id)->update([$type => $category_id]);
				}
			} else if ($type == 'productStatus') {
				$pStatus = array(
					'Inactive' => 0, 
					'Active' => 1, 
					'Blocked' => 2
				);
				$product = Product::where('id',$id)->update(['status'=> $pStatus[$value]]);
				$listingStatus = $pStatus[$value] % 2;
				Listing::where('product_id', $id)->update(['status' => $listingStatus]);
			} else {
				
				Product::where('id', $id)->update([$type => $value]);
				
				if($isUnitUpdateOrders = $request->get('isUnitUpdateOrders')) {
					$product = Product::find($id);
					
					if($type == 'cost') {
						OrderItem::where('ASIN',$product->asin)->where('region',$product->country)->update(['cost'=>$value]);
					}
					
					if($type == 'shipping_cost') {
						OrderItem::where('ASIN',$product->asin)->where('region',$product->country)->update(['shipping_cost'=>$value]);
					}
				}

				$common_types = [
					'peak_season_mode',
					'cn_to_amz_shipping_days',
					'manufacture_days',
					'ps_manufacture_days',
					'ps_cn_to_amz_shipping_days'
				];
				// UPDATE LISTING INFO
				if(in_array($type, $common_types) ) {
					Listing::where('product_id', $id)->update([$type => $value]);
				}
			}
			echo 1;
		}catch (\Exception $e) {
			print $e->getMessage();
		}
		
	}

	function indexNewReleasesAction(){

		$title = "New Releases";
		
		$grid = NewReleaseGrid::grid()->render();

		return view('product.newrelease.index', ['grid' => $grid,"title"=>$title]);
	}	


	function newReleasesSaveAction( Request $request ) {


		foreach($request->website as $website){


			$newrelease = NewRelease::where('product_id',$request->product_id)->where('website',$request->website)->firstOrFail();

			if(!$product_website->id){

				$newrelease = New NewRelease();

			}

			$newrelease->product_id = $request->product_id;
			$newrelease->title = $request->title;
			$newrelease->website = $website;
			$newrelease->price = $request->price;
			$newrelease->image = $request->image;						
			$newrelease->link = $request->link;		
							
			$newrelease->save();

		}

	}	

	function createNewReleases(){

		$giftcard_websites = config('app.giftcard_websites');
		return view('product.newrelease.add', ['giftcard_websites' => $giftcard_websites]);

	}		

	function updateField(Request $request) {
		dd($request);
		$id = $request->id;
		$product = Product::find($id);
		$field = $request->input('name');
		$value = $request->input('value');
		if ( $field === "ai_ads_commission_rate" && !$product->ai_ads_open) {
			return response('please open AI ads first!', 406);
		} else {
			$product->$field = $value;
			$product->save();
			return response('OK', 200);
		}
	}
	
}