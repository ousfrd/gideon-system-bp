<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
// use Illuminate\Routing\Controller;


class DownloadController extends Controller
{
	public function download($file) {
		$filePath = public_path('files/' . $file);

		return response()->download($filePath);
	}
}
