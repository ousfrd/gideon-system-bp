<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use App\ClaimReviewCampaign;
use App\Shipment;
use App\Account;
use App\ProductStatistic;
use App\User;
use App\ClickSendReturnAddress;
use App\ClaimReviewReturnAddress;
use App\OrderItemMarketing;
use App\Grids\ClaimReviewCampaignGrid;
use App\Grids\ProductCampaignGrid;
use App\Grids\ShipmentGrid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\ClickSendClient;
use Illuminate\Support\Facades\Storage;
use App\Helpers\ShipmentFilterReader;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Product;
use App\Order;
use App\OrderItem;
use App\Listing;
use App\AdReport;
use App\Inventory;
use App\Excessnventory;
use App\GiftCardTemplate;
use App\Report;
use DB;
use App\Grids\ProductCostGrid;
use App\Helpers\DataHelper;

class ClaimReviewCampaignController extends Controller
{

    public function __construct()
	{
		parent::__construct();
        $this->middleware('auth');
        // $this->middleware('checkClaimReview');
        $this->middleware('can:claim-review-campaign.index')->only('index');
        $this->middleware('can:claim-review-campaign.create')->only('create');
        $this->middleware('can:claim-review-campaign.store')->only('store');
        $this->middleware('can:claim-review-campaign.show')->only('show');
        $this->middleware('can:claim-review-campaign.edit')->only('edit');
        $this->middleware('can:claim-review-campaign.update')->only('update');
        $this->middleware('can:claim-review-campaign.destroy')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('can:claim-review-campaign.index');
        $query = ClaimReviewCampaign::getCampaignListQuery();
        $grid = (new ClaimReviewCampaignGrid($query))->generateGrid();
        return view('claim-review-campaign.index', ['grid'=>$grid]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $marketplaces = ClaimReviewCampaign::MARKETPLACES;
        $suppliers = ClaimReviewCampaign::SUPPLIERS;
        $channels = config('claimreview.channels');
        return view('claim-review-campaign.create', [
            'channels' => $channels,
            'marketplaces' => $marketplaces,
            'suppliers' => $suppliers
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::where('asin', $request->input("asin"))->where('country', $request->input("marketplace"))->first();
        $giftCardTemplate = GiftCardTemplate::find($request->input("gift_card_template_id"));
        $claimReviewCampaign = new ClaimReviewCampaign();
        if (isset($product->alias)){
            $claimReviewCampaign->name = $product->alias;
        } else {
            $claimReviewCampaign->name = substr($product->name, 0, 30);
        }
        
        $claimReviewCampaign->platform = $request->input("platform");
        $claimReviewCampaign->cashback = $giftCardTemplate->amount;
        $claimReviewCampaign->asin = $request->input("asin");
        $claimReviewCampaign->marketplace = $request->input("marketplace");
        $claimReviewCampaign->start_date = date('Y-m-d', strtotime( $request->input('startDate') ));
        $claimReviewCampaign->end_date = date('Y-m-d', strtotime( $request->input('endDate') ));
        $claimReviewCampaign->created_by = auth()->user()->id;
        $claimReviewCampaign->gift_card_template_id = $request->input("gift_card_template_id");
        $claimReviewCampaign->channel = trim($giftCardTemplate->channel) . "-" . trim($giftCardTemplate->channel_detail);
        $claimReviewCampaign->insert_card_orders_filter = $request->input("insert_card_orders_filter");
        $claimReviewCampaign->save();
        return redirect()->route('claim-review-campaign.show', ['id' => $claimReviewCampaign->id]);
    }

    public function createConfig(Request $request){
        $id = $request->id;
        $claimReviewCampaign = ClaimReviewCampaign::find($id);
        $claimReviewCampaign->cs_return_address_id = $request->input('cs_return_address_id');
        $claimReviewCampaign->cs_duplex = $request->input('cs_duplex');
        $claimReviewCampaign->cs_colour = $request->input('cs_colour');
        $claimReviewCampaign->cs_priority_post = $request->input('cs_priority_post');
        $claimReviewCampaign->cs_exclude_promotion_discounts = $request->input('cs_exclude_promotion_discounts');
        $claimReviewCampaign->cs_delay_hours = $request->input('cs_delay_hours');
        $claimReviewCampaign->save();

        return redirect()->route('claim-review-campaign.show', ['id' => $id]);
    }

    public function createClicksendConfig(Request $request){
        $id = $request->id;
        $claimReviewCampaign = ClaimReviewCampaign::find($id);
        $claimReviewCampaign->cs_return_address_id = $request->input('cs_return_address_id');
        $claimReviewCampaign->cs_duplex = $request->input('cs_duplex');
        $claimReviewCampaign->cs_colour = $request->input('cs_colour');
        $claimReviewCampaign->cs_priority_post = $request->input('cs_priority_post');
        $claimReviewCampaign->cs_exclude_promotion_discounts = $request->input('cs_exclude_promotion_discounts');
        $claimReviewCampaign->cs_delay_hours = $request->input('cs_delay_hours');
        $claimReviewCampaign->save();
        return redirect()->route('claim-review-campaign.show', ['id' => $id]);
    }

    public function clicksendReview(Request $request) {
        $id = $request->id;
        $claimReviewCampaign = ClaimReviewCampaign::find($id);
        $query = $claimReviewCampaign->getClickReviewSendOrderShipments();
        $grid = (new ShipmentGrid($query, $claimReviewCampaign->asin))->generateGrid($claimReviewCampaign);
        $giftCardTemplate = $claimReviewCampaign->giftCardTemplate;
        // $returnAddresses = ClickSendReturnAddress::all();
        $returnAddresses = ClaimReviewReturnAddress::getByPlatform(
            $claimReviewCampaign->platform);
        $returnAddress = $returnAddresses->first(function($value, $key) use ($claimReviewCampaign) { return $value->return_address_id == $claimReviewCampaign->cs_return_address_id; });
        return view('claim-review-campaign.review', [
            "grid"=>$grid, 
            "claimReviewCampaign"=>$claimReviewCampaign, 
            "giftCardTemplate"=>$giftCardTemplate,
            "returnAddresses"=>$returnAddresses,
            "returnAddress"=>$returnAddress
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClaimReviewCampaign  $claimReviewCampaign
     * @return \Illuminate\Http\Response
     */
    public function show(ClaimReviewCampaign $claimReviewCampaign)
    {
        // $query = $claimReviewCampaign->getShipments();
        $query = $claimReviewCampaign->campaignGetShipments();
        $grid = (new ShipmentGrid($query, $claimReviewCampaign->asin))->generateGrid($claimReviewCampaign);
        $giftCardTemplate = $claimReviewCampaign->giftCardTemplate;
        // $returnAddresses = ClickSendReturnAddress::all();
        $returnAddresses = ClaimReviewReturnAddress::getByPlatform(
            $claimReviewCampaign->platform);
        $returnAddress = $returnAddresses->first(function($value, $key) use ($claimReviewCampaign) {
            return $value->return_address_id == $claimReviewCampaign->cs_return_address_id;
        });
        return view('claim-review-campaign.show', [
            "grid"=>$grid, 
            "claimReviewCampaign"=>$claimReviewCampaign, 
            "giftCardTemplate"=>$giftCardTemplate,
            "returnAddresses"=>$returnAddresses,
            "returnAddress"=>$returnAddress
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClaimReviewCampaign  $claimReviewCampaign
     * @return \Illuminate\Http\Response
     */
    public function edit(ClaimReviewCampaign $claimReviewCampaign)
    {
        $suppliers = ClaimReviewCampaign::SUPPLIERS;
        if ($claimReviewCampaign->sent_at) {
            return redirect()->route('claim-review-campaign.show', ['id' => $claimReviewCampaign->id]);
        } else {
            return view('claim-review-campaign.edit', ["claimReviewCampaign" => $claimReviewCampaign, 'suppliers'=>$suppliers]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClaimReviewCampaign  $claimReviewCampaign
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClaimReviewCampaign $claimReviewCampaign)
    {
        if (empty($claimReviewCampaign->sent_at)) {
            if ($request->input('name')) $claimReviewCampaign->name = $request->input('name');
            if ($request->input('platform')) $claimReviewCampaign->platform = $request->input('platform');
            if ($request->input('gift_card_template_id')) $claimReviewCampaign->gift_card_template_id = $request->input('gift_card_template_id');
            if (null !== $request->input('actual_sent_amount') ) $claimReviewCampaign->actual_sent_amount = $request->input('actual_sent_amount');
            if ($request->input('channel')) $claimReviewCampaign->channel = $request->input('channel');
            $claimReviewCampaign->save();
        }
        return redirect()->route('claim-review-campaign.show', ['id' => $claimReviewCampaign->id]);
    }

    public function ajaxUpdate(Request $request)
    {   
        $id = $request->id;
        $claimReviewCampaign = ClaimReviewCampaign::find($id);
        $key = $request->input('pk');
        $value = $request->input('value');
        if ($claimReviewCampaign) {
            $claimReviewCampaign->$key = $value;
            $claimReviewCampaign->save();
            return response("Updated", 200);
        } else {
            return response("Not Found The Campaign With ID ".$id, 404);
        }
        
    }

    public function send(Request $request) 
    {
        $id = $request->id;
        $claimReviewCampaign = ClaimReviewCampaign::find($id);
        $claimReviewCampaign->sent_at = date('Y-m-d H:i:s');
        $claimReviewCampaign->save();
        if ($request->ajax()) {
            return response("Successful", 200);
        } else {
            return redirect()->route('claim-review-campaign.show', ['claimReviewCampaign' => $claimReviewCampaign]);
        }
    }

    public function sendViaApi(Request $request) {
        $id = $request->id;
        $claimReviewCampaign = ClaimReviewCampaign::find($id);
        $claimReviewCampaign->send();

        return redirect()->route('claim-review-campaign.show', ['claimReviewCampaign' => $claimReviewCampaign]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClaimReviewCampaign  $claimReviewCampaign
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClaimReviewCampaign $claimReviewCampaign)
    {
        try {
            OrderItemMarketing::where("claim_review_id", $claimReviewCampaign->id)->delete();
            $claimReviewCampaign->delete();
        } catch (\Exception $e) {
            Log::warning($e->getMessage());
        }

        return response("DELETED", 200);
    }

    public function productsCampaignOverview(Request $request)
    {   
        $this->authorize('manage-products');
        $title = "Products Campaign Overview";

        $startDate = $request->get('start_date', date('Y-m-d', strtotime('-30 days')));
        $endDate = $request->get('end_date', date('Y-m-d'));
        $dateRange = $request->get('date_range', 'Last 30 Days');
        $marketplace = $request->get('marketplace', 'all');
        $marketplaces = config('app.marketplaces');

        $queryConds = [
            ['discontinued', '=', FALSE],
            ['status', '>', 0],
            ['should_claim_review', '>', 0]
        ];
        if (!empty($marketplace) && $marketplace != 'all') {
            $queryConds[] = ['country', '=', $marketplace];
        }

        $claimReviewStatistics = Product::getClaimReviewStatistics($startDate, $endDate);

        $productClaimReviewStatistics = [];
        $products = Product::where($queryConds)->get();
        foreach ($products as $product) {
            $productInfo = $product->attributesToArray();
            $productInfo['amzLink'] = $product->amazonLink();
            $productInfo['startDate'] = $startDate;
            $productInfo['endDate'] = $endDate;

            $k = sprintf('%s-%s', $product->country, $product->asin);
            if (array_key_exists($k, $claimReviewStatistics)) {
                $productClaiReviewStatistic = $claimReviewStatistics[$k];
            } else {
                $productClaiReviewStatistic = Product::getDefaultClaimReviewStatistic();
            }

            $productClaimReviewStatistics[] = (object) array_merge($productInfo, $productClaiReviewStatistic);
        }

        $data = [
            'marketplace' => $marketplace,
            'marketplaces' => $marketplaces,
            'start_date'=>$startDate,
            'end_date' => $endDate,
            'date_range' => $dateRange,
            'title' => $title
        ];
        $productCrsCollection = collect($productClaimReviewStatistics)->sortByDesc('claimReviewToSendCount');
        $grid = ProductCampaignGrid::grid($productCrsCollection);
        $data['grid'] = $grid->render();

        return view('claim-review-campaign.products-campaign-overview', $data);
    }

    public function updateProductStatistic(Request $request) {
        $this->authorize('manage-products');

        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');
        $asin = $request->get('asin');
        $country = $request->get('country');
        if (empty($startDate) || empty($endDate) || empty($asin) || empty($country)) {
            return redirect()->route('claim-review-campaign.products-overview');
        }

        $product = Product::where([
            ['asin', '=', $asin],
            ['country', '=', $country]
        ])->first();
        if ($product) {
            $product->buildClaimReviewStatistics($startDate, $endDate);
        }

        return redirect()->route(
            'claim-review-campaign.products-overview', [
                'start_date' => $startDate,
                'end_date' => $endDate
            ]
        );
    }

    public function filterShipments(Request $request) {
        if ($request->isMethod('get')) {
            return view('claim-review-campaign.filter-shipments');
        }

        $fileName = 'orders_file';
        if (!$request->hasFile($fileName) || !$request->file($fileName)->isValid()) {
            return view('claim-review-campaign.filter-shipments');
        }

        $localFileName = $request->file('orders_file')->store('claim-review-campaign', 'local');
        $filePath = Storage::disk('local')->path($localFileName);

        foreach (ShipmentFilterReader::getShipmentFilterRecords($filePath) as $idx => $shipmentFilterRecord) {
            if ($idx >= 15000) {
                break;
            }

            $asin = $shipmentFilterRecord['asin'] ?? NULL;
            $sku = $shipmentFilterRecord['sku'] ?? NULL;
            $shipments = Shipment::getShipments(
                $shipmentFilterRecord['amazon-order-id'], $asin, $sku);
            if (empty($shipments)) {
                $message = sprintf(
                    '[ShipmentMissing] %s', json_encode($shipmentFilterRecord));
                Log::warning($message);
                continue;
            }

            if (isset($shipmentFilterRecord['note'])) {
                $note = $shipmentFilterRecord['note'];
            } else {
                $note = '';
            }

            foreach ($shipments as $shipment) {
                $shipment->markSkipReviewClaim($note);
            }

            $message = sprintf(
                '[ShipmentFilterRecordImported] %s',
                json_encode($shipmentFilterRecord));
            Log::info($message);
        }

        return redirect()->route('claim-review-campaign.index');
    }
}
