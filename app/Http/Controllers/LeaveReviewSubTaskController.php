<?php

namespace App\Http\Controllers;

use App\LeaveReviewSubTask;
use Illuminate\Http\Request;

class LeaveReviewSubTaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LeaveReviewSubTask  $leaveReviewSubTask
     * @return \Illuminate\Http\Response
     */
    public function show(LeaveReviewSubTask $leaveReviewSubTask)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LeaveReviewSubTask  $leaveReviewSubTask
     * @return \Illuminate\Http\Response
     */
    public function edit(LeaveReviewSubTask $leaveReviewSubTask)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LeaveReviewSubTask  $leaveReviewSubTask
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LeaveReviewSubTask $leaveReviewSubTask)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LeaveReviewSubTask  $leaveReviewSubTask
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeaveReviewSubTask $leaveReviewSubTask)
    {
        //
    }
}
