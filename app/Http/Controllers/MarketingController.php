<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shipment;
use App\OrderItemMarketing;
use App\OrderItem;
use App\Grids\ShipmentGrid;
use App\Grids\MarketingOrderGrid;

class MarketingController extends Controller
{
    public function __construct()
	{
		parent::__construct();
		$this->middleware('auth');
    }

    public function listOrderShipments() {
        $query = Shipment::getWithMarketingStatus();
        $grid = (new ShipmentGrid($query))->generateMarketingGrid();
        return view('marketing.listOrderShipments',['grid'=>$grid]);
    }
    
    public function listOrders()
    {
        $query = OrderItem::displayMarketingOrdersQuery();
        $grid = (new MarketingOrderGrid($query))->generateGrid();
        return view('marketing.listOrders', ['grid'=>$grid]);
    }

    public function updateReviewStatus(Request $request) {
        $value = $request->get('value');
        list($sku, $amazonOrderId, $field) = explode('||', $request->get('pk'));
        try {
            OrderItemMarketing::updateStatus($sku, $amazonOrderId, $field, $value);
            return response('successful', 200);
        } catch (\Exception $e) {
            print $e->getMessage();
        }
    }

    public function updateCustomerInfo(Request $request) {
        $value = $request->get('value');
        list($sku, $amazonOrderId, $field) = explode('||', $request->get('pk'));
        try {
            OrderItemMarketing::updateField($sku, $amazonOrderId, $field, $value);
            return response('successful', 200);
        } catch (\Exception $e) {
            print $e->getMessage();
        }

    }
}
