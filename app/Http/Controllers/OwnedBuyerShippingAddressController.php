<?php

namespace App\Http\Controllers;

use App\OwnedBuyerShippingAddress;
use Illuminate\Http\Request;

class OwnedBuyerShippingAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OwnedBuyerShippingAddress  $ownedBuyerShippingAddress
     * @return \Illuminate\Http\Response
     */
    public function show(OwnedBuyerShippingAddress $ownedBuyerShippingAddress)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OwnedBuyerShippingAddress  $ownedBuyerShippingAddress
     * @return \Illuminate\Http\Response
     */
    public function edit(OwnedBuyerShippingAddress $ownedBuyerShippingAddress)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OwnedBuyerShippingAddress  $ownedBuyerShippingAddress
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OwnedBuyerShippingAddress $ownedBuyerShippingAddress)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OwnedBuyerShippingAddress  $ownedBuyerShippingAddress
     * @return \Illuminate\Http\Response
     */
    public function destroy(OwnedBuyerShippingAddress $ownedBuyerShippingAddress)
    {
        //
    }
}
