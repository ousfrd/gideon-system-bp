<?php

namespace App\Http\Controllers;

use App\Grids\ReviewGrid;
use App\Review;
use Illuminate\Http\Request;
use App\Helpers\ReviewHelper;

class ReviewController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	// $review_id, $review_exist, $video, $image, $review_text, $review_link, $verified_purchase, $asin, $review_date, $profile_name, $review_title, $review_rating);
    // $review_exist, $video, $, $, $verified_purchase, $, $, $, $, $);
      
	// curl -d '{"verified_purchase": "1", "country": "US", "review_exist": "1", "video": "no", "review_id": "RGEGRP1HC0MMD", "review_title": "Excellent price for a LEGO-compatible product", "review_text": "This arrived right away. My son was thrilled. He used his own cash to purchase these which he wants to use to build a world map, and then later glue to a coffee table to have his own brick table. These are great for bricks building on top as well as below--works either way. Theyre sturdy. He tried to bend them to break them, and they had flexibility, but theyre not brittle.", "review_link": "https://www.amazon.com/gp/customer-reviews/RGEGRP1HC0MMD/ref=cm_cr_dp_d_rvw_ttl?ie=UTF8&ASIN=B07FB627NR", "asin": "B07FB627NR", "review_date": "2019-03-11", "profile_name": "Dr. Dolly Garnecki", "review_rating": "5", "image": "https://images-na.ssl-images-amazon.com/images/I/81iwr4KCyxL._SY88.jpg"}' -H "Content-Type: application/json" -X POST  http://homestead.test/review/info
	// curl -d '{"verified_purchase": "1", "country": "US", "review_exist": "1", "video": "https://images-na.ssl-images-amazon.com/images/I/D1-vfL0m0-S.mp4", "review_id": "R32Y2I497IRK14", "review_title": "Simple and easy blackhead remove mask", "review_text": "This blackhead mask is very easy to be put on the nose and cheek. You can use about 10-15 times with this bottle of blackhead mask. You dont need a lot to be put on your nose because it spreads very easy. You can wait 10-15 minutes. When you peel it off, please peel it slowly so the blackheads can be taken out. After using the mask , my nose becomes very soft. After peeling it off, you can find many blackhead mask. I recommend this and it is very simple and easy!", "review_link": "https://www.amazon.com/gp/customer-reviews/R32Y2I497IRK14/ref=cm_cr_dp_d_rvw_ttl?ie=UTF8&ASIN=B07BJCMK6W", "asin": "B07BJCMK6W", "review_date": "2020-02-19", "profile_name": "Samantha Canarelli", "review_rating": "5", "image": "https://images-na.ssl-images-amazon.com/images/I/81iwr4KCyxL._SY88.jpg"}' -H "Content-Type: application/json" -X POST  http://homestead.test/review/info
	// curl -d '{"verified_purchase": "1", "country": "US", "review_exist": "1", "video": "", "review_id": "R24YET5E1HNIYT", "review_title": "Great baseplate, Almost exactly like Lego brand!", "review_text": "Lego brand baseplate on left and Makotoys baseplate on the right. They are almost the same color. The pegs on this baseplate are a tiny bit larger than the Lego brand. Other blocks still fit on easily but they are tighter which is either better or worse depending on who is building. They are harder for my younger kids to take off but have better stability. I would buy again!", "review_link": "https://www.amazon.com/gp/customer-reviews/R24YET5E1HNIYT/ref=cm_cr_getr_d_rvw_ttl?ie=UTF8&ASIN=B07FB627NR", "asin": "B07FB627NR", "review_date": "2019-04-11", "profile_name": "Danae", "review_rating": "5", "image": "https://images-na.ssl-images-amazon.com/images/I/71i0II4QBkL._SY88.jpg"}' -H "Content-Type: application/json" -X POST  http://homestead.test/review/info
	 
	public function __construct()
	{
		parent::__construct();
		// $this->middleware('auth');
	}

	function indexAction(Request $request) {
		$title = "Reviews";
		$asin = $request->asin;
		$grid = ReviewGrid::grid($asin)->render();
		return view('review.index',['grid'=>$grid, 'title'=>$title]);
	}

	public function updateReviewStatus(Request $request) {
        try {
            Review::updateStatus($request->pk, $request->value);
            return response('successful', 200);
        } catch (\Exception $e) {
            print $e->getMessage();
        }
	}

	public function updateReviewFollowUpStatus(Request $request) {
        try {
            Review::updateFollowUpStatus($request->pk, $request->value);
            return response('successful', 200);
        } catch (\Exception $e) {
            print $e->getMessage();
        }
	}
	
	public function updateReviewCategory(Request $request) {
        try {
            Review::updateCategory($request->asin);
            return response('successful', 200);
        } catch (\Exception $e) {
            print $e->getMessage();
        }
    }

	public function insertReview(Request $request) {
		$asin = $request->input('asin');
		$country = $request->input('country');
		$review_date = $request->input('review_date');
		$profile_name = $request->input('profile_name');
		$review_title = $request->input('review_title');
		$review_text = $request->input('review_text');
		$review_exist = $request->input('review_exist');
		$review_rating = $request->input('review_rating');
		$review_id = $request->input('review_id');
		$review_link = $request->input('review_link');
		$verified_purchase = $request->input('verified_purchase');
		$image = $request->input('image');
		$video = $request->input('video');
		Review::insertReviewInfo($review_id, $country, $review_exist, $video, $image, $review_text, $review_link, $verified_purchase, $asin, $review_date, $profile_name, $review_title, $review_rating);
		return response("saved", 200);
	}
}
