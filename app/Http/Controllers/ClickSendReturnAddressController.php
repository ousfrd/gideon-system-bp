<?php

namespace App\Http\Controllers;

use App\ClickSendReturnAddress;
use Illuminate\Http\Request;

class ClickSendReturnAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClickSendReturnAddress  $clickSendReturnAddress
     * @return \Illuminate\Http\Response
     */
    public function show(ClickSendReturnAddress $clickSendReturnAddress)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClickSendReturnAddress  $clickSendReturnAddress
     * @return \Illuminate\Http\Response
     */
    public function edit(ClickSendReturnAddress $clickSendReturnAddress)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClickSendReturnAddress  $clickSendReturnAddress
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClickSendReturnAddress $clickSendReturnAddress)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClickSendReturnAddress  $clickSendReturnAddress
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClickSendReturnAddress $clickSendReturnAddress)
    {
        //
    }
}
