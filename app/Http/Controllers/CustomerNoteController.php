<?php

namespace App\Http\Controllers;

use App\CustomerNote;
use App\Customer;
use Illuminate\Http\Request;

class CustomerNoteController extends Controller
{

    public function __construct()
	{
		parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Customer $customer)
    {
        $customerId = $customer->id;
        $note = new CustomerNote();
        $note->customer_id = $customerId;
        $note->note = $request->input("note");
        $note->created_by = auth()->user()->id;
        $note->save();
        return redirect()->to($request->input('redirect'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomerNote  $customerNote
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer, CustomerNote $customerNote)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomerNote  $customerNote
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer, CustomerNote $customerNote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomerNote  $customerNote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer, CustomerNote $customerNote)
    {
        $field = $request->name;
        $value = $request->value;
        $customerNote->$field = $value;
        $customerNote->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomerNote  $customerNote
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer, CustomerNote $customerNote)
    {
        //
    }
}
