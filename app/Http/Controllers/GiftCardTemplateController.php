<?php

namespace App\Http\Controllers;

use App\GiftCardTemplate;
use Illuminate\Http\Request;
use App\Grids\GiftCardTemplateGrid;

class GiftCardTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $grid = (new GiftCardTemplateGrid())->generateGrid();
        return view('gift-card-template.index', ['grid' => $grid]);
    }

    public function get(Request $request)
    {
        $asin = $request->asin;
        $templates = GiftCardTemplate::where('asin', $asin)->get();
        return response()->json($templates);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $channels = config('claimreview.channels');
        return view('gift-card-template.create', ['channels' => $channels]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gcTemplate = $this->save($request);
        return redirect()->route('gift-card-template.show', ['giftCardTemplate' => $gcTemplate]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GiftCardTemplate  $giftCardTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(GiftCardTemplate $giftCardTemplate)
    {
        return view('gift-card-template.show', ["giftCardTemplate" => $giftCardTemplate]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GiftCardTemplate  $giftCardTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(GiftCardTemplate $giftCardTemplate)
    {
        $channels = config('claimreview.channels');
        return view('gift-card-template.edit', ["giftCardTemplate"=>$giftCardTemplate, "channels"=>$channels]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GiftCardTemplate  $giftCardTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GiftCardTemplate $giftCardTemplate)
    {   
        $gcTemplate = $this->save($request, $giftCardTemplate);
        return redirect()->route('gift-card-template.show', ['giftCardTemplate' => $gcTemplate]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GiftCardTemplate  $giftCardTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(GiftCardTemplate $giftCardTemplate)
    {
        $giftCardTemplate->delete();
        return response("DELETED", 200);
    }

    private function save(Request $request, GiftCardTemplate $giftCardTemplate = null) {
        if (!$giftCardTemplate) {
            $giftCardTemplate = new GiftCardTemplate();
            $giftCardTemplate->created_by = auth()->user()->id;
        }
        $giftCardTemplate->name = $request->input("name");
        $giftCardTemplate->asin = $request->input("asin");
        $giftCardTemplate->amount = $request->input("amount");
        $giftCardTemplate->channel = $request->input("channel");
        $giftCardTemplate->channel_detail = $request->input("channel_detail");
        $giftCardTemplate->version = $request->input("version");
        $giftCardTemplate->template_type = $request->input("template_type");
        $giftCardTemplate->note = $request->input("note");
        if ($request->file('front-design-file')) {
            $fronDesignPath = $request->file('front-design-file')->store('gift-card-template', config('giftcard.storage'));
            $giftCardTemplate->front_design = $fronDesignPath;
        }
        if ($request->file('back-design-file')) {
            $backDesignPath = $request->file('back-design-file')->store('gift-card-template', config('giftcard.storage'));
            $giftCardTemplate->back_design = $backDesignPath;
        }
        
        if ($request->file('attachment1-file')) {
            $attachment1Path = $request->file('attachment1-file')->store('gift-card-template', config('giftcard.storage'));
            $giftCardTemplate->attachment1 = $attachment1Path;
        }
        if ($request->file('attachment2-file')) {
            $attachment2Path = $request->file('attachment2-file')->store('gift-card-template', config('giftcard.storage'));
            $giftCardTemplate->attachment2 = $attachment2Path;
        }
        $giftCardTemplate->save();
        return $giftCardTemplate;
    }
}
