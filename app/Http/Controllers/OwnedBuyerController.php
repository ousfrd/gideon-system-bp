<?php

namespace App\Http\Controllers;

use App\OwnedBuyer;
use App\OwnedBuyerShippingAddress;
use App\OwnedBuyerPaymentMethod;
use App\ManagedReviewOrder;
use Illuminate\Http\Request;
use App\Grids\OwnedBuyerGrid;
use App\Grids\ManagedReviewOrderGrid;

class OwnedBuyerController extends Controller
{

    public function __construct()
	{
		parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = OwnedBuyer::getGridQuery();
        $grid = (new OwnedBuyerGrid($query))->generateGrid();
        return view('owned-buyer.index', ['request' => $request, 'grid' => $grid, 'tab' => 'all']); 
    }

    public function primeBuyers(Request $request) {
        $query = OwnedBuyer::primeGridQuery();
        $grid = (new OwnedBuyerGrid($query))->generateGrid();
        return view('owned-buyer.index', ['request' => $request, 'grid' => $grid, 'tab' => 'prime']); 
    }

    public function newBuyers(Request $request) {
        $query = OwnedBuyer::newBuyersGridQuery();
        $grid = (new OwnedBuyerGrid($query))->generateGrid();
        return view('owned-buyer.index', ['request' => $request, 'grid' => $grid, 'tab' => 'new']); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OwnedBuyer  $ownedBuyer
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if (is_numeric($request->id)) {
            $ownedBuyer = OwnedBuyer::find($request->id);
        } else {
            $ownedBuyer = OwnedBuyer::where('nickname',$request->id)->first();
        }
        if (empty($ownedBuyer)) { return abort(404);}
        $orderTypes = [ManagedReviewOrder::OWNED_BUYER_SELF_ORDER => "Own Seller (自己卖家)" , ManagedReviewOrder::OWNED_BUYER_OTHER_ORDER => "Other Sellers (其他卖家)"];
        $shippingAddress = $ownedBuyer->shippingAddresses->first();
        $paymentMethod = $ownedBuyer->paymentMethods->first();
        $addedToCartItems = $ownedBuyer->addedToCartItems;
        $orderQuery = ManagedReviewOrder::gridQueryByBueryId($ownedBuyer->id);
        $orderGrid = (new ManagedReviewOrderGrid($orderQuery))->generateInsideBuyerGrid();
        return view('owned-buyer.show', [
            'ownedBuyer' => $ownedBuyer, 
            'orderTypes' => $orderTypes,
            'shippingAddress' => $shippingAddress,
            'paymentMethod' => $paymentMethod,
            'addedToCartItems' => $addedToCartItems,
            'orderGrid' => $orderGrid
        ]);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OwnedBuyer  $ownedBuyer
     * @return \Illuminate\Http\Response
     */
    public function edit(OwnedBuyer $ownedBuyer)
    {
        $orderTypes = [ManagedReviewOrder::OWNED_BUYER_SELF_ORDER , ManagedReviewOrder::OWNED_BUYER_OTHER_ORDER];
        $shippingAddress = $ownedBuyer->shippingAddresses->first();
        $shippingAddress = empty($shippingAddress) ? new OwnedBuyerShippingAddress() : $shippingAddress;
        $paymentMethod = $ownedBuyer->paymentMethods->first();
        return view('owned-buyer.edit', [
            'ownedBuyer' => $ownedBuyer,
            'shippingAddress' => $shippingAddress,
            'paymentMethod' => $paymentMethod
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OwnedBuyer  $ownedBuyer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OwnedBuyer $ownedBuyer)
    {
        $ownedBuyer->update($request->all());
        $request->session()->flash('message.success', 'Buyer info has been updated successfully');
        return redirect()->route('owned-buyer.show', $ownedBuyer);
    }

    public function updateStatus(Request $request) 
    {
        $status = $request->value;
        $id = $request->id;
        $ownedBuyer = OwnedBuyer::find($id);
        $ownedBuyer->status = $status;
        $ownedBuyer->save();
        return response($ownedBuyer->status, 200);
    }

    public function updatePaymentMethod(Request $request) {
        $payemntMethodId = $request->input('pk');
        $attributeName = trim($request->input('name'));
        $attributeValue = trim($request->input('value'));
        $paymentMethod = OwnedBuyerPaymentMethod::find($payemntMethodId);
        $paymentMethod->{$attributeName} = $attributeValue;
        $paymentMethod->save();
        return response('OK', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OwnedBuyer  $ownedBuyer
     * @return \Illuminate\Http\Response
     */
    public function destroy(OwnedBuyer $ownedBuyer)
    {
        $ownedBuyer->delete();
        return response('deleted', 200);
    }

    public function addPaymentMethod(Request $request)
    {
        $ownedBuyerId = $request->id;
        $paymentMethod = new OwnedBuyerPaymentMethod($request->all());
        $paymentMethod->owned_buyer_id = $ownedBuyerId;
        $paymentMethod->save();
        return redirect()->route("owned-buyer.show", ['owned-buyer'=>$ownedBuyerId]);
    }

    public function import(Request $request) {
        if ($request->hasFile('buyer_csv')) {
            $filePath = $request->file('buyer_csv')->getRealPath();
            $created_by = auth()->user()->id;
            $importedRecordsCount = $this->importBuyers($created_by, $filePath);
            $request->session()->flash('success', 'successfully imported ' . $importedRecordsCount . ' records');
            return redirect()->route('owned-buyer.index');
        }
    }

    public function findEligibleBuyers(Request $request) {
        $asin = $request->asin;
        $lastOrderBefore = $request->lastOrderBefore;
        $lastReviewBefore = $request->lastReviewBefore;
        $reviewRateLessThan = $request->reviewRateLessThan;
        if ($asin) {
            $query = OwnedBuyer::getEligibleBuyersForProductQuery($asin, $lastOrderBefore, $lastReviewBefore, $reviewRateLessThan);
            $grid = (new OwnedBuyerGrid($query))->generateGrid();
        } else {
            $grid = null;
        }
        return view('owned-buyer.find-eligible-buyers', [
                'asin' => $asin, 
                'lastOrderBefore' => $lastOrderBefore,
                'lastReviewBefore' => $lastReviewBefore,
                'reviewRateLessThan' => $reviewRateLessThan,
                'grid' => $grid
            ]); 
    }

    public function doFindEligibleBuyers(Request $request) {
        $asin = $request->input("asin");
        $lastOrderBefore = $request->lastOrderBefore;
        $lastReviewBefore = $request->lastReviewBefore;
        $reviewRateLessThan = $request->reviewRateLessThan == null ? 1 : $request->reviewRateLessThan;
        return redirect()->route('owned_buyer.find-eligible-buyers-with-parameters', [
            'asin' => $asin,
            'lastOrderBefore' => $lastOrderBefore,
            'lastReviewBefore' => $lastReviewBefore,
            'reviewRateLessThan' => $reviewRateLessThan,
        ]);
    }

    private function importBuyers($createdBy, $buyerFilePath) {
        $handle = fopen($buyerFilePath, "r");
        $importedRecordsCount = 0;
        while ( ($row = fgetcsv($handle) ) !== FALSE ) {
            if (count($row) < 21) { continue; }
            $accountStatus = intval($row[0]);
            $nickName = trim($row[1]);
            $country = strtoupper(trim($row[2]));
            $deviceName = trim($row[3]);
            $browser = trim($row[4]);
            $name = strtolower(trim($row[5]));
            $email = trim($row[6]);
            $phone = preg_replace('/\D/', '', trim($row[7]));
            $password = trim($row[8]);
            $auxiliaryEmail = trim($row[9]);
            $shippingAddress1 = trim($row[10]);
            $shippingAddress2 = trim($row[11]);
            $shippingAddress3 = trim($row[12]);
            $shippingCity = trim($row[13]);
            $shippingState = trim($row[14]);
            $shippingZip = trim($row[15]);
            $shippingCountry = empty($row[16]) ? $country : strtoupper(trim($row[16]));
            $shippingPhone = trim($row[17]);
            $paymentCardNumber = trim($row[18]);
            $availableBalance = trim($row[19]);
            $primeExpiredDate = trim($row[20]);

            if (empty($phone) && (empty($email) || !preg_match('/\w+@\w+\.\w+/', $email))) {
                continue;
            } else {
                $buyer = new OwnedBuyer();
                if (!empty($phone)) {
                    $buyer = OwnedBuyer::firstOrNew(['phone' => $phone]);
                } else {
                    $buyer = OwnedBuyer::firstOrNew(['email' => $email]);
                }
                $buyer->status = $accountStatus;
                $buyer->nickname = $nickName;
                $buyer->marketplace = $country;
                $buyer->name = $name;
                $buyer->email = $email;
                $buyer->phone = $phone;
                $buyer->password = $password;
                $buyer->auxiliary_email = $auxiliaryEmail;
                $buyer->device_name = $deviceName;
                $buyer->browser = $browser;
                if (preg_match('/\d{4}-\d{2}-\d{2}/', $primeExpiredDate)) {
                    $buyer->is_prime = strtotime($primeExpiredDate) - strtotime("now") > 0 ;
                    $buyer->prime_end_at = $primeExpiredDate;
                } else {
                    $buyer->is_prime = false;
                }
                $buyer->created_by = $createdBy;
                $buyer->save();

                if ($shippingAddress1 && $shippingZip) {
                    $shippingAddress = OwnedBuyerShippingAddress::firstOrNew(['owned_buyer_id' => $buyer->id, 'ship_postal_code' => $shippingZip]);
                    $shippingAddress->is_primary = true;
                    $shippingAddress->ship_address_1 = $shippingAddress1;
                    $shippingAddress->ship_address_2 = $shippingAddress2;
                    $shippingAddress->ship_address_3 = $shippingAddress3;
                    $shippingAddress->ship_city = $shippingCity;
                    $shippingAddress->ship_state = $shippingState;
                    $shippingAddress->ship_postal_code = $shippingZip;
                    $shippingAddress->ship_country = $shippingCountry;
                    $shippingAddress->shipping_phone = $shippingPhone;
                    $shippingAddress->save();
                }
                
                if ($paymentCardNumber) {
                    $paymentMethod = OwnedBuyerPaymentMethod::firstOrNew(['owned_buyer_id' => $buyer->id, 'card_number' => $paymentCardNumber]);
                    $paymentMethod->card_number = $paymentCardNumber;
                    $paymentMethod->last_4_digits = $paymentMethod->getCardLast4Digits();
                    $paymentMethod->available_balance = $availableBalance;

                    $paymentMethod->save();
                }
                
                $importedRecordsCount++;
            }
        }
        return $importedRecordsCount;
    }
}
