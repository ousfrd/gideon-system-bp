<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\OrderItem;
use App\Account;
use App\Listing;
use App\AdReport;
use App\Inventory;
use App\Excessnventory;
use App\Report;
use App\ProductStatistic;
use DB;
use App\Grids\ProductCostGrid;
use Illuminate\Support\Facades\Gate;
use App\Helpers\DataHelper;

class IndexController extends Controller {
	protected $site;
	
	public function __construct() {
		parent::__construct();
		$this->middleware ( 'auth' );
	}

	public function indexAction(Request $request) {
		if (Auth::user()->role == 'outsource_customer_service' || (Gate::denies('home') || Gate::denies('dashboard')) ) {
			return redirect(route('redeems.dashboard'));
		}
		$version = $request->get('version', 'v2');
		$box_route = $version == 'v1' ? route('order.call_backs.periodicGrossSales') : route('order.call_backs.periodicGrossSales.v2');
		$start_date = $request->get('start_date',date('Y-m-d'));
		$end_date = $request->get('end_date',date('Y-m-d'));
		$date_range = $request->get('date_range','Today');
		
		$account_id = $request->get('account','');
		$marketplace = $request->get('marketplace','all');

		$fulfillment_type = $request->get('fulfillment_type','all');
		if($account_id) {
			$account = Account::where('seller_id',$account_id)->firstOrFail();
			$account_code = $account->code;
			$marketplaces = config( 'app.marketplaceRegions' )[$account->region];
		}else{
			$account= 'All Accounts';
			$account_code = '';
			$marketplaces = config( 'app.marketplaces' );
		}
		$accounts = Account::getSortedAccounts();
		
		$asin = $request->get('product',0);
		// if($product_id) {
		// 	$product = Product::where('asin',$product_id)->first()->asin;
		// } else {
		// 	$product = "All Products";
		// }

		if(auth()->user()->role == 'product manager assistant') {
			$products = auth()->user()->productList();
			$productIds = auth()->user()->productIds();
		} else {
			$products = [];
			$productIds = [];
		}
		
        $orderStatuses = array_filter(Order::ORDER_STATUS, function($v) {
            return $v != 'Cancelled';
        });
		$params = [
			'account_id' => $account_id,
			'marketplace' => $marketplace,
			'fulfillment_type' => $fulfillment_type,
			'asin' => $asin,
			'products' => $products,
			'statuses' => $orderStatuses
		];
		
		// Refactor order report generation
		$reportType = $start_date == $end_date ? 'hourly' : 'daily';
		if ($reportType == 'hourly') {
			$startDate = \Carbon\Carbon::createFromFormat(
				'Y-m-d', $start_date, config('app.timezone'))->startOfDay()->setTimezone('UTC');
			$endDate = \Carbon\Carbon::createFromFormat(
				'Y-m-d', $end_date, config('app.timezone'))->endOfDay()->setTimezone('UTC');
			$queryParams = array_merge($params, ['purchase_date' => [
				'start_date' => $startDate->format('Y-m-d H:i:s'),
				'end_date' => $endDate->format('Y-m-d H:i:s')
			]]);
			$orderReport = ProductStatistic::getHourlyOrderReports($queryParams);

			$yesterdayStartDate = $startDate->copy()->subDays(1);
			$yesterdayEndDate = $endDate->copy()->subDays(1);
			$queryParams = array_merge($params, ['purchase_date' => [
				'start_date' => $yesterdayStartDate->format('Y-m-d H:i:s'),
				'end_date' => $yesterdayEndDate->format('Y-m-d H:i:s')
			]]);
			$orderReportYesterday = ProductStatistic::getHourlyOrderReports($queryParams);

			$startDateWeekAgo = $startDate->copy()->subDays(7);
			$endDateWeekAgo = $endDate->copy()->subDays(7);
			$queryParams = array_merge($params, ['purchase_date' => [
				'start_date' => $startDateWeekAgo->format('Y-m-d H:i:s'),
				'end_date' => $endDateWeekAgo->format('Y-m-d H:i:s')
			]]);
			$orderReportLastWeek = ProductStatistic::getHourlyOrderReports($queryParams);

			$startDateMonthAgo = $startDate->copy()->subMonth(1);
			$endDateMonthAgo = $endDate->copy()->subMonth(1);
			$queryParams = array_merge($params, ['purchase_date' => [
				'start_date' => $startDateMonthAgo->format('Y-m-d H:i:s'),
				'end_date' => $endDateMonthAgo->format('Y-m-d H:i:s')
			]]);
			$orderReportLastMonth = ProductStatistic::getHourlyOrderReports($queryParams);

			$startDateYearAgo = $startDate->copy()->subYear(1);
			$endDateYearAgo = $endDate->copy()->subYear(1);
			$queryParams = array_merge($params, ['purchase_date' => [
				'start_date' => $startDateYearAgo->format('Y-m-d H:i:s'),
				'end_date' => $endDateYearAgo->format('Y-m-d H:i:s')
			]]);
			$orderReportLastYear = ProductStatistic::getHourlyOrderReports($queryParams);
		} else {
			$startDate = \Carbon\Carbon::createFromFormat(
				'Y-m-d', $start_date, config('app.timezone'))->startOfDay();
			$endDate = \Carbon\Carbon::createFromFormat(
				'Y-m-d', $end_date, config('app.timezone'))->endOfDay();

			$queryParams = [
				'account_id' => $account_id,
				'asin' => $asin,
				'from_date' => $start_date,
				'to_date' => $end_date
			];
			if (array_key_exists($marketplace, config('app.marketplaceCountry'))) {
				$queryParams['country'] = config('app.marketplaceCountry')[$marketplace];
			}
			if (!empty($productIds)) {
				$queryParams['products'] = $productIds;
			}

			$orderReport = ProductStatistic::getDailyOrderReports($queryParams);

			$yesterdayStartDate = $startDate->copy()->subDays(1);
			$yesterdayEndDate = $endDate->copy()->subDays(1);
			$queryParams['from_date'] = $yesterdayStartDate->format('Y-m-d');
			$queryParams['to_date'] = $yesterdayEndDate->format('Y-m-d');
			$orderReportYesterday = ProductStatistic::getDailyOrderReports($queryParams);

			$startDateWeekAgo = $startDate->copy()->subDays(7);
			$endDateWeekAgo = $endDate->copy()->subDays(7);
			$queryParams['from_date'] = $startDateWeekAgo->format('Y-m-d');
			$queryParams['to_date'] = $endDateWeekAgo->format('Y-m-d');
			$orderReportLastWeek = ProductStatistic::getDailyOrderReports($queryParams);

			$startDateMonthAgo = $startDate->copy()->subMonth(1);
			$endDateMonthAgo = $endDate->copy()->subMonth(1);
			$queryParams['from_date'] = $startDateMonthAgo->format('Y-m-d');
			$queryParams['to_date'] = $endDateMonthAgo->format('Y-m-d');
			$orderReportLastMonth = ProductStatistic::getDailyOrderReports($queryParams);

			$startDateYearAgo = $startDate->copy()->subYear(1);
			$endDateYearAgo = $endDate->copy()->subYear(1);
			$queryParams['from_date'] = $startDateYearAgo->format('Y-m-d');
			$queryParams['to_date'] = $endDateYearAgo->format('Y-m-d');
			$orderReportLastYear = ProductStatistic::getDailyOrderReports($queryParams);
		}

		// $message = sprintf(
		// 	'[OrderReport] StartDate: %s, EndDate: %s, Params: %s, Report: %s',
		// 	$start_date, $end_date, json_encode($params), json_encode($orderReport));
		// Log::debug($message);

		$start_date_yesterday = date('Y-m-d',strtotime ("-1 day",strtotime ($start_date)));
		$end_date_yesterday = date('Y-m-d',strtotime("-1 day",strtotime ($end_date) ));

		$start_date_last_week = date('Y-m-d',strtotime ("-1 week",strtotime ($start_date)));
		$end_date_last_week = date('Y-m-d',strtotime("-1 week",strtotime ($end_date) ));

		$start_date_last_month = date('Y-m-d',strtotime ("-1 month",strtotime ($start_date)));
		$end_date_last_month = date('Y-m-d',strtotime("-1 month",strtotime ($end_date) ));

		$start_date_last_year = date('Y-m-d',strtotime ("-1 year",strtotime ($start_date)));
		$end_date_last_year = date('Y-m-d',strtotime ("-1 year",strtotime ($end_date)));

		$raw_qty_sql = "SELECT DISTINCT SUM(distinct_inventory.warehouse_quantity) AS warehouse_qty FROM (SELECT seller_id, status, warehouse_quantity, fulfillable_quantity, unsellable_quantity, reserved_quantity, total_quantity FROM product_inventory UNION SELECT seller_id, status, warehouse_quantity, fulfillable_quantity, unsellable_quantity, reserved_quantity, total_quantity FROM product_inventory) AS distinct_inventory WHERE distinct_inventory.status = 1";
		$allStockingTotalQty = DB::select(DB::raw($raw_qty_sql))[0]->warehouse_qty;

		$raw_cost_sql = "SELECT sum(distinct_inventory.warehouse_quantity * (products.cost+products.shipping_cost)) as total FROM (SELECT seller_id, product_id, status, warehouse_quantity, fulfillable_quantity, unsellable_quantity, reserved_quantity, total_quantity FROM product_inventory UNION SELECT seller_id, product_id, status, warehouse_quantity, fulfillable_quantity, unsellable_quantity, reserved_quantity, total_quantity FROM product_inventory) AS distinct_inventory join products on distinct_inventory.product_id = products.id WHERE distinct_inventory.status = 1";
		$allStockingCostTotal = DB::select(DB::raw($raw_cost_sql))[0]->total;

		if($account_id) {
			$raw_qty_sql = $raw_qty_sql . " AND distinct_inventory.seller_id = '".$account_id."'";
			$sellerStockingTotalQty = DB::select(DB::raw($raw_qty_sql))[0]->warehouse_qty;
			
			$raw_cost_sql = $raw_cost_sql . " AND distinct_inventory.seller_id = '".$account_id."'";
			$sellerStockingCostTotal = DB::select(DB::raw($raw_cost_sql))[0]->total;
		} else {
			$sellerStockingTotalQty = null;
			$sellerStockingCostTotal = null;
		}


		$data = [
				'account'=>$account,'account_id'=>$account_id,
				'account_code'=>$account_code,'accounts'=>$accounts,
				'marketplace'=>$marketplace,'marketplaces'=>$marketplaces,
				'asin'=>$asin,'products'=>$products,
				'params'=>$params,'fulfillment_type'=>$fulfillment_type,
				'sellerStockingTotalQty'=>$sellerStockingTotalQty, 
				'sellerStockingCostTotal'=>$sellerStockingCostTotal,
				'allStockingCostTotal'=>$allStockingCostTotal, 
				'allStockingTotalQty'=>$allStockingTotalQty,
				'start_date'=>$start_date,'end_date'=>$end_date,'date_range'=>$date_range,
				'start_date_yesterday'=>$start_date_yesterday,'end_date_yesterday'=>$end_date_yesterday,
				'start_date_last_week'=>$start_date_last_week,'end_date_last_week'=>$end_date_last_week,
				'start_date_last_month'=>$start_date_last_month,'end_date_last_month'=>$end_date_last_month,
				'start_date_last_year'=>$start_date_last_year,'end_date_last_year'=>$end_date_last_year,
				'orders'=>$orderReport,
				'orderReportYesterday'=>$orderReportYesterday,
				'orderReportLastWeek'=>$orderReportLastWeek,
				'orderReportLastMonth'=>$orderReportLastMonth,
				'orderReportLastYear'=>$orderReportLastYear,
				'box_route' => $box_route
		];

		$queryParams = [
			'account_id' => $account_id,
			'asin' => $asin,
			'from_date' => $start_date,
			'to_date' => $end_date
		];
		if (array_key_exists($marketplace, config('app.marketplaceCountry'))) {
			$queryParams['country'] = config('app.marketplaceCountry')[$marketplace];
		}
		if (!empty($productIds)) {
			$queryParams['products'] = $productIds;
		}
		$bestSellingProducts = ProductStatistic::getBestSellingProducts($queryParams);
		$bestSellingStatistics = $bestSellingProducts;
		$bestSellingAsins = array_map(function($bss) {
			return $bss['ASIN'];
		}, $bestSellingStatistics);
		$products = Product::select('id', 'asin', 'country', 'name', 'alias', 'ai_ads_open', 'ai_ads_commission_rate')
			->whereIn('asin', $bestSellingAsins)->get();
		$products = $products->groupBy(function($product) {
			return sprintf('%s-%s', strtoupper($product->country), $product->asin);
		})->toArray();
		foreach ($bestSellingStatistics as $countryAsin => $bestSeller) {
			if (!isset($products[$countryAsin])) {
				unset($bestSellingStatistics[$countryAsin]);
				continue;
			}

			list($country, $asin) = explode('-', $countryAsin);
			$product = current($products[$countryAsin]);
			$country = $product['country'];
			$bestSeller['marketplace'] = config('app.marketplaces')[$country];
			$linkPrefix = config('app.amzLinks')[strtoupper($country)];
			if (substr($linkPrefix, -1) == '/') {
				$linkPrefix = substr($linkPrefix, 0, -1);
			}
			$bestSeller['amazon_link'] = sprintf('%s/dp/%s', $linkPrefix, $asin);
			$bestSeller['product_id'] = $product['id'];
			$bestSeller['product_name'] = $product['name'];
			$bestSeller['alias'] = $product['alias'];
			$bestSeller['ai_ads_open'] = $product['ai_ads_open'];
			$bestSeller['ai_ads_commission_rate'] = $product['ai_ads_commission_rate'];

			$bestSellingStatistics[$countryAsin] = (object)$bestSeller;
		}
		// foreach ($products as $countryAsin => $productsByCountryAsin) {
		// 	if (!isset($bestSellingStatistics[$countryAsin])) {
		// 		continue;
		// 	}

		// 	list($country, $asin) = explode('-', $countryAsin);
		// 	$bestSeller = $bestSellingStatistics[$countryAsin];
		// 	$product = current($productsByCountryAsin);
		// 	$country = $product['country'];
		// 	$bestSeller['marketplace'] = config('app.marketplaces')[$country];
		// 	$bestSeller['amazon_link'] = sprintf(
		// 		'%s/dp/%s',
		// 		config('app.amzLinks')[strtoupper($country)], $asin);
		// 	$bestSeller['product_id'] = $product['id'];
		// 	$bestSeller['product_name'] = $product['name'];
		// 	$bestSeller['alias'] = $product['alias'];
		// 	$bestSeller['ai_ads_open'] = $product['ai_ads_open'];
		// 	$bestSeller['ai_ads_commission_rate'] = $product['ai_ads_commission_rate'];

		// 	$bestSellingStatistics[$countryAsin] = (object)$bestSeller;
		// 	// array_push($bestSellers, (object)$bestSeller);
		// }
		$data['bestsellers'] = array_values($bestSellingStatistics);
		$data['bestsellers_count'] = count($bestSellingStatistics);

		// if((strtotime($end_date) - strtotime($start_date)) <= 7 * 24 * 3600) {
		
		// 	$bestsellers = Order::bestSellersByDate($start_date, $end_date, $params);

		// 	$asins = array_pluck($bestsellers, 'ASIN');
		// 	$products = Product::whereIn('asin', $asins)->get();

		// 	$refunds = Order::refundsByAsins($start_date, $end_date, $params);
		// 	$refund_counts = Order::refundCountByAsins($start_date, $end_date, $params);

		// 	$ppc_counts = Order::ppcCountByAsins($start_date, $end_date, $params);

		// 	$asin_params = ['account_id'=>$account_id,'marketplace'=>$marketplace,'fulfillment_type'=>$fulfillment_type,'asins'=>$asins];
		// 	$ad_expenses = AdReport::totalByDateRangeAsinCountry($start_date, $end_date, $asin_params);

		// 	foreach ($bestsellers as $bestseller) {
		// 		$bestseller->marketplace = config('app.marketplaces')[$bestseller->country];
		// 		$bestseller->amazon_link = config('app.amzLinks')[strtoupper($bestseller->country)].'dp/'.$bestseller->ASIN;
				
		// 		foreach($products as $product) {
		// 			if($product->asin == $bestseller->ASIN and $product->country == $bestseller->country) {
		// 				$bestseller->product_id = $product->id;
		// 				$bestseller->product_name = $product->name;
		// 				$bestseller->alias = $product->alias;
		// 				$bestseller->ai_ads_open = $product->ai_ads_open;
		// 				$bestseller->ai_ads_commission_rate = $product->ai_ads_commission_rate;
		// 			}
		// 		}

		// 		foreach($ad_expenses as $ad_expense) {
		// 			if($ad_expense->asin == $bestseller->ASIN and $bestseller->country == $ad_expense->country) {
		// 				$bestseller->adExpenses = $ad_expense->total;
		// 			}
		// 		}

		// 		foreach ($refunds as $refund) {
		// 			if($bestseller->ASIN == $refund->asin and $bestseller->country == $refund->country) {
		// 				$bestseller->total_refund = $refund->total_refund;
		// 			}
		// 		}

		// 		foreach ($refund_counts as $refund_count) {
		// 			if($bestseller->ASIN == $refund_count->asin and $bestseller->region == $refund_count->region) {
		// 				$bestseller->refund_count = $refund_count->refund_count;
		// 			}
		// 		}

		// 		foreach ($ppc_counts as $ppc_count) {
		// 			if($bestseller->ASIN == $ppc_count->asin and $bestseller->country == $ppc_count->country) {
		// 				$bestseller->ppc_count = $ppc_count->ppc_count;
		// 				$bestseller->ads_sales = $ppc_count->ads_sales;
		// 			}
		// 		}

		// 	}

		// 	$sortable_bestsellers = [];
		// 	foreach ($bestsellers as $bestseller) {
		// 		$bestseller->gross_sales = $bestseller->total_gross	+	$bestseller->shipping_fee;
		// 		$bestseller->payout = $bestseller->total_gross + $bestseller->shipping_fee + $bestseller->vat_fee + $bestseller->fba_fee + $bestseller->commission + $bestseller->total_promo + $bestseller->total_refund - $bestseller->adExpenses;
		// 		$bestseller->profit = $bestseller->payout - $bestseller->total_cost;
		// 		$sortable_bestsellers[] = $bestseller;
		// 	}

		// 	usort($sortable_bestsellers, array($this,'sortBestSellerByProfit'));

		// 	$data['bestsellers'] = $sortable_bestsellers;
		// 	$data['bestsellers_count'] = count($sortable_bestsellers);
		// } else {
		// 	$bestsellers_count = Order::bestSellersCountByDate($start_date, $end_date, $params);
		// 	$data['bestsellers_count'] = $bestsellers_count;
		// }
		
		// if(auth()->user()->role == "product manager assistant") {
		// 	$nocost_products = Product::select('id', 'asin', 'name', 'cost', 'shipping_cost')->where("cost", 0)->where('discontinued', false)->where("shipping_cost", 0)->where('status', 1)->where('warehouse_qty', '>=', 1)->whereIn('products.asin',auth()->user()->productList())->get();
		// } else {
		// 	$nocost_products = Product::select('id', 'asin', 'name', 'cost', 'shipping_cost')->where("cost", 0)->where('discontinued', false)->where("shipping_cost", 0)->where('status', 1)->where('warehouse_qty', '>=', 1)->get();
		// }
		
		$data['nocost_products'] = Product::getProductsWithoutCost();

		return view('index.index', $data);

	}

	public function addcostAction(Request $request) {
		$nocost_products = Product::select('id', 'asin', 'name', 'cost', 'shipping_cost')->where("cost", 0)->where("shipping_cost", 0)->where('status', 1)->where('warehouse_qty', '>', 1)->get();

		return view('index.addcost', ['nocost_products' => $nocost_products]);
	}

	public function addProductCostAction(Request $request) {

		$grid = ProductCostGrid::grid()->render();

		return view('index.productcost', ['grid'=>$grid]);
	}

	public function searchAction(Request $request){
		$q = $request->get('q');
		
		if (Auth::user()->role == 'outsource_customer_service') {
			if(!(strlen($q) == 19 && strlen(explode("-",$q)[0]) == 3 && strlen(explode("-",$q)[1]) == 7)) {
				return abort(404);
			} else {
				return redirect()->to(route('order.view', [$q]));
			}
		}

		if(substr($q, 0,2) == 'B0' and strlen($q) == 10) {
			$products = Product::where('asin',$q)->get();
			if(count($products) == 1) {
				return redirect()->route('product.view',[$q]);
			} else {
				return redirect()->to(route('products',['Products[filters][asin-like]'=>$q]));
				//route('products',[$q]);
			}
		}

		if(substr($q, 0,2) == 'B0' and strlen($q) < 10) {
			return redirect()->to(route('products',['Products[filters][asin-like]'=>$q]));
		}

		//search by order id
		if(strlen($q) == 19 && strlen(explode("-",$q)[0]) == 3 && strlen(explode("-",$q)[1]) == 7) {
			return redirect()->to(route('order.view', [$q]));
		}

		//search by sku
		try{
			$listing = Listing::where('sku',$q)->firstOrFail();
			return redirect()->route('listing.view',[$listing->id]);
		} catch(\Exception $e){
			
		}

		//search by name
		try{
			$products = Product::where('name', 'like', '%'. $q .'%')->get();
			if(count($products) == 1) {
				return redirect()->route('product.view',[$products[0]->id]);
			} else {
				return redirect()->to(route('products',['Products[filters][name-like]'=>$q]));
				//route('products',[$q]);
			}
		} catch(\Exception $e){
			
		}

	}


	
	public function globalAlertsAction(){
		
		return view('layouts.parts.alerts',[
				'low_stock_count'=> Product::lowOrOutOfStockingCount(),
				'excess_count'=> Excessnventory::total()
		]);
	
	}



	private function sortBestSellerByProfit($a, $b)
	{
	    if($a->profit == $b->profit){ return 0 ; }
		return ($a->profit < $b->profit) ? 1 : -1;
	}
}