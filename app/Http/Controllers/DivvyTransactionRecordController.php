<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DivvyTransactionRecord;
use App\Grids\DivvyTransactionGrid;
use App\OwnedBuyerPaymentMethod;

class DivvyTransactionRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $grid = (new DivvyTransactionGrid())->generateGrid();
        return view('divvy-transaction-record.index', ['request'=>$request, 'grid' => $grid]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function import(Request $request) {
        if ($request->hasFile('divvy_transaction_csv')) {
            $filePath = $request->file('divvy_transaction_csv')->getRealPath();
            $importedRecordsCount = $this->importTransaction($filePath);
            $request->session()->flash('success', 'successfully imported ' . $importedRecordsCount . ' Divvy Transaction records');
            return redirect()->route('divvy-transaction-record.index');
        }
    }

    private function importTransaction($filePath){
        $handle = fopen($filePath, "r");
        $importedRecordsCount = 0;
        while ( ($row = fgetcsv($handle) ) !== FALSE ) {
            if ($importedRecordsCount == 0) {
                $importedRecordsCount++;
                continue;
            }
            $record = DivvyTransactionRecord::firstOrNew(['transaction_id'=>$row[0]]);
            $record->split_from = $row[1];
            $record->date = $row[2];
            $record->first_name = $row[3];
            $record->last_name = $row[4];
            $record->merchant = $row[5];
            $record->clean_merchant_name = $row[6];
            $record->amount = $row[7];
            $record->amount_in_number = -1 * (floatval(preg_replace('/[^0-9\.\,-]/', '', $row[7])));
            $record->card_name = $row[8];
            $record->card_type = $row[9];
            $record->card_last_4 = substr(trim($row[10]), -4);
            $record->card_exp_date = $row[11];
            $record->reviews = $row[12];
            $record->receipt_filename = $row[13];
            $record->budget_id = $row[14];
            $record->card_id = $row[15];
            $record->user_id = $row[16];
            $record->authorized_at = $row[17];
            $record->budget = $row[18];
            $record->has_receipt = $row[19];
            $record->save();
            $importedRecordsCount++;
        }
        OwnedBuyerPaymentMethod::updateCostFromTransactionRecord();
        return $importedRecordsCount;
    }
}
