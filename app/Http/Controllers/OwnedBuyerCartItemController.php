<?php

namespace App\Http\Controllers;

use App\OwnedBuyerCartItem;
use Illuminate\Http\Request;
use App\Grids\OwnedBuyerCartItemGrid;

class OwnedBuyerCartItemController extends Controller
{

    public function __construct()
	{
		parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = OwnedBuyerCartItem::gridQuery();
        $grid = (new OwnedBuyerCartItemGrid($query))->generateGrid();
        return view('owned-buyer-cart-item.index', ['grid'=>$grid]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $asin = $request->input('asin');
        $ownedBuyerId = $request->input('owned_buyer_id');
        $redirectTo = $request->input('redirect_to');
        $cartItem = OwnedBuyerCartItem::firstOrNew(['asin' => $asin, 'owned_buyer_id'=> $ownedBuyerId]);
        if ($cartItem->id) {
            $request->session()->flash('message.alert', 'This item has already in your cart');
        } else {
            $cartItem->save();
            $request->session()->flash('message.success', 'Item has been added successfully');
        }
        if (empty($redirectTo)) { $redirectTo = route('owned-buyer-cart-item.index'); }
        return redirect($redirectTo);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OwnedBuyerCartItem  $ownedBuyerCartItem
     * @return \Illuminate\Http\Response
     */
    public function show(OwnedBuyerCartItem $ownedBuyerCartItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OwnedBuyerCartItem  $ownedBuyerCartItem
     * @return \Illuminate\Http\Response
     */
    public function edit(OwnedBuyerCartItem $ownedBuyerCartItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OwnedBuyerCartItem  $ownedBuyerCartItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OwnedBuyerCartItem $ownedBuyerCartItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OwnedBuyerCartItem  $ownedBuyerCartItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, OwnedBuyerCartItem $ownedBuyerCartItem)
    {
        $ownedBuyerCartItem->delete();
        if ($request->ajax()) {
            return response('Deleted', 200);
        } else {
            return redirect()->route('owned-buyer-cart-item.index');
        }
    }
}
