<?php

namespace App\Http\Controllers;

use App\ProductStatistic;
use Illuminate\Http\Request;

class ProductStatisticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductStatistic  $productStatistic
     * @return \Illuminate\Http\Response
     */
    public function show(ProductStatistic $productStatistic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductStatistic  $productStatistic
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductStatistic $productStatistic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductStatistic  $productStatistic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductStatistic $productStatistic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductStatistic  $productStatistic
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductStatistic $productStatistic)
    {
        //
    }
}
