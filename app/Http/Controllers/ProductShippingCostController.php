<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductShippingCost;

class ProductShippingCostController extends Controller
{
    /**
	 * authencated user only
	 */
	public function __construct()
	{
		parent::__construct();
		$this->middleware('auth');
  }


	public function store(Request $request) {
		$productId = $request->input('product_id');
		$startDate = $request->input('start_date');
		$cost = $request->input('cost');
		$productCost = ProductShippingCost::firstOrNew(['product_id'=>$productId, 'start_date'=>$startDate]);
		$productCost->cost_per_unit = $cost;
		$productCost->save();
		return response()->json($productCost);
	}

	public function update(Request $request) {

	}

	public function destroy(ProductShippingCost $productShippingCost) {
		$productShippingCost->delete();
		return response('deleted', 200);
	}

}
