<?php

namespace App\Http\Controllers;
use App\ProductUpdate;
use App\Product;
use App\Brand;
use Illuminate\Http\Request;

class ProductUpdateController extends Controller
{
    //
    // curl -d '{"asin": "B07BJCMK6W", "sales_ranks": "2048", "brand": "Piero Lorenzo", "small_image": "https://images-na.ssl-images-amazon.com/images/I/41rBrcLSW9L._SS40_.jpg", "weight": "0.2"}' -H "Content-Type: application/json" -X POST  http://homestead.test/product/update-info
    // curl -d '{"asin": "B0843TKP2C", "sales_ranks": "34421", "brand": "TOMIYA", "small_image": "https://images-na.ssl-images-amazon.com/images/I/816jmvbyLeL._AC_SX679_.jpg", "weight": "0.2"}' -H "Content-Type: application/json" -X POST  http://homestead.test/product/update-info
    function updateInfo(Request $request) {
        $asin = $request->input('asin');
        
        $product = Product::where('asin', $asin)->get();
		$brand = $request->input('brand');
		$small_image = $request->input('small_image');
        $weight = $request->input('weight');
        $sales_ranks = $request->input('sales_ranks');
        
        ProductUpdate::updateInfo($asin, $brand, $small_image, $weight, $sales_ranks);
        $name = $brand;
        Brand::insertBrand($name);
        return response("saved", 200);
	}

}
