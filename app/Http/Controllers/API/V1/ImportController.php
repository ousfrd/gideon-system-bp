<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Import;


class ImportController extends Controller
{
	public function indexAction(Request $request) {
		$params = [
			'seller_id' => $request->query('seller_id', NULL),
			'marketplace' => $request->query('marketplace', NULL),
			'report_type' => $request->query('report_type', NULL),
			'date' => $request->query('date', NULL)
		];

		$imports = Import::getImports($params);

		return response($imports, 200);
	}
}
