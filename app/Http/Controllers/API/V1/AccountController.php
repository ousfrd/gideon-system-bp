<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Http\Controllers\API\V1;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Account;


class AccountController extends Controller
{
	public function indexAction(Request $request) {
		$fields = [
			'id', 'seller_id', 'marketplaces', 'name', 'code',
			'disburse_enabled', 'min_disburse_amount', 'status'
		];

		$query = Account::select($fields);
		$sellerId = $request->query('seller_id', NULL);
		if ($sellerId) {
			$accounts = $query->where('seller_id', $sellerId)->get();
		} else {
			$status = $request->query('status', NULL);
			if ($status) {
				$accounts = $query->where('status', $status)->get();
			} else {
				$accounts = $query->all();
			}
		}

		return response()->json($accounts, 200);
	}
}
