<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Http\Controllers\API\V1;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Product;
use App\ProductStatistic;


class ProductController extends Controller
{
	public function getProductOverviewsAction(Request $request) {
		$sellerId = $request->get('account', 'all');
        $startDate = $request->get('start_date', date('Y-m-d', strtotime('-30 days')));
		$endDate = $request->get('end_date', date('Y-m-d'));
        $marketplace = $request->get('marketplace', 'all');

		$params = [
        	'from_date' => $startDate,
        	'to_date' => $endDate,
        ];
        if (!empty($marketplace) && $marketplace != 'all') {
            $params['country'] = $marketplace;
        }
        if (!empty($sellerId) && $sellerId != 'all') {
        	$params['account_id'] = $sellerId;
        }
		$overviews = ProductStatistic::getProductOverviews($params);

		return response()->json($overviews, 200);
	}
}
