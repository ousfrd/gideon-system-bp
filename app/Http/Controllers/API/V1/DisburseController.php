<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Http\Controllers\API\V1;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Disburse;


class DisburseController extends Controller
{
	public function indexAction(Request $request) {
		$params = [
			'seller_id' => $request->query('seller_id', NULL),
			'marketplace' => $request->query('marketplace', NULL),
			'date' => $request->query('date', NULL)
		];

		$disburses = Disburse::getDisburses($params);

		return response($disburses, 200);
	}

	public function createAction(Request $request) {
		$disburseParams = $request->input('disburse');
		$sellerId = $disburseParams['seller_id'];
		$marketplace = $disburseParams['marketplace'];
		$disburseDateStr = $disburseParams['disburse_date'];
		$disburseDate = \Carbon\Carbon::parse($disburseDateStr);
		$formattedDisburseDateStr = $disburseDate->format('Y-m-d');

		$disburses = Disburse::getDisburses([
			'seller_id' => $sellerId,
			'marketplace' => strtoupper($marketplace),
			'date' => $formattedDisburseDateStr
		]);
		if (empty($disburses) || $disburses->count() <= 0) {
			$disburse = new Disburse();
			$disburse->fill($disburseParams);
		} else {
			$disburse = $disburses->first();

			$disburse->fill($disburseParams);
		}

		$disburse->save();

		return response($disburse, 200);
	}
}
