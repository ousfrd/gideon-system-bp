<?php

namespace App\Http\Controllers;

use App\LeaveReviewTask;
use Illuminate\Http\Request;
use App\Grids\LeaveReviewTaskGrid;
use App\Grids\ManagedReviewOrderGrid;
use App\ManagedReviewOrder;
use App\User;
use App\LeaveReviewSubTask;

class LeaveReviewTaskController extends Controller
{
    public function __construct()
	{
		parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grid = (new LeaveReviewTaskGrid)->generateGrid();
        return view('leave-review-task.index', [ 'grid'=>$grid ]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $selfReviewStaffs = User::selfReviewStaffs();
        $leaveReviewTask = new LeaveReviewTask;
        return view('leave-review-task.create', [
            'leaveReviewTask' => $leaveReviewTask,
            'selfReviewStaffs' => $selfReviewStaffs
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $leaveReviewTask = new LeaveReviewTask($request->all());
        $leaveReviewTask->created_by = auth()->user()->id;
        $leaveReviewTask->save();
        $this->saveSubTasks($request, $leaveReviewTask->id);
        return redirect()->route('leave-review-task.show', [
            'leaveReviewTask' => $leaveReviewTask
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LeaveReviewTask  $leaveReviewTask
     * @return \Illuminate\Http\Response
     */
    public function show(LeaveReviewTask $leaveReviewTask)
    {
        $query = ManagedReviewOrder::gridQueryForTask($leaveReviewTask);
        $grid = (new ManagedReviewOrderGrid($query))->generateOverviewGrid();
        $selfReviewStaffs = array_flip(User::selfReviewStaffs());
        $subTasks = $leaveReviewTask->subTasks;
        return view('leave-review-task.show', [
            'leaveReviewTask' => $leaveReviewTask,
            'selfReviewStaffs' => $selfReviewStaffs,
            'subTasks' => $subTasks,
            'grid' => $grid
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LeaveReviewTask  $leaveReviewTask
     * @return \Illuminate\Http\Response
     */
    public function edit(LeaveReviewTask $leaveReviewTask)
    {
        $selfReviewStaffs = User::selfReviewStaffs();
        $subTasks = array_column($leaveReviewTask->subTasks->toArray(), 'goal', 'user_id');
        return view('leave-review-task.edit', [
            'leaveReviewTask' => $leaveReviewTask,
            'selfReviewStaffs' => $selfReviewStaffs,
            'subTasks' => $subTasks
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LeaveReviewTask  $leaveReviewTask
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LeaveReviewTask $leaveReviewTask)
    {
        $leaveReviewTask->update($request->all());
        $this->saveSubTasks($request, $leaveReviewTask->id);
        return redirect()->route('leave-review-task.show', ['leaveReviewTask' => $leaveReviewTask]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LeaveReviewTask  $leaveReviewTask
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, LeaveReviewTask $leaveReviewTask)
    {
        $leaveReviewTask->delete();
        if ($request->ajax()) {
            return response('Deleted', 200);
        } else {
            return redirect()->route('leave-review-task.index');
        }
    }

    private function saveSubTasks(Request $request, $taskId){
        $data = $request->all();
        foreach ($data as $key=>$value) {
            $matches = [];
            if ($value && $value > 0 && preg_match('/owned_buyer_staff_goal_(\d+)/', $key, $matches)) {
                $userId = $matches[1];
                $subTask = LeaveReviewSubTask::firstOrNew(['user_id' => $userId,'leave_review_task_id' => $taskId]);
                $subTask->goal = $value;
                $subTask->save();
            }
        }
    }
}
