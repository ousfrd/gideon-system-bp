<?php 
namespace App\Http\Controllers;
use App\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Grids\UserGrid;

use Illuminate\Support\Facades\Validator;
use App\Grids\ProductGrid;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->middleware('auth');
	}
	
	
	/**
	 * Show the profile for the given user.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function indexAction(Request $request)
	{
		 
		$this->authorize('manage-users');
		
		$input = $request->get("Users");
		
		
		 
		$title = "All Users";
		 
		$grid = UserGrid::grid()->render();
		
// 		Auth::user()->sendAccountCreatedByAdminEmail('test');
		return view('user.index', ['grid' => $grid,"title"=>$title]);
	}
	
	
	function editAction(Request $request, $user_id) {
		$this->authorize('manage-users');
		
		$user = User::where('id',$user_id)->firstOrFail();
		
		if( $data = $request->all()){
			
			if($user->verified == 0 && $data['verified'] == 1) {
				$user->sendAccountVerifiedEmail();
			}
			
			$user->name = $data['name'];
			$user->email = $data['email'];
			$user->verified = $data['verified'];
			$user->email_notice = $data['email_notice'];
			$user->role = $data['role'] ? join(",", $data['role']) : null;
			$user->save();
			
			
			//remove all current before add new ones
			$user->products()->detach();
			if(isset($data['products'])) {

				foreach ( explode(",", $data['products']) as $pid)
				{
					$user->products()->attach($pid);
				}
			}
			
			
			session()->flash('msg','User account has been updated successfully.');
			return redirect()->route('users');
		}
		
		$template = $request->ajax()?'user.form.edit':'user.edit';
		$userRoles = explode(",",$user->role);
		return view($template, ['user' => $user,'ajax'=>$request->ajax(), 'userRoles'=>$userRoles]);
		
	}
	
	function viewAction(Request $request,$id){

		$version = $request->get('version', 'v2');
		$box_route = $version == 'v1' ? route('order.call_backs.periodicGrossSales') : route('order.call_backs.periodicGrossSales.v2');
		
		$user = User::where('id',$id)->firstOrFail();
		
		
		$start_date = $request->get('start_date',date('Y-m-d',strtotime('-7 days')));
		$end_date = $request->get('end_date',date('Y-m-d',strtotime('-1 days')));
		$date_range= $request->get('date_range','Last 7 Days');
		$params = ['user_id'=>$user->id];
		
		
		$orderReport = $user->orderReports($start_date, $end_date);
		
		$orderReportLastWeek = $user->orderReports(date('Y-m-d',strtotime($start_date) - 7 * 24 * 3600), date('Y-m-d',strtotime($end_date) - 7 * 24 * 3600));
		$orderReport30DayAgo = $user->orderReports(date('Y-m-d',strtotime($start_date) - 30 * 24 * 3600), date('Y-m-d',strtotime($end_date) - 30 * 24 * 3600));
		
		
		
		$users=  User::productManagerList();
		
		$managerSources = [];
		foreach ($users as $id=>$name) {
			$managerSources[] = ['id'=>$id,'text'=>$name];
		}
		
		
		$product_grid = ProductGrid::grid($request,['asins'=>$user->productList()]);
		
		return view('user.view',['user' => $user,'start_date'=>$start_date,'end_date'=>$end_date,'date_range'=>$date_range,
				'orders'=>$orderReport,
				'managerSources'=>$managerSources,
				
				'orderReport30DayAgo'=>$orderReport30DayAgo,'orderReportLastWeek'=>$orderReportLastWeek,
				'params'=>$params,
				'product_grid'=>$product_grid,
				'box_route' => $box_route
		]);
		
		
	}
	
	
	function addAction(Request $request){
		$this->authorize('manage-users');
		
		if( $data = $request->all()){
			$userRoles = $data['role'] ? join(',',$data['role']) : null;
			$data['role'] = $userRoles;
			$this->validator($data)->validate();
			
			$user = $this->create($data);
			
			if($user->verified){
				// $user->sendAccountCreatedByAdminEmail($data['password']);
			};
			
			
			// if(isset($data['products'])) {
			// 	foreach ( $data['products'] as $pid)
			// 	{
			// 		$user->products()->attach($pid);
			// 	}
			// }
			if(isset($data['products'])) {
				foreach ( explode(",", $data['products']) as $pid)
				{
					$user->products()->attach($pid);
				}
			}
			
			session()->flash('msg','New user account has been created successfully.');
			
			if($request->ajax()) {
				echo 1;
				return;
			}else{
				return redirect()->route('users');
			}
			//return $this->registered($request, $user) ?: redirect($this->redirectPath());
		}
		
		
		$user = new User;
		return view('user.add', ['user' => $user]);
	}

	function changePassword(Request $request) {
		return view('user.change-password', ['request'=>$request]);
	}

	function doChangePassword(Request $request) {
		$user = auth()->user();
		$currentPassword = trim($request->input('currentPassword'));
		$newPassword = trim($request->input('newPassword'));
		$confirmNewPassword = trim($request->input('confirmNewPassword'));
		if (strlen($currentPassword) < 8 || strlen($newPassword) < 8 || strlen($confirmNewPassword) < 8) {
			$request->session()->flash('alert', 'Password length should longer than 8 characters');
		} else {
			if ($newPassword != $confirmNewPassword) {
				$request->session()->flash('alert', 'New Password and Confirm New Password are different.');
			} else if (!Hash::check($currentPassword, $user->password)) {
				$request->session()->flash('alert', "Current Password you inputed doesn't match our record");
			} else {
				$user->password = Hash::make($newPassword);
				$user->save();
				$request->session()->flash('success', "You successfully changed your password! Now you can log out and log in with your new password");
			}
		}
		return redirect()->route('user.change-password');

	}
	
	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
				'name' => 'required|string|max:255',
				'email' => 'required|string|email|max:255|unique:users',
				'password' => 'required|string|min:6|confirmed',
		]);
	}
	
	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	protected function create(array $data)
	{
		return User::create([
				'name' => $data['name'],
				'email' => $data['email'],
				'verified'=>$data['verified'],
				'role'=>$data['role'],
				'password' => bcrypt($data['password']),
		]);
	}

}