<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ManagedReviewOrder;
use App\Grids\ManagedReviewOrderGrid;

class ManagedReviewOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = ManagedReviewOrder::gridQuery();
        $grid = (new ManagedReviewOrderGrid($query))->generateOverviewGrid();
        $orderTypes = ManagedReviewOrder::$types;
        return view('managed-review-order.index', [
            'orderTypes' => $orderTypes,
            'grid'=> $grid,
            'tab' => 'all'
        ]);
    }

    public function ownOrders(Request $request)
    {
        $query = ManagedReviewOrder::selfOrdersQuery();
        $grid = (new ManagedReviewOrderGrid($query))->generateOverviewGrid();
        $orderTypes = ManagedReviewOrder::$types;
        return view('managed-review-order.index', [
            'orderTypes' => $orderTypes,
            'grid'=> $grid,
            'tab'=>'self-orders'
        ]);
    }

    public function checkReviewOrders(Request $request)
    {
        $query = ManagedReviewOrder::checkReviewOrderQuery();
        $grid = (new ManagedReviewOrderGrid($query))->generateOverviewGrid();
        $orderTypes = ManagedReviewOrder::$types;
        return view('managed-review-order.index', [
            'orderTypes' => $orderTypes,
            'grid'=> $grid,
            'tab'=>'check-review-orders'
        ]);
    }

    public function needReviewOrders(Request $request)
    {
        $query = ManagedReviewOrder::needReviewOrderQuery();
        $grid = (new ManagedReviewOrderGrid($query))->generateOverviewGrid();
        $orderTypes = ManagedReviewOrder::$types;
        return view('managed-review-order.index', [
            'orderTypes' => $orderTypes,
            'grid'=> $grid,
            'tab'=>'need-review-orders'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orderId = $request->input('amazon_order_id');
        $ownedBuyerId = $request->input('owned_buyer_id');
        if (empty($orderId)) {
            $managedReviewOrder = new ManagedReviewOrder();
        } else {
            $managedReviewOrder = ManagedReviewOrder::firstOrNew(['amazon_order_id' => $orderId]);
        }
        $managedReviewOrder->owned_buyer_id = $ownedBuyerId;
        $managedReviewOrder->order_date = $request->input('order_date');
        $managedReviewOrder->order_type = $request->input('order_type');
        $managedReviewOrder->asin = $request->input('asin');
        $managedReviewOrder->order_cost = $request->input('order_cost');
        $managedReviewOrder->need_review = $request->input('need_review');
        $managedReviewOrder->save();
        return redirect()->route('owned-buyer.show', ['id' => $ownedBuyerId]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $managedReviewOrder = ManagedReviewOrder::find($id);
        $updatingAttribute = $request->input('pk');
        $managedReviewOrder->$updatingAttribute = $request->input('value');
        if ($updatingAttribute && substr($updatingAttribute, 0, 6) === "review") {
            $managedReviewOrder->left_review_by = auth()->user()->id;
        }
        $managedReviewOrder->save();
        return response()->json($managedReviewOrder);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $managedReviewOrder = ManagedReviewOrder::find($id);
        $managedReviewOrder->delete();
        return response('deleted', 200);
    }
}
