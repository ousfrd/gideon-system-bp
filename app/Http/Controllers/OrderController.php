<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Grids\OrderGrid;
use App\Grids\CSOrderGrid;
use Formatter;


use App\OrderItem;

use Input;
use Validator;
use App\Account;
use Illuminate\Support\Facades\Auth;
use App\ListingExpense;
use App\AdPayment;
use App\AdReport;
use App\User;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->middleware('auth');
	}
	
	function ordersAction(Request $request,$schedule_id=null) {
		$grid = new OrderGrid();
		$grid = $grid->ordersGrid()->render();
		
		return view('order.orders',['grid'=>$grid]);
	}
	
	function csordersAction(Request $request,$schedule_id=null) {
		if (isset($request['Orders'])){
			$PurchaseDate_gt = $request['Orders']['filters']['PurchaseDate-gt_e'];
			$PurchaseDate_ls = $request['Orders']['filters']['PurchaseDate-ls_e'];
			$AmazonOrderId = $request['Orders']['filters']['AmazonOrderId-eq'];
			$items = $request['Orders']['filters']['items-eq'];
			$ShippingAddressAddressLine1 = $request['Orders']['filters']['ShippingAddressAddressLine1-eq'];
			$ShippingAddressFirstName = $request['Orders']['filters']['ShippingAddressFirstName-eq'];
			$ShippingAddressAddressLine2 = $request['Orders']['filters']['ShippingAddressAddressLine2-eq'];
			$ShippingAddressCity = $request['Orders']['filters']['ShippingAddressCity-eq'];
			$ShippingAddressStateOrRegion = $request['Orders']['filters']['ShippingAddressStateOrRegion-eq'];
			$ShippingAddressCountryCode = $request['Orders']['filters']['ShippingAddressCountryCode-eq'];
			$ShippingAddressPostalCode = $request['Orders']['filters']['ShippingAddressPostalCode-eq'];
			$result = !empty($ShippingAddressPostalCode) || !empty($ShippingAddressCountryCode) || !empty($ShippingAddressStateOrRegion) || !empty($ShippingAddressCity) || !empty($ShippingAddressAddressLine2) || !empty($AmazonOrderId) || !empty($items) || !empty($ShippingAddressAddressLine1) || !empty($ShippingAddressFirstName) || !empty($PurchaseDate_ls) || !empty($PurchaseDate_gt);
			
			if ($result) {
				$dataProvider = Order::with ( [ 'account', 'items' ] );
			} else {
				$dataProvider = Order::whereRaw(0);
			}
		} else {
			$dataProvider = Order::whereRaw(0);
		}
		$grid = new CSOrderGrid();
		
		$grid = $grid->ordersGrid($dataProvider)->render();
		return view('order.orders',['grid'=>$grid]);
		
	}
	
	function viewAction(Request $request,$id) {
		$order = Order::getById($id);
		return view('order.view', ['order' => $order]);
	}


	
	public function ajaxsaveAction(Request $request) {
        $value = $request->get('value');
        list($type,$id) = explode('-', $request->get('pk'));

        try {
            Order::where('id', $id)->update([$type => $value]);

            echo 1;
        } catch (\Exception $e) {
            print $e->getMessage();
        }
	}
	
	public function orderItems(Request $request) {
		$amazon_order_id = $request->amazon_order_id;
		$orderItems = OrderItem::where('AmazonOrderId', $amazon_order_id)->get()->toArray();
		return response()->json($orderItems);
	}
	
	
}
