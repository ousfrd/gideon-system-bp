<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Shipment;
use App\OrderItemMarketing;
use App\Redeem;
use App\User;
use Illuminate\Http\Request;
use App\Grids\CustomerGrid;
use App\Grids\ShipmentGrid;
use App\Grids\RedeemGrid;
use App\Account;
use App\Product; 
use App\CustomerExportor;

use Excel;

class CustomerController extends Controller
{

    public function __construct()
	{
		parent::__construct();
        $this->middleware('auth');
        $this->middleware('can:customers.index')->only('index');
        $this->middleware('can:customers.create')->only('create');
        $this->middleware('can:customers.store')->only('store');
        $this->middleware('can:customers.show')->only('show');
        $this->middleware('can:customers.edit')->only('edit');
        $this->middleware('can:customers.update')->only('update');
        $this->middleware('can:customers.destroy')->only('destroy');
    }

    public function dashboard() {
        $customersCountByCountry = Customer::customersCountByCountry();
        $customersCount = Customer::totalCustomersCount();
        $repeatBuyersCount = Customer::repeatBuyersCount();
        $realEmailsCount = Customer::realEmailsCount();
        $positiveReviewersCount = Customer::positiveReviewersCount();
        $negativeReviewersCount = Customer::negativeReviewersCount();
        return view('customer.dashboard', [
            "customersCount" => $customersCount,
            "repeatBuyersCount" => $repeatBuyersCount,
            "realEmailsCount" => $realEmailsCount,
            "positiveReviewersCount" => $positiveReviewersCount,
            "negativeReviewersCount" => $negativeReviewersCount,
            "customersCountByCountry" => $customersCountByCountry
        ]);
    }

    public function countryDashboard(Request $request) {
        $country = $request->country;
        $customersCount = Customer::totalCustomersCount($country);
        $repeatBuyersCount = Customer::repeatBuyersCount($country);
        $realEmailsCount = Customer::realEmailsCount($country);
        $positiveReviewersCount = Customer::positiveReviewersCount($country);
        $negativeReviewersCount = Customer::negativeReviewersCount($country);
        $stateCustomersCount = Customer::stateCustomersCountByCountry($country);
        return view('customer.country-dashboard', [
            "country" => $country,
            "customersCount" => $customersCount,
            "repeatBuyersCount" => $repeatBuyersCount,
            "realEmailsCount" => $realEmailsCount,
            "positiveReviewersCount" => $positiveReviewersCount,
            "negativeReviewersCount" => $negativeReviewersCount,
            "stateCustomersCount" => $stateCustomersCount
        ]);
    }

    public function all(Request $request) {
        $country = $request->country;
        $query = Customer::allCustomersQuery($country);
        $grid = (new CustomerGrid($query))->allGrid();
        return view('customer.all', [
            'grid' => $grid,
            'country' => $country
        ]);
    }

    public function repeatBuyers(Request $request) {
        $country = $request->country;
        $query = Customer::repeatBuyersQuery($country);
        $grid = (new CustomerGrid($query))->allGrid();
        return view('customer.repeat-buyers', [
            'grid' => $grid,
            'country' => $country,
            'title' => 'Repeat Buyers'
        ]);
    }

    public function realEmailBuyers(Request $request) {
        $country = $request->country;
        $query = Customer::realEmailBuyersQuery($country);
        $grid = (new CustomerGrid($query))->allGrid();
        return view('customer.all', [
            'grid' => $grid,
            'country' => $country,
            'title' => 'Real Email Buyers'
        ]); 
    }

    public function positiveReviewer(Request $request) {
        $country = $request->country;
        $query = Customer::positiveReviewerQuery($country);
        $grid = (new CustomerGrid($query))->allGrid();
        return view('customer.all', [
            'grid' => $grid,
            'country' => $country,
            'title' => 'Positive Reviewer'
        ]); 
    }

    public function negativeReviewer(Request $request) {
        $country = $request->country;
        $query = Customer::negativeReviewersQuery($country);
        $grid = (new CustomerGrid($query))->allGrid();
        return view('customer.all', [
            'grid' => $grid,
            'country' => $country,
            'title' => 'Negative Reviewer'
        ]); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {   
        $id = $request->id;
        $customer = null;
        if (preg_match('/^\d+$/', $id)) {
            $customer = Customer::find($id);
        } else {
            $customer = Customer::where('amazon_buyer_email', $id)->first();
            if (!empty($customer)) {
                return redirect(route('customers.show', ['id'=>$customer->id]));
            }
        }
        if (empty($customer)) { return abort(404); }
        $userIdNameHash = User::idNameHash();
        $shipmentQuery = Shipment::where('buyer_email', $customer->amazon_buyer_email);
        $shipments = $shipmentQuery->get();
        $defaultShipment = $shipments->first();
        $orderIds = $shipments->pluck('amazon_order_id')->toArray();
        $orderItemMarketings = OrderItemMarketing::with('claimReviewCampaign')->whereIn('oim_amazon_order_id', $orderIds)->where('claim_review_id', '!=', 0)->get();
        $redeemQuery = Redeem::with(['order'])->whereIn('AmazonOrderId', $orderIds);
        $redeemGrid = RedeemGrid::detailGrid($redeemQuery);
        $shipmentGrid = (new ShipmentGrid($shipmentQuery))->generateDetailGrid();
        return view('customer.show', [
            'customer' => $customer,
            'defaultShipment' => $defaultShipment,
            'shipmentGrid' => $shipmentGrid,
            'orderItemMarketings' => $orderItemMarketings,
            'redeemGrid' => $redeemGrid,
            'userIdNameHash' => $userIdNameHash
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $field = $request->name;
        $value = $request->value;
        $customer->$field = $value;
        $customer->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }

    public function exportForm(Request $request) {
        $sellers = Account::nameIdArray();
        $exportors = CustomerExportor::mostRecent(100);
        return view('customer.export-form', [
            'sellers' => $sellers,
            'exportors' => $exportors
        ]);
    }

    public function exportGenerate(Request $request) {
        
        $params = $request->except(['_token']);
        $cleanAsins = preg_replace("/\s/","",$params['asins']);
        if (!empty($cleanAsins)) { $params['asins'] = explode(",", $cleanAsins); }
        // $filters = [
        //         "asins" => ['B07BJCMK6W', 'B07ZWG4V7N'],
        //         "repeat_buyer" => true,
        //         "positive_reviewer" => true,
        //         "first_order_date" => "2019-12-01"
        // ];
        $exportor = new CustomerExportor($params);
        $exportor->save();
        $exportor->exportFile();
        return redirect()->route('customers.export-form');
    }

    public function exportHistory(Request $request) {

    }
}
