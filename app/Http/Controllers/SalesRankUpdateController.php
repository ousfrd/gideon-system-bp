<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SalesRankUpdate;
use App\Product;

class SalesRankUpdateController extends Controller
{
    //
    // curl -d '{"asin": "B07RYF665N", "sales_ranks": "9227", "date": "2020-04-07", "rating": "293"}' -H "Content-Type: application/json" -X POST  http://homestead.test/product/update-sales-rank
    // curl -d '{"asin": "B07RYF665N", "sales_ranks": "1027", "date": "2020-02-06", "rating": "153"}' -H "Content-Type: application/json" -X POST  http://homestead.test/product/update-sales-rank
    function updateSalesRank(Request $request) {
        $asin = $request->input('asin');
        $sales_ranks = $request->input('sales_ranks');
        $date = $request->input('date');
        $rating = $request->input('rating');
        $sales_rank = SalesRankUpdate::where(['asin' => $asin, 'date' => $date]) -> get();
        if ($sales_rank->isEmpty()) {
            SalesRankUpdate::insertSalesRank($asin, $sales_ranks, $date, $rating);
        } else {
            SalesRankUpdate::updateSalesRank($asin, $sales_ranks, $date, $rating);
        }
        
        return response("saved", 200);
    }

    function storeSalesRank(Request $request) {
        $asin = $request->input('asin');
        $country = $request->input('country');
        $reviews = $request->input('reviews');
        $rating = $request->input('rating');
        
    }

}
