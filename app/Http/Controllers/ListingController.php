<?php 
namespace App\Http\Controllers;
use App\User;
use App\Product;
use App\Listing;
use App\Http\Controllers\Controller;
use DB;
use Grids;
use Html;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Grids\ListingGrid;
use App\OrderItem;
use App\ListingExpense;



class ListingController extends Controller
{
	
	public function __construct()
	{
		$this->middleware('auth');
		parent::__construct();
	}
	
	public function indexAction(Request $request){
		
		$grid = ListingGrid::grid($request)->render();
		
		$title = "Manage Listings";
		
		$managers=  User::productManagerList('id-list');
		
		return view('listing.index',['grid'=>$grid,'title'=>$title,'managerSources'=>$managers]);
	}
	
	public function lowInventoryAction(Request $request){
		
		$grid = ListingGrid::grid($request,['low'=>1])->render();
		
		$title = "Low Inventory Listings";
		
		$managers=  User::productManagerList('id-list');
		
		return view('listing.index',['grid'=>$grid,'title'=>$title,'managerSources'=>$managers]);
	}
	
	public function lowSalesAction(Request $request){
		
		$grid = ListingGrid::grid($request,['low'=>1])->render();
		
		$title = "Low Sales Listings";
		
		$managers=  User::productManagerList('id-list');
		
		return view('listing.index',['grid'=>$grid,'title'=>$title,'managerSources'=>$managers]);
	}
	
	public function excessInventoryAction() {
		$grid = ListingGrid::excessInventoryGrid()->render();
		
		$managers=  User::productManagerList('id-list');
		
		return view('listing.index',['grid'=>$grid,'title'=>'Excess Inventory','managerSources'=>$managers]);
	}
	
	
	
	
	public function viewAction(Request $request,$id,$country=null){
		$version = $request->get('version', 'v2');
		$box_route = $version == 'v1' ? route('order.call_backs.periodicGrossSales') : route('order.call_backs.periodicGrossSales.v2');
		
		$query = Listing::with(['inventory','product','inbound_shipments','health_report','expenses'])->where(function($q) use ($id){
			$q->where('id',$id)->orWhere('sku',$id);
		});
		
		if ($country != null) {
			$query->where('country',$country);
		}
		
		$listing = $query->firstOrFail();
		
		$start_date = $request->get('start_date', date('Y-m-d'));
		$end_date = $request->get('end_date', date('Y-m-d'));
		$date_range= $request->get('date_range','Today');
		$params = ['listing'=>$listing->id,'asin'=>$listing->asin,'sku'=>$listing->sku,'country'=>$listing->country];
				
		$start_date_yesterday = date('Y-m-d',strtotime ("-1 day",strtotime ($start_date)));
		$end_date_yesterday = date('Y-m-d',strtotime("-1 day",strtotime ($end_date) ));

		$start_date_last_week = date('Y-m-d',strtotime ("-1 week",strtotime ($start_date)));
		$end_date_last_week = date('Y-m-d',strtotime("-1 week",strtotime ($end_date) ));

		$start_date_last_month = date('Y-m-d',strtotime ("-1 month",strtotime ($start_date)));
		$end_date_last_month = date('Y-m-d',strtotime("-1 month",strtotime ($end_date) ));

		$start_date_last_year = date('Y-m-d',strtotime ("-1 year",strtotime ($start_date)));
		$end_date_last_year = date('Y-m-d',strtotime ("-1 year",strtotime ($end_date)));

		
		$orderReport = $listing->orderReports($start_date, $end_date);

		$orderReportYesterday = $orderReportLastWeek = $orderReportLastMonth = $orderReportLastYear = null;
		if($start_date == $end_date) {
			$orderReportYesterday = $listing->orderReports($start_date_yesterday, $end_date_yesterday);
		}
		$orderReportLastWeek = $listing->orderReports($start_date_last_week, $end_date_last_week);
		$orderReportLastMonth = $listing->orderReports($start_date_last_month, $end_date_last_month);
		$orderReportLastYear = $listing->orderReports($start_date_last_year, $end_date_last_year);

		
		
		
		$inventoryHistory = $listing->inventoryHistory($start_date, $end_date);
		
		$managers = $listing->product->users()->select('name')->pluck('name')->toArray();
		
		$managerSources = User::productManagerList('id-list');

		return view('listing.view',['listing'=>$listing,'start_date'=>$start_date,'end_date'=>$end_date,'date_range'=>$date_range,
				'start_date_yesterday'=>$start_date_yesterday,'end_date_yesterday'=>$end_date_yesterday,
				'start_date_last_week'=>$start_date_last_week,'end_date_last_week'=>$end_date_last_week,
				'start_date_last_month'=>$start_date_last_month,'end_date_last_month'=>$end_date_last_month,
				'start_date_last_year'=>$start_date_last_year,'end_date_last_year'=>$end_date_last_year,
// 				'counts'=>$orderReport['counts'],'totals'=>$orderReport['totals'],
// 				'total_gross'=>$orderReport['total_gross'],'total_orders'=>$orderReport['total_orders'],
				'orders'=>$orderReport,
				'orderReportYesterday'=>$orderReportYesterday,
				'orderReportLastMonth'=>$orderReportLastMonth,'orderReportLastWeek'=>$orderReportLastWeek,
				'orderReportLastYear'=>$orderReportLastYear,
				'inventoryHistory'=>$inventoryHistory,'params'=>$params,'managers'=>$managers,'managerSources'=>$managerSources,
				'box_route'=>$box_route
			]);
	}
	
	public function updateInsertCardStatus(Request $request) {
        try {
            Listing::updateInsertCardStatus($request->pk, $request->value);
            return response('successful', 200);
        } catch (\Exception $e) {
            print $e->getMessage();
        }
	}
	
	
	function addExpenseAction(Request $request,$id=null) {
		if($data = $request->get('data')) {
			if(empty($id)) {
				$id = $data['id'];
			}
			
			$expense = new ListingExpense();
			$expense->category = $data['category'];
			$expense->amount= $data['amount'];
			$expense->posted_date= $data['date'];
			$expense->by = $data['by'];
			$expense->notes = $data['notes'];
			$expense->created_by = Auth()->user()->name;
			$expense->listing_id = $id;
			echo $expense->save();
			
		}
	}
	
	function editExpenseAction(Request $request,$id) {
		$expense = ListingExpense::where('id',$id)->firstOrFail();
		
		if($data = $request->get('data')) {
			if(empty($id)) {
				$id = $data['id'];
			}
			
			
			$expense->category = $data['category'];
			$expense->amount= $data['amount'];
			$expense->posted_date= $data['date'];
			$expense->by = $data['by'];
			$expense->notes = $data['notes'];
			$expense->created_by = Auth()->user()->name;
			
			echo $expense->save();
			return;
		}
		
		return view('listing.parts.editexpensemodal',['expense'=>$expense]);
	}
	
	
	function deleteExpenseAction(Request $request,$id) {
		$expense = ListingExpense::where('id',$id)->firstOrFail();
		
		$expense->delete();
		return redirect()->back();
		
		return view('listing.parts.editexpensemodal',['expense'=>$expense]);
	}
	
	
	function ajaxsaveAction(Request $request) {
		$value = $request->get('value');
		list($type,$id) = explode('-', $request->get('pk'));
		// 		var_dump($type,$id);
		try{
			Listing::where('id', $id)->update([$type => $value]);
			
			if($isUnitUpdateOrders = $request->get('isUnitUpdateOrders')) {
				$listing = Listing::find($id);
				
				if($type == 'cost') {
					OrderItem::where('ASIN',$listing->asin)->where('country',$listing->country)->update(['cost'=>$value]);
				}
				
				if($type == 'shipping_cost') {
					OrderItem::where('ASIN',$listing->asin)->where('country',$listing->country)->update(['shipping_cost'=>$value]);
				}
			}
			
			echo 1;
		}catch (\Exception $e) {
			print $e->getMessage();
		}
		
		
		
		
	}

	function updateField(Request $request) {
		$id = $request->id;
		$listing = Listing::find($id);
		$field = $request->input('name');
		$value = $request->input('value');
		if ( $field === "ai_ads_commission_rate" && !$listing->ai_ads_open) {
			return response('please open AI ads first!', 406);
		} else {
			$listing->$field = $value;
			$listing->save();
			return response('OK', 200);
		}
	}
	
}