<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use App\User;
use App\Grids\ProductGrid;

class InventoryManagementController extends Controller
{
    /**
	 * authencated user only
	 */
	public function __construct()
	{
		parent::__construct();
		$this->middleware('auth');
	}
    
    public function summary(Request $request)
	{
		$this->authorize('manage-products');
		
		//page title 
		$title = "FBA Inventory";
		 
		//product grid
    $grid = ProductGrid::inventorySummaryGrid()->render();
        
		//product manager list, used for dropdown list when editing product mananger
		$managers=  User::productManagerList('id-list');
		
		return view('inventory-management.summary', ['grid' => $grid, "title"=>$title, 'managerSources'=>$managers]);
	}
}
