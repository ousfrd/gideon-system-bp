<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Grids\ProductCategoryGrid;
use App\ProductCategory;

class ProductCategoryController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $title = 'Product Category';
        $grid = ProductCategoryGrid::grid()->render();

        return view('product-category.index', ['grid' => $grid, 'title' => $title]);
    }
    public function create(Request $request)
    {
        $title = 'Product Category';
        return view('product-category.create', ['title' => $title]);
    }

    public function ajaxsaveAction(Request $request) {
        
        $value = $request->get('value');
        list($type,$id) = explode('-', $request->get('pk'));
        // Log::debug(gettype($type));
        try {
            $category = ProductCategory::find($id);
            if ($category) {
                $category->$type = $value;
                $category->save();
            }
            return "{\"msg\":\"success\"}";
        } catch (\Exception $e) {
            print $e->getMessage();
        }
    }

    public function store(Request $request) {
        $category = $request->input('category');
        ProductCategory::saveCategory($category);
        return redirect()->action('ProductCategoryController@index');
    }

    public function destroy(Request $request) {
        $categoryId = $request->id;
        ProductCategory::deleteCategory($categoryId);
    }
}
