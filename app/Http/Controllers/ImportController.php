<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use Validator;
use Illuminate\Support\Facades\Auth;

use App\Order;
use App\OrderItem;
use App\Account;

use App\Import;
use App\Jobs\ImportOrders;
use App\Jobs\ImportOrderShipments;
use App\Jobs\ImportListings;
use App\Jobs\ImportInventory;
use App\Jobs\ImportFinances;
use App\Jobs\ImportAds;
use App\Jobs\ImportCampaigns;
use App\Jobs\ImportSearchTerms;
use App\Jobs\ImportBusiness;

use Carbon\Carbon;
use DB;


use App\Grids\ImportGrid;
use Storage;
class ImportController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->middleware('auth');
	}
	
	function indexAction() {

    	$title = "Imports";

		$grid = ImportGrid::grid()->render();

		return view('import.index', ['grid' => $grid, "title"=>$title]);
	}

	function ordersImport() {
		$accounts = Account::where('status', 'Active')->get();
		return view('import.orders', ['accounts' => $accounts]);
	}
	
	function parseOrdersImport(Request $request) {
		if (!$request->has('seller_id')) {
			return redirect()->back();
		}

		$seller_id = $request->get('seller_id');
		$path = $request->file('orders_file')->store('orders_imports/' . $seller_id, 'dropbox');
		$import = Import::create([
				'type' => 'Orders',
				'filename' => $path,
				'seller_id' => $seller_id,
				'status' => 'processing'
		]);
		ImportOrders::dispatch($import);

		return redirect()->route("imports");
	}
	
	function parseOrderShipmentsImport(Request $request) {
		if (!$request->has('seller_id')) {
			return redirect()->back();
		}
		$seller_id = $request->get('seller_id');

		$path = $request->file('order_shipments_file')->store('order_shipments_imports/' . $seller_id, 'dropbox');
		$import = Import::create([
				'type' => "OrderShipments",
				'filename' => $path,
				'seller_id' => $seller_id,
				'status' => 'processing'
		]);

		ImportOrderShipments::dispatch($import);

		return redirect()->route("imports");
	}


	function listingsImport() {
		$accounts = Account::where('status', 'Active')->get();
		return view('import.listings', ['accounts' => $accounts]);
	}

	function parseListingsImport(Request $request) {
		if (!$request->has('seller_id')) {
			return redirect()->back();
		}
		$seller_id = $request->get('seller_id');
		$country = $request->get('country');

		$path = $request->file('listings_file')->store('listings_imports/' . $seller_id, 'dropbox');
		$import = Import::create([
				'type' => "Listings",
				'filename' => $path,
				'seller_id' => $seller_id,
				'status' => 'processing',
				'country' => $country
		]);

		ImportListings::dispatch($import);

		return redirect()->route("imports");
	}


	function inventoryImport() {
		$accounts = Account::where('status', 'Active')->get();
		return view('import.inventory', ['accounts' => $accounts]);
	}

	function parseInventoryImport(Request $request) {
		if (!$request->has('seller_id')) {
			return redirect()->back();
		}
		$seller_id = $request->get('seller_id');
		$country = $request->get('country');

		$path = $request->file('inventory_file')->store('fba_inventory_imports/' . $seller_id, 'dropbox');
		$import = Import::create([
				'type' => "FBA Inventory",
				'filename' => $path,
				'seller_id' => $seller_id,
				'status' => 'processing',
				'country' => $country
		]);

		ImportInventory::dispatch($import);

		return redirect()->route("imports");
	}


	function financesImport() {
		$accounts = Account::where('status', 'Active')->get();
		return view('import.finances', ['accounts' => $accounts]);
	}

	function parseFinancesImport(Request $request) {
		if (!$request->has('seller_id')) {
			return redirect()->back();
		}
		$seller_id = $request->get('seller_id');
		$country = $request->get('country');

		$path = $request->file('finances_file')->store('finances_imports/' . $seller_id, 'dropbox');
		$import = Import::create([
				'type' => "Finances",
				'filename' => $path,
				'seller_id' => $seller_id,
				'status' => 'processing',
				'country' => $country
		]);

		ImportFinances::dispatch($import);

		return redirect()->route("imports");
	}


	function adsImport() {
		$accounts = Account::where('status', 'Active')->get();
		return view('import.ads', ['accounts' => $accounts]);
	}

	function parseAdsImport(Request $request) {
		if (!$request->has('seller_id')) {
			return redirect()->back();
		}
		$seller_id = $request->get('seller_id');
		$country = $request->get('country');

		$path = $request->file('ads_file')->store('ads_imports/' . $seller_id, 'dropbox');
		$import = Import::create([
				'type' => "Ads",
				'filename' => $path,
				'seller_id' => $seller_id,
				'status' => 'processing',
				'country' => $country
		]);

		ImportAds::dispatch($import);

		return redirect()->route("imports");
	}

	function campaignsImport(){
		$accounts = Account::where('status', 'Active')->get();
		return view('import.campaigns', ['accounts' => $accounts]);
	}

	function parseCampaignsImport(Request $request) {
		if (!$request->has('seller_id')) {
			return redirect()->back();
		}

		$seller_id = $request->get('seller_id');
		$country = $request->get('country');
		$report_date = $request->get('report_date');

		$path = $request->file('campaigns_file')->store('campaigns_imports/' . $seller_id, 'dropbox');
		$import = Import::create([
				'type' => "CampaignsBulkReport",
				'filename' => $path,
				'seller_id' => $seller_id,
				'status' => 'processing',
				'country' => $country,
				'report_date' => $report_date
		]);

		ImportCampaigns::dispatch($import);
		
		return redirect()->route("imports");
	}


	function parseSearchTermsImport(Request $request) {
		if (!$request->has('seller_id')) {
			return redirect()->back();
		}

		$seller_id = $request->get('seller_id');
		$country = $request->get('country');

		$path = $request->file('searchterms_file')->store('searchterms_imports/' . $seller_id, 'dropbox');
		$import = Import::create([
				'type' => "SearchTerms",
				'filename' => $path,
				'seller_id' => $seller_id,
				'status' => 'processing',
				'country' => $country
		]);

		ImportSearchTerms::dispatch($import);
		
		return redirect()->route("imports");
	}


	function businessImport() {
		$accounts = Account::where('status', 'Active')->get();
		return view('import.business', ['accounts' => $accounts]);
	}


	function parseBusinessImport(Request $request) {
		if (!$request->has('seller_id')) {
			return redirect()->back();
		}
		$seller_id = $request->get('seller_id');
		$country = $request->get('country');
		$report_date = $request->get('report_date');

		$path = $request->file('business_file')->store('businesss_imports/' . $seller_id, 'dropbox');
		$import = Import::create([
				'type' => "Business",
				'filename' => $path,
				'seller_id' => $seller_id,
				'status' => 'processing',
				'country' => $country,
				'report_date' => $report_date
		]);

		

		ImportBusiness::dispatch($import);

		return redirect()->route("imports");
	}
	
	function ImportStats(){
		$title = "Import Stats";

		$accounts = Account::getSortedAccounts();
		$accounts = $accounts->map(function ($account) {
		    return (object)['name' => $account->name, 'seller_id' => $account->seller_id, 'code' => $account->code];
		});
		$marketplaces = config( 'app.marketplaces' );

		return view('import.daily_stats', compact('title', 'accounts', 'marketplaces'));
	}

	function fetchImportStatsRowsAction(Request $request) {

		$result['columnDefs'][] = ['headerName'=> 'Account', 'field'=> 'account', 'pinned' => 'left'];

		$data = array();
		$accounts = DB::table('seller_accounts')->orderBy('status')->orderBy('code')->get();
		

		$endDay   = Carbon::parse($request->input('endDate'))->endOfDay();
		$startDay =  Carbon::parse($request->input('startDate'))->startOfDay(); //2016-09-29 23:59:59.000000

		$fields = array('Orders','OrderShipments','FBA Inventory','Listings','Finances','Ads');
		
		foreach($accounts AS $account){
			$data = array();
			$data["account"] = $account->code." (".$account->name.")"." (".$account->status.")";
			$date = $endDay->copy();

			while( $startDay->format('Ymd') <= $date->format('Ymd') ){

				//foreach($fields as $field){

					$query  = Import::where('seller_id', $account->seller_id);
					$query->where('status','imported');
					//$query->whereBetween('updated_at', [$date->startOfDay()->format('Y-m-d H:i:s'), $date->endOfDay()->format('Y-m-d H:i:s')]);

					$query->whereRaw("ADDTIME(updated_at,3) >= '".$date->startOfDay()->format('Y-m-d H:i:s')."'");
					$query->whereRaw("ADDTIME(updated_at,3) <= '".$date->endOfDay()->format('Y-m-d H:i:s')."'");

					$query->orderby('updated_at','desc');
					$query_result = $query->get();
					$orders_time = array();
					$updatedorders_time = array();

					foreach($query_result as $item){

						
						if($item){

							if($item->type == "Orders"){

								$key = $item->type.$date->format('Ymd');
								array_push($orders_time, Carbon::parse($item->updated_at)->format('g:ia'));
								$data[$key] = join(", ",$orders_time);

							} elseif($item->type == "UpdatedOrders") {

								$key = $item->type.$date->format('Ymd');
								array_push($updatedorders_time, Carbon::parse($item->updated_at)->format('g:ia'));
								$data[$key] = join(", ",$updatedorders_time);
								
							} else if(in_array($item->type, $fields)){

								$key = $item->type.$date->format('Ymd');
								$data[$key] = $item->status;

							}
							
						}

					}

				//}

				$date = $date->subDay(1);

			}

			$result['rowData'][] = $data;

		}

		$date = $endDay;

		while( $startDay->format('Ymd') <= $date->format('Ymd') ){

			$result['columnDefs'][] = ['headerName'=>$date->format('m/d'), 
			'children'=>[
				['headerName'=> 'Orders', 'field'=> 'Orders'.$date->format('Ymd'), 'width'=> 100],
				// ['headerName'=> 'Updated Orders', 'field'=> 'UpdatedOrders'.$date->format('Ymd'), 'width'=> 100],
				['headerName'=> 'Order Shipments', 'field'=> 'OrderShipments'.$date->format('Ymd'), 'width'=> 100],
				['headerName'=> 'Finances', 'field'=> 'Finances'.$date->format('Ymd'), 'width'=> 100],
				['headerName'=> 'FBA Inventory', 'field'=> 'FBA Inventory'.$date->format('Ymd'), 'width'=> 100],
				['headerName'=> 'Listings', 'field'=> 'Listings'.$date->format('Ymd'), 'width'=> 100],
				['headerName'=> 'Ads', 'field'=> 'Ads'.$date->format('Ymd'), 'width'=> 100],
				// ['headerName'=> 'Business', 'field'=> 'Business'.$date->format('Ymd'), 'width'=> 100],
				// ['headerName'=> 'CampaignsBulkReport', 'field'=> 'CampaignsBulkReport'.$date->format('Ymd'), 'width'=> 100],
				// ['headerName'=> 'Status', 'field'=> 'Status'.$date->format('Ymd'), 'width'=> 100]
			]];	

			$date = $date->subDay(1);


		}
	//	print($result);
		return response()->json($result);

	}

}
