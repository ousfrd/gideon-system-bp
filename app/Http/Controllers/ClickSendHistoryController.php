<?php

namespace App\Http\Controllers;

use App\ClickSendHistory;
use Illuminate\Http\Request;
use App\Grids\ClickSendHistoryGrid;

class ClickSendHistoryController extends Controller
{

    public function __construct()
	{
		parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grid = (new ClickSendHistoryGrid())->generateGrid();
        return view('clicksend-history.index', ['grid' => $grid]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClickSendHistory  $clickSendHistory
     * @return \Illuminate\Http\Response
     */
    public function show(ClickSendHistory $clickSendHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClickSendHistory  $clickSendHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(ClickSendHistory $clickSendHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClickSendHistory  $clickSendHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClickSendHistory $clickSendHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClickSendHistory  $clickSendHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClickSendHistory $clickSendHistory)
    {
        //
    }
}
