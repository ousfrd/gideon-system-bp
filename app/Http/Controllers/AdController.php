<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Html;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Account;
use AmazonAdvertisingApi\Client;
use App\Helpers\CurrencyHelper;

use App\User;
use App\AdReport;
use App\AdCampaign;
use App\AdCampaignsReport;
use App\AdRule;
use App\AdGroup;
use App\AdKeyword;
use App\AdSearchtermsReport;

use App\Grids\AdCampaignGrid;
use App\Grids\AdGroupGrid;
use App\Grids\AdKeywordGrid;
use App\Grids\AdSearchTermGrid;
use App\Grids\AdProductGrid;
use App\Grids\NegativeAdKeywordGrid;
use App\Grids\AdRuleGrid;
use Grids;

class AdController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->middleware('auth');
	}
	
	function listAction(Request $request,$schedule_id=null) {
		$grid = AdGrid::grid();
		
		return view('ad.list',['grid'=>$grid]);
	}
	

	public function indexAction(Request $request) {

		$title = "Ads Management";

		$start_date = $request->get('start_date',date('Y-m-d', strtotime( '-1 days' )));
		$end_date = $request->get('end_date',date('Y-m-d', strtotime( '-1 days' )));
		$date_range = $request->get('date_range','Yesterday');
		
		$account_id = $request->get('account','');
		$marketplace = $request->get('marketplace','all');

		$country = $marketplace == 'all' ? '' : array_search($marketplace, config('app.marketplaces'));

		if($account_id) {
			$account = Account::where('seller_id',$account_id)->firstOrFail();
			$account_code = $account->code;
			$marketplaces = config( 'app.marketplaceRegions' )[$account->region];
		} else {
			$account= 'All Accounts';
			$account_code = '';
			$marketplaces = config( 'app.marketplaces' );
		}
		$accounts = Account::all();

		$params = ['seller_id' => $account_id, 'country' => $country, 'start_date'=>$start_date, 'end_date'=>$end_date];

		// $campaigns = AdCampaign::get_data($params);
		// $adgroups = AdGroup::get_data($params);
		// $keywords = AdKeyword::get_data($params);

		$campaigns_grid = AdCampaignGrid::grid($params)->render();
		$adgroups_grid = AdGroupGrid::grid($params)->render();
		$adkeywords_grid = AdKeywordGrid::grid($params)->render();
		$adsearchterms_grid = AdSearchTermGrid::grid($params)->render();
		$adproducts_grid = AdProductGrid::grid($params)->render();
		$negative_adkeywords_grid = NegativeAdKeywordGrid::grid($params)->render();

		return view('ad.index', compact('title', 'accounts', 'marketplaces', 'account', 'account_code', 'account_id', 
			'marketplace', 'start_date', 'end_date', 'date_range', 'campaigns_grid', 'adgroups_grid', 'adkeywords_grid', 'adsearchterms_grid', 'adproducts_grid', 'negative_adkeywords_grid'));

	}
	

	// public function analyzeAction(Request $request) {

	// 	$title = "Ads Analyze";

	// 	$start_date = $request->get('start_date',date('Y-m-d', strtotime( '-7 days' )));
	// 	$end_date = $request->get('end_date',date('Y-m-d', strtotime( '-1 days' )));
	// 	$date_range = $request->get('date_range','Last 7 Days');
		
	// 	$account_id = $request->get('account','');
	// 	$marketplace = $request->get('marketplace','all');

	// 	$country = $marketplace == 'all' ? '' : array_search($marketplace, config('app.marketplaces'));

	// 	if($account_id) {
	// 		$account = Account::where('seller_id',$account_id)->firstOrFail();
	// 		$account_code = $account->code;
	// 		$marketplaces = config( 'app.marketplaceRegions' )[$account->region];
	// 	} else {
	// 		$account= 'All Accounts';
	// 		$account_code = '';
	// 		$marketplaces = config( 'app.marketplaces' );
	// 	}
	// 	$accounts = Account::all();

	// 	$params = ['seller_id' => $account_id, 'country' => $country, 'start_date'=>$start_date, 'end_date'=>$end_date];

	// 	// $campaigns = AdCampaign::get_data($params);
	// 	// $adgroups = AdGroup::get_data($params);
	// 	// $keywords = AdKeyword::get_data($params);

	// 	$campaigns_grid = AdCampaignGrid::grid($params)->render();
	// 	$adgroups_grid = AdGroupGrid::grid($params)->render();

	// 	$ad_daily_data = AdCampaign::get_daily_data($params);


	// 	return view('ad.analyze', compact('title', 'accounts', 'marketplaces', 'account', 'account_code', 'account_id', 
	// 		'marketplace', 'start_date', 'end_date', 'date_range', 'campaigns_grid', 'adgroups_grid', 'ad_daily_data'));

	// }


	public function analyzeAction(Request $request) {

		$title = "Ads Analyze";

		$start_date = $request->get('start_date',date('Y-m-d', strtotime( '-7 days' )));
		$end_date = $request->get('end_date',date('Y-m-d', strtotime( '-1 days' )));
		$date_range = $request->get('date_range','Last 7 Days');
		
		$account_id = $request->get('account','');
		$marketplace = $request->get('marketplace','all');

		$country = $marketplace == 'all' ? '' : array_search($marketplace, config('app.marketplaces'));

		if($account_id) {
			$account = Account::where('seller_id',$account_id)->firstOrFail();
			$account_code = $account->code;
			$marketplaces = config( 'app.marketplaceRegions' )[$account->region];
		} else {
			$account= 'All Accounts';
			$account_code = '';
			$marketplaces = config( 'app.marketplaces' );
		}
		$accounts = Account::where('status', 'Active')->get()->pluck('name', 'seller_id');

		$params = ['seller_id' => $account_id, 'country' => $country, 'start_date'=>$start_date, 'end_date'=>$end_date];

		return view('ad.analyze_react', compact('title', 'accounts', 'marketplaces', 'account', 'account_code', 'account_id', 
			'marketplace', 'start_date', 'end_date', 'date_range'));

	}


	public function agAnalyzeAction(Request $request) {

		$title = "Ads Analyze";

		$start_date = $request->get('start_date',date('Y-m-d', strtotime( '-7 days' )));
		$end_date = $request->get('end_date',date('Y-m-d', strtotime( '-1 days' )));
		$date_range = $request->get('date_range','Last 7 Days');
		
		$account_id = $request->get('account','');
		$marketplace = $request->get('marketplace','all');

		$country = $marketplace == 'all' ? '' : array_search($marketplace, config('app.marketplaces'));

		if($account_id) {
			$account = Account::where('seller_id',$account_id)->firstOrFail();
			$account_code = $account->code;
			$marketplaces = config( 'app.marketplaceRegions' )[$account->region];
		} else {
			$account= 'All Accounts';
			$account_code = '';
			$marketplaces = config( 'app.marketplaces' );
		}
		$accounts = Account::select('name', 'seller_id', 'code')->where('status', 'Active')->get();
		$accounts = $accounts->map(function ($account) {
		    return (object)['name' => $account->name, 'seller_id' => $account->seller_id, 'code' => $account->code];
		});
		$beginning = AdCampaignsReport::get_beginning();

		$params = ['seller_id' => $account_id, 'country' => $country, 'start_date'=>$start_date, 'end_date'=>$end_date];

		return view('ad.ag_analyze_react', compact('title', 'accounts', 'marketplaces', 'account', 'account_code', 'account_id', 
			'marketplace', 'start_date', 'end_date', 'date_range', 'beginning'));

	}


	public function manageAction(Request $request) {
		$title = "Ads Management  >  Campaigns";

		$campaigns_grid = AdCampaignGrid::grid()->render();

		$accounts = Account::where('status', 'Active')->get()->pluck('name', 'seller_id');
		$account_marketplaces = Account::where('status', 'Active')->get()->pluck('marketplaces', 'seller_id');
		$account = Account::where('status', 'Active')->first();
		$marketplaces = explode(",", $account->marketplaces);

		return view('ad.manage', compact('title', 'campaigns_grid', 'accounts', 'account_marketplaces', 'marketplaces'));

	}


	public function createCampaignAction(Request $request) {

		$title = "Ads Management > Create Campaign";

		if( $data = $request->get("data")){
			$name = $data['name'];
			$targetingType = $data['targetingType'];
			$state = $data['state'];
			$dailyBudget = $data['dailyBudget'];
			$marketplace = $data['marketplace'];

			$campaignData = array(
		        array(
		        	"name" => $name,
		        	"campaignType" => "sponsoredProducts",
		        	"targetingType" => $targetingType,
		        	"state" => $state,
		            "dailyBudget" => $dailyBudget,
		            "startDate" => date("Ymd")
		        )
		    );

		    $account = Account::where("seller_id", $data['seller_id'])->firstOrFail();

			$response = $this->createAdObject($account, $marketplace, 'campaigns', $campaignData);


			if($response['success']) {
				$response = json_decode($response['response'], true, 512, JSON_BIGINT_AS_STRING);

				$campaign = new AdCampaign;
				$campaign->name = $name;
				$campaign->seller_id = $account->seller_id;
				$campaign->country = $account->region;
				$campaign->campaignId = $response[0]['campaignId'];
				$campaign->campaignType = "sponsoredProducts";
				$campaign->targetingType = $targetingType;
				$campaign->state = $state;
				$servingStatusArr = ['enabled'=>'CAMPAIGN_STATUS_ENABLED', 'paused'=>'CAMPAIGN_PAUSED', 'archived'=> 'CAMPAIGN_ARCHIVED'];
				$campaign->servingStatus = $servingStatusArr[$state];
				$campaign->dailyBudget = $dailyBudget;
				$campaign->startDate = date("Y-m-d");
				
				$campaign->save();

				session()->flash('msg', 'Campaign has been created.');
				return redirect()->route("ad.manage");
			} else {
				session()->flash('msg', 'Campaign create failed.');
			}	

			return;
		}

		$accounts = Account::where('status', 'Active')->get()->pluck('name', 'seller_id');
		$account_marketplaces = Account::where('status', 'Active')->get()->pluck('marketplaces', 'seller_id');
		$account = Account::where('status', 'Active')->first();
		$marketplaces = explode(",", $account->marketplaces);

		return view('ad.new_campaign', compact('title', 'accounts', 'account_marketplaces', 'marketplaces'));

	}

	public function viewCampaignAction(Request $request, $id) {

	}

	public function editCampaignAction(Request $request, $id) {
		$title = "Edit Campaign";

		$campaign = AdCampaign::where('campaignId', $id)->firstOrFail();

		if( $data = $request->get("data")){
			$name = $data['name'];
			$state = $data['state'];
			$dailyBudget = $data['dailyBudget'];

			$campaignData = array(
		        array(
		        	"campaignId" => (float) $campaign->campaignId,
		        	"name" => $name,
		        	"state" => $state,
		            "dailyBudget" => $dailyBudget
		        )
		    );

			$response = $this->updateAdObject('campaigns', $campaign, $campaignData);
			
			if($response['success']) {
				$campaign->name = $name;
				$campaign->state = $state;
				$campaign->dailyBudget = $dailyBudget;
				$campaign->save();

				session()->flash('msg', 'Campaign has been updated.');
			} else {
				session()->flash('msg', 'Campaign update failed.');
			}	

			return;
		}

		return view('ad.edit_campaign', compact('title', 'campaign'));

	}


	public function campaignNegativeKeywordsAction(Request $request, $id) {
		
		$campaign = AdCampaign::where('campaignId', $id)->firstOrFail();

		$title = "Campaign Negative Keywords";

		$negative_keywords_grid = NegativeAdKeywordGrid::grid(['campaignId'=>$id])->render();

		if($data = $request->get("data")) {
			$keywords = explode("\n", $data['keywords']);
			$keywordsData = [];
			foreach($keywords as $keywordText) {

				$keywordsData[] = array(
	            "campaignId" => (float) $campaign->campaignId,
	            "keywordText" => $keywordText,
	            "matchType" => $data['matchType'],
	            "state" => $data['state']);

			}

			$response = $this->createAdObject($campaign->account, $campaign->country, 'campaignNegativeKeywords', $keywordsData);

			if($response['success']) {
				$response = json_decode($response['response'], true, 512, JSON_BIGINT_AS_STRING);

				$rows = [];
				foreach ($keywordsData as $key => $data) {
					$keywordId = $response[$key]['keywordId'];

					$data['seller_id'] = $campaign->seller_id;
					$data['country'] = $campaign->country;
					$data['keywordId'] = $keywordId;
					$data['adGroupId'] = '';
					$data['bid'] = 0;
					$data['servingStatus'] = '';
					$rows[] = $data;
				}

				AdKeyword::insert($rows);

				session()->flash('msg', 'Negative Keywords has been created.');

				return redirect()->route("ad.campaigns.negativeKeywords", [$campaign->campaignId]);
			} else {
				session()->flash('msg', 'Negative Keywords create failed.');
			}	

			return;

		}

		return view('ad.campaign_negative_keywords', compact('title', 'campaign', 'negative_keywords_grid'));

	}

	public function deleteCampaignNegativeKeywordAction(Request $request, $campaignId, $keywordId) {

		$keyword = AdKeyword::where('keywordId', $keywordId)->firstOrFail();

		$account = $keyword->account;
		$country = $keyword->country;

		if(empty($account->adv_refresh_token)) {
			session()->flash('msg', 'Account did not register Amazon Advertising API.');
			return;	
		}

		$client = $this->initClient($account);
		$client->doRefreshToken();

		if(!empty($client)) {

			if(empty($account->adv_profile_ids)) {
				$account->updateProfiles($client);
			}
			
			// if not request in url then get first from profiles 
			$profile_ids = json_decode($account->adv_profile_ids);
			$profileId = (string)$account->getProfileByCountry($country)->profileId;

			$client->profileId = $profileId;

			$response = $client->removeCampaignNegativeKeyword($keywordId);
			
			if($response['success']) {
				$keyword->delete();

				session()->flash('msg', 'Negative Keywords has been deleted.');

			} else {
				session()->flash('msg', 'Negative Keywords failed to delete.');
			}

		}	

		return redirect()->route("ad.campaigns.negativeKeywords", [$campaignId]);

	}

	public function deleteCampaignNegativeKeywordsAction(Request $request, $id) {
		$campaign = AdCampaign::where('campaignId', $id)->firstOrFail();

		$negativeKeywords = AdKeyword::where('campaignId', $id)->whereIn('matchType', ['negativeExact', 'negativePhrase'])->get();

		$keywordsData = [];
		if(count($negativeKeywords) > 0) {
			foreach ($negativeKeywords as $key => $negativeKeyword) {
				$keywordsData[] = array(
	            "keywordId" => (float) $negativeKeyword->keywordId,
	            "state" => 'deleted');
			}
		

			$account = $campaign->account;
			$country = $campaign->country;

			if(empty($account->adv_refresh_token)) {
				session()->flash('msg', 'Account did not register Amazon Advertising API.');
				return;	
			}

			$client = $this->initClient($account);
			$client->doRefreshToken();

			if(!empty($client)) {

				if(empty($account->adv_profile_ids)) {
					$account->updateProfiles($client);
				}
				
				// if not request in url then get first from profiles 
				$profile_ids = json_decode($account->adv_profile_ids);
				$profileId = (string)$account->getProfileByCountry($country)->profileId;

				$client->profileId = $profileId;

				$response = $client->updateCampaignNegativeKeywords($keywordsData);
				
				if($response['success']) {
					AdKeyword::where('campaignId', $id)->whereIn('matchType', ['negativeExact', 'negativePhrase'])->delete();

					session()->flash('msg', 'Negative Keywords have been deleted.');

				} else {
					session()->flash('msg', 'Negative Keywords failed to delete.');
				}

			}	
		}

		return redirect()->route("ad.campaigns.negativeKeywords", [$id]);

	}


	public function campaignAdGroupsAction(Request $request, $id) {
		

		$campaign = AdCampaign::where('campaignId', $id)->firstOrFail();

		$title = "Campaign Ad Groups";

		$adgroups_grid = AdGroupGrid::grid(['campaignId'=>$id])->render();

		if($data = $request->get("data")) {

		}

		return view('ad.campaign_adgroups', compact('title', 'campaign', 'adgroups_grid'));

	}


	public function createAdGroupAction(Request $request, $id) {

		$title = "Create AdGroup";

		$campaign = AdCampaign::where('campaignId', $id)->firstOrFail();

		if( $data = $request->get("data")){
			$name = $data['name'];
			$state = $data['state'];
			$defaultBid = $data['defaultBid'];

			$adgroupData = array(
		        array(
		        	"campaignId" => $id,
		        	"name" => $name,
		        	"state" => $state,
		            "defaultBid" => $defaultBid
		        )
		    );

		    $account = $campaign->account;
		    $country = $campaign->country;

			$response = $this->createAdObject($account, $country, 'adgroups', $adgroupData);


			if($response['success']) {
				$response = json_decode($response['response'], true, 512, JSON_BIGINT_AS_STRING);

				$adgroup = new AdGroup;
				$adgroup->name = $name;
				$adgroup->seller_id = $account->seller_id;
				$adgroup->country = $country;
				$adgroup->campaignId = $id;
				$adgroup->adGroupId = $response[0]['adGroupId'];
				$adgroup->state = $state;
				$servingStatusArr = ['enabled'=>'CAMPAIGN_STATUS_ENABLED', 'paused'=>'CAMPAIGN_PAUSED', 'archived'=> 'CAMPAIGN_ARCHIVED'];
				$adgroup->servingStatus = $servingStatusArr[$state];
				$adgroup->defaultBid = $defaultBid;
				
				$adgroup->save();

				session()->flash('msg', 'Campaign has been created.');
				return redirect()->route("ad.manage");
			} else {
				session()->flash('msg', 'Campaign create failed.');
			}	

			return;
		}

		$accounts = Account::where('status', 'Active')->get()->pluck('name', 'seller_id');
		$account_marketplaces = Account::where('status', 'Active')->get()->pluck('marketplaces', 'seller_id');
		$account = Account::where('status', 'Active')->first();
		$marketplaces = explode(",", $account->marketplaces);

		return view('ad.new_adgroup', compact('title', 'accounts', 'account_marketplaces', 'marketplaces'));

	}

	public function viewAdGroupAction(Request $request, $id) {

	}
	
	public function editAdGroupAction(Request $request, $id) {
		$title = "Edit AdGroup";

		$adgroup = AdGroup::where('adGroupId', $id)->firstOrFail();
		$campaign = $adgroup->ad_campaign;

		if( $data = $request->get("data")){
			$name = $data['name'];
			$state = $data['state'];
			$dailyBudget = $data['dailyBudget'];

			$campaignData = array(
		        array(
		        	"campaignId" => (float) $campaign->campaignId,
		        	"name" => $name,
		        	"state" => $state,
		            "dailyBudget" => $dailyBudget
		        )
		    );

			$response = $this->updateAdObject('campaigns', $campaign, $campaignData);
			
			if($response['success']) {
				$campaign->name = $name;
				$campaign->state = $state;
				$campaign->dailyBudget = $dailyBudget;
				$campaign->save();

				session()->flash('msg', 'Campaign has been updated.');
			} else {
				session()->flash('msg', 'Campaign update failed.');
			}	

			return;
		}

		return view('ad.edit_adgroup', compact('title', 'campaign', 'adgroup'));

	}

	public function adGroupKeywordsAction(Request $request) {

	}


	public function fetchRowsAction(Request $request) {

		$account = $request->input('account');
		$marketplace = $request->input('marketplace');
		$startDate = date('Y-m-d', strtotime( $request->input('startDate') ));
		$endDate = date('Y-m-d', strtotime( $request->input('endDate') ));
		$query = $request->input('campaign_query');
		$campaignId = $request->input('campaignId');
		$campaignIds = $request->input('campaignIds');
		$campaignState = $request->input('campaignState');
		$auto_scheduler = $request->input('auto_scheduler');
		$campaignTargetingType = $request->input('campaignTargetingType');
		$adGroupId = $request->input('adGroupId');
		$adId = $request->input('adId');

		$params = ['seller_id' => $account, 'country' => $marketplace, 'start_date'=>$startDate, 'end_date'=>$endDate, 'query'=>$query, 'campaignId'=>$campaignId, 'campaignIds'=>$campaignIds, 'campaignState'=>$campaignState, 'auto_scheduler'=>$auto_scheduler, 'campaignTargetingType'=>$campaignTargetingType, 'adGroupId'=>$adGroupId, 'adId'=>$adId];

		$ad_daily_data = AdCampaign::get_daily_data($params);

		$result = [];
		$date = $startDate;
		while($date <= $endDate) {

			if(array_key_exists($date, $ad_daily_data)) {
				$data = $ad_daily_data[$date];
				$result[] = [ $date, (float)$data['total_sales'], (float)$data['total_cost'], (float)$data['acos'], (float)$data['cpc'], (float)$data['total_impressions'], (float)$data['ctr'], (float)$data['total_clicks'], (float)$data['cr'], (float)$data['total_orders'], isset($data['bid']) ? (float)$data['bid'] : null];
			} else {
				$result[] = [ $date, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
			}

			$date = date('Y-m-d', strtotime($date . ' +1 day'));
		}

		return response()->json($result);

	}


	public function fetchCampaignsAction(Request $request) {

		$account = $request->get('account');
		$marketplace = $request->get('marketplace');
		$startDate = date('Y-m-d', strtotime( $request->get('startDate') ));
		$endDate = date('Y-m-d', strtotime( $request->get('endDate') ));
		$query = $request->get('campaign_query');
		$campaignId = $request->get('campaignId');
		$campaignState = $request->get('campaignState');
		$campaignTargetingType = $request->get('campaignTargetingType');

		$params = ['seller_id' => $account, 'country' => $marketplace, 'start_date'=>$startDate, 'end_date'=>$endDate, 'query'=>$query, 'campaignId'=>$campaignId, 'campaignState'=>$campaignState, 'campaignTargetingType'=>$campaignTargetingType];

		$campaigns = AdCampaign::getCampaigns($params);

		return response()->json($campaigns);

	}

	public function fetchAdGroupsAction(Request $request) {

		$account = $request->get('account');
		$marketplace = $request->get('marketplace');
		$startDate = date('Y-m-d', strtotime( $request->get('startDate') ));
		$endDate = date('Y-m-d', strtotime( $request->get('endDate') ));
		$query = $request->get('campaign_query');
		$campaignId = $request->get('campaignId');
		$campaignState = $request->get('campaignState');
		$campaignTargetingType = $request->get('campaignTargetingType');
		$adGroupState = $request->get('adGroupState');

		$params = ['seller_id' => $account, 'country' => $marketplace, 'start_date'=>$startDate, 'end_date'=>$endDate, 'query'=>$query, 'campaignId'=>$campaignId, 'campaignState'=>$campaignState, 'campaignTargetingType'=>$campaignTargetingType, 'adGroupState'=>$adGroupState];


		$adgroups = AdGroup::getAdgroups($params);

		return response()->json($adgroups);

	}

	public function fetchKeywordsAction(Request $request) {

		$account = $request->get('account');
		$marketplace = $request->get('marketplace');
		$startDate = date('Y-m-d', strtotime( $request->get('startDate') ));
		$endDate = date('Y-m-d', strtotime( $request->get('endDate') ));
		$query = $request->get('campaign_query');
		$campaignId = $request->get('campaignId');
		$campaignState = $request->get('campaignState');
		$campaignTargetingType = $request->get('campaignTargetingType');
		$adGroupId = $request->get('adGroupId');

		$keywordQuery = $request->get('keywordQuery');
		$keywordState = $request->get('keywordState');
		$keywordMatchType = $request->get('keywordMatchType');

		$params = ['seller_id' => $account, 'country' => $marketplace, 'start_date'=>$startDate, 'end_date'=>$endDate, 'query'=>$query, 'campaignId'=>$campaignId, 'campaignState'=>$campaignState, 'campaignTargetingType'=>$campaignTargetingType, 'adGroupId'=>$adGroupId, 
			'keywordQuery' => $keywordQuery, 'keywordState' => $keywordState, 'keywordMatchType' => $keywordMatchType];


		$keywords = AdKeyword::getKeywords($params);

		return response()->json($keywords);

	}

	public function fetchSearchtermsAction(Request $request) {


		$startDate = date('Y-m-d', strtotime( $request->get('startDate') ));
		$endDate = date('Y-m-d', strtotime( $request->get('endDate') ));

		$asin = $request->get('asin');
		$country = $request->get('country');
		$keywordId = $request->get('keywordId');

		$params = ['asin'=>$asin, 'country' => $country, 'start_date'=>$startDate, 'end_date'=>$endDate, 'keywordId'=>$keywordId];


		$searchterms = AdSearchtermsReport::getSearchterms($params);

		return response()->json($searchterms);

	}

	public function fetchProductsAction(Request $request) {

		$account = $request->get('account');
		$marketplace = $request->get('marketplace');
		$startDate = date('Y-m-d', strtotime( $request->get('startDate') ));
		$endDate = date('Y-m-d', strtotime( $request->get('endDate') ));
		$query = $request->get('campaign_query');
		$campaignId = $request->get('campaignId');
		$campaignState = $request->get('campaignState');
		$campaignTargetingType = $request->get('campaignTargetingType');
		$adGroupId = $request->get('adGroupId');
		$keywordId = $request->get('keywordId');

		$params = ['seller_id' => $account, 'country' => $marketplace, 'start_date'=>$startDate, 'end_date'=>$endDate, 'query'=>$query, 'campaignId'=>$campaignId, 'campaignState'=>$campaignState, 'campaignTargetingType'=>$campaignTargetingType, 'adGroupId'=>$adGroupId, 'keywordId'=>$keywordId];


		$products = AdReport::getProducts($params);

		return response()->json($products);

	}

	public function agFetchCampaignsAction(Request $request) {
		$account = $request->input('account');
		$marketplace = $request->input('marketplace');
		$startDate = date('Y-m-d', strtotime( $request->input('startDate') ));
		$endDate = date('Y-m-d', strtotime( $request->input('endDate') ));
		$campaignIds = $request->input('campaignIds');

		$params = ['seller_id' => $account, 'country' => $marketplace, 'start_date'=>$startDate, 'end_date'=>$endDate, 'campaignIds'=>$campaignIds];

		$result['columnDefs'] = [];
		$result['columnDefs'] = [
			['headerName'=> 'State', 'field'=> 'state', 'width'=> 70, 'floatingFilterComponent'=>'campaignStateFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'cellRenderer'=>'campaignStateCellRenderer'],
			['headerName'=> 'Status', 'field'=> 'status', 'width'=> 100, 'floatingFilterComponent'=>'campaignStatusFilter', 'cellRenderer'=>'campaignStatusCellRenderer', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true]],
			['headerName'=> 'Seller Account', 'field'=> 'account', 'width'=> 150, 'floatingFilterComponentParams' => ['suppressFilterButton'=>true]],
			['headerName'=> 'Name', 'field'=> 'name', 'width'=> 250, 'cellRenderer'=>'campaignNameCellRenderer', 'filter' => 'agTextColumnFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true]],			
			['headerName'=> 'Manager', 'field'=> 'product_manager', 'width'=> 80, 'floatingFilterComponentParams' => ['suppressFilterButton'=>true]],
			
			['headerName'=> 'Targeting Type', 'field'=> 'targetingType', 'width'=> 80, 'floatingFilterComponent'=>'campaignTargetingTypeFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true]],
			['headerName'=> 'Auto Scheduler', 'field'=> 'auto_scheduler', 'width'=> 70, 'floatingFilterComponent'=>'autoSchedulerStateFilter', 'cellRenderer'=>'autoSchedulerStateCellRenderer', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true]],			
			['headerName'=> 'Start Date', 'field'=> 'startDate', 'width'=> 100, 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filter' => 'agDateColumnFilter', 'filterParams' => ['inRangeInclusive'=>true]],
			
			['headerName'=> 'Daily Budget', 'field'=> 'dailyBudget', 'type'=> 'numericColumn', 'cellRenderer'=>'campaignBudgetEditableCellRenderer', 'width'=> 90, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Impr.', 'field'=> 'total_impressions', 'type'=> 'numericColumn', 'width'=> 90, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Clicks', 'field'=> 'total_clicks', 'type'=> 'numericColumn', 'width'=> 90, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'CTR', 'field'=> 'ctr', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 90, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Ad Spend', 'field'=> 'total_cost', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 90, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'CPC', 'field'=> 'cpc', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 90, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Sales', 'field'=> 'total_sales', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 90, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'ACoS', 'field'=> 'acos', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 90, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Orders', 'field'=> 'total_orders', 'type'=> 'numericColumn', 'width'=> 90, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'CR', 'field'=> 'cr', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 90, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			
		];


		$result['rowData'] = AdCampaign::getCampaigns($params);
		$result['topRowData'] = AdCampaign::getCampaignsTotal($params);

		return response()->json($result);

	}

	public function agFetchCampaignsTotalAction(Request $request) {
		$account = $request->input('account');
		$marketplace = $request->input('marketplace');
		$startDate = date('Y-m-d', strtotime( $request->input('startDate') ));
		$endDate = date('Y-m-d', strtotime( $request->input('endDate') ));
		$campaignIds = $request->input('campaignIds');

		$params = ['seller_id' => $account, 'country' => $marketplace, 'start_date'=>$startDate, 'end_date'=>$endDate, 'campaignIds'=>$campaignIds];

		$result['topRowData'] = AdCampaign::getCampaignsTotal($params);

		return response()->json($result);
	}

	
	public function agFetchAdGroupsAction(Request $request) {

		$account = $request->input('account');
		$marketplace = $request->input('marketplace');
		$startDate = date('Y-m-d', strtotime( $request->input('startDate') ));
		$endDate = date('Y-m-d', strtotime( $request->input('endDate') ));
		$campaignIds = $request->input('campaignIds');

		$params = ['seller_id' => $account, 'country' => $marketplace, 'start_date'=>$startDate, 'end_date'=>$endDate, 'campaignIds'=>$campaignIds];

		$result['columnDefs'] = [
			['headerName'=> 'State', 'field'=> 'state', 'width'=> 100, 'floatingFilterComponent'=>'campaignStateFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'cellRenderer'=>'adGroupStateCellRenderer', 'headerCheckboxSelection'=>true, 'headerCheckboxSelectionFilteredOnly'=>true, 'checkboxSelection'=>true],
			['headerName'=> 'Name', 'field'=> 'name', 'width'=> 250, 'cellRenderer'=>'adGroupNameCellRenderer', 'filter' => 'agTextColumnFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true]],
			['headerName'=> 'Impr.', 'field'=> 'total_impressions', 'type'=> 'numericColumn', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Clicks', 'field'=> 'total_clicks', 'type'=> 'numericColumn', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'CTR', 'field'=> 'ctr', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Ad Spend', 'field'=> 'total_cost', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'CPC', 'field'=> 'cpc', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Sales', 'field'=> 'total_sales', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'ACoS', 'field'=> 'acos', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Orders', 'field'=> 'total_orders', 'type'=> 'numericColumn', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'CR', 'field'=> 'cr', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Default Bid', 'field'=> 'defaultBid', 'type'=> 'numericColumn', 'cellRenderer'=>'adGroupBidEditableCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
		];


		$result['rowData'] = AdGroup::getAdgroups($params);

		return response()->json($result);

	}

	public function agFetchKeywordsAction(Request $request) {

		$account = $request->input('account');
		$marketplace = $request->input('marketplace');
		$startDate = date('Y-m-d', strtotime( $request->input('startDate') ));
		$endDate = date('Y-m-d', strtotime( $request->input('endDate') ));

		$campaignIds = $request->input('campaignIds');
		$adGroupIds = $request->input('adGroupIds');
		$keywordIds = $request->input('keywordIds');

		$params = ['seller_id' => $account, 'country' => $marketplace, 'start_date'=>$startDate, 'end_date'=>$endDate, 'campaignIds'=>$campaignIds, 'adGroupIds'=>$adGroupIds, 'keywordIds' => $keywordIds];


		$result['columnDefs'] = [
			['headerName'=> 'State', 'field'=> 'state', 'width'=> 100, 'floatingFilterComponent'=>'campaignStateFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'cellRenderer'=>'keywordStateCellRenderer', 'headerCheckboxSelection'=>true, 'headerCheckboxSelectionFilteredOnly'=>true, 'checkboxSelection'=>true],
			['headerName'=> 'Keyword', 'field'=> 'keywordText', 'width'=> 250, 'cellRenderer'=>'keywordTextCellRenderer', 'filter' => 'agTextColumnFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true]],
			['headerName'=> 'Match Type', 'field'=> 'matchType', 'width'=> 250, 'filter' => 'agTextColumnFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true]],
			['headerName'=> 'Impr.', 'field'=> 'total_impressions', 'type'=> 'numericColumn', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Clicks', 'field'=> 'total_clicks', 'type'=> 'numericColumn', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'CTR', 'field'=> 'ctr', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Ad Spend', 'field'=> 'total_cost', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'CPC', 'field'=> 'cpc', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Sales', 'field'=> 'total_sales', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'ACoS', 'field'=> 'acos', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Orders', 'field'=> 'total_orders', 'type'=> 'numericColumn', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'CR', 'field'=> 'cr', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Bid', 'field'=> 'bid', 'type'=> 'numericColumn', 'cellRenderer'=>'keywordBidEditableCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
		];

		$result['rowData'] = AdKeyword::getKeywords($params);

		return response()->json($result);

	}


	public function agFetchProductsAction(Request $request) {

		$account = $request->input('account');
		$marketplace = $request->input('marketplace');
		$startDate = date('Y-m-d', strtotime( $request->input('startDate') ));
		$endDate = date('Y-m-d', strtotime( $request->input('endDate') ));

		$campaignIds = $request->input('campaignIds');
		$adGroupIds = $request->input('adGroupIds');
		$keywordIds = $request->input('keywordIds');

		$params = ['seller_id' => $account, 'country' => $marketplace, 'start_date'=>$startDate, 'end_date'=>$endDate, 'campaignIds'=>$campaignIds, 'adGroupIds'=>$adGroupIds, 'keywordIds'=>$keywordIds];


		$result['columnDefs'] = [
			['headerName'=> 'Image', 'field'=> 'small_image', 'cellRenderer'=>'imageCellRenderer', 'width'=> 80, 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'suppressFilter'=> true],
			['headerName'=> 'ASIN', 'field'=> 'asin', 'cellRenderer'=>'asinCellRenderer', 'width'=> 100, 'filter' => 'agTextColumnFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true]],
			['headerName'=> 'Name', 'field'=> 'name', 'width'=> 250, 'filter' => 'agTextColumnFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true]],
			['headerName'=> 'Product Manager', 'field'=> 'product_manager', 'width'=> 100, 'filter' => 'agTextColumnFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true]],
			['headerName'=> 'View Searchterms', 'field'=> 'view_searchterms', 'width'=> 130, 'filter' => 'agTextColumnFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'suppressFilter'=> true, 'cellRenderer'=>'viewSearchtermsCellRenderer'],
			['headerName'=> 'Profit', 'field'=> 'profit', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Impr.', 'field'=> 'total_impressions', 'type'=> 'numericColumn', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Clicks', 'field'=> 'total_clicks', 'type'=> 'numericColumn', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'CTR', 'field'=> 'ctr', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Ad Spend', 'field'=> 'total_cost', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'CPC', 'field'=> 'cpc', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Sales', 'field'=> 'total_sales', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'ACoS', 'field'=> 'acos', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Orders', 'field'=> 'total_orders', 'type'=> 'numericColumn', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'CR', 'field'=> 'cr', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]]
		];

		$result['rowData'] = AdReport::getProducts($params);

		return response()->json($result);

	}


	public function agFetchSearchtermsAction(Request $request) {

		$startDate = date('Y-m-d', strtotime( $request->input('startDate') ));
		$endDate = date('Y-m-d', strtotime( $request->input('endDate') ));

		$campaignIds = $request->input('campaignIds');
		$adGroupIds = $request->input('adGroupIds');
		$keywordIds = $request->input('keywordIds');
		$asin_countries = $request->input('asin_countries');

		$params = ['start_date'=>$startDate, 'end_date'=>$endDate, 'campaignIds'=>$campaignIds, 'adGroupIds'=>$adGroupIds, 'keywordIds'=>$keywordIds, 'asin_countries'=>$asin_countries];

		$result['columnDefs'] = [
			['headerName'=> 'Searchterms', 'field'=> 'query', 'width'=> 250, 'cellRenderer'=>'searchtermCellRenderer', 'filter' => 'agTextColumnFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true]],
			['headerName'=> 'Impr.', 'field'=> 'total_impressions', 'type'=> 'numericColumn', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Clicks', 'field'=> 'total_clicks', 'type'=> 'numericColumn', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'CTR', 'field'=> 'ctr', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Ad Spend', 'field'=> 'total_cost', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'CPC', 'field'=> 'cpc', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Sales', 'field'=> 'total_sales', 'type'=> 'numericColumn', 'cellRenderer'=>'priceCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'ACoS', 'field'=> 'acos', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'Orders', 'field'=> 'total_orders', 'type'=> 'numericColumn', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]],
			['headerName'=> 'CR', 'field'=> 'cr', 'type'=> 'numericColumn', 'cellRenderer'=>'percentageCellRenderer', 'width'=> 120, 'filter' => 'agNumberColumnFilter', 'floatingFilterComponent' => 'rangeFloatingFilter', 'floatingFilterComponentParams' => ['suppressFilterButton'=>true], 'filterParams' => ['inRangeInclusive'=>true]]
		];


		$result['rowData'] = AdSearchtermsReport::getSearchterms($params);

		return response()->json($result);

	}


	public function updateCampaignStateAction(Request $request) {
		
		$campaignId = $request->get('campaignId');
		$campaign = AdCampaign::where('campaignId', $campaignId)->first();
		$new_state = $campaign->state == 'enabled' ? 'paused' : 'enabled';
		$campaignData = array(
			        array(
			        	"campaignId" => (float) $campaignId,
			            "state" => $new_state
			        )
			    );

		$response = $this->updateAdObject('campaigns', $campaign, $campaignData);

		if($response['success']) {
			$campaign->state = $new_state;
			$campaign->save();
		}	

		return $response;

	}

	

	public function updateCampaignDailyBudgetAction(Request $request) {
		
		$campaignId = $request->get('campaignId');
		$campaign = AdCampaign::where('campaignId', $campaignId)->first();
		$new_budget = $request->get('budget');
		$campaignData = array(
			        array(
			        	"campaignId" => (float) $campaignId,
			            "dailyBudget" => $new_budget
			        )
			    );

		$response = $this->updateAdObject('campaigns', $campaign, $campaignData);

		if($response['success']) {
			$campaign->dailyBudget = $new_budget;
			$campaign->save();
		}	

		return $response;

	}

	public function updateAdGroupStateAction(Request $request) {
		
		$adGroupId = $request->get('adGroupId');
		$adGroup = AdGroup::where('adGroupId', $adGroupId)->first();
		$new_state = $adGroup->state == 'enabled' ? 'paused' : 'enabled';
		$adGroupData = array(
			        array(
			        	"adGroupId" => (float) $adGroupId,
			            "state" => $new_state
			        )
			    );

		$response = $this->updateAdObject('adgroups', $adGroup, $adGroupData);

		if($response['success']) {
			$adGroup->state = $new_state;
			$adGroup->save();
		}	

		return $response;

	}

	public function updateAdGroupDefaultBidAction(Request $request) {
		
		$adGroupId = $request->get('adGroupId');
		$adGroup = AdGroup::where('adGroupId', $adGroupId)->first();
		$new_bid = $request->get('bid');
		$adGroupData = array(
			        array(
			        	"adGroupId" => (float) $adGroupId,
			            "defaultBid" => $new_bid
			        )
			    );

		$response = $this->updateAdObject('adgroups', $adGroup, $adGroupData);

		if($response['success']) {
			$adGroup->defaultBid = $new_bid;
			$adGroup->save();
		}	

		return $response;

	}

	public function updateAdGroupsDefaultBidAction(Request $request) {
		
		$adGroupIds = $request->get('adGroupIds');
		$new_bid = $request->get('bid');
		$adGroupData = [];
		foreach ($adGroupIds as $adGroupId) {
			$adGroupData[] = array(
		        	"adGroupId" => (float) $adGroupId,
		            "defaultBid" => $new_bid
		        );
		}

		$adGroup = AdGroup::where('adGroupId', $adGroupIds[0])->first();
		$response = $this->updateAdObject('adgroups', $adGroup, $adGroupData);

		if($response['success']) {
			AdGroup::whereIn('adGroupId', $adGroupIds)->update(['defaultBid'=>$new_bid]);
		}	

		return $response;
	}



	public function updateKeywordBidAction(Request $request) {
		
		$keywordId = $request->get('keywordId');
		$keyword = AdKeyword::where('keywordId', $keywordId)->first();
		$new_bid = $request->get('bid');
		$keywordData = array(
			        array(
			        	"keywordId" => (float) $keywordId,
			            "bid" => $new_bid
			        )
			    );

		$response = $this->updateAdObject('keywords', $keyword, $keywordData);

		if($response['success']) {
			$keyword->bid = $new_bid;
			$keyword->save();
		}	

		return $response;

	}


	public function updateKeywordStateAction(Request $request) {
		
		$keywordId = $request->get('keywordId');
		$keyword = AdKeyword::where('keywordId', $keywordId)->first();
		$new_state = $keyword->state == 'enabled' ? 'paused' : 'enabled';
		$keywordData = array(
			        array(
			        	"keywordId" => (float) $keywordId,
			            "state" => $new_state
			        )
			    );

		$response = $this->updateAdObject('keywords', $keyword, $keywordData);

		if($response['success']) {
			$keyword->state = $new_state;
			$keyword->save();
		}	

		return $response;

	}

	public function updateKeywordGroupBidAction(Request $request) {
		$keywordIds = $request->get('keywordIds');
		$bid = $request->get('bid');

		$keywordData = [];
		foreach ($keywordIds as $keywordId) {
			$keywordData[] = array(
		        	"keywordId" => (float) $keywordId,
		            "bid" => $bid
		        );
		}

		$keyword = AdKeyword::where('keywordId', $keywordIds[0])->first();
		$response = $this->updateAdObject('keywords', $keyword, $keywordData);

		if($response['success']) {
			AdKeyword::whereIn('keywordId', $keywordIds)->update(['bid'=>$bid]);
		}	

		return $response;
	}


	function updateAdObject($adType, $adObject, $adData) {

		$account = $adObject->account;

		if(empty($account->adv_refresh_token)) {
			session()->flash('msg', 'Account did not register Amazon Advertising API.');
			return;	
		}

		$client = $this->initClient($account);
		$client->doRefreshToken();

		if(!empty($client)) {

			if(empty($account->adv_profile_ids)) {
				$account->updateProfiles($client);
			}
			
			// if not request in url then get first from profiles 
			$profile_ids = json_decode($account->adv_profile_ids);
			$country = $adObject->country ? $adObject->country : $profile_ids[0]->countryCode;

			$profileId = (string)$account->getProfileByCountry($country)->profileId;

			$client->profileId = $profileId;

			switch($adType) {
				case 'campaigns':
					$response = $client->updateCampaigns($adData);
					break;
				case 'adgroups':
					$response = $client->updateAdGroups($adData);
					break;
				case 'keywords':
					$response = $client->updateBiddableKeywords($adData);
					break;

			}
			

			return $response;
		}
	}


	public function createAdObject($account, $country, $adType, $adData) {

		if(empty($account->adv_refresh_token)) {
			session()->flash('msg', 'Account did not register Amazon Advertising API.');
			return;	
		}

		$client = $this->initClient($account);
		$client->doRefreshToken();

		if(!empty($client)) {

			if(empty($account->adv_profile_ids)) {
				$account->updateProfiles($client);
			}
			
			// if not request in url then get first from profiles 
			$profile_ids = json_decode($account->adv_profile_ids);
			$profileId = (string)$account->getProfileByCountry($country)->profileId;

			$client->profileId = $profileId;

			switch($adType) {
				case 'campaigns':
					$response = $client->createCampaigns($adData);
					break;
				case 'adgroups':
					$response = $client->createAdGroups($adData);
					break;
				case 'keywords':
					$response = $client->createBiddableKeywords($adData);
					break;
				case 'negativeKeywords':
					$response = $client->createNegativeKeywords($adData);
					break;
				case 'campaignNegativeKeywords':
					$response = $client->createCampaignNegativeKeywords($adData);
					break;

			}
			

			return $response;
		}		
	}




	/**
	 * Register sandbox amazon advertising api
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function sandboxAction(Request $request, $aid)
	{
		 
		// $this->authorize('manage-advertising');
		$account = Account::where( 'id', $aid)->firstOrFail();

		if($refreshToken = $account->adv_refresh_token) {
			$config = array(
			    "clientId" => config('app.lwa_client_id'),
			    "clientSecret" => config('app.lwa_client_secret'),
			    "refreshToken" => $refreshToken,
			    "region" => "na",
			    "sandbox" => true,
		  	);

			$client = new Client($config);

			$client->registerProfile(array("countryCode" => "US"));		

			$request = $client->doRefreshToken();

			$res = $client->listCampaigns(array("stateFilter" => "enabled"));

			var_dump($res);

		}

	}
	

	public function listCampaignsAction(Request $request, $aid) {

		$account = Account::where( 'id', $aid)->firstOrFail();
		$title = "Manage Campaigns for " . $account->name;
		$campaigns = [];

		if(empty($account->adv_refresh_token)) {
			session()->flash('msg', 'Account did not register Amazon Advertising API.');
			return redirect()->route("accounts");	
		}

		$client = $this->initClient($account);
		$client->doRefreshToken();

		if(!empty($client)) {

			if(empty($account->adv_profile_ids)) {
				$account->updateProfiles($client);
			}
			
			// if not request in url then get first from profiles 
			$profile_ids = json_decode($account->adv_profile_ids);
			$country = (null !==$request->get('country')) ? $request->get('country') : $profile_ids[0]->countryCode;

			$profileId = (string)$account->getProfileByCountry($country)->profileId;

			$client->profileId = $profileId;

			$response = $client->listCampaigns(array("stateFilter" => "enabled"));

			if($response['success']) {
				$campaigns =json_decode($response['response'], false, 512, JSON_BIGINT_AS_STRING);
			}

		}


		return view('ad_campaigns.index', compact('title', 'campaigns'));
	}



	public function requestReportAction(Request $request, $aid) {

		$account = Account::where( 'id', $aid)->firstOrFail();

		$yesterday = date('Ymd',strtotime("-1 days"));
		$account->adRequestReport($yesterday, 'productAds');

	}

	// public function getReportAction(Request $request, $aid) {
	// 	$account = Account::where( 'id', $aid)->firstOrFail();

	// 	$reportId = "amzn1.clicksAPI.v1.p1.5B04101E.43425a5d-cd04-4a1d-b8ac-d480114d0745";
	// 	$country = "US";

	// 	$profile = $account->getProfileByCountry($country);

	// 	$client = $this->initClient($account);
	// 	$client->profileId = $profile->profileId;
	// 	$currency = $profile->currencyCode;

	// 	$res = $client->getReport($reportId);

	// 	dd($res);


	// }

	public function getReportAction(Request $request, $id) {

		$account = Account::where( 'id', $id)->firstOrFail();

		$yesterday = date('Y-m-d',strtotime("-1 days"));
		$account->getAdReportByDate($yesterday, 'searchterms');
		
	}

	
	public function getSnapshotAction(Request $request, $id, $recordType) {

		$account = Account::where( 'id', $id)->firstOrFail();

		$account->getSnapshot($recordType);

	}


	public function initClient($account) {

		$refreshToken = $account->adv_refresh_token;
		if($account->region == "US") {
			$region = "na";
		} elseif ($account->region == "UK") {
			$region = "eu";
		} else {
			return;
		}

		$config = array(
		    "clientId" => config('app.lwa_client_id'),
		    "clientSecret" => config('app.lwa_client_secret'),
		    "refreshToken" => $refreshToken,
		    "region" => $region,
		    "sandbox" => false,
	  	);

		$client = new Client($config);

		return $client;
	}


	public function rulesAction(){// rule index

		$title = "Ads Management  >  Rules";

		//$rules_grid = AdRuleGrid::grid()->render();

		$accounts = Account::where('status', 'Active')->get()->pluck('name', 'seller_id');
		$account_marketplaces = Account::where('status', 'Active')->get()->pluck('marketplaces', 'seller_id');
		$account = Account::where('status', 'Active')->first();
		$marketplaces = explode(",", $account->marketplaces);


		return view('ad.rules', compact('title'));

	}

	public function createRuleAction(Request $request){//create a rule

		

		if($request->get('_token')){

			$rule = new AdRule;
			$inputs = $request->all();

			$conditions = [];

			if($request->get('_token')){

				$inputs = $request->all();
				$conditions = [];
	
				foreach( $inputs as $key=>$value){
	
					if($key == "_token" ) continue;
					if($key == "exec_time" || $key == "rule_active"  || $key == "change_campaign_open" ){

						if($key == "exec_time"){
							$value = str_replace(" : ", ":", $value);
							$value = date("H:i", strtotime($value));
						}

						$rule->$key = $value;
					}else{
						$conditions = array_merge($conditions, [$key=>$value] );				
					}
				}
	
				$rule->conditions = json_encode($conditions);
				$rule->save();
				session()->flash('msg', 'Rule has been created.');
				return redirect()->route("ad.rules");
	
			}

		}

		$user_id = Auth::user()->id; //current user login id

		$title = "Ads Management > Create Rule";
		$accounts = Account::where('status', 'Active')->get()->pluck('name', 'seller_id')->toArray();
		$accounts = array_merge( ['' => "Any"], $accounts);				
		$users = User::where('verified', 1)->get()->pluck('name', 'id')->toArray();
		$users = array("" =>"Any")  + $users;
		
		
		return view('ad.new_rule', compact('title', 'accounts','user_id','users'));

	}

	public function fetchRulesAction(){//get all list of rules

		$rules = AdRule::orderBy('id','desc')->get();

		$data= [];
		$i=0;
		foreach($rules as $rule){

			$data[$i]['id'] = $rule->id;

			$conditions = json_decode($rule->conditions, true);
			$conditions_new_format = [];

		

				$data[$i]['asin'] = ($conditions['asin'])?$conditions['asin']:"Any";
				$data[$i]['state'] = (ucwords($conditions['state']))?ucwords($conditions['state']):"Any";
			//for managers
			/*
				$data[$i]['user_id'] = $conditions['user_id'];

				if($conditions['user_id']){
					
					$user = User::where('id', $conditions['user_id'])->first();
					$data[$i]['manager_name'] = $user->name;
					
				}else{

					$data[$i]['manager_name'] = "Any";
				}

				*/
				
				$data[$i]['seller_id'] = $conditions['seller_id'];
				if($conditions['seller_id']){
					$seller = Account::where('seller_id', $conditions['seller_id']);
					$seller_name = "[".$seller->first()->code."] ".$seller->first()->name;
					$data[$i]['seller_name'] = $seller_name;
				}else{
					$data[$i]['seller_name'] = "Any";
				}

				$data[$i]['containedword'] = ($conditions['containedword'])?$conditions['containedword']:"Any";
				$data[$i]['targetingType'] = (ucwords($conditions['targetingType']))?ucwords($conditions['targetingType']):"Any";
				
				$editLink = url("/ads/rules/".$data[$i]['id']."/edit");
				$delLink = url("/ads/rules/delete/".$data[$i]['id']);
				
				$data[$i]['action_links']['edit'] = $editLink;
				$data[$i]['action_links']['delete'] = $delLink;
				

			

			$data[$i]['exec_time'] = date("g:i A", strtotime($rule->exec_time));
			$data[$i]['rule_active'] = ($rule->rule_active)?"Active":"No";
			$data[$i]['change_campaign_open'] = ($rule->change_campaign_open)?"Enable Campaign":"Pause Campaign";

			$i++;
		}

		return $data;

	}

	public function editRuleAction(Request $request, $id) {//edit rule

		$title = "Edit Rule";

		$rule = AdRule::where('id', $id)->firstOrFail();

		if($request->get('_token')){

			$inputs = $request->all();
			$conditions = [];

			foreach( $inputs as $key=>$value){

				if($key == "_token" ) continue;
				if($key == "exec_time" || $key == "rule_active"  || $key == "change_campaign_open" ){
					if($key == "exec_time"){
						$value = str_replace(" : ", ":", $value);
						$value = date("H:i", strtotime($value));
					}

					$rule->$key = $value;
				}else{
					$conditions = array_merge($conditions, [$key=>$value] );				
				}
			}

			$rule->conditions = json_encode($conditions);
			$rule->save();
			session()->flash('msg', 'Rule has been updated.');
			return redirect()->route("ad.rules");

		}

		$accounts = Account::where('status', 'Active')->get()->pluck('name', 'seller_id')->toArray();
		$accounts = array_merge( ['' => "Any"], $accounts);				
		$users = User::where('verified', 1)->get()->pluck('name', 'id')->toArray();		
		$users = array("" =>"Any")  + $users;
		$conditions = json_decode($rule->conditions);
		
		return view('ad.edit_rule', compact('title','accounts','users','id','rule','conditions'));

	}

	public function deleteRuleAction(Request $request, $id){

		$rule = AdRule::where('id', $id)->firstOrFail();

		$rule->delete();

		session()->flash('msg', 'Rule has been deleted.');
		return redirect()->route("ad.rules");		


	}

	public function updateAutoSchedulerStateAction(Request $request) {
		
		$campaignId = $request->get('campaignId');
		$campaign = AdCampaign::where('campaignId', $campaignId)->first();
		$campaign->auto_scheduler = ( $campaign->auto_scheduler == 'enabled' ) ? 'disabled' : 'enabled';
		$campaign->save();

	}

	public function deleteCampaign(Request $request, $campaignId) {

		$campaign = AdCampaign::where('campaignId', $campaignId)->firstOrFail();

		$new_state = "archived";

		$campaignData = array(
					array(
						"campaignId" => (float) $campaign->campaignId,
						"state" => $new_state
					)
				);
		$compaign_update = new AdCampaign;
		$response = $compaign_update->updateCampaignStateAction($campaign->campaignId, $campaignData); //change state in amazon production
				
		if($response['success']){
			$campaign->state = $new_state;
			$campaign->save();
		}

		session()->flash('msg', 'Campaign has been '.$new_state.'.');
		return redirect()->route("ad.manage");	

	}





	
}
