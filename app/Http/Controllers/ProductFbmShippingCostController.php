<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductFbmShippingCost;

class ProductFbmShippingCostController extends Controller
{
    /**
	 * authencated user only
	 */
	public function __construct()
	{
		parent::__construct();
		$this->middleware('auth');
	}
	
	public function store(Request $request) {
		$productId = $request->input('product_id');
		$startDate = $request->input('start_date');
		$cost = $request->input('cost');
		$productCost = ProductFbmShippingCost::firstOrNew(['product_id'=>$productId, 'start_date'=>$startDate]);
		$productCost->cost_per_unit = $cost;
		$productCost->save();
		return response()->json($productCost);
	}

	public function update(Request $request) {

	}

	public function destroy(ProductFbmShippingCost $productFbmShippingCost) {
		$productFbmShippingCost->delete();
		return response('deleted', 200);
	}


}
