<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Grids\RedeemGrid;
use Illuminate\Http\Request;
use App\Redeem;
use App\OrderItem;
use App\InviteReviewLetterTemplate;
use App\InviteReviewLetter;
use Sheets;
use Log;

class RedeemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    public function dashboard() {
        $title = 'Redeem Dashboard';
        
        return view('redeem.dashboard', [
            'title' => $title
        ]);
    }

    public function freeProductAction(Request $request) {
        $claimReviewCampaignId = $request->claim_review_campaign_id;
        $title = "Free Product Redeems";
        $redeemType = "Same Free Product";
        $grid = RedeemGrid::grid('free-product', $claimReviewCampaignId)->render();

        return view('redeem.index', ['grid' => $grid, "title"=>$title, "redeemType"=>$redeemType]);
    }

    public function warehouseRequestAction() {

        $title = "Warehouse Free Product Requests";
        $grid = RedeemGrid::grid('warehouse_request')->render();

        return view('redeem.index', ['grid' => $grid, "title"=>$title]);
    }

    public function outofstockRequestAction() {

        $title = "Out of Stock Requests";
        $grid = RedeemGrid::grid('out_of_stock')->render();

        return view('redeem.index', ['grid' => $grid, "title"=>$title]);
    }    

    public function giftcardAction(Request $request) {
        $claimReviewCampaignId = $request->claim_review_campaign_id;
        $title = "Gift Card Redeems";
        $redeemType = "Amazon Gift Card";
        $grid = RedeemGrid::grid('giftcard', $claimReviewCampaignId)->render();

        return view('redeem.index', ['grid' => $grid, "title"=>$title, "redeemType"=>$redeemType]);
    }

    public function inviteReviewRedeems(Request $request) {
        $channels = config('invitereview.channels');
        $title = "Invite Review Redeems";
        $redeemType = "INVITE_REVIEW_REDEEM";
        $grid = RedeemGrid::grid('invite_revew_redeems')->render();
        return view('redeem.index', ['grid' => $grid, "title"=>$title, "redeemType"=>$redeemType, "channels"=>$channels]);
    }

    public function followupAction() {

        $title = "Gift Card Follow Up";
        $grid = RedeemGrid::grid('followup')->render();

        return view('redeem.index', ['grid' => $grid, "title"=>$title]);
    }    

    public function inviteReviewFollowUp() {
        $title = "Invite Review Follow Up";
        $grid = RedeemGrid::grid('invite-review-followup')->render();
        return view('redeem.index', ['grid' => $grid, "title"=>$title]);
    }
    public function refundAction() {

        $title = "Refund Redeems";
        $grid = RedeemGrid::grid('refund')->render();

        return view('redeem.index', ['grid' => $grid, "title"=>$title]);
    }

    public function replacementAction() {

        $title = "Replacement Redeems";
        $grid = RedeemGrid::grid('replacement')->render();

        return view('redeem.index', ['grid' => $grid, "title"=>$title]);
    }

    public function otherAction(Request $request) {

        $claimReviewCampaignId = $request->claim_review_campaign_id;
        $title = "Other Redeems";
        $redeemType = "Other Redeems";
        $grid = RedeemGrid::grid('other', $claimReviewCampaignId)->render();

        return view('redeem.index', ['grid' => $grid, "title"=>$title, "redeemType"=>$redeemType]);
    }

    public function ajaxsaveAction(Request $request) {
        $value = $request->get('value');
        list($type,$id) = explode('-', $request->get('pk'));
        try {
            $redeem = Redeem::find($id);
            if ($redeem) {
                $redeem->$type = $value;
                if ($type == 'review_link') {
                    $redeem->review_link_date = date('Y-m-d');
                }
                $redeem->save();
            }
            $user = auth()->user();
            $message = [
                'model' => 'Redeem',
                'id' => $id,
                'user_id' => $user->id,
                'ip' => $request->ip(),
                'attribute' => $type,
                'value' => $value
            ];
            Log::channel('audit')->info(json_encode($message));
            echo 1;
        } catch (\Exception $e) {
            print $e->getMessage();
        }
    }

    public function deleteTestData() {
        Redeem::deleteTestRecord();
        return "Deleted, you may close this tab. Only email with " . join(Redeem::$testEmails, ","). " are deleted." ;
    }

    public function checkExists(Request $request) {
        $amazonOrderId = $request->amazon_order_id;
        return Redeem::where('AmazonOrderId', $amazonOrderId)->exists() ? 1 : 0;
    }

    public function store(Request $request) {
        $amazonOrderId = $request->input('AmazonOrderId');
        $asin = $request->input('asin');
        $name = $request->input('name');
        $email = $request->input('email');
        $redirectTo = $request->input('redirect_to') . "?Redeems%5Bfilters%5D%5BAmazonOrderId-eq%5D=" . $amazonOrderId;
        $orderItem = OrderItem::where('AmazonOrderId', $amazonOrderId)->where('asin', $asin)->first();
        if ($orderItem) {
            if ($request->input('how_to_help') == "INVITE_REVIEW_REDEEM") {
                $redeem = new Redeem($request->except(['redirect_to', 'customer_wechat_account', 'customer_facebook_account']));
                $redeem->customer_wechat_account = $request->input('customer_wechat_account');
                $redeem->customer_facebook_account = $request->input('customer_facebook_account');
            } else {
                $redeem = new Redeem($request->except('redirect_to'));
            }
            $redeem->seller_id = $orderItem->seller_id;
            $redeem->country = $orderItem->country;
            $redeem->created_by = auth()->user()->id;
            $redeem->source = 'unknown';
            $redeem->save();
            return redirect($redirectTo);
        } else {
            return response('not able to find order item with order id and asin', 404);
        }
        
    }

    public function inviteLetter(Request $request) {
        $redeemId = $request->id;
        $redeem = Redeem::find($redeemId);
        $eligibleProducts = $redeem->productsForInviteReview();
        foreach ($eligibleProducts as $product) {
            $product->amzLink = $product->amazonLink();
        }
        $eligibleProductsJson = $eligibleProducts->map(function($item){return [$item->id, $item->asin, $item->country];})->toJson();
        $letterTemplates = InviteReviewLetterTemplate::where('status', 1)->where('country', $redeem->country)->get();
        return view('redeem.invite-review', [
            'title' => 'Invite Review Letter',
            'eligibleProducts' => $eligibleProducts,
            'eligibleProductsJson' => $eligibleProductsJson,
            'redeem' => $redeem,
            'letterTemplates' => $letterTemplates
        ]);
    }

    // just mark as sent, and save the letter. Not actually send email through code.
    public function sendInviteReviewLetter(Request $request) {
        $buyerId = $request->input('buyer_id');
        $redeemId = $request->id;
        if (empty($redeemId) || empty($buyerId)) {
            return redirect(route('redeems.invite-review', ['id' => $redeemId]));
        }

        $redeem = Redeem::find($redeemId);
        $inviteReviewLetter = new InviteReviewLetter([
            'buyer_id' => $buyerId,
            'redeem_id' => $redeemId,
            'subject' => $request->input('subject'),
            'content' => $request->input('content'),
            'products'=> $request->input('products'),
            'invite_review_letter_template_id' => $request->input('invite_review_letter_template_id')
        ]);
        $inviteReviewLetter->created_by = auth()->user()->id;
        $inviteReviewLetter->save();
        $redeem->is_invite_review_letter_sent = true;
        $redeem->save();
        return redirect(route('redeems.invite-review', ['id' => $redeemId]));
    }
}