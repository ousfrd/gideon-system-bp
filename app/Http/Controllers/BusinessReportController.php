<?php

namespace App\Http\Controllers;

use App\Grids\BusinessReportGrid;

class BusinessReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    public function indexAction() {

        $title = "Business Reports";
        $grid = BusinessReportGrid::grid();

        return view('bizreport.index', ['grid' => $grid, "title"=>$title]);
    }

    public function viewAction($asin = null) {

        $title = "Manage Report #" . $asin;

        $grid = BusinessReportGrid::report($asin);

        return view('bizreport.report', ['grid' => $grid, "title"=>$title]);
    }

}