<?php

namespace App\Http\Middleware;

use Closure;

class SecureIpAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $allowedIps = explode(',', env('SECURE_IPS', ""));
        $requestIp = request()->ip();
        if ($requestIp) {
            if (count($allowedIps) > 0) {
                $requestComps = explode('.', $requestIp);
                $ipMatched = false;
                foreach ($allowedIps as $ip) {
                    $ipComps = explode('.', trim($ip));
                    if (
                        ($ipComps[0] == '*' || $requestComps[0] == $ipComps[0]) &&
                        ($ipComps[1] == '*' || $requestComps[1] == $ipComps[1]) &&
                        ($ipComps[2] == '*' || $requestComps[2] == $ipComps[2]) &&
                        ($ipComps[3] == '*' || $requestComps[3] == $ipComps[3])
                        ) {
                            $ipMatched = true;
                            break;
                        }
                }
                if ($ipMatched) {
                    return $next($request);
                } else {
                    return response('Your Ip is not in our trust list', 401);
                }
            }
            else {
                return $next($request);
            }
            
        } else {
            return response('Can not verify you ip', 401);
        }
        
        return $next($request);
    }
}
