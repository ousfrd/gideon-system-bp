<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
    	
    	
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }
		
        $user = Auth::user();
        
       	if($user->verified == 0) {
       		session()->flash('msg','Your account is not verified yet.');
       		Auth::logout();
       		return redirect()->guest('login');
       	}

       	$user->last_login = date('Y-m-d H:i:s');
       	$user->last_login_ip = $this->getClientIP();
       	$user->save();
       
        return $next($request);
    }
    
    /**
     * Get the client IP address.
     *
     * @return string
     */
    public function getClientIP()
    {
    	$remotes_keys = [
    			'HTTP_X_FORWARDED_FOR',
    			'HTTP_CLIENT_IP',
    			'HTTP_X_FORWARDED',
    			'HTTP_FORWARDED_FOR',
    			'HTTP_FORWARDED',
    			'REMOTE_ADDR',
    			'HTTP_X_CLUSTER_CLIENT_IP',
    	];
    	foreach ($remotes_keys as $key) {
    		if ($address = getenv($key)) {
    			foreach (explode(',', $address) as $ip) {
    				if ($this->isValid($ip)) {
    					return $ip;
    				}
    			}
    		}
    	}
    	return '127.0.0.0';
    }
    
    /**
     * Checks if the ip is valid.
     *
     * @param string $ip
     *
     * @return bool
     */
    private function isValid($ip)
    {
    	if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)
    			&& !filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 | FILTER_FLAG_NO_PRIV_RANGE)
    			) {
    				return false;
    			}
    			return true;
    }
}
