<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;

class CheckPayment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $paymentUntil = config('tenant.payment.until');
        if ($paymentUntil == '-1') {
            return $next($request);
        } else {
            if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $paymentUntil)) {
                $today = Carbon::now();
                $until = Carbon::parse($paymentUntil);
                if ($until->greaterThanOrEqualTo($today)) {
                    return $next($request);
                }
            }
            return response(File::get(public_path() . '/html/require-payment.html'));
        }
        
    }
}
