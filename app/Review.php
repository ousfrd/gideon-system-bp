<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use App\Helpers\ReviewHelper;
use Carbon\Carbon;

class Review extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'reviews';
	protected $fillable = ['category', 'asin', 'country', 'review_date', 'profile_name', 'review_title', 'review_text', 'review_rating', 'review_link', 'verified_purchase', 'review_exist', 'image', 'video', 'review_id'];
	static function insertReviewInfo($review_id, $country, $review_exist, $video, $image, $review_text, $review_link, $verified_purchase, $asin, $review_date, $profile_name, $review_title, $review_rating) {
		$review = Review::where('review_id', $review_id)->get();
		$review_new = ['country' => $country, 'review_exist' => $review_exist, 'review_id' => $review_id, 'video' => $video, 'image' => $image, 'review_text' => $review_text, 'review_link' => $review_link, 'verified_purchase' => $verified_purchase, 'asin' => $asin, 'review_date' => $review_date, 'profile_name' => $profile_name, 'review_title' => $review_title, 'review_rating' => $review_rating];
			
		if ($review -> isEmpty()) {
			$newReview = new Review($review_new);
			$newReview->save();
		} else {
			Review::where('review_id', $review_id)->update($review_new);
		};
	}

	static function updateStatus($review_id, $review_exist) {
        $review = Review::where('review_id', $review_id)->update(['review_exist' => $review_exist]);
        return $review;
	}

	static function updateFollowUpStatus($review_id, $review_follow_up) {
        $review = Review::where('review_id', $review_id)->update(['review_follow_up' => $review_follow_up]);
        return $review;
	}

	static function getCategorizedQuantity($asin) {
		$quantity = Review::select(DB::raw('count(*) as quantity, category'))->where('asin', $asin)->groupBy('category')->get()->keyBy('category');
		$result = [];
		foreach (ReviewHelper::ReviewTypes as $cat) {
			if ($quantity->has($cat)) {
				$result[$cat] = $quantity[$cat]->quantity;
			} else {
				$result[$cat] = 0;
			}
		}
		return $result;
	}

	// Not finished Yet.
	static function getTimeSerialQuantity($asin, $startDate, $endDate) {
		$start = Carbon::createFromFormat('Y-m-d', $startDate);
		$end = Carbon::createFromFormat('Y-m-d', $endDate);
		$days = $end->diffInDays($start);
		$reviewQuantity = Review::select('review_date', DB::raw('count(*) as quantity'))
														->where('review_date', '<', $endDate)
														->where('asin', $asin)
														->orderBy('review_date', 'asc')
														->groupBy('review_date')
														->get();
		// for( $i=0; $i<$days; $i++ ) {

		// }
	}
	
	static function updateCategory($asin) {
		if ($asin == '') {
			$reviews = Review::whereNotNull('asin')->get();
		} else {
			$reviews = Review::where('asin', $asin)->get();
		}
		foreach ($reviews as $review) {
			$review_new = $review->update(['category' => ReviewHelper::getReviewType($review->review_id)]);
		};
	}

	static function updateCategoryForAll() {
		$offset = 0;
		$limit = 10;
		while (true) {
			$reviews = Review::select('id', 'category', 'review_id')
												->where('created_at', '>', Carbon::now()->subDays(15)->format('Y-m-d H:i:s'))
												->offset($offset)
												->limit($limit)
												->get();
			if (empty($reviews) || count($reviews) == 0) {
				return;
			}
			foreach ($reviews as $review) {
				$review->category = ReviewHelper::getReviewType($review->review_id);
				$review->save();
			}
			if (count($reviews) < $limit) {
				return;
			} else {
				$offset += $limit;
			}
		}
		
		
	}
}
	