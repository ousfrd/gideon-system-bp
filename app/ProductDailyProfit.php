<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use App\Traits\MassInsertOrUpdate;
// use GuzzleHttp\Client as GuzzleClient;
// use Symfony\Component\DomCrawler\Crawler;
use Nesk\Puphpeteer\Puppeteer;
use Nesk\Rialto\Data\JsFunction;
use Telegram;

class ProductDailyProfit extends Model
{
	
	use MassInsertOrUpdate;

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'product_daily_profit';
	
	public static function get_beginning() {
		$first_record = ProductDailyProfit::orderBy('date', 'asc')->first();
		if($first_record) {
			return $first_record->date;
		} else {
			return "2010-01-20";
		}
	}


	static function asinWatcher($date) {
		// $client = new GuzzleClient();
		$data = [];
		$messages = [];
		$yesterday = date('Y-m-d', strtotime("-1 days"));
		// get all profitable products from yesterday record
		$products = self::where('date', $yesterday)->select('asin', 'country', 'region')->groupBy('asin', 'country')->orderBy('profit', 'desc')->get();
		$puppeteer = new Puppeteer(['executable_path' => '/usr/local/bin/node']);
		$browser = $puppeteer->launch();
		$page = $browser->newPage();	
		foreach($products as $product) {
			echo "Start Watch ASIN: " . $product->asin . ", " . $product->country . "\n";
			// get pm
			$product_exists = Product::where('asin', $product->asin)->where('country', $product->region)->exists();
			if($product_exists) {
				$product = Product::where('asin', $product->asin)->where('country', $product->region)->first();
				if( count($product->users) > 0 ) {
					$pm = $product->users()->first()->name;
				} else {
					$pm = "Others";
				}
			} else {
				$pm = "Others";
			}
			$product_name = $product->name;
			$pieces = explode(" ", $product_name);
			$product_name = implode(" ", array_splice($pieces, 0, 5));
			

			$base_url = config('app.amzLinks')[$product->country] . "dp/" . $product->asin;
			$review_url = config('app.amzLinks')[$product->country] . "product-reviews/" . $product->asin."/ref=cm_cr_arp_d_viewopt_sr?ie=UTF8&reviewerType=all_reviews&filterByStar=critical&pageNumber=1";
			
			$page->goto($base_url);
			$page->waitFor(1000);
			$page_data = $page->evaluate(JsFunction::createWithBody("
				var price = '';
				var priceblock_ourprice =  document.getElementById('priceblock_ourprice');
				if(typeof(priceblock_ourprice) != 'undefined' && priceblock_ourprice != null) {
					price = priceblock_ourprice.textContent;
				}
				var priceblock_dealprice =  document.getElementById('priceblock_dealprice');
				if(typeof(priceblock_dealprice) != 'undefined' && priceblock_dealprice != null) {
					price = priceblock_dealprice.textContent;
				}
				var priceblock_saleprice =  document.getElementById('priceblock_saleprice');
				if(typeof(priceblock_saleprice) != 'undefined' && priceblock_saleprice != null) {
					price = priceblock_saleprice.textContent;
				}

				var reviews_el = document.getElementById('acrCustomerReviewText');
				if(typeof(reviews_el) != 'undefined' && reviews_el != null) {
					reviews_text = reviews_el.textContent;
				} else {
					reviews_text = '';
				}

				var stars_el = document.getElementById('acrPopover');
				if(typeof(stars_el) != 'undefined' && stars_el != null) {
					stars_text = stars_el.getAttribute('title');
				} else {
					stars_text = '';
				}

				var buybox_el = document.querySelector('#merchant-info a');
				if(typeof(buybox_el) != 'undefined' && buybox_el != null) {
					buybox = buybox_el.textContent;
				} else {
					buybox = '';
				}

			    return {
			    	reviews_text: reviews_text,
			    	stars_text: stars_text,
			    	buybox: buybox,
			    	price_text: price,
			    }
			"));

			$insertrow = [];
			$insertrow['asin'] = $product->asin;
			$insertrow['country'] = $product->country;
			$insertrow['date'] = date('Y-m-d');
			$insertrow['reviews'] = (int) str_replace(",", "", explode(" ", $page_data['reviews_text'])[0]);
			$insertrow['stars'] = (float) explode(" ", $page_data['stars_text'])[0];
			$insertrow['buybox'] = $page_data['buybox'];

			$price_text = str_replace(",", ".", $page_data['price_text']);
			$char_length = strlen($price_text);
			$price_arr = str_split($price_text);
			$price_idx = 0;
			for($i = 0; $i < $char_length; $i++) {
				if(is_numeric($price_arr[$i])) {
					$price_idx = $i;
					break;
				}
			}
			$insertrow['price'] = (float) substr($price_text, $price_idx);

			$page->goto($review_url);
			$page->waitFor(1000);
			$critical_reviews_text = $page->evaluate(JsFunction::createWithBody("
				var critical_review = document.querySelector('#cm_cr-review_list span.a-size-base:first-child');
				if(typeof(critical_review) != 'undefined' && critical_review != null) {
					return critical_review.textContent
				} else {
					return ''
				}
			"));

			$critical_reviews = 0;
			if($critical_reviews_text != "") {
				$critical_reviews_arr = explode(" ", $critical_reviews_text);
				foreach($critical_reviews_arr as $cr) {
					if(is_numeric($cr)) {
						$critical_reviews = (int) $cr;
						break;
					}
				}	
			}
			$insertrow['critical_reviews'] = $critical_reviews;
			$insertrow['updated_at'] = date('Y-m-d H:i:s');
			$data[] = $insertrow;

			$today = date('Y-m-d');
			$record_existed = AsinWatcher::where('date', $today)->where('asin', $product->asin)->where('country', $product->country)->exists();
			if( $record_existed ) {

				$last_record = AsinWatcher::where('date', $today)->where('asin', $product->asin)->where('country', $product->country)->first();

				if(abs($last_record->reviews - $insertrow['reviews']) >= 20) {
					$messages[$pm][$product->asin."-".$product->country]['reviews_change'] = "Reviews has changed from " . $last_record->reviews . " to " . $insertrow['reviews'] . "\n";
				}

				if(abs($last_record->stars - $insertrow['stars']) >= 0.1) {
					$messages[$pm][$product->asin."-".$product->country]['stars_change'] = "Stars has changed from " . $last_record->stars . " to " . $insertrow['stars'] . "\n";
				}

				if($last_record->buybox != $insertrow['buybox']) {
					$messages[$pm][$product->asin."-".$product->country]['buybox_change'] = "Buybox has changed from " . $last_record->buybox . " to " . $insertrow['buybox'] . "\n";
				}

				if($last_record->price != $insertrow['price']) {
					$messages[$pm][$product->asin."-".$product->country]['price_change'] = "Price has changed from " . config('app.countriesCurrency')[$product->country] . $last_record->price . " to " . config('app.countriesCurrency')[$product->country] . $insertrow['price'] . "\n";
				}

				if($last_record->critical_reviews != $insertrow['critical_reviews']) {
					$messages[$pm][$product->asin."-".$product->country]['critical_reviews_change'] = "Critical Reviews has changed from " . $last_record->critical_reviews . " to " . $insertrow['critical_reviews'] . "\n";
				}	

				if(!empty($messages[$pm][$product->asin."-".$product->country]))	 {
					$messages[$pm][$product->asin."-".$product->country]['name'] = "<a href='".$base_url."'>" . $product_name . ", " . $product->asin . " " . $product->country . "</a>";
				}

			}

			echo "Finish Watch ASIN: " . $product->asin . ", " . $product->country . "\n";
		}

		$browser->close();

		$group_chat_id = config('app.group_chat_id');
		$my_chat_id = "168790842";
		if(!empty($messages)) {
			$message = "";
			$message_count = count($messages);
			$i = 0;
			foreach($messages as $pm => $changes) {
				$user_ids = ['Eric' => '226884258', 'Yuzhen' => '156111271', 'Fisher' => '130536670', 'Dajie Bai' => '450329095', 'WuLiang' => '132447064', 'Shaoxing' => '202817404', 'Bob' => '159358768'];
				if(array_key_exists($pm, $user_ids)) {
					if($pm == "Eric") {
						$message .= "<a href='tg://user?id=".$user_ids[$pm]."'>" . $pm . "</a>, <a href='tg://user?id=77289779'>Yanjie</a>:\n\n";
					} else {
						$message .= "<a href='tg://user?id=".$user_ids[$pm]."'>" . $pm . "</a>:\n\n";
					}
				} else {
					$message .= $pm . ":\n\n";
				}
				

				foreach ($changes as $asin_country => $sub_changes) {
					$message .= $sub_changes['name'] . ":\n";
					foreach($sub_changes as $key => $value) {
						if($key != 'name') {
							$message .= $value;
						}
					}
					$message .= "\n";
				}
				$i += 1;
				if($i < $message_count) {
					$message .= "----------------------------------------------------\n\n";
				} else {
					$emoticons = "\uD83D\uDE4F\uD83D\uDD25\uD83D\uDCAA\uD83E\uDD41\uD83D\uDE80";
					$message .= "Fighting!\n".json_decode('"'.$emoticons.'"');
				}
				
			}

			$response = Telegram::sendMessage([
			  'chat_id' => $group_chat_id, 
			  'text' => $message,
			  'parse_mode' => 'HTML',
			  'disable_web_page_preview' => true
			]);			
		}


		try {
			$result = AsinWatcher::insertOrUpdate($data);
		} catch( \Exception $e ) {
			var_dump($e->getMessage());
		}

	}


}

