<?php

namespace App;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use App\Redeem;
use App\ManagedReviewOrder;
use App\OrderItemMarketing;
use App\ProductCost;
use App\Order;
use App\ProductReviewOverview;
use App\SalesRankUpdate;
use App\WarehouseActivity;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Listing;
use App\Helpers\DataHelper;

/** 
 * fields
 * qty: sum of all inventory's fulfillable_inventory
*/

class Product extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'products';
	
	protected $fillable = ['asin','code','country','keywords','title','product_group','old_asin','type'];
// 	public $timestamps = false;

	protected $appends = array('short_name', 'medium_image');

	
	const LOW_STOCK_CONDITION = '((warehouse_qty/daily_ave_orders - (manufacture_days+cn_to_amz_shipping_days+5) <= 5 and peak_season_mode = 0)
				or
				(warehouse_qty/daily_ave_orders - (ps_manufacture_days+ps_cn_to_amz_shipping_days+5) <= 5 and peak_season_mode = 1))
				';
	
	protected static $amzLinks = [
			'US'=>'https://www.amazon.com/',
			'CA'=>'https://www.amazon.ca/',
			'MX'=>'https://www.amazon.com.mx/',
			'UK'=>'https://www.amazon.co.uk/',
			'DE'=>'https://www.amazon.de/',
			'ES'=>'https://www.amazon.es/',
			'FR'=>'https://www.amazon.fr/',
			'IT'=>'https://www.amazon.it/',
			'JP'=>'https://www.amazon.co.jp/',
			'AU'=>'https://www.amazon.com.au/',
	];
	
	function country_scoped_active_listings() {
		return $this->hasMany('App\Listing','product_id','id')->where('status',1)->where('country', $this->country);
	}
	
	function listings() {
		return $this->hasMany('App\Listing','product_id','id');
		// return $this->hasMany('App\Listing','asin','asin')->where('country', $this->country);
	}

	function listings2() {
		return $this->hasMany('App\Listing','asin','asin')->where('country', $this->country);
	}

	function active_listings() {
		return $this->hasMany('App\Listing','asin','asin')->where('country', $this->country)->where('status',1);
	}

	function inventories() {
		return $this->hasMany('App\Inventory','asin','asin')->where('country', $this->country);
	}
	
	function active_inventories() {
		return $this->hasMany('App\Inventory','product_id','id')->where('status',1);
	}

	function asin_watchers() {
		return $this->hasMany('App\AsinWatcher','asin','asin');
	}

	function users() {
		return $this->belongsToMany('App\User', 'product_users',  'product_id','user_id');
	}
	
	function email_templates() {
		return $this->belongsToMany('App\EmailTemplate', 'email_template_products',  'product_id','template_id');
	}

	function productCategory() {
		return $this->belongsTo('App\ProductCategory', 'category_id');
	}
	
	function managerList() {
		return $this->users()->pluck('users.name','users.id')->toArray();
	}

	function orderItems() {
		return $this->hasMany('App\OrderItem','ASIN','asin');
	}

	function productCosts() {
		return $this->hasMany('App\ProductCost', 'product_id', 'id');
	}

	function productShippingCosts() {
		return $this->hasMany('App\ProductShippingCost', 'product_id', 'id');
	}

	function productFbmShippingCosts() {
		return $this->hasMany('App\ProductFbmShippingCost', 'product_id', 'id');
	}

	function productStatistics() {
		return $this->hasMany('App\ProductStatistic', 'product_id', 'id');
	}

	function warehouseActivities() {
		return $this->hasMany('App\WarehouseActivity');
	}

	function getShortNameAttribute() {
		if ($this->alias) {
			return $this->alias;
		} else {
			return collect(explode(' ', $this->name))->take(5)->implode(' ');
		}
	}

	function getMediumImageAttribute() {
		if ($this->small_image) {
			return str_replace('_SL75_', '_SL200_' ,$this->small_image);
		} else {
			return null;
		}
	}

	function costInDate($costs, $date) {
		return $costs->first(function ($cost, $key) use($date) {
			return $cost->start_date <= $date && (empty($cost->end_date) || $cost->end_date > $date);
		});
	}

	function productCostInDate($date) {
		$costs = $this->productCosts;
		return $this->costInDate($costs, $date);
	}

	function currentProductCost() {
		return $this->productCostInDate(date('Y-m-d'));
	}

	function getCurrentProductCostAttribute() {
		$cost = $this->currentProductCost();
		return empty($cost) ? 0 : $cost->cost_per_unit;
	}

	function shippingCostInDate($date) {
		$costs = $this->productShippingCosts;
		return $this->costInDate($costs, $date);
	}

	function currentShippingCost() {
		return $this->shippingCostInDate(date('Y-m-d'));
	}

	function getCurrentShippingCostAttribute() {
		$cost = $this->currentShippingCost();
		return empty($cost) ? 0 : $cost->cost_per_unit;
	}

	function fbmShippingCostInDate($date) {
		$costs = $this->productFbmShippingCosts;
		return $this->costInDate($costs, $date);
	}

	function currentfbmShippingCost() {
		return $this->fbmShippingCostInDate(date('Y-m-d'));
	}

	function getCurrentFbmShippingCostAttribute() {
		$cost = $this->currentfbmShippingCost();
		return empty($cost) ? 0 : $cost->cost_per_unit;
	}
	
	
	static function amzLink($asin,$country) {
		return self::$amzLinks[strtoupper($country)].'dp/'.$asin;
	}
	
	function amazonLink() {
		return self::$amzLinks[strtoupper($this->country)].'dp/'.$this->asin;
	}
	
	function title() {
		return @$this->title?$this->title:"N/A";
	}

	function size() {
		$dimentions = [$this->width,$this->height,$this->length];
		
		rsort($dimentions);
		
		$size = "";
		if($this->weight * 16 < 12) {
			if($dimentions[0] < 15 && $dimentions[1] < 12 && $dimentions[2] < 0.75 ) {
				$size = 'small';
			}
		}else if($this->weight  < 20) {
			if($dimentions[0] < 18 && $dimentions[1] < 14 && $dimentions[2] < 8 ) {
				$size = 'large';
			}
		} else {
			$size = 'oversized';
		}
		return "{$size} {$this->length} x {$this->width} x {$this->height}";
	}
	
	
	static function aggregateFBAFee(Command $command = null){
		$ps = self::where('fba_fee_caled',0)->where('weight','>',0)->get();
		
		foreach ($ps as $p) {
			if($p->country == 'US') {
				$p->new_fba_fee = $p->fbaFeeUS();
			}elseif($p->country == 'CA') {
				$p->fbaFeeCA();
			}
			
			$p->fba_fee_caled = 1;
			$p->save();
			if($command!=null){
				$command->info($p->asin.' '.$p->country);
			}
		}
		
	}
	
	
	function fbaFee($month) {
		switch ($this->country) {
			case 'US':
				return $this->fbaFeeUS($month);
			case 'CA':
				return $this->fbaFeeCA($month);
			default:
				return 0;
		}
	}
	
	//https://www.amazon.ca/b/?node=13718757011
	function fbaFeeCA() {
		$weight = ceil($this->weight* 453.592); //grams
		$dimentions = [ceil($this->width*2.54/100),ceil($this->height*2.54/100),ceil($this->length*2.54/100)];
		rsort($dimentions);
		
		//Envelopes can hold packaged units with the following weight and dimensions: 
		//500 g or less, 
		//38 cm or less on its longest side, 
		//27 cm or less on its median side, 
		//2 cm or less on its shortest side. A unit exceeding any of these measurements is Standard-Size.
		
		if($weight <= 500 && $dimentions[0] <= 38 && $dimentions[1] <= 27 && $dimentions[2] <= 2){
			$size = 'small';
		} else {
			$size = 'large';
		}
		//Pick & Pack per unit 1.6
		$pickpack = 1.6;
		
		if($size == 'small') {
			if ($weight <=100 ) {
				$weigtHandling = 1.9;
			}else{
				$weigtHandling = 1.9 + 0.25 * ceil(($weight-100)/100);
			}
		} else {
			if ($weight <=500 ) {
				$weigtHandling = 4;
			}else{
				$weigtHandling = 4 + 0.4 * ceil(($weight-500)/500);
			}
		}
		
		$this->pickpack = 1.6;
		$this->weighthandling = $weigtHandling;
		$this->save();
		
		return $weigtHandling + $pickpack;
	}
	
	
	function fbaFeeUS($month=null) {
		if(empty($month)) {
			$month = date('n');
		}
		
		$size = $this->size();
		
		//print $size;
		$weight = ceil($this->weight);
		if($month <= 9) {
			
			switch ($size) {
				case 'small':
					return 2.41;
				case 'large':
					if($this->weight < 1) {
						return 2.99;
					}elseif($this->weight < 2) {
						return 4.18;
					} else {
						return 4.18 + ($weight-2)*0.39;
					}
				case 'oversized':
					return 6.85+ ($weight-2)*0.39;
			}
			
			
		} else {
			
			switch ($size) {
				case 'small':
					return 2.39;
				case 'large':
					if($this->weight < 1) {
						return 2.88;
					}elseif($this->weight < 2) {
						return 3.96;
					} else {
						return 3.96 + ($weight-2)*0.35;
					}
				case 'oversized':
					return 6.69+ ($weight-2)*0.35;
			}
			
			
		}
	}
	
	
	
	
	function orderReports($start_date,$end_date) {
		
		return Order::orderReportsByDate($start_date, $end_date,['asin'=>$this->asin,'country'=>$this->country]);
		
	}
	
	// 	A2梳子：假设目前已有库存2000，日均销量110，工厂生产周期30天
	// 	总补货周期=30+14+5=49
	// 	安全库存＝日均销量＊（总的补货周期＋5）
	// 	安全库存=110*（49+5）=5940
	
	// 	非高峰期备货采购量＝日均销量＊备货周期-已有库存（程序读取amazon总库存+采购录入已经在生产和在运输途中的库存）
	// 	高峰期备货采购量=日均销量＊备货周期*3（采购可以手动录入数字）
	// 	最佳采购日期＝今日日期＋库存还能支撑天数－总补货周期
	
	function totalQty() {
		return $this->total_qty ? $this->total_qty : $this->qty;
	}
	/**
	 * total_qty includes inbound shipment qty. for some reason, inbound shipment qty is not accurate
	 * @return unknown
	 */
	function wareHouseTotalQty() {
		return $this->warehouse_qty;
	}
	function getManufactureDays() {
		return $this->peak_season_mode? $this->ps_manufacture_days:$this->manufacture_days;
	}
	
	function getShippingDays() {
		return $this->peak_season_mode? $this->ps_cn_to_amz_shipping_days:$this->cn_to_amz_shipping_days;
	}
	
	/**
	 * 总补货周期＝深圳提交工厂采购到收到货的总时长（采购手动录入）＋CN到Amazon仓库运输时间（采购手动录入）（默认14天，特别产品视具体情况而定）＋5
	 * @return number
	 */
	function totalDayToReinstock(){
		return $this->getManufactureDays()+ $this->getShippingDays();
	}
	
	/**
	 * 日均销量=7天日均销量*70%+14天日均销量*20%+21天日均销量*10%
	 * @return number
	 */
	function dailyAverageOrders() {
		// return $this->daily_ave_orders;
		// return  round($this->last_7_days_orders *0.7/7 + $this->last_14_days_orders *0.2/14+ $this->last_21_days_orders *0.1/21,1);
		return $this->last_30_days_avg_orders;
	}
	
	
	
	/**
	 * 安全库存＝日均销量＊（总的补货周期＋5）
	 * @return number
	 */
	function safeStockQty(){
		return ceil($this->dailyAverageOrders() * ($this->totalDayToReinstock() + 5));
	}
	
	function isLowStock() {
		return $this->safeStockQty() > $this->wareHouseTotalQty() || $this->warehouseTotalQty() < 10;
	}
	
	
	
	function replenishNeeded() {
		return $this->safeStockQty() > $this->wareHouseTotalQty();
	}
	
	/**
	 * 非高峰期备货采购量＝日均销量＊备货周期-已有库存（程序读取amazon总库存+采购录入已经在生产和在运输途中的库存）
	 * 高峰期备货采购量=日均销量＊备货周期*3（采购可以手动录入数字）
	 * @return number
	 */
	function recommendedReplenishmentQty() {
		
		if ($this->peak_season_mode){
			$qty = $this->dailyAverageOrders() * $this->totalDayToReinstock() * 3 - $this->wareHouseTotalQty();
		} else {
			$qty = $this->dailyAverageOrders() * $this->totalDayToReinstock() - $this->wareHouseTotalQty();
		}
		
		return ceil($qty);
		
	}
	
	/**
	 * 最佳采购日期＝今日日期＋库存还能支撑天数－总补货周期
	 * @return string
	 */
	function recommendedReplenishmentDate() {
		
		if($this->daysToReplenish() === 'N/A') {
			return 'N/A';
		}
		
		if($this->daysToReplenish() > 0) {
			return date('Y-m-d',strtotime('+ '.($this->warehouseQtyDaysToOutofStock()-$this->totalDayToReinstock()).' days'));
		}
		else{
			return 'TODAY';
		}
	}
	/**
	 * 库存还能支撑天数
	 * @return string|mixed
	 */
	function daysToOutofStock() {
		if ($this->dailyAverageOrders() == 0) {
			return 'N/A';
		}
		//var_dump($this->total_qty);
		
		#USE ONLY WAREHOUSE QTY
// 		return max(0,ceil($this->wareHouseTotalQty() / $this->dailyAverageOrders()));
		
		return max(0,ceil(($this->totalQty()) / $this->dailyAverageOrders() ));
	}
	
	function warehouseQtyDaysToOutofStock(){
		if ($this->dailyAverageOrders() == 0) {
			return 'N/A';
		}
		//var_dump($this->total_qty);
		return max(0,ceil(($this->wareHouseTotalQty()) / $this->dailyAverageOrders()));
	}
	/**
	 * 库存还能支撑天数－总补货周期
	 * @return string|mixed
	 */
	function daysToReplenish() {
		if ($this->dailyAverageOrders() == 0) {
			return 'N/A';
		}
		return max(0,ceil(($this->wareHouseTotalQty() - $this->safeStockQty()) / $this->dailyAverageOrders()));
	}
	
	static function lowOrOutOfStockingCount() {
		
		#use warehouse_qty, inbound qty not accurate
		$query = 

		self::whereRaw(DB::Raw(self::LOW_STOCK_CONDITION))
		->where('products.status',1)
		->where('discontinued',0);
	
		if(Auth::user()->role == "product manager") {
			$query->whereIn('seller_listings.asin',Auth::user()->productList());
		}elseif(auth()->user()->role == "seller account manager") {
			$query->whereIn('seller_listings.seller_id',auth()->user()->accountList());
		}
		
		$count = $query->count();
		
		return $count;
	}
	
	static function lowOrOutOfStockingProducts(User $user=null,$daily_avg_ordes=0) {
		$query =
		
		self::whereRaw(DB::Raw(Product::LOW_STOCK_CONDITION))
		->where('products.status',1)
		->where('discontinued',0);
		
		if(empty($user)) {
			$user = Auth::user();
		}
		
		if(in_array($user->role,["product manager","admin"] )) {
			$query->whereIn('products.asin',$user->productList());
		}elseif($user->role == "seller account manager") {
			$query->whereIn('seller_listings.seller_id',$user->accountList());
		}
		
		$query->where('daily_ave_orders','>=',$daily_avg_ordes);
		
		$query->orderBy('daily_ave_orders','desc');
		
		return $query->get();
	}
	
	
	static function slowProducts(User $user=null,$daily_avg_ordes=0) {
		$query =
		
		self::where('products.status',1)
		->where('discontinued',0)
		
		->where('daily_ave_orders','<',5)
		//->where('open_date','<',date('Y-m-d',strtotime('-30 days')))
		->whereRaw(DB::raw('daily_ave_orders*avg_price_usd < 200'))
		->where('warehouse_qty','>',10)
		->whereRaw(DB::Raw('(total_qty/daily_ave_orders - (manufacture_days+cn_to_amz_shipping_days+5)) >= 60'));
		
		
		
		if(empty($user)) {
			$user = Auth::user();
		}
		
		if(in_array($user->role,["product manager","admin"] )) {
			$query->whereIn('products.asin',$user->productList());
		}elseif($user->role == "seller account manager") {
			$query->whereIn('seller_listings.seller_id',$user->accountList());
		}
		
// 		$query->where('daily_ave_orders','>=',$daily_avg_ordes);
		
		$query->orderBy('daily_ave_orders','asc')->orderByRaw(DB::Raw('total_qty desc'));
		
		return $query->get();
	}
	
	function inventoryHistory($start_date,$end_date) {
		$query = DailyInventoryReport::where('asin',$this->asin)->where('country',$this->country)
		->whereBetween('date',[date('Y-m-d 00:00:00',strtotime($start_date)),date('Y-m-d 23:59:59',strtotime($end_date))]);
		
		$cols = [ 
				'date',
				DB::raw ( 'sum(fulfillable_quantity) as fulfillable_quantity' ),
				DB::raw ( 'sum(total_quantity) as total_quantity' ),
				DB::raw ( 'sum(inbound_working_quantity) as inbound_working_quantity '),
				DB::raw ( 'sum(inbound_shipped_quantity) as inbound_shipped_quantity' ),
				DB::raw ( 'sum(inbound_receiving_quantity) as inbound_receiving_quantity' ),
				DB::raw ( 'sum(reserved_quantity) as reserved_quantity' ),
				DB::raw ( 'sum(unsellable_quantity) as unsellable_quantity' )
		];
		$query->select($cols)->groupBy('date')->orderBy('date','asc');

		$rows = $query->get()->toArray();
		$totals = [];
		foreach ($rows as $row) {
			$totals[$row['date']] = $row;
		}
		
		for($date= $start_date;$date <= $end_date;$date = date("Y-m-d",strtotime("$date +1 day"))) {
			if(!isset($totals[$date]) || empty($totals[$date]) ) {
				$totals[$date] = ['fulfillable_quantity'=>0,
						'total_quantity'=>0,
						'inbound_working_quantity'=>0,
						'inbound_shipped_quantity'=>0,
						'inbound_receiving_quantity'=>0,
						'reserved_quantity'=>0,
						'unsellable_quantity'=>0
				];
			}
		}
		
		ksort($totals);
		
		return $totals;
		// 		return ['totals'=>$totals,'counts'=>$counts,'total_orders'=>$total_orders,'total_gross'=>$total_gross];
	}
	
	
	function ad_report($start_date, $end_date) {
		return AdReport::reportsDateRange($start_date, $end_date, ['asin'=>$this->asin,'country'=>$this->country]);
	}
	
	function reviewByRange($start_date, $end_date) {
		return Review::listByDateRange($start_date, $end_date, ['asin'=>$this->asin,'country'=>$this->country]);
	}
	
	function expenses($start_date=null, $end_date=null) {
		$product = $this;
		$query = ListingExpense::whereHas('listing',function($q) use($product){
			$q->where('product_id',$product->id);
		});
		
		if ($start_date) {
			$query->where('posted_date','>=',$start_date);
		}
		if ($end_date) {
			$query->where('posted_date','<=',$end_date);
		}
		
		$query->orderBy('posted_date','desc');
		
		return $query->get();
	}
	
	function inbound_shipments() {
		$product = $this;
		$query = InboundShipment::whereHas('listing',function($q) use($product){
			$q->where('product_id',$product->id);
		});
			
			
			
		$query->orderBy('id','desc');
			
			return $query->get();
	}


	static function updateProductDailyProfitByDate($start_date, $end_date) {

		$product_ads = AdReport::productAdsByDateRange($start_date, $end_date, []);
		$product_profits = Order::orderReportsByDateRange($start_date, $end_date, []);	
		$refunds = Order::refundsByDateRange($start_date, $end_date, []);

		$row_data = [];
		foreach($product_profits as $product_profit) {

			// $payout = $product_profit->total_gross+$product_profit->tax+$product_profit->shipping_fee+$product_profit->vat_fee+$product_profit->fba_fee+$product_profit->commission+$product_profit->total_promo+$product_profit->total_refund;
			// $cost = $product_profit->total_cost+$product_profit->ad_expenses;
			// $profit = $payout - $cost;


			$prime_key = $product_profit->seller_id . "-" . $product_profit->ASIN . "-" . $product_profit->country;
			$row_data[$prime_key][$product_profit->pdate] = [
				'total_units' => $product_profit->total_units, 
				'total_orders' => $product_profit->total_orders, 
				'total_gross' => $product_profit->total_gross, 
				'shipping_fee' => $product_profit->shipping_fee,
				'tax' => $product_profit->tax,
				'commission' => $product_profit->commission,
				'vat_fee' => $product_profit->vat_fee,
				'fba_fee' => $product_profit->fba_fee,
				'total_promo' => $product_profit->total_promo,
				'promo_count' => $product_profit->promo_count, 
				'total_cost' => $product_profit->total_cost, 
				'giftwrap_fee' => $product_profit->giftwrap_fee, 
				'other_fee' => $product_profit->other_fee, 
				'ad_expenses' => 0,
				'total_refund' => 0
			];


		}



		foreach ($product_ads as $product_ad) {
			if(empty($product_ad->asin)) {
				continue;
			}
			$prime_key = $product_ad->seller_id . "-" . $product_ad->asin . "-" . $product_ad->country;

			if(array_key_exists($prime_key, $row_data)) {
				if(array_key_exists($product_ad->date, $row_data[$prime_key])) {
					$row_data[$prime_key][$product_ad->date]['ad_expenses'] = $product_ad->total;
				} else {
					$row_data[$prime_key][$product_ad->date] = ['total_units' => 0, 'total_orders' => 0, 'total_gross' => 0, 'shipping_fee' => 0, 'tax' => 0, 'commission' => 0, 'vat_fee' => 0, 'fba_fee' => 0, 'total_promo' => 0, 'promo_count' => 0, 'total_cost' => 0, 'giftwrap_fee' => 0, 'other_fee' => 0, 'ad_expenses' => $product_ad->total, 'total_refund' => 0];
				}

			} else {
				$row_data[$prime_key][$product_ad->date] = ['total_units' => 0, 'total_orders' => 0, 'total_gross' => 0, 'shipping_fee' => 0, 'tax' => 0, 'commission' => 0, 'vat_fee' => 0, 'fba_fee' => 0, 'total_promo' => 0, 'promo_count' => 0, 'total_cost' => 0, 'giftwrap_fee' => 0, 'other_fee' => 0, 'ad_expenses' => $product_ad->total, 'total_refund' => 0];
			}

		}


		foreach ($refunds as $refund) {
			if(empty($refund->ASIN)) {
				continue;
			}
			$prime_key = $refund->seller_id . "-" . $refund->ASIN . "-" . $refund->country;

			if(array_key_exists($prime_key, $row_data)) {
				if(array_key_exists($refund->pdate, $row_data[$prime_key])) {
					$row_data[$prime_key][$refund->pdate]['total_refund'] = $refund->total_refund;
				} else {
					$row_data[$prime_key][$refund->pdate] = ['total_units' => 0, 'total_orders' => 0, 'total_gross' => 0, 'shipping_fee' => 0, 'tax' => 0, 'commission' => 0, 'vat_fee' => 0, 'fba_fee' => 0, 'total_promo' => 0, 'promo_count' => 0, 'total_cost' => 0, 'giftwrap_fee' => 0, 'other_fee' => 0, 'ad_expenses' => 0, 'total_refund' => $refund->total_refund];
				}

			} else {
				$row_data[$prime_key][$product_ad->date] = ['total_units' => 0, 'total_orders' => 0, 'total_gross' => 0, 'shipping_fee' => 0, 'tax' => 0, 'commission' => 0, 'vat_fee' => 0, 'fba_fee' => 0, 'total_promo' => 0, 'promo_count' => 0, 'total_cost' => 0, 'giftwrap_fee' => 0, 'other_fee' => 0, 'ad_expenses' => 0, 'total_refund' => $refund->total_refund];
			}

		}


		$data = [];
		foreach($row_data as $prime_key => $row) {

				$keys = explode("-", $prime_key);
				if($keys[1]==''||$keys[2]=='') {
					continue;
				}
				foreach ($row as $date => $value) {

					
					$insertrow['seller_id'] = $keys[0];
					$insertrow['asin'] = $keys[1];
					$insertrow['country'] = $keys[2];
					$insertrow['date'] = $date;
					$insertrow['region'] = config('app.countriesRegion')[$keys[2]];


					$payout = $value['total_gross']+$value['shipping_fee']+$value['vat_fee']+$value['fba_fee']+$value['commission']+$value['total_promo']+$value['giftwrap_fee']+$value['other_fee']+$value['total_refund'];
					$cost = $value['total_cost']+$value['ad_expenses'];
					$profit = $payout - $cost;

					$insertrow['orders'] = $value['total_orders'];
					$insertrow['profit'] = round($profit, 2);
					$insertrow['total_gross'] = round($value['total_gross'], 2);
					$insertrow['refund'] = round($value['total_refund'], 2);
					$insertrow['payout'] = round($payout, 2);
					$insertrow['product_cost'] = round($value['total_cost'], 2);
					$insertrow['ad_expenses'] = round($value['ad_expenses'], 2);
					$insertrow['total_cost'] = round($cost, 2);

					$data[] = $insertrow;
					

				}		

		}

		try {
			$result = ProductDailyProfit::insertOrUpdate($data);
		} catch( \Exception $e ) {
			// var_dump($e->getMessage());
		}



	}

	function calculateLifetimeStatistics() {
		$start_date = "2015-01-01 00:00:00";
		$end_date = date ( 'Y-m-d 23:59:59');
		$query = OrderItem::join( 'orders', function($join) {
			$join->on('orders.AmazonOrderId', '=', 'order_items.AmazonOrderId');
		});
		$query -> where("ASIN", $this->asin) -> whereBetween( 'PurchaseDate', [ 
			\DT::convertToUTC ( $start_date ),
			\DT::convertToUTC ( $end_date )
		]);
		$cols = [ 
			DB::raw ( 'sum(QuantityOrdered) as total_units' ),
			DB::raw ( 'sum( if((PromotionId IS NULL and total_promo = 0), QuantityOrdered, 0)) as total_units_full' ),
			DB::raw ( 'sum( if((PromotionId IS NOT NULL or total_promo != 0), QuantityOrdered, 0)) as total_units_promo' ),
			DB::raw ( 'count(orders.AmazonOrderId) as total_orders' ),
			DB::raw ( 'sum( (ItemPrice + chargeback)*exchange_rate ) as total_gross' ),
			DB::raw ( 'sum( if((PromotionId IS NULL and total_promo = 0), (ItemPrice + chargeback)*exchange_rate, 0)) as total_gross_full' ),
			DB::raw ( 'sum( if((PromotionId IS NOT NULL or total_promo != 0), (ItemPrice + chargeback)*exchange_rate, 0)) as total_gross_promo' ),
			DB::raw ( 'sum( ShippingPrice*exchange_rate) as shipping_fee '),
			DB::raw ( 'sum( tax*exchange_rate ) as tax' ),
			DB::raw ( 'sum(commission*exchange_rate) as commission' ),
			DB::raw ( 'sum(fba_fee*exchange_rate) as fba_fee' ),
			DB::raw ( 'sum(total_promo*exchange_rate) as total_promo' ),
			DB::raw ( 'sum(if((PromotionId IS NOT NULL or total_promo != 0), 1, 0)) as promo_count' ),
			// DB::raw ( 'sum(total_refund*exchange_rate) as total_refund' ),
			// DB::raw ( 'sum(if((total_refund != 0), 1, 0)) as refund_count' ),
			DB::raw ( 'sum((cost+shipping_cost)*QuantityOrdered ) as total_cost' ),
			DB::raw ( 'sum(giftwrap_fee*exchange_rate) as giftwrap_fee' ),
			DB::raw ( 'sum(other_fee*exchange_rate) as other_fee' ),
			DB::raw ( 'sum(vat_fee) as vat_fee' ),
		];
		array_push($cols, DB::raw ( 'sum( (ItemPrice + ShippingPrice + chargeback + commission + vat_fee + fba_fee + total_promo + total_refund + giftwrap_fee + other_fee)*exchange_rate - (cost+shipping_cost)*QuantityOrdered ) as profit') );
		return $query -> select ( $cols ) -> get() -> first();
	}

	function updateLifetimeStatistics() {
		$row = $this -> calculateLifetimeStatistics();
		$this -> lifetime_profit = $row['profit'];
		$this -> lifetime_promotions = $row['total_promo'];
		$this -> lifetime_commission = $row['commission'];
		$this -> lifetime_fba_fee = $row['fba_fee'];
		$this -> lifetime_product_cost = $row['total_cost'];
		$this -> lifetime_gross_sales = $row['total_gross'];
		$this -> save();
	}

	function claimReviewOrderCount($from = null, $to = null) {
		$asin = $this->asin;
		$query1 = Redeem::select(DB::raw('count(*) as quantity, source'))->where('asin', $asin)->whereNotNull('review_link')->groupBy('source');
		$query2 = OrderItemMarketing::where('asin', $asin)->where('claim_review_id', '>', 0)->where('is_sent', true);
		if ($from) {
			$query1->where('created_at', '>=', $from);
			$query2->where('order_date', '>=', $from);
		}
		if ($to) {
			if ($from == $to) {
				$to = (new Carbon($to))->addDay();
			}
			$query1->where('created_at', '<', $to);
			$query2->where('order_date', '<', $to);
		}
		$allRedeems = $query1->get();
		$insertCardRedeems = 0;
		$otherRedeems = 0;
		foreach ($allRedeems as $row) {
			if ($row->source == 'insert_card') {
				$insertCardRedeems = $row->quantity;
			} else {
				$otherRedeems += $row->quantity;
			}
		}
		return ['total' => $query2->count(), 'reviews' => $otherRedeems, 'insert_card' => $insertCardRedeems];
	}

	function selfReviewOrderCount($from = null, $to = null) {
		$asin = $this->asin;
		$query1 = ManagedReviewOrder::where('asin', $asin)->whereNotNull('review_link');
		$query2 = ManagedReviewOrder::where('asin', $asin);
		if ($from) {
			$query1->where('order_date', '>=', $from);
			$query2->where('order_date', '>=', $from);
		}
		if ($to) {
			if ($from == $to) {
				$to = (new Carbon($to))->addDay();
			}
			$query1->where('order_date', '<', $to);
			$query2->where('order_date', '<', $to);
		}
		return ['total' => $query2->count(), 'reviews' => $query1->count()];
	}

	function inviteReviewOrderCount($from = null, $to = null) {
		$asin = $this->asin;
		$query1 = OrderItemMarketing::where('asin', $asin)->whereNotNull('review_link');
		$query2 = OrderItemMarketing::where('asin', $asin)->where('invite_review', true);
		if ($from) {
			$query1->where('order_date', '>=', $from);
			$query2->where('order_date', '>=', $from);
		}
		if ($to) {
			if ($from == $to) {
				$to = (new Carbon($to))->addDay();
			}
			$query1->where('order_date', '<', $to);
			$query2->where('order_date', '<', $to);
		}
		return ['total' => $query2->count(), 'reviews' => $query1->count()];
	}

	static function updateLifetimeStatisticsForAll() {
		$products = Product::where("qty", ">", 0)->get();
		foreach($products as $product) {
			$product->updateLifetimeStatistics();
		}
		Log::info("Product::updateLifetimeStatisticsForAll run at : " . date("Y-m-d H:i:s"));
	}

	function getLast7DaysAvgOrdersAttribute() {
		return $this->lastNdaysAvgOrders(7);
	}

	function getLast14DaysAvgOrdersAttribute() {
		return $this->lastNdaysAvgOrders(14);
	}
	
	function getLast21DaysAvgOrdersAttribute() {
		return $this->lastNdaysAvgOrders(21);
	}

	function getLast30DaysAvgOrdersAttribute() {
		return $this->lastNdaysAvgOrders(30);
	}

	function getLast90DaysAvgOrdersAttribute() {
		return $this->lastNdaysAvgOrders(90);
	}

	// days in [7, 14, 21, 30, 90]
	function lastNdaysAvgOrders($days) {
		$inStockDays = $this->{'last_'. $days . '_in_stock_days'};
		$orders = $this->{'last_' . $days . '_days_orders'};
		if ($inStockDays > 0) {
			return round($orders / $inStockDays);
		} else {
			return 0;
		}
	}

	function getOrdersStatisticTillNow($daysAgo) {
		$carbonDate = new Carbon("{$daysAgo} days ago");
		$date = \DT::convertToUTC($carbonDate->format('Y-m-d'));
		$today = \DT::convertToUTC(date('Y-m-d'));
		$query = $this->productStatistics()
									->whereBetween('date', [$date, $today])
									->select([
										DB::raw('sum(total_orders) as total_orders'),
										DB::raw('count(DISTINCT date) as in_stock_days')
									]);
		$result = $query->first();
		if ($result) {
			$oosDays = $daysAgo - $result->in_stock_days;
			if ($this->first_order_date) {
				$openDate = Carbon::parse($this->first_order_date);
				if ($carbonDate->lessThan($openDate)) {
					$oosDays = Carbon::now()->diffInDays($openDate) - $result->in_stock_days;
				}
			} else {
				$oosDays = 0;
			}
			return [ 
				'total_orders'=> $result->total_orders, 
				'in_stock_days' => $result->in_stock_days,
				'oos_days' => $oosDays
			];
		} else {
			return null;
		}
 	}

	static function calculateOrdersQuantityForAll() {
		$products = Product::all();
		$fields = [
			'last_7' => 7,
			'last_14' => 14,
			'last_21' => 21,
			'last_30' => 30,
			'last_90' => 90,
			'today_so_far_orders' => 0,
			'this_month_orders' => getdate(time())['mday']
		];
		foreach ( $products as $product ) {
			foreach ($fields as $field => $daysAgo) {
				$orderStatistic = $product->getOrdersStatisticTillNow($daysAgo);
				if ($orderStatistic) {
					if (preg_match('/.*_\d+$/', $field)) {
						$product->{$field . '_days_orders'} = $orderStatistic['total_orders'];
						$product->{$field . '_oos_days'} = $orderStatistic['oos_days'];
						$product->{$field . '_in_stock_days'} = $orderStatistic['in_stock_days'];
					} else {
						$product->$field = $orderStatistic['total_orders'];
					}
				}
			}
			$product->save();
		}
		Log::info("Product::calculateOrdersQuantityForAll run at : " . date("Y-m-d H:i:s"));
	}

	/**
	 * Status
	 * -1: removed if all listing of this product are removed
	 * 0: inactive if all listing of this product are inactive
	 * 1: active
	 */
	function updateStatus() {
		$existingListingsStatus = $this->listings->pluck('status');
		$status = 0;
		$removed = $existingListingsStatus->every(function ($value, $key) {
			return $value == -1;
		});
		$inactive = $existingListingsStatus->every(function ($value, $key) {
			return $value == 0;
		});
		if ($removed) {
			$status = -1;
		} else if ($inactive) {
			$status = 0;
		} else {
			$status = 1;
		}
		$this->status = $status;
		$this->save();
	}

	static function updateStatusForAll() {
		$products = Product::all();
		foreach($products as $product) {
			$product->updateStatus();
		}
	}

	static function deleteNoListingProducts() {
		$offset = 0;
		$limit = 20;
		while (true) {
			$products = Product::with("listings")->offset($offset)->limit($limit)->get();
			
			if (count($products) > 0) {
				foreach ($products as $product) {
					if (count($product->listings) == 0) {
						echo "delete ". $product->asin . " - " . $product->country;
						$product->delete();
					}
				}
				if (count($products) < $limit) {
					return;
				} else {
					$offset += $limit;
				}
			} else {
				return;
			}
		}
	}

	function getEarliestListingOpenDate() {
		$listings = $this->listings;
		if (count($listings) > 0) {
			$this->earliest_listing_open_date = $listings->min('open_date');
			$this->save();
		}
	}

	static function fillEarliestListingOpenDate() {
		$offset = 0;
		$limit = 20;
		while (true) {
			$products = Product::with("listings")->offset($offset)->limit($limit)->get();
			if (count($products) > 0) {
				foreach ($products as $product) {
					$product->getEarliestListingOpenDate();
				}
				if (count($products) < $limit) {
					return;
				} else {
					$offset += $limit;
				}
			} else {
				return;
			}
		}
	}

	function calculateInventoryTotalQty() {
		$listings = $this->listings->where('status', '>=', 0)->join('seller_accounts', function($query) {
			$query->on('seller_listings.seller_id', '=', 'seller_id.seller_id')->where('seller_accounts.status', '==', 'Active');
		  });
		$this->total_qty = $listings->sum('afn_total_quantity');
		$this->warehouse_qty = $listings->sum('afn_warehouse_quantity');
		$this->afn_fulfillable_quantity = $listings->sum('afn_fulfillable_quantity');
		$this->save();
	}

	public static function fillFirstOrderDate() {
		$products = Product::whereNull('first_order_date')->get();
		foreach ($products as $product) {
			$row = Order::select(DB::raw('MIN(PurchaseDate) as first_order_date'))
						->join('order_items', 'order_items.AmazonOrderId', '=', 'orders.AmazonOrderId')
						->where('order_items.asin', $product->asin)
						->where('order_items.country', $product->country)
						->first();
			if ($row) {
				$product->first_order_date = $row->first_order_date;
				$product->save();
			}
		}
	}

	 
	/**
	 * fulfillment channels : [Amazon, Merchant]
	 * Merchant: all listings are fulfilled by Merchant
	 * Amazon: if there is one listing is fulfilled by Amazon
	 */
	public function updateFulfillmentChannel() {
		$listings = $this->listings;
		if (count($listings) > 0) {
			$fufilledMerchant = $listings->every(function ($value, $key) {
				return strtolower($value) == "merchant";
			});
			if ($fufilledMerchant) {
				$this->fulfillment_channel = 'Merchant';
			} else {
				$this->fulfillment_channel = 'Amazon';
			}
		} else {
			$this->status = -1; // no listing, product is removed;
		}
		$this->save();
	}

	public function gerReviewOverview($fromDate, $toDate) {
		$asin = $this->asin;
		$country = $this->country;
		if (empty($fromDate)) {
			$fromDate = (new Carbon('1 month ago'))->format('Y-m-d');
		}
		if (empty($toDate)) {
			$toDate = date('Y-m-d');
		}
		$pro = ProductReviewOverview::select('asin', 'country', 'review_quantity', 'review_rating', 'date')
													->where('asin', $asin)
													->where('country', $country)
													->whereBetween('date', [$fromDate, $toDate])
													->orderBy('date', 'asc')
													->get();
		return $pro;
	}

	public function gerSalesRankHistory($fromDate, $toDate) {
		$asin = $this->asin;
		$country = $this->country;
		if (empty($fromDate)) {
			$fromDate = (new Carbon('1 month ago'))->format('Y-m-d');
		}
		if (empty($toDate)) {
			$toDate = date('Y-m-d');
		}
		$salesRanks = SalesRankUpdate::select('asin', 'country', 'category', 'sales_rank', 'date')
											->where('asin', $asin)
											->where('country', $country)
											->whereBetween('date', [$fromDate, $toDate])
											->orderBy('date', 'asc')
											->get()
											->groupBy('category');
		return $salesRanks;
	}

	public function updateLocalWarehouseQyt() {
		$qty = WarehouseActivity::select(DB::raw('sum(quantity) as qty'))->where('product_id', $this->id)->first();
		$this->local_warehouse_qty = $qty->qty;
		$this->save();
	}

	public function getClaimReviewStatistic($fromDate, $toDate = NULL) {
		if ($toDate == NULL) {
	        $toDate = \Carbon\Carbon::now()->format('Y-m-d');
		}

		$statistic = [
			'orderCount' => 0,
			'pendingOrderCount' => 0,
			'shippedOrderCount' => 0,
			'returnedOrderCount' => 0,
			'partialReturnedOrderCount' => 0,
			'unshippedOrderCount' => 0,
			'shippingOrderCount' => 0,
			'innerPageOrderCount' => 0,
			'missingShipmentCount' => 0,
			'toClaimReviewCount' => 0,
			'claimReviewSentCount' => 0,
			'claimReviewToSendCount' => 0
		];

		$claimReviewToSendStartDate = NULL;

		$listings = $this->active_listings;
		foreach ($listings as $listing) {
			$claimReviewStatistics = ClaimReviewStatistic::where([
				['seller_id', '=', $listing->seller_id],
				['asin', '=', $listing->asin],
				['sku', '=', $listing->sku],
				['country', '=', $listing->country],
				['date', '>=', $fromDate],
				['date', '<=', $toDate]
			])->get();

			$statistic['orderCount'] += DataHelper::aggregate($claimReviewStatistics, 'orders', 'sum');
			$statistic['pendingOrderCount'] += DataHelper::aggregate($claimReviewStatistics, 'pending_orders', 'sum');
			$statistic['shippedOrderCount'] += DataHelper::aggregate($claimReviewStatistics, 'shipped_orders', 'sum');
			$statistic['returnedOrderCount'] += DataHelper::aggregate($claimReviewStatistics, 'returned_orders', 'sum');
			$statistic['partialReturnedOrderCount'] += DataHelper::aggregate($claimReviewStatistics, 'partial_returned_orders', 'sum');
			$statistic['unshippedOrderCount'] += DataHelper::aggregate($claimReviewStatistics, 'unshipped_orders', 'sum');
			$statistic['shippingOrderCount'] += DataHelper::aggregate($claimReviewStatistics, 'shipping_orders', 'sum');
			$statistic['innerPageOrderCount'] += DataHelper::aggregate($claimReviewStatistics, 'inner_page_order_cnt', 'sum');
			$statistic['missingShipmentCount'] += DataHelper::aggregate($claimReviewStatistics, 'missing_shipment_cnt', 'sum');
			$statistic['toClaimReviewCount'] += DataHelper::aggregate($claimReviewStatistics, 'to_claim_review_cnt', 'sum');
			$statistic['claimReviewSentCount'] += DataHelper::aggregate($claimReviewStatistics, 'claim_review_sent_cnt', 'sum');
			$statistic['claimReviewToSendCount'] += DataHelper::aggregate($claimReviewStatistics, 'claim_review_to_send_cnt', 'sum');

			foreach ($claimReviewStatistics as $claimReviewStatistic) {
				if ($claimReviewStatistic->claim_review_to_send_cnt > 0) {
					if ($claimReviewToSendStartDate == NULL) {
						$claimReviewToSendStartDate = \Carbon\Carbon::createFromFormat(
							'Y-m-d', $claimReviewStatistic->date);
					} else {
						$date = \Carbon\Carbon::createFromFormat(
							'Y-m-d', $claimReviewStatistic->date);
						if ($claimReviewToSendStartDate->gt($date)) {
							$claimReviewToSendStartDate = $date;
						}
					}
				}
			}
		}
		if ($claimReviewToSendStartDate) {
			$statistic['claimReviewToSendStartDate'] = $claimReviewToSendStartDate->format('Y-m-d');
		} else {
			$statistic['claimReviewToSendStartDate'] = NULL;
		}

		return $statistic;
	}

	public static function updateFulfillmentChannelForAll() {
		$products = Product::all();
		foreach ($products as $product) {
			$product->updateFulfillmentChannel();
		}
	}

	public static function getInviteReviewProducts() {
		return self::select('id', 'asin', 'country', 'brand', 'should_invite_review')->where('should_invite_review', true)->get();
	}

	public static function getProducts($params) {
	}

	public static function getProductsWithoutCost() {
		return self::where(function($query) {
			$query->where('cost', 0)->orWhereNull('cost');
		})->where(function($query) {
			$query->where('shipping_cost', 0)->orWhereNull('shipping_cost');
		})->where(function($query) {
			$query->where('fbm_shipping_cost', 0)->orWhereNull('fbm_shipping_cost');
		})->where([
			['status', '=', 1],
			['warehouse_qty', '>=', 1]
		])->get();
	}

	public static function getProductsByMarketplace() {
		$listings = Listing::where([
			['status', '>', 0]
		])->get();
		return DataHelper::groupByVal($listings, function($listing) {
			return sprintf('%s-%s', $listing->country, $listing->asin);
		});
	}

	public static function getProductsNeedClaimReview() {
		$queryConds = [
            ['discontinued', '=', FALSE],
            ['status', '>', 0],
            ['should_claim_review', '>', 0]
        ];

        return self::where($queryConds)->get();
	}

	public function buildClaimReviewStatistics($fromDate, $toDate = NULL) {
		$claimReviewStatistics = [];

		$orders = $this->getOrdersByDate($fromDate, $toDate);

		$groupedOrders = DataHelper::groupByDate(
			$orders, 'PurchaseDate', config('app.timezone'));

		foreach ($groupedOrders as $date => $ordersByDate) {
			Log::debug(array_map(function ($order) {
				return $order->attributesToArray();
			}, $ordersByDate));

			$orderCount = count($ordersByDate);

			$pendingOrderCount = 0;
			$returnedOrderCount = 0;
			$partialReturnedOrderCount = 0;
			$cancelledOrderCount = 0;
			$shippedOrderCount = 0;
			foreach ($ordersByDate as $order) {
				$status = $order->OrderStatus;
				if ($status == 'Pending') {
					$pendingOrderCount += 1;
				} else if ($status == 'Returned') {
					$returnedOrderCount += 1;
				} else if ($status == 'Partial Returned') {
					$partialReturnedOrderCount += 1;
				} else if ($status == 'Cancelled') {
					$cancelledOrderCount += 1;
				} else if (strtolower($status) == 'shipped') {
					$shippedOrderCount += 1;
				}
			}

			$classifiedOrders = Order::shouldClaimReview($ordersByDate);
			$innerPageOrderCount = count($classifiedOrders['innerPage']);
			$missingShipmentCount = count(
				$classifiedOrders['claimReviewNeeded']['missingShipment']);
			$claimReviewToSendCount = count(
				$classifiedOrders['claimReviewNeeded']['claimReviewToSend']);
			$claimReviewSentCount = count(
				$classifiedOrders['claimReviewNeeded']['claimReviewSent']['normal']);
			$innerPageClaimReviewSentCount = count($classifiedOrders['claimReviewNeeded']['claimReviewSent']['innerPage']);
			$toClaimReviewCount = $missingShipmentCount + $claimReviewToSendCount + $claimReviewSentCount;

			Log::debug(array_map(function ($order) {
				return $order->attributesToArray();
			}, $classifiedOrders['claimReviewNeeded']['missingShipment']));
			$amazonOrderIds = array_map(function ($order) {
				return $order->AmazonOrderId;
			}, $classifiedOrders['claimReviewNeeded']['missingShipment']);
			Log::debug("'" . implode("', '", $amazonOrderIds) . "'");

			$claimReviewStatistics[$date] = (object) [
				'orderCount' => $orderCount,
				'pendingOrderCount' => $pendingOrderCount,
				'shippedOrderCount' => $shippedOrderCount,
				'returnedOrderCount' => $returnedOrderCount,
				'partialReturnedOrderCount' => $partialReturnedOrderCount,
				'cancelledOrderCount' => $cancelledOrderCount,
				'innerPageOrderCount' => $innerPageOrderCount,
				'missingShipmentCount' => $missingShipmentCount,
				'toClaimReviewCount' => $toClaimReviewCount,
				'claimReviewSentCount' => $claimReviewSentCount,
				'innerPageClaimReviewSentCount' => $innerPageClaimReviewSentCount,
				'claimReviewToSendCount' => $claimReviewToSendCount
			];
		}

		Log::debug($claimReviewStatistics);

		return $claimReviewStatistics;
	}

	public function getOrdersByDate($fromDate, $toDate = NULL) {
		$timeZone = config('app.timezone');
		$startDate = \Carbon\Carbon::createFromFormat(
			'Y-m-d', $fromDate, $timeZone)->startOfDay()->setTimezone('UTC');
		$fromDate = $startDate->format('Y-m-d H:i:s');
		$queryCond = [
			['order_items.ASIN', '=', $this->asin],
			['order_items.country', '=', $this->country],
			['orders.PurchaseDate', '>=', $fromDate]
		];
		if ($toDate) {
			$endDate = \Carbon\Carbon::createFromFormat(
				'Y-m-d', $toDate, $timeZone)->endOfDay()->setTimezone('UTC');
			$toDate = $endDate->format('Y-m-d H:i:s');
			$queryCond[] = ['orders.PurchaseDate', '<=', $toDate];
		}

		$fields = [
			'orders.id as order_id', 'order_items.id as order_item_id',
			'orders.seller_id', 'orders.AmazonOrderId',
			'order_items.country', 'order_items.ASIN', 'order_items.SellerSKU',
			'orders.OrderStatus', 'orders.PurchaseDate',
			'orders.SalesChannel', 'orders.FulfillmentChannel',
		];
		$queryBuilder = Order::select($fields)->where($queryCond)
			->join('order_items', 'order_items.AmazonOrderId', '=', 'orders.AmazonOrderId');

		return $queryBuilder->get();
	}

	public static function getClaimReviewStatistics($fromDate, $toDate = NULL) {
		$statistics = [];

		if ($toDate == NULL) {
	        $toDate = \Carbon\Carbon::now()->format('Y-m-d');
		}

		$claimReviewStatistics = ClaimReviewStatistic::where([
			['date', '>=', $fromDate],
			['date', '<=', $toDate]
		])->get();
		$groupedClaimReviewStatistics = DataHelper::groupByVal($claimReviewStatistics, function ($s) {
			return sprintf('%s-%s', $s->country, $s->asin);
		});
		foreach ($groupedClaimReviewStatistics as $k => $crStatisticsByProduct) {
			$statistics[$k] = [
				'orderCount' => 0,
				'pendingOrderCount' => 0,
				'shippedOrderCount' => 0,
				'returnedOrderCount' => 0,
				'partialReturnedOrderCount' => 0,
				'cancelledOrderCount' => 0,
				'innerPageOrderCount' => 0,
				'missingShipmentCount' => 0,
				'toClaimReviewCount' => 0,
				'claimReviewSentCount' => 0,
				'innerPageClaimReviewSentCount' => 0,
				'claimReviewToSendCount' => 0
			];

			$statistics[$k]['orderCount'] += DataHelper::aggregate($crStatisticsByProduct, 'orders', 'sum');
			$statistics[$k]['pendingOrderCount'] += DataHelper::aggregate($crStatisticsByProduct, 'pending_orders', 'sum');
			$statistics[$k]['shippedOrderCount'] += DataHelper::aggregate($crStatisticsByProduct, 'shipped_orders', 'sum');
			$statistics[$k]['returnedOrderCount'] += DataHelper::aggregate($crStatisticsByProduct, 'returned_orders', 'sum');
			$statistics[$k]['partialReturnedOrderCount'] += DataHelper::aggregate($crStatisticsByProduct, 'partial_returned_orders', 'sum');
			$statistics[$k]['cancelledOrderCount'] += DataHelper::aggregate($crStatisticsByProduct, 'cancelledOrderCount', 'sum');
			$statistics[$k]['innerPageOrderCount'] += DataHelper::aggregate($crStatisticsByProduct, 'inner_page_orders', 'sum');
			$statistics[$k]['missingShipmentCount'] += DataHelper::aggregate($crStatisticsByProduct, 'missing_shipment_cnt', 'sum');
			$statistics[$k]['toClaimReviewCount'] += DataHelper::aggregate($crStatisticsByProduct, 'to_claim_review_cnt', 'sum');
			$statistics[$k]['claimReviewSentCount'] += DataHelper::aggregate($crStatisticsByProduct, 'claim_review_sent_cnt', 'sum');
			$statistics[$k]['innerPageClaimReviewSentCount'] += DataHelper::aggregate($crStatisticsByProduct, 'inner_page_claim_review_sent_cnt', 'sum');
			$statistics[$k]['claimReviewToSendCount'] += DataHelper::aggregate($crStatisticsByProduct, 'claim_review_to_send_cnt', 'sum');

			$claimReviewToSendStartDate = NULL;
			foreach ($crStatisticsByProduct as $claimReviewStatistic) {
				if ($claimReviewStatistic->claim_review_to_send_cnt > 0) {
					if ($claimReviewToSendStartDate == NULL) {
						$claimReviewToSendStartDate = \Carbon\Carbon::createFromFormat(
							'Y-m-d', $claimReviewStatistic->date);
					} else {
						$date = \Carbon\Carbon::createFromFormat(
							'Y-m-d', $claimReviewStatistic->date);
						if ($claimReviewToSendStartDate->gt($date)) {
							$claimReviewToSendStartDate = $date;
						}
					}
				}
			}

			if ($claimReviewToSendStartDate) {
				$statistics[$k]['claimReviewToSendStartDate'] = $claimReviewToSendStartDate->format('Y-m-d');
			} else {
				$statistics[$k]['claimReviewToSendStartDate'] = NULL;
			}
		}

		return $statistics;
	}

	public static function getDefaultClaimReviewStatistic() {
		return [
			'orderCount' => 0,
			'pendingOrderCount' => 0,
			'shippedOrderCount' => 0,
			'returnedOrderCount' => 0,
			'partialReturnedOrderCount' => 0,
			'cancelledOrderCount' => 0,
			'innerPageOrderCount' => 0,
			'missingShipmentCount' => 0,
			'toClaimReviewCount' => 0,
			'claimReviewSentCount' => 0,
			'innerPageClaimReviewSentCount' => 0,
			'claimReviewToSendCount' => 0,
			'claimReviewToSendStartDate' => NULL
		];
	}

	public static function boot()
  {
		parent::boot();

		self::saving(function($model) {
			if ($model->isDirty('warehouse_qty') || $model->isDirty('last_30_in_stock_days') || $model->isDirty('last_30_days_orders')) {
				if ($model->last_30_days_avg_orders > 0) {
					$model->est_stock_days = max(0,ceil($model->warehouse_qty) / $model->last_30_days_avg_orders);
				} else {
					$model->est_stock_days = 0;
				}
			}

			if ($model->isDirty('last_30_in_stock_days') || $model->isDirty('last_30_days_orders')) {
				$model->daily_ave_orders = $model->last_30_days_avg_orders;
			}
		});

	
	}

}
