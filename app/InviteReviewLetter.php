<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InviteReviewLetter extends Model
{
    protected $fillable = [
        'redeem_id', 
        'invite_review_letter_template_id', 
        'subject', 
        'content',
        'buyer_id',
        'products'
    ];
}
