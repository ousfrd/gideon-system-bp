<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use App\Shipment;
use App\Helpers\StateHelper;
use Carbon\Carbon;

class Customer extends Model
{
    protected $fillable = ['amazon_buyer_email'];

    public function orderShipments() {
        return $this->hasMany('App\Shipment', 'buyer_email', 'amazon_buyer_email');
    }

    public function customerNotes() {
        return $this->hasMany('App\CustomerNote');
    }

    public static function allCustomersQuery($country = null) { 
        $query = self::with('orderShipments')->select('*');
        if ($country) { $query->where('country', $country); }
        return $query;
    }

    public static function customersCountByCountry() {
        $query = self::select(DB::raw('country, COUNT(*) as amount'))->groupBy('country')->orderBy('amount', 'desc');
        return $query->get()->keyBy('country')->map(function($record) {return $record->amount; });
    }

    public static function stateCustomersCountByCountry($country) {
        $query = self::select(DB::raw('state, COUNT(*) as amount'))
                        ->where('country', $country)
                        ->groupBy('state')
                        ->orderBy('amount', 'desc');
        return $query->get();
    }

    public static function totalCustomersCount($country = null) {
        $query = self::select(DB::raw('COUNT(*) as amount'));
        if ($country) { $query->where('country', $country); }
        return $query->get()->first()->amount;
    }

    public static function repeatBuyersCount($country = null) {
        $query = self::select(DB::raw('COUNT(*) as amount'))
                        ->where('bought_orders_count', '>', '1');
        if ($country) { $query->where('country', $country); }
        return $query->get()->first()->amount;
    }

    public static function realEmailsCount($country = null) {
        $query = self::select(DB::raw('COUNT(*) as amount'))
                    ->whereNotNull('email');
        if ($country) { $query->where('country', $country); }
        return $query->get()->first()->amount;
    }

    public static function repeatBuyersQuery($country = null) {
        $query = self::with(array('orderShipments'=>function($query){
                    $query->select('buyer_email', 'asin', 'product_name', 'seller_id', 'purchase_date');
                }))->select('customers.*')->where('bought_orders_count', '>', '1');
        if ($country) { $query->where('country', $country); }
        return $query;
    }

    public static function realEmailBuyersQuery($country = null) {
        $query = self::with(array('orderShipments'=>function($query){
            $query->select('buyer_email', 'asin', 'product_name', 'seller_id', 'purchase_date');
        }))->select('customers.*')->whereNotNull('email');
        if ($country) { $query->where('country', $country); }
        return $query;
    }

    public static function positiveReviewersCount($country = null) {
        $query = self::select(DB::raw('COUNT(*) as amount'))
                    ->where('leave_possitive_reviews_count', '>', 0);
        if ($country) { $query->where('country', $country); }
        return $query->get()->first()->amount;
    }

    public static function positiveReviewerQuery($country = null) {
        $query = self::with(array('orderShipments'=>function($query){
                                $query->select('buyer_email', 'asin', 'product_name', 'seller_id', 'purchase_date');
                            }))->select('customers.*')->where('leave_possitive_reviews_count', '>', 0);
        if ($country) { $query->where('country', $country); }
        return $query;
    }

    public static function negativeReviewersCount($country = null) {
        $query = self::select(DB::raw('COUNT(*) as amount'))
                    ->where('leave_negative_reviews_count', '>', 0);
        if ($country) { $query->where('country', $country); }
        return $query->get()->first()->amount;
    }

    public static function negativeReviewersQuery($country = null) {
        $query = self::with(array('orderShipments'=>function($query){
            $query->select('buyer_email', 'asin', 'product_name', 'seller_id', 'purchase_date');
        }))->select('customers.*')->where('leave_negative_reviews_count', '>', 0);
        if ($country) { $query->where('country', $country); }
        return $query;
    }

    public static function buildForRecent3Month() {
        $sinceDate = (new Carbon('3 months ago'))->format('Y-m-d');
        self::build($sinceDate);
    }

    public static function buildForRecent1Week() {
        $sinceDate = (new Carbon('1 week ago'))->format('Y-m-d');
        self::build($sinceDate);
    }

    public static function build($sinceDate = null) {
        $orderShipmentOffset = 0;
        $orderShipmentLimit = 1000;
        while (true) {
            $query = Shipment::with(['order_item_marketing', 'managedReviewOrder']);
            if ($sinceDate) {
                $query->where('purchase_date', '>=', $sinceDate);
            }
            $query->offset($orderShipmentOffset)->limit($orderShipmentLimit);
            $orderShipments = $query->get();

            foreach ($orderShipments as $orderShipment) {
                $orderId = $orderShipment->amazon_order_id;
                $customer = Customer::firstOrNew(['amazon_buyer_email' => $orderShipment->buyer_email]);
                if ($orderShipment->isSelfBought()) {
                    $customer->delete();
                    continue;
                } else {
                    $customer->city = trim($orderShipment->bill_city ? $orderShipment->bill_city : $orderShipment->ship_city);
                    $customer->state = trim($orderShipment->bill_state ? $orderShipment->bill_state : $orderShipment->ship_state);
                    $customer->postal_code = trim($orderShipment->bill_postal_code ? $orderShipment->bill_postal_code : $orderShipment->ship_postal_code);
                    $customer->country = trim($orderShipment->bill_country ? $orderShipment->bill_country : $orderShipment->ship_country);
                    $customer->addRecordToArrayFields('bought_orders', $orderId);
                    $customer->name = $orderShipment->buyer_name;
                    if ($orderShipment->is_refunded) {
                        $customer->addRecordToArrayFields('refunded_orders', $orderId);
                    }
                    $oim = $orderShipment->order_item_marketing;
                    $redeem = $orderShipment->redeem;
                    if ($oim) {
                        $customer->email = $oim->customer_real_email;
                        $customer->facebook_account = $oim->customer_facebook_account;
                        $customer->wechat_account = $oim->customer_wechat_account;
                        if ($oim->claim_review_id && $oim->processed) {
                            $customer->addRecordToArrayFields('claim_reviews', $oim->claim_review_id);
                        }
                        if ($oim->invite_review) {
                            $customer->addRecordToArrayFields('invite_reviews', $oim->claim_review_id);
                        }
                    }
                    if ($redeem) {
                        if ($redeem->edited_email) {
                            $customer->email = $redeem->edited_email;
                        } else {
                            $customer->email = $redeem->email;
                        }
                        $customer->addRecordToArrayFields('redeems', $redeem->id);
                        if ($redeem->review_link) {
                            $customer->addRecordToArrayFields('leave_possitive_reviews', $redeem->id);
                        } else {
                            if ($redeem->star <= 3) {
                                $customer->addRecordToArrayFields('leave_negative_reviews', $redeem->id);
                            }
                        }
                    }
                    $customer->save();
                }
                
            }
            if (count($orderShipments) < $orderShipmentLimit) {
                break;
            } else {
                $orderShipmentOffset += $orderShipmentLimit;
            }
        }
            
    }

    public function addRecordToArrayFields($field, $value) {
        $arr = [];
        if ($this->$field) {
            $arr = json_decode($this->$field);
            if (!in_array( $value, $arr)) {
                $arr[] = $value;
            }
        } else {
            $arr = [$value];
        }
        $this->$field = json_encode($arr);
    }

    /**
     * $filters [
     *  sellers: [array],
     *  asins: [array],
     *  is_repeated: [boolean],
     *  is_positive_reviewer: [boolean],
     *  is_negative_reviewer: [boolean],
     *  first_purchase_date: [date]
     * ]
     * example:
     *  $fitlers = [
     *         "asins" => ['B07BJCMK6W', 'B07ZWG4V7N'],
     *         "is_repeated" => true,
     *         "is_positive_reviewer" => true,
     *         "is_negative_reviewer" => "2019-12-01"
     * ]
     *  Customer::getCustomers
     */
    public static function getCustomers($filters) {
        $query = self::select('customers.*', 'order_shipments.amazon_order_id', 'order_shipments.purchase_date', 'order_shipments.asin', 'order_shipments.seller_id', 'order_shipments.product_name')
                        ->join('order_shipments', 'customers.amazon_buyer_email', '=', 'order_shipments.buyer_email');
        if (array_key_exists('sellers', $filters) && !empty($filters['sellers'])) {
            $query->whereIn('order_shipments.seller_id', $filters['sellers']);
        }
        if (array_key_exists('asins', $filters) && !empty($filters['asins'])) {
            $query->whereIn('order_shipments.asin', $filters['asins']);
        } 
        if (array_key_exists('excludes_refund_orders', $filters) && $filters['excludes_refund_orders']) {
            $compareQuantity = 0;
            if (array_key_exists('is_repeated', $filters) && $filters['is_repeated']) {
                $compareQuantity = 1;
            }
            $query->whereRaw('customers.bought_orders_count - customers.refunded_orders_count > ?', [$compareQuantity]);
        } else {
            if (array_key_exists('is_repeated', $filters) && $filters['is_repeated']) {
                $query->where('customers.bought_orders_count', '>', 1);
            }
        }
        if (array_key_exists('is_positive_reviewer', $filters) && $filters['is_positive_reviewer']) {
            $query->where('customers.leave_possitive_reviews_count', '>', 0);
        }
        if (array_key_exists('is_negative_reviewer', $filters) && $filters['is_negative_reviewer']) {
            $query->where('customers.leave_negative_reviews_count', '>', 0);
        }
        if (array_key_exists('first_purchase_date', $filters) && $filters['first_purchase_date']) {
            $query->where('order_shipments.purchase_date', '>', $filters['first_purchase_date']);
        }
        if (array_key_exists('has_real_email', $filters) && $filters['has_real_email']) {
            $query->whereNotNull('customers.email');
        }
        if (array_key_exists('has_wechat_account', $filters) && $filters['has_wechat_account']) {
            $query->whereNotNull('customers.wechat_account');
        }
        if (array_key_exists('has_facebook_account', $filters) && $filters['has_facebook_account']) {
            $query->whereNotNull('customers.facebook_account');
        }
        $query->groupBy('customers.amazon_buyer_email');
        
        return $query->get();
    }

    public static function getBlockListedEmails() {
        return self::select('amazon_buyer_email')
            ->where('blocklisted', TRUE)->get()->pluck('amazon_buyer_email')->all();
    }

    public static function boot()
    {
        parent::boot();

        self::saving(function($model) {
            if ($model->country == "US") {
                $model->state = StateHelper::toAbrev($model->state);
            }
            if ($model->isDirty('bought_orders')) {
                $items = json_decode($model->bought_orders);
                $model->bought_orders_count = count($items);
            }
            if ($model->isDirty('refunded_orders')) {
                $items = json_decode($model->refunded_orders);
                $model->refunded_orders_count = count($items);
            }
            if ($model->isDirty('leave_reviews')) {
                $items = json_decode($model->leave_reviews);
                $model->leave_reviews_count = count($items);
            }
            if ($model->isDirty('leave_possitive_reviews')) {
                $items = json_decode($model->leave_possitive_reviews);
                $model->leave_possitive_reviews_count = count($items);
            }
            if ($model->isDirty('leave_negative_reviews')) {
                $items = json_decode($model->leave_negative_reviews);
                $model->leave_negative_reviews_count = count($items);
            }
            if ($model->isDirty('claim_reviews')) {
                $items = json_decode($model->claim_reviews);
                $model->claim_reviews_count = count($items);
            }
            if ($model->isDirty('invite_reviews')) {
                $items = json_decode($model->invite_reviews);
                $model->invite_reviews_count = count($items);
            }
            if ($model->isDirty('redeems')) {
                $items = json_decode($model->redeems);
                $model->redeems_count = count($items);
            }
        });
    }
}
