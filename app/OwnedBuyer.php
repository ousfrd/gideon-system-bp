<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Listing;

class OwnedBuyer extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'password', 'auxiliary_email', 'device_name', 'prime_purchased_at', 'prime_end_at', 'nickname', 'browser', 'status'];

    public function user() 
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function shippingAddresses()
    {
        return $this->hasMany('App\OwnedBuyerShippingAddress');
    }

    public function paymentMethods()
    {
        return $this->hasMany('App\OwnedBuyerPaymentMethod');
    }

    public function addedToCartItems()
    {
        return $this->hasMany('App\OwnedBuyerCartItem');
    }

    public function managedReviewOrders()
    {
        return $this->hasMany('App\ManagedReviewOrder');
    }

    public function updateReviewInfo() 
    {
        $orders = $this->managedReviewOrders;
        $orderCount = count($orders);
        if ($orderCount > 0) {
            $reviewCount = 0;
            $lastReviewAt = null;
            foreach ($orders as $order) {
                if ($order->review_link && $order->left_review_at) {  
                    if (empty($lastReviewAt) || $order->left_review_at > $lastReviewAt) {
                        $lastReviewAt = $order->left_review_at;
                    }
                    $reviewCount++;
                }
                
            }
            $this->last_left_review_at = $lastReviewAt;
            $this->reviewed_orders = $reviewCount;
            $this->save();
        }
    }

    public static function updateReviewInfoAll() {
        $buyers = self::all();
        foreach ($buyers as $buyer) {
            $buyer->updateReviewInfo();
        }
    }

    public function updateOrderStatistics() {
        $orders = $this->managedReviewOrders;
        $orderCount = count($orders);
        $selfOrderCount = 0;
        $otherOrderCount = 0;
        if ($orderCount > 0) {
            foreach ($orders as $order) {
                if ($order->order_type == ManagedReviewOrder::OWNED_BUYER_SELF_ORDER) {
                    $selfOrderCount++;
                }
                if ($order->order_type == ManagedReviewOrder::OWNED_BUYER_OTHER_ORDER) {
                    $otherOrderCount++;
                }
            }
        }
        $this->own_orders = $selfOrderCount;
        $this->other_orders = $otherOrderCount;
        $this->save();
    }

    public static function updateOrderStatisticsForAll() {
        $buyers = OwnedBuyer::all();
        foreach ($buyers as $buyer) {
            $buyer->updateOrderStatistics();
        }
    }

    public static function getGridQuery() {
        $query = self::select("owned_buyers.*", "owned_buyer_payment_methods.card_number", "owned_buyer_payment_methods.available_balance", "owned_buyer_payment_methods.total_spent", "users.name as user_name")
                        ->leftJoin("owned_buyer_payment_methods", "owned_buyers.id", "=" , "owned_buyer_payment_methods.owned_buyer_id")
                        ->join("users", "users.id", "=", "owned_buyers.created_by");
        return $query;
    }

    public static function primeGridQuery() {
        $query = self::getGridQuery();
        $query->where('owned_buyers.is_prime', true);
        return $query;
    }

    public static function newBuyersGridQuery() {
        $query = self::getGridQuery();
        $query->whereNull('owned_buyers.last_placed_order_at');
        return $query;
    }

    public static function getEligibleBuyersForProductQuery($asin, $lastOrderBefore, $lastReviewBefore, $reviewRateLessThan = 1)
    {
        $sellerIdsSellingIt = Listing::where("status", 1)->where("asin", $asin)->pluck('seller_id')->toArray();
        $sellersOtherProductAsins = [];
        foreach ($sellerIdsSellingIt as $sellerId) {
            $productAsins = Listing::where('seller_id', $sellerId)->pluck('asin')->toArray();
            $sellersOtherProductAsins = array_merge($sellersOtherProductAsins, $productAsins);
        }
        $asins = array_unique($sellersOtherProductAsins);
        $buyerIds = ManagedReviewOrder::whereIn('asin', $asins)->whereNotNull('left_review_at')->pluck('owned_buyer_id')->toArray();
        $query = OwnedBuyer::select('owned_buyers.*', "owned_buyer_payment_methods.card_number", "owned_buyer_payment_methods.available_balance", "owned_buyer_payment_methods.total_spent", "users.name as user_name")
                    ->join('owned_buyer_payment_methods', 'owned_buyer_payment_methods.owned_buyer_id', '=', 'owned_buyers.id')
                    ->join("users", "users.id", "=", "owned_buyers.created_by")
                    ->whereNull('owned_buyers.not_synced_order_id')                   
                    ->whereNotIn('owned_buyers.id', $buyerIds)
                    ->where('owned_buyers.status', '=', 1)
                    ->where('owned_buyers.cart_items_count', '=', 0)
                    ->where(function($q) use($lastOrderBefore) {
                        $q->whereNull('owned_buyers.last_placed_order_at')
                            ->orWhere('owned_buyers.last_placed_order_at', '<', $lastOrderBefore);   
                    })
                    ->where(function($q) use($lastReviewBefore) {
                        $q->whereNull('owned_buyers.last_left_review_at')
                            ->orWhere('owned_buyers.last_left_review_at', '<', $lastReviewBefore);   
                    })
                    ->where('review_rate', '<=', $reviewRateLessThan)
                    ->whereRaw('LENGTH(owned_buyer_payment_methods.card_number) > ?', 0)
                    ->where('owned_buyer_payment_methods.available_balance', '>', 0);
        return $query;
    }

    public static function updatePrimeStatus($offset = 0, $limit = 100) {
        $buyers = OwnedBuyer::select('id', 'is_prime', 'prime_end_at')->offset($offset)->limit($limit)->get();
        $today = new Carbon;
        foreach ($buyers as $buyer) {
            if ($buyer->prime_end_at && $buyer->prime_end_at >= $today) {
                $buyer->is_prime = true;
            } else {
                $buyer->is_prime = false;
            }
            $buyer->save();
        }
        if (count($buyers) == $limit) {
            OwnedBuyer::updatePrimeStatus($offset + $limit, $limit);
        }
    }

    public static function getDevices($createdBy = null) {
        $query = OwnedBuyer::select('device_name');
        if ($createdBy) {
            $query->where('created_by', $createdBy);
        }
        $devices = $query->distinct()->pluck('device_name')->toArray();
        return array_combine($devices, $devices);
    }

    public static function boot()
    {
        parent::boot();

        self::saving(function($model) {
            $today = new Carbon;
            if ($model->prime_end_at && $today < $model->prime_end_at) {
                $model->is_prime = true;
            } else {
                $model->is_prime = false;
            }
            if (empty($model->not_synced_order_id)) {
                $model->not_synced_order_id = null;
            }
            if ($model->isDirty('own_orders') || $model->isDirty('other_orders')) {
                $totalOrder = $model->own_orders + $model->other_orders;
                $model->total_orders = $totalOrder;
                if ($totalOrder != 0) {
                    $model->own_other_orders_rate = $model->own_orders / $totalOrder;
                } else {
                    $model->own_other_orders_rate = 0;
                }
                
            }
            if ($model->isDirty('reviewed_orders') || $model->isDirty('total_orders')) {
                $totalOrder = $model->total_orders;
                if ($totalOrder != 0) {
                    $model->review_rate = $model->reviewed_orders/$totalOrder;
                } else {
                    $model->review_rate = 0;
                }
            }
        });

        self::deleting(function($model) {
            $model->shippingAddresses->each->delete();
            $model->paymentMethods->each->delete();
            $model->addedToCartItems->each->delete();
            $model->managedReviewOrders->each->delete();
        });
    }

}
