<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Product;
use App\Listing;
use App\OwnedBuyer;
use App\Redeem;
use App\OwnedBuyerPaymentMethod;
use App\ClickSendReturnAddress;
use App\ClickSendHistory;
use App\Customer;
use App\OrderItem;
use App\Shipment;
use App\OrderItemMarketing;
use App\Import;
use App\Review;
use App\ProductStatistic;
use App\ProductInfoService;
use App\ClaimReviewStatistic;
use App\Jobs\ImportShipStationOrder;
use App\Helpers\ProductHelper;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
    		Commands\Order::class,
    		Commands\Email::class,
            Commands\MailMan::class,
            Commands\Tracking::class,
            Commands\PPC::class,
            Commands\AsinWatcher::class,
            Commands\JobsMaker::class,
            Commands\DispatchShipStationOrderImportJob::class,
            Commands\DispatchListingImportJob::class,
            Commands\DispatchInventoryImportJob::class,
            Commands\DispatchOrderShipmentImportJob::class,
            Commands\DispatchOrderImportJob::class,
            Commands\DispatchAdReportImportJob::class,
            Commands\DeleteWrongOrders::class,
            Commands\DeleteDsListing::class,
            Commands\SyncProductCosts::class,
            Commands\MissingListingOrderRemover::class,
            Commands\SyncClaimReviewReturnAddress::class,
            Commands\ClaimReviewAudit::class,
            Commands\OrdersWithoutAddress::class,
            Commands\OrderFeeUpdater::class,
            Commands\ProductPerformanceExporter::class,
            Commands\ProductClaimReviewStatistic::class,
            Commands\ClaimReviewStatisticBuilder::class,
            Commands\ClaimReviewSentStatusUpdater::class,
            Commands\ClaimReviewSyncMailAddress::class,
            Commands\RemovedListingClear::class,
            Commands\ProductOverviewExporter::class,
            Commands\ProductOverviewBuilder::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        //     if(config('app.use_mailman_service')) {
        //         $schedule->command('mailman:send greeting')->cron('*/15 * * * *')->withoutOverlapping();
        //         $schedule->command('mailman:send delivered')->cron('5 8 * * *')->withoutOverlapping();
        //         $schedule->command('mailman:send product_review')->cron('5 8 * * *')->withoutOverlapping();
        //         $schedule->command('mailman:send product_review_history')->cron('5 18 * * *')->withoutOverlapping();
        //     }

        //     if(config('app.use_advertising_api')) {
        //         $schedule->command('ppc:manager requestProductAdsReportYesterday')->cron('10 */4 * * *')->withoutOverlapping();
        //         $schedule->command('ppc:manager getProductAdsReportYesterday')->cron('50 */4 * * *')->withoutOverlapping();

        //         $schedule->command('ppc:manager requestProductAdsReport3days')->cron('10 4 * * *')->withoutOverlapping();
        //         $schedule->command('ppc:manager getProductAdsReport3days')->cron('50 4 * * *')->withoutOverlapping();

        //         $schedule->command('ppc:manager requestProductAdsReport7days')->cron('10 3 * * *')->withoutOverlapping();
        //         $schedule->command('ppc:manager getProductAdsReport7days')->cron('50 3 * * *')->withoutOverlapping();

        //         $schedule->command('ppc:manager updateProductAds')->cron('15 * * * *')->withoutOverlapping();
        //         $schedule->command('ppc:manager runAdsRules')->cron('* * * * *')->withoutOverlapping();

        //     }

        //     if(config('app.use_asin_watcher') && config('app.group_chat_id')) {
        //         $schedule->command('asin:watcher')->everyThirtyMinutes()->appendOutputTo(storage_path('logs/eagle.log'));
        //     }

        //     // calculate last 7 days daily profit data every hour 
        $schedule->command('order daily_profit_last_day')->cron("17 */3 * * *");
        $schedule->command('order daily_profit_7_days')->cron("12 */5 * * *");
        $schedule->command('order daily_profit_2_months')->dailyAt('01:15');
        $schedule->command('claim_review:sync_return_address')->daily();
        $schedule->command('orders:remove_listing_missing 3')->daily();

        //     $schedule->command('make:jobs imports')->cron('* * * * *')->withoutOverlapping();
        $schedule->call(function () {
            Product::updateLifetimeStatisticsForAll();
        })->hourly();

        $schedule->call(function() {
            Listing::calculateDailyAveOrdersForAll();
        })->dailyAt('01:00');

        $schedule->call(function() {
            Product::updateStatusForAll();
        })->dailyAt("02:00");

        $schedule->call(function() {
            Listing::UpdateSalesPriceForAll();
        })->weeklyOn(1, '2:40');

        $schedule->call(function() {
            Product::calculateOrdersQuantityForAll();
        })->cron("10 */3 * * *");

        $schedule->call(function() {
            Product::fillFirstOrderDate();
        })->dailyAt("12:30");

        $schedule->call(function() {
            Product::updateFulfillmentChannelForAll();
        })->dailyAt("18:30");

        $schedule->call(function() {
            Listing::calculateOrdersQuantityForAll();
        })->cron("30 */3 * * *");

        $schedule->call(function() {
            OwnedBuyer::updatePrimeStatus();
        })->dailyAt("00:30");

        $schedule->call(function() {
            Redeem::deleteTestRecord();
        })->cron("10 */6 * * *");

        $schedule->call(function() {
            OwnedBuyerPaymentMethod::recalculateAvailableBalance();
        })->monthlyOn(1, '2:00');
        
        // $schedule->call(function() {
        //     ClickSendReturnAddress::sync();
        // })->dailyAt("01:20");
        
        $schedule->call(function() {
            ClickSendHistory::sync();
        })->dailyAt("03:20");

        $schedule->call(function() {
            Customer::buildForRecent1Week();
        })->dailyAt("01:00");

        $schedule->call(function() {
            Customer::buildForRecent3Month();
        })->cron('7 7 7 */2 *');

        $schedule->call(function() {
            OrderItem::updateOrderRefundStatusFor3Months();
        })->dailyAt("04:00");

        $schedule->call(function() {
            Shipment::updateShipmentFieldsFor3Months();
        })->dailyAt("04:00");

        $schedule->call(function() {
            OrderItemMarketing::syncAll();
        })->dailyAt("00:50");

        $schedule->call(function() {
            Import::deleteFiles();
        })->dailyAt("20:00");

        $schedule->call(function() {
            Review::updateCategoryForAll();
        })->dailyAt("22:50");

        $schedule->call(function() {
            ProductStatistic::buildStatisticsForLast2Days();
        })->hourly();

        $schedule->call(function() {
            ProductStatistic::buildStatisticsForLastWeek();
        })->weeklyOn(5, '21:00');

        $schedule->call(function() {
            ProductStatistic::buildStatisticsForLastMonth();
        })->monthlyOn(15, '23:00');

        $schedule->call(function() {
            ProductInfoService::requestInfoForAllProducts();
            ProductInfoService::requestRatingForAllProducts();
        })->dailyAt("12:00");

        $schedule->call(function() {
            ProductInfoService::getInfoForAllProducts();
            ProductInfoService::getRatingForAllProducts();
        })->dailyAt("1:30");

        $schedule->call(function() {
            ClaimReviewStatistic::buildStatisticsForRecent30Days();
        })->dailyAt("1:11");

        $schedule->call(function() {
            ProductHelper::exportProductOverviews();
        })->dailyAt("7:57");

        // $schedule->job(new ImportShipStationOrder(), 'shipstation')->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
