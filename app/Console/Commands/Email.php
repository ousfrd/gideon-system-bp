<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\OrderItem;
use App\Product;
use App\User;

class Email extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$user = $this->argument('user');
    	
    	if(empty($user)) {
    		$users = User::where('verified',1)->get();
    		foreach ($users as $user) {
    			$this->sendToUser($user);
    		}
    	} else {
    		$user = User::where('id',$user)->firstOrFail();
            if($user->email_notice) {
                $this->sendToUser($user);
            }
    		
    	}
    	
    	
    }
    
    public function sendToUser(User $user) {
    	
    	
    	$this->info('Sending notification emails to '.$user->name);
    	
    	$user->sendNotificationEmails();
    	
    	
    	$this->info('Done sending notification emails to '.$user->name);
    }
}
