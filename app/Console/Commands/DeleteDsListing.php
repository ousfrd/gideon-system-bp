<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;
use App\Listing;
use App\Product;
use App\Brand;
use App\Order;
use App\OrderItem;
use App\ProductStatistic;
use App\Helpers\ListingReportImporter;

class DeleteDsListing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'listing:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete DS listings.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $listings = Listing::all();
        foreach ($listings as $listing) {
            $dsListing = ListingReportImporter::isDsListing($listing->sku);
            if (!$dsListing) {
                continue;
            }

            $message = sprintf(
                '[DSListingFound] ASIN: %s, SKU: %s',
                $listing->asin, $listing->sku
            );
            Log::info($message);

            $listing->remove();
        }
    }
}
