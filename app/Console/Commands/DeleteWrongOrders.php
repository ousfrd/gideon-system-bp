<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Order;
use App\Account;
use App\Product;
use App\Listing;
use App\ProductStatistic;

class DeleteWrongOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:clean {order_report} {--seller=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete mistakenly imported orders.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sellerId = $this->option('seller');
        $orderReportPath = realpath($this->argument('order_report'));
        if (!$sellerId) {
            $this->error('SellerID is required to delete mistakenly imported orders.');
            return;
        }

        if (!$orderReportPath) {
            $this->error('Order report is required to delete mistakenly imported orders.');
            return;
        }

        $dateFormat = 'Y-m-d\TH:i:s.u';
        $fromDate = $toDate = NULL;
        $delimiter = "\t";
        $headers = NULL;
        foreach (file($orderReportPath, FILE_IGNORE_NEW_LINES) as $line) {
            $dataArr = str_getcsv($line, $delimiter);
            if (!$dataArr) {
                continue;
            }

            if ($headers === NULL) {
                $headers = $dataArr;
                continue;
            }

            $record = array_combine($headers, $dataArr);
            if (!$record) {
                $this->error('[IncompleteOrderRecord] ' . implode(',', $dataArr));
                continue;
            }

            $purchaseDate = \DateTime::createFromFormat(
                \DateTimeInterface::ISO8601, $record['purchase-date']);
            if ($fromDate === NULL || $fromDate > $purchaseDate) {
                $fromDate = $purchaseDate;
            }
            if ($toDate === NULL || $toDate < $purchaseDate) {
                $toDate = $purchaseDate;
            }

            $amazonOrderId = $record['amazon-order-id'];
            Order::where([
                ['seller_id', '=', $sellerId],
                ['AmazonOrderId', '=', $amazonOrderId]
            ])->delete();

            $message = sprintf(
                '[OrderDeleted] SellerID: %s, AmazonOrderId: %s',
                $sellerId, $amazonOrderId
            );
            $this->info($message);
        }

        if ($fromDate && $toDate) {
            $this->info('Updating Seller Statistics...');

            $account = Account::where('seller_id', '=', $sellerId)->first();
            if ($account) {
                ProductStatistic::buildStatisticsForSeller(
                    $account, $fromDate->format($dateFormat), $toDate->format($dateFormat));
            }
            ProductStatistic::buildStatisticsForLast2Days();
            Product::calculateOrdersQuantityForAll();
            Listing::calculateOrdersQuantityForAll();

            $this->info('Seller Statistics Updated.');
        }
    }
}
