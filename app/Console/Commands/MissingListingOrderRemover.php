<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Account;
use App\Order;
use App\OrderItem;
use App\Listing;
use App\Helpers\DataHelper;

class MissingListingOrderRemover extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:remove_listing_missing {days=30} {--include-removed-listings} {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove orders without listing that imported by incorrect order report.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dryRun = $this->option('dry-run');
        $includeRemovedListings = $this->option('include-removed-listings');
        $days = (int) $this->argument('days');
        $missingListingOrderCnt = 0;

        $fromPurchaseDate = \Carbon\Carbon::now()->subDays($days)->startOfDay();
        $fromPurchaseDateStr = $fromPurchaseDate->format('Y-m-d H:i:s');
        $queryParams = ['purchase_date' => ['start_date' => $fromPurchaseDateStr]];
        $orderItems = Order::getOrders(Order::getQuery($queryParams));
        $groupedOrderItems = DataHelper::groupByVal($orderItems, 'seller_id');
        foreach ($groupedOrderItems as $sellerId => $orderItemsBySeller) {
            $account = Account::getAccountBySellerId($sellerId);
            $listingKeys = [];
            foreach ($account->listings as $listing) {
                if ($listing->status < 0 && !$includeRemovedListings) {
                    continue;
                }

                $listingKeys[$listing->asin . ':' . $listing->sku] = 1;
            }

            $accountIdentifier = sprintf(
                '[%s %s] %s', $account->code, $account->name, $account->seller_id);
            foreach ($orderItemsBySeller as $orderItem) {
                $listingKey = $orderItem->ASIN . ':' . $orderItem->SellerSKU;
                if (!array_key_exists($listingKey, $listingKeys)) {
                    $message = sprintf(
                        '[OrderMissingListing] AmazonOrderId: %s, ASIN: %s, SKU: %s, Seller: %s',
                        $orderItem->AmazonOrderId, $orderItem->ASIN,
                        $orderItem->SellerSKU, $accountIdentifier
                    );
                    $this->error($message);

                    $missingListingOrderCnt++;

                    if (!$dryRun) {
                        $orderId = $orderItem->orderId;
                        $order = Order::find($orderId);
                        if ($order) {
                            $order->delete();
                        } else {
                            $this->error('[OrderMissing] %s', $orderId);
                        }
                        $orderItem->delete();
                    }
                }
            }
        }
        $this->info('[MissingListingOrdersCount] ' . $missingListingOrderCnt);
    }
}
