<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account;
use App\Import;
use App\Helpers\ListingReportImporter;

class DispatchListingImportJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dispatch:listing_import {listing_report} {--S|seller=} {--C|country=US}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Listing Report.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $listingReportPath = $this->argument('listing_report');
        $sellerCode = $this->option('seller');
        $country = $this->option('country');

        if (!is_file($listingReportPath)) {
            $this->error('Could not find listing report - ' . $listingReportPath);
            return;
        }

        $account = Account::where('code', $sellerCode)->first();
        if ($account === NULL) {
            $this->error('Could not find seller - ' . $sellerCode);
            return;
        }

        ListingReportImporter::importListingReport(
            $listingReportPath, $account->seller_id, $country);
    }
}
