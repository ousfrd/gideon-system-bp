<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Listing;


class RemovedListingClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'listing:removed:clear {--S|seller=*} {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear listings marked as removed.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dryRun = $this->option('dry-run');
        $sellerIds = $this->option('seller');

        if ($sellerIds) {
            $listings = Listing::where([
                ['status', '<', 0],
            ])->whereIn('seller_id', $sellerIds)->get();
        } else {
            $listings = Listing::where([['status', '<', 0]])->get();
        }
        foreach ($listings as $listing) {
            if ($dryRun) {
                $message = sprintf(
                    '[DeleteListing] SellerID: %s, ASIN: %s, SKU: %s',
                    $listing->seller_id, $listing->asin, $listing->sku
                );
                $this->info($message);

                continue;
            }

            $listing->remove();
        }
    }
}
