<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;
use App\Order;
use App\OrderItemMarketing;
use App\ClaimReviewCampaign;
use App\Helpers\Click2MailHelper;

class ClaimReviewSentStatusUpdater extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'claim_review:sent_status:update {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update claim review sent status.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dryRun = $this->option('dry-run');

        $c2mInvalidAddressCnt = 0;
        $orderAddressMapping = [];
        $orderMissingShipping = [];
        $oimAlreadySent = [];
        $campaigns = ClaimReviewCampaign::with('orderItemMarketings.orderShipment')
            ->where('platform', 'click2mail')->get();
        foreach ($campaigns as $campaign) {
            $orderItemMarketings = $campaign->orderItemMarketings;
            foreach ($orderItemMarketings as $orderItemMarketing) {
                if ($orderItemMarketing->self_review || $orderItemMarketing->insert_card_review) {
                    continue;
                }

                $orderShipment = $orderItemMarketing->orderShipment;
                if (empty($orderShipment) || empty($orderShipment->ship_address_1)) {
                    continue;
                }

                $address = strtolower($orderShipment->ship_address_1);
                $orderAddressMapping[$address] = $orderItemMarketing->id;
            }
        }

        $client = Click2MailHelper::getClick2MailClient();
        $addressLists = $client->getAddressLists();
        foreach ($addressLists as $addressList) {
            $addressListInfo = $client->getAddressListInfo($addressList->id);
            foreach ($addressListInfo->address as $addressInfo) {
                if (empty($addressInfo->address1)) {
                    continue;
                }

                if (!property_exists($addressInfo, 'address1') || empty($addressInfo->address1)) {
                    Log::warning(json_encode($addressInfo));

                    continue;
                }

                if (!is_string($addressInfo->address1)) {
                    $c2mInvalidAddressCnt += 1;
                    Log::warning(json_encode($addressInfo));

                    continue;
                }

                $addressKey = strtolower($addressInfo->address1);
                if (array_key_exists($addressKey, $orderAddressMapping)) {
                    $oimAlreadySent[] = $orderAddressMapping[$addressKey];
                    unset($orderAddressMapping[$addressKey]);

                    $this->info('[AddressFiltered] ' . $addressInfo->address1);
                } else {
                    $orderMissingShipping[] = $addressInfo->address1;
                    $this->error('[AddressMissing] ' . $addressInfo->address1);
                }
            }
        }

        $this->info('[C2MInvalidAddress] ' . $c2mInvalidAddressCnt);
        $this->info('[AddressAlreadySent] ' . count($oimAlreadySent));
        $this->info('[AddressToResend] ' . count($orderAddressMapping));
        $this->info('[AddressMissing] ' . count($orderMissingShipping));

        if ($dryRun) {
            return;
        }

        foreach ($oimAlreadySent as $oimId) {
            try {
                OrderItemMarketing::where('id', $oimId)->update(
                    ['is_sent' => TRUE]);
            } catch (\Exception $e) {
                Log::warning('[FailedToUpdateOrderItemMarketing] ' . $e->getMessage());
            }
        }

        foreach ($orderAddressMapping as $orderItemMarketingId) {
            try {
                OrderItemMarketing::where('id', $orderItemMarketingId)->update(
                    ['is_sent' => FALSE]);
            } catch (\Exception $e) {
                Log::warning('[FailedToUpdateOrderItemMarketing] ' . $e->getMessage());
            }
        }

        $campaigns = ClaimReviewCampaign::where([
            ['platform', '=', 'click2mail']
        ])->get();
        foreach ($campaigns as $campaign) {
            $orderItemMarketings = $campaign->orderItemMarketings;
            $orderItemMarketingSent = $orderItemMarketings->filter(function ($oim) {
                return $oim->is_sent;
            });
            $campaign->actual_sent_amount = $orderItemMarketingSent->count();
            if ($campaign->actual_sent_amount > 0) {
                $campaign->unit_price = $campaign->total_cost / $campaign->actual_sent_amount;
            } else {
                $campaign->unit_price = 0;
            }
            $campaign->save();
        }
    }
}
