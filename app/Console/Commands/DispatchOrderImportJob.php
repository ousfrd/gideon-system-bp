<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Account;
use App\Helpers\OrderReportImporter;


class DispatchOrderImportJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dispatch:order_import {order_report} {--S|seller=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import order report.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orderReportPath = $this->argument('order_report');
        $sellerCode = $this->option('seller');

        if (!is_file($orderReportPath)) {
            $this->error('Could not find order report - ' . $orderReportPath);
            return;
        }

        $account = Account::where('code', $sellerCode)->first();
        if ($account === NULL) {
            $this->error('Could not find seller - ' . $sellerCode);
            return;
        }

        OrderReportImporter::importOrderReport($orderReportPath, $account->seller_id);
    }
}
