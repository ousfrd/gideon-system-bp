<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Helpers\ProductHelper;


class ProductOverviewExporter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:overview:export {--S|service-account=} {--D|days=1} {sheetId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export product overview to google sheet.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $serviceAccountPath = $this->option('service-account');
        $spreadSheetId = $this->argument('sheetId');
        $days = $this->option('days', 1);

        ProductHelper::exportProductOverviews($serviceAccountPath, $spreadSheetId, $days);
    }
}
