<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ProductStatistic;

class ProductOverviewBuilder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:overview:builder {--D|days=30}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build ProductOverview.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days = $this->option('days', 30);
        $fromDate = \Carbon\Carbon::now()->subDays($days)->startOfDay();
        $toDate = \Carbon\Carbon::now()->endOfDay();
        $fromDateStr = $fromDate->format('Y-m-d');
        $toDateStr = $toDate->format('Y-m-d');

        ProductStatistic::buildProductOverviews($fromDateStr, $toDateStr);
    }
}
