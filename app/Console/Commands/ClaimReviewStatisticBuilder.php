<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ClaimReviewStatistic;

class ClaimReviewStatisticBuilder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'claim_review:statistic {days=30}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build claim review statistics.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days = $this->argument('days') ?? 30;
        $fromDate = \Carbon\Carbon::now()->subDays($days)->startOfDay();
        $toDate = \Carbon\Carbon::now()->endOfDay();
        $fromDateStr = $fromDate->format('Y-m-d');
        $toDateStr = $toDate->format('Y-m-d');

        ClaimReviewStatistic::buildStatistics($fromDateStr, $toDateStr);
    }
}
