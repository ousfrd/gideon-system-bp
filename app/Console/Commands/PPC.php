<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account;
use App\AdReportRequest;
use App\AdCampaign;
use App\AdRule;
use DB;

class PPC extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ppc:manager {type} {date?} {seller_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PL PPC Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $type = $this->argument('type');
        $date = $this->argument('date');
        $seller_id = $this->argument('seller_id');

        $accounts = Account::where('status', 'Active')->where('adv_refresh_token', '<>', NULL)->get();
        // remove 'campaignNegativeKeywords'
        $recordTypes = ['campaigns', 'adGroups', 'keywords', 'negativeKeywords', 'campaignNegativeKeywords', 'productAds'];
        $reportTypes = ['productAds', 'campaigns', 'adGroups', 'keywords', 'searchterms', 'targets'];

        if($type == "requestProductAdsReportYesterday") {

            $yesterday = date('Ymd',strtotime("-1 days"));
            foreach ($accounts as $account) {
                foreach ($reportTypes as $reportType) {
                    $account->adRequestReport($yesterday, $reportType);
                }
            }

        } elseif ($type == "getProductAdsReportYesterday") {

            $yesterday = date('Y-m-d',strtotime("-1 days"));
            foreach ($accounts as $account) {
                foreach ($reportTypes as $reportType) {
                    $account->getAdReportByDate($yesterday, $reportType);
                }
            }

        } if($type == "requestProductAdsReport7days") {

            $date = date('Ymd',strtotime("-7 days"));
            foreach ($accounts as $account) {
                foreach ($reportTypes as $reportType) {
                    $account->adRequestReport($date, $reportType);
                }
            }

        } elseif ($type == "getProductAdsReport7days") {

            $date = date('Y-m-d',strtotime("-7 days"));
            foreach ($accounts as $account) {
                foreach ($reportTypes as $reportType) {
                    $account->getAdReportByDate($date, $reportType);
                }
            }

        } if($type == "requestProductAdsReport3days") {

            $date = date('Ymd',strtotime("-3 days"));
            foreach ($accounts as $account) {
                foreach ($reportTypes as $reportType) {
                    $account->adRequestReport($date, $reportType);
                }
            }

        } elseif ($type == "getProductAdsReport3days") {

            $date = date('Y-m-d',strtotime("-3 days"));
            foreach ($accounts as $account) {
                foreach ($reportTypes as $reportType) {
                    $account->getAdReportByDate($date, $reportType);
                }
            }

        } elseif($type == "requestProductAdsReportByDate") {
            if($date) {
                $date = date("Ymd", strtotime($date));
                foreach ($accounts as $account) {
                    foreach ($reportTypes as $reportType) {
                        $account->adRequestReport($date, $reportType);
                    }
                }
            }
        } elseif($type == "getProductAdsReportByDate") {
            if($date) {
                $date = date("Y-m-d", strtotime($date));
                foreach ($accounts as $account) {
                    foreach ($reportTypes as $reportType) {
                        $account->getAdReportByDate($date, $reportType);
                    }
                }
            }
        } elseif($type == "requestAdSnapshots") {
            foreach ($accounts as $account) {
                foreach($recordTypes as $recordType) {
                    $account->adRequestSnapshot($recordType);
                }
            }
        } elseif ($type == "getSnapshots") {
            foreach ($accounts as $account) {
                foreach($recordTypes as $recordType) {
                    $account->getSnapshot($recordType);
                }
            }
        } elseif ($type == "requestProductAdsReportHistorical") {
            $start_date = $date ? date("Ymd", strtotime($date)) : date("Ymd", strtotime("-2 months"));
            $end_date = date("Ymd", strtotime("-1 days"));
            while($end_date >= $start_date) {
                foreach ($accounts as $account) {
                    foreach ($reportTypes as $reportType) {
                        $account->adRequestReport($end_date, $reportType);
                    }
                }

                $end_date = date("Ymd", strtotime($end_date . ' -1 day'));
            }
        } elseif ($type == "getProductAdsReportHistorical") {
            $start_date = $date ? date("Y-m-d", strtotime($date)) : date("Ymd", strtotime("-2 months"));
            $end_date = date("Y-m-d", strtotime("-1 days"));
            while($end_date >= $start_date) {
                foreach ($accounts as $account) {
                    foreach ($reportTypes as $reportType) {
                        $account->getAdReportByDate($end_date, $reportType);
                    }
                }

                $end_date = date("Y-m-d", strtotime($end_date . ' -1 day'));
            }
        } elseif($type == "updateProductAds") {
            foreach ($accounts as $account) {
                foreach($recordTypes as $recordType) {
                    $account->updateProductAds($recordType);
                }
            }

        } elseif($type == "updateProductAdsForAccount") {
            if($seller_id) {
                $account = Account::where('seller_id', $seller_id)->first();
                foreach($recordTypes as $recordType) {
                    $account->updateProductAds($recordType);
                }
            }

        } elseif($type == "requestProductAdsReportByAccount") {
          if($seller_id) {
            $date = date("Ymd", strtotime($date));
            $account = Account::where('seller_id', $seller_id)->first();
            foreach ($reportTypes as $reportType) {
                $account->adRequestReport($date, $reportType);
            }
          }
        } elseif($type == "getProductAdsReportByAccount") {
          if($seller_id) {
            $date = date("Y-m-d", strtotime($date));
            $account = Account::where('seller_id', $seller_id)->first();
            foreach ($reportTypes as $reportType) {
                $account->getAdReportByDate($date, $reportType);
            }
          }
        } elseif($type == "requestProductAdsReportHistoricalByAccount") {
          if($seller_id) {
            $start_date = $date ? date("Ymd", strtotime($date)) : date("Ymd", strtotime("-1 months"));
            $end_date = date("Ymd", strtotime("-1 days"));
            $account = Account::where('seller_id', $seller_id)->first();
            while($end_date >= $start_date) {
                foreach ($reportTypes as $reportType) {
                    $account->adRequestReport($end_date, $reportType);
                }

                $end_date = date("Ymd", strtotime($end_date . ' -1 day'));
            }
          }
        } elseif($type == "getProductAdsReportHistoricalByAccount") {
          if($seller_id) {
            $start_date = $date ? date("Y-m-d", strtotime($date)) : date("Y-m-d", strtotime("-1 months"));
            $end_date = date("Y-m-d", strtotime("-1 days"));
            $account = Account::where('seller_id', $seller_id)->first();
            while($end_date >= $start_date) {
                foreach ($reportTypes as $reportType) {
                    $account->getAdReportByDate($end_date, $reportType);
                }

                $end_date = date("Y-m-d", strtotime($end_date . ' -1 day'));
            }
          }
        } elseif ($type == "requestTargetsReportHistorical") {
            $start_date = $date ? date("Ymd", strtotime($date)) : date("Ymd", strtotime("-2 months"));
            $end_date = date("Ymd", strtotime("-1 days"));
            while($end_date >= $start_date) {
                foreach ($accounts as $account) {
                    $account->adRequestReport($end_date, "targets");
                }

                $end_date = date("Ymd", strtotime($end_date . ' -1 day'));
            }
        } elseif ($type == "getTargetsReportHistorical") {
            $start_date = $date ? date("Y-m-d", strtotime($date)) : date("Ymd", strtotime("-2 months"));
            $end_date = date("Y-m-d", strtotime("-1 days"));
            while($start_date <= $end_date) {
                foreach ($accounts as $account) {
                    $account->getAdReportByDate($start_date, "targets");
                }

                $start_date = date("Y-m-d", strtotime($start_date . ' +1 day'));
            }
        } elseif($type == "runAdsRules") {//run Ads Rule to make change for Ad enabled or paused
            echo "Time:";
            echo $time = date('H:i');// get current time
            echo "\n";
            $ad_rules = AdRule::where('rule_active', true)->where('exec_time', $time)->get(); //get list of all matching exec_time ads rules
            
            foreach($ad_rules as $rule){ //check if there are any matching campaigns with condition of the rule                
                $campaigns = AdCampaign::getCampaignsByRule($rule);//get compaigns by rule
                foreach($campaigns as $campaign){// get list of campaigns
                    $new_state = $rule->change_campaign_open == 1 ? 'enabled' : 'paused';// decide new state according to change_campaign_open value in ad_rule
                    $campaignData = array(
                                array(
                                    "campaignId" => (float) $campaign->campaignId,
                                    "state" => $new_state
                                )
                            );
                    $compaign_update = new AdCampaign;
                    echo $campaign->campaignId;
                    echo "\n";
                    $response = $compaign_update->updateCampaignStateAction($campaign->campaignId, $campaignData); //change state in amazon production
                }
            }
 
        }



    }
}
