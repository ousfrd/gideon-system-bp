<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account;
use App\Helpers\AdReportImporter;

class DispatchAdReportImportJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dispatch:ad_import {ad_report} {--S|seller=} {--C|country=US}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import ad report.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $adReportPath = $this->argument('ad_report');
        $sellerCode = $this->option('seller');
        $country = $this->option('country');

        if (!is_file($adReportPath)) {
            $this->error('Could not find ad report - ' . $adReportPath);
            return;
        }

        $account = Account::where('code', $sellerCode)->first();
        if ($account === NULL) {
            $this->error('Could not find seller - ' . $sellerCode);
            return;
        }

        AdReportImporter::importAdReport(
            $adReportPath, $account->seller_id, $country);
    }
}
