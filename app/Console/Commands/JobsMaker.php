<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\ImportOrders;
use App\Jobs\ImportOrderShipments;
use App\Jobs\ImportListings;
use App\Jobs\ImportInventory;
use App\Jobs\ImportFinances;
use App\Jobs\ImportAds;
use App\Jobs\ImportBusiness;
use App\Jobs\ImportCampaigns;
use App\Jobs\ImportSearchTerms;
use App\Import;

class JobsMaker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:jobs {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make Jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$this->info('Start Making Jobs');
    	
        $type = $this->argument('type');
        
        if($type == "imports") {
            $imports = Import::where('status', 'pending')->get();
            foreach ($imports as $key => $import) {
                switch ($import['type']) {
                    case 'Orders':
                        ImportOrders::dispatch($import);
                        break;
                    case 'UpdatedOrders':
                        ImportOrders::dispatch($import);
                        break;
                    case 'OrderShipments':
                        ImportOrderShipments::dispatch($import);
                        break;
                    case 'Listings':
                        ImportListings::dispatch($import);
                        break;
                    case 'FBA Inventory':
                        ImportInventory::dispatch($import);
                        break;
                    case 'Finances':
                        ImportFinances::dispatch($import);
                        break;                                        
                    case 'Ads':
                        ImportAds::dispatch($import);
                        break;  
                    case 'Business':
                        ImportBusiness::dispatch($import);
                        break;
                    case 'CampaignsBulkReport':
                        ImportCampaigns::dispatch($import);
                        break;                        
                    case 'SearchTerms':
                        ImportSearchTerms::dispatch($import);
                        break;                   
                    default:
                        break;
                }

                $import->status = 'processing';
                $import->save();
            }
        }
      
    	$this->info('Finish Making Jobs');
    	
    }
}
