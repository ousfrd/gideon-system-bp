<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account;
use App\Helpers\InventoryReportImporter;

class DispatchInventoryImportJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dispatch:inventory_import {inventory_report} {--S|seller=} {--C|country=US}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import inventory report.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $inventoryReportPath = $this->argument('inventory_report');
        $sellerCode = $this->option('seller');
        $country = $this->option('country');

        if (!is_file($inventoryReportPath)) {
            $this->error('Could not find inventory report - ' . $inventoryReportPath);
            return;
        }

        $account = Account::where('code', $sellerCode)->first();
        if ($account === NULL) {
            $this->error('Could not find seller - ' . $sellerCode);
            return;
        }

        InventoryReportImporter::importInventoryReport(
            $inventoryReportPath, $account->seller_id, $country);
    }
}
