<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Jobs\ImportShipStationOrder;
use App\Helpers\ListingReportImporter;


class DispatchShipStationOrderImportJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dispatch:shipstation:order_import {days?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispath ShipStation Order Import Job.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days = $this->argument('days');
        if ($days) {
            $dateFormat = 'Y-m-d\TH:i:s.u';
            $tz = new \DateTimeZone('America/Los_Angeles');
            $interval = new \DateInterval(sprintf('P%sD', $days));
            $orderDate = new \DateTime();
            $orderDate->sub($interval);
            $orderDateStr = $orderDate->format($dateFormat);
            Log::info('Import ShipStation Orders with orderDateStart - ' . $orderDateStr);
        } else {
            $orderDateStr = NULL;
            Log::info('Import ShipStation Orders');
        }

        ImportShipStationOrder::dispatch($orderDateStr)->onQueue('shipstation');
    }
}
