<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Order;
use App\Account;

class OrdersWithoutAddress extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:missing_address {days?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Find orders that missing address information.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days = $this->argument('days') ?? 30;
        $fromPurchaseDate = \Carbon\Carbon::now()->subDays($days)->startOfDay();
        $fromPurchaseDateStr = $fromPurchaseDate->format('Y-m-d H:i:s');
        $queryParams = ['purchase_date' => ['start_date' => $fromPurchaseDateStr]];
        $orders = Order::getOrders(Order::getQuery($queryParams));

        $ordersMissingAddress = [];
        foreach ($orders as $order) {
            if (empty($order->ShippingAddressName) || empty($order->ShippingAddressAddressLine1)) {
                $ordersMissingAddress[] = $order;
            }
        }

        $accounts = [];
        foreach (Account::all() as $account) {
            $accounts[$account->seller_id] = sprintf(
                '%s %s', $account->code, $account->name);
        }

        foreach ($ordersMissingAddress as $order) {
            $sellerId = $order->seller_id;
            if (!array_key_exists($sellerId, $accounts)) {
                continue;
            }

            $accountName = $accounts[$sellerId];
            $message = sprintf(
                "%s\t%s\t%s\t%s",
                $accountName, $order->AmazonOrderId,
                $order->SalesChannel, $order->PurchaseDate);
            $this->info($message);
        }
    }
}
