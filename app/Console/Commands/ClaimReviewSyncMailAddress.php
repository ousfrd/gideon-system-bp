<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Console\Command;
use App\Helpers\Click2MailHelper;

class ClaimReviewSyncMailAddress extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'claim_review:sync_mail_address';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync click2mail mail addresses.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $addresses = [];

        $client = Click2MailHelper::getClick2MailClient();
        $addressLists = $client->getAddressLists();
        foreach ($addressLists as $addressList) {
            $addresses[$addressList->id] = [];

            $addressListInfo = $client->getAddressListInfo($addressList->id);
            foreach ($addressListInfo->address as $addressInfo) {
                Log::debug(json_encode($addressInfo));
                $addresses[$addressList->id][] = $addressInfo;
            }
        }

        Storage::disk('local')->put(
            'claim-review-campaign/click2mail_mail_addresses.json',
            json_encode($addresses)
        );
    }
}
