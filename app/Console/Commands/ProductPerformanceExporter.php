<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;

use League\Csv\Writer;

use App\ProductStatistic;


class ProductPerformanceExporter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:performance:export {days=30}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export product performance in CSV format.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days = $this->argument('days') ?? 30;
        $fromDate = \Carbon\Carbon::now()->subDays($days)->startOfDay();
        $toDate = \Carbon\Carbon::now()->endOfDay();
        $fromDateStr = $fromDate->format('Y-m-d');
        $toDateStr = $toDate->format('Y-m-d');
        $queryParams = ['from_date' => $fromDateStr, 'to_date' => $toDateStr];
        // $fromDateStr = '2020-10-01';
        // $queryParams = ['from_date' => '2020-10-01', 'to_date' => $toDateStr];
        $bestSellingProductStatistics = ProductStatistic::getBestSellingProducts($queryParams);

        $headers = [
            'Marketplace', 'ASIN', 'ROI', 'Profit', 'Payout', 'TotalRevenue', 'TotalCost',
            'TotalGross', 'ShippingFee', 'VAT Fee', 'FBA Fee', 'TotalRefund',
            'Tax', 'Commission', 'Giftwrap Fee', 'Other Fee', 'AD Expenses',
            'TotalOrders', 'TotalUnits', 'RefundCount', 'PPC Count', 'AD Sales'
        ];

        $fileName = sprintf(
            'ProductPerformance-%s-%s.csv', $fromDateStr, $toDateStr);
        $writer = Writer::createFromPath($fileName, 'w+');
        $writer->insertOne($headers);

        foreach ($bestSellingProductStatistics as $productStatistic) {
            $productStatistic = (object) $productStatistic;

            $record = [];
            $record[] = $productStatistic->country;
            $record[] = $productStatistic->ASIN;
            $cost = $productStatistic->total_cost;
            if ($cost > 0) {
                $record[] = round($productStatistic->profit / $cost * 100, 2);
            } else {
                $record[] = 'N/A';
            }
            $record[] = $productStatistic->profit;
            $record[] = $productStatistic->payout;
            $record[] = $productStatistic->total_revenue;
            $record[] = $productStatistic->total_cost;
            $record[] = $productStatistic->total_gross;
            $record[] = $productStatistic->shipping_fee;
            $record[] = $productStatistic->vat_fee;
            $record[] = $productStatistic->fba_fee;
            $record[] = $productStatistic->total_refund;
            $record[] = $productStatistic->tax;
            $record[] = $productStatistic->commission;
            $record[] = $productStatistic->giftwrap_fee;
            $record[] = $productStatistic->other_fee;
            $record[] = $productStatistic->adExpenses;
            $record[] = $productStatistic->total_orders;
            $record[] = $productStatistic->total_units;
            $record[] = $productStatistic->refund_count;
            $record[] = $productStatistic->ppc_count;
            $record[] = $productStatistic->ads_sales;

            $writer->insertOne($record);
        }
    }
}
