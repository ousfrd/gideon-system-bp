<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;

use App\ClickSendClient;
use App\ClaimReviewReturnAddress;
use App\ClaimReviewCampaign;
use App\Helpers\Click2MailHelper;


class SyncClaimReviewReturnAddress extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'claim_review:sync_return_address';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync return addresses from claim review service providers.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $providers = ClaimReviewCampaign::SUPPLIERS;
        $click2MailProvider = 'click2mail';

        if (array_key_exists(ClaimReviewCampaign::CLICK_SEND_PROVIDER, $providers)) {
            try {
                $addresses = ClickSendClient::getReturnAddresses();
                foreach ($addresses as $address) {
                    $returnAddressId = $address['return_address_id'];
                    $userId = $address['user_id'];
                    $returnAddress = ClaimReviewReturnAddress::where([
                        ['return_address_id', '=', $returnAddressId],
                        ['user_id', '=', $userId],
                        ['platform', '=', ClaimReviewCampaign::CLICK_SEND_PROVIDER]
                    ])->first();
                    if ($returnAddress == NULL) {
                        $returnAddress = new ClaimReviewReturnAddress();
                    }
                    $returnAddress->fill($address);
                    $returnAddress->platform = ClaimReviewCampaign::CLICK_SEND_PROVIDER;
                    $returnAddress->save();

                    $message = '[ClickSendReturnAddressSynced] ' . json_encode($address);
                    Log::info($message);
                    $this->info($message);
                }
            } catch (\Exception $e) {
                $this->error('[SyncClickSendReturnAddressFailed] ' . $e->getMessage());
            }
        }

        if (array_key_exists(ClaimReviewCampaign::CLICK2MAIL_PROVIDER, $providers)) {
            try {
                $addresses = Click2MailHelper::getReturnAddresses();
                $returnAddressIds = [];
                foreach ($addresses as $address) {
                    $returnAddressId = $address['return_address_id'];
                    $returnAddressIds[] = $returnAddressId;

                    $returnAddress = ClaimReviewReturnAddress::where([
                        ['return_address_id', '=', $returnAddressId],
                        ['platform', '=', ClaimReviewCampaign::CLICK2MAIL_PROVIDER]
                    ])->first();
                    if ($returnAddress == NULL) {
                        $returnAddress = new ClaimReviewReturnAddress();
                    }
                    $returnAddress->fill($address);
                    $returnAddress->save();

                    $message = '[Click2MailReturnAddressSynced] ' . json_encode($address);
                    Log::info($message);
                    $this->info($message);
                }

                ClaimReviewReturnAddress::where(
                    'platform', ClaimReviewCampaign::CLICK2MAIL_PROVIDER
                )->whereNotIn('return_address_id', $returnAddressIds)->delete();
            } catch (\Exception $e) {
                $this->error('[SyncClick2MailReturnAddressFailed] ' . $e->getMessage());
            }
        }
    }
}
