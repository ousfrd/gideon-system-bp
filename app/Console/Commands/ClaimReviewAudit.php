<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\ClaimReviewCampaign;
use App\Helpers\DataHelper;


class ClaimReviewAudit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'claim_review:audit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Audit claim review by month.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = \Carbon\Carbon::now();
        $startDate = $now->copy()->startOfYear()->format('Y-m-d\TH:i:s');
        $campaigns = ClaimReviewCampaign::where('sent_at', '>=', $startDate)->get();

        $result = [];
        $groupedCampaigns = DataHelper::groupByDate($campaigns, 'sent_at', 'UTC', 'month');
        foreach ($groupedCampaigns as $k => $campaignsByMonth) {
            $sentAmount = DataHelper::aggregate($campaignsByMonth, 'actual_sent_amount');
            $orderAmount = DataHelper::aggregate($campaignsByMonth, 'order_quantity');

            $result[$k] = ['sentAmount' => $sentAmount, 'orderAmount' => $orderAmount];
        }

        ksort($result);

        foreach ($result as $k => $v) {
            $message = sprintf(
                '[%s] OrderAmount: %s, SentAmount: %s',
                $k, $v['orderAmount'], $v['sentAmount']);

            $this->info($message);
        }
    }
}
