<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Product;
use App\Order;
use App\Helpers\DataHelper;


class SyncProductCosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:sync_costs  {days=30}  {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync product cost/shipping cost/fbm shipping cost.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Product::where([
            ['status', '=', 1],
            ['warehouse_qty', '>=', 1]
        ])->get();
        foreach ($products as $product) {
            $product->cost = $product->current_product_cost;
            $product->shipping_cost = $product->current_shipping_cost;
            $product->fbm_shipping_cost = $product->fbm_current_shipping_cost;
            $product->save();
        }

        $dryRun = $this->option('dry-run');

        $days = $this->argument('days') ?? 30;
        $fromPurchaseDate = \Carbon\Carbon::now()->subDays($days)->startOfDay();
        $fromPurchaseDateStr = $fromPurchaseDate->format('Y-m-d H:i:s');
        $queryParams = ['purchase_date' => ['start_date' => $fromPurchaseDateStr]];

        $orders = Order::getOrders(Order::getQuery($queryParams));
        $groupedOrders = DataHelper::groupByDate($orders, 'PurchaseDate', config('app.timezone'));
        foreach ($groupedOrders as $date => $ordersByDate) {
            $productCosts = [];

            foreach ($ordersByDate as $order) {
                $product = Product::where([
                    ['asin', '=', $order->ASIN],
                    ['country', '=', $order->country]
                ])->first();

                if ($product == NULL) {
                    continue;
                }

                if (array_key_exists($product->id, $productCosts)) {
                    $cost = $productCosts[$product->id]['cost'];
                    $shippingCost = $productCosts[$product->id]['shipping_cost'];
                } else {
                    $productCost = $product->productCostInDate($date);
                    $cost = empty($productCost) ? 0 : $productCost->cost_per_unit;
                    $productShippingCost = $product->shippingCostInDate($date);
                    $shippingCost = empty($productShippingCost) ? 0 : $productShippingCost->cost_per_unit;

                    $productCosts[$product->id] = [
                        'cost' => $cost, 'shipping_cost' => $shippingCost
                    ];
                }

                if ($cost || $shippingCost) {
                    $order->cost = $cost;
                    $order->shipping_cost = $shippingCost;

                    $message = sprintf(
                        '[OrderCostUpdated] AmazonOrderID: %s, PurchaseDate: %s, Cost: %s, ShippingCost: %s',
                        $order->AmazonOrderID, $date, $cost, $shippingCost
                    );
                    $this->info($message);
                }

                $order->save();
            }
        }
    }
}
