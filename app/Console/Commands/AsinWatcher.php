<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ProductDailyProfit;

class AsinWatcher extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asin:watcher';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Watch asins';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	
    	$this->info('Start Watching ASIN');
    	

        $date = date('Y-m-d');

        
        ProductDailyProfit::asinWatcher($date);
        
      
    	$this->info('Finish Watching ASIN');
    	
    }
}
