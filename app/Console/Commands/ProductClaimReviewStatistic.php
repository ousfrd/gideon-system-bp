<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Product;
use League\Csv\Writer;

class ProductClaimReviewStatistic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:claim_review:statistic {days=30}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export product claim review statistic in CSV format.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days = $this->argument('days') ?? 30;
        $fromDate = \Carbon\Carbon::now()->subDays($days)->startOfDay();
        $toDate = \Carbon\Carbon::now()->endOfDay();
        $fromDateStr = $fromDate->format('Y-m-d');
        $toDateStr = $toDate->format('Y-m-d');

        $headers = [
            'Marketplace', 'ASIN',
            'orderCount', 'pendingOrderCount', 'shippedOrderCount', 'unshippedOrderCount',
            'returnedOrderCount', 'partialReturnedOrderCount', 'shippingOrderCount',
            'innerPageOrderCount', 'missingShipmentCount', 'toClaimReviewCount',
            'claimReviewToSendCount', 'claimReviewSentCount'
        ];
        $fileName = sprintf(
            'ProductClaimReviewStatistic-%s-%s.csv', $fromDateStr, $toDateStr);
        $writer = Writer::createFromPath($fileName, 'w+');
        $writer->insertOne($headers);

        $statistics = Product::getClaimReviewStatistics($fromDateStr, $toDateStr);
        foreach ($statistics as $k => $statistic) {
            list($marketplace, $asin) = explode('-', $k);

            $record = [$marketplace, $asin];
            foreach ($headers as $header) {
                if (!in_array($header, ['Marketplace', 'ASIN'])) {
                    $record[] = $statistic[$header];
                }
            }

            $writer->insertOne($record);
        }
    }
}
