<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\EmailTemplate;

class MailMan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mailman:send {type} {order_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PL MailMan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $type = $this->argument('type');
        $order_id = $this->argument('order_id');

        if($type == "greeting") {

            $template = EmailTemplate::where('type', 'Greeting')->where('active', 1)->first();

            if(!empty($template)) {

                if(isset($order_id) && !empty($order_id)) {
                    $order = Order::where('AmazonOrderId', $order_id)->first();
                    $order->sendGreetingEmail();
                } else {
                    $orders = Order::newOrders();
                    foreach ($orders as $order) {
                        $order->sendGreetingEmail();
                    }
                }
            }

        } elseif($type == "delivered") {
            
            $template = EmailTemplate::where('type', 'Delivered')->where('active', 1)->first();

            if(!empty($template)) {

                if(isset($order_id) && !empty($order_id)) {
                    $order = Order::where('AmazonOrderId', $order_id)->first();
                    $order->sendDeliveredEmail();
                } else {
                    $orders = Order::deliveredOrders();
                    foreach ($orders as $order) {
                        $new_order = Order::where('AmazonOrderId', $order->AmazonOrderId)->first();
                        $new_order->sendDeliveredEmail();
                    }
                }
            }

        } elseif($type == "product_review") {
            $template = EmailTemplate::where('type', 'Product Review')->where('active', 1)->first();

            if(!empty($template)) {

                if(isset($order_id) && !empty($order_id)) {
                    $order = Order::where('AmazonOrderId', $order_id)->first();
                    $order->sendProductReviewEmail();
                } else {
                    $orders = Order::pendingReviewOrders();
                    foreach ($orders as $order) {
                        if($order->need_review()) {
                            $new_order = Order::where('AmazonOrderId', $order->AmazonOrderId)->first();
                            $new_order->sendProductReviewEmail();
                        }
                    }
                }
            }

        } elseif($type == "product_review_history") {
            $template = EmailTemplate::where('type', 'Product Review')->where('id', 13)->first();

            if(!empty($template)) {
                if(isset($order_id) && !empty($order_id)) {
                    $order = Order::where('AmazonOrderId', $order_id)->first();
                    $order->sendProductReviewEmail();
                } else {
                    $orders = Order::historyOrders();

                    foreach ($orders as $order) {
                        $old_order = Order::where('AmazonOrderId', $order->AmazonOrderId)->where('seller_id', $order->seller_id)->first();
                        $old_order->sendProductReviewEmail();
                    }
                }
            }

        }

    } 
}
