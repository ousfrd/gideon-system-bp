<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\Shipment;

class Tracking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tracking:check {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update order delivered_date by check tracking number.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        # get orders which is shipped but not yet delivered
        $orders = Order::shippedOrders();
        
        foreach ($orders as $order) {
            
            $shipment = Shipment::where('amazon_order_id', $order->AmazonOrderId)->first();

            if($shipment) {
                $delivered_date = $shipment->checkDelivered();

                if($delivered_date) {
                    $order->delivered_date = $delivered_date;
                    $order->save();
                }
            }
        }

    }
}
