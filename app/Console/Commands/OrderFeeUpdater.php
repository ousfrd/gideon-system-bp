<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Order;


class OrderFeeUpdater extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:update_fees {days=30}  {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update order FBA Fee and Refer Fee.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dryRun = $this->option('dry-run');

        $days = $this->argument('days') ?? 30;
        $fromPurchaseDate = \Carbon\Carbon::now()->subDays($days)->startOfDay();
        $fromPurchaseDateStr = $fromPurchaseDate->format('Y-m-d H:i:s');
        $queryParams = ['purchase_date' => ['start_date' => $fromPurchaseDateStr]];
        $orders = Order::getOrders(Order::getQuery($queryParams));

        foreach ($orders as $order) {
            $updated = FALSE;

            if (in_array($order->OrderStatus, ['Cancelled', 'Partial Returned', 'Returned'])) {
                if ($order->commission < 0) {
                    $order->commission = 0;
                    $updated = TRUE;
                }

                if ($order->OrderStatus == 'Cancelled') {
                    if ($order->fba_fee < 0) {
                        $order->fba_fee = 0;
                        $order->save();
                        $updated = TRUE;
                    }
                }
            } else {
                $commission = (float) $order->commission;
                if (empty($commission)) {
                    $order->commission = -1 * round($order->ItemPrice * 0.15, 2);
                        $updated = TRUE;
                }

                $fbaFee = (float) $order->fba_fee;
                if (empty($fbaFee)) {
                    if ($order->FulfillmentChannel == 'AFN') {
                        $order->fba_fee = -2.99 * $order->QuantityOrdered;
                        $updated = TRUE;
                    }
                }
            }

            if (!$updated) {
                continue;
            }

            if (!$dryRun) {
                $order->save();
            }

            $message = sprintf(
                '[OrderFeeUpdated] AmazonOrderId: %s, Status: %s, ItemPrice: %s, FBA: %s, Refer: %s',
                $order->AmazonOrderId, $order->OrderStatus, $order->ItemPrice,
                $order->fba_fee, $order->commission);
            $this->info($message);
        }
    }
}
