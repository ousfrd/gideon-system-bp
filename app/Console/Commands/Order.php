<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\OrderItem;
use App\Product;

class Order extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order {type} {start_date?} {end_date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$type = $this->argument('type');
    	
    	$this->info('Starting aggregate order '.$type.' data...');
    	
    	if($type == 'fba') {
    		try{
    			OrderItem::aggregateFBAFee($this);
    		}catch(\Exception $e) {
    			var_dump($e->getMessage());
    		}
    		
    	} elseif($type == 'pfba') {

    		Product::aggregateFBAFee($this);

    	} elseif ($type == 'daily_profit') {
            
            $start_date = date('Y-m-d', strtotime($this->argument('start_date')));
            $end_date = date('Y-m-d', strtotime($this->argument('end_date')));
            $date = $start_date;
            while($date <= $end_date) {
                try{
                    Product::updateProductDailyProfitByDate($date, $date);
                    echo "Finish Aggregate Profit Data: " . $date . "\n";
                } catch(\Exception $e) {
                    var_dump($e->getMessage());
                }

                $date = date('Y-m-d', strtotime($date . ' +1 day'));
            }
        } elseif ($type == 'daily_profit_last_day') {
            
            $start_date = date('Y-m-d',strtotime("-1 days"));
            $end_date = date('Y-m-d');
            $date = $start_date;
            while($date <= $end_date) {
                try{
                    Product::updateProductDailyProfitByDate($date, $date);
                    echo "Finish Aggregate Profit Data: " . $date . "\n";
                } catch(\Exception $e) {
                    var_dump($e->getMessage());
                }

                $date = date('Y-m-d', strtotime($date . ' +1 day'));
            }
        } elseif ($type == 'daily_profit_7_days') {
            
            $start_date = date('Y-m-d',strtotime("-7 days"));
            $end_date = date('Y-m-d',strtotime("-1 days"));
            $date = $start_date;
            while($date <= $end_date) {
                try{
                    Product::updateProductDailyProfitByDate($date, $date);
                    echo "Finish Aggregate Profit Data: " . $date . "\n";
                } catch(\Exception $e) {
                    var_dump($e->getMessage());
                }

                $date = date('Y-m-d', strtotime($date . ' +1 day'));
            }
        } elseif ($type == 'daily_profit_2_months') {
            
            $start_date = date('Y-m-d',strtotime("-2 month"));
            $end_date = date('Y-m-d',strtotime("-1 days"));
            $date = $start_date;
            while($date <= $end_date) {
                try{
                    Product::updateProductDailyProfitByDate($date, $date);
                    echo "Finish Aggregate Profit Data: " . $date . "\n";
                } catch(\Exception $e) {
                    var_dump($e->getMessage());
                }

                $date = date('Y-m-d', strtotime($date . ' +1 day'));
            }
        }
    	
    	
    	
    	$this->info('Finished aggregate order '.$type.' data...');
    	
    }
}
