<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account;
use App\Helpers\OrderShipmentReportImporter;

class DispatchOrderShipmentImportJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dispatch:order_shipment_import {order_shipment_report} {--S|seller=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Order Shipment reports.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orderShipmentReportPath = $this->argument('order_shipment_report');
        $sellerCode = $this->option('seller');

        if (!is_file($orderShipmentReportPath)) {
            $this->error('Could not find order shipment report - ' . $orderShipmentReportPath);
            return;
        }

        $account = Account::where('code', $sellerCode)->first();
        if ($account === NULL) {
            $this->error('Could not find seller - ' . $sellerCode);
            return;
        }

        OrderShipmentReportImporter::importOrderShipmentReport(
            $orderShipmentReportPath, $account->seller_id);
    }
}
