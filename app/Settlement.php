<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use App\Traits\MassInsertOrUpdate;
// use GuzzleHttp\Client as GuzzleClient;
// use Symfony\Component\DomCrawler\Crawler;
use Nesk\Puphpeteer\Puppeteer;
use Nesk\Rialto\Data\JsFunction;
use Telegram;

class Settlement extends Model
{
	
	use MassInsertOrUpdate;

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'settlements';

    function accounts()
    {
        return $this->belongsTo('App\Account','code','account_code');
    }    

	public static function get_beginning() {
        $first_record = Settlement::orderBy('settlement_start_date', 'asc')->first();

		if($first_record) {
			return $first_record->settlement_start_date;
		} else {
			return "2010-01-20";
		}
	}


}

