<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
class InboundShipment extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'inbound_shipments';
	
	
	function listing() {
		return $this->belongsTo('App\Listing','seller_sku','sku');
	}
	
	function merchant() {
		return $this->belongsTo('App\Account','seller_id','seller_id');
	}
}

