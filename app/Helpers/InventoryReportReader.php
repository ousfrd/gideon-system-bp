<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use League\Csv\Reader;
use App\AmazonFBAInventory;


class InventoryReportReader {
	public static function getInventoryRecords($inventoryReportPath) {
		$csv = Reader::createFromPath($inventoryReportPath, 'r');
		self::configReader($csv);

		foreach ($csv->getRecords() as $inventoryRecord) {
			yield AmazonFBAInventory::fromInventoryRecord($inventoryRecord);
		}
	}

	public static function getInventoryRecordsFromString($content) {
		$csv = Reader::createFromString($content);
		self::configReader($csv);

		foreach ($csv->getRecords() as $inventoryRecord) {
			yield AmazonFBAInventory::fromInventoryRecord($inventoryRecord);
		}
	}

	public static function configReader($csv) {
		$csv->setDelimiter("\t");
		$csv->setHeaderOffset(0);
	}
}
