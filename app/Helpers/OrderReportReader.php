<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use League\Csv\Reader;
use App\AmazonOrder;


class OrderReportReader {
	public static function getOrders($orderReportPath) {
		$csv = Reader::createFromPath($orderReportPath, 'r');
		self::configReader($csv);

		foreach ($csv->getRecords() as $orderRecord) {
			yield AmazonOrder::fromOrderRecord($orderRecord);
		}
	}

	public static function getOrdersFromString($content) {
		$csv = Reader::createFromString($content);
		self::configReader($csv);

		foreach ($csv->getRecords() as $orderRecord) {
			yield AmazonOrder::fromOrderRecord($orderRecord);
		}
	}

	public static function configReader($csv) {
		$csv->setDelimiter("\t");
		$csv->setHeaderOffset(0);
	}
}
