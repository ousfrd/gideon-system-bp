<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use League\Csv\Reader;


class ListingReportReader {
	public static function getListings($listingReportPath) {
		$csv = Reader::createFromPath($listingReportPath, 'r');
		self::configReader($csv);

		return $csv->getRecords();
	}

	public static function getListingsFromString($content) {
		$csv = Reader::createFromString($content);
		self::configReader($csv);

		return $csv->getRecords();
	}

	public static function configReader($csv) {
		$csv->setDelimiter("\t");
		$csv->setHeaderOffset(0);
	}
}
