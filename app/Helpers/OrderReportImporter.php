<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use App\Account;
use App\Product;
use App\Listing;
use App\Order;
use App\OrderItem;
use App\Shipment;
use App\ProductStatistic;
use App\ManagedReviewOrder;
use App\Helpers\CurrencyHelper;
use App\Helpers\OrderReportReader;


class OrderReportImporter {
	public static function importOrderReport($orderReportPath, $sellerId) {
		$orderRecords = OrderReportReader::getOrders($orderReportPath);
		self::importOrders($orderRecords, $sellerId);
	}

	public static function importOrderReportFromString($content, $sellerId) {
		$orderRecords = OrderReportReader::getOrdersFromString($content);
		self::importOrders($orderRecords, $sellerId);
	}

	public static function importOrders($orderRecords, $sellerId) {
		$fromDate = NULL;
		$toDate = NULL;
		foreach ($orderRecords as $orderRecord) {
			self::importOrder($orderRecord, $sellerId);

			// TODO: '2020-11-02T06:14:47+00:00'
			$purchaseDate = \DateTime::createFromFormat(
				\DateTimeInterface::RFC3339, $orderRecord->purchaseDate);
			if ($fromDate === NULL || $fromDate > $purchaseDate) {
				$fromDate = $purchaseDate;
			}
			if ($toDate === NULL || $toDate < $purchaseDate) {
				$toDate = $purchaseDate;
			}
		}

        // if ($fromDate && $toDate) {
        //     self::updateSellerStatistics(
        //         $sellerId, $fromDate->format('Y-m-d'), $toDate->format('Y-m-d'));
        // }
		ManagedReviewOrder::syncSelfOrdersWithOrderItem();
	}

	public static function importOrder($orderRecord, $sellerId) {
		$errorMessageTemplate = '[OrderImportError] Reason: %s, SellerID: %s, Order: %s';
		$amazonOrderId = $orderRecord->amazonOrderId;
		if (empty($amazonOrderId) || strlen($amazonOrderId) != 19) {
			$errorMessage = sprintf(
				$errorMessageTemplate, 'Missing amazon-order-id',
				$sellerId, json_encode($orderRecord)
			);
        	Log::warning($errorMessage);

        	return FALSE;
        }

        if ($orderRecord->salesChannel == 'Non-Amazon') {
        	$errorMessage = sprintf(
				$errorMessageTemplate, 'None Amazon Order',
				$sellerId, json_encode($orderRecord)
			);
        	Log::warning($errorMessage);

        	return FALSE;
        }

		$listing = Listing::where([
			['seller_id', '=', $sellerId],
			['asin', '=', $orderRecord->asin],
			['sku', '=', $orderRecord->sku],
            ['status', '>=', 0]
		])->first();
		if ($listing == NULL) {
			$errorMessage = sprintf(
				$errorMessageTemplate, 'Listing Missing',
				$sellerId, json_encode($orderRecord)
			);
        	Log::warning($errorMessage);

        	return FALSE;
		}

		$result = self::storeOrderItem($orderRecord, $sellerId);
		$result = $result && self::storeOrder($orderRecord, $sellerId);
		if ($result) {
			$message = sprintf(
        		'[OrderImported] SellerID: %s, Order: %s',
        		$sellerId, json_encode($orderRecord)
        	);
        	Log::info($message);
		}

		return $result;
    }

    public static function storeOrderItem($orderRecord, $sellerId) {
		$errorMessageTemplate = '[OrderImportError] Reason: %s, SellerID: %s, Order: %s';

        $salesChannelLower = strtolower($orderRecord->salesChannel);
        if (!array_key_exists($salesChannelLower, config('app.marketplaceCountry'))) {
        	$errorMessage = sprintf(
				$errorMessageTemplate, 'Unsupported Sales Channel',
				$sellerId, json_encode($orderRecord)
			);
        	Log::warning($errorMessage);

        	return FALSE;
        }

    	$orderItem = OrderItem::where([
        	['AmazonOrderId', '=', $orderRecord->amazonOrderId],
        	['SellerSKU', '=', $orderRecord->sku],
        	['seller_id', '=', $sellerId]
        ])->first();
        if ($orderItem === NULL) {
            $orderItem = new OrderItem();
            $orderItem->seller_id = $sellerId;
            $orderItem->AmazonOrderId = $orderRecord->amazonOrderId;
            $orderItem->SellerSKU = $orderRecord->sku;
        }
        $orderItem->ASIN = $orderRecord->asin;
        $orderItem->Title = $orderRecord->productName;
        $orderItem->QuantityOrdered = $orderRecord->quantity;
        $orderItem->Currency = $orderRecord->currency;
        $orderItem->ItemPrice = $orderRecord->itemPrice;
        $orderItem->ShippingPrice = $orderRecord->shippingPrice;
        $orderItem->giftwrap_fee = $orderRecord->giftWrapPrice;
        $orderItem->tax = $orderRecord->itemTax + $orderRecord->shippingTax + $orderRecord->giftWrapTax;
        $orderItem->item_promo = $orderRecord->itemPromotionDiscount;
        $orderItem->shipping_promo = $orderRecord->shipPromotionDiscount;
        $orderItem->total_promo = -1 * ($orderItem->item_promo + $orderItem->shipping_promo);

        // estimate commission and fba fee
        if ($orderRecord->orderStatus == "Pending") {
            $orderItem->commission = -1 * round($orderItem->ItemPrice * 0.15, 2);
            $orderItem->fba_fee = -2.99 * $orderItem->QuantityOrdered;
        } else {
            $commission = (float) $orderItem->commission;
            if (empty($commission)) {
                if (!in_array($orderRecord->orderStatus, ['Cancelled', 'Partial Returned', 'Returned'])) {
                    $orderItem->commission = -1 * round($orderItem->ItemPrice * 0.15, 2);
                }
            }

            $fbaFee = (float) $orderItem->fba_fee;
            if (empty($fbaFee)) {
                if ($orderRecord->fulfillmentChannel == 'Amazon' && $orderRecord->orderStatus != 'Cancelled') {
                    $orderItem->fba_fee = -2.99 * $orderItem->QuantityOrdered;
                }
            }
        }

        $country = config('app.marketplaceCountry')[$salesChannelLower];
        $time = strtotime($orderRecord->purchaseDate);
        $date = date('Y-m-d', $time);
        $orderItem->exchange_rate = CurrencyHelper::get_exchange_rate(
        	$date, $orderRecord->currency, 'USD');
        $orderItem->ItemPriceUSD = $orderItem->ItemPrice * $orderItem->exchange_rate;
        $orderItem->ShippingPriceUSD = $orderItem->ShippingPrice * $orderItem->exchange_rate;
        $orderItem->country = $country;
        $orderItem->region = config('app.countriesRegion')[$country];
        
        if($orderItem->region == "UK") {
            $orderItem->vat_fee = ($orderItem->ShippingPriceUSD + $orderItem->ItemPriceUSD) * config('app.vat_fee_rate') * -1;
        }

		$product = Product::where([
        	['asin', '=', $orderRecord->asin],
        	['country', '=', $country]
        ])->first();
        if ($product !== NULL) {
            $orderItem->cost = $product->cost;
            $orderItem->shipping_cost = $product->shipping_cost;
        }

        return $orderItem->save();
    }

    public static function storeOrder($orderRecord, $sellerId) {
    	$order = Order::where([
        	['AmazonOrderId', '=', $orderRecord->amazonOrderId],
        	['seller_id', '=', $sellerId]
        ])->first();
        if ($order === NULL) {
        	$order = new Order();
        	$order->seller_id = $sellerId;
        	$order->AmazonOrderId = $orderRecord->amazonOrderId;
        }

        $order->OrderStatus = $orderRecord->orderStatus;
        $order->PurchaseDate = $orderRecord->purchaseDate;
        $order->SalesChannel = $orderRecord->salesChannel;
        $order->FulfillmentChannel = $orderRecord->fulfillmentChannel == "Amazon" ? "AFN" : "MFN";

        return $order->save();
    }

    public static function updateSellerStatistics($sellerId, $fromDate, $toDate) {
		$account = Account::getAccountBySellerId($sellerId);
		if ($account) {
    		ProductStatistic::buildStatisticsForSeller($account, $fromDate, $toDate);
		}
	}
}
