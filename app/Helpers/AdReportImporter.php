<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use App\AdReport;
use App\Helpers\CurrencyHelper;
use App\Helpers\AdReportReader;


class AdReportImporter {
	public static function importAdReport($adReportPath, $sellerId, $country) {
		$adRecords = AdReportReader::getAdReports($adReportPath);
		self::importAdRecords($adRecords, $sellerId, $country);
	}

	public static function importAdRecords($adRecords, $sellerId, $country) {
		foreach ($adRecords as $adRecord) {
			$result = self::importAdRecord($adRecord, $sellerId, $country);
			if ($result) {
				$message = sprintf(
					'[AdRecordImported] SellerID: %s, Country: %s, AD: %s',
					$sellerId, $country, json_encode($adRecord)
				);
				Log::info($message);
			} else {
				$message = sprintf(
					'[AdRecordImportError] SellerID: %s, Country: %s, AD: %s',
					$sellerId, $country, json_encode($adRecord)
				);
				Log::warning($message);
			}
		}
	}

	public static function importAdRecord($adRecord, $sellerId, $country) {
		Log::debug(json_encode($adRecord));
		if (array_key_exists('Date', $adRecord)) {
			$date = $adRecord['Date'];
		} else if (array_key_exists('Start Date', $adRecord)) {
			$date = $adRecord['Start Date'];
		} else {
			return FALSE;
		}
        $dateStr = $date->format('Y-m-d');

        $country = strtoupper($country);
    	$asin = $adRecord['Advertised ASIN'];
    	$sku = $adRecord['Advertised SKU'];
    	$campaignName = $adRecord['Campaign Name'];
    	$adGroupName = $adRecord['Ad Group Name'];
        $adReport = AdReport::where([
        	['seller_id', '=', $sellerId],
            ['country', '=', $country],
        	['date', '=', $dateStr],
        	['asin', '=', $asin],
        	['sku', '=', $sku],
        	['campaignName', '=', $campaignName],
        	['adGroupName', '=', $adGroupName]
        ])->first();

        if ($adReport === NULL) {
            $adReport = new AdReport();
            $adReport->seller_id = $sellerId;
            $adReport->country = $country;
            $adReport->date = $dateStr;
            $adReport->asin = $asin;
            $adReport->sku = $sku;
            $adReport->campaignName = $campaignName;
            $adReport->adGroupName = $adGroupName;
        }

        $adReport->currency = $adRecord['Currency'];
        $adReport->impressions = $adRecord['Impressions'];
        $adReport->clicks = $adRecord['Clicks'];
        $adReport->cost = $adRecord['Spend'];
        $adReport->attributedConversions7d = $adRecord['7 Day Total Orders (#)'];
        $adReport->attributedUnitsOrdered7d = $adRecord['7 Day Total Units (#)'];
        foreach ($adRecord as $k => $v) {
            if (strpos($k, '7 Day Total Sales') === FALSE) {
                continue;
            }

            $adReport->attributedSales7d = $v;
        }
        // if (array_key_exists('7 Day Total Sales', $adRecord)) {
        //     $adReport->attributedSales7d = $adRecord['7 Day Total Sales'];
        // } else if (array_key_exists('7 Day Total Sales ($)', $adRecord)) {
        //     $adReport->attributedSales7d = $adRecord['7 Day Total Sales ($)'];
        // } else if (array_key_exists('7 Day Total Sales (£)', $adRecord)) {
        //     $adReport->attributedSales7d = $adRecord['7 Day Total Sales (£)'];
        // }

        $adReport->exchange_rate = CurrencyHelper::get_exchange_rate(
        	$dateStr, $adRecord['Currency'], 'USD');

        return $adReport->save();
	}
}
