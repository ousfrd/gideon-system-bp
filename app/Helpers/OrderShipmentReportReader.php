<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use League\Csv\Reader;
use App\AmazonOrderShipment;


class OrderShipmentReportReader {
	public static function getOrderShipments($orderShipmentReportPath) {
		$csv = Reader::createFromPath($orderShipmentReportPath, 'r');
		self::configReader($csv);

		foreach($csv->getRecords() as $orderShipmentRecord) {
			yield AmazonOrderShipment::fromOrderShipmentRecord($orderShipmentRecord);
		}
	}

	public static function getOrderShipmentsFromString($content) {
		$csv = Reader::createFromString($content);
		self::configReader($csv);

		foreach($csv->getRecords() as $orderShipmentRecord) {
			yield AmazonOrderShipment::fromOrderShipmentRecord($orderShipmentRecord);
		}
	}

	public static function configReader($csv) {
		$csv->setDelimiter("\t");
		$csv->setHeaderOffset(0);
	}
}
