<?php 
namespace App\Helpers;

class StateHelper {
  static $us_state_abbrevs_names = [
    "AL" => "ALABAMA",
    "AK" => "ALASKA",
    "AZ" => "ARIZONA",
    "AR" => "ARKANSAS",
    "CA" => "CALIFORNIA",
    "CO" => "COLORADO",
    "CT" => "CONNECTICUT",
    "DE" => "DELAWARE",
    "DC" => "DISTRICT OF COLUMBIA",
    "FL" => "FLORIDA",
    "GA" => "GEORGIA",
    "HI" => "HAWAII",
    "ID" => "IDAHO",
    "IL" => "ILLINOIS",
    "IN" => "INDIANA",
    "IA" => "IOWA",
    "KS" => "KANSAS",
    "KY" => "KENTUCKY",
    "LA" => "LOUISIANA",
    "ME" => "MAINE",
    "MD" => "MARYLAND",
    "MA" => "MASSACHUSETTS",
    "MI" => "MICHIGAN",
    "MN" => "MINNESOTA",
    "MS" => "MISSISSIPPI",
    "MO" => "MISSOURI",
    "MT" => "MONTANA",
    "NE" => "NEBRASKA",
    "NV" => "NEVADA",
    "NH" => "NEW HAMPSHIRE",
    "NJ" => "NEW JERSEY",
    "NM" => "NEW MEXICO",
    "NY" => "NEW YORK",
    "NC" => "NORTH CAROLINA",
    "ND" => "NORTH DAKOTA",
    "OH" => "OHIO",
    "OK" => "OKLAHOMA",
    "OR" => "OREGON",
    "PA" => "PENNSYLVANIA",
    "RI" => "RHODE ISLAND",
    "SC" => "SOUTH CAROLINA",
    "SD" => "SOUTH DAKOTA",
    "TN" => "TENNESSEE",
    "TX" => "TEXAS",
    "UT" => "UTAH",
    "VT" => "VERMONT",
    "VA" => "VIRGINIA",
    "WA" => "WASHINGTON",
    "WV" => "WEST VIRGINIA",
    "WI" => "WISCONSIN",
    "WY" => "WYOMING",
  ];

  static function toAbrev($state) {
    $state = trim($state);
    if (strlen($state) == 2) {
      return $state;
    } else {
      return self::fullNameToAbrev($state);
    }
  }

  static function fullNameToAbrev($name) {
    $name = preg_replace("/\s/", "",strtoupper($name));
    foreach (self::$us_state_abbrevs_names as $abrev => $fullName) {
      $fullName = preg_replace("/\s/", "", $fullName);
      if ($name == $fullName) {
        return $abrev;
      }
    }
  }

  static function abrevToFullName($abrev) {
    return self::$us_state_abbrevs_names[$abrev];
  }
}