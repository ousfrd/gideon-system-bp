<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Helpers;

use Oefenweb\Statistics\Statistics;


class DataHelper {
	const DATE_METRICS = [
		'year' => 'Y',
		'month' => 'Y-m',
		'day' => 'Y-m-d',
		'hour' => 'Y-m-d\TH'
	];
	const AGGREGATE_METHODS = [
		'sum' => 'sum',
		'count' => 'frequency',
		'max' => 'max',
		'min' => 'min',
		'avg' => 'mean'
	];

	public static function aggregate($data, $field, $method='sum') {
		if (!array_key_exists($method, self::AGGREGATE_METHODS)) {
			return FALSE;
		}

		$fieldVals = [];
		$df = get_defined_functions();
		foreach ($data as $item) {
			if ($field instanceof Closure) {
				array_push($fieldVals, $field($item));
			} else if (is_callable($field) && !in_array($field, $df['internal'])) {
				array_push($fieldVals, $field($item));
			} else {
				array_push($fieldVals, $item->$field);
			}
		}

		$aggre = self::AGGREGATE_METHODS[$method];
		return Statistics::$aggre($fieldVals);
	}

	public static function groupByVal($data, $field) {
		$groupedData = [];
		// $fieldLower = strtolower($field);
		$df = get_defined_functions();
		foreach ($data as $item) {
			if ($field instanceof Closure) {
				$fieldVal = $field($item);
			} else if (is_callable($field) && !in_array($field, $df['internal'])) {
				$fieldVal = $field($item);
			} else {
				$fieldVal = $item->$field;
			}

			if (array_key_exists($fieldVal, $groupedData)) {
				array_push($groupedData[$fieldVal], $item);
			} else {
				$groupedData[$fieldVal] = [$item];
			}
		}

		return $groupedData;
	}

	public static function groupByDate($data, $dateField, $tz, $metric='day') {
		if (!array_key_exists($metric, self::DATE_METRICS)) {
			return FALSE;
		}

		$dateFormat = self::DATE_METRICS[$metric];
		$groupedData = [];
		foreach ($data as $item) {
			$fieldVal = $item->$dateField;
			$date = is_string($fieldVal) ? \Carbon\Carbon::parse($fieldVal, 'UTC') : $fieldVal;
			$dateStr = $date->setTimezone($tz)->format($dateFormat);

			if (array_key_exists($dateStr, $groupedData)) {
				array_push($groupedData[$dateStr], $item);
			} else {
				$groupedData[$dateStr] = [$item];
			}
		}

		return $groupedData;
	}
}
