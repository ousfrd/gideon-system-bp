<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use App\Account;
use App\Product;
use App\Listing;
use App\Shipment;
use App\Helpers\CurrencyHelper;
use App\Helpers\ListingReportReader;


class ListingReportImporter {
	public static $exchangeRates = [];
	public static $isbn = NULL;

	public static function importListingReport($listingReportPath, $sellerId, $country) {
		if (!is_file($listingReportPath)) {
			return FALSE;
		}

		$listingRecords = ListingReportReader::getListings($listingReportPath);
		self::importListings($listingRecords, $sellerId, $country);
	}

	public static function importListingReportFromString($content, $sellerId, $country) {
		$listingRecords = ListingReportReader::getListingsFromString($content);
		self::importListings($listingRecords, $sellerId, $country);
	}

	public static function importListings($listingRecords, $sellerId, $country) {
		$country = strtoupper($country);
		$productIds = [];
		$listingIds = Listing::where([
        	['seller_id', '=', $sellerId],
        	['country', '=', $country]
        ])->select('id')->pluck('id')->all();
        $listingIds = array_flip($listingIds);

		foreach ($listingRecords as $listingRecord) {
			if (!$listingRecord['seller-sku'] || !$listingRecord['asin1']) {
				continue;
			}

			if (self::isDsListing($listingRecord['seller-sku'])) {
				continue;
			}

			$result = self::importListing($listingRecord, $sellerId, $country);
			if ($result) {
				$message = sprintf(
					'[ListingImported] SellerID: %s, Country: %s, Listing: %s',
					$sellerId, $country, json_encode($listingRecord)
				);
				Log::debug($message);

				array_push($productIds, $result['productId']);
				if (array_key_exists($result['listingId'], $listingIds)) {
					unset($listingIds[$result['listingId']]);
				}
			} else {
				$message = sprintf(
					'[ListingImportError] SellerID: %s, Country: %s, Listing: %s',
					$sellerId, $country, json_encode($listingRecord)
				);
				Log::warning($message);
				continue;
			}
		}

		// Update product status after imported all listings
		foreach ($productIds as $productId) {
			$product = Product::where('id', $productId)->first();
			if ($product) {
            	$product->updateStatus();
			}
        }

        // Listings do not appear in the report are considered removed.
        foreach ($listingIds as $listingId => $v) {
        	$listing = Listing::where('id', $listingId)->first();
        	if ($listing) {
        		$listing->markRemoved();
        	}
        }
	}

	public static function importListing($listingRecord, $sellerId, $country) {
		$sku = $listingRecord['seller-sku'];
		$asin = $listingRecord['asin1'];
		$itemName = trim($listingRecord['item-name']);
		$openDateStr = $listingRecord['open-date'];
		$itemCondition = intval($listingRecord['item-condition']);
		$fulfillmentChannel = $listingRecord['fulfillment-channel'];
		$price = floatval($listingRecord['price']);
		$status = $listingRecord['status'];

        $account = Account::where('seller_id', $sellerId)->first();
        if ($account === NULL) {
        	Log::warning('[AccountMissing] SellerID: ' . $sellerId);
        	return FALSE;
        }
        $region = $account->region;

		$realCountry = Shipment::getCountry($asin, $sku);
        if ($realCountry) {
            $country = $realCountry;
        }

        $product = Product::where('asin', $asin)->where('country', $country)->first();
        if ($product === NULL) {
            $product = new Product();
            $product->asin = $asin;
            $product->earliest_listing_open_date = $openDateStr;
        }
        $product->country = $country;
        $product->name = $itemName;
        $product->save();

        $listing = Listing::where([
        	['seller_id', '=', $sellerId],
        	['asin', '=', $asin],
        	['sku', '=', $sku]
        ])->first();
        if ($listing === NULL) {
            $listing = new Listing();
            $listing->seller_id = $sellerId;
            $listing->asin = $asin;
            $listing->sku = $sku;
        }
        $listing->country = $country;
        $listing->item_name = $itemName;
        $listing->region = $region;
        $listing->product_id = $product->id;
        $listing->item_condition = $itemCondition;
        $listing->fulfillment_channel = $fulfillmentChannel == 'DEFAULT' ? 'Merchant' : 'Amazon';
        $listing->open_date = date('Y-m-d', strtotime($openDateStr));
        $listing->price = $price;
        $listing->currency = config('app.countriesCurrency')[$country];
        $exchangeRate = self::getExchangeRate($listing->currency);
        $listing->price_usd = $price * $exchangeRate;
        $status = strtolower($status);
        if ($status == 'active') {
        	$listing->status = 1;
        } else if ($status == 'inactive') {
        	$listing->status = 0;
        } else {
        	$listing->status = -2;
        }
        $listing->updated_at = \Carbon\Carbon::now('UTC')->format('Y-m-d H:i:s');
        $listing->save();

        return ['productId' => $product->id, 'listingId' => $listing->id];
	}

	public static function isDsListing($sku) {
		$productCodes = ['b', 'c', 'g', 'p'];
		$conditionCodes = ['p', 'b', 'n', 'x', 'm', 'g', 'u', 'j'];
		$conditionPatterns = ['new', 'xin', 'mint', 'brand_new'];
		// $sourceCountryCodes = ['us', 'uk', 'ca', 'de'];

		$formatedSku = str_replace('_', '-', strtolower($sku));
		// PL skus don't have word "book"
		if (strpos($formatedSku, 'book') !== FALSE) {
			return TRUE;
		}

		if (substr($formatedSku, -2) == '-s' && substr_count($formatedSku, '-') == 1) {
			return TRUE;
		}

		// PL does not sell books
		if (self::$isbn === NULL) {
			self::$isbn = new \Isbn\Isbn();
		}
		if (self::$isbn->validation->isbn($sku)) {
			return TRUE;
		}

		$serialNumber = substr($formatedSku, -5);
		$serialNumberFound = preg_match('/[0-9\.]{5}/', $serialNumber) == 1;

		$skuParts = explode('-', $formatedSku);
		$lastPart = end($skuParts);
		$productCode = substr($lastPart, 0, 1);
		$productCodeFound = in_array($productCode, $productCodes);

		$conditionCode = substr($formatedSku, 0, 1);
		$conditionMatched = FALSE;
		foreach ($conditionPatterns as $conditionPattern) {
			if (strpos($formatedSku, $conditionPattern) !== FALSE) {
				$conditionMatched = TRUE;
				break;
			}
		}
		$conditionFound = in_array($conditionCode, $conditionCodes) || $conditionMatched;

		return $conditionFound && $productCodeFound && $serialNumberFound;
	}

	public static function getExchangeRate($currency) {
		$date = date('Y-m-d');
		if (in_array($date, self::$exchangeRates)) {
			if (in_array($currency, self::$exchangeRates[$date])) {
				$exchangeRate = self::$exchangeRates[$date][$currency];
			} else {
				$exchangeRate = CurrencyHelper::get_exchange_rate($date, $currency, 'USD');
				self::$exchangeRates[$date][$currency] = $exchangeRate;
			}
		} else {
			$exchangeRate = CurrencyHelper::get_exchange_rate($date, $currency, 'USD');
			self::$exchangeRates[$date] = [];
			self::$exchangeRates[$date][$currency] = $exchangeRate;
		}

		return $exchangeRate;
	}
}
