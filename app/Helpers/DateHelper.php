<?php 
namespace App\Helpers;

class DateHelper {
	static function convertDateFromTimezone($date,$timezone,$timezone_to,$format){
		$date = new \DateTime($date,new \DateTimeZone($timezone));
		$date->setTimezone( new \DateTimeZone($timezone_to) );
		return $date->format($format);
	}
	
	static function convertUTCToTimezone($date,$timezone_to=null,$format='m/d/Y, H:i A T'){
		if($timezone_to== null) {
			$timezone_to= config('app.timezone');
		}
		return self::convertDateFromTimezone($date,'UTC',$timezone_to,$format);
	}
	
	static function convertToUTC($date,$timezone_from=null,$format='Y-m-d H:i:s'){
		if($timezone_from == null) {
			$timezone_from = config('app.timezone');
		}
		return self::convertDateFromTimezone($date,$timezone_from,'UTC',$format);
	}
	
	static function timezoneUTCOffset($timezone=null, $datetime=null){
		if($timezone == null) {
			$timezone = config('app.timezone');
		}
		if($datetime == null) {
			$now = date('Y-m-d H:i:s');
		} else {
			$now = $datetime;
		}
		
		$date = new \DateTime($now,new \DateTimeZone($timezone));
		return $date->getOffset()/3600;
		//return self::convertDateFromTimezone($date,$timezone_from,'UTC',$format);
	}
}