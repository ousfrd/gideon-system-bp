<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use App\ProductStatistic;


class ProductHelper {
	public static function exportProductOverviews($serviceAccountPath = NULL, $spreadSheetId = NULL, $days = 1) {
		if (empty($serviceAccountPath)) {
			$serviceAccountPath = env('PRODUCT_OVERVIEW_SERVICE_ACCOUNT', NULL);
		}
		if (empty($spreadSheetId)) {
			$spreadSheetId = env('PRODUCT_OVERVIEW_SPREAD_SHEET_ID', NULL);
		}

		if (empty($serviceAccountPath) || empty($spreadSheetId)) {
			return;
		}

        if (!file_exists($serviceAccountPath)) {
            return;
        }

        $headers = [
            'ID', 'Status', 'PM', 'Marketplace', 'ASIN', 'Name/Alias',
            'Listing分级', 'Group', '广告花费', '广告销售额',
            '广告花费占利润的百分比(%)', '销量', '亚马逊库存', '利润', 'Payout',
            'Rating星级', 'Rating数量'
        ];
        $baseColumnMapping = [
            'ID' => 'serial_num',
            'Status' => 'statusText',
            'PM' => 'PM',
            'Marketplace' => 'country',
            'ASIN' => 'asin',
            'Name/Alias' => 'productName',
            'Listing分级' => 'category',
            'Group' => 'productGroup',
        ];

        $timeZone = 'UTC';
        $fromDate = \Carbon\Carbon::now($timeZone)->subDays($days)->startOfDay();
        $toDate = \Carbon\Carbon::now($timeZone)->subDays(1)->endOfDay();
        $fromDateStr = $fromDate->format('Y-m-d');
        $toDateStr = $toDate->format('Y-m-d');

        $params = [
            'from_date' => $fromDateStr,
            'to_date' => $toDateStr,
        ];
        $overviews = ProductStatistic::getProductOverviews($params);
        $productOverviews = [];
        foreach ($overviews as $overview) {
            $k = sprintf('%s-%s', $overview->country, $overview->asin);
            $productOverviews[$k] = $overview;
        }

        $client = new \Google\Client();
        $client->setAuthConfig($serviceAccountPath);
        $client->setApplicationName("Product Overview Exportor");
        $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
        $client->setAccessType('offline');
        $service = new \Google_Service_Sheets($client);
        // $spreadSheet = $service->spreadsheets->get($spreadSheetId);
        // var_dump($spreadSheet);

        $toDateField = $toDate->format('m/d');
        // Get month to date
        $monthToDate = [];
        $firstDateOfMonth = $toDate->copy()->firstOfMonth();
        for ($date = $toDate; $date->gte($firstDateOfMonth); $date->subDay()) {
            $dateStr = $date->format('m/d');
            $monthToDate[] = $dateStr;
        }

        $sheetsMapping = [
            [
                'sheetName' => 'PM-Dev',
                'keepHistory' => TRUE,
                'historyFields' => ['profit'],
                'columnMapping' => []
            ],
            // [
            //     'sheetName' => '采购部-Dev',
            //     'keepHistory' => FALSE,
            //     'historyFields' => [],
            //     'columnMapping' => []
            // ],
            [
                'sheetName' => '财务部-Dev',
                'keepHistory' => FALSE,
                'historyFields' => [],
                'columnMapping' => ['Payout' => 'payout']
            ],
            [
                'sheetName' => 'KBS-Dev',
                'keepHistory' => FALSE,
                'historyFields' => [],
                'columnMapping' => [
                    '昨日销量' => 'total_units',
                    '7天销量' => 'last7DaysUnits',
                    '亚马逊库存' => 'amazonQty',
                    '亚马逊发货数量' => 'inboundShippedQuantity'
                ]
            ],
            [
                'sheetName' => '广告部-Dev',
                'keepHistory' => FALSE,
                'historyFields' => [],
                'columnMapping' => [
                    '广告花费' => 'adExpenses',
                    '广告销售额' => 'ads_sales',
                    '广告花费占利润的百分比' => 'adProfitPercentage',
                    '广告的ACOS' => 'acos'
                ]
            ],
            [
                'sheetName' => '市场部-Dev',
                'keepHistory' => FALSE,
                'historyFields' => [],
                'columnMapping' => [
                    "上线\n日期" => 'earliest_listing_open_date',
                    "主卖\n账户" => 'sellerAccounts',
                    "Rating\n星级" => 'reviewRating',
                    "GRating\n数量" => 'reviewQty',
                    "过去\n七天利润" => 'last7DaysProfit',
                    // "本月\n月标杆" => 'expected_monthly_profit',
                    "利润\n梯队" => 'profit_level',
                    "是否\n有内页" => 'hasInnerPage',
                    "是否\n索评" => 'shouldClaimReview',
                    "是否\n测评" => 'shouldInviteReview',
                    "是否\n去差评" => 'negtiveReviewRemove',
                    "是否\n自刷" => 'selfReview',
                    "索评发信\n数量" => 'totalSent',
                    "索评回复\n数量" => 'claimReviewRespCnt',
                    "索评留评\n数量" => 'claimReviewReviewedCnt',
                    "索评发信\n花费" => 'totalCost',
                    "测评\n回复数" => 'inviteReviewRespCnt',
                    "测评\n留评数" => 'inviteReviewReviewedCnt',
                    "自刷\n订单数量" => 'selfReviewCnt',
                    "自刷\n留评数量" => 'reviewedSelfReviewCnt',
                    "Giftcard\n发送数量" => 'sentGiftcardCnt',
                    "Giftcard\n发送金额" => 'sentGiftcardAmount',
                    "Giftcard\n未发送数量" => 'unsentGiftcardCnt',
                    "Giftcard\n未发送金额" => 'unsentGiftcardAmount'
                ]
            ],
            [
                'sheetName' => '客服部',
                'keepHistory' => FALSE,
                'historyFields' => [],
                'columnMapping' => [
                    "Rating星级" => 'reviewRating',
                    "Rating数量" => 'reviewQty',
                    "是否\n去差评" => 'negtiveReviewRemove',
                    "索评\n回复数量" => 'claimReviewRespCnt',
                    "索评\n留评数量" => 'claimReviewReviewedCnt',
                    "测评\n回复数量" => 'inviteReviewRespCnt',
                    "测评\n留评数量" => 'inviteReviewReviewedCnt',
                ]
            ],
            [
                'sheetName' => '设计部-Dev',
                'keepHistory' => FALSE,
                'historyFields' => [],
                'columnMapping' => [
                    'Profit' => 'profit'
                ]
            ],
        ];

        foreach ($sheetsMapping as $sheetMapping) {
            $sheetColumnMapping = array_merge($baseColumnMapping, $sheetMapping['columnMapping']);

            $productsProcessed = [];
            $values = [];
            $sheetName = $sheetMapping['sheetName'];
            $range = $sheetName;
            $response = $service->spreadsheets_values->get($spreadSheetId, $range);
            $sheetValues = $response->getValues();
            $headerRow = current($sheetValues);
            $headerCnt = count($headerRow);
            Log::debug('[Header] ' . implode(', ', $headerRow));

            $keepHistory = $sheetMapping['keepHistory'];
            $historyFields = $sheetMapping['historyFields'];
            $historyField = current($historyFields);
            $moveHistoryColumnsRequired = TRUE;
            if ($keepHistory && $historyField) {
                $toDateIdx = array_search($toDateField, $headerRow);
                if ($toDateIdx === FALSE) {
                    $toDateIdx = $headerCnt;
                } else {
                    $moveHistoryColumnsRequired = FALSE;
                }
                foreach ($headerRow as $idx => $header) {
                    if (in_array(trim($header), $monthToDate)) {
                        if ($toDateIdx > $idx) {
                            $toDateIdx = $idx;
                        }
                    }
                }
                $sheetColumnMapping = array_merge(
                    $sheetColumnMapping, [$toDateField => $historyField]);
            } else {
                $moveHistoryColumnsRequired = FALSE;
            }
            Log::debug('[ColumnMapping] ' . json_encode($sheetColumnMapping));

            $marketplaceIdx = array_search('Marketplace', $headerRow);
            $asinIdx = array_search('ASIN', $headerRow);
            if ($marketplaceIdx === FALSE || $asinIdx === FALSE) {
                continue;
            }

            foreach ($sheetValues as $rowNum => $row) {
                if ($rowNum == 0) {
                    $values[] = $row;
                    continue;
                }

                $rowFieldCnt = count($row);
                if ($rowFieldCnt <= $marketplaceIdx || $rowFieldCnt <= $asinIdx) {
                    $values[] = $row;
                    continue;
                }

                $k = sprintf('%s-%s', $row[$marketplaceIdx], $row[$asinIdx]);
                if (!array_key_exists($k, $productOverviews)) {
                    $values[] = $row;
                    continue;
                }
                $productOverview = $productOverviews[$k];
                $productsProcessed[$k] = NULL;

                $record = [];
                foreach ($headerRow as $idx => $header) {
                    if (array_key_exists($header, $sheetColumnMapping)) {
                        $propertyKey = $sheetColumnMapping[$header];
                        $record[] = $productOverview->$propertyKey ?? '';
                        continue;
                    }

                    if ($idx >= $rowFieldCnt) {
                        $record[] = '';
                    } else {
                        $record[] = $row[$idx];
                    }
                }
                $values[] = $record;
            }

            foreach ($productOverviews as $k => $productOverview) {
                if (array_key_exists($k, $productsProcessed)) {
                    continue;
                }

                $record = [];
                foreach ($headerRow as $header) {
                    if (array_key_exists($header, $sheetColumnMapping)) {
                        $propertyKey = $sheetColumnMapping[$header];
                        $record[] = $productOverview->$propertyKey ?? '';
                    } else {
                        $record[] = '';
                    }
                }
                $values[] = $record;
            }

            if ($moveHistoryColumnsRequired) {
                $valuesUpdated = [];
                foreach ($values as $rowNum => $row) {
                    if ($rowNum == 0) {
                        $valuesUpdated[] = array_merge(
                            array_slice($row, 0, $toDateIdx), [$toDateField],
                            array_slice($row, $toDateIdx)
                        );
                        continue;
                    }

                    $propertyKey = $sheetColumnMapping[$toDateField];
                    $k = sprintf('%s-%s', $row[$marketplaceIdx], $row[$asinIdx]);
                    if (array_key_exists($k, $productOverviews)) {
                        $val = $productOverviews[$k]->$propertyKey ?? '';
                    } else {
                        $val = '';
                    }
                    $valuesUpdated[] = array_merge(
                        array_slice($row, 0, $toDateIdx), [$val],
                        array_slice($row, $toDateIdx)
                    );
                }
            } else {
                $valuesUpdated = $values;
            }
            // foreach ($valuesUpdated as $idx => $record) {
            //     Log::debug('[Row] Num: ' . $idx . ', Value: ' . implode(', ', $record));
            // }

            $range = sprintf("'%s'", $sheetName);
            $body = new \Google_Service_Sheets_ValueRange([
                'values' => $valuesUpdated
            ]);
            $params = ['valueInputOption' => 'RAW'];
            $service->spreadsheets_values->update($spreadSheetId, $range, $body, $params);
        }
	}
}