<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use App\Listing;
use App\Order;
use App\OrderItem;
use App\Shipment;
use App\Helpers\CurrencyHelper;
use App\Helpers\OrderShipmentReportReader;


class OrderShipmentReportImporter {
	public static function importOrderShipmentReport(
		$orderShipmentReportPath, $sellerId) {
		$orderShipmentRecords = OrderShipmentReportReader::getOrderShipments($orderShipmentReportPath);
		self::importOrderShipments($orderShipmentRecords, $sellerId);
	}

	public static function importOrderShipmentReportFromString(
		$content, $sellerId) {
		$orderShipmentRecords = OrderShipmentReportReader::getOrderShipmentsFromString($content);
		self::importOrderShipments($orderShipmentRecords, $sellerId);
	}

	public static function importOrderShipments($orderShipmentRecords, $sellerId) {
		foreach ($orderShipmentRecords as $orderShipmentRecord) {
			self::importOrderShipment($orderShipmentRecord, $sellerId);
		}
	}

	public static function importOrderShipment($orderShipmentRecord, $sellerId) {
		$errorMessageTemplate = '[OrderShipmentImportError] Reason: %s, SellerID: %s, OrderShipment: %s';
		$amazonOrderId = $orderShipmentRecord->amazonOrderId;
		if (empty($amazonOrderId) || strlen($amazonOrderId) != 19) {
			$errorMessage = sprintf(
				$errorMessageTemplate, 'Missing amazon-order-id',
				$sellerId, json_encode($orderShipmentRecord)
			);
        	Log::warning($errorMessage);

        	return FALSE;
        }

        $orderUpdated = self::updateOrder($orderShipmentRecord, $sellerId);
        if (!$orderUpdated) {
			$errorMessage = sprintf(
				$errorMessageTemplate, 'Failed to update order!',
				$sellerId, json_encode($orderShipmentRecord)
			);
        	Log::warning($errorMessage);
        }

        $orderItemUpdated = self::updateOrderItem($orderShipmentRecord, $sellerId);
        if (!$orderItemUpdated) {
			$errorMessage = sprintf(
				$errorMessageTemplate, 'Failed to update order item!',
				$sellerId, json_encode($orderShipmentRecord)
			);
        	Log::warning($errorMessage);
        }

        $result = self::updateShipment($orderShipmentRecord, $sellerId);
        if ($result) {
        	$message = sprintf(
        		'[OrderShipmentImported] SellerID: %s, OrderShipment: %s',
        		$sellerId, json_encode($orderShipmentRecord)
        	);
        	Log::info($message);

        	return TRUE;
        } else {
			$errorMessage = sprintf(
				$errorMessageTemplate, 'Failed to update shipment!',
				$sellerId, json_encode($orderShipmentRecord)
			);
        	Log::warning($errorMessage);

        	return FALSE;
        }
	}

	public static function updateOrder($orderShipmentRecord, $sellerId) {
		$amazonOrderId = $orderShipmentRecord->amazonOrderId;
		$order = Order::where([
        	['AmazonOrderId', '=', $amazonOrderId],
        	['seller_id', '=', $sellerId]
        ])->first();
        if ($order === NULL) {
            return FALSE;
        }

		$order->OrderStatus = "Shipped";
		$order->ShippingAddressName = $orderShipmentRecord->recipientName;
		$order->ShippingAddressAddressLine1 = $orderShipmentRecord->shipAddress1;
		$order->ShippingAddressAddressLine2 = $orderShipmentRecord->shipAddress2;
		$order->ShippingAddressAddressLine3 = $orderShipmentRecord->shipAddress3;
		$order->ShippingAddressCity = $orderShipmentRecord->shipCity;
		$order->ShippingAddressStateOrRegion = $orderShipmentRecord->shipState;
		$order->ShippingAddressPostalCode = $orderShipmentRecord->shipPostalCode;
		$order->ShippingAddressCountryCode = $orderShipmentRecord->shipCountry;

		return $order->save();
	}

	public static function updateOrderItem($orderShipmentRecord, $sellerId) {
		$amazonOrderId = $orderShipmentRecord->amazonOrderId;
		$orderItems = OrderItem::where([
			['AmazonOrderId', '=', $amazonOrderId],
			['SellerSKU', '=', $orderShipmentRecord->sku],
			['seller_id', '=', $sellerId]
		])->get();

		if ($orderItems->count() > 1) {
			// TODO: Remove extra order items
			$amazonOrderItemId = $orderShipmentRecord->amazonOrderItemId;
			$orderItem = $orderItems->first(function($orderItem, $k) use ($amazonOrderItemId) {
				return $orderItem->OrderItemId == $amazonOrderItemId;
			});
			if ($orderItem == NULL) {
				$orderItem = $orderItems->first();
			}
		} else {
			$orderItem = $orderItems->first();
		}

		if ($orderItem == NULL) {
			return FALSE;
		}

		$orderItem->OrderItemId = $orderShipmentRecord->amazonOrderItemId;
		$orderItem->QuantityShipped = $orderShipmentRecord->quantityShipped;

		try {
			$result = $orderItem->save();
		} catch (Exception $e) {
			$result = FALSE;
		}

		return $result;
	}

	public static function updateShipment($orderShipmentRecord, $sellerId) {
		$orderItem = OrderItem::where([
			['AmazonOrderId', '=', $orderShipmentRecord->amazonOrderId],
			['SellerSKU', '=', $orderShipmentRecord->sku],
			['seller_id', '=', $sellerId]
		])->first();
		if ($orderItem === NULL) {
			return FALSE;
		}

		$shipment = Shipment::where([
			['shipment_id', '=', $orderShipmentRecord->shipmentId],
			['shipment_item_id', '=', $orderShipmentRecord->shipmentItemId]
		])->first();
		if ($shipment === NULL) {
			$shipment = new Shipment();
			$shipment->shipment_id = $orderShipmentRecord->shipmentId;
			$shipment->shipment_item_id = $orderShipmentRecord->shipmentItemId;
		}

		$shipment->is_refunded = ($orderItem->total_refund < 0);
		$shipment->asin = $orderItem->ASIN;

		$shipment->amazon_order_id = $orderShipmentRecord->amazonOrderId;
		$shipment->merchant_order_id = $orderShipmentRecord->merchantOrderId;
		$shipment->amazon_order_item_id = $orderShipmentRecord->amazonOrderItemId;
		$shipment->merchant_order_item_id = $orderShipmentRecord->merchantOrderItemId;
		$shipment->purchase_date = $orderShipmentRecord->purchaseDate;
		$shipment->payments_date = $orderShipmentRecord->paymentsDate;
		$shipment->shipment_date = $orderShipmentRecord->shipmentDate;
		$shipment->reporting_date = $orderShipmentRecord->reportingDate;
		$shipment->buyer_email = $orderShipmentRecord->buyerEmail;
		$shipment->buyer_name = $orderShipmentRecord->buyerName;
		$shipment->buyer_phone_number = $orderShipmentRecord->buyerPhoneNumber;
		$shipment->sku = $orderShipmentRecord->sku;
		$shipment->product_name = $orderShipmentRecord->productName;
		$shipment->quantity_shipped = $orderShipmentRecord->quantityShipped;
		$shipment->item_price = $orderShipmentRecord->itemPrice;
		$shipment->item_tax = $orderShipmentRecord->itemTax;
		$shipment->shipping_price = $orderShipmentRecord->shippingPrice;
		$shipment->shipping_tax = $orderShipmentRecord->shippingTax;
		$shipment->gift_wrap_price = $orderShipmentRecord->giftWrapPrice;
		$shipment->gift_wrap_tax = $orderShipmentRecord->giftWrapTax;
		$shipment->ship_promotion_discount = $orderShipmentRecord->shipPromotionDiscount;
		$shipment->item_promotion_discount = $orderShipmentRecord->itemPromotionDiscount;
		$shipment->recipient_name = $orderShipmentRecord->recipientName;
		$shipment->ship_service_level = $orderShipmentRecord->shipServiceLevel;
		$shipment->ship_address_1 = $orderShipmentRecord->shipAddress1;
		$shipment->ship_address_2 = $orderShipmentRecord->shipAddress2;
		$shipment->ship_address_3 = $orderShipmentRecord->shipAddress3;
		$shipment->ship_city = $orderShipmentRecord->shipCity;
		$shipment->ship_state = $orderShipmentRecord->shipState;
		$shipment->ship_postal_code = $orderShipmentRecord->shipPostalCode;
		$shipment->ship_country = $orderShipmentRecord->shipCountry;
		$shipment->bill_address_1 = $orderShipmentRecord->billAddress1;
		$shipment->bill_address_2 = $orderShipmentRecord->billAddress2;
		$shipment->bill_address_3 = $orderShipmentRecord->billAddress3;
		$shipment->bill_city = $orderShipmentRecord->billCity;
		$shipment->bill_state = $orderShipmentRecord->billState;
		$shipment->bill_postal_code = $orderShipmentRecord->billPostalCode;
		$shipment->bill_country = $orderShipmentRecord->billCountry;
		$shipment->carrier = $orderShipmentRecord->carrier;
		$shipment->tracking_number = $orderShipmentRecord->trackingNumber;
		$shipment->estimated_arrival_date = $orderShipmentRecord->estimatedArrivalDate;
		$shipment->fulfillment_center_id = $orderShipmentRecord->fulfillmentCenterId;
		$shipment->fulfillment_channel = $orderShipmentRecord->fulfillmentChannel;
		$shipment->sales_channel = $orderShipmentRecord->salesChannel;
		$shipment->currency = $orderShipmentRecord->currency;
		$date = date('Y-m-d', strtotime($orderShipmentRecord->paymentsDate));
		$shipment->exchange_rate = CurrencyHelper::get_exchange_rate($date, $shipment->currency, 'USD');
		$shipment->seller_id = $sellerId;

		return $shipment->save();
	}
}
