<?php 
namespace App\Helpers;
use App\Currency;
use GuzzleHttp\Client as GuzzleClient;
use App\OrderItem;

class CurrencyHelper{
	static $currencies = ['USD'=>'$','EUR'=>'€','GBP'=>'£','JPY'=>'¥','MXN'=>'M$','CAD'=>'C$','AUD'=>'A$'];
	static function format($value,$currency_code="USD"){
		return self::$currencies[$currency_code].number_format($value,2);
	}
	
	static function get_exchange_rate($date, $base, $target) {
		$currency = Currency::where('date', $date)->where('base', $base)->where('target', $target)->first();
		if($currency === null) {

			// $currencyRates = new CurrencyRates;
			// $exchange_rate = $currencyRates->driver('exchangerates')->date($date)->base($base)->target($target)->get()->rates[$target];
			$base_url = 'https://api.exchangeratesapi.io/'.$date;
			$params = ['base'=>$base, 'symbols'=>$target];

			$client = new GuzzleClient();
			$res = $client->request('GET', $base_url, ['query' => $params]);

			if($res->getStatusCode() == '200') {
				$result = json_decode($res->getBody());
				$exchange_rate = $result->rates->$target;
			} else {
				// get currency from last order item rate
				$exchange_rate = OrderItem::where('Currency', $base)->where('exchange_rate', '<>', 0)->orderBy('id', 'desc')->first()->exchange_rate;
			}

			Currency::create(['date'=>$date, 'base'=>$base, 'target'=>$target, 'exchange_rate' => $exchange_rate]);
			
			return $exchange_rate;
		} else {
			return $currency->exchange_rate;
		}
	}
}