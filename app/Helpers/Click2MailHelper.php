<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Helpers;

use Illuminate\Support\Facades\Log;

use App\ClaimReviewCampaign;
use Click2Mail\Click2MailClient;


class Click2MailHelper {
	public static function getReturnAddresses() {
		$client = self::getClick2MailClient();
		$response = $client->getAccountReturnAddresses();
        if ($response->status > 0) {
            return [];
        }

		$formatedAddresses = [];
		foreach ($response->addresses->address as $address) {
			$formatedAddress = [
				'user_id' => NULL,
				'platform' => ClaimReviewCampaign::CLICK2MAIL_PROVIDER
			];
			$formatedAddress['return_address_id'] = (string) $address->addressId;
			$formatedAddress['address_name'] = sprintf(
				'%s %s', $address->name->firstName, $address->name->lastName);
			$formatedAddress['address_company'] = (string) $address->organization;
			$formatedAddress['address_line_1'] = (string) $address->address1;
			$formatedAddress['address_line_2'] = (string) $address->address2;
			$formatedAddress['address_city'] = (string) $address->city;
			$formatedAddress['address_state'] = (string) $address->state;
			$formatedAddress['address_postal_code'] = (string) $address->zip;
			$formatedAddress['address_country'] = (string) $address->country;
			$defaultStr = (string) $address->default;
			$formatedAddress['default'] = (strtolower($defaultStr) == 'true') ? TRUE : FALSE;

			$formatedAddresses[] = $formatedAddress;
		}

		return $formatedAddresses;
	}

	public static function getBalance() {
		$client = self::getClick2MailClient();
		$response = $client->getAccountCredit();
		$balanceStr = (string) $response->balance;
		return (float) $balanceStr;
	}

	public static function send($claimReviewCampaign) {
		$document = self::getDocumentFromCampaign($claimReviewCampaign);
		if ($document == NULL) {
			$message = sprintf(
				'[Click2MailSendError] Could not get document - %s',
				$claimReviewCampaign->id);
			Log::warning($message);

			return FALSE;
		}

	    $delayHours = $claimReviewCampaign->cs_delay_hours;
	    $scheduleBase = time() + $delayHours * 60 * 60;
	    $shipments = $claimReviewCampaign->getClickReviewSendOrderShipments()->get();

	    $shipmentAddresses = [];
		foreach ($shipments as $shipment) {
			$oim = $shipment->order_item_marketing;
			if ($oim->is_sent) {
			  continue;
			}

			$oim->click_send_scheduled_send_at = $scheduleBase;

			$address = self::getAddressFromShipment($shipment);
			$shipmentAddresses[] = ['shipment' => $shipment, 'address' => $address];
			$scheduleBase += 1;
		}
		$addresses = array_column($shipmentAddresses, 'address');
	    if (count($addresses) <= 0) {
	    	$message = sprintf(
				'[Click2MailSendError] No recipient to send - %s',
				$claimReviewCampaign->id);
			Log::warning($message);

	    	return FALSE;
	    }
	
		$claimReviewCampaign->is_sending = TRUE;
        $claimReviewCampaign->save();

    	try {
			$options = self::getOptionsFromCampaign($claimReviewCampaign);
			$returnAddress = $claimReviewCampaign->cs_return_address_id;
		    $scheduledAt = 'Next Day';

			$client = self::getClick2MailClient();

			$credit = $client->getAccountCredit();
	    	$claimReviewCampaign->balance_before = (float) $credit->balance;

    		$cost = 0;
	        $amount = count($addresses);
	    	try {
	    		$response = $client->send($addresses, $document, $returnAddress, $scheduledAt, $options);
	    		$errno = property_exists($response, 'errno') ? $response->errno : 0;
	    		$status = property_exists($response, 'status') ? $response->status : 0;
	    		if ($errno > 0) {
	    			Log::warning($response->message);

					$claimReviewCampaign->note = $response->message;
					$amount = 0;
	    		} else {
	    			if ($status <= 0) {
	    				$claimReviewCampaign->sent_at = date('Y-m-d H:i:s');
						if (property_exists($response, 'cost')) {
							$cost = (float) $response->cost;
						}

						foreach ($shipmentAddresses as $shipmentAddress) {
							$oim = $shipmentAddress['shipment']->order_item_marketing;
				        	$oim->is_sent = TRUE;
				        	$oim->click_send_response = '';
				        	$oim->save();
			        	}
					}

					try {
						if (isset($response->productionCost->quantity)) {
							$amount = $response->productionCost->quantity;
						} else {
							$amount = 0;
						}
					} catch (\Exception $e) {
						$amount = 0;
					}

					$claimReviewCampaign->note = json_encode($response);
				}
	    	} catch (\Exception $e) {
				$amount = 0;
				Log::warning($e->getMessage());
				$claimReviewCampaign->note = $e->getMessage();
	    	}

			$claimReviewCampaign->actual_sent_amount = $amount;

			$credit = $client->getAccountCredit();
			$balance = (float) $credit->balance;
			$claimReviewCampaign->balance_after = $balance;
			if ($cost) {
				$claimReviewCampaign->total_cost = $cost;
			} else {
				$claimReviewCampaign->total_cost = $balance - $claimReviewCampaign->balance_before;
			}
			if ($amount > 0) {
	        	$claimReviewCampaign->unit_price = $claimReviewCampaign->total_cost / $amount;
			} else {
				$claimReviewCampaign->unit_price = $claimReviewCampaign->total_cost;
			}
		} catch (\Exception $e) {
			Log::warning($e->getMessage());
			$claimReviewCampaign->note = $e->getMessage();
		} finally {
			$claimReviewCampaign->is_sending = FALSE;
	        $claimReviewCampaign->save();
		}
	}

	public static function getClick2MailClient() {
		$user = env('CLICK2MAIL_USERNAME', '');
        $password = env('CLICK2MAIL_PASSWORD', '');
        $environment = env('CLICK2MAIL_ENV', 'stage');
        return new Click2MailClient($user, $password, $environment);
	}

	public static function getAddressFromShipment($shipment) {
		if (empty($shipment->ship_address_2)) {
			$shipAddress2 = '';
		} else {
			$shipAddress2 = htmlspecialchars($shipment->ship_address_2);
		}
		$address = [
			'address1' => htmlspecialchars($shipment->ship_address_1),
			'address2' => $shipAddress2,
			'city' => $shipment->ship_city,
			'state' => $shipment->ship_state,
			'zip_code' => $shipment->ship_postal_code,
			'country' => $shipment->ship_country
		];
		$name = $shipment->recipient_name;
		$nameParts = explode(' ', $name);
		$address['last_name'] = htmlspecialchars(array_pop($nameParts));
		if (empty($nameParts)) {
			$address['first_name'] = $address['last_name'];
		} else {
			$address['first_name'] = htmlspecialchars(implode(' ', $nameParts));
		}

		return $address;
	}

	public static function getDocumentFromCampaign($claimReviewCampaign) {
		$template = $claimReviewCampaign->giftCardTemplate;
	    if (!$template || !$template->attachment1) {
	    	return NULL;
	    }

	    $filePath1 = $template->attachment1_path;
	    if ($filePath1 == NULL || !is_file($filePath1)) {
	    	return NULL;
	    }

	    $document = ['files' => [['path' => $filePath1]]];

	    $filePath2 = $template->attachment2_path;
	    if ($filePath2 && is_file($filePath2)) {
	    	$document['files'][] = ['path' => $filePath2];
	    }

	    return $document;
	}

	public static function getOptionsFromCampaign($claimReviewCampaign) {
		$options = [
			'document_class' => 'Letter 8.5 x 11',
			'layout' => 'Picture and Address First Page',
			'production_time' => 'Next Day',
			'envelope' => '#10 Open Window Envelope',
			'paper_type' => 'White 24#',
			'colored' => $claimReviewCampaign->cs_colour ? 1 : 0,
			'duplex' => $claimReviewCampaign->cs_duplex ? 1 : 0,
			'mail_class' => $claimReviewCampaign->cs_priority_post ? 'First Class' : 'Standard'
		];

		return $options;
	}
}
