<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use League\Csv\Reader;


class ShipmentFilterReader {
	public static function getShipmentFilterRecords($filePath) {
		$delimiter = self::detectDelimiter($filePath);

		$csv = Reader::createFromPath($filePath, 'r');
        $csv->setDelimiter($delimiter);
        $csv->setHeaderOffset(0);
        foreach ($csv->getRecords() as $shipmentFilterRecord) {
            if (!isset($shipmentFilterRecord['amazon-order-id']) || 
            	empty($shipmentFilterRecord['amazon-order-id'])) {
                $message = sprintf(
                	'[InvalidFileterShipmentRecord] %s',
                	json_encode($shipmentFilterRecord));
                Log::warning($message);
                continue;
            }

            yield $shipmentFilterRecord;
        }
	}

	public static function detectDelimiter($csvFile) {
	    $delimiters = [";" => 0, "," => 0, "\t" => 0, "|" => 0];

	    $handle = fopen($csvFile, "r");
	    $firstLine = fgets($handle);
	    fclose($handle); 
	    foreach ($delimiters as $delimiter => &$count) {
	        $count = count(str_getcsv($firstLine, $delimiter));
	    }

	    return array_search(max($delimiters), $delimiters);
	}
}
