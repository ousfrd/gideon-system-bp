<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use App\InventoryHistory;
use App\Listing;
use App\Helpers\InventoryReportReader;


class InventoryReportImporter {
	public static function importInventoryReport($listingReportPath, $sellerId, $country) {
		$inventoryRecords = InventoryReportReader::getInventoryRecords($listingReportPath);
		self::importInventories($inventoryRecords, $sellerId, $country);
	}

	public static function importInventoryReportFromString($content, $sellerId, $country) {
		$inventoryRecords = InventoryReportReader::getInventoryRecordsFromString($content);
		self::importInventories($inventoryRecords, $sellerId, $country);
	}

	public static function importInventories($inventoryRecords, $sellerId, $country) {
		foreach ($inventoryRecords as $inventoryRecord) {
			$result = self::importInventory($inventoryRecord);
			if ($result) {
				$message = sprintf(
					'[InventoryImported] SellerID: %s, Country: %s, Inventory: %s',
					$sellerId, $country, json_encode($inventoryRecord)
				);
				Log::info($message);
			} else {
				$message = sprintf(
					'[InventoryImportError] SellerID: %s, Country: %s, Inventory: %s',
					$sellerId, $country, json_encode($inventoryRecord)
				);
				Log::warning($message);
			}
		}
	}

	public static function importInventory($inventoryRecord) {
		$asin = $inventoryRecord->asin;
        $sku = $inventoryRecord->sku;
        $condition = strtolower(trim($inventoryRecord->condition)) == "new" ? 11 : 0;

        if (empty($sku) || empty($asin) || $condition != 11) {
            return FALSE;
        }

        $listing = Listing::where([
            ['sku', '=', $sku],
            ['asin', '=', $asin],
            ['item_condition', '=', $condition]
        ])->first();
        if ($listing === NULL) {
            return FALSE;
        }

        $listing->mfn_listing_exists = $inventoryRecord->mfnListingExists;
        $listing->mfn_fulfillable_quantity = $inventoryRecord->mfnFulfillableQuantity;
        $listing->afn_listing_exists = $inventoryRecord->afnListingExists;
        $listing->afn_warehouse_quantity = $inventoryRecord->afnWarehouseQuantity;
        $listing->afn_fulfillable_quantity = $inventoryRecord->afnFulfillableQuantity;
        $listing->afn_unsellable_quantity = $inventoryRecord->afnUnsellableQuantity;
        $listing->afn_reserved_quantity = $inventoryRecord->afnReservedQuantity;
        $listing->afn_total_quantity = $inventoryRecord->afnTotalQuantity;
        $listing->per_unit_volume = $inventoryRecord->perUnitVolume;
        $listing->afn_inbound_working_quantity = $inventoryRecord->afnInboundWorkingQuantity;
        $listing->afn_inbound_shipped_quantity = $inventoryRecord->afnInboundShippedQuantity;
        $listing->afn_inbound_receiving_quantity = $inventoryRecord->afnInboundReceivingQuantity;
        $listing->afn_researching_quantity = $inventoryRecord->afnResearchingQuantity;
        $listing->qty = $inventoryRecord->afnFulfillableQuantity;
        $listing->inventory_updated_at = \Carbon\Carbon::now('UTC')->format('Y-m-d H:i:s');
        $listing->save();

        InventoryHistory::saveFromListing($listing);

        return TRUE;
	}
}
