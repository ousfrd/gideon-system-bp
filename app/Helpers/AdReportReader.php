<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Helpers;

use Illuminate\Support\Facades\Log;

use Rap2hpoutre\FastExcel\FastExcel;


class AdReportReader {
	public static function getAdReports($adReportPath) {
		$adReports = [];
		$originAdReports = (new FastExcel)->import($adReportPath);
		foreach ($originAdReports as $originAdReport) {
			Log::debug(json_encode($originAdReport));
			$adReport = [];
			foreach ($originAdReport as $k => $v) {
				if ($v instanceof \DateTime) {
					$adReport[trim($k)] = self::formatDate($v);
				} else {
					$adReport[trim($k)] = $v;
				}
			}

			array_push($adReports, $adReport);
		}

		return $adReports;
	}

	protected static function formatDate($dateField) {
		return $dateField->setTimezone(new \DateTimeZone('UTC'));
	}
}
