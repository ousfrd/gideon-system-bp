<?php 
namespace App\Helpers;
use App\Redeem;
use App\ManagedReviewOrder;
use App\OrderItemMarketing;

class ReviewHelper {

  private const INVITE_REVIEW = "INVITE_REVIEW";
  private const SELF_REVIEW = "SELF_REVIEW";
  private const CLAIM_REVIEW = "CLAIM_REVIEW";
  private const OTHER_REVIEW = "OTHER_REVIEW";

  const ReviewTypes = [
    self::INVITE_REVIEW, self::SELF_REVIEW, self::CLAIM_REVIEW, self::OTHER_REVIEW
  ];

  static function extractId($reviewLink) {
    if ($reviewLink) {
      if (
        preg_match("/\/customer-reviews\/(\w+)?[\/\?]/", $reviewLink, $matches) ||
        preg_match("/\/review\/(\w+)?\//", $reviewLink, $matches)
      ) {
        return $matches[1];
      }
    } 
    return null;
  }

  static function getReviewType($reviewId) {
    if (Redeem::where('review_id', $reviewId)->exists()) {
      return self::CLAIM_REVIEW;
    } else if (ManagedReviewOrder::where('review_id', $reviewId)->exists()) {
      return self::SELF_REVIEW;
    } else if (OrderItemMarketing::where('review_id', $reviewId)->exists()) {
      return self::INVITE_REVIEW;
    } else {
      return self::OTHER_REVIEW;
    }
  }
}