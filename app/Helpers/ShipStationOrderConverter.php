<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use App\Order;
use App\Helpers\CurrencyHelper;

class ShipStationOrderConverter
{
	public static $salesChannelMapping = array(
		['marketplaceId' => 2, 'accountName' => 'ATVPDKIKX0DER', 'salesChannel' => 'amazon.com'],
		['marketplaceId' => 15, 'accountName' => 'A1F83G8C2ARO7P', 'salesChannel' => 'amazon.co.uk'],
		['marketplaceId' => 32, 'accountName' => 'A2EUQ1WTGCTBG2', 'salesChannel' => 'amazon.ca'],
		['marketplaceId' => 42, 'accountName' => 'A1PA6795UKMFR9', 'salesChannel' => 'amazon.de'],
		['marketplaceId' => 43, 'accountName' => 'A1RKKUPIHCS9HS', 'salesChannel' => 'amazon.es'],
		['marketplaceId' => 44, 'accountName' => 'A13V1IB3VIYZZH', 'salesChannel' => 'amazon.fr'],
		['marketplaceId' => 45, 'accountName' => 'APJ6JRA9NG5V4', 'salesChannel' => 'amazon.it'],
		['marketplaceId' => 87, 'accountName' => 'A1AM78C64UM0Y8', 'salesChannel' => 'amazon.com.mx'],
		['marketplaceId' => 113, 'accountName' => 'A39IBJ37TRP1C6', 'salesChannel' => 'amazon.com.au'],
	);

	public static $orderStatusMapping = array(
		'awaiting_shipment' => 'Unshipped',
		'awaiting_payment' => 'Pending',
		'on_hold' => 'Pending'
	);

	public function convert($shipStationStore, $shipStationOrder)
	{
		$order = ['items' => []];

		$sellerId = $shipStationStore->getStoreName();
		$amazonOrderId = $shipStationOrder->getOrderNumber();
		$orderStatus = $this->formatOrderStatus(ucfirst($shipStationOrder->getOrderStatus()));
		$lastUpdateDate = self::formatDate($shipStationOrder->getModifyDate());
		$fulfillmentChannel = $shipStationOrder->getExternallyFulfilled() ? 'AFN' : 'MFN';
		$purchaseDate = self::formatDate($shipStationOrder->getPaymentDate());
		if (!$purchaseDate) {
			$purchaseDate = self::formatDate($shipStationOrder->getOrderDate());
		}
		$salesChannel = self::getSalesChannelFromStore($shipStationStore);
        $country = config('app.marketplaceCountry')[$salesChannel];
        $region = config('app.countriesRegion')[$country];
        $currency = config('app.countriesCurrency')[$country];
        $exchangeRate = CurrencyHelper::get_exchange_rate(
            date('Y-m-d', strtotime($purchaseDate)), $currency, 'USD');

        $shipTo = $shipStationOrder->getShipTo();

		$order['seller_id'] = $sellerId;
		$order['AmazonOrderId'] = $amazonOrderId;
		$order['PurchaseDate'] = $purchaseDate;
		$order['OrderTotal'] = $shipStationOrder->getOrderTotal();
		$order['OrderTotalUSD'] = $order['OrderTotal'] * $exchangeRate;
		if (TRUE) {
			$order['ShippingAddressName'] = $shipTo['name'];
			$order['ShippingAddressAddressLine1'] = $shipTo['street1'];
			$order['ShippingAddressAddressLine2'] = $shipTo['street2'];
			$order['ShippingAddressAddressLine3'] = $shipTo['street3'];
			$order['ShippingAddressCity'] = $shipTo['city'];
			$order['ShippingAddressStateOrRegion'] = $shipTo['state'];
			$order['ShippingAddressPostalCode'] = $shipTo['postalCode'];
			$order['ShippingAddressCountryCode'] = $shipTo['country'];
			$order['ShippingAddressPhone'] = $shipTo['phone'];
			// $order['BuyerName'] = $shipTo['name'] ?? $shipStationOrder->getCustomerUsername();
			$order['BuyerName'] = $shipTo['name'];
		} else {
			$order['ShippingAddressName'] = $shipTo->getName();
			$order['ShippingAddressAddressLine1'] = $shipTo->getStreet1();
			$order['ShippingAddressAddressLine2'] = $shipTo->getStreet2();
			$order['ShippingAddressAddressLine3'] = $shipTo->getStreet3();
			$order['ShippingAddressCity'] = $shipTo->getCity();
			$order['ShippingAddressStateOrRegion'] = $shipTo->getState();
			$order['ShippingAddressPostalCode'] = $shipTo->getPostalCode();
			$order['ShippingAddressCountryCode'] = $shipTo->getCountry();
			$order['ShippingAddressPhone'] = $shipTo->getPhone();
			// $order['BuyerName'] = $shipTo->getName() ?? $shipStationOrder->getCustomerUsername();
			$order['BuyerName'] = $shipTo->getName();
		}
		$order['BuyerEmail'] = $shipStationOrder->getCustomerEmail();
		$order['PaymentMethod'] = $shipStationOrder->getPaymentMethod();
		$order['OrderStatus'] = $orderStatus;
		$order['LastUpdateDate'] = $lastUpdateDate;
		$order['FulfillmentChannel'] = $fulfillmentChannel;
		$order['SalesChannel'] = $salesChannel;
		$order['Currency'] = $currency;

		foreach ($shipStationOrder->getItems() as $item) {
			if (TRUE) {
				$sku = $item['sku'];
				$orderItem = array(
					'seller_id' => $sellerId,
					'AmazonOrderId' => $amazonOrderId,
					'SellerSKU' => $sku,
					'OrderItemId' => $item['lineItemKey'],
					'Title' => $item['name'],
		            'QuantityOrdered' => $item['quantity'],
		            'ItemPrice' => $item['unitPrice'],
		            'ShippingPrice' => $item['shippingAmount'],
		            'tax' => $item['taxAmount'],
		            'country' => $country,
		            'region' => $region,
		            'Currency' => $currency,
		            'exchange_rate' => $exchangeRate
				);
			} else {
				$sku = $item->getSku();
				$orderItem = array(
					'seller_id' => $sellerId,
					'AmazonOrderId' => $amazonOrderId,
					'SellerSKU' => $sku,
					'Title' => $item->getName(),
		            'QuantityOrdered' => $item->getQuantity(),
		            'ItemPrice' => $item->getUnitPrice(),
		            'ShippingPrice' => $item->getShippingAmount(),
		            'tax' => $item->getTaxAmount(),
		            'country' => $country,
		            'region' => $region,
		            'Currency' => $currency,
		            'exchange_rate' => $exchangeRate
				);
			}

            // estimate commission and fba fee
            if ($fulfillmentChannel == 'AFN') {
                $orderItem['commission'] = -1 * round($orderItem['ItemPrice'] * 0.15, 2);
                $orderItem['fba_fee'] = -2.99 * $orderItem['QuantityOrdered'];
            }

            $orderItem['ItemPriceUSD'] = $orderItem['ItemPrice'] * $exchangeRate;
            $orderItem['ShippingPriceUSD'] = $orderItem['ShippingPrice'] * $exchangeRate;

            if ($orderItem['region'] == "UK") {
            	$itemTotalUSD = $orderItem['ShippingPriceUSD'] + $orderItem['ItemPriceUSD'];
                $orderItem['vat_fee'] = $itemTotalUSD * config('app.vat_fee_rate') * -1;
            } else {
            	$orderItem['vat_fee'] = 0;
            }

            array_push($order['items'], $orderItem);
        }

        return $order;
	}

	public function formatOrderStatus($shipstationOrderStatus)
	{
		if (in_array($shipstationOrderStatus, Order::ORDER_STATUS)) {
			$orderStatus = $shipstationOrderStatus;
		} else {
			$shipstationOrderStatus = strtolower($shipstationOrderStatus);
			if (in_array($shipstationOrderStatus, self::$orderStatusMapping)) {
				$orderStatus = self::$orderStatusMapping[$shipstationOrderStatus];
			} else {
				$orderStatus = 'Unshipped';
			}
		}

		return $orderStatus;
	}

	public static function toDateTime($shipStationDateStr)
	{
		if (!$shipStationDateStr) {
			return NULL;
		}

		$dateFormat = 'Y-m-d\TH:i:s.u+';
		$tz = new \DateTimeZone('America/Los_Angeles');

		return \DateTime::createFromFormat($dateFormat, $shipStationDateStr, $tz);
	}

	public static function formatDate($shipStationDateStr)
	{
		if (!$shipStationDateStr) {
			return NULL;
		}

		$dateFormat = 'Y-m-d\TH:i:s.u+';
		$tz = new \DateTimeZone('America/Los_Angeles');
		$utcZone = new \DateTimeZone('UTC');

		$date = \DateTime::createFromFormat($dateFormat, $shipStationDateStr, $tz);
		if (!$date) {
			Log::error($shipStationDateStr);
		}
		$date->setTimeZone($utcZone);

		return $date->format(\DateTimeInterface::RFC3339);
	}

	public static function getSalesChannelFromStore($store)
	{
		$key = array_search(
			$store->getAccountName(),
			array_column(self::$salesChannelMapping, 'accountName'));
		if ($key === FALSE) {
			$salesChannel = NULL;
		} else {
			$salesChannel = self::$salesChannelMapping[$key]['salesChannel'];
		}

		return $salesChannel;
	}

	public static function getCountryFromStore($store)
	{
		$salesChannel = self::getSalesChannelFromStore($store);
		if ($salesChannel) {
			$country = config('app.marketplaceCountry')[$salesChannel];
		} else {
			$country = NULL;
		}

		return $country;
	}
}
