<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Helpers;


class Utils {
	public static function getValueByKeys($obj, $keys) {
		$value = NULL;

		if (is_object($obj)) {
			foreach ($keys as $key) {
				if (property_exists($obj, $key)) {
					$value = $obj->$key;
					break;
				}
			}
		}

		if (is_array($obj)) {
			foreach ($keys as $key) {
				if (array_key_exists($key, $obj)) {
					$value = $obj[$key];
					break;
				}
			}
		}

		return $value;
	}
}
