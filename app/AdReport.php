<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use App\Traits\MassInsertOrUpdate;
use App\Helpers\DataHelper;

class AdReport extends Model
{

	use MassInsertOrUpdate;
	
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'ad_reports';
	public $timestamps = false;
	
// 	public function listing(){
// 		return $this->belongsTo('App\Listing','listing_id','id');
// 	}
	
	public function listing(){
		return $this->belongsTo('App\Listing','sku','sku');
	}

	public function ad_campaign(){
		return $this->belongsTo('App\AdCampaign','campaignId','campaignId');
	}
	
	public function ad_group(){
		return $this->belongsTo('App\AdGroup','adGroupId','adGroupId');
	}

	static function reportsDateRange($start_date, $end_date,$params){
		
		$query = self::whereBetween('date',[date('Y-m-d',strtotime($start_date)),date('Y-m-d',strtotime($end_date)+24*60*60)]);
		$query = self::buildQuery($query,$params);
		
		
		
		$rows =  $query->get();
		
		$data = [];
		foreach ($rows as $row) {
			$data[$row->date] = $row;
		}
		
		
		
		for($date = $start_date;$date <= $end_date;$date = date("Y-m-d",strtotime("$date +1 day"))) {
			
			if(!isset($data[$date]) || empty($data[$date])) {
				$data[$date] = null;
			}
		}
		
		ksort($data);
		
		return $data;
		
		
	}
	
	static function totalByDateRange($start_date, $end_date,$params){
		
		$query = self::whereBetween('date',[date('Y-m-d',strtotime($start_date)),date('Y-m-d',strtotime($end_date))]);
		$query = self::buildQuery($query,$params);
		
		$query->select(DB::raw('round(sum(cost*exchange_rate), 2) as total'));

		return $query->first()->total;
		
	}

	static function totalByDateRangeAsins($start_date, $end_date,$params){
		
		$query = self::whereBetween('date',[date('Y-m-d',strtotime($start_date)),date('Y-m-d',strtotime($end_date))]);
		$query = self::buildQuery($query,$params);

		$query->select(['asin', DB::raw('round(sum(cost*exchange_rate), 2) as total')]);

		$query->groupBy('asin');
		
		return $query->get();
		
	}

	static function totalByDateRangeAsinCountry($start_date, $end_date,$params){
		
		$query = self::whereBetween('date',[date('Y-m-d',strtotime($start_date)),date('Y-m-d',strtotime($end_date))]);
		$query = self::buildQuery($query,$params);

		$query->select(['asin', 'country', DB::raw('round(sum(cost*exchange_rate), 2) as total')]);

		$query->groupBy('asin', 'country');
		
		return $query->get();
		
	}

	static function totalByGroup($start_date, $end_date,$params){
		
		$query = self::whereBetween('date',[date('Y-m-d',strtotime($start_date)),date('Y-m-d',strtotime($end_date))]);

		$query = self::buildQuery($query,$params);
		
		$query->select([DB::raw('round(sum(cost*exchange_rate), 2) as total'), 'product_group']);

		$query->groupBy('product_group');

		return $query->get();
		
	}

	static function totalBySeller($start_date, $end_date,$params){
		
		$query = self::whereBetween('date',[date('Y-m-d',strtotime($start_date)),date('Y-m-d',strtotime($end_date))]);

		$query = self::buildQuery($query,$params);
		
		$query->select([DB::raw('round(sum(cost*exchange_rate), 2) as total'), 'seller_id']);

		$query->groupBy('seller_id');

		return $query->get();
		
	}


	static function sellerAdsByDateRange($start_date, $end_date,$params) {

		$query = self::whereBetween('date',[date('Y-m-d',strtotime($start_date)),date('Y-m-d',strtotime($end_date))]);

		$query = self::buildQuery($query,$params);

		$query->select([DB::raw('round(sum(cost*exchange_rate), 2) as total'), 'seller_id', 'date', 'country']);

		$query->groupBy('seller_id', 'date', 'country');

		return $query->get();
	}


	static function productAdsByDateRange($start_date, $end_date,$params) {
		
		$query = self::whereBetween('date',[date('Y-m-d',strtotime($start_date)),date('Y-m-d',strtotime($end_date))]);

		$query = self::buildQuery($query,$params);

		$query->select([DB::raw('round(sum(cost*exchange_rate), 2) as total'), 'seller_id', 'date', 'country', 'asin']);

		$query->groupBy('seller_id', 'date', 'country', 'asin');

		return $query->get();

	}

	static function adsWithNoSellProductsByDate($start_date, $end_date, $params, $asins) {

		$query = self::whereBetween('date',[date('Y-m-d',strtotime($start_date)),date('Y-m-d',strtotime($end_date))]);

		$query = self::buildQuery($query,$params);

		$query->select([DB::raw('round(sum(cost*exchange_rate), 2) as total'), 'asin', 'country']);

		$query->where('cost', '>', 0);
		$query->whereNotIn('asin', $asins);
		$query->groupBy('asin', 'country');

		# get products with ad record yet with no sells
		$results = $query->get ();

		return $results;

	}

	static function buildQuery($query,$params=[]) {
		
		if(!empty($params)) {
				if(isset($params['account_id']) && !empty($params['account_id']) && $params['account_id'] != 'all') {
					$query->where('ad_reports.seller_id',$params['account_id']);
				}
				
				
				
				if(isset($params['sku']) && !empty($params['sku'])) {
					$query->where('ad_reports.sku',$params['sku']);
				}
				
				
				if(isset($params['asin']) && !empty($params['asin'])) {
					$query->where('ad_reports.asin',$params['asin']);
				}

				if(isset($params['country']) && !empty($params['country'])) {
					if($params['country'] == 'UK') {
						$countries = ['UK', 'DE', 'ES', 'FR', 'IT'];
					} else {
						$countries = [$params['country']];
					}
					$query->whereIn('ad_reports.country',$countries);
				}

				if(isset($params['asins']) && !empty($params['asins'])) {
					$query->whereIn('ad_reports.asin',$params['asins']);
				}

				if( isset($params['products']) && !empty($params['products']) ) {
					$query->whereHas('listing',function($q) use ($params){
						$q->whereIn('asin',$params['products']);
					});
					
				}

				if(isset($params['marketplace']) && !empty($params['marketplace']) && $params['marketplace']!='all') {
					$country = array_search($params['marketplace'], config('app.marketplaces'));
					$query->where('ad_reports.country',$country);
				}
	
		}
		
		return $query;
	}


	public static function getProducts($params) {

		$dataProvider = AdReport::leftJoin('products', function($join)
                         {
                             $join->on('ad_reports.asin', '=', 'products.asin');
                             $join->on('ad_reports.country', '=', 'products.country');

                         });

		$dataProvider->leftJoin('product_users', function($join)
			              {
                             $join->on('products.id', '=', 'product_users.product_id');
                         });

		$dataProvider->leftJoin('users', function($join)
			              {
                             $join->on('product_users.user_id', '=', 'users.id');
                         });


		if(isset($params['seller_id']) && $params['seller_id'] && $params['seller_id'] != 'all') {
			$dataProvider->where('ad_reports.seller_id', $params['seller_id']);

		}

		if(isset($params['country']) && $params['country'] && $params['country'] != 'all') {	
			$dataProvider->where('ad_reports.country', $params['country']);
		}

		if(isset($params['start_date']) && isset($params['end_date'])) {	
			$dataProvider->whereBetween('ad_reports.date', [$params['start_date'], $params['end_date']]);
		}

		if(isset($params['campaignId']) && $params['campaignId']) {	
			$dataProvider->where('ad_reports.campaignId', $params['campaignId']);
		}

		if(isset($params['adGroupId']) && $params['adGroupId']) {	
			$dataProvider->where('ad_reports.adGroupId', $params['adGroupId']);
		}

		if(isset($params['keywordId']) && $params['keywordId']) {	
			$adGroupId = AdKeyword::where('keywordId', $params['keywordId'])->first()->adGroupId;
			$dataProvider->where('ad_reports.adGroupId', $adGroupId);
		}


		if(isset($params['campaignIds']) && $params['campaignIds']) {	
			$count = count(json_decode($params['campaignIds']));
			if($count == 1) {
				$dataProvider->where('ad_reports.campaignId', json_decode($params['campaignIds'])[0]);
			} elseif($count > 1) {
				$dataProvider->whereIn('ad_reports.campaignId', json_decode($params['campaignIds']));
			}
		}

		if(isset($params['adGroupIds']) && $params['adGroupIds']) {	
			$count = count(json_decode($params['adGroupIds']));
			if($count == 1) {
				$dataProvider->where('ad_reports.adGroupId', json_decode($params['adGroupIds'])[0]);
			} elseif($count > 1) {
				$dataProvider->whereIn('ad_reports.adGroupId', json_decode($params['adGroupIds']));
			}
			
		}


		$dataProvider->select(DB::raw('users.name as product_manager, ad_reports.*, products.name, products.small_image, products.id as product_id, SUM(ad_reports.clicks) as total_clicks, SUM(ad_reports.impressions) as total_impressions, ROUND(SUM(ad_reports.cost * ad_reports.exchange_rate), 2) as total_cost, SUM(ad_reports.attributedSales30d) as total_sales, SUM(ad_reports.attributedConversions30d) as total_orders'));

		$dataProvider->groupBy('ad_reports.asin', 'ad_reports.country');

		$dataProvider->orderBy('total_cost', 'desc');

		$data = $dataProvider->get();


		$asin_profits = self::getAsinProfits($params);

		foreach ($data as $key => $value) {
			$value->total_clicks = (float)$value->total_clicks;
			$value->total_impressions = (float)$value->total_impressions;
			$value->total_cost = (float)$value->total_cost;
			$value->total_sales = (float)$value->total_sales;
			$value->total_orders = (float)$value->total_orders;

			$value->product_link = route ( "product.view", [ "id" => $value->product_id ] );
			$value->amz_link = Product::amzLink($value->asin, $value->country);

			$value->acos = (isset($value->total_sales) && $value->total_sales > 0) ? round(($value->total_cost / $value->total_sales) * 100, 2) : "0.0";
			$value->cpc = (isset($value->total_clicks) && $value->total_clicks > 0) ? round($value->total_cost / $value->total_clicks, 2) : 0;
			$value->ctr = (isset($value->total_impressions) && $value->total_impressions > 0) ? round(($value->total_clicks / $value->total_impressions) * 100, 2) : 0;
			$value->cr = (isset($value->total_clicks) && $value->total_clicks > 0) ? round(($value->total_orders / $value->total_clicks) * 100, 2) : 0;
			$value->profit = isset($asin_profits[$value->asin][$value->country]) ? $asin_profits[$value->asin][$value->country] : 0;
		}


		return $data;

	}


	static function getAsinProfits($params) {
				
		$products_params = [];
		$asin_profits = [];
		if(isset($params['start_date']) && isset($params['end_date'])) {	

			if(isset($params['seller_id']) && $params['seller_id'] && $params['seller_id'] != 'all') {
				$products_params['account_id'] = $params['seller_id'];
			}
			if(isset($params['country']) && $params['country'] && $params['country'] != 'all') {	
				$products_params['marketplace'] = $params['marketplace'];
			}

			$bestsellers = Order::bestSellersByDate($params['start_date'], $params['end_date'], $products_params);

			$asins = array_pluck($bestsellers, 'ASIN');
			$asin_params = ['account_id'=>$params['seller_id'],'marketplace'=>$params['country'],'asins'=>$asins];
			$ad_expenses = AdReport::totalByDateRangeAsinCountry($params['start_date'], $params['end_date'], $asin_params);

			foreach ($bestsellers as $bestseller) {

				foreach($ad_expenses as $ad_expense) {
					if($ad_expense->asin == $bestseller->ASIN and $bestseller->region == $ad_expense->country) {
						$adExpenses = $ad_expense->total;
						break;
					}
				}

	    		$total_gross = isset($bestseller->total_gross) ? $bestseller->total_gross : 0;
	    		$shipping_fee = isset($bestseller->shipping_fee) ? $bestseller->shipping_fee : 0;
	    		$tax = isset($bestseller->tax) ? $bestseller->tax : 0;
	    		$commission = isset($bestseller->commission) ? $bestseller->commission : 0;
	    		$fba_fee = isset($bestseller->fba_fee) ? $bestseller->fba_fee : 0;
	    		$total_promo = isset($bestseller->total_promo) ? $bestseller->total_promo : 0;
	    		$total_cost = isset($bestseller->total_cost) ? $bestseller->total_cost : 0;
	    		$total_units = isset($bestseller->total_units) ? $bestseller->total_units : 0;
	    		$total_orders = isset($bestseller->total_orders) ? $bestseller->total_orders : 0;
	    		$promo_count = isset($bestseller->promo_count) ? $bestseller->promo_count : 0;
	    		$total_refund = isset($bestseller->total_refund) ? $bestseller->total_refund : 0;

	    		$payout = $total_gross + $shipping_fee + $tax + $commission + $fba_fee + $total_promo + $total_refund;
	    		$cost = $total_cost + $adExpenses;
	    		$profit = $payout - $cost;

				$asin_profits[$bestseller->ASIN][$bestseller->region] = round($profit, 2);
			}

		}

		return $asin_profits;

	}

	static function summaryByAsinAndCountry($start_date, $end_date, $asin, $country) {
		DB::enableQueryLog();
		$query = AdReport::whereBetween ( 'date', [ $start_date, $end_date ] )
										->select ( DB::raw ( 'asin, country, sum(attributedConversions7d) as ppc_count, sum(attributedSales7d) as ads_sales' ) )
										->where('asin', $asin)
										->where('country', $country )
										->groupBy('asin', 'country');
		$data = $query->get ()->first();
		return $data;
	}

	public static function getAggregatedAdReports($params) {
		$adReports = self::getAdReports($params);
		return self::aggregateAdReports($adReports);
	}

	public static function aggregateAdReports($adReports) {
		$adCost = DataHelper::aggregate($adReports, function($adReport) {
			return $adReport->cost * $adReport->exchange_rate;
		}, 'sum');
		$adOrders = DataHelper::aggregate($adReports, 'attributedConversions7d', 'sum');
		$adSales = DataHelper::aggregate($adReports, 'attributedSales7d', 'sum');

		return [
			'cost' => empty($adCost) ? 0 : round($adCost, 2),
			'orders' => empty($adOrders) ? 0 : $adOrders,
			'sales' => empty($adSales) ? 0 : $adSales
		];
	}

	public static function getAdReports($params) {
		$queryConds = self::getQuery($params);
		return self::where($queryConds)->get();
	}

	public static function getQuery($params) {
		$query = [];

		if (isset($params['seller_id']) && !empty($params['seller_id'])) {
			array_push($query, ['seller_id', '=', $params['seller_id']]);
		}

		if (isset($params['asin']) && !empty($params['asin'])) {
			array_push($query, ['asin', '=', $params['asin']]);
		}

		if (isset($params['sku']) && !empty($params['sku'])) {
			array_push($query, ['sku', '=', $params['sku']]);
		}

		if (isset($params['date']) && !empty($params['date'])) {
			if (is_array($params['date'])) {
				if (isset($params['date']['from_date']) && !empty($params['date']['from_date'])) {
					array_push($query, ['date', '>=', $params['date']['from_date']]);
				}

				if (isset($params['date']['to_date']) && !empty($params['date']['to_date'])) {
					array_push($query, ['date', '<=', $params['date']['to_date']]);
				}
			} else {
				array_push($query, ['date', '=', $params['date']]);
			}
		}

		return $query;
	}

	public static function boot()
  {
		parent::boot();

		self::saving(function($model) {
			if ($model->cost == 1900) {
				$model->cost = 0;
			}
		});
		
	}
	
}
	