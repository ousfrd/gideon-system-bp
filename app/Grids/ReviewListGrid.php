<?php
namespace App\Grids;

use Illuminate\Support\Facades\Config;
use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

use App\Product;
use App\Review;
use DB;
use App\Excessnventory;
use App\Helpers\ReviewHelper;
use Illuminate\Support\Facades\Input;
use App\Account;

class ReviewListGrid extends GeneralGrid  {
	protected $_name = 'reviews';
	protected $_pageLimit = 100;
	function __construct($name = null, $limit = 50) {
		if ($name != null) {
			$this->_name = $name;
		}
		
		$this->_pageLimit = $limit;
		
		return $this;
	}
	

	static function grid($request=null,$params=[]) {
		$dataProvider = Review::where('asin', $params['asin'])->orderBy ( "created_at", "asc" );
		$gridConfig = new GridConfig ();
		
		$gridConfig
		->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'Reviews' )->setPageSize ( 20 )
		->setColumns ( [
				// SampleRequestGrid::productManagerField(),
				ReviewListGrid::asinField(),
				ReviewListGrid::reviewDateField(),
				ReviewListGrid::verifiedPurchaseField(),
				ReviewListGrid::profileNameField(),
				ReviewListGrid::reviewTitleField(),
				ReviewListGrid::reviewRatingField(),
				ReviewListGrid::reviewIdField(),
				ReviewListGrid::reviewLinkField(),
				ReviewListGrid::imageField(),
				ReviewListGrid::videoField(),
				ReviewListGrid::reviewExistField(),
				ReviewListGrid::reviewTextField(),
				ReviewListGrid::reviewTypeField(),
		] )
		->setComponents ( [
				GeneralGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] ) ;
		
		
		$grid = new Grid ($gridConfig);
		
		return $grid;

	}


	static function titleField() {
		return (new FieldConfig ())->setName ( 'title' )->setLabel ( 'title' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) );
	}

	static function asinField() {
		return (new FieldConfig ())->setName ( 'asin' )->setLabel ( 'asin' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->
				setSortable ( true );
	}


	static function reviewDateField() {
		return (new FieldConfig ())->setName ( 'review_date' )->setLabel ( 'Review Date' )->setSortable ( true )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_GTE )->setTemplate ( "grid-components.date" )
					->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
						$dp->getBuilder ()->where ( 'review_date' , '>=', date('Y-m-d',strtotime($value)) );
				} ) )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LSE )->setTemplate ( "grid-components.date" )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
					$dp->getBuilder ()->where ( 'review_date', '<=', date('Y-m-d',strtotime($value)));
				} ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					
					return $val;
					
				});
	}

	static function verifiedPurchaseField() {
		return (new FieldConfig ())->setName ( 'verified_purchase' )->setLabel ( 'Verified Purchase' )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'verified_purchase' )->setSubmittedOnChange ( true )-> 
			setOptions ( [ 
					"1" => "True",
					"0" => "False"
				] ) )->setCallback ( function ($val, ObjectDataRow $row) {
				return $val == 1 ? "True" : "False";
			});
	}

	static function profileNameField() {
		return (new FieldConfig ())->setName ( 'profile_name' )->setLabel ( 'profile_name' )->setSortable ( true )->addFilter ( (new SelectFilterConfig ()));
	}

	static function reviewTitleField() {
		return (new FieldConfig ())->setName ( 'review_title' )->setLabel ( 'review_title' )->setSortable ( true );
	}

	static function reviewTextField() {
		return (new FieldConfig ())->setName ( 'review_text' )->setLabel ( 'review_text' )->setSortable ( true );
	}
	static function reviewRatingField() {
		return  (new FieldConfig ())->setName ( 'review_rating' )->setLabel ( 'Review Rating' )->addFilter ( (new SelectFilterConfig ())->setName ( 'review_rating' )->setMultipleMode ( true )->setSubmittedOnChange ( true )-> 
			setOptions ( [ 
					"1" => "1 Star",
					"2" => "2 Stars",
					"3" => "3 Stars",
					"4" => "4 Stars",
					"5" => "5 Stars"
				] ) );
	}
	static function reviewIdField() {
		return (new FieldConfig ())->setName ( 'review_id' )->setLabel ( 'review_id' )->setSortable ( true );
	}
	static function reviewLinkField() {

		return (new FieldConfig ())->setName ( 'review_link' )->setLabel ( 'review_link' )->setSortable ( true )->setCallback ( function ($val, ObjectDataRow $row) {
			$p = '/gp\/customer-reviews\/(.*?)\/ref/';
			preg_match($p, $val, $match);
			return "<a href='" . $val . "' target='_blank'>".$match[1]."</a>" ;
		});
	}

	static function imageField() {
		return (new FieldConfig ())->setName ( 'image' )->setLabel ( 'image' )->setSortable ( true );
	}
	static function videoField() {
		return (new FieldConfig ())->setName ( 'video' )->setLabel ( 'video' )->setSortable ( true );
	}
	static function reviewExistField() {
		return (new FieldConfig())->setName ( 'review_exist' )->setLabel ( 'Review Exist' )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'review_exist' )->setSubmittedOnChange ( true )-> 
			setOptions ( [ 
					"1" => "True",
					"0" => "False"
				] ) )->setCallback ( function ($val, ObjectDataRow $row) {
				return $val == 1 ? "True" : "False";
			})

		->setCallback(function ($val, ObjectDataRow $row) {
			if (Gate::allows('self-review')) {
				return '<input value="1" data-size="small" type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" ' . 
									' data-pk="' . $row->getCellValue('review_id') . '"' . 
									' data-url="'.route('review.updateReviewStatus').'" '.($row->getCellValue('review_exist') ? 'checked' : '') .
									' id="review_status"' . ' />';
			} else {
				return $row->getCellValue('statreview_existus') ? 'Active' : 'Inactive';
			}
			});
	}

	static function reviewTypeField() {
		// return (new FieldConfig())->setLabel ( 'Review Category' );
		return (new FieldConfig())
		->setName ( 'category' )
		->setLabel ( 'Review Category' )
		->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) );
    }

}

