<?php

namespace App\Grids;

use App\ClaimReviewCampaign;
use App\GiftCardTemplate;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;


class ClaimReviewCampaignGrid extends GeneralGrid {
  protected $_pageLimit = 100;
  protected $_query = NULL;
  protected $_name = '';
	function __construct($query, $name = 'claim-review-campaign-report', $limit = 100) {
    if (!$query) {
      $this->_query = Shipment::whereNotNull("amazon_order_id");
    } else {
      $this->_query = $query;
    }
    $this->_name = $name;
    $this->_pageLimit = $limit;
		return $this;
  }

  function generateGrid() {
    $gridConfig = new GridConfig();
    $gridConfig->setName($this->_name)
               ->setDataProvider(new EloquentDataProvider($this->_query))
               ->setCachingTime(0) 
               ->setPageSize ( $this->_pageLimit )
               ->setColumns ( [
                (new FieldConfig)
                  ->setName('name')
                  ->setLabel('Name')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('name')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  )
                  ->setCallback(function($val, ObjectDataRow $row) {
                    $campaign = $row->getSrc();
                    return '<a target="_blank" href="'.route('claim-review-campaign.show', ['claimReviewCampaign' => $campaign]) .'">' . $val . '</a>';
                  }),

                (new FieldConfig)
                  ->setName('platform')
                  ->setLabel('Supplier')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('platform')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  ),
                (new FieldConfig)
                  ->setName('jobId')
                  ->setLabel('Click2mail JobId')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('jobId')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  )
                  ->setCallback(function($val, ObjectDataRow $row) {
                    $campaign = $row->getSrc();
                    if (isset($campaign->note) && !empty($campaign->note)) {
                      $note = json_decode($campaign->note, TRUE);
                      if (isset($note['id'])) {
                        $jobId = $note['id'];
                      } else {
                        $jobId = '';
                      }
                      return $jobId;
                    }
                }),
                (new FieldConfig)
                  ->setName('cashback')
                  ->setLabel('Cashback')
                  ->setSortable(true),
                GeneralGrid::countryField('marketplace', 'Marketplace'),
                (new FieldConfig)
                  ->setName('asin')
                  ->setLabel('ASIN')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('claim_review_campaigns.asin')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  ),
                (new FieldConfig)
                  ->setName('channel')
                  ->setLabel('Channel')
                  ->addFilter ( 
                    (new SelectFilterConfig ())
                      ->setName ( 'channel' )
                      ->setSubmittedOnChange ( true )
                      ->setOptions (array_flip(ClaimReviewCampaign::CHANNELS))
                      ->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
                        if ($value) {
                          $dp->getBuilder()->where('channel', $value);
                        }
                      })
                  ),
                (new FieldConfig)
                  ->setName('order_quantity')
                  ->setLabel('Order Quantity'),
                (new FieldConfig)
                  ->setName('actual_sent_amount')
                  ->setLabel('Actual Sent Amount')
                  ->setCallback(function($val, ObjectDataRow $row) {
                    $campaign = $row->getSrc();
                    $campaignId = $campaign->id;
                    if (empty($campaign->sent_at)) {
                      $url = route('claim-review-campaign.ajaxUpdate', ['id' => $campaignId]);
                      return '<a data-type="text" class="pUpdate" ' . 
                          ' data-pk="actual_sent_amount"' .
                          ' data-url="' . $url . '"' . 
                          ' data-value="' . $val .'" data-title="">' . $val. '</a>';
                    } else {
                      if (isset($campaign->note) && !empty($campaign->note)) {
                        $note = json_decode($campaign->note, TRUE);
                        if (isset($note['productionCost']) && isset($note['productionCost']['quantity'])) {
                          $quantity = $note['productionCost']['quantity'];
                          return $quantity;
                        }
                      } else {
                        return $val;
                      }
                    }
                    
                }),
                (new FieldConfig)
                  ->setName('response_quantity')
                  ->setLabel('Response')
                  ->setSortable(true),
                (new FieldConfig)
                  ->setName('response_rate')
                  ->setLabel('Response Rate %')
                  ->setSortable(true)
                  ->setCallback(function($val, ObjectDataRow $row) {
                    return $val * 100;
                  }),
                (new FieldConfig)
                  ->setName('positive_response_rate')
                  ->setLabel('Positive Response Rate %')
                  ->setSortable(true)
                  ->setCallback(function($val, ObjectDataRow $row) {
                    return $val * 100;
                  }),
                (new FieldConfig)
                  ->setName('balance_before')
                  ->setLabel('Balance Before')
                  ->setSortable(true)
                  ->setCallback(function($val, ObjectDataRow $row) {
                    return $val;
                }),
                (new FieldConfig)
                  ->setName('balance_after')
                  ->setLabel('Balance After')
                  ->setSortable(true)
                  ->setCallback(function($val, ObjectDataRow $row) {
                    return $val;
                }),
                (new FieldConfig)
                  ->setName('total_cost')
                  ->setLabel('Total Cost')
                  ->setSortable(true)
                  ->setCallback(function($val, ObjectDataRow $row) {
                    return $val;
                }),
                (new FieldConfig)
                  ->setName('unit_price')
                  ->setLabel('Unit Price')
                  ->setSortable(true)
                  ->setCallback(function($val, ObjectDataRow $row) {
                    return $val;
                }),
                (new FieldConfig)
                  ->setLabel('Date Range')
                  ->setCallback(function ($val, ObjectDataRow $row) {
                    $claimReviewCampaign = $row->getSrc();
                    return $claimReviewCampaign->start_date . ' TO ' . $claimReviewCampaign->end_date;
                  }),
                (new FieldConfig)
                  ->setLabel('Created By')
                  ->setName('creator'),
                (new FieldConfig)
                  ->setLabel('Created At')
                  ->setName('created_at'),
                (new FieldConfig)
                  ->setLabel('Sent At')
                  ->setName('sent_at')
                  ->setSortable(true),
                (new FieldConfig)
                  ->setLabel('Gift Card Template')
                  ->setCallBack(function ($val, ObjectDataRow $row) {
                    $name = $row->getCellValue('gift_card_template_name');
                    $id = $row->getCellValue('gift_card_template_id');
                    return '<a target="_blank" href="' . route('gift-card-template.show', ['id' => $id]). '">' .$name. '</a>';
                  }),
                (new FieldConfig)
                  ->setLabel('Gift Card Front')
                  ->setCallback(function ($val, ObjectDataRow $row) {
                    $design = $row->getCellValue('front_design');
                    if ($design) {
                      return "<img class='small-thumbnail' src='". GiftCardTemplate::getUrl($design)."' />";
                    } else {
                      return "None";
                    }
                  }),
                  (new FieldConfig)
                    ->setLabel('Gift Card Back')
                    ->setCallback(function ($val, ObjectDataRow $row) {
                      $design = $row->getCellValue('back_design');
                      if ($design) {
                        return "<img class='small-thumbnail' src='". GiftCardTemplate::getUrl($design)."' />";
                      } else {
                        return "None";
                      }
                    }),
                (new FieldConfig)
                  ->setLabel('Action')
                  ->setCallback(function ($val, ObjectDataRow $row) {
                    $claimReviewCampaign = $row->getSrc();
                    if (empty($claimReviewCampaign->sent_at)) {
                      return '<a class="btn btn-primary" href="'. route('claim-review-campaign.show', ['claimReviewCampaign'=>$claimReviewCampaign]). '">View</a> ' . 
                          '<a class="btn btn-success edit" href="'. route('claim-review-campaign.edit', ['claimReviewCampaign'=>$claimReviewCampaign]). '">Edit</a> ' .
                          '<button type="button" class="btn btn-warning send" data-remote-method="put" data-toggle="modal"  data-target="#comfirm-send" data-remote-url="' .route('claim-review-campaign.send', ['id' => $claimReviewCampaign->id]). '">Send</button> '.
                          '<button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#confirmDelete" data-campaign-id="' . $claimReviewCampaign->id . '" data-campaign-name="' . $claimReviewCampaign->name . '">Delete</button>';
                    } else {
                      return '<a class="btn btn-primary" href="'. route('claim-review-campaign.show', ['claimReviewCampaign'=>$claimReviewCampaign]). '">View</a> ';
                    }
                    
                  })
               ])
               ->setComponents ( [
                  ClaimReviewCampaignGrid::headerComponent(),
                  GeneralGrid::footerComponenet()
               ] ) ;
    
    $grid = new Grid ($gridConfig);
    if (empty($grid->getInputProcessor()->getInput())) {
      $gridConfig->setDataProvider((new EloquentDataProvider($this->_query))->orderBy('id', 'desc'));
    };
		return $grid;
  }
  

}