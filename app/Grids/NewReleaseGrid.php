<?php

namespace App\Grids;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use App\NewRelease;

class NewReleaseGrid extends GeneralGrid {
	protected $_name = 'newreleases';
	protected $_pageLimit = 100;
	function __construct($name = null, $limit = 50) {
		if ($name != null) {
			$this->_name = $name;
		}
		
		$this->_pageLimit = $limit;
		
		return $this;
	}
	
	static function grid() {
		$dataProvider = NewRelease::orderBy( "created_at", "asc" );

		$gridConfig = new GridConfig ();
		
		$gridConfig
		->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'NewReleases' )->setPageSize ( 20 )
		->setColumns ( [
				// SampleRequestGrid::productManagerField(),
				NewReleaseGrid::titleField(),
				NewReleaseGrid::websiteField(),
				NewReleaseGrid::imageField(),
				NewReleaseGrid::linkField(),
				NewReleaseGrid::priceField(),
				NewReleaseGrid::createdatField(),
				NewReleaseGrid::editField(),				
		] )
		->setComponents ( [
				GeneralGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] ) ;
		
		
		$grid = new Grid ($gridConfig);
		
		return $grid;

	}


	

	static function titleField() {
		return (new FieldConfig ())->setName ( 'title' )->setLabel ( 'Title' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->
				setSortable ( true );
	}

	static function websiteField() {
		return (new FieldConfig ())->setName ( 'website' )->setLabel ( 'Website' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->
				setSortable ( true );
	}

	static function imageField() {
		return (new FieldConfig ())->setName ( 'image' )->setLabel ( 'Image' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->
				setSortable ( true );
	}

	static function linkField() {
		return (new FieldConfig ())->setName ( 'link' )->setLabel ( 'Link' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->
				setSortable ( true );
	}

	static function priceField() {
		return (new FieldConfig ())->setName ( 'price' )->setLabel ( 'Price' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->
				setSortable ( true );
	}	

	static function createdatField() {
		return (new FieldConfig ())->setName ( 'created_at' )->setLabel ( 'Date' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_GTE )->setTemplate ( "grid-components.date" )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
					$dp->getBuilder ()->where ( DB::raw ( 'date(created_at)' ), '>=', $value );
				} ) )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LSE )->setTemplate ( "grid-components.date" )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
					$dp->getBuilder ()->where ( DB::raw ( 'date(created_at)' ), '<=', $value );
				} ) );
	}

	static function editField() {
		return (new FieldConfig ())->setName ( 'id' )->setLabel ( 'Actions' )->setSortable ( true )->setCallback ( function ($val, ObjectDataRow $row) {
					$newrelease = $row->getSrc ();
					return ' <a data-toggle="modal"  data-target="#editModal" class="modal-a" href="' . route ( "user.edit", [ "id" => $newrelease->id ] ) . '">Edit</a>';
				} );
	}	

	


}