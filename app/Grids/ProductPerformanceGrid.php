<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Grids;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;

use App\Grids\CollectionDataProvider;


class ProductPerformanceGrid extends GeneralGrid {
	public static function grid($productPerformances) {
		$gridConfig = new GridConfig();

		$gridConfig->setDataProvider(new CollectionDataProvider($productPerformances))
			->setName('ProductPerformance')
			->setPageSize($productPerformances->count())
			->setColumns([
				self::imageField(),
	            self::asinField(),
	            self::aliasField(),
	            self::pmField(),
	            self::roiField(),
	            self::roi2Field(),
	            self::profitField(),
	            self::payoutField(),
	            self::totalRevenueField(),
	            self::totalCostField(),
	            self::totalGrossField(),
	            self::shippingFeeField(),
	            self::vatFeeField(),
	            self::fbaFeeField(),
	            self::totalRefundField(),
	            self::taxField(),
	            self::commissionField(),
	            self::giftwrapFeeField(),
	            self::otherFeeField(),
	            self::adExpensesField(),
	            self::totalOrdersField(),
	            self::totalUnitsField(),
	            self::refundCountField(),
	            self::ppcCountField(),
	            self::adSalesField(),
	            self::priceField(),
			])->setComponents([
	            self::headerComponent(),
				GeneralGrid::footerComponenet()
			]);

		return new Grid($gridConfig);
	}
	
	public static function imageField() {
		return (new FieldConfig())
			->setName('small_image')
			->setLabel('Image')
			->setCallback(function ($val, ObjectDataRow $row) {
				$product = $row->getSrc();
				$amzUrl = $product->amzLink;
				$imageLink = $product->small_image;
				if (empty($imageLink)) {
					$imageLink = "/images/No_Image_Available.jpg";
				}

				return '<a  href="' . $amzUrl . '" target="_blank" data-placement="right" data-toggle="popover"  data-content="' . $product->name . '"><img src="' . $imageLink . '" style="max-width:100px;max-height:100px"/></a>';
			});
	}
	
	public static function asinField() {
		return (new FieldConfig())
			->setName('asin')
			->setLabel('ASIN')
			->setCallback(function ($val, ObjectDataRow $row) {
				$product = $row->getSrc();
				$url = route("product.view", ["id" => $product->id]);

				return '<a  href="' . $url . '">' . $product->asin . '</a> <br />' . $product->country;
			});
	}
	
	public static function aliasField() {
		return  (new FieldConfig())
			->setName('alias')
			->setLabel('Alias');
	}

	public static function pmField() {
		return  (new FieldConfig())
			->setName('PM')
			->setLabel('PM');
	}

	public static function roiField() {
		return (new FieldConfig())
			->setName('roi')
			->setLabel('ROI')
			->setSortable(TRUE);
	}

	public static function roi2Field() {
		return (new FieldConfig())
			->setName('roi2')
			->setLabel('ROI2')
			->setSortable(TRUE);
	}

	public static function profitField() {
		return (new FieldConfig())
			->setName('profit')
			->setLabel('Profit')
			->setSortable(TRUE);
	}

	public static function payoutField() {
		return (new FieldConfig())
			->setName('payout')
			->setLabel('Payout')
			->setSortable(TRUE);
	}

	public static function totalRevenueField() {
		return (new FieldConfig())
			->setName('total_revenue')
			->setLabel('Total Revenue')
			->setSortable(TRUE);
	}

	public static function totalCostField() {
		return (new FieldConfig())
			->setName('total_cost')
			->setLabel('Total Cost')
			->setSortable(TRUE);
	}

	public static function totalGrossField() {
		return (new FieldConfig())
			->setName('total_gross')
			->setLabel('Total Gross')
			->setSortable(TRUE);
	}

	public static function shippingFeeField() {
		return (new FieldConfig())
			->setName('shipping_fee')
			->setLabel('Shipping')
			->setSortable(TRUE);
	}

	public static function vatFeeField() {
		return (new FieldConfig())
			->setName('vat_fee')
			->setLabel('VAT')
			->setSortable(TRUE);
	}

	public static function fbaFeeField() {
		return (new FieldConfig())
			->setName('fba_fee')
			->setLabel('FBA')
			->setSortable(TRUE);
	}

	public static function totalRefundField() {
		return (new FieldConfig())
			->setName('total_refund')
			->setLabel('Total Refund')
			->setSortable(TRUE);
	}

	public static function taxField() {
		return (new FieldConfig())
			->setName('tax')
			->setLabel('TAX')
			->setSortable(TRUE);
	}

	public static function commissionField() {
		return (new FieldConfig())
			->setName('commission')
			->setLabel('Commission')
			->setSortable(TRUE);
	}

	public static function giftwrapFeeField() {
		return (new FieldConfig())
			->setName('giftwrap_fee')
			->setLabel('Giftwrap Fee')
			->setSortable(TRUE);
	}

	public static function otherFeeField() {
		return (new FieldConfig())
			->setName('other_fee')
			->setLabel('Other Fee')
			->setSortable(TRUE);
	}

	public static function adExpensesField() {
		return (new FieldConfig())
			->setName('adExpenses')
			->setLabel('AD Fee')
			->setSortable(TRUE);
	}

	public static function totalOrdersField() {
		return (new FieldConfig())
			->setName('total_orders')
			->setLabel('Orders')
			->setSortable(TRUE);
	}

	public static function totalUnitsField() {
		return (new FieldConfig())
			->setName('total_units')
			->setLabel('Units')
			->setSortable(TRUE);
	}

	public static function refundCountField() {
		return (new FieldConfig())
			->setName('refund_count')
			->setLabel('Refund Cnt')
			->setSortable(TRUE);
	}

	public static function ppcCountField() {
		return (new FieldConfig())
			->setName('ppc_count')
			->setLabel('PPC Cnt')
			->setSortable(TRUE);
	}

	public static function adSalesField() {
		return (new FieldConfig())
			->setName('ads_sales')
			->setLabel('AD Sales')
			->setSortable(TRUE);
	}

	public static function priceField() {
		return (new FieldConfig())
			->setName('price')
			->setLabel('Price')
			->setSortable(TRUE);
	}

	public static function headerComponent() {
		return (new THead())->setComponents([
				(new ColumnHeadersRow()),
				(new FiltersRow()),
				(new OneCellRow())->setRenderSection(RenderableRegistry::SECTION_END)
					->setComponents([
						(new RecordsPerPage())->setVariants([20, 50, 100, 500, 1000]),
						new ColumnsHider(),
						(new CsvExport())->setFileName('ProductPerformance-' . date('Y-m-d')),
						(new HtmlTag())->setContent('<span class="glyphicon glyphicon-refresh"></span>Filter')
							->setTagName('button')
							->setRenderSection(RenderableRegistry::SECTION_END)
							->setAttributes(['class' => 'btn btn-success btn-sm'])
					]),
			]);
	}
}