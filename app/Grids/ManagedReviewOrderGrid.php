<?php

namespace App\Grids;

use App\ClaimReviewCampaign;
use App\GiftCardTemplate;
use App\User;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;


class ManagedReviewOrderGrid extends GeneralGrid {
  protected $_pageLimit = 100;
  protected $_query = NULL;
  protected $_name = '';
	function __construct($query, $name = 'managed-review-orders', $limit = 100) {
    if (!$query) {
      $this->_query = Shipment::whereNotNull("amazon_order_id");
    } else {
      $this->_query = $query;
    }
    $this->_name = $name;
    $this->_pageLimit = $limit;
		return $this;
  }

  function generateInsideBuyerGrid() {
    $columns = [
      $this->amazonOrderIdColumn(),
      $this->sellerIdColumn(),
      $this->asinColumn(),
      $this->orderTypeColumn(),
      $this->orderCostColumn(),
      $this->orderDateColumn(),
      $this->estimatedDeliveryDateColumn(),
      $this->reviewLinkColumn(),
      $this->reviewTitleColumn(),
      $this->reviewContentColumn(),
      $this->leftReviewAtColumn(),
      $this->actionColumn()
    ];
    $grid = $this->generateGrid($columns);
    return $grid;
  }

  function generateOverviewGrid() {
    $columns = [
      $this->createdByColumn(),
      $this->amazonOrderIdColumn(),
      $this->buyerNickNameColumn(),
      $this->buyerDeviceNameColumn(),
      $this->buyerBrowserColumn(),
      $this->asinColumn(),
      $this->orderTypeColumn(),
      $this->orderCostColumn(),
      $this->orderDateColumn(),
      $this->estimatedDeliveryDateColumn(),
      $this->reviewTitleColumn(),
      $this->reviewLinkColumn(),
      $this->reviewContentColumn(),
      $this->leftReviewAtColumn()
    ];
    $grid = $this->generateGrid($columns);
    return $grid;
  }

  function generateGrid($columns) {
    $gridConfig = new GridConfig();
    $gridConfig->setName($this->_name)
               ->setDataProvider(new EloquentDataProvider($this->_query))
               ->setCachingTime(0) 
               ->setPageSize ( $this->_pageLimit )
               ->setColumns ( $columns )
               ->setComponents ( [
                  ManagedReviewOrderGrid::headerComponent(),
                  GeneralGrid::footerComponenet()
               ] ) ;
    $grid = new Grid ($gridConfig);
		return $grid;
  }

  public function createdByColumn() {
    return (new FieldConfig)
            ->setLabel('Owner')
            ->setName('name')
            ->addFilter ( 
              (new SelectFilterConfig ())
                ->setName ( 'created_by' )
                ->setSubmittedOnChange ( true )
                ->setOptions (["0" => "All"]  + array_flip(User::selfReviewStaffs()) )
                ->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
                  if ($value > 0) {
                    $dp->getBuilder()->where('managed_review_orders.created_by', $value);
                  } else if ($value == null) {
                    if (auth()->user()->isSelfReviewStaff()) {
                      $dp->getBuilder()->where('managed_review_orders.created_by', auth()->user()->id);
                    }
                  }
                })
              );
  }
  public function amazonOrderIdColumn() {
    return (new FieldConfig)
      ->setLabel('Amazon Order Id')
      ->setName('amazon_order_id')
      ->addFilter(
        (new FilterConfig)
            ->setName('amazon_order_id')
            ->setOperator(FilterConfig::OPERATOR_EQ)
      )
      ->setCallback(function($val, $row) {
        $order = $row->getSrc();
        if ($order->seller_id) {
          return '<a target="_blank" href="'. route('order.view', [ 'id'=>$val ]) .'">' . $val . '</a>';
        } else {
          return $val;
        }
      });
  } 

  public function orderTypeColumn() {
    return (new FieldConfig)
      ->setLabel('Order Type')
      ->setName('order_type');
  } 

  public function orderCostColumn() {
    return (new FieldConfig)
      ->setLabel('Order Cost')
      ->setName('order_cost');
  } 

  public function sellerIdColumn() {
    return (new FieldConfig)
      ->setLabel('Seller Id')
      ->setName('seller_id')
      ->setCallback(function($val, $row) {
        $sellerId = $row->getCellValue('seller_id');
        $sellerName = $row->getCellValue('name');
        $sellerCode = $row->getCellValue('code');
        if ($sellerId) {
          return '<a target="_blank" href="' . route('dashboard', ['account' => $sellerId]) .'">' . $sellerName . "-" . $sellerCode . "-" . $val . '</a>';
        } else {
          return 'NOT KNOWN';
        }
      });
  }

  public function asinColumn() {
    return (new FieldConfig)
      ->setLabel('ASIN')
      ->setName('asin')
      ->addFilter(
        (new FilterConfig)
            ->setName('asin')
            ->setOperator(FilterConfig::OPERATOR_EQ)
      );
  } 
  
  public function orderDateColumn() {
    return (new FieldConfig)
      ->setLabel('Order Date')
      ->setName('order_date');
  } 

  public function estimatedDeliveryDateColumn() {
    return (new FieldConfig)
      ->setLabel('Estimated Delivery Date')
      ->setName('estimated_delivery_date');
  } 
  
  public function reviewLinkColumn() {
    return (new FieldConfig)
      ->setLabel('Review Link')
      ->setName('review_link')
      ->setCallback(function($val, $row) {
        $order = $row->getSrc();
        $url = route("managed-review-order.update", ["managed-review-order" =>  $order]);
        return '<a data-type="text" 
                    class="pUpdate"  
                    data-url="'. $url .'"
                    data-pk="review_link" 
                    data-value="'.$order->review_link.'" 
                    data-title="">'.$order->review_link.'</a>';
      });
  }

  public function leftReviewAtColumn() {
    return (new FieldConfig)
      ->setLabel('Left Review At')
      ->setName('left_review_at')
      ->setCallback(function($val, $row) {
        $order = $row->getSrc();
        $url = route("managed-review-order.update", ["managed-review-order" =>  $order]);
        if ($order->created_by == auth()->user()->id || auth()->user()->is_admin()) {
          return '<a data-type="date" 
                  class="pUpdate"
                  data-url="'. $url .'"
                  data-pk="left_review_at" 
                  data-value="'.$order->left_review_at.'" 
                  data-title="Left Review At">'.$order->left_review_at.'</a>';
        } else {
          return $val;
        }
      });
  }

  public function leftReviewByColumn() {
    return (new FieldConfig)
      ->setLabel('Left Review By')
      ->setName('left_review_by');
  }

  public function reviewTitleColumn() {
    return (new FieldConfig)
      ->setLabel('Review Title')
      ->setName('review_title')
      ->setCallback(function($val, $row) {
        $order = $row->getSrc();
        $url = route("managed-review-order.update", ["managed-review-order" =>  $order]);
        return '<a data-type="text" 
                    class="pUpdate"  
                    data-url="'. $url .'"
                    data-pk="review_title" 
                    data-value="'.$order->review_title.'" 
                    data-title="">'.$order->review_title.'</a>';
      });
  }

  public function reviewContentColumn() {
    return (new FieldConfig)
      ->setLabel('Review Content')
      ->setName('review_content')
      ->setCallback(function($val, $row) {
        $order = $row->getSrc();
        $url = route("managed-review-order.update", ["managed-review-order" =>  $order]);
        return '<a data-type="textarea" 
                    class="pUpdate"  
                    data-url="'. $url .'"
                    data-pk="review_content" 
                    data-value="'.$order->review_content.'" 
                    data-title="">'.$order->review_content.'</a>';
      });
  }

  public function buyerNickNameColumn() {
    return (new FieldConfig)
      ->setLabel('Buyer')
      ->setName('nickname')
      ->addFilter(
        (new FilterConfig)
            ->setName('nickname')
            ->setOperator(FilterConfig::OPERATOR_LIKE)
      )
      ->setCallback(function($val, $row) {
        $ownedBuyerId = $row->getCellValue('owned_buyer_id');
        if ($ownedBuyerId && $ownedBuyerId > 0) {
          return '<a href="' .route('owned-buyer.show', ['id'=> $ownedBuyerId]) . '">' . $val . '</a>';
        }
      });
  }

  public function buyerDeviceNameColumn() {
    return (new FieldConfig)
            ->setName('device_name')
            ->setLabel('Buyer Device Name');
  }

  public function buyerBrowserColumn() {
    return (new FieldConfig)
            ->setName('browser')
            ->setLabel('Buyer Browser');
  }

  public function actionColumn() {
    return (new FieldConfig)
                  ->setLabel('Action')
                  ->setCallback(function ($val, ObjectDataRow $row) {
                    $order = $row->getSrc();
                    return '<button type="button" class="btn btn-danger" 
                            data-toggle="modal" 
                            data-target="#confirmDelete" 
                            data-key-value="' . $order->amazon_order_id . '" 
                            data-url="' .route('managed-review-order.destroy', ['id'=>$order->id]). '"
                            data-key-name="order id">Delete</button>';
                  });
  }
}