<?php

namespace App\Grids;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use DB;
use App\AdKeyword;
use App\AdReport;

class NegativeAdKeywordGrid extends GeneralGrid {
	static function grid($params=[]) {
		
		$dataProvider = AdKeyword::whereIn('ad_keywords.matchType', ['negativePhrase', 'negativeExact']);


		if(isset($params['seller_id']) && $params['seller_id']) {
			$dataProvider->where('ad_keywords.seller_id', $params['seller_id']);

		}

		if(isset($params['country']) && $params['country']) {	
			$dataProvider->where('ad_keywords.country', $params['country']);
		}

		if(isset($params['campaignId']) && $params['campaignId']) {
			$dataProvider->where('ad_keywords.campaignId', $params['campaignId']);
		}

		$gridConfig = new GridConfig ();
		$gridConfig->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'NegativeAdKeywords' )
		->setPageSize ( 50 )
		->setColumns ( [
				NegativeAdKeywordGrid::stateField(),
				NegativeAdKeywordGrid::keywordTextField(),
				NegativeAdKeywordGrid::matchTypeField(),
				NegativeAdKeywordGrid::actionField(),
		] )->setComponents ( [
				NegativeAdKeywordGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] );
		
		$grid = new Grid ($gridConfig);
		
		return $grid;
	}
	
	
	static function stateField() {
		return  (new FieldConfig ())->setName ( 'state' )->setLabel ( 'State' )->setSortable ( true )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'state' )->setMultipleMode ( true )->setSubmittedOnChange ( true )
				->setOptions ( ['enabled'=>'Enabled','paused'=>'Paused','archived'=>'Archived'] )
				);
	}

	static function keywordTextField() {
		return  (new FieldConfig ())->setName ( 'keywordText' )->setLabel ( 'Keyword Text' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) );
	}

	static function matchTypeField() {
		return  (new FieldConfig ())->setName ( 'matchType' )->setLabel ( 'Match Type' )->setSortable ( true )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'matchType' )->setMultipleMode ( true )->setSubmittedOnChange ( true )
				->setOptions ( ['negativePhrase'=>'negativePhrase','negativeExact'=>'negativeExact'] )
				);
	}

	static function actionField() {
		return (new FieldConfig ())->setName ( 'id' )->setLabel ( 'Actions' )->setSortable ( true )->setCallback ( function ($val, ObjectDataRow $row) {
					return '<a  href="' . route ( "ad.campaigns.deleteCampaignNagativeKeyword", ["id" => $row->getSrc ()->campaignId, "keywordId" => $row->getSrc ()->keywordId ] ) . '">Delete</a> ';
				} );
	}





	static function headerComponent() {
		return (new THead ())->setComponents ( [

				(new ColumnHeadersRow ()),
				(new FiltersRow ()),
				
				(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [

						(new RecordsPerPage ())->setVariants ( [
								20,
								50,
								100,
								500,
								1000
						] ),
						new ColumnsHider (),
						(new ExcelExport ())->setFileName ( 'NegativeAdKeywords-' . date ( 'Y-m-d' ) ),
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
								'class' => 'btn btn-success btn-sm'
						] )
				] ),
				
		] );
	}


}