<?php

namespace App\Grids;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use App\User;

class UserGrid extends GeneralGrid {
	protected $_name = 'users';
	protected $_pageLimit = 100;
	function __construct($name = null, $limit = 50) {
		if ($name != null) {
			$this->_name = $name;
		}
		
		$this->_pageLimit = $limit;
		
		return $this;
	}
	

	static function grid() {
		$dataProvider = User::visible()->orderBy ( "created_at", "asc" );
		$gridConfig = new GridConfig ();
		
		$gridConfig
		->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'Users' )->setPageSize ( 20 )
		->setColumns ( [
				// SampleRequestGrid::productManagerField(),
				UserGrid::nameField(),
				UserGrid::emailField(),
				UserGrid::createdatField(),

				UserGrid::verifiedField(),
				UserGrid::roleField(),
				
				UserGrid::lastLoginField(),

				UserGrid::editField(),
		] )
		->setComponents ( [
				GeneralGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] ) ;
		
		
		$grid = new Grid ($gridConfig);
		
		return $grid;

	}


	static function nameField() {
		return (new FieldConfig ())->setName ( 'name' )->setLabel ( 'Name' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->
				setCallback ( function ($val, ObjectDataRow $row) {
					$user = $row->getSrc ();
					return '<a  href="' . route ( "user.view", [ 
							"id" => $user->id 
					] ) . '">' . $user->name . '</a>';
				} );
	}

	static function emailField() {
		return (new FieldConfig ())->setName ( 'email' )->setLabel ( 'Email' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->
				setSortable ( true );
	}

	static function createdatField() {
		return (new FieldConfig ())->setName ( 'created_at' )->setLabel ( 'Date' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_GTE )->setTemplate ( "grid-components.date" )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
					$dp->getBuilder ()->where ( DB::raw ( 'date(created_at)' ), '>=', $value );
				} ) )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LSE )->setTemplate ( "grid-components.date" )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
					$dp->getBuilder ()->where ( DB::raw ( 'date(created_at)' ), '<=', $value );
				} ) );
	}

	static function verifiedField() {
		return (new FieldConfig ())->setName ( 'verified' )->setLabel ( 'Verified' )->addFilter ( (new SelectFilterConfig ())->setName ( 'verified' )->setMultipleMode ( true )->setSubmittedOnChange ( true )-> // on change submit request
			setOptions ( [ 
						"0" => "No",
						"1" => "Yes" 
				] ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					$user = $row->getSrc ();
					return $user->verified ? 'Yes' : "No";
				} );
	}

	static function roleField() {
		return (new FieldConfig ())->setName ( 'role' )->setLabel ( 'Role' )->setSortable ( true )->addFilter ( (new SelectFilterConfig ())->setName ( 'role' )->setMultipleMode ( true )->setSubmittedOnChange ( true )-> // on change submit request
			setOptions ( [ 
						"Admin" => "Admin",
						"Account Manager" => "Account Manager",
						"Product Manager" => "Product Manager" 
				] ) );
	}

	static function lastLoginField() {
		return (new FieldConfig ())->setName ( 'last_login' )->setLabel ( 'Last Login At' )->setSortable ( true )->setCallback ( function ($val, ObjectDataRow $row) {
					$user = $row->getSrc ();
					if(empty( $user->last_login)) {
						return 'Never';
					}
					return date ( 'M d, Y H:i', strtotime ( $user->last_login ) );
				} );
	}

	static function editField() {
		return (new FieldConfig ())->setName ( 'id' )->setLabel ( 'Actions' )->setSortable ( true )->setCallback ( function ($val, ObjectDataRow $row) {
					$user = $row->getSrc ();
					return ' <a data-toggle="modal"  data-target="#editModal" class="modal-a" href="' . route ( "user.edit", [ "id" => $user->id ] ) . '">Edit</a>';
				} );
	}


}