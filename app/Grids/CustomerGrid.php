<?php

namespace App\Grids;
use App\Customer;
use App\Account;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;


class CustomerGrid extends GeneralGrid {
  protected $_pageLimit = 100;
  protected $_query = NULL;
  protected $_name = '';

	function __construct($query, $name = 'customers-report', $limit = 100) {
    $this->_query = $query;
    $this->_name = $name;
    $this->_pageLimit = $limit;
    $this->gridConfig = new GridConfig();
    $this->gridConfig->setName($this->_name)
               ->setDataProvider(new EloquentDataProvider($this->_query))
               ->setCachingTime(0) 
               ->setPageSize ( $this->_pageLimit )
               ->setComponents ( [
                  self::headerComponent(),
                  self::footerComponenet()
               ]) ;
		return $this;
  }

  function allGrid() {
    $this->sellers = Account::all()->keyBy('seller_id');
    $this->gridConfig->setColumns ( [
                  $this->nameField(),
                  $this->amazonEmailField(),
                  $this->emailField(),
                  $this->cityField(),
                  $this->stateField(),
                  self::countryField(),
                  $this->leavePossitiveReviewsCountField(),
                  $this->leaveNegativeReviewsCountField(),
                  $this->claimReviewsCountField(),
                  $this->inviteReviewsCountField(),
                  $this->boughtItemsCountField(),
                  $this->boughtItemsField()
               ]);
    $grid = new Grid ($this->gridConfig);
		return $grid;
  }

  function nameField() {
    return (new FieldConfig)
              ->setName('name')
              ->setLabel('Name')
              ->addFilter(
                (new FilterConfig)
                    ->setName('name')
                    ->setOperator(FilterConfig::OPERATOR_LIKE)
              )
              ->setCallback(function($val, ObjectDataRow $row) {
                $customer = $row->getSrc();
                return '<a target="_blank" href="' . route("customers.show", ["id"=>$customer->id]) .'">' . $val .'</a>';
              });
  }

  function emailField() {
    return (new FieldConfig)
              ->setName('email')
              ->setLabel('Real Email')
              ->addFilter(
                (new FilterConfig)
                    ->setName('email')
                    ->setOperator(FilterConfig::OPERATOR_EQ)
              );
  }
  
  function amazonEmailField() {
    return (new FieldConfig)
              ->setName('amazon_buyer_email')
              ->setLabel('Amazon Buyer Email')
              ->addFilter(
                (new FilterConfig)
                    ->setName('amazon_buyer_email')
                    ->setOperator(FilterConfig::OPERATOR_EQ)
              );
  }

  function cityField() {
    return (new FieldConfig)
              ->setName('city')
              ->setLabel('City')
              ->setSortable(true)
              ->addFilter(
                (new FilterConfig)
                    ->setName('city')
                    ->setOperator(FilterConfig::OPERATOR_EQ)
              );
  }

  function stateField() {
    return (new FieldConfig)
              ->setName('state')
              ->setLabel('State')
              ->setSortable(true)
              ->addFilter(
                (new FilterConfig)
                    ->setName('state')
                    ->setOperator(FilterConfig::OPERATOR_EQ)
              );
  }

  function customerCountryField() {
    return (new FieldConfig)
              ->setName('country')
              ->setLabel('Country')
              ->setSortable(true)
              ->addFilter(
                (new FilterConfig)
                    ->setName('country')
                    ->setOperator(FilterConfig::OPERATOR_EQ)
              );
  }

  function boughtItemsCountField() {
    return (new FieldConfig)
              ->setName('bought_orders_count')
              ->setLabel('Bought Items Count')
              ->setSortable(true);
  }

  function boughtItemsField() {
    return (new FieldConfig)
              ->setLabel('Bought Items')
              ->setCallback(function($val, ObjectDataRow $row) {
                $customer = $row->getSrc();
                $shipments = $customer->orderShipments;
                $content = '<table class="table table-bordered">';
                foreach ($shipments as $shipment) {
                  $seller = $this->sellers[$shipment->seller_id];

                  $content .= '<tr>' . 
                                '<td>'.substr($shipment->product_name, 0, 30).'...</td>' .
                                '<td>'. $shipment->asin . '<br>' . $seller->code . ' '  . $seller->name .'</td>' . 
                                '<td>'. $shipment->purchase_date . '<br>' .
                                  ($shipment->is_refunded ? '<label class="text-danger">Refunded</label>' : '<label class="text-success">Shipped</label>') .
                                '</td>' .
                              '</tr>';
                }
                $content .= '</table>';
                return $content;
              });

  }

  function leavePossitiveReviewsCountField(){
    return (new FieldConfig)
            ->setLabel('Possitive Reviews')
            ->setName('leave_possitive_reviews_count')
            ->setSortable(true);
  }

  function leaveNegativeReviewsCountField() {
    return (new FieldConfig)
            ->setLabel('Negative Reviews')
            ->setName('leave_negative_reviews_count')
            ->setSortable(true);
  }

  function claimReviewsCountField() {
    return (new FieldConfig)
            ->setLabel('Claim Reviews')
            ->setName('claim_reviews_count')
            ->setSortable(true);
  }

  function inviteReviewsCountField() {
    return (new FieldConfig)
            ->setLabel('Invite Reviews')
            ->setName('invite_reviews_count')
            ->setSortable(true);
  }
  

  

}