<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Grids;


class Paginator extends \Illuminate\Pagination\Paginator {
	public function total() {
		return $this->count();
	}
}

