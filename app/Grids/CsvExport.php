<?php 
namespace App\Grids;

use Nayjest\Grids\Components\CsvExport as GridCsvExport;

class CSVExport extends GridCsvExport{
	
	protected function renderCsv()
	{
		$file = fopen('php://output', 'w');
		
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="'. $this->getFileName() .'"');
		header('Pragma: no-cache');
		
		set_time_limit(0);
		
		/** @var $provider DataProvider */
		$provider = $this->grid->getConfig()->getDataProvider();
		
		$this->renderHeader($file);
		
		$this->resetPagination($provider);
		$provider->reset();
		/** @var DataRow $row */
		while ($row = $provider->getRow()) {
			$output = [];
			foreach ($this->grid->getConfig()->getColumns() as $column) {
				if (!$column->isHidden()) {
					$output[] = $this->escapeString( $column->getValue($row) );
				}
			}
			// fputcsv($file, $output, static::CSV_DELIMITER);
			fputcsv($file, $output, ",");
		}
		
		fclose($file);
		exit;
	}
	

    protected function renderHeader($file)
    {
        $output = [];
        foreach ($this->grid->getConfig()->getColumns() as $column) {
            if (!$column->isHidden()) {
                $output[] = $this->escapeString($column->getLabel());
            }
        }
        // fputcsv($file, $output, static::CSV_DELIMITER);
        fputcsv($file, $output, ",");
    }

}