<?php

namespace App\Grids;

use App\Order;
use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use App\Product;

use DB;
use App\Account;
use TheIconic\NameParser\Parser;

class OrderGrid extends GeneralGrid {
	protected $_name = 'order';
	protected $_pageLimit = 100;
	function __construct($name = null, $limit = 20) {
		if ($name != null) {
			$this->_name = $name;
		}
		
		$this->_pageLimit = $limit;
		
		return $this;
	}
	
	
	function ordersGrid() {
		
		$dataProvider = Order::with ( [ 'account', 'items' ] );
		// Order::join('order_items', 'orders.AmazonOrderId', '=', 'order_items.AmazonOrderId');

		if(auth()->user()->role == "product manager assistant") {
			$dataProvider->whereHas('items',function($q){
				$q->whereIn('asin',auth()->user()->productList());
			});
		}

		$gridConfig = new GridConfig ();
		
		$gridConfig
		->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'Orders' )->setPageSize ( 20 )
		->setColumns ( [
				OrderGrid::purchaseDateField(),
				OrderGrid::merchantField(),
				OrderGrid::amazonOrderIdField(),
				OrderGrid::salesChannelField(),
				OrderGrid::fulfillmentChannelField(),
				OrderGrid::orderStatusField(),
				// OrderGrid::skipReviewClaimField(),
				// OrderGrid::orderTotalField(),
				OrderGrid::productsField(),
				OrderGrid::totalPromoField(),
				OrderGrid::shippingAddressField(),
				OrderGrid::shippingAddressFirstNameField(),
				OrderGrid::shippingAddressLastNameField(),
				OrderGrid::shippingAddress1Field(),
				OrderGrid::shippingAddress2Field(),
				OrderGrid::shippingAddressCityField(),
				OrderGrid::shippingAddressStateField(),
				OrderGrid::shippingAddressCountryField(),
				OrderGrid::shippingAddressPostalCodeField(),
				OrderGrid::noteField(),
				OrderGrid::isOptOutfield(),
		] )
		->setComponents ( [
				GeneralGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] ) ;
		
		
		$grid = new Grid ($gridConfig);
		
		return $grid;

	}

	static function purchaseDateField() {
		return (new FieldConfig ())->setName ( 'PurchaseDate' )->setLabel ( 'PurchaseDate' )->setSortable ( true )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_GTE )->setTemplate ( "grid-components.date" )
					->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
						$dp->getBuilder ()->where ( 'PurchaseDate' , '>=', \DT::convertToUTC(date('Y-m-d 00:00:00',strtotime($value))) );
				} ) )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LSE )->setTemplate ( "grid-components.date" )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
					$dp->getBuilder ()->where ( 'PurchaseDate', '<=', \DT::convertToUTC(date('Y-m-d 00:00:00',strtotime($value)+26*360)));
				} ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					
					return \DT::convertUTCToTimezone($val);
					
				})->setSorting(Grid::SORT_DESC);
	}


	static function merchantField() {
		$accounts = Account::all();
		$options = [];
		foreach($accounts as $account) {
			$options[$account->seller_id] = $account->name . " (" . $account->code . ")";
		}

		return (new FieldConfig ())->setName ( 'seller_id' )->setLabel ( 'Merchant' )
				->addFilter ( (new SelectFilterConfig ())->setName ( 'seller_id' )->setSubmittedOnChange ( false )-> // on change submit request
				setOptions ( $options ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					return '<a  href="' . route ( "dashboard", [ "account" =>$row->getSrc()->seller_id] ) . '">' . $row->getSrc()->account->name . ' (' . $row->getSrc()->account->code . ')</a><br>' . $row->getSrc()->account->seller_email;
				} )
				->setSortable ( true );
	}

	static function amazonOrderIdField() {
		return (new FieldConfig ())->setName ( 'AmazonOrderId' )->setLabel ( 'AmazonOrderId' )->
				setSortable ( true )
				->addFilter(
					(new FilterConfig())
					->setFilteringFunc(function($value, EloquentDataProvider $dp) {
						$dp->getBuilder()->where('orders.AmazonOrderId', $value);
					})
				)->setCallback ( function ($val) {
					return "<a href='" . route ( "order.view", ["id" => $val ] ) . "'>$val</a>";
				} );
	}

	static function fulfillmentChannelField() {
		return (new FieldConfig ())
							->setName ( 'FulfillmentChannel' )
							->setLabel ( 'FulfillmentChannel' )
							->setSortable ( true )
							->addFilter ( 
								(new SelectFilterConfig ())
									->setName ( 'FulfillmentChannel' )
									->setMultipleMode ( true )
									->setSubmittedOnChange ( true )
									->setOptions ([
										"AFN" => "AFN",
										"MFN" => "MFN"
									])
							);

	}

	static function salesChannelField() {
		return (new FieldConfig ())->setName ( 'SalesChannel' )->setLabel ( 'SalesChannel' )->setSortable ( true )->addFilter ( (new SelectFilterConfig ())->setName ( 'SalesChannel' )->setMultipleMode ( true )->setSubmittedOnChange ( true )-> // on change submit request
			setOptions ( [ 
					"Amazon.com" => "US",
					"Amazon.ca" => "CA",
					"Amazon.com.mx" => "MX",
					"Amazon.co.uk" => "UK",
					"Amazon.de" => "DE",
					"Amazon.fr" => "FR",
					"Amazon.it" => "IT",
					"Amazon.es" => "ES",
					'Amazon.co.jp'=>'JP'
				] ) );
	}


	static function orderStatusField() {
		return (new FieldConfig ())->setName ( 'OrderStatus' )->setLabel ( 'OrderStatus' )->setSortable ( true )->addFilter ( (new SelectFilterConfig ())->setName ( 'OrderStatus' )->setMultipleMode ( true )->setSubmittedOnChange ( false )
						->setOptions ( [ 
						"Pending" => "Pending",
						"Unshipped" => "Unshipped",
						"Shipped" => "Shipped",
						"Cancelled"=>"Cancelled",
						"Returned" => "Returned",
						"Partial Returned" => "Partial Returned"
				] )
				->setDefaultValue(["Unshipped",'Shipped'])		
				);
	}


	// static function orderTotalField() {
	// 	return (new FieldConfig ())->setName ( 'OrderTotal' )->setLabel ( 'OrderTotal' )->setSortable ( true );
	// }


	static function productsField() {
		return (new FieldConfig ())->setName ( 'items' )->setLabel ( 'Order Products' )
				->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp){

						if(strlen($value) == 10 && substr($value, 0, 1) == "B") {
							$dp->getBuilder ()->join('order_items', 'orders.AmazonOrderId', '=', 'order_items.AmazonOrderId')->where('order_items.ASIN',$value);
						} else {
							$dp->getBuilder ()->join('order_items', 'orders.AmazonOrderId', '=', 'order_items.AmazonOrderId')->where('Title','like','%'.$value.'%');
						}
				}) )
				->setCallback ( function ($val, ObjectDataRow $row) {
							$order = $row->getSrc ();
							$amzLinks = [
									'US'=>'https://www.amazon.com/',
									'CA'=>'https://www.amazon.ca/',
									'MX'=>'https://www.amazon.com.mx/',
									'UK'=>'https://www.amazon.co.uk/',
									'DE'=>'https://www.amazon.de/',
									'ES'=>'https://www.amazon.es/',
									'FR'=>'https://www.amazon.fr/',
									'IT'=>'https://www.amazon.it/',
									'JP'=>'https://www.amazon.co.jp/',
									'AU'=>'https://www.amazon.com.au/',
							];
							$str = "";
							foreach ($order->items as $item) {
								$str .= '<div class="prodTitle">'.$item->Title.'</div>';
								$str .= '<table><tbody><tr><td><dl class="dl-horizontal orderItemDetail">
								<dt>Item ID:</dt><dd>'.$item->OrderItemId.'</dd>
								<dt>ASIN:</dt><dd><a href="'.$amzLinks[strtoupper($item->country)].'dp/'.$item->ASIN.'" target="_blank">'.$item->ASIN.'</a></dd>
								<dt>SKU:</dt><dd>'.$item->SellerSKU.'</dd>
								<dt>Qty:</dt><dd>'.$item->QuantityOrdered.'</dd>
								<dt>Price:</dt><dd>'.$item->ItemPrice.' '.$item->Currency.'</dd>
								<dt>Shipping:</dt><dd>'.$item->ShippingPrice.' '.$item->Currency.'</dd>
								<dt>Commision & FBA Fee:</dt><dd>'.((float)$item->commission + (float)$item->fba_fee).' '.$item->Currency.'</dd>
								<dt>Promos:</dt><dd>'.$item->total_promo.' '.$item->Currency.'</dd>
								<dt>Refund:</dt><dd>'.$item->total_refund.' '.$item->Currency.'</dd>
								</dl></td></tr></tbody></table>';
							}
							
							return $str;
						} );
	}

	static function totalPromoField() {
		return (new FieldConfig ())->setName ( 'promo_percentage' )->setLabel ( 'Promotion Percentage' )
				->setCallback ( function ($val, ObjectDataRow $row) {
					$order = $row->getSrc ();
					$items = $order->items;
					if (count($items) > 0) {
						return $items[0]->ItemPrice > 0 ? abs(round($items[0]->total_promo / $items[0]->ItemPrice, 2)) : 0;
					} else {
						return 0;
					}
				});			
	}

	static function shippingAddressField() {
		return (new FieldConfig ())->setName ( 'ShippingAddressAddressLine1' )->setLabel ( 'ShippingAddress' )
				->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp){
					$dp->getBuilder ()->where('ShippingAddressName','like', '%'.$value.'%');
				}) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					$order = $row->getSrc ();
					
					$address = $order->ShippingAddressName;
					$address .= "<br/> ".$order->ShippingAddressAddressLine1;
					if (! empty ( $order->ShippingAddressAddressLine2 )) {
						$address .= ", " . $order->ShippingAddressAddressLine2;
					}
					
					$address .= "<br/> " . $order->ShippingAddressCity;
					if (! empty ( $order->ShippingAddressStateOrRegion )) {
						$address .= "<br/> " . $order->ShippingAddressStateOrRegion;
					}
					
					if(!empty($order->ShippingAddressPostalCode)) {
						$address .= ", " . $order->ShippingAddressPostalCode;
					}
					$address .= "<br/> " . $order->ShippingAddressCountryCode;
					$address .= "<br/> " . $order->ShippingAddressPhone;
					return $address;
				} );
	}

	static function shippingAddressFirstNameField() {
		return (new FieldConfig ())->setName ( 'ShippingAddressFirstName' )->setLabel ( 'First Name' )
				->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp){
					if(strlen($value) == 1) {
						$dp->getBuilder ()->where('ShippingAddressName', 'like', $value.' %');
					} else {
						$dp->getBuilder ()->where('ShippingAddressName', 'like', $value.'%');
					}
				}) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					$order = $row->getSrc ();
					
					if(!empty($order->ShippingAddressName)) {
						$parser = new Parser();
						$addressName = $parser->parse($order->ShippingAddressName);
						return $addressName->getFirstname();
					} else {
						return "";
					}
				} );
	}

	static function shippingAddressLastNameField() {
		return (new FieldConfig ())->setName ( 'ShippingAddressLastName' )->setLabel ( 'Last Name' )
				->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp){
					if(strlen($value) == 1) {
						$dp->getBuilder ()->where('ShippingAddressName', 'like', '% '.$value);
					} else {
						$dp->getBuilder ()->where('ShippingAddressName', 'like', '%'.$value);
					}
				}) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					$order = $row->getSrc ();
										
					if(!empty($order->ShippingAddressName)) {
						$parser = new Parser();
						$addressName = $parser->parse($order->ShippingAddressName);
						return $addressName->getLastname();
					} else {
						return "";
					}
				} );
	}

	static function shippingAddress1Field() {
		return (new FieldConfig ())->setName ( 'ShippingAddressAddressLine1' )->setLabel ( 'ShippingAddress1' )
				->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp){
					$dp->getBuilder ()->where('ShippingAddressAddressLine1','like', '%'.$value.'%');
				}) );
	}

	static function shippingAddress2Field() {
		return (new FieldConfig ())->setName ( 'ShippingAddressAddressLine2' )->setLabel ( 'ShippingAddress2' )
				->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp){
					$dp->getBuilder ()->where('ShippingAddressAddressLine2','like', '%'.$value.'%');
				}) );
	}

	static function shippingAddressCityField() {
		return (new FieldConfig ())->setName ( 'ShippingAddressCity' )->setLabel ( 'City' )
				->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp){
					$dp->getBuilder ()->where('ShippingAddressCity','like', '%'.$value.'%');
				}) );
	}

	static function shippingAddressStateField() {
		return (new FieldConfig ())->setName ( 'ShippingAddressStateOrRegion' )->setLabel ( 'State' )
				->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp){
					$dp->getBuilder ()->where('ShippingAddressStateOrRegion','like', '%'.$value.'%');
				}) );
	}

	static function shippingAddressCountryField() {
		return (new FieldConfig ())->setName ( 'ShippingAddressCountryCode' )->setLabel ( 'Country' )
				->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp){
					$dp->getBuilder ()->where('ShippingAddressCountryCode','like', '%'.$value.'%');
				}) );
	}

	static function shippingAddressPostalCodeField() {
		return (new FieldConfig ())->setName ( 'ShippingAddressPostalCode' )->setLabel ( 'PostalCode' )
				->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp){
					$dp->getBuilder ()->where('ShippingAddressPostalCode','like', '%'.$value.'%');
				}) );
	}

	static function noteField() {
		return  (new FieldConfig ())->setName ( 'note' )->setLabel ( 'Note' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->setSortable ( true )->setCallback ( function ($val, ObjectDataRow $row) {
					return '<a  class="pUpdate note" data-type="textarea"  data-pk="note-' . $row->getSrc ()->id . '"  data-value="' . $val . '">' . $val . '</a>';
				} );	
	}

	static function isOptOutfield() {
		return  (new FieldConfig ())->setName ( 'is_opt_out' )->setLabel ( 'Is Opt Out' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->setSortable ( true )->setCallback ( function ($val, ObjectDataRow $row) {
					$order =  $row->getSrc();
					return '<input value="1" data-size="small" data-pk="is_opt_out-'.$order->id.'" data-url="'.route('order.ajaxsave').'" '.($order->is_opt_out?'checked':'').' type="checkbox" data-toggle="toggle"  />';
				} );		
	}
	
	static function headerComponent() {
		return (new THead ())->setComponents ( [

				(new ColumnHeadersRow ()),
				(new FiltersRow ()),
				
				(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [

						(new RecordsPerPage ())->setVariants ( [
								20,
								50,
								100,
								500,
								1000
						] ),
						new ColumnsHider (),
						(new ExcelExport ())->setFileName ( 'Orders-' . date ( 'Y-m-d' ) )->setRowsLimit(999999),
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
								'class' => 'btn btn-success btn-sm'
						] )
				] ),
				
		] );
	}

	static function skipReviewClaimField() {
		return (new FieldConfig())
			->setName('skip_review_claim')
			->setLabel('SkipReviewClaim')
			->addFilter(
				(new SelectFilterConfig())
				->setFilteringFunc(function($value, EloquentDataProvider $dp) {
					$dp->getBuilder()->join('order_shipments', function($join) {
						$join->on('orders.AmazonOrderId', '=', 'order_shipments.amazon_order_id')
						->on('orders.seller_id', '=', 'order_shipments.seller_id');
					})
					->whereIn('order_shipments.skip_review_claim', $value)
					->orWhereNull('order_shipments.skip_review_claim');
				})
				->setMultipleMode(TRUE)
				->setSubmittedOnChange(FALSE)
				->setOptions([ 
					1 => "Yes",
					0 => "No",
				])
				->setDefaultValue([0, 1])		
			);
	}
}