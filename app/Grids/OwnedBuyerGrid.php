<?php

namespace App\Grids;

use App\OwnedBuyer;
use App\User;
use Carbon\Carbon;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;


class OwnedBuyerGrid extends GeneralGrid {
  protected $_pageLimit = 100;
  protected $_query = NULL;
  protected $_name = '';

	function __construct($query, $name = 'owned-buyers-report', $limit = 100) {
    $this->_query = $query;
    $this->_name = $name;
    $this->_pageLimit = $limit;
    $this->createdBy = null;
		return $this;
  }

  function generateGrid() {
    $gridConfig = new GridConfig();
    $gridConfig->setName($this->_name)
               ->setDataProvider(new EloquentDataProvider($this->_query))
               ->setCachingTime(0) 
               ->setPageSize ( $this->_pageLimit )
               ->setColumns ( [
                $this->createdByColumn(),
                $this->statusColumn(),
                $this->deviceNameColumn(),
                $this->nickNameColumn(),
                $this->lastPlacedOrderAtColumn(),
                $this->lastLeftReviewAtColumn(),
                // $this->reviewRateColumn(),
                $this->reviewedOrderColumn(),
                $this->ownOrdersColumn(),
                $this->otherOrdersColumn(),
                $this->totalOrdersColumn(),
                // $this->ownTotalOrderRateColumn(),
                $this->isPrimeColumn(),
                $this->primeExpiredAtColumn(),
                // $this->totalSpentColumn(),
                // $this->availableBalanceColumn(),
                // $this->createdAtColumn(),
                (new FieldConfig)
                  ->setLabel('Action')
                  ->setCallback(function ($val, ObjectDataRow $row) {
                    $ownedBuyer = $row->getSrc();
                    return '<a href="'. route('owned-buyer.show', ['ownedBuyer'=>$ownedBuyer]). '">View</a> | ' . 
                          '<a href="'. route('owned-buyer.edit', ['ownedBuyer'=>$ownedBuyer]). '">Edit</a> | ' .
                          '<a href="javascript:void(0)"
                            data-toggle="modal" 
                            data-target="#confirmDelete" 
                            data-key-value="' . $ownedBuyer->nickname . '" 
                            data-url="' .route('owned-buyer.destroy', ['id'=>$ownedBuyer->id]). '"
                            data-key-name="nickname">Delete</button>';
                    })
               ])
               ->setComponents ( [
                  ClaimReviewCampaignGrid::headerComponent(),
                  GeneralGrid::footerComponenet()
               ] ) ;
    $grid = new Grid ($gridConfig);
		return $grid;
  }

  public function createdByColumn() {
    return (new FieldConfig)
                  ->setName('user_name')
                  ->setLabel('Created By')
                  ->addFilter ( 
                    (new SelectFilterConfig ())
                      ->setName ( 'owned_buyers.created_by' )
                      ->setSubmittedOnChange ( true )
                      ->setOptions (array_flip(User::selfReviewStaffs()) )
                      // ->setDefaultValue(auth()->user()->isSelfReviewStaff() ? auth()->user()->id : null)
                      ->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
                        if ($value > 0) {
                          $dp->getBuilder()->where('owned_buyers.created_by', $value);
                        } else if ($value == null) {
                          // if (auth()->user()->isSelfReviewStaff()) {
                          //   $dp->getBuilder()->where('owned_buyers.created_by', auth()->user()->id);
                          // }
                        }
                      })
                    );
  }

  public function statusColumn() {
    return (new FieldConfig)
            ->setName('status')
            ->setLabel('Status')
            ->setSortable(true)
            ->addFilter ( 
              (new SelectFilterConfig ())
              ->setName ( 'owned_buyers.status' )
              ->setSubmittedOnChange ( true )
              ->setOptions(["0" => "Inactive", "1" => "Active"])
              ->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
                if ($value == 0 || $value == 1) {
                  $dp->getBuilder()->where('owned_buyers.status', $value);
                }
              })
            )
            ->setCallback(function($val, $row){
              $ownedBuyer = $row->getSrc();
              return '<input value="1" data-size="small" type="checkbox" data-toggle="toggle" data-on="Active" data-off="Inactive" ' . 
              ' data-pk="status" ' .
              ' data-url="'.route('owned-buyer.update-status', ["id"=> $ownedBuyer->id] ).'" '.($val ? 'checked' : '') .
              ' id="status"' . ' />';
            });
  }

  public function deviceNameColumn() {
    return (new FieldConfig)
                ->setName('device_name')
                ->setLabel('Device Name')
                ->addFilter ( 
                  (new SelectFilterConfig ())
                  ->setName ( 'owned_buyers.device_name' )
                  ->setMultipleMode ( true )
                  ->setSubmittedOnChange ( true )
                  ->setOptions(OwnedBuyer::getDevices())
                );
  }

  public function browserColumn() {
    return (new FieldConfig)
                  ->setName('browser')
                  ->setLabel('Browser');
  }

  public function nickNameColumn() {
    return (new FieldConfig)
                  ->setName('nickname')
                  ->setLabel('NickName')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('nickname')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  )
                  ->setCallback(function ($val, ObjectDataRow $row) {
                    $ownedBuyer = $row->getSrc();
                    return '<a href="' . route('owned-buyer.show', ['ownedBuyer'=>$ownedBuyer]) . '">' .$ownedBuyer->nickname. '</a>';
                  });
  }

  public function marketplaceColumn() {
    return (new FieldConfig)
              ->setName('marketplace')
              ->setLabel('Country')
              ->setSortable(true)
              ->addFilter(
                (new FilterConfig)
                    ->setName('marketplace')
                    ->setOperator(FilterConfig::OPERATOR_EQ)
              );
  }

  public function lastPlacedOrderAtColumn() {
    return (new FieldConfig)
              ->setName('last_placed_order_at')
              ->setLabel('Last Order')
              ->setSortable(true)
              ->setCallback(function($val, $row){
                return empty($val) ? '' : (new Carbon($val))->format('Y-m-d');
              });
  }

  public function lastLeftReviewAtColumn() {
    return (new FieldConfig)
              ->setName('last_left_review_at')
              ->setLabel('Last Review')
              ->setSortable(true)
              ->setCallback(function($val, $row){
                return empty($val) ? '' : (new Carbon($val))->format('Y-m-d');
              });
  }
  public function reviewRateColumn() {
    return (new FieldConfig)
              ->setName('review_rate')
              ->setLabel('Review Rate')
              ->setSortable(true);
  }

  public function buyerNameColumn() {
    return (new FieldConfig)
              ->setName('name')
              ->setLabel('Buyer Name');
  }

  public function emailColumn() {
    return (new FieldConfig)
              ->setName('email')
              ->setLabel('Email')
              ->setSortable(true)
              ->addFilter(
                (new FilterConfig)
                    ->setName('email')
                    ->setOperator(FilterConfig::OPERATOR_LIKE)
              );
  }

  public function phoneColumn() {
    return (new FieldConfig)
              ->setName('phone')
              ->setLabel('Phone');
  }

  public function passwordColumn() {
    return (new FieldConfig)
              ->setName('password')
              ->setLabel('Password');
  }

  public function isPrimeColumn() {
    return (new FieldConfig)
              ->setName('is_prime')
              ->setLabel('Prime')
              ->setCallback(function($val, $row) {
                return $val ? "Yes" : "No";
              });

  }

  public function primeExpiredAtColumn() {
    return (new FieldConfig)
              ->setName('prime_end_at')
              ->setLabel('Prime Expired Date');
  }

  public function cardNumberColumn() {
    return (new FieldConfig)
              ->setName('card_number')
              ->setLabel('Card Number');
  }

  public function totalSpentColumn() {
    return (new FieldConfig)
              ->setName('total_spent')
              ->setLabel('Total Spent')
              ->setSortable(true);
  }

  public function availableBalanceColumn() {
    return (new FieldConfig)
              ->setName('available_balance')
              ->setLabel('Available Balance')
              ->setSortable(true);
  }

  public function createdAtColumn() {
    return (new FieldConfig)
              ->setName('created_at')
              ->setLabel('Created At')
              ->setSortable(true);
  }

  public function reviewedOrderColumn() {
    return (new FieldConfig)
              ->setName('reviewed_orders')
              ->setLabel('Reved')
              ->setSortable(true);
  }

  public function ownOrdersColumn() {
    return (new FieldConfig)
              ->setName('own_orders')
              ->setLabel('Own')
              ->setSortable(true);
  }

  public function otherOrdersColumn() {
    return (new FieldConfig)
              ->setName('other_orders')
              ->setLabel('Other')
              ->setSortable(true);
  }

  public function totalOrdersColumn() {
    return (new FieldConfig)
              ->setName('total_orders')
              ->setLabel('Total')
              ->setSortable(true);
  }

  public function ownTotalOrderRateColumn() {
    return (new FieldConfig)
              ->setName('own_other_orders_rate')
              ->setLabel('Own/Total Rate')
              ->setSortable(true);
  }

}