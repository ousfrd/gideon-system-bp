<?php

namespace App\Grids;
use App\DivvyTransactionRecord;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;


class DivvyTransactionGrid extends GeneralGrid {
  protected $_pageLimit = 100;
  protected $_query = NULL;
  protected $_name = '';
	function __construct($query = null, $name = 'divvy-transaction-record', $limit = 100) {
    if (!$query) {
      $this->_query = DivvyTransactionRecord::select('*');
    } else {
      $this->_query = $query;
    }
    $this->_name = $name;
    $this->_pageLimit = $limit;
		return $this;
  }

  function generateGrid() {
    $gridConfig = new GridConfig();
    $gridConfig->setName($this->_name)
               ->setDataProvider(new EloquentDataProvider($this->_query))
               ->setCachingTime(0) 
               ->setPageSize ( $this->_pageLimit )
               ->setColumns ( [
                (new FieldConfig)
                  ->setName('transaction_id')
                  ->setLabel('Transaction Id'),
                (new FieldConfig)
                  ->setName('date')
                  ->setLabel('Date')
                  ->setSortable(true),
                (new FieldConfig)
                  ->setName('merchant')
                  ->setLabel('Merchant'),
                (new FieldConfig)
                  ->setName('amount')
                  ->setLabel('Amount'),
                (new FieldConfig)
                  ->setName('card_name')
                  ->setLabel('Card Name'),
                (new FieldConfig)
                  ->setName('card_last_4')
                  ->setLabel('Last 4 Digits')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('card_last_4')
                        ->setOperator(FilterConfig::OPERATOR_EQ)),
                (new FieldConfig)
                  ->setName('card_exp_date')
                  ->setLabel('Card Exp Date'),
                (new FieldConfig)
                  ->setLabel('Authorized At')
                  ->setName('authorized_at'),
               ])
               ->setComponents ( [
                  ClaimReviewCampaignGrid::headerComponent(),
                  GeneralGrid::footerComponenet()
               ] ) ;
    $grid = new Grid ($gridConfig);
		return $grid;
  }
  

}