<?php

namespace App\Grids;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use App\OwnedBuyerCartItem;

class OwnedBuyerCartItemGrid extends GeneralGrid {
	protected $_pageLimit = 100;
  protected $_query = NULL;
  protected $_name = '';

  function __construct($query = null, $name = 'owned-buyer-shopping-cart', $limit = 100) {
    if (!$query) {
      $this->_query = OwnedBuyerCartItem::gridQuery();
    } else {
      $this->_query = $query;
    }
    $this->_name = $name;
    $this->_pageLimit = $limit;
		return $this;
  }

  function generateGrid() {
    $gridConfig = new GridConfig();
    $gridConfig->setName($this->_name)
               ->setDataProvider(new EloquentDataProvider($this->_query))
               ->setCachingTime(0) 
               ->setPageSize ( $this->_pageLimit )
               ->setColumns ( [
                (new FieldConfig)
                  ->setName('device_name')
                  ->setLabel('Buyer Device Name')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('owned_buyers.device_name')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  ),
                (new FieldConfig)
                  ->setName('nickname')
                  ->setLabel('Buyer Nickname')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('owned_buyers.nickname')
                        ->setOperator(FilterConfig::OPERATOR_LIKE)
                  )->setCallback(function ($val, ObjectDataRow $row) {
                    $buyerId = $row->getCellValue('owned_buyer_id');
                    return '<a target="_blank" href="' . route('owned-buyer.show', ['id'=> $buyerId]) . '">' . $val . '</a>';
                  }),
                (new FieldConfig)
                  ->setName('email')
                  ->setLabel('Buyer Email')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('owned_buyers.email')
                        ->setOperator(FilterConfig::OPERATOR_LIKE)
                  ),
                (new FieldConfig)
                  ->setName('asin')
                  ->setLabel('ASIN')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('owned_buyer_cart_items.asin')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  ),
                (new FieldConfig)
                  ->setName('created_at')
                  ->setLabel('Added To Cart At')
                  ->setSortable(true),
               ])
               ->setComponents ( [
                OwnedBuyerCartItemGrid::headerComponent(),
                GeneralGrid::footerComponenet()
               ] ) ;
  $grid = new Grid ($gridConfig);
  return $grid;
  }

}