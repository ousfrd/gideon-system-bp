<?php

namespace App\Grids;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use DB;
use App\AdKeyword;
use App\AdReport;

class AdKeywordGrid extends GeneralGrid {
	static function grid($params=[]) {
		
		$dataProvider = AdKeyword::join('ad_keywords_reports', 'ad_keywords.keywordId', '=', 'ad_keywords_reports.keywordId');


		if(isset($params['seller_id']) && $params['seller_id']) {
			$dataProvider->where('ad_keywords.seller_id', $params['seller_id']);

		}

		if(isset($params['country']) && $params['country']) {	
			$dataProvider->where('ad_keywords.country', $params['country']);
		}

		if(isset($params['start_date']) && isset($params['end_date'])) {	
			$dataProvider->whereBetween('ad_keywords_reports.date', [$params['start_date'], $params['end_date']]);
		}

		$dataProvider->select(DB::raw('ad_keywords.*, ad_keywords_reports.adGroupId, SUM(ad_keywords_reports.clicks) as total_clicks, SUM(ad_keywords_reports.impressions) as total_impressions, ROUND(SUM(ad_keywords_reports.cost * ad_keywords_reports.exchange_rate), 2) as total_cost, SUM(ad_keywords_reports.attributedSales30d) as total_sales, SUM(ad_keywords_reports.attributedConversions30d) as total_orders'));

		$dataProvider->whereIn('ad_keywords.matchType', ['broad', 'phrase', 'exact']);

		$dataProvider->groupBy('ad_keywords_reports.keywordId');


		$gridConfig = new GridConfig ();
		$gridConfig->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'AdKeywords' )
		->setPageSize ( 50 )
		->setColumns ( [
				AdKeywordGrid::stateField(),
				AdKeywordGrid::keywordTextField(),
				AdKeywordGrid::matchTypeField(),
				AdKeywordGrid::impressionsField(),
				AdKeywordGrid::clicksField(),
				AdKeywordGrid::adSpendField(),
				AdKeywordGrid::salesField(),
				AdKeywordGrid::acosField(),
				AdKeywordGrid::cpcField(),
				AdKeywordGrid::ctrField(),
				AdKeywordGrid::ordersField(),
				AdKeywordGrid::crField(),
		] )->setComponents ( [
				AdKeywordGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] );
		
		$grid = new Grid ($gridConfig);
		
		return $grid;
	}
	
	
	static function stateField() {
		return  (new FieldConfig ())->setName ( 'state' )->setLabel ( 'State' )->setSortable ( true )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'state' )->setMultipleMode ( true )->setSubmittedOnChange ( false )
				->setOptions ( ['enabled'=>'Enabled','paused'=>'Paused','archived'=>'Archived'] )
				);
	}

	static function keywordTextField() {
		return  (new FieldConfig ())->setName ( 'keywordText' )->setLabel ( 'Keyword Text' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) );
	}

	static function matchTypeField() {
		return  (new FieldConfig ())->setName ( 'matchType' )->setLabel ( 'Match Type' )->setSortable ( true )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'matchType' )->setMultipleMode ( true )->setSubmittedOnChange ( false )
				->setOptions ( ['broad'=>'Broad','phrase'=>'Phrase','exact'=>'Exact'] )
				);
	}

	static function impressionsField() {
		return  (new FieldConfig ())->setName ( 'total_impressions' )->setLabel ( 'Impr.' );
	}

	static function clicksField() {
		return  (new FieldConfig ())->setName ( 'total_clicks' )->setLabel ( 'Clicks' )->setSortable ( true );
	}

	static function adSpendField() {
		return  (new FieldConfig ())->setName ( 'total_cost' )->setLabel ( 'Ad Spend (USD)' )->setSortable ( true )->setSorting(Grid::SORT_DESC);
	}

	static function salesField() {
		return  (new FieldConfig ())->setName ( 'total_sales' )->setLabel ( 'Sales (USD)' )->setSortable ( true );
	}

	static function acosField() {
		return  (new FieldConfig ())->setName ( '' )->setLabel ( 'ACoS (%)' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return (isset($row->getSrc ()->total_sales) && $row->getSrc ()->total_sales > 0) ? round(($row->getSrc ()->total_cost / $row->getSrc ()->total_sales) * 100, 2) : "0.0";
				} );
	}

	static function cpcField() {
		return  (new FieldConfig ())->setName ( '' )->setLabel ( 'CPC (USD)' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return (isset($row->getSrc ()->total_clicks) && $row->getSrc ()->total_clicks > 0) ? round($row->getSrc ()->total_cost / $row->getSrc ()->total_clicks, 2) : 0;

				} );
	}

	static function ctrField() {
		return  (new FieldConfig ())->setName ( '' )->setLabel ( 'CTR (%)' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return (isset($row->getSrc ()->total_impressions) && $row->getSrc ()->total_impressions > 0) ? round(($row->getSrc ()->total_clicks / $row->getSrc ()->total_impressions) * 100, 2) : 0;

				} );
	}

	static function ordersField() {
		return  (new FieldConfig ())->setName ( 'total_orders' )->setLabel ( 'Orders' )->setSortable ( true );
	}

	static function crField() {
		return  (new FieldConfig ())->setName ( '' )->setLabel ( 'CR (%)' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return (isset($row->getSrc ()->total_clicks) && $row->getSrc ()->total_clicks > 0) ? round(($row->getSrc ()->total_orders / $row->getSrc ()->total_clicks) * 100, 2) : 0;

				} );
	}



	static function headerComponent() {
		return (new THead ())->setComponents ( [

				(new ColumnHeadersRow ()),
				(new FiltersRow ()),
				
				(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [

						(new RecordsPerPage ())->setVariants ( [
								20,
								50,
								100,
								500,
								1000
						] ),
						new ColumnsHider (),
						(new ExcelExport ())->setFileName ( 'AdKeywords-' . date ( 'Y-m-d' ) ),
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
								'class' => 'btn btn-success btn-sm'
						] )
				] ),
				
		] );
	}


}