<?php

namespace App\Grids;

use App\User;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

use App\Product;
use DB;
use App\Helpers\CurrencyHelper;

class ProductsOfBrandGrid extends GeneralGrid {
	static function grid($request=null,$params=[]) {
		// $dataProvider = Product::with ( [ 'listings' ] );->where ( "name", "<>", null );
		$dataProvider = Product::with ( [ 'listings', 'inventories' ] );
		$dataProvider->where('fulfillment_channel', "Amazon");
		$dataProvider->where('brand', $request);
		$asins = array_key_exists('asins', $params) ? $params['asins'] : null;
		if(auth()->user()->role == "product manager assistant") {
			$asins2 = auth()->user()->productList();
			$dataProvider->whereIn('asin', $asins2);
		}
		
		if(isset($asins)) {
			$dataProvider->whereIn('asin',$asins);
		}
		
		
		if(isset($params['low']) && $params['low'] == 1) {
			#use warehouse qty, not include inbound qty, as inbound qty is not accurate
			$dataProvider->whereRaw(DB::Raw(Product::LOW_STOCK_CONDITION))
			->where('daily_ave_orders','>',0)
			->where('discontinued',0);
		} elseif( (isset($params['lowsales']) && $params['lowsales'] == 1)) {
			$dataProvider->where('discontinued',0)
			->where('daily_ave_orders','<',5)
			->whereRaw(DB::raw('daily_ave_orders*avg_price_usd < 200'))
			->where('total_qty','>',0)
			->whereRaw(DB::Raw('total_qty/daily_ave_orders - (manufacture_days+cn_to_amz_shipping_days+5) >= 60'))
			;
			
		}
		//print $dataProvider->toSql();
		$gridConfig = new GridConfig ();
		
		$gridConfig
		->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'Products' )->setPageSize ( 25 )
		->setColumns ( [
				ProductsOfBrandGrid::imageField(),
				ProductsOfBrandGrid::asinField($asins),
				ProductsOfBrandGrid::aliasField(),
				ProductsOfBrandGrid::managersField(),
				ProductsOfBrandGrid::statusField(),
				ProductsOfBrandGrid::discontinuedField(),
				ProductsOfBrandGrid::salesRanksField(),
				ProductsOfBrandGrid::peakSeasonSettingField(),
				ProductsOfBrandGrid::productGroupField(),
				
				ProductsOfBrandGrid::lifetimeProfitField(),
				ProductsOfBrandGrid::dailyAvgOrdersField(),
				ProductsOfBrandGrid::qtyField(),
				ProductsOfBrandGrid::stockCostField(),
				ProductsOfBrandGrid::replenishmentField(),
				ProductsOfBrandGrid::costField(),
				ProductsOfBrandGrid::manufactureSettingField(),
				ProductsOfBrandGrid::shippingDaysSettingField(),
				ProductsOfBrandGrid::productListingsField(),
				GeneralGrid::countryField(),
				ProductsOfBrandGrid::aiAdsOpenField(),
				ProductsOfBrandGrid::aiAdsCommisionRateField(),
				ProductsOfBrandGrid::actionsField()
		] )
		->setComponents ( [
				ProductsOfBrandGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] ) ;
		
		
		$grid = new Grid ($gridConfig);
		
		return $grid;
	}
	
	
	static function imageField() {
		return 
		(new FieldConfig ())
		->setName ( 'name' )->setLabel ( 'Product' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) )
		->setCallback ( function ($val, ObjectDataRow $row) {
			$product = $row->getSrc();
			$imageLink = $product->small_image;
			if (empty ($imageLink)) {
				$imageLink = "/images/No_Image_Available.jpg";
			}
			return '<a  href="' . $product->amazonLink () . '" target="_blank" data-placement="right" data-toggle="popover"  data-content="'.$product->name.'"><img src="' . $imageLink . '" style="max-width:100px;max-height:100px"/></a>';
		} );
	}

	static function nameField() {
		return (new FieldConfig ())
					->setName ( 'name' )
					->setLabel ( 'Product' )
					->addFilter ( 
						(new FilterConfig ())
							->setOperator ( FilterConfig::OPERATOR_LIKE ) 
					)
					->setCallback ( function ($val, ObjectDataRow $row) {
						return '<label>' . $row->getSrc()->name. '</label>';
					});
	}
	
	static function asinField($asins = []) {
		$fieldConfig = (new FieldConfig ())
											->setName ( 'asin' )
											->setLabel ( 'ASIN' );
		if (!$asins || count($asins) == 0) {
			$fieldConfig->addFilter ( 
											(new FilterConfig ())
												->setOperator ( FilterConfig::OPERATOR_LIKE ) );
		} else {
			$options = [];
			foreach ($asins as $asin) {
				$options[$asin] = $asin;
			}
			$fieldConfig->addFilter (
										(new SelectFilterConfig) 
											->setOptions($options)
											->setMultipleMode(true)
			);
		}
		$fieldConfig->setCallback ( function ($val, ObjectDataRow $row) {
			return '<a  href="' . route ( "product.view", [ "id" => $row->getSrc ()->id ] ) . '">' . $row->getSrc ()->asin . '</a> <br>'.$row->getSrc()->country;
		} )->setSortable ( true );
		return $fieldConfig;
	}
	
	static function aliasField() {
		return  (new FieldConfig ())->setName ( 'alias' )->setLabel ( 'Alias' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->setSortable ( true )->setCallback ( function ($val, ObjectDataRow $row) {
					return '<a  class="pUpdate alias"  data-pk="alias-' . $row->getSrc ()->id . '"  data-value="' . $val . '">' . $val . '</a>';
				} );		
	}

	static function managersField() {
		return (new FieldConfig ())->setName ( 'managers' )->setLabel ( 'Manager' )
		->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
			$user = User::findByName($value);
			
			return $dp->getBuilder()->whereIn('asin',$user->productList());
		}))
		->setCallback ( function ($val, ObjectDataRow $row) {
			//
			$users = $row->getSrc()->users()->select('name','user_id')->get();
			$ids = [];
			$names = [];
			foreach ($users as $user) {
				$ids[] = $user->user_id;
				$names[] = $user->name;
			}
			if(Auth::user()->can('manage-product-manager')) {
				return '<a href="#" data-type="select" class="pmanagers"  data-pk="manager-'.$row->getSrc()->id.'" data-title="">'.implode(', ', $names).'</a>';
			}else{
				return implode(', ', $names);
			}
			
		} )->setSortable ( true );
	}
	
	static function statusField() {
		return (new FieldConfig ())->setName ( 'status' )->setLabel ( 'Status' )->setSortable ( true )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'status' )->setSubmittedOnChange ( true )
				->setOptions ( ['Inactive','Active'] )
				->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
					
					if($value == 0) {
						$dp->getBuilder()->where('status',0);
					}elseif ($value == 1) {
						$dp->getBuilder()->where('status',1);
					}
				})
				
				)
				->setCallback ( function ($val, ObjectDataRow $row) {
					$product =  $row->getSrc();
					if($product->status <=0) {
						return 'Product Inactive';
					}else{
						
						$discontinue = '<br/><br/><a class="pUpdate"  data-type="select" data-source=\'[{value: 1, text: "Yes"}, {value: 0, text: "No"}]\' data-pk="discontinued-'.$product->id.'" data-url="'.route('product.ajaxsave').'" >Discontinued?</a>';
						
						
						if ($product->totalQty() == 0) {
							$text =   'Out of Stock';

							$text .= $discontinue;
							
						}else if($product->safeStockQty() > $product->wareHouseTotalQty() || $product->warehouseTotalQty() < 10) {
							$text = '<span class="low-stock">Low Stock</a>';
							
							//if($product->warehouseTotalQty() <50) {
								$text .= $discontinue;
							//}
							
						} else {
							$text = 'Active';
						}
						
						//
						return $text;
						
					}
				} );
	}

	static function discontinuedField() {
		return (new FieldConfig ())->setName ( 'discontinued' )->setLabel ( 'Discontinued?' )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'discontinued' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( ['No','Yes'] ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					$listing =  $row->getSrc();
					return '<input value="1" data-size="small" data-pk="discontinued-'.$listing->id.'" data-url="'.route('product.ajaxsave').'" '.($listing->discontinued?'checked':'').' type="checkbox" data-toggle="toggle"  />';
				});
	}

	static function peakSeasonSettingField() {
		return (new FieldConfig ())->setName ( 'peak_season_mode' )->setLabel ( 'Peak Season' )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'peak_season_mode' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( ['No','Yes'] ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					$listing =  $row->getSrc();
					return '<input value="1" data-size="small" data-pk="peak_season_mode-'.$listing->id.'" data-url="'.route('product.ajaxsave').'" '.($listing->peak_season_mode?'checked':'').' type="checkbox" data-toggle="toggle"  />';
				});
	}

	static function productGroupField() {
		$groups = Product::select('product_group')->where('product_group', '<>', '')->orderBy('product_group', 'asc')->distinct()->get()->toArray();

		$product_groups = array_column($groups, 'product_group');

		$options = array_combine($product_groups, $product_groups);

		return (new FieldConfig ())->setName ( 'product_group' )->setLabel ( 'Product Group' )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'product_group' )->setSubmittedOnChange ( true )
				->setOptions ( $options )
				->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
					
					$dp->getBuilder()->where('product_group', $value);
					
				})
				
				)->setCallback(function($val,ObjectDataRow $row){
					$listing =  $row->getSrc();
					return '<a data-type="text" class="pUpdate"  data-pk="product_group-'.$listing->id.'" data-value="'.$listing->product_group.'" data-title="">'.$listing->product_group.'</a>';
				});
	}
	
	static function dailyAvgOrdersField() {
		return (new FieldConfig ())->setName ( 'daily_ave_orders' )->setLabel ( 'AVG Orders Per Day' )
		->setSortable ( true )
		->setSorting(Grid::SORT_DESC)
		->setCallback(function($val,ObjectDataRow $row){
			$listing = $row->getSrc();
			$orders = [
					'Today So Far: '.$listing->today_so_far_orders,
					'Last 7 Days: '.$listing->last_7_days_orders,
					'Last 14 Days: '.$listing->last_14_days_orders,
					'Last 21 Days: '.$listing->last_21_days_orders,
					'Last 30 Days: '.$listing->last_30_days_orders,
					'This Month So Far: '.$listing->this_month_orders,
					
			];
			
			$content =  implode('<br/>', $orders);
			return '<a data-toggle="popover" title="Order Overview" data-content="'.$content.'">'.round($listing->dailyAverageOrders(),1).' </a>';
			
		});
		
	}
	
	static function qtyField() {
		return (new FieldConfig)
		->setName('warehouse_qty')
		->setLabel('Qty')
		->setSortable(true)
		->addFilter ( (new SelectFilterConfig ())
				->setName ( 'qty' )->setSubmittedOnChange ( true )
				->setOptions ( [1=>'In Stock',0=>'Out of Stock'] )
				->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
					if($value == 0) {
						$dp->getBuilder()->where(function($q){
							$q->where(function($q){
								$q->where('qty','=',0)->orWhere('total_qty',0);
							});
						});
							// 								$dp->getBuilder()
					}else{
						$dp->getBuilder()->where(function($q){
							$q->where(function($q){
								$q->where('qty','>',0)->orWhere('total_qty','>',0);
							});
						});
							
					}
					
				})
				)
				
				
				->setCallback(function($val,ObjectDataRow $row){
					
					
					$product = $row->getSrc();
					
					
					$qty_cols = [
							'fulfillable_quantity'=>'Fulfillable Qty',
							'unsellable_quantity'=>'Unsellable Qty',
							'reserved_quantity'=>'Reserved Qty',
							'warehouse_quantity'=>'Warehouse Total Qty',
							'inbound_working_quantity'=>'<br/>Inbound Working Qty',
							'inbound_shipped_quantity'=>'Inbound Shipped Qty',
							'inbound_receiving_quantity'=>'Inbound Receiving Qty',
							'total_quantity'=>'<br/>Total Qty',
					];
					
					$qtys = [];
					foreach ($qty_cols as $k=>$l) {
						@$qtys[$l] += 0;
					}
					$warehouse_quantities = [];
					$total_quantities = [];
					if($inventories = $product->inventories) {
						foreach ($inventories as $inventory) {
							if(in_array($inventory->warehouse_quantity, $warehouse_quantities) and in_array($inventory->total_quantity, $total_quantities)) {
								continue;
							}
							if($inventory->status == 1) {
								foreach ($qty_cols as $k=>$l) {
									if($inventory->$k > 0 || $k == 'fulfillable_quantity') {
										@$qtys[$l] += $inventory->$k;
									}
								}
								$warehouse_quantities[] = $inventory->warehouse_quantity;
								$total_quantities[] = $inventory->total_quantity;
							}
						}
					}
					
					
					
					$qtyTxt = [];
					foreach ($qty_cols as $k=>$l) {
						$qtyTxt[] = $l.': '.$qtys[$l];
					}
					
					$qtyTxt[] = 'Safe Stock: '.$product->safeStockQty();
					$qtyTxt[] = '<br/>Out of Stock (include inbound qty) in '. $product->daysToOutofStock(). ' days';
					$qtyTxt[] = 'Out of Stock (warehouse qty only) in '. $product->warehouseQtyDaysToOutofStock(). ' days';
					$qtyTxt[] = 'Replenishment Cycle: '.$product->totalDayToReinstock(). ' days';
					
					$content = implode('<br/>', $qtyTxt);
					
					
					
					$text =  '<a data-toggle="popover" title="Inventory Overview" data-content="'.$content.'">'.$qtys['Warehouse Total Qty'].'<br/>'.$qtys['Fulfillable Qty'].' fulfillable</a>';
// 					if($product->cost > 0){
// 						$text .= "<br/>Total Cost: ".CurrencyHelper::format($qtys['Warehouse Total Qty'] * ($product->cost + $product->shipping_cost));
// 					}
					if($product->safeStockQty() > $product->wareHouseTotalQty()) {
						if($product->replenished){
							$text .= '<br/><br/><a class="pUpdate text-success"  data-type="select" data-source=\'[{value: 1, text: "Yes"}, {value: 0, text: "No"}]\' data-pk="replenished-'.$product->id.'" data-url="'.route('product.ajaxsave').'" >Replenished</a>';
						}else{
							$text .= '<br/><br/><a class="pUpdate"  data-type="select" data-source=\'[{value: 1, text: "Yes"}, {value: 0, text: "No"}]\' data-pk="replenished-'.$product->id.'" data-url="'.route('product.ajaxsave').'" >Replenished?</a>';
						}
						
					}
					
					return $text;
					
				});
	}
	
	static function stockCostField() {
		return (new FieldConfig)
		->setName('warehouse_qty')
		->setLabel('Inventory Cost')
		->setSortable(true)
		->setCallback(function($val,ObjectDataRow $row){
			$product = $row->getSrc();
			
			if($product->cost + $product->shipping_cost> 0){
				return CurrencyHelper::format($product->warehouse_qty* ($product->cost + $product->shipping_cost));
			}else {
				return "N/A";
			}
		});
	}
	static function replenishmentField() {
		return (new FieldConfig ())->setName ( 'last_30_days_ord' )->setLabel ( 'Replenishment Date' )
		
		->setCallback(function($val,ObjectDataRow $row){
			$listing = $row->getSrc();
			$lines = [
					'Daily Average Orders: '.$listing->dailyAverageOrders(),
					'<br/>Warehouse Total Qty: '.$listing->warehouseTotalQty(),
					'Out of Stock in '. $listing->warehouseQtyDaysToOutofStock(). ' days',
					'Replenishment Cycle: '.$listing->totalDayToReinstock(). ' days',
					'<br/>Replenishment Needed: '.($listing->replenishNeeded() ? '<span class=\'text-danger alert-qty\'>Yes</span>':'<span  class=\'text-success\'>No</span>'),
					'Recommended Replenishment Date: '. $listing->recommendedReplenishmentDate(),
					
			];
			
			
			if($listing->safeStockQty() > $listing->warehouseTotalQty()) {
				$lines[] = 'Recommended Replenishment Qty: '. $listing->recommendedReplenishmentQty();
				$lines[] = '<br/><a href=\''.route("inbound_shipment.create_plan",[$listing->id]).'\'>Create Shipment Plan</a>';
			}
			
			$content = implode('<br/>', $lines);
			
			$text = '<a data-toggle="popover" title="Replenishment Recommendation" data-content="'.$content.'">'. $listing->recommendedReplenishmentDate().'</a>';

			return $text;
			
		});
	}
	
	static function costField() {
		return (new FieldConfig ())->setName ( 'cost' )->setLabel ( 'Cost' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->
		setCallback ( function ($val, ObjectDataRow $row) {
			$costs[] = 'Unit Cost: <a  class="pUpdate unit-cost"  data-pk="cost-' . $row->getSrc ()->id . '"  data-value="' . $val . '">' . $val . '</a>';
			$costs[] = 'Shipping: <a  class="pUpdate unit-cost" data-pk="shipping_cost-' . $row->getSrc ()->id . '"  data-value="' . $row->getSrc ()->shipping_cost. '">' .  $row->getSrc ()->shipping_cost. '</a>';
			return implode('<br/>', $costs);
		} );
	}
	
	static function salesRanksField() {
		return (new FieldConfig ())->setName ( 'sales_ranks' )->setLabel ( 'Sales Ranks' )
		->setCallback ( function ($val, ObjectDataRow $row) {
			$content = "";
			$topRankText= "N/A";
			$topRank = 10000000;
			$product = $row->getSrc();
			if($product->sales_ranks){
                  $ranks = json_decode($product->sales_ranks,true);
                   if(isset($ranks) && is_array($ranks) && !array_key_exists('ranks', $ranks)) {
                   		$num = 0;
               			foreach($ranks as $cat) {
               				$num += 1;
                        	foreach($cat as $rank => $subcat) {
                            	$content .= "#" . number_format($rank) ." in ";
                            	if(is_array($subcat)) {
                            		$count = 1; $length = count($subcat);
   									foreach (array_reverse($subcat) as $catname) {
   										$count += 1;
   										if(reset($catname) == 'Products') {
   											continue;
   										}
   											$content .= reset($catname);
   										if($count < $length) {
   											$content .= " > ";
   										}
   									}
                            	} else {
                            	 	$content .= $subcat;
                            	}
                        	}
                        	if($num == 1) {
                        		$topRankText = $content;
                        	}
                        	$content .= "<br>";
                        }
                   }
                            
            }
                            
            if($topRankText== "N/A") {
            	return $topRankText;
            }
            return '<a data-toggle="popover" title="Product Sales Rank" data-content="'.$content.'"> '.$topRankText.' </a>';
		});
	}
	
	static function manufactureSettingField() {
		return (new FieldConfig ())->setName ( 'manufacture_days' )->setLabel ( 'Manufacture Days' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->
		setCallback ( function ($val, ObjectDataRow $row) {
			return
			'Normal: <a  class="pUpdate" data-pk="manufacture_days-' . $row->getSrc ()->id . '"  data-value="' . $val . '">' . $val . '</a>'.
			'<br/>Peak Season: <a  class="pUpdate" data-pk="ps_manufacture_days-' . $row->getSrc ()->id . '"  data-value="' . $row->getSrc()->ps_manufacture_days. '">' . $row->getSrc()->ps_manufacture_days. '</a>';
		} );
	}
	
	static function shippingDaysSettingField() {
		return (new FieldConfig ())->setName ( 'cn_to_amz_shipping_days' )->setLabel ( 'ToAMZ ShippingDays' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->
		setCallback ( function ($val, ObjectDataRow $row) {
			return
			'Normal: <a  class="pUpdate" data-pk="cn_to_amz_shipping_days-' . $row->getSrc ()->id . '"  data-value="' . $val . '">' . $val . '</a>'.
			'<br/>Peak Season: <a  class="pUpdate" data-pk="ps_cn_to_amz_shipping_days-' . $row->getSrc ()->id . '"  data-value="' . $row->getSrc()->ps_cn_to_amz_shipping_days. '">' . $row->getSrc()->ps_cn_to_amz_shipping_days. '</a>';
		} );
	}
	
	static function productListingsField(){
		return (new FieldConfig ())->setName ( 'id' )->setLabel ( 'Listings' )->setSortable ( true )->
		setCallback ( function ($val, ObjectDataRow $row) {
			$listings = [ ];
			$qty_cols = [
					'fulfillable_quantity'=>'Fulfillable Qty',
					'unsellable_quantity'=>'Unsellable Qty',
					'reserved_quantity'=>'Reserved Qty',
					'warehouse_quantity'=>'Warehouse Total Qty',
					'inbound_working_quantity'=>'<br/>Inbound Working Qty',
					'inbound_shipped_quantity'=>'Inbound Shipped Qty',
					'inbound_receiving_quantity'=>'Inbound Receiving Qty',
					'total_quantity'=>'<br/>Total Qty',
			];

			
			foreach ( $row->getSrc ()->listings as $listing ) {
				if($listing->status != 1 or $listing->fulfillment_channel!='Amazon') {
					continue;
				}
				$qtys = [];
				foreach ($qty_cols as $k=>$l) {
					@$qtys[$l] += 0;
				}
				if( $listing->inventory) {
					foreach ($qty_cols as $k=>$l) {
							$qtys[$l] = $listing->inventory->$k;
					}
				}
				
				$qtyTxt = [];
				foreach ($qty_cols as $k=>$l) {
					$qtyTxt[] = $l.': '.$qtys[$l];
				}
				
				$qtyTxt[] = 'Safe Stock: '.$listing->safeStockQty();
				$qtyTxt[] = '<br/>Out of Stock in '. $listing->daysToOutofStock(). ' days';
				$qtyTxt[] = 'Replenishment Cycle: '.$listing->totalDayToReinstock(). ' days';
				
				$content = implode('<br/>', $qtyTxt);
				
				$listings [$listing->seller_id.$listing->region.$listing->sku] = 
				"<a  data-toggle='popover' title='Inventory Summary' data-content='".$content."' href='".route('listing.view',[$listing->id])."'>".$listing->merchant->code.' '.$listing->sku .($listing->region=="US"?"":" - ".$listing->region).
						' - Qty: '.$qtys['Warehouse Total Qty'].'</a>';

			}

			return '<div class="text-nowrap">'.implode ( "<br/>", $listings ).'</div>';
		} );
	}
	
	static function lifetimeProfitField() {
		return (new FieldConfig ())->setName ( 'lifetime_profit' )->setLabel ( 'Lifetime Profit' )->setSortable ( true )->
		setCallback ( function ($val, ObjectDataRow $row) {
			$p = $row->getSrc();
			
			#lifetime_gross_sales-lifetime_product_cost-lifetime_ad_expense-lifetime_other_expense-lifetime_refund
			$lifttimeProfit = $p->lifetime_gross_sales - $p->lifetime_product_cost - $p->lifetime_ad_expense - $p->lifetime_other_expense - $p->lifetime_refund - $p->lifetime_review_expense
			+ $p->lifetime_commission + $p->lifetime_fba_fee - $p->lifetime_promotions
			;
		
			$content = "Lifetime Gross: ".CurrencyHelper::format($row->getSrc()->lifetime_gross_sales);
			
			if($p->lifetime_gross_sales > 0) {
				$content .= "<br/><br/>Lifetime FBA Fee: <span class='text-danger'> ".CurrencyHelper::format($row->getSrc()->lifetime_fba_fee) ."</span> (".number_format($p->lifetime_fba_fee*100/$p->lifetime_gross_sales)."%)";
				$content .= "<br/>Lifetime Amazon Commission:  <span class='text-danger'> ".CurrencyHelper::format($row->getSrc()->lifetime_commission) ."</span>  (".number_format($p->lifetime_commission*100/$p->lifetime_gross_sales)."%)";
				$content .= "<br/>Lifetime Refund:  <span class='text-danger'>-".CurrencyHelper::format($row->getSrc()->lifetime_refund) ."</span>  (".number_format($p->lifetime_refund*100/$p->lifetime_gross_sales)."%)";
				$content .= "<br/><br/>Lifetime Promossions:  <span class='text-danger'>-".CurrencyHelper::format($row->getSrc()->lifetime_promotions) ."</span>  (".number_format($p->lifetime_promotions*100/$p->lifetime_gross_sales)."%)";

				$content .= "<br/>Lifetime Ad Expenses:  <span class='text-danger'>-".CurrencyHelper::format($row->getSrc()->lifetime_ad_expense) ."</span>  (".number_format($p->lifetime_ad_expense*100/$p->lifetime_gross_sales)."%)";
				$content .= "<br/>Lifetime Review Expenses:  <span class='text-danger'>-".CurrencyHelper::format($row->getSrc()->lifetime_review_expense) ."</span> (".number_format($p->lifetime_review_expense*100/$p->lifetime_gross_sales)."%)";
				
				
				$content .= "<br/><br/>Lifetime Product Cost:  <span class='text-danger'>-".CurrencyHelper::format($row->getSrc()->lifetime_product_cost) ."</span>  (".number_format($p->lifetime_product_cost*100/$p->lifetime_gross_sales)."%)";
				$content .= "<br/>Lifetime Other Expenses:  <span class='text-danger'>-".CurrencyHelper::format($row->getSrc()->lifetime_other_expense) ."</span>  (".number_format($p->lifetime_other_expense*100/$p->lifetime_gross_sales)."%)";
				$content .= "<br/><br/>Lifetime Profit: ".CurrencyHelper::format($lifttimeProfit) ."</span>  (".number_format($lifttimeProfit*100/$p->lifetime_gross_sales)."%)";
			}
			$text = '<a data-toggle="popover" title="Est. Lifttime Profit" data-content="'.$content.'">'. CurrencyHelper::format($lifttimeProfit).'</a>';
			
			return $text;
		} );
	}

	static function aiAdsOpenField() {
		return (new FieldConfig)
						->setName('ai_ads_open')
						->setLabel('AI Ads Open?')
						->setCallback(function ($val, ObjectDataRow $row) {
							if (Gate::allows('manage-ads')) {
								$id = $row->getCellValue('id');
								return '<input value="1" data-size="small" type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" ' . 
																			' data-pk="' . $id . '"' . 
																			' data-url="'.route('product.update-field', ['id'=> $id]).'" '. ($val ? 'checked' : '') .
																			' id="ai_ads_open"' .' />';
							} else {
								return $val ? 'Yes' : 'No';
							}
							
						});
	}

	static function aiAdsCommisionRateField() {
		return (new FieldConfig)
						->setName('ai_ads_commission_rate')
						->setLabel('AI Ads Commission (%)')
						->setCallback(function ($val, ObjectDataRow $row) {
							if (Gate::allows('manage-ads')) {
								$id = $row->getCellValue('id');
								return '<a data-type="text" class="editable" ' . 
                  ' data-pk="' . $id . '"' .
                  ' data-url="' . route('product.update-field', ['id'=> $id]) . '"' . 
                  ' data-value="' . $val .'" id="ai_ads_commission_rate" data-title="AI Ads Commission (%)" >' . $val. '</a>';
							} else {
								return $val . "%";
							}
						});
	}

	static function actionsField() {
		return (new FieldConfig ())->setName ( 'id' )->setLabel ( 'Actions' )->setSortable ( true )->setCallback ( function ($val, ObjectDataRow $row) {
			$user = $row->getSrc ();
			return '<a  href="' . $user->amazonLink () . '" target="_blank">View on Amazon</a> | <a  href="' . route ( "product.view", [
					"id" => $user->id
			] ) . '">View Dashboard</a> ';
		} ) ;
	}


	static function headerComponent() {
		return (new THead ())->setComponents ( [

				(new ColumnHeadersRow ()),
				(new FiltersRow ()),
				
				(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [

						(new RecordsPerPage ())->setVariants ( [
								20,
								50,
								100,
								500,
								1000
						] ),
						new ColumnsHider (),
						(new ExcelExport ())->setFileName ( 'Products-' . date ( 'Y-m-d' ) ),
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
								'class' => 'btn btn-success btn-sm'
						] )
				] ),
				
		] );
	}

}