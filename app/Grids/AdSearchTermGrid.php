<?php

namespace App\Grids;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use DB;
use App\AdSearchtermsReport;
use App\AdReport;

class AdSearchTermGrid extends GeneralGrid {
	static function grid($params=[]) {
		
		$dataProvider = AdSearchtermsReport::select(DB::raw('ad_searchterms_reports.*, ad_searchterms_reports.clicks as total_clicks, ad_searchterms_reports.impressions as total_impressions, ROUND(ad_searchterms_reports.cost * ad_searchterms_reports.exchange_rate, 2) as total_cost, ad_searchterms_reports.attributedSales30d as total_sales, ad_searchterms_reports.attributedConversions30d as total_orders'));


		if(isset($params['seller_id']) && $params['seller_id']) {
			$dataProvider->where('seller_id', $params['seller_id']);

		}

		if(isset($params['country']) && $params['country']) {	
			$dataProvider->where('country', $params['country']);
		}

		if(isset($params['start_date']) && isset($params['end_date'])) {	
			$dataProvider->whereBetween('date', [$params['start_date'], $params['end_date']]);
		}



		$gridConfig = new GridConfig ();
		$gridConfig->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'AdSearchTerms' )
		->setPageSize ( 50 )
		->setColumns ( [
				// AdSearchTermGrid::campaignNameField(),
				AdSearchTermGrid::keywordTextField(),
				AdSearchTermGrid::matchTypeField(),
				AdSearchTermGrid::queryField(),
				AdSearchTermGrid::impressionsField(),
				AdSearchTermGrid::clicksField(),
				AdSearchTermGrid::adSpendField(),
				AdSearchTermGrid::salesField(),
				AdSearchTermGrid::acosField(),
				AdSearchTermGrid::cpcField(),
				AdSearchTermGrid::ctrField(),
				AdSearchTermGrid::ordersField(),
				AdSearchTermGrid::crField(),
		] )->setComponents ( [
				AdSearchTermGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] );
		
		$grid = new Grid ($gridConfig);
		
		return $grid;
	}
	
	
	static function campaignNameField() {
		return  (new FieldConfig ())->setName ( 'campaignName' )->setLabel ( 'Campaign Name' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) );
	}

	static function keywordTextField() {
		return  (new FieldConfig ())->setName ( 'keywordText' )->setLabel ( 'Keyword Text' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) );
	}

	static function matchTypeField() {
		return  (new FieldConfig ())->setName ( 'matchType' )->setLabel ( 'Match Type' )->setSortable ( true )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'matchType' )->setMultipleMode ( true )->setSubmittedOnChange ( false )
				->setOptions ( ['broad'=>'Broad','phrase'=>'Phrase','exact'=>'Exact'] )
				);
	}

	static function queryField() {
		return  (new FieldConfig ())->setName ( 'query' )->setLabel ( 'Query' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) );
	}

	static function impressionsField() {
		return  (new FieldConfig ())->setName ( 'total_impressions' )->setLabel ( 'Impr.' );
	}

	static function clicksField() {
		return  (new FieldConfig ())->setName ( 'total_clicks' )->setLabel ( 'Clicks' )->setSortable ( true );
	}

	static function adSpendField() {
		return  (new FieldConfig ())->setName ( 'total_cost' )->setLabel ( 'Ad Spend (USD)' )->setSortable ( true )->setSorting(Grid::SORT_DESC);
	}

	static function salesField() {
		return  (new FieldConfig ())->setName ( 'total_sales' )->setLabel ( 'Sales (USD)' )->setSortable ( true );
	}

	static function acosField() {
		return  (new FieldConfig ())->setName ( '' )->setLabel ( 'ACoS (%)' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return (isset($row->getSrc ()->total_sales) && $row->getSrc ()->total_sales > 0) ? round(($row->getSrc ()->total_cost / $row->getSrc ()->total_sales) * 100, 2) : "0.0";
				} );
	}

	static function cpcField() {
		return  (new FieldConfig ())->setName ( '' )->setLabel ( 'CPC (USD)' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return (isset($row->getSrc ()->total_clicks) && $row->getSrc ()->total_clicks > 0) ? round($row->getSrc ()->total_cost / $row->getSrc ()->total_clicks, 2) : 0;

				} );
	}

	static function ctrField() {
		return  (new FieldConfig ())->setName ( '' )->setLabel ( 'CTR (%)' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return (isset($row->getSrc ()->total_impressions) && $row->getSrc ()->total_impressions > 0) ? round(($row->getSrc ()->total_clicks / $row->getSrc ()->total_impressions) * 100, 2) : 0;

				} );
	}

	static function ordersField() {
		return  (new FieldConfig ())->setName ( 'total_orders' )->setLabel ( 'Orders' )->setSortable ( true );
	}

	static function crField() {
		return  (new FieldConfig ())->setName ( '' )->setLabel ( 'CR (%)' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return (isset($row->getSrc ()->total_clicks) && $row->getSrc ()->total_clicks > 0) ? round(($row->getSrc ()->total_orders / $row->getSrc ()->total_clicks) * 100, 2) : 0;

				} );
	}



	static function headerComponent() {
		return (new THead ())->setComponents ( [

				(new ColumnHeadersRow ()),
				(new FiltersRow ()),
				
				(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [

						(new RecordsPerPage ())->setVariants ( [
								20,
								50,
								100,
								500,
								1000
						] ),
						new ColumnsHider (),
						(new ExcelExport ())->setFileName ( 'AdKeywords-' . date ( 'Y-m-d' ) ),
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
								'class' => 'btn btn-success btn-sm'
						] )
				] ),
				
		] );
	}


}