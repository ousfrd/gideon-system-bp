<?php

namespace App\Grids;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\ReviewCsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use App\Review;
use DB;
use Illuminate\Support\Facades\Gate;

class ReviewGrid extends GeneralGrid {

	protected $_name = 'reviews';
	protected $_pageLimit = 100;
	function __construct($name = null, $limit = 50) {
		if ($name != null) {
			$this->_name = $name;
		}
		
		$this->_pageLimit = $limit;
		
		return $this;
	}

	static function grid($asin) {
		
		// $dataProvider = Review::join('products', function ($join) {
            // $join->on('products.asin', '=', 'reviews.asin');
            // $join->on('products.country', '=', 'reviews.country');
		// });
		if ($asin == '') {
			$dataProvider = Review::whereNotNull('asin');
		} else {
			$dataProvider = Review::where('asin', $asin);
		}

		// $dataProvider = Review::where('asin', $params['asin'])->orderBy ( "created_at", "asc" );
		

		// $dataProvider->select(DB::raw('reviews.*, products.brand'));

        // if(auth()->user()->role == "product manager assistant") {
		// 	$dataProvider->whereIn('products.asin',auth()->user()->productList());
		// }

		// $dataProvider->where('review_exist', 1);
			
		// if (!isset($_GET["Reviews"]["sort"])) {
		// 	$dataProvider->orderByRaw("reviews.created_at DESC, reviews.review_date DESC");
		// }

		$gridConfig = new GridConfig ();
		$gridConfig->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'Reviews' )
		->setPageSize ( 100 )
		->setColumns ( [
				ReviewGrid::reviewDateField(),
				ReviewGrid::reviewCrawlDateField(),
				ReviewGrid::asinField(),
				ReviewGrid::countryField(),
				// ReviewGrid::brandField(),
				ReviewGrid::profileNameField(),
				ReviewGrid::reviewTitleField(),
				// ReviewGrid::reviewTextField(),
				ReviewGrid::reviewRatingField(),
				// ReviewGrid::reviewLinkField(),
				ReviewGrid::reviewIdField(),
				ReviewGrid::imageField(),
				ReviewGrid::videoField(),
				ReviewGrid::reviewExistField(),		
				ReviewGrid::reviewFollowUpField(),		
				ReviewGrid::verifiedPurchaseField(),
				// ReviewGrid::createdAtField()
				ReviewGrid::reviewTypeField(),
		] )->setComponents ( [
				ReviewGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] );
		
		$grid = new Grid ($gridConfig);
		
		return $grid;
	}

	static function reviewDateField() {
		return (new FieldConfig ())->setName ( 'review_date' )->setLabel ( 'Review Date' )->setSortable ( true )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_GTE )->setTemplate ( "grid-components.date" )
					->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
						$dp->getBuilder ()->where ( 'review_date' , '>=', date('Y-m-d',strtotime($value)) );
				} ) )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LSE )->setTemplate ( "grid-components.date" )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
					$dp->getBuilder ()->where ( 'review_date', '<=', date('Y-m-d',strtotime($value)));
				} ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					
					return $val;
					
				});
	}

	static function reviewCrawlDateField() {
		return (new FieldConfig ())->setName ( 'created_at' )->setLabel ( 'Review Crawl Date' )->setSortable ( true )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_GTE )->setTemplate ( "grid-components.date" )
					->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
						$dp->getBuilder ()->where ( 'created_at' , '>=', date('Y-m-d',strtotime($value)) );
				} ) )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LSE )->setTemplate ( "grid-components.date" )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
					$dp->getBuilder ()->where ( 'created_at', '<=', date('Y-m-d',strtotime($value)));
				} ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					
					return date("Y-m-d", strtotime($val));
					
				});
	}

	static function asinField() {
		return  (new FieldConfig ())->setName ( 'asin' )->setLabel ( 'ASIN' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
					$dp->getBuilder ()->where ('reviews.asin', $value);
				} ) )
		->setSortable ( true );
	}


	static function profileNameField() {
		return  (new FieldConfig ())->setName ( 'profile_name' )->setLabel ( 'Profile Name' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) );
	}
	
	static function reviewTitleField() {
		return  (new FieldConfig ())->setName ( 'review_title' )->setLabel ( 'Review Title' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) );
	}

	static function reviewTextField() {
		return  (new FieldConfig ())->setName ( 'review_text' )->setLabel ( 'Review Text' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) );
	}

	static function reviewRatingField() {
		return  (new FieldConfig ())->setName ( 'review_rating' )->setLabel ( 'Review Rating' )->addFilter ( (new SelectFilterConfig ())->setName ( 'review_rating' )->setMultipleMode ( true )->setSubmittedOnChange ( true )-> 
			setOptions ( [ 
					"1" => "1 Star",
					"2" => "2 Stars",
					"3" => "3 Stars",
					"4" => "4 Stars",
					"5" => "5 Stars"
				] ) );
	}

	static function reviewIdField() {
		return (new FieldConfig ())->setName('review_id')
			->setLabel('Review ID')
			->addFilter((new FilterConfig ())->setOperator(FilterConfig::OPERATOR_EQ))
			->setCallback(function($val, ObjectDataRow $row) {
				return '<a href="' . $row->getSrc()->review_link . '" target="_blank">' . $val . '</a>';
			});
	}
	
	static function reviewLinkField() {
		return (new FieldConfig ())->setName('review_link')->setLabel('Review Link');
	}

	static function verifiedPurchaseField() {
		return (new FieldConfig ())->setName ( 'verified_purchase' )->setLabel ( 'Verified Purchase' )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'verified_purchase' )->setSubmittedOnChange ( true )-> 
			setOptions ( [ 
					"1" => "True",
					"0" => "False"
				] ) )->setCallback ( function ($val, ObjectDataRow $row) {
				return $val == 1 ? "True" : "False";
			});
	}
	

	static function headerComponent() {
		return (new THead())->setComponents([
			(new OneCellRow())->setComponents([
				(new Pager ()),
				(new HtmlTag ())->setAttributes([
					'class' => 'pagination summary'
				])->addComponent(new ShowingRecords())
			]),
			(new ColumnHeadersRow()),
			(new FiltersRow()),
			(new OneCellRow())->setRenderSection(RenderableRegistry::SECTION_END )
				->setComponents([
					(new RecordsPerPage ())->setVariants([
						20, 50, 100, 500, 1000
					]),
					new ColumnsHider (),
					(new ReviewCsvExport())->setFileName('Reviews')->setRowsLimit(999999),
					(new HtmlTag())->setContent('<span class="glyphicon glyphicon-refresh"></span> Filter')
						->setTagName('button')
						->setRenderSection(RenderableRegistry::SECTION_END)
						->setAttributes(['class' => 'btn btn-success btn-sm'])
				]),
		]);
	}

	static function imageField() {
		return (new FieldConfig ())
				->setName ( 'image' )
				->setLabel ( 'image' )
				->addFilter ( (new FilterConfig ())
				->setOperator ( FilterConfig::OPERATOR_EQ ))
				->setCallback ( function ($val, ObjectDataRow $row) {
					if ($val == 'no') {
						return ("No");
					} else {
						return '<a  href="' . $row->getSrc ()->image . '" target="_blank">' . "Yes" . '</a>';
					}
				})
				->setSortable ( true );
	}

	static function videoField() {
		
		return (new FieldConfig ())
				->setName ( 'video' )
				->setLabel ( 'video' )
				->setSortable ( true )
				->addFilter ( (new FilterConfig ())
				->setOperator ( FilterConfig::OPERATOR_EQ ))
				->setCallback ( function ($val, ObjectDataRow $row) {
					if ($val == 'no') {
						return ("No");
					} else {
						return '<a  href="' . $row->getSrc ()->video . '" target="_blank">' . "Yes" . '</a>';
					}
				});
	}
	static function reviewExistField() {
		return (new FieldConfig())
			->setName ( 'review_exist' )
			->setLabel ( 'Review Exist' )
			->addFilter ( (new SelectFilterConfig ())
			->setSubmittedOnChange ( true )
			->setOptions ( [ 
				"1" => "Yes",
				"0" => "No"
			] ) )->setCallback ( function ($val, ObjectDataRow $row) {
			return $val == 1 ? "Yes" : "No";
			})
			->setCallback(function ($val, ObjectDataRow $row) {
				if (Gate::allows('self-review')) {
					return '<input value="1" data-size="small" type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" ' . 
										' data-pk="' . $row->getCellValue('review_id') . '"' . 
										' data-url="'.route('review.updateReviewStatus').'" '.($row->getCellValue('review_exist') ? 'checked' : '') .
										' id="review_status"' . ' />';
				} else {
					return $row->getCellValue('review_exist') ? 'Active' : 'Inactive';
				}
			});
	}

	static function reviewFollowUpField() {
		return (new FieldConfig())
			->setName ( 'review_follow_up' )
			->setLabel ( 'Review Follow Up' )
			->addFilter ( (new SelectFilterConfig ())
			->setSubmittedOnChange ( true )
			->setOptions ( [ 
					"1" => "Yes",
					"0" => "No"
				] ) )->setCallback ( function ($val, ObjectDataRow $row) {
				return $val == 1 ? "Yes" : "No";
			})
			->setCallback(function ($val, ObjectDataRow $row) {
				if (Gate::allows('self-review')) {
					return '<input value="1" data-size="small" type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" ' . 
										' data-pk="' . $row->getCellValue('review_id') . '"' . 
										' data-url="'.route('review.updateReviewFollowUpStatus').'" '.($row->getCellValue('review_follow_up') ? 'checked' : '') .
										' id="review_status"' . ' />';
				} else {
					return $row->getCellValue('review_follow_up') ? 'Yes' : 'No';
				}
			});
	}

	static function reviewTypeField() {
		// return (new FieldConfig())->setLabel ( 'Review Category' );
		return (new FieldConfig())
		->setName ( 'category' )
		->setLabel ( 'Review Category' )
		->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) );
    }


}