<?php

namespace App\Grids;

use App\LeaveReviewTask;
use App\User;
use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;

class LeaveReviewTaskGrid extends GeneralGrid {
  protected $_pageLimit = 100;
  protected $_query = NULL;
  protected $_name = '';

	function __construct($query = null, $name = 'leave-review-task-report', $limit = 100) {
    if (!$query) {
      $this->_query = LeaveReviewTask::gridQuery();
    } else {
      $this->_query = $query;
    }
    $this->_name = $name;
    $this->_pageLimit = $limit;
		return $this;
  }

  function generateGrid() {
    $gridConfig = new GridConfig();
    $selfReviewStaffs = array_flip(User::selfReviewStaffs());
    $gridConfig->setName($this->_name)
                ->setDataProvider(new EloquentDataProvider($this->_query))
                ->setCachingTime(0) 
                ->setPageSize ( $this->_pageLimit )
                ->setColumns ( [
                  (new FieldConfig)
                    ->setName('name')
                    ->setLabel('Name')
                    ->setSortable(true)
                    ->addFilter(
                      (new FilterConfig)
                          ->setName('name')
                          ->setOperator(FilterConfig::OPERATOR_EQ)
                    ),
                  (new FieldConfig)
                    ->setName('asin')
                    ->setLabel('ASIN')
                    ->setSortable(true)
                    ->addFilter(
                      (new FilterConfig)
                          ->setName('asin')
                          ->setOperator(FilterConfig::OPERATOR_EQ)
                    ),
                  (new FieldConfig)
                    ->setName('total_review_goal')
                    ->setLabel('Goal'),
                  (new FieldConfig)
                    ->setName('deadline')
                    ->setLabel('deadline')
                    ->setSortable(true),
                  (new FieldConfig)
                    ->setName('owned_buyer_review_goal')
                    ->setLabel('Owned Buyer Goal'),
                  (new FieldConfig)
                    ->setName('owned_buyer_approved_reviews_count')
                    ->setLabel('Owned Buyer Reviews'),
                  (new FieldConfig)
                    ->setLabel('Subtask Overview')
                    ->setCallback(function ($val, ObjectDataRow $row) use($selfReviewStaffs) {
                      $task = $row->getSrc();
                      $subTasks = $task->subTasks;
                      if ($subTasks && count($subTasks) > 0) {
                        $table = "<table class='table table-bordered'>";
                        $table .= "<thead>";
                        $table .= "<tr><th>Name</th><th>Goal</th><th>Progress</th></tr>";
                        $table .= "</thead>";
                        $table .= "<tbody>";
                        foreach ($subTasks as $subtask) {
                          $table .= "<tr><td>" .$selfReviewStaffs[$subtask->user_id]. "</td><td>" .$subtask->goal. "</td><td>" .$subtask->progress. "</td></tr>";
                        }
                        $table .= "</tbody>";
                        $table .= "</table>";

                        return $table;
                      } else {
                        return 'NA';
                      }
                    }),
                  // (new FieldConfig)
                  //   ->setName('owned_buyer_rejected_reviews_count')
                  //   ->setLabel('Owned Buyer Rejected Reviews'),
                  (new FieldConfig)
                    ->setName('invite_review_goal')
                    ->setLabel('Invite Review(测评) Goal'),
                  (new FieldConfig)
                    ->setName('invite_review_approved_reviews_count')
                    ->setLabel('Invite Review(测评) Reviews'),
                  // (new FieldConfig)
                  //   ->setName('invite_review_rejected_reviews_count')
                  //   ->setLabel('Invite Review(测评) Rejected Reviews'),
                  (new FieldConfig)
                    ->setName('created_at')
                    ->setLabel('Created At'),
                  (new FieldConfig)
                    ->setLabel('Action')
                    ->setCallback(function ($val, ObjectDataRow $row) {
                      $leaveReviewTask = $row->getSrc();
                      return '<a class="btn btn-primary" href="'. route('leave-review-task.show', ['leaveReviewTask'=>$leaveReviewTask]). '">View</a> | ' . 
                            '<a class="btn btn-success" href="'. route('leave-review-task.edit', ['leaveReviewTask'=>$leaveReviewTask]). '">Edit</a> | ' .
                            '<button type="button" class="btn btn-danger" 
                                      data-toggle="modal" 
                                      data-target="#confirmDelete" 
                                      data-key-name="Name"
                                      data-key-value="' .$leaveReviewTask->name. '"
                                      data-url="' .route('leave-review-task.destroy', ['leaveReviewTask'=>$leaveReviewTask]). '"
                                      data-campaign-id="' . $leaveReviewTask->id . '" 
                                      data-campaign-name="' . $leaveReviewTask->name . '">Delete</button>';
                    })
                ])
                ->setComponents ( [
                  GeneralGrid::headerComponent(),
                  GeneralGrid::footerComponenet()
                ]) ;
    $grid = new Grid ($gridConfig);
		return $grid;
  }
}
