<?php

namespace App\Grids;
use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;

use DB;
use App\Import;
use App\Account;

class ImportGrid{
	
	static function grid($request=null,$params=[]) {
		$dataProvider = Import::with ( 'account' )->orderBy('created_at', 'desc');

		//print $dataProvider->toSql();
		$gridConfig = new GridConfig ();
		
		$gridConfig
		->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'Emails' )->setPageSize ( 25 )
		->setColumns ( [
				ImportGrid::sellerIdField(),
				ImportGrid::sellerNameField(),
				ImportGrid::typeField(),
				ImportGrid::fileNameField(),
				ImportGrid::statusField(),
				ImportGrid::createdAtField(),
		] )
		->setComponents ( [
				ImportGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] ) ;
		
		
		$grid = new Grid ($gridConfig);
		
		return $grid;
	}

	static function sellerIdField() {
		return  (new FieldConfig ())->setName ( 'seller_id' )->setLabel ( 'Seller Id' );
	}

	static function sellerNameField() {
		$accounts = Account::where('status', 'Active')->get();
		$options = [];
		foreach ($accounts as $key => $account) {
			$options[$account->seller_id] = $account->name . " (" . $account->code . ")";
		}

		return  (new FieldConfig ())->setName ( 'seller_id' )->setLabel ( 'Merchant' )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'seller_id' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( $options ) )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return $row->getSrc()->account->name . ' (' . $row->getSrc()->account->code . ')';
		} );
	}

	static function typeField() {
		$options = [
			"Orders" => "Orders",
            "OrderShipments" => "OrderShipments",
            "Finances" => "Finances",
            "FBA Inventory" => "FBA Inventory",
            "Listings" => "Listings",
            "Ads" => "Ads",
			"Business" => "Business",
			"CampaignsBulkReport" => "CampaignsBulkReport",
			"SearchTerms" => "SearchTerms"
		];
		return  (new FieldConfig ())->setName ( 'type' )->setLabel ( 'Type' )
				->addFilter ( (new SelectFilterConfig ())->setName ( 'type' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( $options ) );
	}

	static function fileNameField() {
		return  (new FieldConfig ())->setName ( 'filename' )->setLabel ( 'Filename' );
	}

	static function statusField() {
		$options = [
			"pending" => "pending",
            "processing" => "processing",
            "imported" => "imported"
		];
		return  (new FieldConfig ())->setName ( 'status' )->setLabel ( 'Status' )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'status' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( $options ) );
	}

	static function createdAtField() {
		return  (new FieldConfig ())->setName ( 'created_at' )->setLabel ( 'Created At' );
	}

	static function headerComponent() {
		return (new THead ())->setComponents ( [
				
				(new OneCellRow ())->setComponents ( [
						(new Pager ()),
						(new HtmlTag ())->setAttributes ( [
								'class' => 'pagination summary'
						] )->addComponent ( new ShowingRecords () )
				] ),
				
				(new ColumnHeadersRow ()),
				(new FiltersRow ()),
				
				(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [
						
						
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-go"></span> Go' )->setTagName ( 'button' )->
						// ->setRenderSection(RenderableRegistry::SECTION_END)
						setAttributes ( [
								'type' => 'button',
								'class' => 'btn btn-success btn-sm  action-go',
								'style' => "margin-right:10px"
						] ),
						
						(new RecordsPerPage ())->setVariants ( [
								20,
								50,
								100,
								500,
								1000
						] ),
						new ColumnsHider (),
						(new ExcelExport ())->setFileName ( 'Imports-' . date ( 'Y-m-d' ) ),
						// new ExcelExport(),
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
								'class' => 'btn btn-success btn-sm'
						] )
				] )
				
		] );
	}
}