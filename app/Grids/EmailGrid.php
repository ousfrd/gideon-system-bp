<?php

namespace App\Grids;

use App\User;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;

use Illuminate\Support\Facades\Auth;
use App\EmailList;
use App\Account;
use App\Order;
use DB;

class EmailGrid extends GeneralGrid {
	
	static function grid($request=null,$params=[]) {
		$dataProvider = EmailList::where ( "email_lists.seller_id", "<>", null );
		

		//print $dataProvider->toSql();
		$gridConfig = new GridConfig ();
		
		$gridConfig
		->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'Emails' )->setPageSize ( 25 )
		->setColumns ( [
				EmailGrid::typeField(),
				EmailGrid::sendFromField(),
				EmailGrid::sendFromEmailField(),
				EmailGrid::sellerIdField(),
				EmailGrid::orderIdField(),
				EmailGrid::sendToField(),
				EmailGrid::sendToEmailField(),
				EmailGrid::orderDateField(),
				EmailGrid::sendDateField(),
				EmailGrid::subjectField(),
				EmailGrid::messageField(),

				// EmailGrid::actionsField(),
		] )
		->setComponents ( [
				EmailGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] ) ;
		
		
		$grid = new Grid ($gridConfig);
		
		return $grid;
	}
	
	
	static function typeField() {
		return 
		(new FieldConfig ())
		->setName ( 'type' )->setLabel ( 'Email Type' )->addFilter ( (new SelectFilterConfig ())->setName ( 'type' )->setSubmittedOnChange ( true )
				->setOptions ( ['Greeting','Delivered','Product Review'] )
				->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
					
					if($value == 0) {
						$dp->getBuilder()->where('type','Greeting');
					}elseif ($value == 1) {
						$dp->getBuilder()->where('type','Delivered');
					}else{
						$dp->getBuilder()->where('type','Product Review');
					}
					
				})
				
				);
	}
	
	static function sendFromField() {
		return  (new FieldConfig ())->setName ( 'sendfrom' )->setLabel ( 'Send From' )->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
				$seller_id = Account::where('name', $value)->first()->seller_id;
					if($seller_id) {
						$dp->getBuilder()->where('email_lists.seller_id',$seller_id);
					}
					
				}) )->setCallback ( function ($val, ObjectDataRow $row) {
			return Account::where('seller_id', $row->getSrc()->seller_id)->first()->name;
		} )->setSortable ( true );
	}
	
	static function sendFromEmailField() {
		return  (new FieldConfig ())->setName ( 'sendfromemail' )->setLabel ( 'Send From Email' )->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
				$seller_id = Account::where('seller_email', $value)->first()->seller_id;
					if($seller_id) {
						$dp->getBuilder()->where('email_lists.seller_id',$seller_id);
					}
					
				}) )->setCallback ( function ($val, ObjectDataRow $row) {
					return Account::where('seller_id', $row->getSrc()->seller_id)->first()->seller_email;
				} );
	}

	static function sellerIdField() {
		return  (new FieldConfig ())->setName ( 'seller_id' )->setLabel ( 'Seller ID' )->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
						$dp->getBuilder()->where('email_lists.seller_id',$value);
					
				}) )->setSortable ( true );
	}

	static function orderIdField() {
		return  (new FieldConfig ())->setName ( 'amazon_order_id' )->setLabel ( 'Amazon Order ID' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) );
	}

	static function sendToField() {
		return  (new FieldConfig ())->setName ( 'to_name' )->setLabel ( 'Send To' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->setSortable ( true );
	}

	static function sendToEmailField() {
		return  (new FieldConfig ())->setName ( 'to_email' )->setLabel ( 'Send To Email' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) );
	}

	static function orderDateField() {
		return  (new FieldConfig ())->setName ( 'order_date' )->setLabel ( 'Purchase Date' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_GTE )->setTemplate ( "grid-components.date" )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
					$dp->getBuilder ()->join('orders', 'email_lists.amazon_order_id', 'orders.AmazonOrderId')->where ( DB::raw ( 'date(orders.PurchaseDate)' ), '>=', $value );
				} ) )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LSE )->setTemplate ( "grid-components.date" )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
					$dp->getBuilder ()->where ( DB::raw ( 'date(orders.PurchaseDate)' ), '<=', $value );
				} ) )
				->setCallback( function ($val, ObjectDataRow $row) {
			return Order::where('AmazonOrderId', $row->getSrc()->amazon_order_id)->first()->PurchaseDate;
		})->setSortable ( true );
	}

	static function sendDateField() {
		return  (new FieldConfig ())->setName ( 'sent_date' )->setLabel ( 'Email Date' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_GTE )->setTemplate ( "grid-components.date" )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
					$dp->getBuilder ()->where ( DB::raw ( 'date(sent_date)' ), '>=', $value );
				} ) )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LSE )->setTemplate ( "grid-components.date" )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
					$dp->getBuilder ()->where ( DB::raw ( 'date(sent_date)' ), '<=', $value );
				} ) )->setSortable ( true )->setSorting(Grid::SORT_DESC);
	}

	static function subjectField() {
		return  (new FieldConfig ())->setName ( 'subject' )->setLabel ( 'Subject' );
	}

	static function messageField() {
		return  (new FieldConfig ())->setName ( 'message' )->setLabel ( 'Message' );
	}

	static function actionsField() {
		return (new FieldConfig ())->setName ( 'id' )->setLabel ( 'Actions' )->setSortable ( true )->setCallback ( function ($val, ObjectDataRow $row) {
			$email = $row->getSrc ();
			return ' <a href="' . route ( "email.view", [ "id" => $email->id ] ) . '">View</a>';
		} ) ;
	}

	static function headerComponent() {
		return (new THead ())->setComponents ( [
				
				(new OneCellRow ())->setComponents ( [
						(new Pager ()),
						(new HtmlTag ())->setAttributes ( [
								'class' => 'pagination summary'
						] )->addComponent ( new ShowingRecords () )
				] ),
				
				(new ColumnHeadersRow ()),
				(new FiltersRow ()),
				
				(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [
						
						
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-go"></span> Go' )->setTagName ( 'button' )->
						// ->setRenderSection(RenderableRegistry::SECTION_END)
						setAttributes ( [
								'type' => 'button',
								'class' => 'btn btn-success btn-sm  action-go',
								'style' => "margin-right:10px"
						] ),
						
						(new RecordsPerPage ())->setVariants ( [
								20,
								50,
								100,
								500,
								1000
						] ),
						new ColumnsHider (),
						(new ExcelExport ())->setFileName ( 'Emails-' . date ( 'Y-m-d' ) ),
						// new ExcelExport(),
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
								'class' => 'btn btn-success btn-sm'
						] )
				] )
				
		] );
	}



}