<?php

namespace App\Grids;
use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use App\Product;
use App\Inventory;
use App\Listing;

use DB;
use App\Account;
use Illuminate\Support\Facades\Auth;

class AccountGrid {
	
	static function grid() {
		if (Auth::user ()->role == 'account manager') {
			$dataProvider = Account::with('users')
				->whereHas('users', function($q) {
					$q->where('users.id', Auth::user()->id);
				})->orderBy('status', 'asc')->orderBy('code', 'asc');
		} else {
			$dataProvider = Account::with('users')
				->where("id", '>', 0)
				->orderBy('status', 'asc')->orderBy('code', 'asc');
		}

		$grid = new Grid ( (new GridConfig ())
				->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
				->setName ( 'Accounts' )
				->setPageSize ( 50 )
				
				->setColumns ( [ 
				(new FieldConfig ())->setName ( 'status' )
				->setLabel ( 'Status' )
				// ->addFilter ( (new SelectFilterConfig ())->setName ( 'status' )->setMultipleMode ( true )->setSubmittedOnChange ( true )
				// 		->setOptions ( [ 
				// 		'Active' => 'Active',
				// 		'Suspended' => 'Suspended',
				// 		'Blocked' => 'Blocked'
				// ] ) )
				,
				
				(new FieldConfig ())->setName ( 'seller_id' )->setLabel ( 'Seller ID' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) )->setCallback ( function ($val, ObjectDataRow $row) {
					return '<a  href="' . route ( "dashboard", [ "account" => $row->getSrc ()->seller_id] ) . '">' . $row->getSrc ()->seller_id . '</a>';
				} )->setSortable ( true ),
				
				(new FieldConfig ())->setName ( 'region' )->setLabel ( 'Region' )->addFilter ( (new SelectFilterConfig ())->setName ( 'region' )->setMultipleMode ( true )->setSubmittedOnChange ( true )
						->setOptions ( config ( 'app.regions' ) ) )
				,

				(new FieldConfig())->setLabel('Marketplaces')
													  ->setCallback(function($val, $row){
															$account = $row->getSrc();
															if (!empty($account->amz_seller_links)) {
																$content = '';
																foreach ($account->amz_seller_links as $marketPlace=>$link) {
																	$content .= '<a target="_blank" href=" ' . $link .' ">' . $marketPlace . '</a>&nbsp;&nbsp;';
																}
																return $content;
															}
														}),
				(new FieldConfig ())
							->setLabel('Managers')
							->setCallback(function($val, $row) {
								$account = $row->getSrc();
								return $account->users->map(function($user){
									return '<a target="_blank" href='.route('user.view', ['id'=>$user->id]).'>'.$user->name.'</a>';
								})->implode(',');
							}),
				(new FieldConfig ())->setName ( 'name' )->setLabel ( 'Name' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) ),
				
				(new FieldConfig ())->setName ( 'code' )->setLabel ( 'Code' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) ),
				
				(new FieldConfig ())->setName ( 'inventory' )->setLabel ( 'Inventory' )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					$seller_id = $row->getSrc ()->seller_id;
					// $query = Inventory::select(DB::raw("sum(warehouse_quantity) as total_qty"))->where('seller_id', $seller_id);
					$query = Listing::select(DB::raw("sum(afn_warehouse_quantity) as total_qty"))->where('seller_id', $seller_id);
					return number_format($query->first()->total_qty);
				}),

				(new FieldConfig ())->setName ( 'inventory_cost' )->setLabel ( 'Inventory Cost' )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					$seller_id = $row->getSrc ()->seller_id;
					// $query = Listing::join('product_inventory', 'product_inventory.id', '=', 'seller_listings.inventory_id')->join('products', 'products.asin', '=', 'seller_listings.asin')->select(DB::raw("sum(product_inventory.warehouse_quantity * (products.cost+products.shipping_cost)) as total"))->where('seller_listings.seller_id', $seller_id);
					$query = Listing::join(
						'products', 'products.id', '=', 'seller_listings.product_id'
						)->select(
							DB::raw("sum(afn_warehouse_quantity * (products.cost + products.shipping_cost)) as total")
						)->where('seller_listings.seller_id', $seller_id);
					return "$".number_format($query->first()->total, 2);
				}),

				(new FieldConfig ())->setName ( 'last_order_updated' )->setLabel ( 'Last Order Update' )
				->setSortable ( true )
				->addFilter ( (new FilterConfig ())
				->setOperator ( FilterConfig::OPERATOR_LIKE ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					if(empty($val)) {
						return 'Never';
					}
					return \DT::convertUTCToTimezone($val,$row->getSrc()->timezone_name);
					
				}),
				
				(new FieldConfig ())
				->setName ( 'last_refund_updated' )
				->setLabel ( 'Last Refund Update' )
				->setSortable ( true )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					if(empty($val)) {
						return 'Never';
					}
					return \DT::convertUTCToTimezone($val,$row->getSrc()->timezone_name);
					
				}),
				
				(new FieldConfig ())
				->setName ( 'last_inventory_report_updated' )
				->setLabel ( 'Last Inventory Report Update' )
				->setSortable ( true )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					if(empty($val)) {
						return 'Never';
					}
					return \DT::convertUTCToTimezone($val,$row->getSrc()->timezone_name);
					
				}),
				(new FieldConfig ())->setName ( 'status' )->setLabel ( 'MWS Status' )->setCallback ( function ($val, ObjectDataRow $row) {
					$account = $row->getSrc ();
					
					if (empty ( $account->mws_auth )) {
						return "Missing token";
					}
					
					if ($account->status == "Suspended" || $account->status == "Blocked") {
						return "Suspended";
					}
					
					return '<span>' . $account->mws_status . "</span> <br/><a class='checkmws' href='" . route ( 'account.checkmws', [ 
							$account->id 
					] ) . "'>Check Now</a>";
				} ),
			
				(new FieldConfig ())->setName ( 'advertising_api' )->setLabel ( 'Advertising API' )->setCallback ( function ($val, ObjectDataRow $row) {
					$account = $row->getSrc ();
					
					if(empty($account->adv_refresh_token)) {
						return '<a href="' . route('account.advertising_api_auth', [ 'id' => $account->id ]) . '">Register</a>';
						// return '<a href="https://www.amazon.com/ap/oa?client_id='. config("app.lwa_client_id") . '&scope=cpc_advertising:campaign_management&response_type=code&redirect_uri=' . route( 'account.advertising_api_auth', [ $account->id ]) . '">Register</a>';
					} else {
						return "Registered";
					}
				} ),

				(new FieldConfig())
				->setName('disburse_enabled')
				->setLabel('Disburse Enabled')
				->setCallback(function($val, ObjectDataRow $row) {
					$account = $row->getSrc();
					$checked = $account->disburse_enabled ? 'checked' : '';
					$url = route('account.ajaxsave');

					return '<input value="1" data-size="small" data-pk="disburse_enabled-' . $account->id . '" data-url="'. $url . '" ' . $checked . ' type="checkbox" data-toggle="toggle"  />';
				}),

				(new FieldConfig())
				->setName('min_disburse_amount')
				->setLabel('Minimum Disburse Amount')
				->setCallback(function ($val, ObjectDataRow $row) {
					$url = route('account.ajaxsave');

					return '<a  class="editable" data-pk="min_disburse_amount-' . $row->getSrc()->id . '" data-url="' . $url . '" data-value="' . $val . '">' . $val . '</a>';
				}),

				(new FieldConfig ())->setName ( 'id' )->setLabel ( 'Actions' )->setSortable ( true )->setCallback ( function ($val, ObjectDataRow $row) {
					$user = $row->getSrc ();
					return '<a data-toggle="modal"  data-target="#editModal"  href="' . route ( "account.edit", ["id" => $user->id ] ) . '">Edit</a>';
				} ) 
		
		] )->
		setComponents ( [ 
				(new THead ())->setComponents ( [ 
						
						(new OneCellRow ())->setComponents ( [ 
								(new Pager ()),
								(new HtmlTag ())->setAttributes ( [ 
										'class' => 'pagination summary' 
								] )->addComponent ( new ShowingRecords () ) 
						] ),
						
						(new ColumnHeadersRow ()),
						(new FiltersRow ()),
						
						(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [ 
								
								// (new HtmlTag)
								// ->setContent('<option value="">Actions</option><option value="auto-reply">Auto Reply</option>')
								// ->setTagName('select')
								// ->setRenderSection(RenderableRegistry::SECTION_BEGIN)
								// ->setAttributes([
								// 'name'=>'action',
								// 'class' => 'input-sm batch-action',
								// 'style'=>"margin-right:5px"
								// ]),
								
								(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-go"></span> Go' )->setTagName ( 'button' )->
								// ->setRenderSection(RenderableRegistry::SECTION_END)
								setAttributes ( [ 
										'type' => 'button',
										'class' => 'btn btn-success btn-sm  action-go',
										'style' => "margin-right:10px" 
								] ),
								
								(new RecordsPerPage ())->setVariants ( [ 
										20,
										50,
										100,
										500,
										1000 
								] ),
								new ColumnsHider (),
								(new ExcelExport ())->setFileName ( 'Seller Accounts-' . date ( 'Y-m-d' ) ),
								// new ExcelExport(),
								(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [ 
										'class' => 'btn btn-success btn-sm' 
								] ) 
						] ) 
				
				] ),
				
				(new TFoot ())->setComponents ( [ 
						// (new TotalsRow(['posts_count', 'comments_count'])),
						// (new TotalsRow(['posts_count', 'comments_count']))
						// ->setFieldOperations([
						// 'posts_count' => TotalsRow::OPERATION_AVG,
						// 'comments_count' => TotalsRow::OPERATION_AVG,
						// ])
						// ,
						(new OneCellRow ())->setComponents ( [ 
								new Pager (),
								(new HtmlTag ())->setAttributes ( [ 
										'class' => 'pull-right' 
								] )->addComponent ( new ShowingRecords () ) 
						] ) 
				] ) 
		
		] ) );
		
		return $grid;
	}
}