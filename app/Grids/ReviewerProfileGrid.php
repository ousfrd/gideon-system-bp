<?php

namespace App\Grids;

use App\ReviewerProfile;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;

use Illuminate\Support\Facades\Auth;
use App\Account;
use DB;

class ReviewerProfileGrid extends GeneralGrid {
	
	static function grid($request=null,$params=[]) {
		$dataProvider = ReviewerProfile::where ( "seller_id", "<>", null )->orderBy("id", "desc");
		

		//print $dataProvider->toSql();
		$gridConfig = new GridConfig ();
		
		$gridConfig
		->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'ReviewerProfile' )->setPageSize ( 25 )
		->setColumns ( [
				ReviewerProfileGrid::sellerField(),
				ReviewerProfileGrid::amazonOrderIdField(),
				ReviewerProfileGrid::amazonAsinField(),
				// ReviewerProfileGrid::reviewStarField(),
				// ReviewerProfileGrid::reviewDateField(),
				ReviewerProfileGrid::profileIdField(),
				ReviewerProfileGrid::buyerNameField(),
				ReviewerProfileGrid::buyerEmailField(),
				ReviewerProfileGrid::treadIdField(),

				// EmailGrid::actionsField(),
		] )
		->setComponents ( [
				GeneralGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] ) ;
		
		
		$grid = new Grid ($gridConfig);
		
		return $grid;
	}
	
	static function sellerField() {
		return  (new FieldConfig ())->setName ( 'seller' )->setLabel ( 'Seller' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->setCallback ( function ($val, ObjectDataRow $row) {
			return Account::where('seller_id', $row->getSrc()->seller_id)->first()->code;
		} );
	}
	
	static function amazonOrderIdField() {
		return  (new FieldConfig ())->setName ( 'amazon_order_id' )->setLabel ( 'Amazon Order ID' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) );
	}

	static function amazonAsinField() {
		return  (new FieldConfig ())->setName ( 'amazon_asin' )->setLabel ( 'Amazon ASIN' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) );
	}

	// static function reviewStarField() {
	// 	return  (new FieldConfig ())->setName ( 'review_star' )->setLabel ( 'Review Star' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) );
	// }

	// static function reviewDateField() {
	// 	return  (new FieldConfig ())->setName ( 'review_date' )->setLabel ( 'Review Date' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) );
	// }

	static function profileIdField() {
		return  (new FieldConfig ())->setName ( 'profile_id' )->setLabel ( 'Profile ID' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) );
	}

	static function buyerNameField() {
		return  (new FieldConfig ())->setName ( 'buyer_name' )->setLabel ( 'Buyer Name' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) );
	}

	static function buyerEmailField() {
		return  (new FieldConfig ())->setName ( 'buyer_email' )->setLabel ( 'Buyer Email' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) );
	}

	static function treadIdField() {
		return  (new FieldConfig ())->setName ( 'tread_id' )->setLabel ( 'Tread ID' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) );
	}


	static function headerComponent() {
		return (new THead ())->setComponents ( [
				
				(new OneCellRow ())->setComponents ( [
						(new Pager ()),
						(new HtmlTag ())->setAttributes ( [
								'class' => 'pagination summary'
						] )->addComponent ( new ShowingRecords () )
				] ),
				
				(new ColumnHeadersRow ()),
				(new FiltersRow ()),
				
				(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [
						
						
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-go"></span> Go' )->setTagName ( 'button' )->
						// ->setRenderSection(RenderableRegistry::SECTION_END)
						setAttributes ( [
								'type' => 'button',
								'class' => 'btn btn-success btn-sm  action-go',
								'style' => "margin-right:10px"
						] ),
						
						(new RecordsPerPage ())->setVariants ( [
								20,
								50,
								100,
								500,
								1000
						] ),
						new ColumnsHider (),
						(new CsvExport ())->setFileName ( 'emails' . date ( 'Y-m-d' ) ),
						// new ExcelExport(),
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
								'class' => 'btn btn-success btn-sm'
						] )
				] )
				
		] );
	}



}