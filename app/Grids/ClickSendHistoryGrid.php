<?php

namespace App\Grids;

use App\ClickSendHistory;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;


class ClickSendHistoryGrid extends GeneralGrid {
  protected $_pageLimit = 100;
  protected $_query = NULL;
  protected $_name = '';
	function __construct($query = null, $name = 'clicksend-history-report', $limit = 100) {
    if (!$query) {
      $this->_query = ClickSendHistory::select("*");
    } else {
      $this->_query = $query;
    }
    $this->_name = $name;
    $this->_pageLimit = $limit;
		return $this;
  }

  function generateGrid() {
    $gridConfig = new GridConfig();
    $gridConfig->setName($this->_name)
               ->setDataProvider(new EloquentDataProvider($this->_query))
               ->setCachingTime(0) 
               ->setPageSize ( $this->_pageLimit )
               ->setColumns ( [
                (new FieldConfig)
                  ->setName('message_id')
                  ->setLabel('Message Id')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('message_id')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  ),

                (new FieldConfig)
                  ->setName('type')
                  ->setLabel('Type')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('type')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  ),
                (new FieldConfig)
                  ->setName('address_name')
                  ->setLabel('Address Name')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('address_name')
                        ->setOperator(FilterConfig::OPERATOR_LIKE)
                  ),
                (new FieldConfig)
                  ->setName('address_line_1')
                  ->setLabel('Address 1')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('address_line_1')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  ),
                (new FieldConfig)
                  ->setName('address_line_2')
                  ->setLabel('Address 2')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('address_line_2')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  ),
                (new FieldConfig)
                  ->setName('address_city')
                  ->setLabel('City')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('address_city')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  ),
                (new FieldConfig)
                  ->setName('address_state')
                  ->setLabel('State')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('address_state')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  ),
                (new FieldConfig)
                  ->setName('address_postal_code')
                  ->setLabel('Postal Code')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('address_postal_code')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  ),
                (new FieldConfig)
                  ->setName('address_country')
                  ->setLabel('Country')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('address_country')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  ),
                (new FieldConfig)
                  ->setName('schedule')
                  ->setLabel('Scheduled Sent')
                  ->setSortable(true),
                (new FieldConfig)
                  ->setName('post_price')
                  ->setLabel('Price')
                  ->setSortable(true),
                (new FieldConfig)
                  ->setLabel('Config')
                  ->setCallback(function($val, ObjectDataRow $row) {
                    $record = $row->getSrc();
                    return "<ul>".
                              "<li>duplex:". $record->duplex ." </li>" .
                              "<li>color: " . $record->colour . "</li>" .
                              "<li>priority:" . $record->priority_post ." </li>" .
                            "</ul>";
                  }),
                (new FieldConfig)
                  ->setName('status')
                  ->setLabel('Status')
                  ->setSortable(true)
                  ->setCallback(function($val, ObjectDataRow $row) {
                    $record = $row->getSrc();
                    return $record->status_text .' / ' . $record->status_code;
                  }),
                (new FieldConfig)
                  ->setName('_return_address')
                  ->setLabel('Return Address')
                  ->setCallback(function($val, ObjectDataRow $row) {
                    $responseArray = json_decode($val, true);
                    $jsonString = json_encode($responseArray, JSON_PRETTY_PRINT);
                    return '<pre>' . $jsonString . '</pre>';
                  }),
                (new FieldConfig)
                  ->setName('date_added')
                  ->setLabel('Date Added')
                  ->setSortable(true),
               ])
               ->setComponents ( [
                  ClickSendHistoryGrid::headerComponent(),
                  GeneralGrid::footerComponenet()
               ] ) ;
    
    $grid = new Grid ($gridConfig);
    if (empty($grid->getInputProcessor()->getInput())) {
      $gridConfig->setDataProvider((new EloquentDataProvider($this->_query))->orderBy('date_added', 'desc'));
    };
		return $grid;
  }
  

}