<?php

namespace App\Grids;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use DB;
use App\AdReport;

class AdGrid extends GeneralGrid {
	static function grid() {
		
		$dataProvider = AdReport::with(['listing','product'])->where('product_id','<>',NULL);
		
		$gridConfig = new GridConfig ();
		$gridConfig->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'Ads' )
		->setPageSize ( 20 )
		->setColumns ( [
				AdGrid::imageField(),
				AdGrid::dateField(),
				AdGrid::asinField(),
				AdGrid::countryField(),
				AdGrid::xiajiaField(),
				AdGrid::typeGrid(),
				AdGrid::qtyField(),
				AdGrid::priceField(),
				AdGrid::totalPriceField()
		] )->setComponents ( [
				GeneralGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] );
		
		$grid = new Grid ($gridConfig);
		
		return $grid;
	}
	
	
	static function asinField() {
		return  (new FieldConfig ())->setName ( 'asin' )->setLabel ( 'ASIN' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )
		->setCallback ( function ($val, ObjectDataRow $row) {
			return '<a  href="' . route ( "product.view", [ "id" => $row->getSrc ()->product_id ] ) . '">' . $row->getSrc ()->asin . '</a>';
		} )->setSortable ( true );
	}
	
	static function dateField(){
		return (new FieldConfig ())->setName ( 'date' )->setLabel ( 'Date' )
		->setSortable ( true )
		->setSorting(Grid::SORT_DESC)
		->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LSE )->setTemplate ( "grid-components.date" )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
			$dp->getBuilder ()->where ( 'date', '=', $value );
		} ) );
	}
	
	static function xiajiaField() {
		return (new FieldConfig ())->setName ( 'xiajia_id' )->setLabel ( 'Xiajia' )
		->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
			return $row->getSrc()->xiajia->name;
		});
		
	}
	
	static function qtyField() {
		return (new FieldConfig ())->setName ( 'qty' )->setLabel ( 'Qty' )
		->setSortable ( true );
	}
	
	static function priceField() {
		return (new FieldConfig ())->setName ( 'unit_price' )->setLabel ( 'Unit Price' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
			return '<a  class="pUpdate unit-cost"  data-pk="unit_price-' . $row->getSrc ()->id . '"  data-value="' . $val . '">' . $val . '</a>';
			
		} );
	}
	
	static function totalPriceField() {
		return (new FieldConfig ())->setName ( 'total_price' )->setLabel ( 'Total Price' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
			return '<a  class="pUpdate"  data-pk="total_price-' . $row->getSrc ()->id . '"  data-value="' . $val . '">' . $val . '</a>';
			
		} );
	}
	
	static function imageField() {
		return
		(new FieldConfig ())
		->setName ( 'id' )->setLabel ( '' )
		->setCallback ( function ($val, ObjectDataRow $row) {
			if (empty ( $row->getSrc()->product->small_image)) {
				return 'N/A';
			}
			$listing =  $row->getSrc()->product;
			return '<a  href="' . $listing->amazonLink () . '" target="_blank" data-placement="right" data-toggle="popover"  data-content="'.$listing->name.'"><img src="' . $listing->small_image. '" style="max-width:100px;max-height:100px"/></a>';
		} );
	}
	
	static function typeGrid() {
		return (new FieldConfig ())->setName ( 'type' )->setLabel ( 'Type' )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'Type' )
				->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( [
						"ZP" => "ZP",
						"VP" => "VP"
				] ) );
	}
}