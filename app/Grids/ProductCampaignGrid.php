<?php

namespace App\Grids;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;

use App\Grids\CollectionDataProvider;


class ProductCampaignGrid extends GeneralGrid {
	public static function grid($productClaimReviewStatistics) {
		$gridConfig = new GridConfig();

		$gridConfig->setDataProvider(new CollectionDataProvider($productClaimReviewStatistics))
			->setName('ProductClaimReviewStatistics')
			->setPageSize($productClaimReviewStatistics->count())
			->setColumns([
	            ProductCampaignGrid::imageField(),
	            ProductCampaignGrid::asinField(),
	            ProductCampaignGrid::aliasField(),
	            ProductCampaignGrid::statusField(),
	            ProductCampaignGrid::orderCountField(),
	            ProductCampaignGrid::pendingOrderCountField(),
	            ProductCampaignGrid::shippedOrderCountField(),
	            ProductCampaignGrid::returnedOrderCountField(),
	            // ProductCampaignGrid::partialReturnedOrderCountField(),
	            ProductCampaignGrid::cancelledOrderCountField(),
	            ProductCampaignGrid::innerPageOrderCountField(),
	            ProductCampaignGrid::toClaimReviewCountField(),
	            ProductCampaignGrid::missingShipmentCountField(),
	            ProductCampaignGrid::claimReviewSentCountField(),
	            ProductCampaignGrid::innerPageClaimReviewSentCountField(),
	            ProductCampaignGrid::claimReviewToSendCountField(),
	            ProductCampaignGrid::claimReviewToSendStartDateField(),
	            ProductCampaignGrid::actionsField(),
			])->setComponents([
	            ProductCampaignGrid::headerComponent(),
				GeneralGrid::footerComponenet()
			]);

		return new Grid($gridConfig);
	}
	
	public static function imageField() {
		return (new FieldConfig())
			->setName('small_image')
			->setLabel('图片')
			->setCallback(function ($val, ObjectDataRow $row) {
				$product = $row->getSrc();
				$amzUrl = $product->amzLink;
				$imageLink = $product->small_image;
				if (empty($imageLink)) {
					$imageLink = "/images/No_Image_Available.jpg";
				}

				return '<a  href="' . $amzUrl . '" target="_blank" data-placement="right" data-toggle="popover"  data-content="' . $product->name . '"><img src="' . $imageLink . '" style="max-width:100px;max-height:100px"/></a>';
			});
	}
	
	public static function asinField() {
		return (new FieldConfig())
			->setName('asin')
			->setLabel('ASIN')
			->setCallback(function ($val, ObjectDataRow $row) {
				$product = $row->getSrc();
				$url = route("product.view", ["id" => $product->id]);

				return '<a  href="' . $url . '">' . $product->asin . '</a> <br />' . $product->country;
			});
	}
	
	public static function aliasField() {
		return  (new FieldConfig())
			->setName('alias')
			->setLabel('别名');
	}
	
	public static function statusField() {
		return (new FieldConfig())
			->setName('status')
			->setLabel('状态')
			->setSortable(TRUE)
			->setCallback(function ($val, ObjectDataRow $row) {
				$product =  $row->getSrc();
				return $product->status > 0 ? 'Active' : 'Inactive';
			});
	}

	public static function orderCountField() {
		return (new FieldConfig())
			->setName('orderCount')
			->setLabel('总订单')
			->setSortable(TRUE);
	}

	public static function pendingOrderCountField() {
		return (new FieldConfig())
			->setName('pendingOrderCount')
			->setLabel('待确定订单')
			->setSortable(TRUE);
	}

	public static function shippedOrderCountField() {
		return (new FieldConfig())
			->setName('shippedOrderCount')
			->setLabel('已发货订单')
			->setSortable(TRUE);
	}

	public static function returnedOrderCountField() {
		return (new FieldConfig())
			->setName('returnedOrderCount')
			->setLabel('退货订单')
			->setSortable(TRUE);
	}

	public static function partialReturnedOrderCountField() {
		return (new FieldConfig())
			->setName('partialReturnedOrderCount')
			->setLabel('部分退货订单')
			->setSortable(TRUE);
	}

	public static function cancelledOrderCountField() {
		return (new FieldConfig())
			->setName('cancelledOrderCount')
			->setLabel('取消订单')
			->setSortable(TRUE);
	}

	public static function innerPageOrderCountField() {
		return (new FieldConfig())
			->setName('innerPageOrderCount')
			->setLabel('内页')
			->setSortable(TRUE);
	}

	public static function missingShipmentCountField() {
		return (new FieldConfig())
			->setName('missingShipmentCount')
			->setLabel('地址缺失')
			->setSortable(TRUE);
	}

	public static function toClaimReviewCountField() {
		return (new FieldConfig())
			->setName('toClaimReviewCount')
			->setLabel('需要索评')
			->setSortable(TRUE);
	}

	public static function claimReviewSentCountField() {
		return (new FieldConfig())
			->setName('claimReviewSentCount')
			->setLabel('已索评')
			->setSortable(TRUE);
	}

	public static function innerPageClaimReviewSentCountField() {
		return (new FieldConfig())
			->setName('innerPageClaimReviewSentCount')
			->setLabel('内页已索评')
			->setSortable(TRUE);
	}

	public static function claimReviewToSendCountField() {
		return (new FieldConfig())
			->setName('claimReviewToSendCount')
			->setLabel('待索评')
			->setSortable(TRUE);
	}

	public static function claimReviewToSendStartDateField() {
		return (new FieldConfig())
			->setName('claimReviewToSendStartDate')
			->setLabel('索评起始时间')
			->setSortable(TRUE);
	}

	public static function actionsField() {
		return (new FieldConfig ())
			->setName('id')
			->setLabel('Actions')
			->setCallback(function ($val, ObjectDataRow $row) {
				$product = $row->getSrc();
				$url = route(
					'claim-review-campaign.update-product-statistic', [
						'asin' => $product->asin,
						'country' => $product->country,
						'start_date' => $product->startDate,
						'end_date' => $product->endDate
					]);
				return '<a href="' . $url . '" target="_blank">Update</a>';
			});
	}

	public static function headerComponent() {
		return (new THead())->setComponents([
				(new ColumnHeadersRow()),
				(new FiltersRow()),
				(new OneCellRow())->setRenderSection(RenderableRegistry::SECTION_END)
					->setComponents([
						(new RecordsPerPage())->setVariants([20, 50, 100, 500, 1000]),
						new ColumnsHider(),
						(new CsvExport())->setFileName('ProductClaimReviewStatistics' . date('Y-m-d')),
						(new HtmlTag())->setContent('<span class="glyphicon glyphicon-refresh"></span>Filter')
							->setTagName('button')
							->setRenderSection(RenderableRegistry::SECTION_END)
							->setAttributes(['class' => 'btn btn-success btn-sm'])
					]),
			]);
	}

}