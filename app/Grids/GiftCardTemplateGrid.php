<?php

namespace App\Grids;

use App\GiftCardTemplate;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;

use Illuminate\Support\Facades\Storage;

class GiftCardTemplateGrid extends GeneralGrid {
  protected $_pageLimit = 100;
  protected $_query = NULL;
  protected $_name = '';
	function __construct($query = null, $name = 'gift-card-template-list', $limit = 100) {
    if (!$query) {
      $this->_query = GiftCardTemplate::orderBy("id", "DESC");
    } else {
      $this->_query = $query;
    }
    $this->_name = $name;
    $this->_pageLimit = $limit;
		return $this;
  }

  function generateGrid() {
    $gridConfig = new GridConfig();
    $gridConfig->setName($this->_name)
               ->setDataProvider(new EloquentDataProvider($this->_query))
               ->setCachingTime(0) 
               ->setPageSize ( $this->_pageLimit )
               ->setColumns ( [
                (new FieldConfig)
                  ->setName('name')
                  ->setLabel('Name')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('name')
                        ->setOperator(FilterConfig::OPERATOR_LIKE)
                  )
                  ->setCallback(function ($val, ObjectDataRow $row) {
                    $giftCardTemplate = $row->getSrc();
                    return '<a target="_blank" href="'.route('gift-card-template.show', ['giftCardTemplate'=>$giftCardTemplate]) .'">' . $giftCardTemplate->name . '</a>';
                  }),
                (new FieldConfig)
                  ->setName('asin')
                  ->setLabel('ASIN')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('asin')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  ),
                (new FieldConfig)
                  ->setName('amount')
                  ->setLabel('Amount'),
                (new FieldConfig)
                  ->setName('channel')
                  ->setLabel('Channel'),
                (new FieldConfig)
                  ->setName('channel_detail')
                  ->setLabel('Channel Detail'),
                (new FieldConfig)
                  ->setName('version')
                  ->setLabel('Version'),
                (new FieldConfig)
                  ->setName('template_type')
                  ->setLabel('type')
                  ->addFilter ( (new SelectFilterConfig ())
                                  ->setName ( 'template_type' )
                                  ->setMultipleMode ( true )
                                  ->setSubmittedOnChange ( true )
                                  ->setOptions ( GiftCardTemplate::$templateTypes) 
                ),
                (new FieldConfig)
                  ->setLabel('Front Design')
                  ->setCallback(function ($val, ObjectDataRow $row) {
                    $giftCardTemplate = $row->getSrc();
                    $imgSrc = $giftCardTemplate->frontDesignUrl;
                    return '<img class="thumbnail" src="' . $imgSrc . '" alt="">';
                }),
                (new FieldConfig)
                  ->setLabel('Back Design')
                  ->setCallback(function ($val, ObjectDataRow $row) {
                    $giftCardTemplate = $row->getSrc();
                    $imgSrc = $giftCardTemplate->backDesignUrl;
                    return '<img class="thumbnail" src="' . $imgSrc . '" alt="">';
                }),
                (new FieldConfig)
                  ->setLabel('Attachement 1')
                  ->setCallback(function ($val, ObjectDataRow $row) {
                    $filePath = $row->getCellValue('attachment1');
                    $fileUrl = Storage::disk(config('giftcard.storage'))->url($filePath);
                    return '<a target="_blank" href="' . $fileUrl . '">attachment 1</a>';
                }),
                (new FieldConfig)
                  ->setLabel('Attachement 2')
                  ->setCallback(function ($val, ObjectDataRow $row) {
                    $filePath = $row->getCellValue('attachment2');
                    $fileUrl = Storage::disk(config('giftcard.storage'))->url($filePath);
                    return '<a target="_blank" href="' . $fileUrl . '">attachment 2</a>';
                }),
                (new FieldConfig)
                  ->setLabel('Note')
                  ->setName('note')
                  ->setCallback(function($val, ObjectDataRow $row) {
                    if ($val && strlen($val) > 300) {
                      return substr($val, 0, 300) . " ... ";
                    } else {
                      return $val;
                    }
                  }),
                (new FieldConfig)
                  ->setLabel('Action')
                  ->setCallback(function ($val, ObjectDataRow $row) {
                    $giftCardTemplate = $row->getSrc();
                    return '<a class="btn btn-primary" href="' . route('gift-card-template.show', ['giftCardTemplate'=>$giftCardTemplate]) . '">View</a>' . ' | ' .
                      '<a class="btn btn-success" href="' . route('gift-card-template.edit', ['giftCardTemplate'=>$giftCardTemplate]) . '">Edit</a>' . ' | ' .
                      '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete" data-url="' . route('gift-card-template.show', ['giftCardTemplate'=>$giftCardTemplate]). '" data-id="' . $giftCardTemplate->id . '" data-name="' . $giftCardTemplate->asin . '">Delete</button>';
                })
               ])
               ->setComponents ( [
                  ClaimReviewCampaignGrid::headerComponent(),
                  GeneralGrid::footerComponenet()
               ] ) ;
    $grid = new Grid ($gridConfig);
		return $grid;
  }
  

}