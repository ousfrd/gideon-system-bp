<?php

namespace App\Grids;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use DB;
use App\Redeem;
use App\Order;
use App\Product;
use Illuminate\Support\Facades\Gate;

class RedeemGrid extends GeneralGrid {

	static function detailGrid($dataProvider) {
		$gridConfig = new GridConfig ();
		$gridConfig->setDataProvider (new EloquentDataProvider($dataProvider))
							->setName ( 'Redeems' )
							->setPageSize ( 100 )->setComponents ( [
									RedeemGrid::headerComponent(),
									GeneralGrid::footerComponenet()
							]);
		$gridConfig->setColumns([
				RedeemGrid::claimReviewCampaignField(),
				RedeemGrid::requestDateField(),
				RedeemGrid::amazonOrderIdField(),
				RedeemGrid::asinField(),
				RedeemGrid::productNameField(),
				RedeemGrid::starField(),
				RedeemGrid::nameField(),
				RedeemGrid::emailField(),
				RedeemGrid::attachmentField(),
				RedeemGrid::isReviewedField(),
				RedeemGrid::noteField(),
				RedeemGrid::reviewLinkField(),
				RedeemGrid::reviewLinkUpdateDateField(),
				RedeemGrid::trackingNumberField(),
				RedeemGrid::sellerIdField(),
				RedeemGrid::shippingAddressField(),
				RedeemGrid::newAddressField(),
				RedeemGrid::countryField(),
				RedeemGrid::sourceField(),
				RedeemGrid::usingTimeField(),
				RedeemGrid::newsletterField(),
		]);

		$grid = new Grid ($gridConfig);	
		return $grid;
	}

	static function grid($type, $claimReviewCampaignId = null) {
		
		$dataProvider = Redeem::with(['order', 'account', 'product']);
		// $dataProvider = Redeem::with(['order', 'account']);
		
		if($type == "free-product") {
			$dataProvider->whereIn('star',[4,5])->where('how_to_help', 'Same Free Product');
		} elseif($type == "giftcard") {
			$dataProvider->whereIn('star',[4,5])->whereIn('how_to_help', ['Amazon Gift Card', ""]);
		}elseif($type == "refund") {
			$dataProvider->where('how_to_help','Get a Full Refund');
		} elseif($type == "replacement") {
			$dataProvider->where('how_to_help','Get a Free Replacement');
		} elseif($type == "other") {
			$dataProvider->whereIn('star',[1,2,3])->whereNotIn('how_to_help',['Get a Full Refund', 'Get a Free Replacement']);
		} elseif ($type == "warehouse_request") {
			$dataProvider->whereIn('star',[4,5])->where('how_to_help', 'Same Free Product')->where('is_warehouse_request', 1);
		} elseif ($type == "out_of_stock") {
			$dataProvider->whereIn('star',[4,5])->where('how_to_help', 'Same Free Product')->where('is_warehouse_request', 1)->where('tracking_number', "not like", "%9%");
		} elseif ($type == "followup") {
			$dataProvider->whereIn('star',[4,5])->whereIn('how_to_help', ['Amazon Gift Card', ""])->where('is_follow_up', 1);
		} elseif ($type == "invite_revew_redeems") {
			$dataProvider->where('how_to_help', 'INVITE_REVIEW_REDEEM');
		} elseif ($type == "invite-review-followup") {
			$dataProvider->where('how_to_help', 'INVITE_REVIEW_REDEEM')->where('is_follow_up', 1);
		}
		
		// if ($type == "warehouse_request" && !isset($_GET["Redeems"]["sort"])) {
		// 	$dataProvider->orderByRaw("if(redeems.tracking_number = '' or redeems.tracking_number is null, 0, 1), redeems.id DESC");
		// }
		if ($type == "warehouse_request") {
			$dataProvider->orderBy('redeems.tracking_number', 'asc')->orderBy('redeems.id', 'desc');
		} else {
			// $dataProvider->orderBy('redeems.requestDate', 'desc');
		}

		if ($claimReviewCampaignId) {
			$dataProvider->where('redeems.claim_review_campaign_id', $claimReviewCampaignId);
		}

		$gridConfig = new GridConfig ();
		$gridConfig->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'Redeems' )
		->setPageSize ( 100 )->setComponents ( [
				RedeemGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] );

		if($type == "free-product") {
			$columns = [
				RedeemGrid::inviteReviewLetterSentField(),
				'claimReviewCampaignField' => RedeemGrid::claimReviewCampaignField(),
				'requestDateField' => RedeemGrid::requestDateField(),
				'amazonOrderIdField' => RedeemGrid::amazonOrderIdField(),
				'asinField' => RedeemGrid::asinField(),
				'productNameField' => RedeemGrid::productNameField(),
				'starField' => RedeemGrid::starField(),
				'nameField' => RedeemGrid::nameField(),
				'emailField' => RedeemGrid::emailField(),
				'attachmentField' => RedeemGrid::attachmentField(),
				'isReviewedField' => RedeemGrid::isReviewedField(),
				'statusField' => RedeemGrid::statusField(),
				'noteField' => RedeemGrid::noteField(),
				'reviewLinkField' => RedeemGrid::reviewLinkField(),
				'reviewLinkUpdateDateField' => RedeemGrid::reviewLinkUpdateDateField(),
				'trackingNumberField' => RedeemGrid::trackingNumberField(),
				'isWarehouseRequestField' => RedeemGrid::isWarehouseRequestField(),
				'isTrackingSendField' => RedeemGrid::isTrackingSendField(),
				'sellerIdField' => RedeemGrid::sellerIdField(),
				RedeemGrid::shippingAddressField(), // leave it deliberately for free product.
				'newAddressField' => RedeemGrid::newAddressField(),
				'countryField' => RedeemGrid::countryField(),
				'sourceField' => RedeemGrid::sourceField(),
				'usingTimeField' => RedeemGrid::usingTimeField(),
				'newsletterField' => RedeemGrid::newsletterField(),				
			];
			
		} elseif($type == "giftcard") {
			$columns = [
				RedeemGrid::inviteReviewLetterSentField(),
				'claimReviewCampaignField' => RedeemGrid::claimReviewCampaignField(),
				'requestDateField' => RedeemGrid::requestDateField(),
				'amazonOrderIdField' => RedeemGrid::amazonOrderIdField(),
				'asinField' => RedeemGrid::asinField(),
				RedeemGrid::countryField(),
				'productNameField' => RedeemGrid::productNameField(),	
				'amountField' => RedeemGrid::amountField(),
				RedeemGrid::starField(),
				RedeemGrid::nameField(),
				RedeemGrid::emailField(),
				RedeemGrid::attachmentField(),
				RedeemGrid::isReviewedField(),
				RedeemGrid::isGiftCardSentField(),
				RedeemGrid::isFollowUpField(),
				RedeemGrid::statusField(),
				RedeemGrid::noteField(),
				RedeemGrid::reviewLinkField(),
				RedeemGrid::reviewLinkUpdateDateField(),
				'sellerIdField' => RedeemGrid::sellerIdField(),
				'shippingAddressField' => RedeemGrid::shippingAddressField(),
				RedeemGrid::sourceField(),
				RedeemGrid::usingTimeField(),
				RedeemGrid::newsletterField(),
			];
		} elseif($type == "refund") {
			$columns = [
				RedeemGrid::claimReviewCampaignField(),
				RedeemGrid::redeemIdField(),
				RedeemGrid::requestDateField(),
				RedeemGrid::amazonOrderIdField(),
				'sellerIdField' => RedeemGrid::sellerIdField(),
				RedeemGrid::asinField(),
				RedeemGrid::productNameField(),				
				'shippingAddressField' => RedeemGrid::shippingAddressField(),				
				RedeemGrid::countryField(),
				RedeemGrid::sourceField(),
				'amountField' => RedeemGrid::amountField(),
				RedeemGrid::usingTimeField(),
				RedeemGrid::starField(),
				RedeemGrid::nameField(),
				RedeemGrid::emailField(),
				RedeemGrid::howToHelpField(),
				RedeemGrid::isRefundedField(),
				RedeemGrid::isRepliedField(),
				RedeemGrid::noteField(),
			];			
		} elseif($type == "replacement") {
			$columns = [
				RedeemGrid::redeemIdField(),
				RedeemGrid::requestDateField(),
				RedeemGrid::amazonOrderIdField(),
				'sellerIdField' => RedeemGrid::sellerIdField(),
				RedeemGrid::asinField(),
				RedeemGrid::productNameField(),				
				'shippingAddressField' => RedeemGrid::shippingAddressField(),				
				RedeemGrid::countryField(),
				RedeemGrid::sourceField(),
				'amountField' => RedeemGrid::amountField(),
				RedeemGrid::usingTimeField(),
				RedeemGrid::starField(),
				RedeemGrid::nameField(),
				RedeemGrid::emailField(),
				RedeemGrid::howToHelpField(),
				RedeemGrid::isReplacementSentField(),
				RedeemGrid::trackingNumberField(),
				RedeemGrid::isRepliedField(),
				RedeemGrid::noteField(),
			];			
		} elseif($type == "other") {
			$columns = [
				// RedeemGrid::redeemIdField(),
				RedeemGrid::claimReviewCampaignField(),
				RedeemGrid::requestDateField(),
				RedeemGrid::amazonOrderIdField(),
				'sellerIdField' => RedeemGrid::sellerIdField(),
				RedeemGrid::asinField(),
				RedeemGrid::productNameField(),	
				RedeemGrid::starField(),
				RedeemGrid::emailField(),
				RedeemGrid::howToHelpField(),
				RedeemGrid::handlingStatusField(),
				RedeemGrid::statusField(),
				RedeemGrid::noteField(),
				RedeemGrid::reviewLinkField(),			
				RedeemGrid::reviewLinkUpdateDateField(),			
				'shippingAddressField' => RedeemGrid::shippingAddressField(),				
				RedeemGrid::countryField(),
				RedeemGrid::sourceField(),
				'amountField' => RedeemGrid::amountField(),
				RedeemGrid::usingTimeField(),
				RedeemGrid::nameField()
			];			
		} elseif($type == "warehouse_request") {
			$columns = [
				RedeemGrid::claimReviewCampaignField(),
				RedeemGrid::redeemIdField(),
				RedeemGrid::requestDateField(),
				RedeemGrid::amazonOrderIdField(),
				'sellerIdField' => RedeemGrid::sellerIdField(),
				RedeemGrid::asinField(),
				RedeemGrid::productNameField(),				
				'shippingAddressField' => RedeemGrid::shippingAddressField(),	
				RedeemGrid::newAddressField(),			
				RedeemGrid::countryField(),
				RedeemGrid::emailField(),
				RedeemGrid::reviewLinkField(),
				RedeemGrid::isWarehouseInStockField(),
				RedeemGrid::trackingNumberField(),
				RedeemGrid::isTrackingSendField(),
				RedeemGrid::noteField(),
			];			
		}elseif($type == "out_of_stock") {
			$columns = [
				RedeemGrid::redeemIdField(),
				RedeemGrid::requestDateField(),
				RedeemGrid::amazonOrderIdField(),
				'sellerIdField' => RedeemGrid::sellerIdField(),
				RedeemGrid::asinField(),
				RedeemGrid::productNameField(),				
				'shippingAddressField' => RedeemGrid::shippingAddressField(),				
				RedeemGrid::countryField(),
				RedeemGrid::emailField(),
				RedeemGrid::isWarehouseInStockField(),
				RedeemGrid::trackingNumberField(),
				RedeemGrid::isTrackingSendField(),
				RedeemGrid::noteField(),
			];			

		}elseif($type == "followup") {
			$columns = [
				RedeemGrid::inviteReviewLetterSentField(),
				RedeemGrid::claimReviewCampaignField(),
				RedeemGrid::requestDateField(),
				RedeemGrid::amazonOrderIdField(),
				'sellerIdField' => RedeemGrid::sellerIdField(),
				RedeemGrid::asinField(),
				RedeemGrid::countryField(),
				'amountField' => RedeemGrid::amountField(),
				RedeemGrid::nameField(),
				RedeemGrid::emailField(),
				RedeemGrid::attachmentField(),
				RedeemGrid::isReviewedField(),
				RedeemGrid::reviewLinkField(),
				RedeemGrid::reviewLinkUpdateDateField(),
				RedeemGrid::isGiftCardSentField(),
				RedeemGrid::isFollowUpField(),
				RedeemGrid::statusField(),
				RedeemGrid::noteField(),
			];			
		} elseif ($type == "invite_revew_redeems") {
			$columns = [
				'requestDateField' => RedeemGrid::requestDateField(),
				'amazonOrderIdField' => RedeemGrid::amazonOrderIdField(),
				'asinField' => RedeemGrid::asinField(),
				RedeemGrid::countryField(),
				'productNameField' => RedeemGrid::productNameField(),	
				'amountField' => RedeemGrid::amountField(),
				RedeemGrid::starField(),
				RedeemGrid::nameField(),
				RedeemGrid::emailField(),
				RedeemGrid::attachmentField(),
				RedeemGrid::isReviewedField(),
				RedeemGrid::isGiftCardSentField(),
				RedeemGrid::isRefundedField(),
				RedeemGrid::isFollowUpField(),
				RedeemGrid::statusField(),
				RedeemGrid::noteField(),
				RedeemGrid::reviewLinkField(),
				RedeemGrid::reviewLinkUpdateDateField(),
				'sellerIdField' => RedeemGrid::sellerIdField(),
				'shippingAddressField' => RedeemGrid::shippingAddressField(),
				RedeemGrid::sourceField()
			];
		} elseif ($type= "invite-review-followup") {
			$columns = [
				RedeemGrid::claimReviewCampaignField(),
				RedeemGrid::requestDateField(),
				RedeemGrid::amazonOrderIdField(),
				'sellerIdField' => RedeemGrid::sellerIdField(),
				RedeemGrid::asinField(),
				RedeemGrid::countryField(),
				'amountField' => RedeemGrid::amountField(),
				RedeemGrid::nameField(),
				RedeemGrid::emailField(),
				RedeemGrid::attachmentField(),
				RedeemGrid::isReviewedField(),
				RedeemGrid::reviewLinkField(),
				RedeemGrid::reviewLinkUpdateDateField(),
				RedeemGrid::isGiftCardSentField(),
				RedeemGrid::isRefundedField(),
				RedeemGrid::isFollowUpField(),
				RedeemGrid::statusField(),
				RedeemGrid::noteField(),
			];
		}
		
		if (Gate::denies('manage-redeems.view-seller-column')) {
			unset($columns['sellerIdField']);
		}
		
		if (Gate::denies('manage-redeems.view-shipping-address-column')) {
			unset($columns['shippingAddressField']);
		}
		
		$gridConfig->setColumns ($columns);

		$grid = new Grid ($gridConfig);
		
		return $grid;
	}
	
	static function getShippingAddress($order) {
		$break = "<br/>";

		$address = $order['ShippingAddressName'] . "	" . $order['ShippingAddressAddressLine1'];
		if (! empty ( $order['ShippingAddressAddressLine2'])) {
			$address .= ", " . $order['ShippingAddressAddressLine2'];
		}
		
		$address .= $break . $order['ShippingAddressCity'];
		if (! empty ( $order['ShippingAddressStateOrRegion'] )) {
			$address .= "$break " . $order['ShippingAddressStateOrRegion'];
		}
		$address .= ", " . $order['ShippingAddressPostalCode'];

		return $address;
	}

	static function claimReviewCampaignField() {
		return (new FieldConfig ())
							->setLabel('CR Campaign')
							->setName('claim_review_campaign_name')
							->setSortable ( true )
							->setCallback(function ($val, ObjectDataRow $row) {
								$redeem = $row->getSrc();
								return '<a target="_blank" href="'. route('claim-review-campaign.show', ['id' => $redeem->claim_review_campaign_id]) .'">' . $redeem->claim_review_campaign_name . '</a>';
							});
	}

	static function redeemIdField() {
		return (new FieldConfig ())
							->setName ( 'id' )
							->setLabel ( 'ID' )
							->setSortable ( true )
							->setCallback(function ($val, ObjectDataRow $row) {
								return '<a target="_blank" href="'. route('redeems.invite-review', ['id' => $val]). '">' . $val. '</a>';
							});
	}

	static function inviteReviewLetterSentField() {
		return (new FieldConfig ())
						->setName('is_invite_review_letter_sent')
						->setLabel('IR letter?')
						->setSortable(true)
						->setCallback(function($val, $row) {
							$id = $row->getSrc()->id;
							$text = empty($val) ? 'No' : 'Yes';
							return '<a target="_blank" href="' . route('redeems.invite-review', ['id' => $id]). '">' . $text. '</a>';
						});
	}

	static function requestDateField() {
		return (new FieldConfig ())
							->setName ( 'requestDate' )
							->setLabel ( 'Request Date' )
							->setSortable ( true )
							->setSorting(Grid::SORT_DESC)
							->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_GTE )->setTemplate ( "grid-components.date" )
								->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
									$dp->getBuilder ()->where ( 'requestDate' , '>=', date('Y-m-d',strtotime($value)) );
							} ) )
							->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LSE )->setTemplate ( "grid-components.date" )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
								$dp->getBuilder ()->where ( 'requestDate', '<=', date('Y-m-d',strtotime($value)));
							} ) )
							->setCallback ( function ($val, ObjectDataRow $row) {
								return $val;
							});
	}
	
	static function amazonOrderIdField() {
		return (new FieldConfig ())->setName ( 'AmazonOrderId' )->setLabel ( 'AmazonOrderId' )->
				setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->setCallback ( function ($val, ObjectDataRow $row) {
			return "<a href='" . route ( "order.view", ["id" => $val ] ) . "' target='_blank'>$val</a>";
		});
	}
		
	static function sellerIdField() {
		return (new FieldConfig ())->setName ( 'seller_id' )->setLabel ( 'Seller' )->
				setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->setCallback ( function ($val, ObjectDataRow $row) {
			return $row->getSrc()->account->name . " (" . $row->getSrc()->account->code . ")";
		});
	}

	static function asinField() {
		return (new FieldConfig ())
						->setName ( 'asin' )
						->setLabel ( 'Asin' )
						->setSortable ( true )
						->addFilter ( 
							(new FilterConfig ())
								->setOperator ( FilterConfig::OPERATOR_EQ ) 
						)
						->setCallback ( function ($val, ObjectDataRow $row) {
								$redeem = $row->getSrc();
								return "<a href='" . $redeem->review_page_link . "' target='_blank'>".$val."</a>";
						});
	}

	static function productNameField() {
		return (new FieldConfig ())->setName ( 'productName' )->setLabel ( 'Product Name' )
				->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp){
					$query = $dp->getBuilder ()->join('products', function($join)
					{
					    $join->on('redeems.asin', '=', 'products.asin');
					    $join->on('redeems.country','=', 'products.country');
					});

					if(trim($value)) $query->where('products.name','like','%'.$value.'%');					
				}) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					$redeem = $row->getSrc();
					$product = $redeem->product;
					$productName = "";
					if ($product) {
						if ($product->alias) {
							$productName = $product->alias;
						} else {
							if ($product->name) {
								$productName = substr($product->name, 0, 20);
							}
						}
					}
					return $productName;	

				} );
	}

	static function shippingAddressField() {
		return (new FieldConfig ())->setName ( 'shippingAddress' )->setLabel ( 'Shipping Address' )
				->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp){
					
					$query = $dp->getBuilder ()->join('orders', 'redeems.AmazonOrderId', '=', 'orders.AmazonOrderId');

					if(trim($value)){
						$query->whereRaw ("( orders.ShippingAddressAddressLine1 like '%".$value."%' 
						OR orders.ShippingAddressAddressLine2 like '%".$value."%' 
						OR orders.ShippingAddressCity like '%".$value."%' 
						OR orders.ShippingAddressStateOrRegion like '%".$value."%' 
						OR orders.ShippingAddressStateOrRegion like '%".$value."%' 
						OR orders.ShippingAddressPostalCode like '%".$value."%' )
						"
					);

					}
	
				}) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					$order = $row->getSrc()->order;
					if($order == NULL) {
						$order = Order::where('AmazonOrderId', $row->getSrc()->AmazonOrderId)->where('seller_id', $row->getSrc()->seller_id)->first();
					}
					return RedeemGrid::getShippingAddress($order);
				} );
	}

	static function newAddressField() {
		return (new FieldConfig ())->setName ( 'new_address' )->setLabel ( 'New Address' )
				->addFilter ( (new FilterConfig ()) );
	}

	static function sourceField() {
		return (new FieldConfig ())->setName ( 'source' )->setLabel ( 'Source' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) );
	}

	static function amountField() {
		return (new FieldConfig())
			->setName('amount')
			->setLabel('Amount')
			->setSortable(TRUE)
			->addFilter(
				(new SelectFilterConfig())
				->setName('amount')
				->setSubmittedOnChange(TRUE)
				->setOptions([ 
					"$5" => "$5",
					"$10" => "$10",
					"$15" => "$15",
					"$20" => "$20",
					"£5" => "£5",
					"£15" => "£15"
				])
			)->setCallback(function ($val, ObjectDataRow $row) {
				$redeem = $row->getSrc();
				return $redeem->getAmount();
			});
	}

	static function usingTimeField() {
		return (new FieldConfig ())->setName ( 'usingTime' )->setLabel ( 'Using Time' )->setSortable ( true )->addFilter ( (new SelectFilterConfig ())->setName ( 'usingTime' )->setMultipleMode ( true )->setSubmittedOnChange ( true )-> 
			setOptions ( [ 
					"less than 7 days"=>"less than 7 days",
					"for a while"=>"for a while",
					"more than 60 days"=>"more than 60 days"
				] ) );
	}
	
	static function starField() {
		return (new FieldConfig ())->setName ( 'star' )->setLabel ( 'Rating' )->setSortable ( true )->addFilter ( (new SelectFilterConfig ())->setName ( 'star' )->setMultipleMode ( true )->setSubmittedOnChange ( true )-> 
			setOptions ( [ 
					"1" => "1 Star",
					"2" => "2 Stars",
					"3" => "3 Stars",
					"4" => "4 Stars",
					"5" => "5 Stars"
				] ) );
	}
	
	static function howToHelpField() {
		return (new FieldConfig ())->setName ( 'how_to_help' )->setLabel ( 'How to Help' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) );
	}

	static function nameField() {
		return (new FieldConfig ())->setName ( 'name' )->setLabel ( 'Name' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) )->setCallback ( function ($val, ObjectDataRow $row) {

			return $row->getSrc()->name;	

		} );
	}

	static function emailField() {
		return (new FieldConfig())
			->setName('email')
			->setLabel('Email')
			->setSortable(TRUE)
			->addFilter( 
				(new FilterConfig ())
				->setFilteringFunc(function($value, EloquentDataProvider $dp) {
					$query = $dp->getBuilder();
					$value = trim($value);
					if ($value) {
						$likeVal = '%' . $value . '%';
						$query->where('redeems.email', 'like', $likeVal)->orWhere('redeems.edited_email', 'like', $likeVal);
					}
				})
			)->setCallback(function($val, ObjectDataRow $row) {
				$redeem = $row->getSrc();
				$email = $redeem->email;
				if (!empty($redeem->edited_email)) {
					$email = $redeem->edited_email;
				}
				return '<a href="#" data-type="text" class="pUpdate" data-pk="edited_email-'.$redeem->id.'" data-title="">' .$email. '</a>';
			});
	}

	static function attachmentField() {
		return (new FieldConfig ())->setName ( 'attachments' )->setLabel ( 'Attachment' )->setCallback ( function ($val, ObjectDataRow $row) {
					$redeem =  $row->getSrc();
					$attachments = $redeem->attachments;
					$result = "";
					$image_address = "34.94.41.11";
					if ($redeem->requestDate > "2021-01-22") {
						$image_address = "thank2u.com";
					}
					foreach (explode("||", $attachments) as $key => $value) {
						if (substr( $value, 0, 4 ) === "http") {
							$result .= "<a target='_blank' href='" . $value . "'>" . "attachment" . "<a/><br>";
						} else {
							$result .= "<a target='_blank' href='https://".$image_address."/upload/" . $redeem->AmazonOrderId . "_" . $redeem->asin . "_" . $value . "'>".$value."</a><br>";
						}
					}
					return $result;
			});
	}

	static function newsletterField() {
		return (new FieldConfig ())->setName ( 'newsletter' )->setLabel ( 'Newsletter' )->setSortable ( true )->addFilter ( (new SelectFilterConfig ())->setName ( 'Newsletter' )->setSubmittedOnChange ( true )-> 
			setOptions ( [ 
					"1" => "Yes",
					"0" => "No"
				] ) )->setCallback ( function ($val, ObjectDataRow $row) {
				return $val == 1 ? "Yes" : "No";
			});
	}

	static function isReviewedField() {
		return (new FieldConfig ())->setName ( 'is_reviewed' )->setLabel ( 'Reviewed?' )->setSortable ( true )
			->addFilter ( (new SelectFilterConfig ())->setName ( 'is_reviewed' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( ["1" => "Yes","0" => "No"] ) )
			->setCallback ( function ($val, ObjectDataRow $row) {
					$redeem =  $row->getSrc();
					return '<input value="1" data-size="small" data-pk="is_reviewed-'.$redeem->id.'" data-url="'.route('redeem.ajaxsave').'" '.($redeem->is_reviewed?'checked':'').' type="checkbox" data-toggle="toggle"  />';
			});
	}

	static function isFreeProductSentField() {
		return (new FieldConfig ())->setName ( 'is_giftcard_sent' )->setLabel ( 'Is Free Product Sent?' )->setSortable ( true )
			->addFilter ( (new SelectFilterConfig ())->setName ( 'is_giftcard_sent' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( ["1" => "Yes","0" => "No"] ) )
			->setCallback ( function ($val, ObjectDataRow $row) {
					$redeem =  $row->getSrc();
					return '<input value="1" data-size="small" data-pk="is_giftcard_sent-'.$redeem->id.'" data-url="'.route('redeem.ajaxsave').'" '.($redeem->is_giftcard_sent?'checked':'').' type="checkbox" data-toggle="toggle"  />';
			});		
	}

	static function isWarehouseRequestField() {
		return (new FieldConfig ())->setName ( 'is_warehouse_request' )->setLabel ( 'Add to Warehouse?' )->setSortable ( true )
			->addFilter ( (new SelectFilterConfig ())->setName ( 'is_warehouse_request' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( ["1" => "Yes","0" => "No"] ) )
			->setCallback ( function ($val, ObjectDataRow $row) {
					$redeem = $row->getSrc();
					return '<input value="1" data-size="small" data-pk="is_warehouse_request-'.$redeem->id.'" data-url="'.route('redeem.ajaxsave').'" '.($redeem->is_warehouse_request?'checked':'').' type="checkbox" data-toggle="toggle"  />';
			});	
	}

	static function isWarehouseInStockField() {
		return (new FieldConfig ())->setName ( 'is_warehouse_instock' )->setLabel ( 'Is Warehouse In Stock?' )->setSortable ( true )
			->addFilter ( (new SelectFilterConfig ())->setName ( 'is_warehouse_instock' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( ["1" => "Yes","0" => "No"] ) )
			->setCallback ( function ($val, ObjectDataRow $row) {
					$redeem =  $row->getSrc();
					return '<input value="1" data-size="small" data-pk="is_warehouse_instock-'.$redeem->id.'" data-url="'.route('redeem.ajaxsave').'" '.($redeem->is_warehouse_instock?'checked':'').' type="checkbox" data-toggle="toggle"  />';
			});		
	}

	static function isGiftCardSentField() {
		return (new FieldConfig ())->setName ( 'is_giftcard_sent' )->setLabel ( 'Is Gift Card Sent?' )->setSortable ( true )
			->addFilter ( (new SelectFilterConfig ())->setName ( 'is_giftcard_sent' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( ["1" => "Yes","0" => "No"] ) )
			->setCallback ( function ($val, ObjectDataRow $row) {
					$redeem =  $row->getSrc();
					return '<input value="1" data-size="small" data-pk="is_giftcard_sent-'.$redeem->id.'" data-url="'.route('redeem.ajaxsave').'" '.($redeem->is_giftcard_sent?'checked':'').' type="checkbox" data-toggle="toggle"  />';
			});		
	}

	static function isFollowUpField() {
		return (new FieldConfig ())->setName ( 'is_follow_up' )->setLabel ( 'Need Follow Up?' )->setSortable ( true )
			->addFilter ( (new SelectFilterConfig ())->setName ( 'is_follow_up' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( ["1" => "Yes","0" => "No"] ) )
			->setCallback ( function ($val, ObjectDataRow $row) {
					$redeem =  $row->getSrc();
					return '<input value="1" data-size="small" data-pk="is_follow_up-'.$redeem->id.'" data-url="'.route('redeem.ajaxsave').'" '.($redeem->is_follow_up?'checked':'').' type="checkbox" data-toggle="toggle"  />';
			});		
	}

	static function isRefundedField() {
		return (new FieldConfig ())->setName ( 'is_refunded' )->setLabel ( 'Is Refunded?' )->setSortable ( true )
			->addFilter ( (new SelectFilterConfig ())->setName ( 'is_refunded' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( ["1" => "Yes","0" => "No"] ) )
			->setCallback ( function ($val, ObjectDataRow $row) {
					$redeem =  $row->getSrc();
					return '<input value="1" data-size="small" data-pk="is_refunded-'.$redeem->id.'" data-url="'.route('redeem.ajaxsave').'" '.($redeem->is_refunded?'checked':'').' type="checkbox" data-toggle="toggle"  />';
			});		
	}

	static function isRepliedField() {
		return (new FieldConfig ())->setName ( 'is_replied' )->setLabel ( 'Is Replied Email?' )->setSortable ( true )
			->addFilter ( (new SelectFilterConfig ())->setName ( 'is_replied' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( ["1" => "Yes","0" => "No"] ) )
			->setCallback ( function ($val, ObjectDataRow $row) {
					$redeem =  $row->getSrc();
					return '<input value="1" data-size="small" data-pk="is_replied-'.$redeem->id.'" data-url="'.route('redeem.ajaxsave').'" '.($redeem->is_replied?'checked':'').' type="checkbox" data-toggle="toggle"  />';
			});		
	}

	static function isReplacementSentField() {
		return (new FieldConfig ())->setName ( 'is_replacement_sent' )->setLabel ( 'Is Replacement Sent?' )->setSortable ( true )
			->addFilter ( (new SelectFilterConfig ())->setName ( 'is_replacement_sent' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( ["1" => "Yes","0" => "No"] ) )
			->setCallback ( function ($val, ObjectDataRow $row) {
					$redeem =  $row->getSrc();
					return '<input value="1" data-size="small" data-pk="is_replacement_sent-'.$redeem->id.'" data-url="'.route('redeem.ajaxsave').'" '.($redeem->is_replacement_sent?'checked':'').' type="checkbox" data-toggle="toggle"  />';
			});		
	}

	static function trackingNumberField() {
		return (new FieldConfig ())->setName ( 'tracking_number' )->setLabel ( 'Tracking#' )->setSortable ( true )
			->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )
			->setCallback ( function ($val, ObjectDataRow $row) {
					return '<a  class="pUpdate tracking_number"  data-pk="tracking_number-' . $row->getSrc ()->id . '"  data-value="' . $val . '">' . $val . '</a>';
			} );
	}

	static function reviewLinkField() {
		return (new FieldConfig ())
						->setName('review_link')
						->setLabel('Review Link')
						->addFilter(
							(new FilterConfig ())
								->setOperator ( FilterConfig::OPERATOR_LIKE))
						->setCallback(function ($val, ObjectDataRow $row) {
							$reviewId = $row->getSrc()->review_id;
							return '<a  class="pUpdate review_link"  data-pk="review_link-' . $row->getSrc ()->id . '"  data-value="' . $val . '">' . $reviewId . '</a>' . '<a class="go-to-review-link" data-toggle="tooltip" data-placement="top" title="Go To Link" href="javascript:void(0)"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>';
						});
	}

	static function reviewLinkUpdateDateField() {
		return (new FieldConfig ())
						->setName('review_link_date')
						->setLabel('Review Link Update Date')
						->setSortable ( true )
						->addFilter(
							(new FilterConfig ())
								->setOperator ( FilterConfig::OPERATOR_LIKE));
	}

	static function isTrackingSendField() {
		return (new FieldConfig ())->setName ( 'is_tracking_send' )->setLabel ( 'Tracking# Sent?' )->setSortable ( true )
			->addFilter ( (new SelectFilterConfig ())->setName ( 'is_tracking_send' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( ["1" => "Yes","0" => "No"] ) )
			->setCallback ( function ($val, ObjectDataRow $row) {
					$redeem =  $row->getSrc();
					return '<input value="1" data-size="small" data-pk="is_tracking_send-'.$redeem->id.'" data-url="'.route('redeem.ajaxsave').'" '.($redeem->is_tracking_send?'checked':'').' type="checkbox" data-toggle="toggle"  />';
			});		
	}

	static function handlingStatusField() {
		return (new FieldConfig ())->setName ( 'handling_status' )->setLabel ( 'Handling Status' )->setSortable ( true )
			->addFilter ( (new SelectFilterConfig ())->setName ( 'Newsletter' )->setSubmittedOnChange ( true )-> 
			setOptions ( ["In Progress","Done"] ) )
			->setCallback ( function ($val, ObjectDataRow $row) {
				return '<a href="#" data-type="select" class="handling_status"  data-pk="handling_status-'.$row->getSrc()->id.'" data-title="">'.$val.'</a>';
			} );
	}

	static function statusField() {
		return (new FieldConfig ())
							->setName('status')
							->setLabel('status')
							->addFilter ( 
								(new SelectFilterConfig ())
									->setName ( 'status' )
									->setOptions(Redeem::STATUS)
									->setMultipleMode ( true )
									->setSubmittedOnChange ( true )
								)
							->setSortable(true)
							->setCallback ( function ($val, ObjectDataRow $row) {

								return '<a href="#" data-type="select" class="pUpdate" data-source=\''. json_encode(Redeem::STATUS) .'\'  data-pk="status-'.$row->getSrc()->id.'" data-title="">'. Redeem::getStatusText($val) . '</a>';
							} );
	}

	static function noteField() {
		return  (new FieldConfig ())->setName ( 'note' )->setLabel ( 'Note' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->setSortable ( true )->setCallback ( function ($val, ObjectDataRow $row) {
					return '<a  class="pUpdate note" data-type="textarea"  data-pk="note-' . $row->getSrc ()->id . '"  data-value="' . $val . '">' . $val . '</a>';
				} );	
	}

	static function headerComponent() {
		return (new THead ())->setComponents ( [
				(new OneCellRow ())->setComponents ( [
						(new Pager ()),
						(new HtmlTag ())->setAttributes ( [
								'class' => 'pagination summary'
						] )->addComponent ( new ShowingRecords () )
				] ),

				(new ColumnHeadersRow ()),
				(new FiltersRow ()),
				
				(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [

						(new RecordsPerPage ())->setVariants ( [
								20,
								50,
								100,
								500,
								1000
						] ),
						new ColumnsHider (),
						(new CsvExport ())->setFileName ( 'Redeems' )->setRowsLimit(999999),
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
								'class' => 'btn btn-success btn-sm'
						] )
				] ),
				
		] );
	}

	
}