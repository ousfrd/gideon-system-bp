<?php 
namespace App\Grids;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;

class GeneralGrid{
	
	static function countryField($name="country", $label="Country") {
		return (new FieldConfig ())
						->setName($name)
						->setLabel($label)
						->setSortable(true)
						->addFilter( (new SelectFilterConfig() )
							->setName($name)
							->setMultipleMode ( true )
							->setSubmittedOnChange ( true )
							->setOptions ( config ( 'app.countries' ) ) );
	}
	
	
	static function headerComponent() {
		return (new THead ())->setComponents ( [
				
				(new OneCellRow ())->setComponents ( [
						(new Pager ()),
						(new HtmlTag ())->setAttributes ( [
								'class' => 'pagination summary'
						] )->addComponent ( new ShowingRecords () )
				] ),
				
				(new ColumnHeadersRow ()),
				(new FiltersRow ()),
				
				(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [

						
						(new RecordsPerPage ())->setVariants ( [
								20,
								50,
								100,
								500,
								1000
						] ),
						new ColumnsHider (),
						(new ExcelExport ())->setFileName ( "default" ),
						// new ExcelExport(),
						(new CsvExport())->setFileName ( "default" ),
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
								'class' => 'btn btn-success btn-sm'
						] )
				] )
				
		] );
	}
	
	static function footerComponenet(){
		return (new TFoot ())->setComponents ( [
				(new OneCellRow ())->setComponents ( [
						new Pager (),
						(new HtmlTag ())->setAttributes ( [
								'class' => 'pull-right'
						] )->addComponent ( new ShowingRecords () )
				] )
		] );
	}
}