<?php

namespace App\Grids;
use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use App\Product;

use DB;
use App\Account;
use Illuminate\Support\Facades\Auth;
use App\InboundShipment;

class InboundShipmentGrid{
	
	static function grid() {
		$dataProvider = InboundShipment::with(['listing','merchant'])->where('shipment_status','<>','DELETED');
		
		if(auth()->user()->role == "product manager") {
			$dataProvider->whereHas('listing',function($q){
				$q->whereIn('asin',auth()->user()->productList());
			});
			
			
		}elseif(auth()->user()->role == "seller account manager") {
			$dataProvider->whereIn('seller_id',auth()->user()->accountList());
		}
		
		
		$grid = new Grid ( (new GridConfig ())
				->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
				->setName ( 'InboundShipment' )
				->setPageSize ( 50 )
				
				->setColumns ( [ 
				
				(new FieldConfig ())->setName ( 'seller_id' )->setLabel ( 'Seller ID' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->setCallback ( function ($val, ObjectDataRow $row) {
					return '<a  href="' . route ( "account.view", [ 
							"id" => $row->getSrc ()->id 
					] ) . '">' . $row->getSrc ()->seller_id . '</a>';
				} )->setSortable ( true ),
				
				(new FieldConfig ())->setName ( 'shipment_id' )->setLabel ( 'Shipment ID' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) ),
				
				
				(new FieldConfig ())->setName ( 'shipment_name' )->setLabel ( 'Name' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) ),
				
				
				(new FieldConfig ())->setName ( 'date_updated' )->setLabel ( 'Last Updated' )
				->setSortable ( true )
				->setSorting(Grid::SORT_DESC)
				->addFilter ( (new FilterConfig ())
				->setOperator ( FilterConfig::OPERATOR_EQ ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					if(empty($val)) {
						return 'Never';
					}
					return \DT::convertUTCToTimezone($val,$row->getSrc()->merchant->timezone_name,'m/d/Y');
					
				}),
				
				
				
				
				(new FieldConfig ())->setName ( 'shipment_status' )
				->setLabel ( 'Status' )
				->addFilter ( (new SelectFilterConfig ())->setName ( 'shipment_status' )->setMultipleMode ( true )->setSubmittedOnChange ( true )
						->setOptions ( [ 
						'WORKING' => 'WORKING',
						'SHIPPED' => 'SHIPPED',
								'RECEIVING' => 'RECEIVING',
								'CLOSED' => 'CLOSED',
								
				] ) )
				,
				
				(new FieldConfig ())->setName ( 'seller_sku' )->setLabel ( 'SKU' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) ),
				
				(new FieldConfig ())->setName ( 'quantity_shipped' )->setLabel ( 'QTY Shipped' )->setSortable ( true )
				//->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )
				,
				
				(new FieldConfig ())->setName ( 'quantity_received' )->setLabel ( 'QTY Received' )->setSortable ( true )
				//->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )
				,
				
				(new FieldConfig ())->setName ( 'unit_cost' )->setLabel ( 'UnitCost' )->setSortable ( true )
				//->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					if(empty($val)) {
						$val = 0;
					}
					return '<a  class="pUpdate unit-cost"  data-pk="unit_cost-' . $row->getSrc ()->id . '"  data-value="' . $val . '">' . $val . '</a>';
				} ),
				
				(new FieldConfig ())->setName ( 'shipping_cost' )->setLabel ( 'ShippingCost' )->setSortable ( true )
				//->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					return '<a  class="pUpdate unit-cost" data-pk="shipping_cost-' . $row->getSrc ()->id . '"  data-value="' . $val . '">' . $val . '</a>';
				} ),
				
				
				(new FieldConfig ())->setName ( 'id' )->setLabel ( 'Actions' )->setSortable ( true )->setCallback ( function ($val, ObjectDataRow $row) {
					$shipment = $row->getSrc ();
					if($shipment->shipment_status == 'WORKING') {
						return '<a onclick="return confirm(\'Please confirm you want to delete this inbound shipment.\')" href="' . route ( "inbound_shipment.delete", ["id" => $shipment->id ] ) . '">Delete</a>';
					}
					//return '<a class="fm"  href="' . route ( "account.edit", ["id" => $user->id ] ) . '">Edit</a>';
				} ) 
		
		] )->
		setComponents ( [ 
				(new THead ())->setComponents ( [ 
						
						(new OneCellRow ())->setComponents ( [ 
								(new Pager ()),
								(new HtmlTag ())->setAttributes ( [ 
										'class' => 'pagination summary' 
								] )->addComponent ( new ShowingRecords () ) 
						] ),
						
						(new ColumnHeadersRow ()),
						(new FiltersRow ()),
						
						(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [ 
								
								// (new HtmlTag)
								// ->setContent('<option value="">Actions</option><option value="auto-reply">Auto Reply</option>')
								// ->setTagName('select')
								// ->setRenderSection(RenderableRegistry::SECTION_BEGIN)
								// ->setAttributes([
								// 'name'=>'action',
								// 'class' => 'input-sm batch-action',
								// 'style'=>"margin-right:5px"
								// ]),
								
								(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-go"></span> Go' )->setTagName ( 'button' )->
								// ->setRenderSection(RenderableRegistry::SECTION_END)
								setAttributes ( [ 
										'type' => 'button',
										'class' => 'btn btn-success btn-sm  action-go',
										'style' => "margin-right:10px" 
								] ),
								
								(new RecordsPerPage ())->setVariants ( [ 
										20,
										50,
										100,
										500,
										1000 
								] ),
								new ColumnsHider (),
								(new ExcelExport ())->setFileName ( 'InboundShipments-' . date ( 'Y-m-d' ) ),
								// new ExcelExport(),
								(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [ 
										'class' => 'btn btn-success btn-sm' 
								] ) 
						] ) 
				
				] ),
				
				(new TFoot ())->setComponents ( [ 
						// (new TotalsRow(['posts_count', 'comments_count'])),
						// (new TotalsRow(['posts_count', 'comments_count']))
						// ->setFieldOperations([
						// 'posts_count' => TotalsRow::OPERATION_AVG,
						// 'comments_count' => TotalsRow::OPERATION_AVG,
						// ])
						// ,
						(new OneCellRow ())->setComponents ( [ 
								new Pager (),
								(new HtmlTag ())->setAttributes ( [ 
										'class' => 'pull-right' 
								] )->addComponent ( new ShowingRecords () ) 
						] ) 
				] ) 
		
		] ) );
		
		return $grid;
	}
}