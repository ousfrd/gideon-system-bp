<?php

namespace App\Grids;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use App\Report;
use App\Account;


class ReportGrid {
	static function grid() {
		$dataProvider = Report::orderBy ( "created_at", "desc" );
		
		$grid = new Grid ( (new GridConfig ())->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )->
		setName ( 'Reports' )->setPageSize ( 20 )->setColumns ( [ 
// 				(new FieldConfig ())->setName ( 'id' )->setLabel ( '<a id="select-all">Select All</a>' )->
// 				setCallback ( function ($val) {
// 					return "<input type='checkbox' class='select-row' name=id[] value='{$val}'/>";
// 				} ),
				
				(new FieldConfig ())->setName ( 'user' )->setLabel ( 'User Name' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->
				setCallback ( function ($val, ObjectDataRow $row) {
					$user = $row->getSrc ()->user;
					return '<a  href="' . route ( "user.view", [ 
							"id" => $user->id 
					] ) . '">' . $user->name . '</a>';
				} ),
				
				(new FieldConfig ())->setName ( 'start_date' )->setLabel ( 'Start Date' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->
				setSortable ( true ),

				(new FieldConfig ())->setName ( 'end_date' )->setLabel ( 'End Date' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->
				setSortable ( true ),

				(new FieldConfig ())->setName ( 'merchant' )->setLabel ( 'Merchant' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->
				setSortable ( true )->
				setCallback ( function ($val, ObjectDataRow $row) {
					$seller_id = $row->getSrc ()->seller_id;
					$account = $seller_id == "all" ? "All Accounts" : Account::where('seller_id', $seller_id)->first()->name;
					return $account;
				} ),
				
				(new FieldConfig ())->setName ( 'type' )->setLabel ( 'Report Type' )->addFilter( (new SelectFilterConfig ())->setName ( 'type' )->setMultipleMode ( true )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( [ 
						"Groups Profit Report" => "Groups Profit Report",
						"Products Profit Report" => "Products Profit Report",
						"Sellers Profit Report" => "Sellers Profit Report",
						"Orders Report" => "Orders Report"
				] ) )->
				setSortable ( true ),

				(new FieldConfig ())->setName ( 'status' )->setLabel ( 'Status' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->
				setSortable ( true )->
				setCallback ( function ($val, ObjectDataRow $row) {
					$id = $row->getSrc ()->id;
					$filename = $row->getSrc ()->filename;

					if(file_exists(storage_path('reports/') . $filename)) {
						return '<a href="/reports/download/'.$id.'" class="btn btn-success btn-xs"><i class="fa fa-download"></i> Download</a>';
					} else {
						return '<a href="#" class="btn btn-success btn-xs"><i class="fa fa-download"></i> File not exist</a>';
					}
					
				} ),

				(new FieldConfig ())->setName ( 'created_at' )->setLabel ( 'Date' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_GTE )->setTemplate ( "grid-components.date" )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
					$dp->getBuilder ()->where ( DB::raw ( 'date(created_at)' ), '>=', $value );
				} ) )
				->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LSE )->setTemplate ( "grid-components.date" )->setFilteringFunc ( function ($value, EloquentDataProvider $dp) {
					$dp->getBuilder ()->where ( DB::raw ( 'date(created_at)' ), '<=', $value );
				} ) )
		
		] )->
		setComponents ( [ 
				(new THead ())->setComponents ( [ 
						
						(new OneCellRow ())->setComponents ( [ 
								(new Pager ()),
								(new HtmlTag ())->setAttributes ( [ 
										'class' => 'pagination summary' 
								] )->addComponent ( new ShowingRecords () ) 
						] ),
						
						(new ColumnHeadersRow ()),
						(new FiltersRow ()),
						
						(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [ 
								
// 								(new HtmlTag ())->setContent ( '<option value="">Actions</option><option  value="auto-reply">Auto Reply</option>' )->setTagName ( 'select' )->setRenderSection ( RenderableRegistry::SECTION_BEGIN )->setAttributes ( [ 
// 										'name' => 'action',
// 										'class' => 'input-sm batch-action',
// 										'style' => "margin-right:5px" 
// 								] ),
								
								(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-go"></span> Go' )->setTagName ( 'button' )->
								// ->setRenderSection(RenderableRegistry::SECTION_END)
								setAttributes ( [ 
										'type' => 'button',
										'class' => 'btn btn-success btn-sm  action-go',
										'style' => "margin-right:10px" 
								] ),
								
								(new RecordsPerPage ())->setVariants ( [ 
										20,
										50,
										100,
										500,
										1000 
								] ),
								new ColumnsHider (),
								
								(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [ 
										'class' => 'btn btn-success btn-sm' 
								] ) 
						] ) 
				
				] ),
				
				(new TFoot ())->setComponents ( [ 
						(new OneCellRow ())->setComponents ( [ 
								new Pager (),
								(new HtmlTag ())->setAttributes ( [ 
										'class' => 'pull-right' 
								] )->addComponent ( new ShowingRecords () ) 
						] ) 
				] ) 
		
		] ) );
		
		return $grid;
	}
}