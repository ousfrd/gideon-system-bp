<?php

namespace App\Grids;
use Nayjest\Grids\GridConfig;
use App\ProductCategory;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\ObjectDataRow;
use DB;
use App\Redeem;
use App\Order;
use App\Product;
use Illuminate\Support\Facades\Gate;

class ProductCategoryGrid extends GeneralGrid {
    public static function grid() {
        $dataprovider = ProductCategory::whereNotNull('category');
        $gridConfig = new GridConfig();
        $gridConfig->setDataProvider(new EloquentDataProvider($dataprovider))
                    ->setName('Product Category')
                    ->setPageSize(25)
                    ->setColumns([
                        ProductCategoryGrid::categoryField(),
                        ProductCategoryGrid::actionField()
                    ])
                    ->setComponents([
                        GeneralGrid::headerComponent(),
					    GeneralGrid::footerComponenet()
                    ]);
        $grid = new Grid($gridConfig);
        return $grid;
    }

    static function categoryField(){
        return (new FieldConfig())
                ->setName('category')
                ->setLabel('Product Category')
                ->addFilter(
                    (new FilterConfig())->setOperator(FilterConfig::OPERATOR_LIKE)
                )
                ->setCallback(function($val, ObjectDataRow $row){
                    $category = $row->getSrc();
                    $categoryName = $category->category;
                    return '<a href="#" data-type="text" class="pUpdate" data-pk="category-'.$category->id.'" data-title="">' .$categoryName. '</a>';
                });
    }
    static function actionField() {
		return (new FieldConfig ())
                  ->setLabel('Action')
                  ->setCallback(function ($val, ObjectDataRow $row) {
					$category = $row->getSrc();
					$categoryId = $category->id;
					$url = route('product-category.delete', ['id' => $categoryId]);
                    return '<button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#confirmDelete" data-category-id="' . $categoryId . '" data-category-name="' . $category->category . '">Delete</button>';
                  });
	}
}
