<?php

namespace App\Grids;

use App\User;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;

use Illuminate\Support\Facades\Auth;
use App\EmailTemplate;
use App\Account;
use DB;

class EmailTemplateGrid extends GeneralGrid {
	static function grid($request=null,$params=[]) {
		$dataProvider = EmailTemplate::where ( "id", "<>", null );
		

		//print $dataProvider->toSql();
		$gridConfig = new GridConfig ();
		
		$gridConfig
		->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'EmailTemplate' )->setPageSize ( 25 )
		->setColumns ( [
				EmailTemplateGrid::typeField(),
				EmailTemplateGrid::subjectField(),
				EmailTemplateGrid::messageField(),
				EmailTemplateGrid::productsField(),
				EmailTemplateGrid::isGeneralField(),
				EmailTemplateGrid::isActiveField(),
				EmailTemplateGrid::actionsField(),
		] )
		->setComponents ( [
				EmailTemplateGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] ) ;
		
		
		$grid = new Grid ($gridConfig);
		
		return $grid;
	}
	
	
	static function typeField() {
		return 
		(new FieldConfig ())
		->setName ( 'type' )->setLabel ( 'Email Type' )->setCallback ( function ($val, ObjectDataRow $row) {
			$email_template = $row->getSrc ();
			$general = $email_template->general == 1 ? "General" : "Specific";
			return $email_template->type . " Email [" . $general . "]";
		});
	}
	
	static function subjectField() {
		return  (new FieldConfig ())->setName ( 'subject' )->setLabel ( 'Subject' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) );
	}

	static function messageField() {
		return  (new FieldConfig ())->setName ( 'message' )->setLabel ( 'Message' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) );
	}

	static function productsField() {
		return  (new FieldConfig ())->setName ( 'products' )->setLabel ( 'Products' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->setCallback ( function ($val, ObjectDataRow $row) {
			$email_template = $row->getSrc ();
			$list = "<ul>";
			foreach($email_template->products as $product) {
				$list .= '<li><a href="' . route ( "product.view", [ "id" => $product->id ] ) . '">'.$product->asin.'</a></li>';
			}
			$list .= "</ul>";
			return $list;
		} );
	}

	static function isGeneralField() {
		return  (new FieldConfig ())->setName ( 'general' )->setLabel ( 'Is General' )->addFilter ( (new SelectFilterConfig ())->setName ( 'general' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( [0 =>'No',1=>'Yes'] ) )
		->setCallback ( function ($val, ObjectDataRow $row) {
			return $val == 1 ? "Yes" : "No";
		});
	}

	static function isActiveField() {
		return  (new FieldConfig ())->setName ( 'active' )->setLabel ( 'Is Active' )->addFilter ( (new SelectFilterConfig ())->setName ( 'active' )->setSubmittedOnChange ( true )-> // on change submit request
				setOptions ( [0 =>'No',1=>'Yes'] ) )
		->setCallback ( function ($val, ObjectDataRow $row) {
			return $val == 1 ? "Yes" : "No";
		});
	}

	static function actionsField() {
		return (new FieldConfig ())->setName ( 'id' )->setLabel ( 'Actions' )->setCallback ( function ($val, ObjectDataRow $row) {
			$email_template = $row->getSrc ();
			return ' <a href="' . route ( "email.template.edit", [ "id" => $email_template->id ] ) . '">Edit</a> <a href="' . route ( "email.template.delete", [ "id" => $email_template->id ] ) . '" onclick="return confirm(\'Are you sure to delete this?\')">Delete</a>';
		} ) ;
	}

	static function headerComponent() {
		return (new THead ())->setComponents ( [
				
				(new OneCellRow ())->setComponents ( [
						(new Pager ()),
						(new HtmlTag ())->setAttributes ( [
								'class' => 'pagination summary'
						] )->addComponent ( new ShowingRecords () )
				] ),
				
				(new ColumnHeadersRow ()),
				(new FiltersRow ()),
				
				(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [
						
						
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-go"></span> Go' )->setTagName ( 'button' )->
						// ->setRenderSection(RenderableRegistry::SECTION_END)
						setAttributes ( [
								'type' => 'button',
								'class' => 'btn btn-success btn-sm  action-go',
								'style' => "margin-right:10px"
						] ),
						
						(new RecordsPerPage ())->setVariants ( [
								20,
								50,
								100,
								500,
								1000
						] ),
						new ColumnsHider (),
						(new ExcelExport ())->setFileName ( 'Email Templates-' . date ( 'Y-m-d' ) ),
						// new ExcelExport(),
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
								'class' => 'btn btn-success btn-sm'
						] )
				] )
				
		] );
	}

}