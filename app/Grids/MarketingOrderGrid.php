<?php
namespace App\Grids;

use Illuminate\Support\Facades\Gate;
use App\Shipment;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;

class MarketingOrderGrid extends GeneralGrid {
  protected $_pageLimit = 100;
  protected $_shipments = [];
  protected $_query = NULL;
  protected $_name = 'marketing-order-report';
  protected $_asin = '';

	function __construct($query, $name = 'marketing-order-report', $limit = 100) {
    $this->_query = $query;
    $this->_name = $name;
    $this->_pageLimit = $limit;
		return $this;
  }

  function generateGrid() {
    $gridConfig = new GridConfig();
    $gridConfig->setName($this->_name)
               ->setDataProvider(new EloquentDataProvider($this->_query))
               ->setCachingTime(0) 
               ->setPageSize ( $this->_pageLimit )
               ->setColumns ( [
                  $this->orderIdColumn(),
                  $this->asinColumn(),
                  $this->skuColumn(),
                  $this->productNameColumn(),
                  $this->inviteReviewColumn(),
                  $this->selfReviewColumn(),
                  $this->emailPromotionReviewColumn(),
                  $this->insertCardReviewColumn(),
                  $this->claimReviewColumn(),
                  $this->customerRealEmailColumn(),
                  $this->customerFacebookAccountColumn(),
                  $this->customerWechatColumn(),
                  $this->reviewTitleColumn(),
                  $this->reviewContentColumn(),
                  $this->reviewLinkColumn(),
                  $this->leftReviewAtColumn(),
                  $this->noteColumn(),
               ])
               ->setComponents ( [
                  ShipmentGrid::headerComponent($this->_name . '-' .date("Y-m-d H:m:s")),
                  GeneralGrid::footerComponenet()
               ] ) ;
    $grid = new Grid ($gridConfig);
		return $grid;
  }

  function orderIdColumn() {
    return (new FieldConfig)
              ->setName('AmazonOrderId')
              ->setLabel('Order ID')
              ->setSortable(true)
              ->addFilter(
                (new FilterConfig)
                    ->setName('AmazonOrderId')
                    ->setOperator(FilterConfig::OPERATOR_EQ)
              )->setCallback(function ($val, ObjectDataRow $row) {
                $orderId = $row->getCellValue("AmazonOrderId");
                return '<a target="_blank" href=" ' . route('order.view', ['id'=>$orderId]) . ' "> ' . $orderId . '</a>';
              });
  }

  

  function asinColumn() {
    return (new FieldConfig)
              ->setLabel('ASIN')
              ->setName('asin');
  }

  function productNameColumn() {
    return (new FieldConfig)
              ->setName('title')
              ->setLabel('Product');
  }

  function skuColumn() {
    return (new FieldConfig)
              ->setName('sku')
              ->setSortable(true)
              ->setLabel('SKU');
  }

  function inviteReviewColumn() {
    return (new FieldConfig)
      ->setLabel('Invite Review<br>(测评)')
      ->addFilter ( 
        (new SelectFilterConfig ())
          ->setName ( 'invite_review' )
          ->setSubmittedOnChange ( true )
          ->setOptions ( ['No', 'Yes'] )
          ->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
            if($value == 0) {
              $dp->getBuilder()->where('invite_review', 0);
            }elseif ($value == 1) {
              $dp->getBuilder()->where('invite_review', 1);
            }
          })
      )
      ->setCallback(function ($val, ObjectDataRow $row) {
        if (Gate::allows('invite-review')) {
          return '<input value="1" data-size="small" type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" ' . 
                                ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('AmazonOrderId'). '||invite_review' . '"' .
                                ' data-url="'.route('marketing.updateReviewStatus').'" '.($row->getCellValue('invite_review') ? 'checked' : '') .
                                ' data-title="invite_review" data-name="invite_review"' .' />';
        } else {
          return $row->getCellValue('invite_review') ? 'Yes' : 'No';
        }
        
      });
  }

  function selfReviewColumn() {
    return (new FieldConfig)
      ->setLabel('Self Review<br>(自评)')
      ->addFilter ( 
        (new SelectFilterConfig ())
          ->setName ( 'self_review' )
          ->setSubmittedOnChange ( true )
          ->setOptions ( ['No', 'Yes'] )
          ->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
            if($value == 0) {
              $dp->getBuilder()->where('self_review', 0);
            }elseif ($value == 1) {
              $dp->getBuilder()->where('self_review', 1);
            }
          })
      )
      ->setCallback(function ($val, ObjectDataRow $row) {
        if (Gate::allows('self-review')) {
          return '<input value="1" data-size="small" type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" ' . 
                                ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('AmazonOrderId'). '||self_review' . '"' .
                                ' data-url="'.route('marketing.updateReviewStatus').'" '.($row->getCellValue('self_review') ? 'checked' : '') .
                                ' id="self_review"' . ' />';
        } else {
          return $row->getCellValue('self_review') ? 'Yes' : 'No';
        }
      });
  }

  function emailPromotionReviewColumn() {
    return (new FieldConfig())
      ->setLabel('Email Promotion Review<br>(email获评)')
      ->addFilter ( 
        (new SelectFilterConfig ())
          ->setName ( 'email_promotion_review' )
          ->setSubmittedOnChange ( true )
          ->setOptions ( ['No', 'Yes'] )
          ->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
            if($value == 0) {
              $dp->getBuilder()->where('email_promotion_review', 0);
            }elseif ($value == 1) {
              $dp->getBuilder()->where('email_promotion_review', 1);
            }
          })
      )
      ->setCallback(function ($val, ObjectDataRow $row) {
        $cellValue = $row->getCellValue('email_promotion_review');
        if (Gate::allows('email-promotion-review')) {
          return '<input value="1" data-size="small" type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" ' . 
                                ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('AmazonOrderId'). '||email_promotion_review'. '"' .
                                ' data-url="'.route('marketing.updateReviewStatus').'" '.($cellValue ? 'checked' : ''). 
                                ' id="email_promotion_review"'.' /><span class="hidden">' .($cellValue ? 'Yes' : 'No').'</span>';
        } else {
          return $cellValue ? 'Yes' : 'No';
        }
      });
  }

  function insertCardReviewColumn() {
    return (new FieldConfig)
      ->setLabel('Insert Card')
      ->setName('insert_card_review')
      ->setCallback(function ($val, ObjectDataRow $row) {
        return $val ? 'Yes' : 'No';
      });
  }

  function claimReviewColumn() {
    return (new FieldConfig)
      ->setLabel('Claim Review<br>(索评)')
      ->setCallback(function ($val, ObjectDataRow $row) {
        $claim_review_id = $row->getCellValue('claim_review_id');
        return $claim_review_id ? 'Yes' : 'No';
      });
  }

  function customerRealEmailColumn() {
    return (new FieldConfig)
        ->setName('customer_real_email')
        ->setLabel("Customer Real Email")
        ->addFilter(
          (new FilterConfig)
              ->setName('customer_real_email')
              ->setOperator(FilterConfig::OPERATOR_EQ)
        )
        ->setCallback(function ($val, ObjectDataRow $row) {
          $customer_real_email = $row->getCellValue('customer_real_email');
          $text = $customer_real_email ? $customer_real_email : "Empty";
          return '<a data-type="text" class="pUpdate" ' . 
                  ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('AmazonOrderId'). '||customer_real_email'. '"' .
                  ' data-url="' . route('marketing.updateCustomerInfo') . '"' . 
                  ' data-value="' . $customer_real_email .'" data-title="">' . $row->getCellValue('customer_real_email'). '</a>';
        });
  }

  function customerFacebookAccountColumn() {
    return (new FieldConfig)
        ->setName('customer_facebook_account')
        ->setLabel("Customer Facebook Account")
        ->addFilter(
          (new FilterConfig)
              ->setName('customer_facebook_account')
              ->setOperator(FilterConfig::OPERATOR_EQ)
        )
        ->setCallback(function ($val, ObjectDataRow $row) {
          $customer_facebook_account = $row->getCellValue('customer_facebook_account');
          $text = $customer_facebook_account ? $customer_facebook_account : "Empty";
          return '<a data-type="text" class="pUpdate" ' . 
                  ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('AmazonOrderId'). '||customer_facebook_account'. '"' .
                  ' data-url="' . route('marketing.updateCustomerInfo') . '"' . 
                  ' data-value="' . $customer_facebook_account .'" data-title="">' . $row->getCellValue('customer_facebook_account'). '</a>';
        });
  }

  function customerWechatColumn() {
    return (new FieldConfig)
        ->setName('customer_wechat_account')
        ->setLabel("Customer Wechat Account")
        ->addFilter(
          (new FilterConfig)
              ->setName('customer_wechat_account')
              ->setOperator(FilterConfig::OPERATOR_EQ)
        )
        ->setCallback(function ($val, ObjectDataRow $row) {
          $customer_wechat_account = $row->getCellValue('customer_wechat_account');
          $text = $customer_wechat_account ? $customer_wechat_account : "Empty";
          return '<a data-type="text" class="pUpdate" ' . 
                  ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('AmazonOrderId'). '||customer_wechat_account'. '"' .
                  ' data-url="' . route('marketing.updateCustomerInfo') . '"' . 
                  ' data-value="' . $customer_wechat_account .'" data-title="">' . $row->getCellValue('customer_wechat_account'). '</a>';
        });
  }

  function noteColumn() {
    return (new FieldConfig)
        ->setName('note')
        ->setLabel("Note")
        ->setCallback(function ($val, ObjectDataRow $row) {
          $note = $row->getCellValue('note');
          $text = $note ? $note : "Empty";
          return '<a data-type="textarea" class="pUpdate" ' . 
                  ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('AmazonOrderId'). '||note'. '"' .
                  ' data-url="' . route('marketing.updateCustomerInfo') . '"' . 
                  ' data-value="' . $note .'" data-title="">' . $row->getCellValue('note'). '</a>';
        });
  }

  function reviewTitleColumn() {
    return (new FieldConfig)
              ->setName('review_title')
              ->setLabel('Review Title')
              ->setSortable(true)
              ->setCallback(function ($val, ObjectDataRow $row) {
                $attr = $row->getCellValue('review_title');
                return '<a data-type="textarea" class="pUpdate" ' . 
                  ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('AmazonOrderId'). '||review_title'. '"' .
                  ' data-url="' . route('marketing.updateCustomerInfo') . '"' . 
                  ' data-value="' . $attr .'" data-title="">' . $attr. '</a>';
              });
  }

  function reviewContentColumn() {
    return (new FieldConfig)
              ->setName('review_content')
              ->setLabel('Review Content')
              ->setSortable(true)
              ->setCallback(function ($val, ObjectDataRow $row) {
                $attr = $row->getCellValue('review_content');
                return '<a data-type="textarea" class="pUpdate" ' . 
                  ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('AmazonOrderId'). '||review_content'. '"' .
                  ' data-url="' . route('marketing.updateCustomerInfo') . '"' . 
                  ' data-value="' . $attr .'" data-title="">' . $attr. '</a>';
              });
  }

  function reviewLinkColumn() {
    return (new FieldConfig)
              ->setName('review_link')
              ->setLabel('Review Link')
              ->setSortable(true)
              ->setCallback(function ($val, ObjectDataRow $row) {
                return '<a data-type="textarea" class="pUpdate" ' . 
                  ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('AmazonOrderId'). '||review_link'. '"' .
                  ' data-url="' . route('marketing.updateCustomerInfo') . '"' . 
                  ' data-value="' . $val .'" data-title="">' . $val. '</a>';
              });
  }

  function leftReviewAtColumn() {
    return (new FieldConfig)
            ->setLabel('Left Review At')
            ->setName('left_review_at')
            ->setCallback(function($val, $row) {
              $attr = $row->getCellValue('left_review_at');
              return '<a data-type="date" class="pUpdate" ' . 
                  ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('AmazonOrderId'). '||left_review_at'. '"' .
                  ' data-url="' . route('marketing.updateCustomerInfo') . '"' . 
                  ' data-value="' . $attr .'" data-title="">' . $attr. '</a>';
            });
  }

}