<?php

namespace App\Grids;

use App\User;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

use App\Brand;
use DB;
use App\Helpers\CurrencyHelper;


class BrandGrid extends GeneralGrid {
	static function grid($request=null,$params=[]) {
		// $dataProvider = Product::with ( [ 'listings' ] );->where ( "name", "<>", null );
		$dataProvider = Brand::whereNotNull('name');
		$gridConfig = new GridConfig ();
		
		$gridConfig
		->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'Brands' )->setPageSize ( 25 )
		->setColumns ( [
				BrandGrid::nameField(),
				BrandGrid::sellerCountField(),
				BrandGrid::productCountField(),
				BrandGrid::actionField()
		] )
		->setComponents ( [
				BrandGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] ) ;
		
		$grid = new Grid ($gridConfig);
		
		return $grid;
	}

	static function nameField() {
		return (new FieldConfig ())
					->setName ( 'name' )
					->setLabel ( 'Brand' )
					->addFilter ( 
						(new FilterConfig ())
							->setOperator ( FilterConfig::OPERATOR_LIKE ) 
					)
					->setCallback ( function ($val, ObjectDataRow $row) {
                
                        return '<a  href="' . route ( "brand.products", [ "brand" => $row->getSrc ()->name ] ) . '" target="_blank">' . $row->getSrc ()->name. '</a>';
		
                    } )
                    ;
	}
	static function sellerCountField() {
		return (new FieldConfig ())
					->setName ( 'seller_count' )
					->setLabel ( 'Seller Count' )
					->setCallback(function ($val, ObjectDataRow $row) {
						$brand = $row->getSrc();
						$brandName = $brand->name;
						return Brand::sellersOfBrandCount($brandName);
					  })
					;
	}
	static function productCountField() {
		return (new FieldConfig ())
					->setName ( 'product_count' )
					->setLabel ( 'Product Count' )
					->setCallback(function ($val, ObjectDataRow $row) {
						$brand = $row->getSrc();
						$brandName = $brand->name;
						return Brand::productsOfBrandCount($brandName);
					  })
					;
	}
	static function actionField() {
		return (new FieldConfig ())
                  ->setLabel('Action')
                  ->setCallback(function ($val, ObjectDataRow $row) {
					$brand = $row->getSrc();
					$brandId = $brand->id;
					$url = route('brand.delete', ['id' => $brandId]);
                    return '<button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#confirmDelete" data-brand-id="' . $brand->id . '" data-brand-name="' . $brand->name . '">Delete</button>';
                  });
	}

	static function headerComponent() {
		return (new THead ())->setComponents ( [

				(new ColumnHeadersRow ()),
				(new FiltersRow ()),
				
				(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [

						(new RecordsPerPage ())->setVariants ( [
								20,
								50,
								100,
								500,
								1000
						] ),
						new ColumnsHider (),
						(new ExcelExport ())->setFileName ( 'Products-' . date ( 'Y-m-d' ) ),
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
								'class' => 'btn btn-success btn-sm'
						] )
				] ),
				
		] );
	}

}