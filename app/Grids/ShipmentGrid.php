<?php
namespace App\Grids;

use Illuminate\Support\Facades\Gate;
use App\Shipment;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use Carbon\Carbon;

class ShipmentGrid extends GeneralGrid {
  protected $_pageLimit = 100;
  protected $_shipments = [];
  protected $_query = NULL;
  protected $_name = 'shipment-report';
  protected $_asin = '';
	function __construct($query, $asin = '', $name = 'shipment-report', $limit = 100) {
    if (!$query) {
      $this->_query = Shipment::whereNotNull("amazon_order_id");
    } else {
      $this->_query = $query;
    }
    $this->_asin = $asin;
    $this->_name = $name;
    $this->_pageLimit = $limit;
		return $this;
  }

  function generateDetailGrid() {
    $gridConfig = new GridConfig();
    $this->_name = "Order Shipment Detail";
    $gridConfig->setName($this->_name)
                ->setDataProvider(new EloquentDataProvider($this->_query))
                ->setCachingTime(0) 
                ->setPageSize ( $this->_pageLimit )
                ->setColumns ( [
                  $this->orderIdColumn(),
                  $this->orderStatusColumn(),
                  $this->asinColumn(),
                  $this->productNameColumn(),
                  $this->skuColumn(),
                  $this->itemUnitPrice(),
                  $this->itemUnitPromotion(),
                  $this->purchaseDateColumn(),
                  $this->paymentDateColumn(),
                  $this->shipmentDateColumn(),
                  $this->buyerEmailColumn(),
                  $this->buyerNameColumn(),
                  $this->buyerPhoneNumberColumn(),
                  $this->quantityShippedColumn(),
                  $this->addressColumn(),
                  $this->recipientNameColumn(),
                  $this->shippingAddress1Column(),
                  $this->shippingAddress2Column(),
                  $this->shippingAddress3Column(),
                  $this->shippingCityColumn(),
                  $this->shippingStateColumn(),
                  $this->shippingPostalCodeColumn(),
                  $this->shippingCountryColumn()
                ])
                ->setComponents ( [
                  ShipmentGrid::headerComponent($this->_name),
                  GeneralGrid::footerComponenet()
                ] ) ;
    $grid = new Grid ($gridConfig);
    return $grid;
  }

  function generateGrid($claimReviewCampaign) {
    $gridConfig = new GridConfig();
    $this->_name = $claimReviewCampaign->description();
    $gridConfig->setName($this->_name)
               ->setDataProvider(new EloquentDataProvider($this->_query))
               ->setCachingTime(0) 
               ->setPageSize ( $this->_pageLimit )
               ->setColumns ( [
                  $this->orderIdColumn(),
                  $this->asinColumn(),
                  $this->productNameColumn(),
                  $this->skuColumn(),
                  $this->itemUnitPrice(),
                  $this->itemUnitPromotion(),
                  $this->purchaseDateColumn(),
                  $this->paymentDateColumn(),
                  $this->addressColumn(),
                  $this->recipientNameColumn(),
                  $this->shippingAddress1Column(),
                  $this->shippingAddress2Column(),
                  $this->shippingAddress3Column(),
                  $this->shippingCityColumn(),
                  $this->shippingStateColumn(),
                  $this->shippingPostalCodeColumn(),
                  $this->shippingCountryColumn(),
                  $this->isSentColumn(),
                  $this->clickSendResponse()
               ])
               ->setComponents ( [
                  ShipmentGrid::headerComponent($this->_name),
                  GeneralGrid::footerComponenet()
               ] ) ;
    $grid = new Grid ($gridConfig);
		return $grid;
  }

  function generateMarketingGrid(){
    $gridConfig = new GridConfig();
    $gridConfig->setName($this->_name)
               ->setDataProvider(new EloquentDataProvider($this->_query))
               ->setCachingTime(0) 
               ->setPageSize ( $this->_pageLimit )
               ->setColumns ( [
                  $this->orderIdColumn(),
                  $this->orderStatusColumn(),
                  $this->asinColumn(),
                  $this->productNameColumn(),
                  $this->skuColumn(),
                  $this->purchaseDateColumn(),
                  $this->paymentDateColumn(),
                  // $this->addressColumn(),
                  $this->inviteReviewColumn(),
                  $this->selfReviewColumn(),
                  $this->emailPromotionReviewColumn(),
                  $this->claimReviewColumn(),
                  $this->customerRealEmailColumn(),
                  $this->customerFacebookAccountColumn(),
                  $this->customerWechatColumn(),
                  $this->noteColumn(),
                  $this->operatorColumn()
               ])
               ->setComponents ( [
                  ShipmentGrid::headerComponent(),
                  GeneralGrid::footerComponenet()
               ] ) ;
    $grid = new Grid ($gridConfig);
		return $grid;
  }

  function orderIdColumn() {
    return (new FieldConfig)
              ->setName('amazon_order_id')
              ->setLabel('Order ID')
              ->setSortable(true)
              ->addFilter(
                (new FilterConfig)
                    ->setName('amazon_order_id')
                    ->setOperator(FilterConfig::OPERATOR_EQ)
              )->setCallback(function ($val, ObjectDataRow $row) {
                $orderId = $row->getCellValue("amazon_order_id");
                return '<a target="_blank" href=" ' . route('order.view', ['id'=>$orderId]) . ' "> ' . $orderId . '</a>';
              });
  }

  function asinColumn() {
    return (new FieldConfig)
              ->setLabel('ASIN')
              ->setCallback(function ($val, ObjectDataRow $row) {
                return $this->_asin ? $this->_asin : $row->getCellValue("asin");
              });
  }

  function orderStatusColumn() {
    return (new FieldConfig)
              ->setLabel('Order Status')
              ->setCallback(function ($val, ObjectDataRow $row) {
                return $row->getCellValue("is_refunded") ? '<label class="text-danger">Refunded</label>' : '<label class="text-success">Shipped</label>';
              });
  }

  function productNameColumn() {
    return (new FieldConfig)
              ->setName('product_name')
              ->setLabel('Product')
              ->setCallback(function ($val, ObjectDataRow $row) {
                return substr($val, 0, 30);
              });
  }
  
  function skuColumn() {
    return (new FieldConfig)
              ->setName('sku')
              ->setSortable(true)
              ->setLabel('SKU');
  }

  function itemUnitPrice() {

    return (new FieldConfig)
              ->setLabel('Item Unit Price')
              ->setSortable(true)
              ->setCallback(function ($val, ObjectDataRow $row) {
                $shipment = $row->getSrc();
                $unitPrice = $shipment->item_price / $shipment->quantity_shipped;
                $content = "Item Price: " . "<b>" . $shipment->item_price . "</b>";
                $content .= "<br>Quantity Shipped: " . "<b>" . $shipment->quantity_shipped . "</b>";
                $content .= "<br>Item Promotion Discount: " . "<b>" . $shipment->item_promotion_discount . "</b>";
                $text = '<a data-toggle="popover" title="Item Price" data-content="'. $content  .'">'. $unitPrice .'</a>';
                return $text;
              });

  }

  function itemUnitPromotion() {
    return (new FieldConfig)
              ->setLabel('Item Promotion Discount')
              ->setCallback(function ($val, ObjectDataRow $row) {
                $shipment = $row->getSrc();
                return $shipment->item_promotion_discount / $shipment->quantity_shipped;
              });
  }

  function purchaseDateColumn() {
    return (new FieldConfig)
                ->setName('purchase_date')
                ->setSortable(true)
                ->setLabel('Purchase Date');
  }

  function paymentDateColumn() {
    return (new FieldConfig)
                ->setName('payments_date')
                ->setSortable(true)
                ->setLabel('Payments Date');
  }

  function shipmentDateColumn() {
    return (new FieldConfig)
                ->setName('shipment_date')
                ->setSortable(true)
                ->setLabel('Shipment Date');
  }

  function buyerEmailColumn() {
    return (new FieldConfig)
                ->setName('buyer_email')
                ->setSortable(true)
                ->setLabel('Buyer Email');
  }

  function buyerNameColumn() {
    return (new FieldConfig)
                ->setName('buyer_name')
                ->setSortable(true)
                ->setLabel('Name');
  }

  function buyerPhoneNumberColumn() {
    return (new FieldConfig)
                ->setName('buyer_phone_number')
                ->setSortable(true)
                ->setLabel('Phone Number');
  }

  function quantityShippedColumn() {
    return (new FieldConfig)
                ->setName('quantity_shipped')
                ->setSortable(true)
                ->setLabel('Quantity');
  }

  function addressColumn() {
    return (new FieldConfig)
              ->setLabel('Address')
              ->setCallback(function ($val, ObjectDataRow $row) {
                $shipment = $row->getSrc();
                return '<span style="white-space: pre-line">' . $shipment->shippingAddress() . "</span>";
              });
  }

  function recipientNameColumn() {
    return (new FieldConfig)
              ->setName('recipient_name')
              ->setLabel("Recipient Name")
              ->addFilter(
                (new FilterConfig)
                    ->setName('recipient_name')
                    ->setOperator(FilterConfig::OPERATOR_LIKE)
              );
  }

  function shippingAddress1Column() {
    return (new FieldConfig)
              ->setName('ship_address_1')
              ->setLabel("Ship Address 1")
              ->addFilter(
                (new FilterConfig)
                    ->setName('ship_address_1')
                    ->setOperator(FilterConfig::OPERATOR_EQ)
              );
  }

  function shippingAddress2Column() {
    return (new FieldConfig)
              ->setName('ship_address_2')
              ->setLabel("Ship Address 2")
              ->addFilter(
                (new FilterConfig)
                    ->setName('ship_address_2')
                    ->setOperator(FilterConfig::OPERATOR_EQ)
              );
  }

  function shippingAddress3Column() {
    return (new FieldConfig)
              ->setName('ship_address_3')
              ->setLabel("Ship Address 3")
              ->addFilter(
                (new FilterConfig)
                    ->setName('ship_address_3')
                    ->setOperator(FilterConfig::OPERATOR_EQ)
              );
  }

  function shippingCityColumn() {
    return (new FieldConfig)
              ->setName('ship_city')
              ->setLabel("Ship City")
              ->addFilter(
                (new FilterConfig)
                    ->setName('ship_city')
                    ->setOperator(FilterConfig::OPERATOR_EQ)
              );
  }

  function shippingStateColumn() {
    return (new FieldConfig)
              ->setName('ship_state')
              ->setLabel("Ship State")
              ->addFilter(
                (new FilterConfig)
                    ->setName('ship_state')
                    ->setOperator(FilterConfig::OPERATOR_EQ)
              );
  }

  function shippingPostalCodeColumn() {
    return (new FieldConfig)
              ->setName('ship_postal_code')
              ->setLabel("Zip")
              ->addFilter(
                (new FilterConfig)
                    ->setName('ship_postal_code')
                    ->setOperator(FilterConfig::OPERATOR_EQ)
              );
  }

  function shippingCountryColumn() {
    return (new FieldConfig)
              ->setName('ship_country')
              ->setLabel("Ship Country")
              ->addFilter(
                (new FilterConfig)
                    ->setName('ship_country')
                    ->setOperator(FilterConfig::OPERATOR_EQ)
              );
  }

  function inviteReviewColumn() {
    return (new FieldConfig)
      ->setLabel('Invite Review<br>(测评)')
      ->setCallback(function ($val, ObjectDataRow $row) {
        if (Gate::allows('invite-review') && false) {
          return '<input value="1" data-size="small" type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" ' . 
                                ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('amazon_order_id'). '||invite_review' . '"' .
                                ' data-url="'.route('marketing.updateReviewStatus').'" '.($row->getCellValue('invite_review') ? 'checked' : '') .
                                ' data-title="invite_review" data-name="invite_review"' .' />';
        } else {
          return $row->getCellValue('invite_review') ? 'Yes' : 'No';
        }
        
      });
  }

  function selfReviewColumn() {
    return (new FieldConfig)
      ->setLabel('Self Review<br>(自评)')
      ->setCallback(function ($val, ObjectDataRow $row) {
        if (Gate::allows('self-review') && false ) {
          return '<input value="1" data-size="small" type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" ' . 
                                ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('amazon_order_id'). '||self_review' . '"' .
                                ' data-url="'.route('marketing.updateReviewStatus').'" '.($row->getCellValue('self_review') ? 'checked' : '') .
                                ' id="self_review"' . ' />';
        } else {
          return $row->getCellValue('self_review') ? 'Yes' : 'No';
        }
      });
  }

  function emailPromotionReviewColumn() {
    return (new FieldConfig)
      ->setLabel('Email Promotion Review<br>(email获评)')
      ->setCallback(function ($val, ObjectDataRow $row) {
        if (Gate::allows('email-promotion-review') && false) {
          return '<input value="1" data-size="small" type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" ' . 
                                ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('amazon_order_id'). '||email_promotion_review'. '"' .
                                ' data-url="'.route('marketing.updateReviewStatus').'" '.($row->getCellValue('email_promotion_review') ? 'checked' : ''). 
                                ' id="email_promotion_review"'.' />';
        } else {
          return $row->getCellValue('email_promotion_review') ? 'Yes' : 'No';
        }
      });
  }

  function claimReviewColumn() {
    return (new FieldConfig)
      ->setLabel('Claim Review<br>(索评)')
      ->setCallback(function ($val, ObjectDataRow $row) {
        $claim_review_id = $row->getCellValue('claim_review_id');
        return $claim_review_id ? 'Yes' : 'No';
      });
  }

  function customerRealEmailColumn() {
    return (new FieldConfig)
        ->setName('customer_real_email')
        ->setLabel("Customer Real Email")
        ->addFilter(
          (new FilterConfig)
              ->setName('customer_real_email')
              ->setOperator(FilterConfig::OPERATOR_EQ)
        )
        ->setCallback(function ($val, ObjectDataRow $row) {
          $customer_real_email = $row->getCellValue('customer_real_email');
          $text = $customer_real_email ? $customer_real_email : "Empty";
          return '<a data-type="text" class="pUpdate" ' . 
                  ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('amazon_order_id'). '||customer_real_email'. '"' .
                  ' data-url="' . route('marketing.updateCustomerInfo') . '"' . 
                  ' data-value="' . $customer_real_email .'" data-title="">' . $row->getCellValue('customer_real_email'). '</a>';
        });
  }

  function customerFacebookAccountColumn() {
    return (new FieldConfig)
        ->setName('customer_facebook_account')
        ->setLabel("Customer Facebook Account")
        ->addFilter(
          (new FilterConfig)
              ->setName('customer_facebook_account')
              ->setOperator(FilterConfig::OPERATOR_EQ)
        )
        ->setCallback(function ($val, ObjectDataRow $row) {
          $customer_facebook_account = $row->getCellValue('customer_facebook_account');
          $text = $customer_facebook_account ? $customer_facebook_account : "Empty";
          return '<a data-type="text" class="pUpdate" ' . 
                  ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('amazon_order_id'). '||customer_facebook_account'. '"' .
                  ' data-url="' . route('marketing.updateCustomerInfo') . '"' . 
                  ' data-value="' . $customer_facebook_account .'" data-title="">' . $row->getCellValue('customer_facebook_account'). '</a>';
        });
  }

  function customerWechatColumn() {
    return (new FieldConfig)
        ->setName('customer_wechat_account')
        ->setLabel("Customer Wechat Account")
        ->addFilter(
          (new FilterConfig)
              ->setName('customer_wechat_account')
              ->setOperator(FilterConfig::OPERATOR_EQ)
        )
        ->setCallback(function ($val, ObjectDataRow $row) {
          $customer_wechat_account = $row->getCellValue('customer_wechat_account');
          $text = $customer_wechat_account ? $customer_wechat_account : "Empty";
          return '<a data-type="text" class="pUpdate" ' . 
                  ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('amazon_order_id'). '||customer_wechat_account'. '"' .
                  ' data-url="' . route('marketing.updateCustomerInfo') . '"' . 
                  ' data-value="' . $customer_wechat_account .'" data-title="">' . $row->getCellValue('customer_wechat_account'). '</a>';
        });
  }

  function noteColumn() {
    return (new FieldConfig)
        ->setName('note')
        ->setLabel("Note")
        ->setCallback(function ($val, ObjectDataRow $row) {
          $note = $row->getCellValue('note');
          $text = $note ? $note : "Empty";
          return '<a data-type="textarea" class="pUpdate" ' . 
                  ' data-pk="' . $row->getCellValue('sku') .'||'. $row->getCellValue('amazon_order_id'). '||note'. '"' .
                  ' data-url="' . route('marketing.updateCustomerInfo') . '"' . 
                  ' data-value="' . $note .'" data-title="">' . $row->getCellValue('note'). '</a>';
        });
  }

  function operatorColumn() {
    return (new FieldConfig)
      ->setLabel('Operated By')
      ->setCallback(function ($val, ObjectDataRow $row) {
        $operator = $row->getCellValue('operator');
        return $operator ? $operator : '';
      });
  }

  function isSentColumn() {
    return (new FieldConfig)
            ->setLabel('click send')
            ->setCallback(function($val, ObjectDataRow $row) {
              $isSent = $row->getCellValue('is_sent');
              if ($isSent) {
                $schedule = $row->getCellValue('click_send_scheduled_send_at');
                return "<p>Scheduled Send At: ". Carbon::createFromTimestamp($schedule)->toDateTimeString() . "</p>";
              } else {
                return "Not Send Yet";
              }
            });
  }

  function clickSendResponse() {
    return (new FieldConfig)
              ->setLabel('click send response')
              ->setCallback(function($val, ObjectDataRow $row) {
                $isSent = $row->getCellValue('is_sent');
                if ($isSent) {
                  $orderId = $row->getCellValue('amazon_order_id');
                  $response = $row->getCellValue('click_send_response');
                  $responseArray = json_decode($response, true);
                  $jsonString = json_encode($responseArray, JSON_PRETTY_PRINT);
                  return '<pre id="order-' .$orderId. '">' . $jsonString . '</pre>';
                } else {
                  return 'N/A';
                }
              });
  }

  static function headerComponent($exportFileName="default") {
		return (new THead ())->setComponents ( [
				
				(new OneCellRow ())->setComponents ( [
						(new Pager ()),
						(new HtmlTag ())->setAttributes ( [
								'class' => 'pagination summary'
						] )->addComponent ( new ShowingRecords () )
				] ),
				
				(new ColumnHeadersRow ()),
				(new FiltersRow ()),
				
				(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [

						
						(new RecordsPerPage ())->setVariants ( [
								20,
								50,
								100,
								500,
								1000
						] ),
						new ColumnsHider (),
						(new ExcelExport ())->setFileName ( $exportFileName ),
						// new ExcelExport(),
						(new CsvExport())->setFileName ( $exportFileName ),
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
								'class' => 'btn btn-success btn-sm'
						] )
				] )
				
		] );
	}

}
