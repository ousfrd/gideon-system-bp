<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Grids;

use Nayjest\Grids\Components\CsvExport;


class ReviewCSVExport extends CsvExport{
	protected function renderCsv()
	{
		$fieldsToExport = ['"ASIN","Country","Profile Name","Review Title","Review Rating","Review ID",image,video,"Review Exist","Verified Purchase","Review Category"'];
		$file = fopen('php://output', 'w');

		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="'. $this->getFileName() .'"');
		header('Pragma: no-cache');

		set_time_limit(0);

		/** @var $provider DataProvider */
		$provider = $this->grid->getConfig()->getDataProvider();

		$this->renderHeader($file);

		$this->resetPagination($provider);
		$provider->reset();
		/** @var DataRow $row */
		while ($row = $provider->getRow()) {
			$output = [];
			$review = $row->getSrc();
			foreach ($this->getFieldMapping() as $k => $v) {
				$output[] = $review->$k;
			}
			fputcsv($file, $output, ",");
		}

		fclose($file);
		exit;
	}

    protected function renderHeader($file)
    {
        fputcsv($file, array_values($this->getFieldMapping()), ',');
    }

    public function getFieldMapping() {
    	return [
    		'review_date' => 'Review Date',
    		'created_at' => 'Review Crawl Date',
    		'asin' => 'ASIN',
    		'country' => 'Country',
    		'profile_name' => 'Profile Name',
    		'review_id' => 'Review ID',
    		'review_rating' => 'Review Rating',
    		'review_link' => 'Review Link',
    		'review_exist' => 'Review Exist',
    		'review_follow_up' => 'Review Follow Up',
    		'review_title' => 'Review Title'
    	];
    }
}