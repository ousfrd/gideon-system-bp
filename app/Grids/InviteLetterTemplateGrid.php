<?php

namespace App\Grids;
use App\InviteReviewLetterTemplate;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;

class InviteLetterTemplateGrid extends GeneralGrid {
  protected $_pageLimit = 100;
  protected $_query = NULL;
  protected $_name = '';

	function __construct($query = null, $name = 'invite-review-letter-template-report', $limit = 100) {
    if (!$query) {
      $this->_query = InviteReviewLetterTemplate::select('*')->where('status', 1);
    } else {
      $this->_query = $query;
    }
    $this->_name = $name;
    $this->_pageLimit = $limit;
		return $this;
  }

  function generateGrid() {
    $gridConfig = new GridConfig();
    $gridConfig->setName($this->_name)
               ->setDataProvider(new EloquentDataProvider($this->_query))
               ->setCachingTime(0) 
               ->setPageSize ( $this->_pageLimit )
               ->setColumns ( [
                (new FieldConfig)
                  ->setName('name')
                  ->setLabel('Name')
                  ->setSortable(true)
                  ->addFilter(
                    (new FilterConfig)
                        ->setName('name')
                        ->setOperator(FilterConfig::OPERATOR_EQ)
                  ),
                self::countryField(),
                (new FieldConfig)
                  ->setName('subject')
                  ->setLabel('Subject')
                  ->setSortable(true),
                (new FieldConfig)
                  ->setLabel('Action')
                  ->setCallback(function($val, $row) {
                    $template = $row->getSrc();
                    $id = $template->id;
                    return '<a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#letter-modal" data-body="' . $template->body. '" data-name=" ' . $template->name . '" data-subject="'. $template->subject .'" data-country="' . $template->country . '">show</a> | ' .
                            '<a href="' .route('invite-review-letter-template.edit', ['id'=>$id]) . '" class="btn btn-success">Edit</a> | ' .
                            '<button type="button" class="btn btn-danger" data-url="' . route('invite-review-letter-template.destroy', ['id' => $id]) . '" data-key-name="Name" data-key-value=" ' . $template->name . '"  data-toggle="modal" data-target="#confirmDelete">Delete</button>';
                  })
               ])
               ->setComponents ( [
                  InviteLetterTemplateGrid::headerComponent(),
                  GeneralGrid::footerComponenet()
               ] ) ;
    
    $grid = new Grid ($gridConfig);
    if (empty($grid->getInputProcessor()->getInput())) {
      $gridConfig->setDataProvider((new EloquentDataProvider($this->_query))->orderBy('id', 'desc'));
    };
		return $grid;
  }


}
