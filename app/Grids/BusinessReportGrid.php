<?php

namespace App\Grids;

use App\BusinessReport;
use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;

class BusinessReportGrid {
	static function grid() {
		$dataProvider = BusinessReport::groupBy('child_asin')
            ->groupBy('parent_asin')
            ->orderBy ( "created_time", "desc" );
		
		$grid = new Grid ( (new GridConfig ())->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )->
		setName ( 'Reports' )->setPageSize ( 20 )->setColumns ( [

            (new FieldConfig ())
                ->setName ( 'seller_id' )
                ->setLabel ( 'Seller' )
                ->setSortable ( true )
                ->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) ),

            (new FieldConfig ())
                ->setName ( 'parent_asin' )
                ->setLabel ( 'Parent Asin' )
                ->setSortable ( true )
                ->addFilter ( (new FilterConfig ())
                    ->setOperator ( FilterConfig::OPERATOR_EQ ) ),

            (new FieldConfig ())->setName ( 'child_asin' )->setLabel ( 'Child Asin' )
                ->setSortable ( true )
                ->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )
                ->setCallback ( function ($val) {
                    return '<a  href="' . route ( "business.report", [
                            "asin" => $val
                        ] ) . '">' . $val . '</a>';
                } ),

            (new FieldConfig ())->setName ( 'title' )->setLabel ( 'Title' )
                ->setSortable ( true )
                ->addFilter ( (new FilterConfig ())
                    ->setOperator ( FilterConfig::OPERATOR_EQ ) ),

            (new FieldConfig ())->setName ( 'sku' )->setLabel ( 'SKU' )
                ->setSortable ( true )
                ->addFilter ( (new FilterConfig ())
                    ->setOperator ( FilterConfig::OPERATOR_EQ ) ),

            (new FieldConfig ())->setName ( 'created_time' )->setLabel ( 'Created Time' )
                ->setSortable ( true ),
		
		] )->
		setComponents ( [ 
				(new THead ())->setComponents ( [ 
						
						(new OneCellRow ())->setComponents ( [ 
								(new Pager ()),
								(new HtmlTag ())->setAttributes ( [ 
										'class' => 'pagination summary' 
								] )->addComponent ( new ShowingRecords () ) 
						] ),
						
						(new ColumnHeadersRow ()),
						(new FiltersRow ()),
						
						(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [ 

								(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-go"></span> Go' )->setTagName ( 'button' )->
								// ->setRenderSection(RenderableRegistry::SECTION_END)
								setAttributes ( [ 
										'type' => 'button',
										'class' => 'btn btn-success btn-sm  action-go',
										'style' => "margin-right:10px" 
								] ),
								
								(new RecordsPerPage ())->setVariants ( [ 
										20,
										50,
										100,
										500,
										1000 
								] ),
								new ColumnsHider (),
								
								(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [ 
										'class' => 'btn btn-success btn-sm' 
								] ) 
						] ) 
				
				] ),
				
				(new TFoot ())->setComponents ( [ 
						(new OneCellRow ())->setComponents ( [ 
								new Pager (),
								(new HtmlTag ())->setAttributes ( [ 
										'class' => 'pull-right' 
								] )->addComponent ( new ShowingRecords () ) 
						] ) 
				] ) 
		
		] ) );
		
		return $grid;
	}

    static function report($asin)
    {
        $dataProvider = BusinessReport::where('child_asin', $asin)
            ->orderBy ( "report_date", "desc" );

        $grid_config = new GridConfig ();
        $grid_config->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
            ->setName ( 'BusinessReports' )->setPageSize ( 20 )
            ->setColumns ( [

                (new FieldConfig ())->setName ( 'seller_id' )->setLabel ( 'Seller Name' )
                    ->setSortable ( true )
                    ->setCallback ( function ($val, ObjectDataRow $row) {
                        return $row->getSrc()->merchant->name;
                    } ),

                (new FieldConfig ())->setName ( 'report_date' )->setLabel ( 'Report Date' )
                    ->setSortable ( true ),

                (new FieldConfig ())->setName ( 'sessions' )->setLabel ( 'Sessions' )
                    ->setSortable ( true )
                    ->setCallback ( function ($val, ObjectDataRow $row) {
                        return $val . '/' . $row->getSrc()->session_percentage;
                    } ),

                (new FieldConfig ())->setName ( 'page_views' )->setLabel ( 'Page Views' )
                    ->setSortable ( true )
                    ->setCallback ( function ($val, ObjectDataRow $row) {
                        return $val . '/' . $row->getSrc()->page_views_percentage;
                    } ),

                (new FieldConfig ())->setName ( 'units_ordered' )->setLabel ( 'Units Ordered' )
                    ->setSortable ( true )
                    ->setCallback ( function ($val, ObjectDataRow $row) {
                        return $val . '/' . $row->getSrc()->unit_session_percentage;
                    } ),

                (new FieldConfig ())->setName ( 'units_ordered' )->setLabel ( 'Orderd Conversion Rate' )
                    ->setSortable ( true )
                    ->setCallback ( function ($val, ObjectDataRow $row) {
                        if((float)$row->getSrc()->sessions > 0){
                            $rate = ((float)$row->getSrc()->units_ordered / (float)$row->getSrc()->sessions) * 100;
                        } else {
                            $rate = 0.00;
                        }
                        return number_format($rate, 2) . '%';
                    } ),

                (new FieldConfig ())->setName ( 'ordered_product_sales' )->setLabel ( 'Ordered Sales' )
                    ->setSortable ( true ),

                (new FieldConfig ())->setName ( 'total_order_items' )->setLabel ( 'Total Ordered Items' )
                    ->setSortable ( true ),

                (new FieldConfig ())->setName ( 'unit_session_percentage_b2b' )->setLabel ( 'B2B Unit Session' )
                    ->setSortable ( true ),

                (new FieldConfig ())->setName ( 'ordered_product_sales_b2b' )->setLabel ( 'B2B Ordered Sales' )
                    ->setSortable ( true ),

                (new FieldConfig ())->setName ( 'total_order_items_b2b' )->setLabel ( 'B2B Total Ordered Items' )
                    ->setSortable ( true ),

            ] )
            ->setComponents ( [
                (new THead ())->setComponents ( [
                    (new OneCellRow ())->setComponents ( [
                        (new Pager ()),
                        (new HtmlTag ())->setAttributes ( [
                            'class' => 'pagination summary'
                        ] )->addComponent ( new ShowingRecords () )
                    ] ),

                    (new ColumnHeadersRow ()),
                    (new FiltersRow ()),

                    (new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [

                        (new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-go"></span> Go' )->setTagName ( 'button' )->
                        // ->setRenderSection(RenderableRegistry::SECTION_END)
                        setAttributes ( [
                            'type' => 'button',
                            'class' => 'btn btn-success btn-sm  action-go',
                            'style' => "margin-right:10px"
                        ] ),

                        (new RecordsPerPage ())->setVariants ( [
                            20,
                            50,
                            100,
                            500,
                            1000
                        ] ),
                        new ColumnsHider (),

                        (new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
                            'class' => 'btn btn-success btn-sm'
                        ] )
                    ] )

                ] ),

                (new TFoot ())->setComponents ( [
                    (new OneCellRow ())->setComponents ( [
                        new Pager (),
                        (new HtmlTag ())->setAttributes ( [
                            'class' => 'pull-right'
                        ] )->addComponent ( new ShowingRecords () )
                    ] )
                ] )
            ] );

        $grid = new Grid ( $grid_config );

        return $grid;
    }

}