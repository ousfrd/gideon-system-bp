<?php

namespace App\Grids;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use DB;
use App\AdGroup;
use App\AdReport;

class AdGroupGrid extends GeneralGrid {
	static function grid($params=[]) {
		
		$dataProvider = AdGroup::with ( 'account' );


		if(isset($params['seller_id']) && $params['seller_id']) {
			$dataProvider->where('ad_groups.seller_id', $params['seller_id']);
		}

		if(isset($params['country']) && $params['country']) {	
			$dataProvider->where('ad_groups.country', $params['country']);
		}

		if(isset($params['campaignId']) && $params['campaignId']) {
			$dataProvider->where('ad_groups.campaignId', $params['campaignId']);
		}



		$gridConfig = new GridConfig ();
		$gridConfig->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'AdGroups' )
		->setPageSize ( 50 )
		->setColumns ( [
				AdGroupGrid::stateField(),
				AdGroupGrid::nameField(),
				// AdGroupGrid::impressionsField(),
				// AdGroupGrid::clicksField(),
				// AdGroupGrid::adSpendField(),
				// AdGroupGrid::salesField(),
				// AdGroupGrid::acosField(),
				// AdGroupGrid::cpcField(),
				// AdGroupGrid::ctrField(),
				// AdGroupGrid::ordersField(),
				// AdGroupGrid::crField(),
				AdGroupGrid::servingStatusField(),
				AdGroupGrid::defaultBidField(),
				AdGroupGrid::actionsField(),
		] )->setComponents ( [
				AdGroupGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] );
		
		$grid = new Grid ($gridConfig);
		
		return $grid;
	}
	
	
	static function stateField() {
		return  (new FieldConfig ())->setName ( 'state' )->setLabel ( 'State' )->setSortable ( true )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'state' )->setMultipleMode ( true )->setSubmittedOnChange ( false )
				->setOptions ( ['enabled'=>'Enabled','paused'=>'Paused','archived'=>'Archived'] )
				);
	}

	static function nameField() {
		return  (new FieldConfig ())->setName ( 'name' )->setLabel ( 'Name' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					return $val;
				} );
	}

	static function impressionsField() {
		return  (new FieldConfig ())->setName ( 'total_impressions' )->setLabel ( 'Impr.' );
	}

	static function clicksField() {
		return  (new FieldConfig ())->setName ( 'total_clicks' )->setLabel ( 'Clicks' )->setSortable ( true );
	}

	static function adSpendField() {
		return  (new FieldConfig ())->setName ( 'total_cost' )->setLabel ( 'Ad Spend (USD)' )->setSortable ( true )->setSorting(Grid::SORT_DESC);
	}

	static function salesField() {
		return  (new FieldConfig ())->setName ( 'total_sales' )->setLabel ( 'Sales (USD)' )->setSortable ( true );
	}

	static function acosField() {
		return  (new FieldConfig ())->setName ( '' )->setLabel ( 'ACoS (%)' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return (isset($row->getSrc ()->total_sales) && $row->getSrc ()->total_sales > 0) ? round(($row->getSrc ()->total_cost / $row->getSrc ()->total_sales) * 100, 2) : "0.0";
				} );
	}

	static function cpcField() {
		return  (new FieldConfig ())->setName ( '' )->setLabel ( 'CPC (USD)' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return (isset($row->getSrc ()->total_clicks) && $row->getSrc ()->total_clicks > 0) ? round($row->getSrc ()->total_cost / $row->getSrc ()->total_clicks, 2) : 0;

				} );
	}

	static function ctrField() {
		return  (new FieldConfig ())->setName ( '' )->setLabel ( 'CTR (%)' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return (isset($row->getSrc ()->total_impressions) && $row->getSrc ()->total_impressions > 0) ? round(($row->getSrc ()->total_clicks / $row->getSrc ()->total_impressions) * 100, 2) : 0;

				} );
	}

	static function ordersField() {
		return  (new FieldConfig ())->setName ( 'total_orders' )->setLabel ( 'Orders' )->setSortable ( true );
	}

	static function crField() {
		return  (new FieldConfig ())->setName ( '' )->setLabel ( 'CR (%)' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return (isset($row->getSrc ()->total_clicks) && $row->getSrc ()->total_clicks > 0) ? round(($row->getSrc ()->total_orders / $row->getSrc ()->total_clicks) * 100, 2) : 0;

				} );
	}

	static function servingStatusField() {
		return  (new FieldConfig ())->setName ( 'servingStatus' )->setLabel ( 'Serving Status' )->setSortable ( true );
	}

	static function defaultBidField() {
		return  (new FieldConfig ())->setName ( 'defaultBid' )->setLabel ( 'Default Bid (USD)' )->setSortable ( true );
	}


	static function actionsField() {
		return (new FieldConfig ())->setName ( 'id' )->setLabel ( 'Actions' )->setSortable ( true )->setCallback ( function ($val, ObjectDataRow $row) {
					return '<a  href="' . route ( "ad.adgroups.edit", ['id'=>$row->getSrc()->campaignId, "gid" => $row->getSrc ()->adGroupId ] ) . '">Edit</a> ' 
					. '<br><a  href="' . route ( "ad.adgroups.keywords", ['id'=>$row->getSrc()->campaignId, "gid" => $row->getSrc ()->adGroupId ] ) . '">Biddable Keywords</a>'
					. '<br><a  href="' . route ( "ad.adgroups.negativeKeywords", ['id'=>$row->getSrc()->campaignId, "gid" => $row->getSrc ()->adGroupId ] ) . '">Negative Keywords</a>';
				} );

	}


	static function headerComponent() {
		return (new THead ())->setComponents ( [

				(new ColumnHeadersRow ()),
				(new FiltersRow ()),
				
				(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [

						(new RecordsPerPage ())->setVariants ( [
								20,
								50,
								100,
								500,
								1000
						] ),
						new ColumnsHider (),
						(new ExcelExport ())->setFileName ( 'AdGroups-' . date ( 'Y-m-d' ) ),
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
								'class' => 'btn btn-success btn-sm'
						] )
				] ),
				
		] );
	}


}