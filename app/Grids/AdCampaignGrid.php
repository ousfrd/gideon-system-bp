<?php

namespace App\Grids;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use DB;
use App\AdCampaign;
use App\AdReport;
use App\Account;

class AdCampaignGrid extends GeneralGrid {
	static function grid($params=[]) {
		
		$dataProvider = AdCampaign::with ( [ 'account' ] );

		$accounts = Account::where('status', 'Active')->pluck('name', 'seller_id')->all();
		$gridConfig = new GridConfig ();
		$gridConfig->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'AdCampaigns' )
		->setPageSize ( 50 )
		->setColumns ( [
				AdCampaignGrid::accountField($accounts),
				AdCampaignGrid::countryField(),
				AdCampaignGrid::stateField(),
				AdCampaignGrid::statusField(),
				AdCampaignGrid::nameField(),
				AdCampaignGrid::targetTypeField(),
				// AdCampaignGrid::impressionsField(),
				// AdCampaignGrid::clicksField(),
				// AdCampaignGrid::adSpendField(),
				// AdCampaignGrid::salesField(),
				// AdCampaignGrid::acosField(),
				// AdCampaignGrid::cpcField(),
				// AdCampaignGrid::ctrField(),
				// AdCampaignGrid::ordersField(),
				// AdCampaignGrid::crField(),
				AdCampaignGrid::dailyBudgetField(),
				AdCampaignGrid::startAtField(),
				AdCampaignGrid::actionsField(),
		] )->setComponents ( [
				AdCampaignGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] );
		
		$grid = new Grid ($gridConfig);
		
		return $grid;
	}
	
	static function accountField($accounts) {
		return  (new FieldConfig ())->setName ( 'seller_id' )->setLabel ( 'Account' )->setSortable ( true )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'seller_id' )->setMultipleMode ( false )->setSubmittedOnChange ( true )
				->setOptions ( $accounts )
				)
		->setCallback ( function ($val, ObjectDataRow $row) {
					return $row->getSrc()->account->name;
				} );		
	}

	static function stateField() {
		return  (new FieldConfig ())->setName ( 'state' )->setLabel ( 'State' )->setSortable ( true )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'state' )->setMultipleMode ( true )->setSubmittedOnChange ( true )
				->setOptions ( ['enabled'=>'Enabled','paused'=>'Paused','archived'=>'Archived'] )
				);
	}

	static function statusField() {
		return  (new FieldConfig ())->setName ( 'servingStatus' )->setLabel ( 'Status' )->setSortable ( true )
		->addFilter ( (new SelectFilterConfig ())->setName ( 'servingStatus' )->setMultipleMode ( true )->setSubmittedOnChange ( true )
				->setOptions ( ['CAMPAIGN_STATUS_ENABLED'=>'Delivering','CAMPAIGN_OUT_OF_BUDGET'=>'Out of Budget','CAMPAIGN_PAUSED'=>'Paused','CAMPAIGN_ARCHIVED'=>'Archived'] )
				)->setCallback ( function ($val, ObjectDataRow $row) {
					$status = ['CAMPAIGN_STATUS_ENABLED'=>'Delivering','CAMPAIGN_OUT_OF_BUDGET'=>'Out of Budget','CAMPAIGN_PAUSED'=>'Paused','CAMPAIGN_ARCHIVED'=>'Archived'];
					return $status[$val];
				} );
	}

	static function nameField() {
		return  (new FieldConfig ())->setName ( 'name' )->setLabel ( 'Name' )->setSortable ( true )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) )
				->setCallback ( function ($val, ObjectDataRow $row) {
					return "<a href='".route('ad.campaigns.view', [ "id" => $row->getSrc()->campaignId ] )."'>".$val."</a>";
				} );
	}

	static function impressionsField() {
		return  (new FieldConfig ())->setName ( 'total_impressions' )->setLabel ( 'Impr.' );
	}

	static function clicksField() {
		return  (new FieldConfig ())->setName ( 'total_clicks' )->setLabel ( 'Clicks' )->setSortable ( true );
	}

	static function adSpendField() {
		return  (new FieldConfig ())->setName ( 'total_cost' )->setLabel ( 'Ad Spend (USD)' )->setSortable ( true )->setSorting(Grid::SORT_DESC);
	}

	static function salesField() {
		return  (new FieldConfig ())->setName ( 'total_sales' )->setLabel ( 'Sales (USD)' )->setSortable ( true );
	}

	static function acosField() {
		return  (new FieldConfig ())->setName ( '' )->setLabel ( 'ACoS (%)' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return (isset($row->getSrc ()->total_sales) && $row->getSrc ()->total_sales > 0) ? round(($row->getSrc ()->total_cost / $row->getSrc ()->total_sales) * 100, 2) : "0.0";
				} );
	}

	static function cpcField() {
		return  (new FieldConfig ())->setName ( '' )->setLabel ( 'CPC (USD)' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return (isset($row->getSrc ()->total_clicks) && $row->getSrc ()->total_clicks > 0) ? round($row->getSrc ()->total_cost / $row->getSrc ()->total_clicks, 2) : 0;

				} );
	}

	static function ctrField() {
		return  (new FieldConfig ())->setName ( '' )->setLabel ( 'CTR (%)' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return (isset($row->getSrc ()->total_impressions) && $row->getSrc ()->total_impressions > 0) ? round(($row->getSrc ()->total_clicks / $row->getSrc ()->total_impressions) * 100, 2) : 0;

				} );
	}

	static function ordersField() {
		return  (new FieldConfig ())->setName ( 'total_orders' )->setLabel ( 'Orders' )->setSortable ( true );
	}

	static function crField() {
		return  (new FieldConfig ())->setName ( '' )->setLabel ( 'CR (%)' )->setSortable ( true )
		->setCallback ( function ($val, ObjectDataRow $row) {
					return (isset($row->getSrc ()->total_clicks) && $row->getSrc ()->total_clicks > 0) ? round(($row->getSrc ()->total_orders / $row->getSrc ()->total_clicks) * 100, 2) : 0;

				} );
	}


	static function targetTypeField() {
		return  (new FieldConfig ())->setName ( 'targetingType' )->setLabel ( 'Targeting Type' )->setSortable ( true )
			->addFilter ( (new SelectFilterConfig ())->setName ( 'targetingType' )->setSubmittedOnChange ( true )
				->setOptions ( ['auto'=>'Auto','manual'=>'Manual'] )
				);
	}

	static function dailyBudgetField() {
		return  (new FieldConfig ())->setName ( 'dailyBudget' )->setLabel ( 'Daily Budget (USD)' )->setSortable ( true );
	}

	static function startAtField() {
		return  (new FieldConfig ())->setName ( 'startDate' )->setLabel ( 'Start At' )->setSortable ( true );
	}

	static function actionsField() {
		return (new FieldConfig ())->setName ( 'id' )->setLabel ( 'Actions' )->setSortable ( true )->setCallback ( function ($val, ObjectDataRow $row) {
					return '<a  href="' . route ( "ad.campaigns.edit", ["id" => $row->getSrc ()->campaignId ] ) . '">Edit</a> ' 
					. '<br><a  href="' . route ( "ad.campaigns.delete", ["id" => $row->getSrc ()->campaignId ] ) . '" onClick="return confirm(\'Are you sure to delte?\')"  >Delete</a>'
					. '<br><a  href="' . route ( "ad.campaigns.adgroups", ["id" => $row->getSrc ()->campaignId ] ) . '">Ad Groups</a>'
					. '<br><a  href="' . route ( "ad.campaigns.negativeKeywords", ["id" => $row->getSrc ()->campaignId ] ) . '">Negative Keywords</a>';
				} );
	}

	static function headerComponent() {
		return (new THead ())->setComponents ( [

				(new ColumnHeadersRow ()),
				(new FiltersRow ()),
				
				(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [

						(new RecordsPerPage ())->setVariants ( [
								20,
								50,
								100,
								500,
								1000
						] ),
						new ColumnsHider (),
						(new ExcelExport ())->setFileName ( 'AdCampaigns-' . date ( 'Y-m-d' ) ),
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
								'class' => 'btn btn-success btn-sm'
						] )
				] ),
				
		] );
	}


}