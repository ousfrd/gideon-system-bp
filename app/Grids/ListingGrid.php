<?php
namespace App\Grids;

use Illuminate\Support\Facades\Config;
use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

use App\Product;
use App\Listing;
use DB;
use App\Excessnventory;

use Illuminate\Support\Facades\Input;
use App\User;
use App\Account;

class ListingGrid extends GeneralGrid  {
	static function grid($request=null,$params=[]) {
		$dataProvider = Listing::with(['product.users', 'merchant'])
			->select('seller_listings.*')
			->addSelect('seller_accounts.seller_id as account_seller_id')
			->addSelect('seller_accounts.status as account_status')
			->join('seller_accounts', 'seller_listings.seller_id', '=', 'seller_accounts.seller_id')
			->where('seller_accounts.status', '=', 'Active')
			->where('seller_listings.status', '>=', 0);
		
		if(auth()->user()->role == "product manager assistant") {
			// $dataProvider->whereHas('product',function($q){
			// 	$q->whereRaw(DB::Raw('product_id in (select product_id from product_users where user_id ='.auth()->user()->id.')'));
			// });
			$dataProvider->whereIn('asin',auth()->user()->productList());
		}

		if(isset($params['product_id']) && !empty($params['product_id'])) {
			$dataProvider->where('product_id',$params['product_id']);
		}

		// $dataProvider->where('status', 1);
		
		if($request != null) {
			
			if($request->get('low') == 1 || (isset($params['low']) && $params['low'] == 1)) {

				$dataProvider->whereHas('inventory',function($q){
					$q->whereRaw(DB::Raw('total_qty/daily_ave_orders - (manufacture_days+cn_to_amz_shipping_days+5) <= 7'));
				})
				->where('discontinued',0)
				->where('fulfillment_channel','Amazon')
				;
			} elseif($request->get('lowsales') == 1  || (isset($params['lowsales']) && $params['lowsales'] == 1)) {
				$dataProvider->where('discontinued',0)
				->where('daily_ave_orders','<',5)
				->where('open_date','<',date('Y-m-d',strtotime('-30 days')))
				->whereRaw(DB::raw('daily_ave_orders*price_usd < 200'))
				->where('fulfillment_channel','Amazon')
				->where('total_qty','>',0)
				->whereHas('inventory',function($q){
					$q->whereRaw(DB::Raw('total_qty/daily_ave_orders - (manufacture_days+cn_to_amz_shipping_days+5) >= 60'))
					->orWhere('daily_ave_orders',0);
				});
			}
		}
		$accounts = Account::getSortedAccounts()->pluck('name', 'seller_id')->all();
		// $accounts = Account::pluck('name', 'seller_id')->all();

		$gridConfig = new GridConfig ();
		
		$gridConfig
		->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
		->setName ( 'Listing' )->setPageSize ( 25 )
		->setColumns ( [
				ListingGrid::imageField(),
				ListingGrid::sellerIdField($accounts),
				ListingGrid::openDateField(),
				ListingGrid::managersField(),
				ListingGrid::skuField(),
				ListingGrid::insertCardField(),
				ListingGrid::insertCardCostField(),
				ListingGrid::statusField(),
				ListingGrid::aveOrdersField(),
				ListingGrid::totalQtyField(),
				
				ListingGrid::replenishmentField(),
				ListingGrid::fulfillmentField(),
				GeneralGrid::countryField(),
				ListingGrid::priceField(),
				ListingGrid::actionsField()
		] )
		->setComponents ( [
				ListingGrid::headerComponent(),
				GeneralGrid::footerComponenet()
		] ) ;
		
		
		$grid = new Grid ($gridConfig);
		
		return $grid;

	}


	static function imageField() {
		return (new FieldConfig ())->setName ( 'item_name' )->setLabel ( 'Listing' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) )
		->setCallback ( function ($val, ObjectDataRow $row) {
			$listing =  $row->getSrc();
			$imageLink = $listing->product->small_image;
			if (empty($imageLink)) {
				$imageLink = "/images/No_Image_Available.jpg";
			}
			return '<a  href="' . Product::amzLink($listing->asin, $listing->country) . '" target="_blank" data-placement="right" data-toggle="popover"  data-content="'.$listing->product->name.'"><img src="' . $imageLink. '" style="max-width:100px;max-height:100px"/></a>';
		} );
	}

	static function nameField() {
		return (new FieldConfig ())->setName ( 'item_name' )->setLabel ( 'Listing' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_LIKE ) )
		->setCallback ( function ($val, ObjectDataRow $row) {
			$listing =  $row->getSrc();
			return '<a  href="' . route ( "product.view", [ "id" => $listing ->product->id ] )  . '" target="_blank" data-placement="right" data-toggle="popover">'.$listing->product->name.'</a>';
		} );
	}


	static function sellerIdField($accounts) {
		return (new FieldConfig ())->setName ( 'seller_id' )
				->setLabel('Merchant')
				->addFilter(
					(new SelectFilterConfig())->setFilteringFunc(function($sellerId, EloquentDataProvider $provider) {
						$cond = [['seller_listings.seller_id', '=', $sellerId]];
						$provider->getBuilder()->where($cond);
					})
					->setName('seller_id')
					->setSubmittedOnChange(TRUE)
					->setOptions($accounts)
				)
				->setCallback(function ($val, ObjectDataRow $row) {
					$merchant = $row->getSrc()->merchant;
					return '<a  href="' . route ( "dashboard", [ "account" => $row->getSrc ()->seller_id ] ) . '">' . $merchant->code . ' ' . $merchant->name . '</a>';
				})->setSortable ( true );
	}

	static function openDateField() {
		return (new FieldConfig())
						->setName('open_date')
						->setLabel('Open Date')
						->setSortable(true);
	}


	static function managersField() {
		return (new FieldConfig ())->setName ( 'managers' )->setLabel ( 'Manager' )
				->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
					$user = User::findByName($value);
					
					return $dp->getBuilder()->whereIn('asin',$user->productList());
				}))
				->setCallback ( function ($val, ObjectDataRow $row) {
					//
					$users = $row->getSrc()->product->users;
					$names = [];
					foreach ($users as $user) {
						$names[] = $user->name;
					}
					if(Auth::user()->can('manage-product-manager')) {
						return '<a href="#" data-type="select" class="pmanagers"  data-pk="manager-'.$row->getSrc()->product->id.'" data-title="">'.implode(', ', $names).'</a>';
					}else{
						return implode(', ', $names);
					}
				} )->setSortable ( true );
	}


	static function skuField() {
		return (new FieldConfig ())->setName ( 'sku' )->setLabel ( 'SKU/ASIN' )
				->addFilter ( (new FilterConfig ())->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
					return $dp->getBuilder()->where('sku',$value)->orWhere('asin',$value);
				}))
				->setCallback ( function ($val, ObjectDataRow $row) {
					return 
					'<b>'. $row->getSrc()->country . '</b>' .
					'<br/>SKU: <a  href="' . route ( "listing.view", [ "id" => $row->getSrc ()->id ] ) . '">' . $row->getSrc ()->sku . '</a>'.
					'<br/>ASIN: <a  href="' . route ( "product.view", [ "id" => $row->getSrc ()->product->id ] ) . '">' . $row->getSrc ()->asin . '</a>';
				} )->setSortable ( true );
	}

	static function insertCardField() {
		return (new FieldConfig ())
				->setName ( 'insert_card' )
				->setLabel ( 'Insert Card' )
				->addFilter ( (new SelectFilterConfig ())
				->setSubmittedOnChange ( true )
				->setOptions ( [ 
						"1" => "Yes",
						"0" => "No"
					] ))
				->setCallback ( function ($val, ObjectDataRow $row) {
					return $val == 1 ? "Yes" : "No";
				})
				->setCallback(function ($val, ObjectDataRow $row) {
					if (Gate::allows('manage-insertcard')) {
						return '<input value="0" data-size="small" type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" ' . 
										' data-pk="' . $row->getCellValue('sku') . '"' . 
										' data-url="'.route('listing.updateInsertCardStatus').'" '.($row->getCellValue('insert_card') ? 'checked' : '') .
										' id="insert_card_status"' . ' />';
				} else {
					return $row->getCellValue('insert_card') ? 'Active' : 'Inactive';
				}
			})
			->setSortable ( true )
				;
	}

	static function insertCardCostField() {
		return  (new FieldConfig ())->setName ('insert_card_review_cost')->setLabel ('Insert Card Cost')->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->setSortable ( true )
			->setCallback(function ($val, ObjectDataRow $row) {
					$id = $row->getCellValue('id');
					return '<a data-type="text" class="editable" ' . 
							' data-pk="' . $id . '"' .
							' data-url="' . route('listing.update-field', ['id'=> $id]) . '"' . 
							// ' data-value="' . $val .'"' . 
							' id="insert_card_review_cost" data-title="insert Card Cost" >' . $val. '</a>';
				
			});
	}


	static function statusField() {
		return (new FieldConfig ())->setName ( 'status' )->setLabel ( 'Status' )->setSortable ( true )
						->addFilter ( (new SelectFilterConfig ())->setName ( 'status' )->setSubmittedOnChange ( true )
								->setOptions ( ['Inactive','Active','Discontinued'] )
								->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
									
									if($value == 0) {
										$dp->getBuilder()->whereHas('product',function($q) use($value){
											return $q->where('status',0);
										})->orWhere('seller_listings.status',0);
									}elseif ($value == 1) {
										$dp->getBuilder()->whereHas('product',function($q) use($value){
											return $q->where('status',1);
										})->where('seller_listings.status',1)
										->where('discontinued',0);
									}else{
										$dp->getBuilder()->where('discontinued',1);
									}
									
								})
								
								)
								->setCallback ( function ($val, ObjectDataRow $row) {
									$listing =  $row->getSrc();
									if($listing->product->status <=0) {
										return 'Product Inactive';
									}else{
										if($listing->discontinued== 1) {
											return 'Discontinued'.'<br/> <a class="pUpdate"  data-type="select" data-source=\'[{value: 0, text: "Yes"}, {value: 1, text: "No"}]\' data-pk="discontinued-'.$listing->id.'" data-url="'.route('listing.ajaxsave').'" >Set as Active</a>';
										}
										
										if($listing->fulfillment_channel == 'Amazon') {
											if ($listing->afn_fulfillable_quantity == 0) {
												return  'Out of Stock'.'<br/> <a class="pUpdate"  data-type="select" data-source=\'[{value: 1, text: "Yes"}, {value: 0, text: "No"}]\' data-pk="discontinued-'.$listing->id.'" data-url="'.route('listing.ajaxsave').'" >Set as Discontinued</a>';
											}
											
											if($listing->safeStockQty() > $listing->total_qty) {
												return 'Low Stock';
											}
											return 'Active';
										} else {
											if( $listing->qty <=0) {
												return  'Out of Stock';
											}
											
											return 'Active';
										}
									}
								} );

	}	


	static function aveOrdersField() {
		return (new FieldConfig ())->setName ( 'daily_ave_orders' )->setLabel ( 'AVG Orders Per Day' )
				->setSortable ( true )
				->setSorting(Grid::SORT_DESC)
				->setCallback(function($val,ObjectDataRow $row){
					$listing = $row->getSrc();
					$orders = [
							'Today So Far: '.$listing->today_so_far_orders,
							'Last 7 Days: '.$listing->last_7_days_orders,
							'Last 14 Days: '.$listing->last_14_days_orders,
							'Last 21 Days: '.$listing->last_21_days_orders,
							'Last 30 Days: '.$listing->last_30_days_orders,
							'This Month So Far: '.$listing->this_month_orders,
							
					];
					
					$content =  implode('<br/>', $orders);
					return '<a data-toggle="popover" title="Order Overview" data-content="'.$content.'">'.round($listing->dailyAverageOrders()).' </a>';
					
				});
	}


	static function totalQtyField() {
		return (new FieldConfig)
						->setName('qty')
						->setLabel('Qty')
						->setSortable(true)
						->addFilter ( (new SelectFilterConfig ())
								->setName ( 'qty' )->setSubmittedOnChange ( true )
						->setOptions ( [1=>'In Stock',0=>'Out of Stock'] )
						->setFilteringFunc(function ($value, EloquentDataProvider $dp) {
							if($value == 0) {
								$dp->getBuilder()->where(function($q){
									$q->where(function($q){
										$q->where('qty','=',0)->where('fulfillment_channel','Merchant');
									})->orWhere(function($q){
										$q->where('fulfillment_channel','Amazon')->whereHas('inventory',function($q){
																				$q->where('fulfillable_quantity',0);
																			});;
									});
								});
// 								$dp->getBuilder()
							}else{
								$dp->getBuilder()->where(function($q){
									$q->where(function($q){
										$q->where('qty','>',0)->where('fulfillment_channel','Merchant');
									})->orWhere(function($q){
										$q->where('fulfillment_channel','Amazon')->whereHas('inventory',function($q){
											$q->where('fulfillable_quantity','>',0);
										});;;
									});
								});
// 								$dp->getBuilder()->whereHas('inventory',function($q){
// 									$q->where('fulfillable_quantity','>',0);
// 								});
							}
// 							$dp->getBuilder()->where(DB::raw('date(PurchaseDate)'), '>=', $value);
						})
						)		
						->setCallback(function($val,ObjectDataRow $row){
							$listing = $row->getSrc();
								
							$qty_cols = [
									
									'afn_fulfillable_quantity'=>'Fulfillable Qty',
									'afn_unsellable_quantity'=>'Unsellable Qty',
									'afn_reserved_quantity'=>'Reserved Qty',
									'afn_warehouse_quantity'=>'Warehouse Total Qty',
									'afn_inbound_working_quantity'=>'Inbound Working Qty',
									'afn_inbound_shipped_quantity'=>'Inbound Shipped Qty',
									'afn_inbound_receiving_quantity'=>'Inbound Receiving Qty',
									'afn_researching_quantity' => 'Researching Qty',
									'afn_total_quantity' => 'Total Qty'
							];
							$qtys = [];
							foreach ($qty_cols as $k=>$l) {
								$qtys[] = $l.': '.$listing->$k;
							}
							$qtys[] = '<br/>Safe Stock: '.$listing->safeStockQty();
							
							$qtys[] = '<br/>Out of Stock in '. $listing->daysToOutofStock(). ' days';
							$qtys[] = 'Replenishment Cycle: '.$listing->totalDayToReinstock(). ' days';
							
							$content = implode('<br/>', $qtys);
							
							return '<a data-toggle="popover" title="Inventory Overview" data-content="'.$content.'">'.round($listing->afn_fulfillable_quantity).'</a>';		
							
						});
	}


	static function replenishmentField() {
		return	(new FieldConfig ())->setName ( 'last_30_days_ord' )->setLabel ( 'Replenishment Date' )
								
							->setCallback(function($val,ObjectDataRow $row){
									$listing = $row->getSrc();
									$lines = [
											'Daily Average Orders: '.$listing->dailyAverageOrders(),
											'<br/>Total Qty: '.$listing->totalQty(),
											'Out of Stock in '. $listing->daysToOutofStock(). ' days',
											'Replenishment Cycle: '.$listing->totalDayToReinstock(). ' days',
											'<br/>Replenishment Needed: '.($listing->replenishNeeded() ? '<span class=\'text-danger alert-qty\'>Yes</span>':'<span  class=\'text-success\'>No</span>'),
											'Recommended Replenishment Date: '. $listing->recommendedReplenishmentDate(),
											
									];
									
									
									if($listing->safeStockQty() > $listing->totalQty()) {
										$lines[] = 'Recommended Replenishment Qty: '. $listing->recommendedReplenishmentQty();
										$lines[] = '<br/><a href=\''.route("inbound_shipment.create_plan",[$listing->id]).'\'>Create Shipment Plan</a>';
									}
									
									$content = implode('<br/>', $lines);
									
									$text = '<a data-toggle="popover" title="Replenishment Recommendation" data-content="'.$content.'">'. $listing->recommendedReplenishmentDate().'</a>';
									if($listing->safeStockQty() > $listing->total_qty) {
										$text .= '<br/><br/><a class="alert-qty" href=\''.route("inbound_shipment.create_plan",[$listing->id]).'\'>Create Plan</a>';
									}
									return $text;
// 									非高峰期备货采购量＝日均销量＊备货周期-已有库存（程序读取amazon总库存+采购录入已经在生产和在运输途中的库存）
// 									高峰期备货采购量=日均销量＊备货周期*3（采购可以手动录入数字）
									
								});

	}


	static function fulfillmentField() {
		return (new FieldConfig ())->setName ( 'fulfillment_channel' )->setLabel ( 'Fulfillment Channel' )
										->addFilter ( (new SelectFilterConfig ())->setName ( 'fulfillment_channel' )->setSubmittedOnChange ( true )-> // on change submit request
												setOptions ( ['Amazon'=>'Amazon','Merchant'=>'Merchant'] ) );
												


	}
	
	static function priceField() {
		return (new FieldConfig)
				->setName('sale_price')
				->setLabel('Price')
				->setSortable(true)
				
				->setCallback(function($val,ObjectDataRow $row){
					$listing = $row->getSrc();
					if($listing->sale_price > 0) {
						return \CurrencyHelper::format($listing->sale_price, $listing->currency) . " (" . \CurrencyHelper::format($listing->price, $listing->currency) . ")";
					} else {
						return \CurrencyHelper::format($listing->price, $listing->currency);
					}
					
				});
	}

	static function aiAdsOpenField() {
		return (new FieldConfig)
						->setName('ai_ads_open')
						->setLabel('AI Ads Open?')
						->setCallback(function ($val, ObjectDataRow $row) {
							if (Gate::allows('manage-ads')) {
								$id = $row->getCellValue('id');
								return '<input value="1" data-size="small" type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" ' . 
																			' data-pk="' . $id . '"' . 
																			' data-url="'.route('listing.update-field', ['id'=> $id]).'" '. ($val ? 'checked' : '') .
																			' id="ai_ads_open"' .' />';
							} else {
								return $val ? 'Yes' : 'No';
							}
							
						});
	}

	static function aiAdsCommisionRateField() {
		return (new FieldConfig)
						->setName('ai_ads_commission_rate')
						->setLabel('AI Ads Commission (%)')
						->setCallback(function ($val, ObjectDataRow $row) {
							if (Gate::allows('manage-ads')) {
								$id = $row->getCellValue('id');
								return '<a data-type="text" class="pUpdate" ' . 
                  ' data-pk="' . $id . '"' .
                  ' data-url="' . route('listing.update-field', ['id'=> $id]) . '"' . 
                  ' data-value="' . $val .'" id="ai_ads_commission_rate" data-title="AI Ads Commission (%)" >' . $val. '</a>';
							} else {
								return $val . "%";
							}
						});
	}

	static function actionsField() {
		return (new FieldConfig ())->setName ( 'id' )->setLabel ( 'Actions' )->setSortable ( true )->setCallback ( function ($val, ObjectDataRow $row) {
					$listing = $row->getSrc ();
					return '<a  href="' . route ( "listing.view", ["id" => $listing->id ] ) . '">View Dashboard</a> ';
				} );

	}

	static function headerComponent() {
		return (new THead ())->setComponents ( [

				(new ColumnHeadersRow ()),
				(new FiltersRow ()),
				
				(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [

						(new RecordsPerPage ())->setVariants ( [
								20,
								50,
								100,
								500,
								1000
						] ),
						new ColumnsHider (),
						(new ExcelExport ())->setFileName ( 'Listings-' . date ( 'Y-m-d' ) ),
						(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
								'class' => 'btn btn-success btn-sm'
						] )
				] ),
				
		] );
	}
	
	static function excessInventoryGrid() {
		$dataProvider = Excessnventory::with ( [ 'listing','merchant' ] );
		
		if(auth()->user()->role == "product manager assistant") {
			$dataProvider->whereIn('asin',auth()->user()->productList());
		}elseif(auth()->user()->role == "seller account manager") {
			$dataProvider->whereIn('seller_id',auth()->user()->accountList());
		}
		
		
		$grid = new Grid ( (new GridConfig ())->setDataProvider ( new EloquentDataProvider ( $dataProvider ) )
				->setName ( 'Listing' )->setPageSize ( 15 )->setColumns ( [

					(new FieldConfig ())->setName ( 'id' )->setLabel ( '' )->
								setCallback ( function ($val, ObjectDataRow $row) {
									$listing =  $row->getSrc()->listing;
									if (empty ( $listing->product->small_image)) {
										return '<a  href="' . Product::amzLink($row->getSrc()->asin, $row->getSrc()->country) . '" target="_blank">N/A</a>';
									}
									
									return '<a  href="' . $listing->product->amazonLink () . '" target="_blank" data-placement="right" data-toggle="popover"  data-content="'.$listing->product->name.'"><img src="' . $listing->product->small_image. '" style="max-width:100px;max-height:100px"/></a>';
								} ),
								
								
								
						(new FieldConfig ())->setName ( 'seller_id' )->setLabel ( 'Merchant' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->setCallback ( function ($val, ObjectDataRow $row) {
							return '<a  href="' . route ( "dashboard",['account'=>$row->getSrc ()->seller_id] )  .'">' . $row->getSrc()->merchant->name . '</a>';
						} )->setSortable ( true ),
						
						(new FieldConfig ())->setName ( 'date' )->setLabel ( 'Report Date' )
						->addFilter ( (new FilterConfig ())->setTemplate ( "grid-components.date" )->setOperator ( FilterConfig::OPERATOR_EQ ) )
						->setSortable ( true )
						->setSorting(Grid::SORT_DESC),
						
						
						(new FieldConfig ())->setName ( 'sku' )->setLabel ( 'SKU' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->setCallback ( function ($val, ObjectDataRow $row) {
							return '<a  href="' . route ( "listing.view", [ "id" => $row->getSrc ()->listing_id ] ) . '">' . $row->getSrc ()->sku . '</a>';
						} )->setSortable ( true ),
						
						(new FieldConfig ())->setName ( 'asin' )->setLabel ( 'ASIN' )
						->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )
						->setCallback ( function ($val, ObjectDataRow $row) {
							return '<a target="_blank" href="' .Product::amzLink($row->getSrc()->asin, $row->getSrc()->country). '">' . $row->getSrc ()->asin . '</a>';
						} )
						->setSortable ( true ),
						
						(new FieldConfig ())->setName ( 'country' )->setLabel ( 'Country' )->addFilter ( (new SelectFilterConfig ())->setName ( 'country' )->setMultipleMode ( true )->setSubmittedOnChange ( true )-> // on change submit request
								setOptions ( config ( 'app.countries' ) ) )
								,
								
								
								
								(new FieldConfig ())->setName ( 'estimated_excess' )->setLabel ( 'Estimated Excess' )->setSorting(Grid::SORT_DESC)->setSortable ( true ),
								
								(new FieldConfig ())->setName ( 'days_of_supply' )->setLabel ( 'Days of Supply' )->setSortable ( true ),
								
								//(new FieldConfig ())->setName ( 'excess_threshold' )->setLabel ( 'Excess Threshold' )->setSortable ( true ),
						(new FieldConfig ())->setName ( 'estimated_total_storage_cost' )->setLabel ( 'Estimated total storage cost' )->setSortable ( true ),
						
						(new FieldConfig ())->setName ( 'recommended_removal_quantity' )->setLabel ( 'Recommended Removal Quantity' )->setSortable ( true ),
						
						(new FieldConfig ())->setName ( 'estimated_cost_savings_of_removal' )->setLabel ( 'Estimated cost savings of removal' )->setSortable ( true ),
						
								
								
								(new FieldConfig ())->setName ( 'alert' )->setLabel ( 'Alert' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->setSortable ( true ),
								
								(new FieldConfig ())->setName ( 'recommended_action' )->setLabel ( 'Recommended action' )->addFilter ( (new FilterConfig ())->setOperator ( FilterConfig::OPERATOR_EQ ) )->setSortable ( true ),
								
								(new FieldConfig ())->setName ( 'healthy_inventory_level' )->setLabel ( 'Healthy Inventory Level' )->setSortable ( true ),
								
								(new FieldConfig ())->setName ( 'your_price' )->setLabel ( 'Price' )->setSortable ( true ),
								
								(new FieldConfig ())->setName ( 'recommended_sales_price' )->setLabel ( 'Recommended sales price' )->setSortable ( true ),
								(new FieldConfig ())->setName ( 'recommended_sale_duration' )->setLabel ( 'Recommended sale duration' )->setSortable ( true ),
								(new FieldConfig ())->setName ( 'qty' )->setLabel ( 'Total Qty' )->setSortable ( true ),
								(new FieldConfig ())->setName ( 'unit_sold_7' )->setLabel ( 'Units Sold' )
								->setSortable ( true )
								
								->setCallback(function($val,ObjectDataRow $row){
									$listing = $row->getSrc();
									$orders = [
											
											'Last 7 Days: '.$listing->unit_sold_7,
											'Last 30 Days: '.$listing->unit_sold_30,
											'Last 60 Days: '.$listing->unit_sold_60,
											'Last 90 Days: '.$listing->unit_sold_90,
											
									];
									
									return implode('<br/>', $orders);
								})
								,
								
								] )->
								setComponents ( [
										(new THead ())->setComponents ( [
												
												(new OneCellRow ())->setComponents ( [
														(new Pager ()),
														(new HtmlTag ())->setAttributes ( [
																'class' => 'pagination summary'
														] )->addComponent ( new ShowingRecords () )
												] ),
												
												(new ColumnHeadersRow ()),
												(new FiltersRow ()),
												
												(new OneCellRow ())->setRenderSection ( RenderableRegistry::SECTION_END )->setComponents ( [
														
														// (new HtmlTag)
														// ->setContent('<option value="">Actions</option><option value="auto-reply">Auto Reply</option>')
														// ->setTagName('select')
														// ->setRenderSection(RenderableRegistry::SECTION_BEGIN)
														// ->setAttributes([
														// 'name'=>'action',
																// 'class' => 'input-sm batch-action',
																// 'style'=>"margin-right:5px"
																// ]),
																
																// 								(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-go"></span> Go' )->setTagName ( 'button' )->
												// 								// ->setRenderSection(RenderableRegistry::SECTION_END)
												// 								setAttributes ( [
														// 										'type' => 'button',
												// 										'class' => 'btn btn-success btn-sm  action-go',
												// 										'style' => "margin-right:10px"
												// 								] ),
														
														(new RecordsPerPage ())->setVariants ( [
																20,
																50,
																100,
																500,
																1000
														] ),
														new ColumnsHider (),
														(new ExcelExport ())->setFileName ( 'ExcessInventory-Listings-' . date ( 'Y-m-d' ) ),
														// new ExcelExport(),
														(new HtmlTag ())->setContent ( '<span class="glyphicon glyphicon-refresh"></span> Filter' )->setTagName ( 'button' )->setRenderSection ( RenderableRegistry::SECTION_END )->setAttributes ( [
																'class' => 'btn btn-success btn-sm'
														] )
												] )
												
										] ),
										
										(new TFoot ())->setComponents ( [
												// (new TotalsRow(['posts_count', 'comments_count'])),
												// (new TotalsRow(['posts_count', 'comments_count']))
												// ->setFieldOperations([
												// 'posts_count' => TotalsRow::OPERATION_AVG,
												// 'comments_count' => TotalsRow::OPERATION_AVG,
												// ])
												// ,
										(new OneCellRow ())->setComponents ( [
												new Pager (),
												(new HtmlTag ())->setAttributes ( [
														'class' => 'pull-right'
												] )->addComponent ( new ShowingRecords () )
										] )
								] )
								
								] ) )
								;
								
								return $grid;
	}
}

