<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App\Grids;

use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use App\Grids\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use App\Grids\CollectionDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\SelectFilterConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\ObjectDataRow;

use DB;
use App\Disburse;
use App\Account;

class DisburseGrid extends GeneralGrid {
	public static function grid($disburseStatistics) {
		$gridConfig = new GridConfig();
		$gridConfig->setDataProvider(new CollectionDataProvider($disburseStatistics))
			->setName('Disburses')
			->setPageSize($disburseStatistics->count())
			->setColumns([
				self::sellerNameField(),
				self::sellerIdField(),
				self::marketplaceField(),
				self::amountField(),
				self::totalBalanceField(),
				self::unavailableBalanceField(),
				self::instantTransferBalanceField(),
				self::requestTransferAvailableField(),
			])->setComponents([
				self::headerComponent(),
				self::footerComponent()
			]);

		$grid = new Grid($gridConfig);

		return $grid;
	}

	public static function sellerIdField() {
		return (new FieldConfig())
			->setName('sellerId')
			->setLabel('SellerID');
	}

	public static function sellerNameField() {
		return (new FieldConfig ())
			->setName('name')
			->setLabel('Merchant')
			->setCallback(function ($val, ObjectDataRow $row) {
				$disburse = $row->getSrc();
				return sprintf('%s %s', $disburse->code, $disburse->name);
			});
	}

	static function marketplaceField() {
		return (new FieldConfig())
			->setName('marketplace')
			->setLabel('Marketplace');
	}

	public static function amountField() {
		return (new FieldConfig())
			->setName('amount')
			->setLabel('Amount')
			->setSortable(TRUE);
	}

	public static function totalBalanceField() {
		return (new FieldConfig())
			->setName('totalBalance')
			->setLabel('Total Balance')
			->setSortable(TRUE);
	}

	public static function unavailableBalanceField() {
		return (new FieldConfig())
			->setName('unavailableBalance')
			->setLabel('Unavailable Balance')
			->setSortable(TRUE);
	}

	public static function instantTransferBalanceField() {
		return (new FieldConfig())
			->setName('instantTransferBalance')
			->setLabel('Instant Transfer Balance')
			->setSortable(TRUE);
	}

	public static function requestTransferAvailableField() {
		return (new FieldConfig())
			->setName('requestTransferAvailable')
			->setLabel('Request Transfer Available')
			->setSortable(TRUE)
			->setCallback(function ($val, ObjectDataRow $row) {
				$disburse = $row->getSrc();
				return $disburse->requestTransferAvailable ? 'YES' : 'NO';
			});;
	}

	public static function headerComponent() {
		return (new THead())->setComponents([
				(new ColumnHeadersRow()),
				(new FiltersRow()),
				(new OneCellRow())->setRenderSection(RenderableRegistry::SECTION_END)
					->setComponents([
						(new RecordsPerPage())->setVariants([20, 50, 100, 500, 1000]),
						new ColumnsHider(),
						(new CsvExport())->setFileName('Disburses-' . date('Y-m-d')),
						(new HtmlTag())->setContent('<span class="glyphicon glyphicon-refresh"></span>Filter')
							->setTagName('button')
							->setRenderSection(RenderableRegistry::SECTION_END)
							->setAttributes(['class' => 'btn btn-success btn-sm'])
					]),
			]);
	}

	public static function footerComponent() {
		return (new TFoot())
			->setComponents([
				new TotalsRow([
                    'amount',
                    'totalBalance',
                    'unavailableBalance',
                    'instantTransferBalance',
                ]),
				(new OneCellRow())->setComponents([
					new Pager(),
					(new HtmlTag())->setAttributes([
						'class' => 'pull-right'
					])->addComponent(new ShowingRecords())
				])
			]);
	}
}