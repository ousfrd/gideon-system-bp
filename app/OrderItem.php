<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Console\Command;
use Carbon\Carbon;

class OrderItem extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'order_items';
	
	public $timestamps = true;

	protected $fillable = [
		'seller_id',
		'AmazonOrderId',
		'SellerSKU',
		'OrderItemId',
		'ASIN',
		'Title',
		'QuantityOrdered',
		'QuantityShipped',
		'ItemPrice',
		'ShippingPrice',
		'tax',
		'country',
		'region',
		'Currency',
		'exchange_rate',
		'commission',
		'fba_fee',
		'ItemPriceUSD',
		'ShippingPriceUSD',
		'vat_fee',
		'giftwrap_fee',
		'item_promo',
		'shipping_promo',
		'total_promo',
		'cost',
		'shipping_cost'
	];
	
	public function product() {
		return $this->belongsTo('App\Product','ASIN','asin');
	}

	public function listing() {
		return $this->belongsTo('App\Listing','SellerSKU','sku');
	}
	
	public function order() {
		return $this->belongsTo('App\Order','AmazonOrderId','AmazonOrderId');
	}
	
	public function shipment() {
		return $this->hasOne('App\Shipment','amazon_order_item_id','OrderItemId');
	}
	
	public function transactions($type=null) {
		$q = $this->hasOne('App\Transaction','order_item_code','OrderItemId');
		
		if($type != null) {
			$q->where('transaction_type',$type); 
		}
		
		return $q->get();
	}
	
	static function aggregateFBAFee(Command $command = null){
		$orderItems = self::whereHas('order',function($q){
			$q->where('FulfillmentChannel','AFN')
			->whereIn('OrderStatus',['Shipped','Unshipped']);
		})->where('fba_fee',0)->get();
		
		echo self::whereHas('order',function($q){
			$q->where('FulfillmentChannel','AFN')
			->whereIn('OrderStatus',['Shipped','Unshipped']);
		})->where('fba_fee',0)->toSql();
		
		foreach ($orderItems as $orderItem) {
			try{
				var_dump($orderItem->ASIN,$orderItem->country,$orderItem->AmazonOrderId);
				$orderItem->fba_fee = $orderItem->fbaFee();
				$orderItem->save();
				
				if($command!=null){
					$command->info($orderItem->AmazonOrderId.' '.$orderItem->fba_fee);
				}
			}catch(\Exception $e) {
				$command->info($orderItem->AmazonOrderId.' '.$e->getMessage());
			}
		}
		
	}
	
	static function countItems($start_date,$end_date,$params=[],$selecct_cat="promo") {
			
		$query = self::join('orders','orders.AmazonOrderId','order_items.AmazonOrderId');
			
		$query->whereBetween('PurchaseDate',[date('Y-m-d 00:00:00',strtotime($start_date)),date('Y-m-d 23:59:59',strtotime($end_date))]);
			
		$query->whereIn('OrderStatus',['Shipped','Unshipped','Pending']);
			
		if(isset($params['sku']) && !empty($params['sku'])) {
			
			$query->where('SellerSKU',$params['sku']);
			
		}
		
		elseif(isset($params['asin']) && !empty($params['asin'])) {
			
			$query->where('asin',$params['asin']);
			
			
		}
		
		if(isset($params['country']) && !empty($params['country']) ) {
			$query->where('country',$params['country']);
		}
		
		
		if( isset($params['products']) && ( (isset($params['user_id']) &&!empty($params['user_id'])) || !empty($params['products'])  ) ) {
			
			$query->whereIn('asin',$params['products']);
			
		}
		
		if ($selecct_cat== 'promo') {
				$query->where(function($q){
					$q->where('shipping_promo','>',0)
					->orWhere('item_promo','>',0);
				});
			}
			
			return $query->count();
			
	}
	
	
	public function totalPaid() {
		$total = $this->ItemPrice + $this->ShippingPrice;
		
		//if($this->shipping_promo) {
		$total -= $this->shipping_promo+$this->item_promo;
		//}
		
		return round($total,2);
	}

	public function totalPaidIncludingTax() {
		return $this->totalPaid() + $this->tax;
	}
	
	function shippingChargeBack() {
		return $this->chargeback;
// 		if($this->order->FulfillmentChannel == 'AFN') {
// 			$total = $this->ShippingPrice;
			
// 			if( $this->shipping_promo) {
// 				$total -=  $this->shipping_promo;
// 			}
			
// 			return $total;
// 		}
		
		return 0;
	}
	public function totalMerchantGross() {
		$total =  $this->totalPaid() - $this->shippingChargeBack();
		
		return $total;
	}
	
	public function amzCom() {
		if($this->commission) {
			return $this->commission;
		}
		return round($this->totalMerchantGross()*0.15,2);
	}

	
	public function fbaFee() {
		
		if($this->order->FulfillmentChannel == 'AFN') {
			
			if($this->fba_fee) {
				return $this->fba_fee;
			}
			switch ($this->country) {
				case 'US':
					return $this->fbaFeeUS();
				default:
					
					$fee = $this->product->fbaFee(date('n',strtotime($this->order->PurchaseDate)));
					$fee = $fee * $this->QuantityOrdered;
					return round($fee,2);
			}
			
			
			
		}
		
		return 0;
	}
	
	
	public function fbaFeeUS(){
		
		$weight = ceil($this->product->weight);
		
		if($this->order->PurchaseDate < '2017-02-22') {
			
			if($weight < 1) {
				$weightFee = 0.96;
			}elseif($weight < 2) {
				$weightFee = 1.96;
			} else {
				$weightFee = 1.95 + ($weight-2)*0.39;
			}
			
			$fee =  ($this->product->handling + $this->product->pickpack + $weightFee)* $this->QuantityOrdered;
		} else {
			
			$fee = $this->product->fbaFee(date('n',strtotime($this->order->PurchaseDate)));
			$fee = $fee * $this->QuantityOrdered;
		}
		
		return round($fee,2);
	}
	
// 	public function fbaFeeCA(){
// 		$weight = ceil($this->product->weight);
		
// 		if($this->order->PurchaseDate < '2017-02-22') {
			
// 			if($weight < 1) {
// 				$weightFee = 0.96;
// 			}elseif($weight < 2) {
// 				$weightFee = 1.96;
// 			} else {
// 				$weightFee = 1.95 + ($weight-2)*0.39;
// 			}
			
// 			$fee =  ($this->product->handling + $this->product->pickpack + $weightFee)* $this->QuantityOrdered;
// 		} else {
			
// 			$fee = $this->product->fbaFee(date('n',strtotime($this->order->PurchaseDate)));
// 			$fee = $fee * $this->QuantityOrdered;
// 		}
		
// 		return $fee;
// 	}
	
	public function amzTotalFee() {
		
		$fee = $this->amzCom() + $this->fbaFee() + $this->shippingChargeBack();
		
// 		if($this->order->FulfillmentChannel == 'AFN') {
// 			$fee += $this->ShippingPrice;
// 		}
		
		return round($fee,2);
	}
	
	public function totalPayout() {
		$total =  $this->totalPaid() + $this->amzTotalFee() + $this->total_refund;
		
		// if($refunds = $this->transactions('refund')) {
		// 	foreach($refunds as $refund) {
		// 		$total += $refund->amount;
		// 	}
		// }
			
		return $total;
	}
	
	public function totalCost() {
		$pcost = $this->product->cost * $this->QuantityOrdered;
		
		return $pcost;
	}
	
	public function netProfit() {
		return $this->totalPayout() - $this->totalCost();
	}
	
	public function itemROI(){
		
	}

	public static function displayMarketingOrdersQuery() {
		$query = OrderItem::select(
										"order_items.id as order_item_id",
										"order_items.AmazonOrderId",
										"order_items.SellerSKU as sku",
										"order_items.ASIN as asin",
										"order_items.Title as title",
										"order_item_marketings.invite_review",
										"order_item_marketings.self_review",
										"order_item_marketings.email_promotion_review",
										"order_item_marketings.insert_card_review",
										"order_item_marketings.claim_review_id",
										"order_item_marketings.customer_real_email",
										"order_item_marketings.customer_facebook_account",
										"order_item_marketings.customer_wechat_account",
										"order_item_marketings.review_title",
										"order_item_marketings.review_content",
										"order_item_marketings.review_link",
										"order_item_marketings.left_review_at",
										"order_item_marketings.note"
									)
									->leftJoin('order_item_marketings', function($join) {
										$join->on("order_items.AmazonOrderId", "=", "order_item_marketings.oim_amazon_order_id");
										$join->on("order_items.SellerSKU", "=", "order_item_marketings.oim_sku");
									})
									->orderBy('order_items.id', 'desc');
		return $query;
	}

	public static function updateOrderRefundStatusFor3Months() {
		$since = (new Carbon('3 months ago'))->format('Y-m-d');
		self::updateOrderRefundStatus($since);
	}

	public static function updateOrderRefundStatus($since=null) {
		$limit = 1000;
		$offset = 0;
		while (true) {
			$query = self::select('AmazonOrderId', 'ASIN', 'OrderItemId', 'SellerSKU', 'QuantityShipped', 'total_refund', 'refund_item_quantity');
			if ($since) {
				$query->where('created_at', '>=', $since);
			}
			$orderItems = $query->offset($offset)
													->limit($limit)
													->get();
			$grouped = $orderItems->groupBy('AmazonOrderId');
			$grouped->each(function($items, $orderId) {
				$purchasedItemQuantity = $items->sum('QuantityShipped');
				$returnedItemQuantity = $items->where('total_refund', '<', 0)
																			->sum(function ($item) {
																				if ($item->refund_item_quantity) {
																					return $item->refund_item_quantity;
																				} else {
																					return $item->QuantityShipped;
																				}
																			});
				if ($returnedItemQuantity > 0) {
					$order = Order::where('AmazonOrderId', $orderId)->first();
					if ($purchasedItemQuantity == $returnedItemQuantity) {
						$order->OrderStatus = "Returned";
					} else {
						$order->OrderStatus = "Partial Returned";
					}
					$order->save();
				}
			});
			if (count($orderItems) < $limit) {break;}
			else {
				$offset = $offset + $limit;
				if ($offset > 0) { $offset -= 10; }
			}
		}
	}

	public static function boot()
    {
        parent::boot();

        self::saving(function($model) {
					if ($model->isDirty('total_refund')) {
						$shipment = $model->shipment;
						if ($shipment) {
							$shipment->is_refunded = ($model->total_refund < 0);
							$shipment->save();
						}
					}
        });
		}
}
