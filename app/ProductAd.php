<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use App\Traits\MassInsertOrUpdate;

class ProductAd extends Model
{

	use MassInsertOrUpdate;
	
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'product_ads';
	
	
	public function account(){
		return $this->belongsTo('App\Account','seller_id','seller_id');
	}
	

}
	