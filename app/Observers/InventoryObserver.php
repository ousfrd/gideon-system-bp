<?php

namespace App\Observers;

use App\Inventory;

class InventoryObserver
{
    /**
     * Handle the inventory "created" event.
     *
     * @param  \App\Inventory  $inventory
     * @return void
     */
    public function created(Inventory $inventory)
    {
        
    }

    public function saved(Inventory $inventory) {
        // $product = $inventory->product;
        // $inventories = $product->active_inventories;
        // $qty = 0;
        // foreach($inventories as $inventory) {
        //     $qty += $inventory->fulfillable_quantity;
        // }
        // $product->qty = $qty;
        // $product->save();
    }

    /**
     * Handle the inventory "updated" event.
     *
     * @param  \App\Inventory  $inventory
     * @return void
     */
    public function updated(Inventory $inventory)
    {
        //
    }

    /**
     * Handle the inventory "deleted" event.
     *
     * @param  \App\Inventory  $inventory
     * @return void
     */
    public function deleted(Inventory $inventory)
    {
        //
    }

    /**
     * Handle the inventory "restored" event.
     *
     * @param  \App\Inventory  $inventory
     * @return void
     */
    public function restored(Inventory $inventory)
    {
        //
    }

    /**
     * Handle the inventory "force deleted" event.
     *
     * @param  \App\Inventory  $inventory
     * @return void
     */
    public function forceDeleted(Inventory $inventory)
    {
        //
    }
}
