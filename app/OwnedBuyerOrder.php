<?php

namespace App;
use ManagedReviewOrder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class OwnedBuyerOrder extends ManagedReviewOrder
{
    protected $table = 'managed_review_orders';

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order_type', function (Builder $builder) {
            $builder->where('order_type', '!=', ManagedReviewOrder::INVITE_REVIEW_ORDER);
        });
    }
}
