<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;

class AdSnapshot extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'ad_snapshots';
	
	
	public function account(){
		return $this->belongsTo('App\Account','seller_id','seller_id');
	}
	

}
	