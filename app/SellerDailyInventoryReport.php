<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellerDailyInventoryReport extends Model
{
    //
    protected $table = 'seller_daily_inventory_reports';

}
