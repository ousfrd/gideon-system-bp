<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use App\Traits\MassInsertOrUpdate;

class AdSearchtermsReport extends Model
{
	use MassInsertOrUpdate;
	
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'ad_searchterms_reports';
	
	public $timestamps = false;
		
	public function account(){
		return $this->belongsTo('App\Account','seller_id','seller_id');
	}
	
	public function ad_keyword() {
		return $this->belongsTo('App\AdKeyword','keywordId','keywordId');
	}	

	public static function getSearchterms($params) {
		$dataProvider = AdSearchtermsReport::leftJoin('ad_reports', function($join)
                         {
                             $join->on('ad_searchterms_reports.campaignId', '=', 'ad_reports.campaignId');
                             $join->on('ad_searchterms_reports.adGroupId', '=', 'ad_reports.adGroupId');
                             $join->on('ad_searchterms_reports.date', '=', 'ad_reports.date');
                         });


		if(isset($params['asin']) && $params['asin']) {	
			$dataProvider->where('ad_reports.asin', $params['asin']);
		}

		if(isset($params['country']) && $params['country']) {	
			$dataProvider->where('ad_reports.country', $params['country']);
		}

		if(isset($params['start_date']) && isset($params['end_date'])) {	
			$dataProvider->whereBetween('ad_searchterms_reports.date', [$params['start_date'], $params['end_date']]);
		}

		if(isset($params['campaignIds']) && $params['campaignIds']) {	
			if(count(json_decode($params['campaignIds'])) == 1) {
				$dataProvider->where('ad_searchterms_reports.campaignId', json_decode($params['campaignIds'])[0]);
			} elseif(count(json_decode($params['campaignIds'])) > 1) {
				$dataProvider->whereIn('ad_searchterms_reports.campaignId', json_decode($params['campaignIds']));
			}
		}

		if(isset($params['adGroupIds']) && $params['adGroupIds']) {	
			if(count(json_decode($params['adGroupIds'])) == 1) {
				$dataProvider->where('ad_searchterms_reports.adGroupId', json_decode($params['adGroupIds'])[0]);
			} elseif(count(json_decode($params['adGroupIds'])) > 1) {
				$dataProvider->whereIn('ad_searchterms_reports.adGroupId', json_decode($params['adGroupIds']));
			}
		}

		if(isset($params['keywordId']) && $params['keywordId']) {	
			$dataProvider->where('ad_searchterms_reports.keywordId', $params['keywordId']);
		}

		if(isset($params['keywordIds']) && $params['keywordIds']) {	
			if(count(json_decode($params['keywordIds'])) == 1) {
				$dataProvider->where('ad_searchterms_reports.keywordId', json_decode($params['keywordIds'])[0]);
			} elseif(count(json_decode($params['keywordIds'])) > 1) {
				$dataProvider->whereIn('ad_searchterms_reports.keywordId', json_decode($params['keywordIds']));
			}
		}

		if(isset($params['asin_countries']) && $params['asin_countries']) {	
			if(count(json_decode($params['asin_countries'])) == 1) {
				$dataProvider->where('ad_reports.asin', json_decode($params['asin_countries'])[0][0])->where('ad_reports.country', json_decode($params['asin_countries'])[0][1]);
			} elseif(count(json_decode($params['asin_countries'])) > 1) {

				$values = array_map(function (array $value) {
		            return "('".implode($value, "', '")."')"; 
		        }, json_decode($params['asin_countries']));

				$dataProvider->whereRaw(
		            '('.implode(['ad_reports.asin', 'ad_reports.country'], ', ').') in ('.implode($values, ', ').')'
		        );
			}
		}


		$dataProvider->select(DB::raw('ad_searchterms_reports.*, SUM(ad_searchterms_reports.clicks) as total_clicks, SUM(ad_searchterms_reports.impressions) as total_impressions, ROUND(SUM(ad_searchterms_reports.cost * ad_searchterms_reports.exchange_rate), 2) as total_cost, SUM(ad_searchterms_reports.attributedSales30d) as total_sales, SUM(ad_searchterms_reports.attributedConversions30d) as total_orders'));

		$dataProvider->groupBy('ad_searchterms_reports.query');

		$dataProvider->orderBy('total_cost', 'desc');

		$data = $dataProvider->get();

		foreach ($data as $key => $value) {
			$value->total_clicks = (float)$value->total_clicks;
			$value->total_impressions = (float)$value->total_impressions;
			$value->total_cost = (float)$value->total_cost;
			$value->total_sales = (float)$value->total_sales;
			$value->total_orders = (float)$value->total_orders;
			
			$value->acos = (isset($value->total_sales) && $value->total_sales > 0) ? round(($value->total_cost / $value->total_sales) * 100, 2) : "0.0";
			$value->cpc = (isset($value->total_clicks) && $value->total_clicks > 0) ? round($value->total_cost / $value->total_clicks, 2) : 0;
			$value->ctr = (isset($value->total_impressions) && $value->total_impressions > 0) ? round(($value->total_clicks / $value->total_impressions) * 100, 2) : 0;
			$value->cr = (isset($value->total_clicks) && $value->total_clicks > 0) ? round(($value->total_orders / $value->total_clicks) * 100, 2) : 0;
		}

		return $data;
	}
}
	