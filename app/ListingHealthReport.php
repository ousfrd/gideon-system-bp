<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
class ListingHealthReport extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'product_inventory_health_report';
	
	
	function listing() {
		return $this->belongsTo('App\Listing','listing_id','id');
	}
	
	function merchant(){
		return $this->belongsTo('App\Account','seller_id','seller_id');
	}
	
	static function total() {
		$count = self::where('date','>',date('Y-m-d',strtotime('-7 days')))
		
		->count();
		
		
		
		return $count;
	}
}

