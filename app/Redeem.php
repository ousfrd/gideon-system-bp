<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use App\Shipment;
use App\OrderItem;
use App\Listing;
use App\OrderItemMarketing;
use App\Customer;
use App\Traits\ReviewHandlable;
use App\Helpers\DataHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class Redeem extends Model
{
    use ReviewHandlable;
    //
    protected $table = 'redeems';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    static $testEmails = ["test@300gideon.com", "test@test.com"];

    const SOURCES = [
        "post_card" => "Post Card (Email Box)",
        "insert_card" => "Insert Card (Email Box)",
    ];

    const INVITE_REVIEW_SOURCES = [
        "invite_review_wechat" => "WeChat",
        "invite_review_whatsapp" => "Whatsapp",
        "invite_review_fbmessenger" => "FB Messenger",
        "invite_review_email" => "Email"
    ];

    const STATUS = [
        'NEWLY_CREATED' => "Newly Created", 
        'INTRODUCTION' => "Introduction", 
        'RECEIVE_PENDING' => "Receive Pending", 
        'FOLLOW_UP' => "Follow Up", 
        'ASK_FOR_REVIEW_LINK' => "Ask for Review Link", 
        'PROCESSED' => "Processed"
    ];

    public $customer_wechat_account = "";

    public $customer_facebook_account = "";

    function order() {
		return $this->belongsTo ( 'App\Order', 'AmazonOrderId', 'AmazonOrderId' );
	}

	function account() {
		return $this->belongsTo ( 'App\Account', 'seller_id', 'seller_id' );
	}

	function product() {
        // return $this->belongsTo ( 'App\Product', 'asin', 'asin' )->where('country', $this->country);
        return $this->belongsTo ( 'App\Product', 'asin', 'asin' );
    }	

    function orderItemMarketing() {
        return $this->hasOne ('App\OrderItemMarketing', 'oim_amazon_order_id', 'AmazonOrderId');
    }

    function claimReviewCampaign() {
        return $this->belongsTo('App\ClaimReviewCampaign');
    }

    function orderShipment() {
        return $this->hasOne('App\Shipment', 'amazon_order_id', 'AmazonOrderId')->where('asin', $this->asin);
    }

    function inviteReviewLetters() {
        return $this->hasMany('App\InviteReviewLetter');
    }

    public function getReviewPageLinkAttribute() {
        $amzLinks = [
            'US'=>'https://www.amazon.com/',
            'CA'=>'https://www.amazon.ca/',
            'MX'=>'https://www.amazon.com.mx/',
            'UK'=>'https://www.amazon.co.uk/',
            'DE'=>'https://www.amazon.de/',
            'ES'=>'https://www.amazon.es/',
            'FR'=>'https://www.amazon.fr/',
            'IT'=>'https://www.amazon.it/',
            'JP'=>'https://www.amazon.co.jp/',
            'AU'=>'https://www.amazon.com.au/',
        ];
        $link = $amzLinks[$this->country] . 'product-reviews/' . $this->asin . '/ref=cm_cr_arp_d_viewopt_srt?reviewerType=all_reviews&sortBy=recent';
        if ($this->star == 5) {
            $link .= "&filterByStar=five_star";
        }
        if ($this->star == 4) {
            $link .= "&filterByStar=four_star";
        }
        return $link;
    }

    /**
     * filter condition: Get all redeems of this customer called customer-redeems.
     *  1. exclude all products with the brand of customer-redeems.
     *  2. exclude all products sold by customer-redeems sellers.
     * 
     * random pick at most 8 eligible products.
     */
    public function productsForInviteReview() {
        $shipment = Shipment::where('amazon_order_id', $this->AmazonOrderId)->first();
        if ($shipment == NULL) {
            $excludeSellerIds = [$this->seller_id];
            $excludedBrands = Product::where('asin', $this->asin)->whereNotNull('brand')->pluck('brand')->all();
        } else {
            $buyerId = $shipment->buyer_email;
            $buyerShipments = Shipment::where('buyer_email', $buyerId)->get();
            $orderIds =  $buyerShipments->map(function($item){return $item->amazon_order_id;})->all();
            $redeems = self::whereIn('AmazonOrderId', $orderIds)->get();
            $redeemedAsins = $redeems->map(function($item) {return $item->asin;})->all();
            $excludeSellerIds = $redeems->map(function($item){return $item->seller_id;})->all();
            $excludedBrands = Product::whereIn('asin', $redeemedAsins)->whereNotNull('brand')->pluck('brand')->all();
        }
        $productsCandidates = Product::select('products.id', 'products.asin' ,'products.brand', 'products.name', 'products.alias', 'products.country', 'products.small_image')
                                    ->join('seller_listings', 'products.id', '=', 'seller_listings.product_id')
                                    ->where('products.should_invite_review', true)
                                    ->where('products.country', $this->country)
                                    ->whereNotIn('products.brand', $excludedBrands)
                                    ->whereNotNull('products.small_image')
                                    ->whereNotIn('seller_listings.seller_id', $excludeSellerIds)
                                    ->groupBy('products.id')
                                    ->get();
        if ($productsCandidates->count() > 0) {
            $quantity = min(
                $productsCandidates->count(),
                config('giftcard.invite_review_product_count'));
            return $productsCandidates->random($quantity);
        } else {
            return collect([]);
        }
        
    }

    public static function updateClaimReviewCampaignId() {
        $redeems = Redeem::all();
        foreach($redeems as $redeem) {
            $oim = $redeem->orderItemMarketing;
            if ($oim && $campaign = $oim->claimReviewCampaign) {
                $redeem->claim_review_campaign_id = $campaign->id;
                $redeem->claim_review_campaign_name = $campaign->name;
                $redeem->save();
            }
        }
    }

    public static function updateRedeemReviewLinkDate() {
        $redeems = Redeem::all();
        foreach($redeems as $redeem) {
            if (empty($redeem->review_link_date) and !empty($redeem->review_link)) {
                $redeem->review_link_date = $redeem->updated_at->format('Y-m-d');
                $redeem->save();
            }
        }
    }

    public static function deleteTestRecord() {
        $redeems = Redeem::whereIn("email", Redeem::$testEmails)->get();
        foreach ($redeems as $redeem) {
            $redeem->delete();
        }
    }

    public function updateOrderItemMarketing() {
        $oim = $this->orderItemMarketing;
        if (empty($oim)) {
            $orderShipment = Shipment::where("amazon_order_id", $this->AmazonOrderId)->where("asin", $this->asin)->first();
            if ($orderShipment) {
                $oim = new OrderItemMarketing();
                $oim->oim_amazon_order_id = $orderShipment->amazon_order_id;
                $oim->oim_sku = $orderShipment->sku;
                $oim->asin = $orderShipment->asin;
                $oim->buyer_id = $orderShipment->buyer_email;
                $oim->seller_id = $orderShipment->seller_id;
                
            }
        } 
        if ($oim) {
            $oim->customer_real_email = $this->email;
            $insert_card = Listing::select('insert_card')->where('sku', '=', $oim->oim_sku)->where('asin', '=', $oim->asin)->first();
            if ($insert_card->insert_card > 0){
                $this->source = 'insert_card';
                $oim->insert_card_review = true;
                $oim->email_promotion_review = true;
            }
            // if (strtolower(trim($this->source)) == "insert_card") {
            //     $oim->insert_card_review = true;
            //     $oim->email_promotion_review = true;
            // }
            if ($oim->isDirty()) {
                $oim->save();
            }
        }
    }

    public static function getStatusText($status) {
        if (empty($status) || !array_key_exists($status, Redeem::STATUS)) {
            return "";
        } else {
            return Redeem::STATUS[$status];
        }
    }

    public static function getlast30Statistics() {
        $startDate = Carbon::parse('30 days ago')->format('Y-m-d');
        $query = Redeem::select(DB::raw('count(id) as quantity'))
                            ->groupBy('how_to_help')
                            ->where('created_at', '>=', $startDate);
    }

    public function getAmount() {
        if (!empty($this->amount)) {
            return $this->amount;
        }

        $orderItem = $orderItem = OrderItem::select('SellerSKU')->where([
            ['seller_id', '=', $this->seller_id],
            ['AmazonOrderId', '=', $this->AmazonOrderId],
            ['ASIN', '=', $this->asin]
        ])->first();
        if (empty($orderItem)) {
            return 0;
        }

        $sku = $orderItem->SellerSKU;
        $listing = Listing::select('insert_card_review_cost')->where([
            ['sku', '=', $sku],
            ['seller_id', '=', $this->seller_id],
            ['asin', '=', $this->asin],
            ['country', '=', $this->country]
        ])->first();
        if (empty($listing)) {
            return 0;
        }

        $insertCardReviewCost = $listing->insert_card_review_cost;
        if ($insertCardReviewCost != NULL && $this->amount != $insertCardReviewCost) {
            $this->amount = $insertCardReviewCost;
            $this->save();
        }

        return $this->amount;
    }

    public static function updateRedeemSource(){
        $redeems = Redeem::where('created_at', '>', '2021-01-19')->get();
        foreach ($redeems as $redeem) {
            if ($redeem->source == 'unknown') {
                $oim = $redeem->orderItemMarketing;
                if ($oim && $claimReviewCampaign = $oim->claimReviewCampaign) {
                    if ($oim->is_sent > 0){
                        continue;
                    }
                }
                $insert_card = Listing::select('insert_card')->where('sku', '=', $oim->oim_sku)->where('asin', '=', $oim->asin)->first();
                if ($insert_card->insert_card > 0){
                    $redeem->source = 'insert_card';
                    $oim->insert_card_review = true;
                    $redeem->save();
                    $oim->save();
                }
            }
        }
    }

    public static function getRedeemStatistics($fromDate, $toDate) {
        $result = [];

        $redeems = self::where([
            ['requestDate', '>=', $fromDate],
            ['requestDate', '<=', $toDate]
        ])->get();

        $groupedRedeems = DataHelper::groupByVal($redeems, function ($redeem) {
            return sprintf('%s-%s', $redeem->country, $redeem->asin);
        });
        foreach ($groupedRedeems as $k => $redeemsByProduct) {
            $redeemsByType = DataHelper::groupByVal($redeemsByProduct, 'how_to_help');
            $freeProductRedeems = $redeemsByType['Same Free Product'] ?? [];
            $reviewedFreeProductRedeems = array_filter($freeProductRedeems, function ($redeem) {
                return $redeem->is_reviewed > 0;
            });
            $giftcardRedeems = $redeemsByType['Amazon Gift Card'] ?? [];
            $reviewedGiftcardRedeems = array_filter($giftcardRedeems, function ($redeem) {
                return $redeem->is_reviewed > 0;
            });
            $sentGiftcardAmount = 0;
            $sentGiftcardCnt = 0;
            $unsentGiftcardAmount = 0;
            $unsentGiftcardCnt = 0;
            $unsentGiftcardWithoutAmount = 0;
            foreach ($reviewedGiftcardRedeems as $redeem) {
                if ($redeem->is_giftcard_sent > 0) {
                    $sentGiftcardCnt += 1;
                    if ($redeem->amount) {
                        $sentGiftcardAmount += $redeem->amount;
                    } else {
                        $sentGiftcardAmount += 15;
                    }

                    continue;
                }

                $unsentGiftcardCnt += 1;
                if ($redeem->amount) {
                    $unsentGiftcardAmount += $redeem->amount;
                } else {
                    $unsentGiftcardWithoutAmount += 1;
                    $unsentGiftcardAmount += 15;
                }
            }

            $otherRedeems = array_filter($redeemsByProduct, function ($redeem) {
                return $redeem->star < 4 && !in_array(
                    $redeem->how_to_help, ['Get a Full Refund', 'Get a Free Replacement']);
            });
            $reviewedOtherRedeems = array_filter($otherRedeems, function ($redeem) {
                return $redeem->review_id != NULL;
            });

            $claimReviewRespCnt = count($freeProductRedeems) + count($giftcardRedeems) + count($otherRedeems);
            $claimReviewReviewedCnt = count($reviewedFreeProductRedeems) + count($reviewedGiftcardRedeems) + count($reviewedOtherRedeems);

            $inviteReviewRedeems = $redeemsByType['INVITE_REVIEW_REDEEM'] ?? [];
            $reviewedInviteReviewRedeems = array_filter($inviteReviewRedeems, function ($redeem) {
                return $redeem->is_reviewed > 0;
            });
            $inviteReviewRespCnt = count($inviteReviewRedeems);
            $inviteReviewReviewedCnt = count($reviewedInviteReviewRedeems);

            $result[$k] = [
                'claimReviewRespCnt' => $claimReviewRespCnt,
                'claimReviewReviewedCnt' => $claimReviewReviewedCnt,
                'inviteReviewRespCnt' => $inviteReviewRespCnt,
                'inviteReviewReviewedCnt' => $inviteReviewReviewedCnt,
                'sentGiftcardCnt' => $sentGiftcardCnt,
                'sentGiftcardAmount' => $sentGiftcardAmount,
                'unsentGiftcardCnt' => $unsentGiftcardCnt,
                'unsentGiftcardAmount' => $unsentGiftcardAmount,
                'unsentGiftcardWithoutAmount' => $unsentGiftcardWithoutAmount,
            ];
        }

        return $result;
    }

    public static function getDefaultRedeemStatistic() {
        return [
            'claimReviewRespCnt' => 0,
            'claimReviewReviewedCnt' => 0,
            'inviteReviewRespCnt' => 0,
            'inviteReviewReviewedCnt' => 0,
            'sentGiftcardCnt' => 0,
            'sentGiftcardAmount' => 0,
            'unsentGiftcardCnt' => 0,
            'unsentGiftcardAmount' => 0,
            'unsentGiftcardWithoutAmount' => 0,
        ];
    }

    public static function getGiftcardRedeemStatistic($fromDate, $toDate) {
        $result = [];

        $redeems = self::where([
            ['requestDate', '>=', $fromDate],
            ['requestDate', '<=', $toDate]
        ])->get();
    }

    public static function boot() {
        parent::boot();
        self::creating(function($model) {
            $model->updateOrderItemMarketing();
            $oim = $model->orderItemMarketing;
            if ($oim && $claimReviewCampaign = $oim->claimReviewCampaign) {
                $model->claim_review_campaign_id = $claimReviewCampaign->id;
                $model->claim_review_campaign_name = $claimReviewCampaign->name;
                $claimReviewCampaign->increment('response_quantity');
                if (strtolower($model->how_to_help) == strtolower("Same Free Product")){
                    $claimReviewCampaign->free_product_quantity = $claimReviewCampaign->free_product_quantity + 1;
                }
                if (strtolower($model->how_to_help) == strtolower("Amazon Gift Card")){
                    $claimReviewCampaign->gift_card_quantity = $claimReviewCampaign->gift_card_quantity + 1;
                    $model->amount = $claimReviewCampaign->cashback;
                }
                $claimReviewCampaign->save();
                if ($oim->is_sent > 0){
                    $model->source = 'post_card';
                    $oim->insert_card_review = false;
                    $oim->email_promotion_review = false;
                }
            } else {
                if ($oim && $oim->insert_card_review == true) {
                    ProductStatistic::updateInsertCardReviews($oim->oim_amazon_order_id);
                    $model->amount = Listing::select('insert_card_review_cost')->where('sku', '=', $oim->oim_sku)->where('asin', '=', $model->asin)->pluck('insert_card_review_cost')[0];
                }
            }
            if ($oim && $model->how_to_help == "INVITE_REVIEW_REDEEM") {
                $oim->customer_real_email = $model->email;
                $oim->customer_wechat_account = $model->customer_wechat_account;
                $oim->customer_facebook_account = $model->customer_facebook_account;
                $oim->invite_review = true;
                $oim->save();
            }
        });

        self::saved(function($model) {
            if ($model->isDirty('review_link')) {
                if ($claimReviewCampaign = $model->claimReviewCampaign) {
                    $claimReviewCampaign->updateResponse();
                }
            }
            if ($model->isDirty('edited_email') && $model->orderShipment) {
                $amazonEmail = $model->orderShipment->buyer_email;
                Customer::where('amazon_buyer_email', $amazonEmail)->update(['email' => $model->edited_email]);
            }
        });

        self::deleted(function($model) {
            if ($claimReviewCampaign = $model->claimReviewCampaign) {
                $claimReviewCampaign->updateResponse();
            }
            if ($oim = $model->orderItemMarketing) {
                $oim->insert_card_review = false;
                $oim->email_promotion_review = false;
                $oim->customer_real_email = null;
                $oim->customer_wechat_account = null;
                $oim->customer_facebook_account = null;
                $oim->invite_review = false;
                $oim->save();
            }
        });
    }

}
