<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use App\Traits\MassInsertOrUpdate;

class AdGroup extends Model
{
	
	use MassInsertOrUpdate;

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'ad_groups';
	
	
	public function account(){
		return $this->belongsTo('App\Account','seller_id','seller_id');
	}

	public function ad_campaign() {
		return $this->belongsTo('App\AdCampaign','campaignId','campaignId');
	}

	public function ad_groups_reports() {
		return $this->hasMany('App\AdGroupsReport','adGroupId','adGroupId');
	}

	public function ad_keywords() {
		return $this->hasMany('App\AdKeyword','keywordId','keywordId');
	}
		
	public function ad_reports() {
		return $this->hasMany('App\AdReport','adGroupId','adGroupId');
	}


	public static function get_data($params) {
		$data = [];

		if($params['seller_id']) {

			$query = AdGroup::where('seller_id', $params['seller_id']);

			if($params['country']) {
				$query->where('country', $params['country']);
			}

			$adgroups = $query->get();
		} else {
			$adgroups = AdGroup::all();
		}

		foreach ($adgroups as $adgroup) {
			$data[$adgroup->adGroupId]['data'] = $adgroup;
			
	
			// $data[$adgroup->adGroupId]['campaign_name'] = $adgroup->ad_campaign->name;
		}


		$adgroups_query = DB::table('ad_reports')
                     ->select(DB::raw('adGroupId, campaignName, SUM(attributedSales30d) as total_sales, 
                     	SUM(clicks) as total_clicks, 
                     	SUM(impressions) as total_impressions, 
                     	SUM(cost * exchange_rate) as total_cost,
                     	SUM(attributedConversions30d) as total_orders'));

        if(isset($params['seller_id'])) {
			$adgroups_query->where('seller_id', $params['seller_id']);
		}

		if(isset($params['country'])) {
			$adgroups_query->where('country', $params['country']);
		}

		if(isset($params['start_date']) && isset($params['end_date'])) {	
			$adgroups_query->whereBetween('date', [$params['start_date'], $params['end_date']]);
		}

		$adgroups_data = $adgroups_query->groupBy('adGroupId')->orderBy('total_cost', 'desc')->get();


		foreach ($adgroups_data as $key => $value) {
			$data[$value->adGroupId]['campaign_name'] = $value->campaignName;
			$data[$value->adGroupId]['total_sales'] = $value->total_sales;
			$data[$value->adGroupId]['total_clicks'] = $value->total_clicks;
			$data[$value->adGroupId]['total_impressions'] = $value->total_impressions;
			$data[$value->adGroupId]['total_cost'] = round($value->total_cost,2);
			$data[$value->adGroupId]['total_orders'] = $value->total_orders;
		}

		return $data;


	}


	public static function getAdgroups($params) {

		$dataProvider = AdGroup::join('ad_groups_reports', 'ad_groups.adGroupId', '=', 'ad_groups_reports.adGroupId')
					->join('ad_campaigns', 'ad_groups.campaignId', '=', 'ad_campaigns.campaignId');


		if(isset($params['seller_id']) && $params['seller_id'] && $params['seller_id'] != 'all') {
			$dataProvider->where('ad_groups.seller_id', $params['seller_id']);

		}

		if(isset($params['country']) && $params['country'] && $params['country'] != 'all') {	
			$dataProvider->where('ad_groups.country', $params['country']);
		}

		if(isset($params['start_date']) && isset($params['end_date'])) {	
			$dataProvider->whereBetween('ad_groups_reports.date', [$params['start_date'], $params['end_date']]);
		}

		if(isset($params['query']) && $params['query']) {	
			$dataProvider->where('ad_groups_reports.campaignName', 'like', "%".$params['query']."%");
		}

		if(isset($params['campaignState']) && $params['campaignState'] && $params['campaignState'] != 'all') {	
			$dataProvider->where('ad_campaigns.state', $params['campaignState']);
		}

		if(isset($params['campaignTargetingType']) && $params['campaignTargetingType'] && $params['campaignTargetingType'] != 'all') {	
			$dataProvider->where('ad_campaigns.targetingType', $params['campaignTargetingType']);
		}

		if(isset($params['campaignId']) && $params['campaignId']) {	
			$dataProvider->where('ad_groups.campaignId', $params['campaignId']);
		}

		if(isset($params['campaignIds']) && $params['campaignIds']) {	

			if(count(json_decode($params['campaignIds'])) == 1) {
				$dataProvider->where('ad_groups.campaignId', json_decode($params['campaignIds'])[0]);
			} elseif(count(json_decode($params['campaignIds'])) > 1) {
				$dataProvider->whereIn('ad_groups.campaignId', json_decode($params['campaignIds']));
			}

		}

		if(isset($params['adGroupState']) && $params['adGroupState']) {	
			$dataProvider->where('ad_groups.state', $params['adGroupState']);
		}

		$dataProvider->select(DB::raw('ad_groups.*, ad_campaigns.targetingType, ad_groups_reports.adGroupId, SUM(ad_groups_reports.clicks) as total_clicks, SUM(ad_groups_reports.impressions) as total_impressions, ROUND(SUM(ad_groups_reports.cost * ad_groups_reports.exchange_rate), 2) as total_cost, SUM(ad_groups_reports.attributedSales30d) as total_sales, SUM(ad_groups_reports.attributedConversions30d) as total_orders'));

		$dataProvider->groupBy('ad_groups_reports.adGroupId');


		$data = $dataProvider->get();

		foreach ($data as $key => $value) {
			$value->defaultBid = (float)$value->defaultBid;
			$value->total_clicks = (float)$value->total_clicks;
			$value->total_impressions = (float)$value->total_impressions;
			$value->total_cost = (float)$value->total_cost;
			$value->total_sales = (float)$value->total_sales;
			$value->total_orders = (float)$value->total_orders;

			$value->acos = (isset($value->total_sales) && $value->total_sales > 0) ? round(($value->total_cost / $value->total_sales) * 100, 2) : "0.0";
			$value->cpc = (isset($value->total_clicks) && $value->total_clicks > 0) ? round($value->total_cost / $value->total_clicks, 2) : 0;
			$value->ctr = (isset($value->total_impressions) && $value->total_impressions > 0) ? round(($value->total_clicks / $value->total_impressions) * 100, 2) : 0;
			$value->cr = (isset($value->total_clicks) && $value->total_clicks > 0) ? round(($value->total_orders / $value->total_clicks) * 100, 2) : 0;
		}

		return $data;

	}


}
	