<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App;

use App\Helpers\Utils;


class AmazonOrder {
	public $amazonOrderId;
	public $purchaseDate;
	public $orderStatus;
	public $sku;
	public $asin;
	public $productName;
	public $quantity;
	public $currency;
	public $itemPrice;
	public $itemTax;
	public $shippingPrice;
	public $shippingTax;
	public $giftWrapPrice;
	public $giftWrapTax;
	public $itemPromotionDiscount;
	public $shipPromotionDiscount;
	public $salesChannel;
	public $fulfillmentChannel;

	protected static $propertyMapping = [
		'amazonOrderId' => ['amazon-order-id'],
		'purchaseDate' => ['purchase-date'],
		'orderStatus' => ['order-status'],
		'sku' => ['sku'],
		'asin' => ['asin'],
		'productName' => ['product-name'],
		'quantity' => ['quantity'],
		'currency' => ['currency'],
		'itemPrice' => ['item-price'],
		'itemTax' => ['item-tax'],
		'shippingPrice' => ['shipping-price'],
		'shippingTax' => ['shipping-tax'],
		'giftWrapPrice' => ['gift-wrap-price'],
		'giftWrapTax' => ['gift-wrap-tax'],
		'itemPromotionDiscount' => ['item-promotion-discount'],
		'shipPromotionDiscount' => ['ship-promotion-discount'],
		'salesChannel' => ['sales-channel'],
		'fulfillmentChannel' => ['fulfillment-channel'],
	];

	public static function fromOrderRecord($orderRecord) {
		$order = new self();
		foreach (self::$propertyMapping as $k => $v) {
			$order->$k = Utils::getValueByKeys($orderRecord, $v);
		}

		$order->quantity = (int) $order->quantity;
		$order->itemPrice = (float) $order->itemPrice;
		$order->itemTax = (float) $order->itemTax;
		$order->shippingPrice = (float) $order->shippingPrice;
		$order->shippingTax = (float) $order->shippingTax;
		$order->giftWrapPrice = (float) $order->giftWrapPrice;
		$order->giftWrapTax = (float) $order->giftWrapTax;
		$order->itemPromotionDiscount = (float) $order->itemPromotionDiscount;
		$order->shipPromotionDiscount = (float) $order->shipPromotionDiscount;

		return $order;
	}

	public static function fromArray($record) {
		$order = new self();
		foreach (self::$propertyMapping as $k => $v) {
			$order->$k = $record[$k] ?? NULL;
		}

		return $order;
	}
}
