<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App;

use App\Helpers\Utils;


class AmazonFBAInventory {
	public $asin;
	public $sku;
	public $condition;
	public $mfnListingExists;
	public $mfnFulfillableQuantity;
	public $afnListingExists;
	public $afnWarehouseQuantity;
	public $afnFulfillableQuantity;
	public $afnUnsellableQuantity;
	public $afnReservedQuantity;
	public $afnTotalQuantity;
	public $perUnitVolume;
	public $afnInboundWorkingQuantity;
	public $afnInboundShippedQuantity;
	public $afnInboundReceivingQuantity;
	public $afnResearchingQuantity;

	protected static $propertyMapping = [
		'afnResearchingQuantity' => ['afn-researching-quantity', 'afn-researching-qty', 'AFN Researching Qty'],
		'afnInboundReceivingQuantity' => ['afn-inbound-receiving-quantity', 'AFN Inbound Receiving Qty'],
		'afnInboundShippedQuantity' => ['afn-inbound-shipped-quantity', 'AFN Inbound Shipped Qty'],
		'afnInboundWorkingQuantity' => ['afn-inbound-working-quantity', 'AFN Inbound Working Qty'],
		'perUnitVolume' => ['per-unit-volume', 'Volume'],
		'afnTotalQuantity' => ['afn-total-quantity', 'AFN Total Qty'],
		'afnReservedQuantity' => ['afn-reserved-quantity', 'AFN Encumbered Qty'],
		'afnUnsellableQuantity' => ['afn-unsellable-quantity', 'AFN Unsellable Qty'],
		'afnFulfillableQuantity' => ['afn-fulfillable-quantity', 'AFN Fulfillable Qty'],
		'afnWarehouseQuantity' => ['afn-warehouse-quantity', 'AFN Warehouse Qty'],
		'afnListingExists' => ['afn-listing-exists', 'AFN Listing Exists'],
		'mfnFulfillableQuantity' => ['mfn-fulfillable-quantity', 'MFN Fulfillable Qty'],
		'mfnListingExists' => ['mfn-listing-exists', 'MFN Listing Exists'],
		'condition' => ['condition', 'Condition'],
		'sku' => ['sku', 'Merchant SKU'],
		'asin' => ['asin', 'ASIN']
	];

	public static function fromInventoryRecord($inventoryRecord) {
		$integerFields = [
			'mfnFulfillableQuantity',
			'afnWarehouseQuantity',
			'afnFulfillableQuantity',
			'afnUnsellableQuantity',
			'afnReservedQuantity',
			'afnTotalQuantity',
			'afnInboundWorkingQuantity',
			'afnInboundShippedQuantity',
			'afnInboundReceivingQuantity',
			'afnResearchingQuantity'
		];

		$inventory = new self();
		foreach (self::$propertyMapping as $k => $v) {
			if (in_array($k, $integerFields)) {
				$inventory->$k = (int) Utils::getValueByKeys($inventoryRecord, $v);
			} else {
				$inventory->$k = Utils::getValueByKeys($inventoryRecord, $v);
			}
		}

		$inventory->perUnitVolume = round(floatval($inventory->perUnitVolume), 2);

		return $inventory;
	}

	public static function fromArray($record) {
		$inventory = new self();
		foreach (self::$propertyMapping as $k => $v) {
			$inventory->$k = $record[$k] ?? NULL;
		}

		return $inventory;
	}
}