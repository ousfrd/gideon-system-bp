<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PmGoal extends Model
{
    protected $table = 'pm_goal';
    protected $fillable = ['user_id', 'year', 'month', 'goal'];
}
