<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\ShipStationOrderImportService;

class ShipStationServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ShipStationOrderImportService::class, function ($app) {
            return new ShipStationOrderImportService(
                $app['config']['shipstation']['api_key'],
                $app['config']['shipstation']['api_secret'],
                $app['config']['shipstation']['retry_enabled'],
                $app['config']['shipstation']['max_retry']
            );
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [Configuration::class];
    }
}
