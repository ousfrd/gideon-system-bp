<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use Illuminate\Support\Facades\File;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();


        // Gate::define('view-dashboard', function ($user) {
        //     return in_array(strtolower($user->role), ['system admin','admin','product manager','product manager assistant']);
        // });

        $routes = $this->getRoutes();
        $outsouceCSAllowRoutes = [
            'user.change-password', 'user.do-change-password',
            'redeems.freeProduct', 'redeems.giftcard', 'redeems.other',
            'redeem.ajaxsave', 'redeems.dashboard', 'redeems.checkExists',
            'redeems.store', 'redeems.list-invite-reviews',
            'order.view', 'order.orderItems',
            'search'
        ];
        foreach($routes as $route) {
            Gate::define($route, function($user) use($route, $outsouceCSAllowRoutes) {
                if ((($user->role == 'outsource_customer_service')) && !in_array($route, $outsouceCSAllowRoutes)) {
                    return false;
                } else {
                    return true;
                }
            });
        }

        Gate::define('view-home-page', function ($user) {
            $deniedRoles = [];
            return $this->hasPermission($user, [], $deniedRoles);
        });

        Gate::define('manage-users', function ($user) {
            $allowedRoles = ['system admin'];
            return $this->hasPermission($user, $allowedRoles);
        });
        
        Gate::define('manage-accounts', function ($user) {
            $allowedRoles = ['system admin','admin','product manager'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('manage-orders', function ($user) {
            $allowedRoles = ['system admin','admin','product manager','product manager assistant','cs','marketing','ads manager'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('manage-outsource-orders', function ($user) {
            $allowedRoles = ['system admin','admin','product manager','product manager assistant','cs','marketing','ads manager','outsource_customer_service'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('manage-products', function ($user) {
            $allowedRoles = ['system admin','admin','product manager','product manager assistant','ads manager','cs','marketing'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('manage-ads', function ($user) {
            $allowedRoles = ['system admin','admin','product manager','ads manager'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('manage-reports', function ($user) {
            $allowedRoles = ['system admin','admin','product manager','product manager assistant','ads manager','cs','marketing'];
            return $this->hasPermission($user, $allowedRoles);
        });     	

        Gate::define('generate-report', function ($user) {
            $allowedRoles = ['system admin','admin','product manager'];
            return $this->hasPermission($user, $allowedRoles);
        });  

        Gate::define('view-pm-monthly-report', function ($user) {
            $allowedRoles = ['system admin','admin','product manager'];
            return $this->hasPermission($user, $allowedRoles);
        });  

        Gate::define('view-disbursement-report', function ($user) {
            $allowedRoles = ['system admin','admin'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('view-performance-report', function ($user) {
            $allowedRoles = ['system admin','admin', 'product manager'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('manage-inbound-shipments', function ($user) {
            $allowedRoles = ['system admin','admin','product manager'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('manage-emails', function ($user) {
            $allowedRoles = ['system admin','admin','product manager','cs'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('manage-reviews', function ($user) {
            $allowedRoles = ['system admin','admin','product manager','product manager assistant','cs','marketing'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('manage-redeems', function ($user) {
            $allowedRoles = ['system admin','admin','product manager','cs','marketing', 'outsource_customer_service'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('manage-redeems.list-follow-up', function($user) {
            if (Gate::allows('manage-redeems')) {
                $deniedRoles = ['outsource_customer_service'
            ];
                return $this->hasPermission($user, [], $deniedRoles);
            } else {
                return false;
            }
        });

        Gate::define('manage-redeems.list-warehouse-requests', function($user) {
            if (Gate::allows('manage-redeems')) {
                $deniedRoles = ['outsource_customer_service'];
                return $this->hasPermission($user, [], $deniedRoles);
            } else {
                return false;
            }
        });

        Gate::define('manage-redeems.view-seller-column', function($user) {
            if (Gate::allows('manage-redeems')) {
                $deniedRoles = ['outsource_customer_service'];
                return $this->hasPermission($user, [], $deniedRoles);
            } else {
                return false;
            }
        });

        Gate::define('manage-redeems.view-amount-column', function($user) {
            if (Gate::allows('manage-redeems')) {
                $deniedRoles = ['outsource_customer_service'];
                return $this->hasPermission($user, [], $deniedRoles);
            } else {
                return false;
            }
        });

        Gate::define('manage-redeems.view-shipping-address-column', function($user) {
            if (Gate::allows('manage-redeems')) {
                $deniedRoles = ['outsource_customer_service'];
                return $this->hasPermission($user, [], $deniedRoles);
            } else {
                return false;
            }
        });



        Gate::define('manage-redeems.delete-test-data', function($user) {
            if (Gate::allows('manage-redeems')) {
                $allowedRoles = ['admin'];
                return $this->hasPermission($user, $allowedRoles);
            } else {
                return false;
            }
        });

        Gate::define('manage-imports', function ($user) {
            $allowedRoles = ['system admin','admin','product manager','marketing'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('manage-product-manager', function ($user) {
            $allowedRoles = ['system admin','admin','product manager','product manager assistant'];
            return $this->hasPermission($user, $allowedRoles);
        });
        
        Gate::define('manage-product', function ($user,$product) {
            $allowedRoles = ['system admin','admin'];
            $hasPermission = $this->hasPermission($user, $allowedRoles);
            if ($hasPermission) {
                return true;
            }
            $userRoles = explode(',', strtolower($user->role));
            if(in_array('product manager', $userRoles) || in_array('product manager assistant', $userRoles)) {
                if($user->canManageProduct($product->id)) {
                    return true;
                }
            }
            return false;
        });


        Gate::define('manage-account', function ($user,$account) {
            $allowedRoles = ['system admin','admin','product manager'];
            $hasPermission = $this->hasPermission($user, $allowedRoles);
            if ($hasPermission) {
                return true;
            }
            $userRoles = explode(',', strtolower($user->role));
            
            if(in_array('account manager', $userRoles)) {
                if($user->canManageAccount($account->id)) {
                    return true;
                }
            }
            return false;
            
        });

        Gate::define('view-marketing', function($user) {
            $allowedRoles = ['system admin','admin'];
            $hasPermission = $this->hasPermission($user, $allowedRoles);
            if ($hasPermission) {
                return true;
            }else {
                $userRoles = explode(',', strtolower($user->role));
                foreach($userRoles as $role) {
                    if (substr($role, 0, 9) == "marketing" || $role == "outsource_customer_service") {
                        return true;
                    }
                }
                return false;
            }
        });

        Gate::define('manage-claim-review', function($user) {
            $allowedRoles = ['system admin','admin', 'marketing_claim_review'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('invite-review', function($user) {
            $allowedRoles = ['system admin','admin', 'marketing_invite_review'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('self-review', function($user) {
            $allowedRoles = ['system admin','admin', 'marketing_self_review'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('access-managed-review', function($user) {
            $allowedRoles = ['system admin','admin', 'marketing_self_review'];
            return $this->hasPermission($user, $allowedRoles);
        });

        

        Gate::define('email-promotion-review', function($user) {
            $allowedRoles = ['system admin','admin', 'marketing_email_review'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('edit-customer-info', function($user) {
            $allowedRoles = ['system admin','admin', 'cs', 'marketing'];
            return $this->hasPermission($user, $allowedRoles); 
        });

        Gate::define('view-inventory-management', function($user) {
            $allowedRoles = ['system admin','admin', 'product manager', 'product manager assistant'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('view-customers', function($user) {
            $allowedRoles = ['system admin','admin', 'product manager', 'product manager assistant', 'cs', 'marketing'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('manage-insertcard', function($user) {
            $allowedRoles = ['system admin','admin', 'product manager', 'product manager assistant'];
            return $this->hasPermission($user, $allowedRoles);
        });

        Gate::define('view-own-dashboard', function($user) {
            $deniedRoles = ['outsource_customer_service'];
            return $this->hasPermission($user, [], $deniedRoles);
        });

        Gate::define('user.view', function($user, $id) {
            $allowedRoles = [
                'product manager assistant', 'product manager',
                'admin', 'system admin'
            ];
            return $this->hasPermission($user, $allowedRoles) || $user->id == $id;
        });

        Gate::define('edit-product-info', function ($user, $product) {
            $allowedRoles = ['system admin','admin'];
            $pms = $product->users->map(function($user) {
                return $user->id;
            })->all();
            $isPm = in_array($user->id, $pms);
            $hasRole = $this->hasPermission($user, $allowedRoles);
            return $isPm || $hasRole;
        });

    }

    private function hasPermission($user, $allowedRoles, $deniedRoles = []) {
        if (!$user->role) return false;
        $userRoles = explode(',', strtolower($user->role));
        if (count($deniedRoles) > 0) {
            if (count($userRoles) == 1) {
                return !in_array($userRoles[0], $deniedRoles);
            } else {
                return true;
            }
            
        } else {
            return count(array_intersect($userRoles, $allowedRoles)) > 0;
        }
        
    }

    private function getRoutes() {
        $routesStr = File::get(base_path().'/routes/web.php');
        $singleRouteMatches = [];
        $singleResourceMathes = [];
        $routeNames = [];
        $resourceActions = ['index', 'create', 'store', 'show', 'edit', 'update', 'destroy'];
        preg_match_all('/->name\(\s*[\'"]\s*(.+?)\s*[\'"]\s*\)/', $routesStr, $singleRouteMatches);
        if ($singleRouteMatches && count($singleRouteMatches) > 1) {
            $routeNames = $singleRouteMatches[1];
        }
        preg_match_all('/Route::resource\s*\([\'"](.+?)[\'"]\s*,/', $routesStr, $singleResourceMathes);
        if ($singleResourceMathes && count($singleResourceMathes) > 1) {
            foreach($singleResourceMathes[1] as $resource) {
                foreach($resourceActions as $action) {
                    $routeName = $resource . "." . $action;
                    $routeNames[] = $routeName;
                }
            }
        }
        return $routeNames;
    }
}
