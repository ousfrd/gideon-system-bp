<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Account;
use App\Order;
use App\Product;
use App\EmailTemplate;
use App\EmailList;
use App\Traits\MailSenderChangeable;

class TestEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels, MailSenderChangeable;

    protected $order, $template;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($template_id)
    {
        // $this->order = Order::where('OrderStatus', 'Shipped')->orderBy('PurchaseDate', 'desc')->first();
        $this->template = EmailTemplate::find($template_id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $asin = "B010101010";
        $country = "US";

        $template = $this->template;

        if(!$template) {
            // no template was found
            return;
        }

        $from_email = env('TEST_SMTP_FROM');
        $from_name = "Test Seller";
        $product_name = "Sample product name";
        // foreach($this->order->order_items as $order_item) {
        //     $product_name .= $order_item->Title;
        // }
        $product_image_url = "https://images-na.ssl-images-amazon.com/images/I/71vwtfVhqyL._SX522_.jpg";
        $product_image = "<img src='".$product_image_url."' width='100' height='100'>";
        $first_name = "John";
        $marketplace_id = "ATVPDKIKX0DER";
        $country = 'US';
        $amzLink = 'https://www.amazon.com/';
        $order_id = '12032123231';
        $product_link = $amzLink."dp/".$asin;
        $order_link = $amzLink."gp/your-account/order-details/ref=oh_aui_or_o00_?ie=UTF8&orderID=".$order_id;

        $data = [
                'order_id' => $order_id,
                'order_link' => $order_link,
                'order_link_with_id' => "<a href='".$order_link."' target='_blank'>".$order_id."</a>",
                'customer_name' => "John Smith",
                'first_name' => $first_name,
                'seller_name' => $from_name,
                'product_name' => $product_name,
                'product_link' => $product_link,
                'product_link_with_name' => "<a href='".$product_link."' target='_blank'>".$product_name."</a>",
                'product_image_url' => $product_image_url,
                'product_image' => $product_image,
                'contact_us_link' => $amzLink."gp/help/customer/contact-us?ie=UTF8&ref_=hp_gt_comp_cu&",
                'product_review_link' => $amzLink."review/create-review?ie=UTF8&asin=".$asin."&channel=glance-detail&ref_=cm_cr_dp_d_wr_but_top&",
                'seller_feedback_link' => $amzLink."gp/feedback/?orderID=".$order_id
        ];


        $subject = $this->filter_email($template->subject, $data);
        $content = $this->filter_email($template->message, $data);

        // EmailList::
        // $email_list = new EmailList;
        // $email_list->type = "Product Review";
        // $email_list->seller_id = $this->order->seller_id;
        // $email_list->amazon_order_id = $this->order->AmazonOrderId;
        // $email_list->to_name = $this->order->BuyerName;
        // $email_list->to_email = $this->order->BuyerEmail;
        // $email_list->sent_date = date('Y-m-d H:i:s');
        // $email_list->subject = $subject;
        // $email_list->message = $content;
        // $email_list->save();


        // if($this->order->account->smtp_host == "smtp.google.com" || $this->order->account->smtp_host == "smtp.mailgun.org") {
        //     $port = "587";
        // } else {
        //     $port = "2587";
        // }
        $this->changeMailSender([
            'username'=>env('TEST_SMTP_USERNAME'),
            'password'=>env('TEST_SMTP_PASSWORD'),
            'host'=>env('TEST_SMTP_HOST'),
            'port'=>env('TEST_SMTP_PORT')
        ]);

        return $this->from($from_email, $from_name)
        ->subject($subject)
        ->view('email.orders.product_review')->with([
                'content' => $content
            ]);
    }


    private function filter_email($string, $data) {

        $found = preg_match_all('/\[\[(.*?)\]\]/', $string, $matches);

        if($found) {
            $groups = $matches[0];
            foreach ($matches[1] as $k => $match) {
                $key_text = explode(":", $match);

                if(array_key_exists(0, $key_text) && !empty($key_text[0])) {
                    $key = str_replace("-", "_", $key_text[0]);
                }

                $origin = $groups[$k];

                if(array_key_exists($key, $data)) {
                    if(array_key_exists(1, $key_text) && !empty($key_text[1])) {
                        $text = $key_text[1];
                        $replace = "<a href='".$data[$key]."' target='_blank'>".$text."</a>";
                    } else {
                        $replace = $data[$key];
                    }

                    $string = str_replace($origin, $replace, $string);
                }

            }

        }

        return $string;
    }
}
