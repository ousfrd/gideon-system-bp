<?php

namespace App\Jobs;

use App\Import;
use Storage;
use App\Helpers\AdReportImporter;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportAds implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $import;

    public $timeout = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Import $import)
    {
        $this->import = $import;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sellerId = $this->import->seller_id;
        $country = $this->import->country;
        $adReportPath = $this->import->filename;
        if (!Storage::disk('local')->exists($adReportPath)) {
            Storage::disk('local')->put(
                $adReportPath,
                Storage::disk('dropbox')->get($adReportPath)
            );
        }

        if (!Storage::disk('local')->exists($adReportPath)) {
            return;
        }

        AdReportImporter::importAdReport(
            Storage::disk('local')->path($adReportPath), $sellerId, $country);

        $this->import->status = "imported";
        $this->import->save();

    }
}
