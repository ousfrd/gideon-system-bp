<?php

namespace App\Jobs;

use App\Import;
use App\Product;
use App\Listing;
use App\Account;
use App\AdReport;
use App\AdCampaign;
use App\AdGroup;
use App\AdKeyword;
use App\AdCampaignsReport;
use App\AdGroupsReport;
use App\AdKeywordsReport;
use App\ProductAd;

use DB;

use Rap2hpoutre\FastExcel\FastExcel;
use Storage;
use App\Helpers\CurrencyHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportCampaigns implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $import;

    public $timeout = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Import $import)
    {
        $this->import = $import;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if(!Storage::disk('local')->exists($this->import->filename)) {
            Storage::put($this->import->filename, Storage::disk('dropbox')->get($this->import->filename));
        }


        $items = (new FastExcel)->sheet(2)->import(storage_path('app/'.$this->import->filename));

        $seller_id = $this->import->seller_id;
        $country = $this->import->country;
        $report_date = $this->import->report_date;
        $method = $this->import->method;



        $this->importData($items, $seller_id, $country, $report_date, $method);


        echo "Finish importing";

        $this->import->status = "imported";
        $this->import->save();

    }

    public function importData($items, $seller_id, $country, $report_date, $method) {

        $row_data = [];
        $adcampaign_data = [];
        $adcampaignreports_data = [];
        $adgroup_data = [];
        $adgroupreport_data = [];
        $adkeyword_data = [];
        $adkeywordsreport_data = [];
        $productad_data = [];


        foreach ($items as $key => $row) {

            try {
                if ( $row['Record Type'] == "Campaign" ) { // Campaign

                        $row_data['adcampaign'] = [];

                        $row_data['adcampaign']['seller_id'] = $seller_id;   
                        $row_data['adcampaign']['country'] = $country;
                        $row_data['adcampaign']['campaignType'] = 'sponsoredProducts';      
                        $row_data['adcampaign']['auto_scheduler'] = 'disabled';      
                                      
                        $row_data['adcampaign']['campaignId'] = $row['Campaign ID'];
                        $row_data['adcampaign']['name'] = $row['Campaign'];         
                        $row_data['adcampaign']['dailyBudget'] = $row['Campaign Daily Budget'];         
                                                  
                        $row_data['adcampaign']['targetingType'] = strtolower($row['Campaign Targeting Type']);

                        $row_data['adcampaign']['startDate'] = date("Y-m-d", strtotime($row['Campaign Start Date']));                                
                        $row_data['adcampaign']['state'] = $row['Campaign Status'];   
                    
                        $servingStatusArr = ['enabled'=>'CAMPAIGN_STATUS_ENABLED', 'paused'=>'CAMPAIGN_PAUSED', 'archived'=> 'CAMPAIGN_ARCHIVED'];
                        $row_data['adcampaign']['servingStatus'] = $servingStatusArr[$row['Campaign Status']];
                        $row_data['adcampaign']['lastUpdatedDate'] = time(); 
                    
                        $adcampaign_data[]= $row_data['adcampaign'];



                        $row_data['adcampaignreports'] = [];

                        $row_data['adcampaignreports']['seller_id'] = $seller_id;   
                        $row_data['adcampaignreports']['country'] = $country;
                        $row_data['adcampaignreports']['date'] = $report_date;
                        $row_data['adcampaignreports']['currency'] = config ( 'app.countriesCurrency' )[$country];
                        $row_data['adcampaignreports']['exchange_rate'] = CurrencyHelper::get_exchange_rate($report_date, $row_data['adcampaignreports']['currency'], 'USD');
                        $row_data['adcampaignreports']['campaignName'] = $row['Campaign'];                                   
                        $row_data['adcampaignreports']['campaignId'] = $row['Campaign ID'];
                        $row_data['adcampaignreports']['campaignStatus'] = $row['Campaign Status'];                                
                        $row_data['adcampaignreports']['campaignBudget'] = $row['Campaign Daily Budget'];    


                        if($method == "fix" || $method == "plus") {
                            $row_data['adcampaignreports']['impressions'] = $row['Impressions'];
                            $row_data['adcampaignreports']['clicks'] = $row['Clicks'];
                            $row_data['adcampaignreports']['cost'] = $row['Spend'];
                            $row_data['adcampaignreports']['attributedSales1d'] = $row['Sales'];
                            $row_data['adcampaignreports']['attributedConversions1d'] = $row['Orders'];
                            $row_data['adcampaignreports']['attributedUnitsOrdered1d'] = $country == "UK" ? $row['Total units'] : $row['Total Units'];
                        } elseif($method == "minus") {
                            $row_data['adcampaignreports']['impressions_diff'] = $row['Impressions'];
                            $row_data['adcampaignreports']['clicks_diff'] = $row['Clicks'];
                            $row_data['adcampaignreports']['cost_diff'] = $row['Spend'];
                            $row_data['adcampaignreports']['attributedSales1d_diff'] = $row['Sales'];
                            $row_data['adcampaignreports']['attributedConversions1d_diff'] = $row['Orders'];
                            $row_data['adcampaignreports']['attributedUnitsOrdered1d_diff'] = $country == "UK" ? $row['Total units'] : $row['Total Units'];
                        }                           

                        $adcampaignreports_data[]= $row_data['adcampaignreports'];
                }




                if ( $row['Record Type'] == "Ad Group" ){ 

                        $row_data['adgroup'] = [];

                        $row_data['adgroup']['seller_id'] = $seller_id;   
                        $row_data['adgroup']['country'] = $country;
                                      
                        $row_data['adgroup']['adGroupId'] = $row['Record ID'];
                        $row_data['adgroup']['name'] = $row['Ad Group'];       
                        $row_data['adgroup']['maxbid'] = $row['Max Bid'];                     
                        $row_data['adgroup']['campaignId'] = $row['Campaign ID'];
                        $row_data['adgroup']['state'] = $row['Ad Group Status'];    
                        $servingStatusArr = ['enabled'=>'CAMPAIGN_STATUS_ENABLED', 'paused'=>'CAMPAIGN_PAUSED', 'archived'=> 'CAMPAIGN_ARCHIVED'];
                        $row_data['adgroup']['servingStatus'] = $servingStatusArr[$row['Campaign Status']];
                        $row_data['adgroup']['lastUpdatedDate'] = time(); 

                        $adgroup_data[]= $row_data['adgroup'];


                        $row_data['adgroupreport'] = [];

                        $row_data['adgroupreport']['seller_id'] = $seller_id;   
                        $row_data['adgroupreport']['country'] = $country;
                        $row_data['adgroupreport']['date'] = $report_date;
                        $row_data['adgroupreport']['currency'] = config ( 'app.countriesCurrency' )[$country];
                        $row_data['adgroupreport']['exchange_rate'] = CurrencyHelper::get_exchange_rate($report_date, $row_data['adgroupreport']['currency'], 'USD');
                                      
                        $row_data['adgroupreport']['campaignName'] = $row['Campaign'];
                        $row_data['adgroupreport']['campaignId'] = $row['Campaign ID'];
                        $row_data['adgroupreport']['adGroupName'] = $row['Ad Group']; 
                        $row_data['adgroupreport']['adGroupId'] = $row['Record ID'];

                        if($method == "fix" || $method == "plus") {
                            $row_data['adgroupreport']['impressions'] = $row['Impressions'];
                            $row_data['adgroupreport']['clicks'] = $row['Clicks'];
                            $row_data['adgroupreport']['cost'] = $row['Spend'];
                            $row_data['adgroupreport']['attributedSales1d'] = $row['Sales'];
                            $row_data['adgroupreport']['attributedConversions1d'] = $row['Orders'];
                            $row_data['adgroupreport']['attributedUnitsOrdered1d'] = $country == "UK" ? $row['Total units'] : $row['Total Units'];
                        } elseif($method == "minus") {
                            $row_data['adgroupreport']['impressions_diff'] = $row['Impressions'];
                            $row_data['adgroupreport']['clicks_diff'] = $row['Clicks'];
                            $row_data['adgroupreport']['cost_diff'] = $row['Spend'];
                            $row_data['adgroupreport']['attributedSales1d_diff'] = $row['Sales'];
                            $row_data['adgroupreport']['attributedConversions1d_diff'] = $row['Orders'];
                            $row_data['adgroupreport']['attributedUnitsOrdered1d_diff'] = $country == "UK" ? $row['Total units'] : $row['Total Units'];
                        }     

                        $adgroupreport_data[]= $row_data['adgroupreport'];                      
                }

           

                if ( $row['Record Type'] == "Keyword" ) { //3 ad_keywords
                    
                        $row_data['adkeyword'] = [];

                        $row_data['adkeyword']['seller_id'] = $seller_id;   
                        $row_data['adkeyword']['country'] = $country;
                                      
                        $row_data['adkeyword']['keywordId'] = $row['Record ID'];
                        $row_data['adkeyword']['campaignId'] = $row['Campaign ID'];

                        if($row['Ad Group']){
                            $adgroup = AdGroup::where('seller_id', $seller_id)->where('campaignId', $row['Campaign ID'])->where('name', trim($row['Ad Group']))->first();
                            $adGroupId = $adgroup['adGroupId'];
                            $maxbid = $adgroup['maxbid'];
                        }else{
                            $adGroupId = null;
                            $maxbid = null;
                        }          

                        $row_data['adkeyword']['adGroupId'] = $adGroupId;
                        $row_data['adkeyword']['keywordText'] = $row['Keyword or Product Targeting'];   
                        $row_data['adkeyword']['state'] = $row['Status'];
                        $row_data['adkeyword']['bid'] = $maxbid;

                        $servingStatusArr = ['enabled'=>'CAMPAIGN_STATUS_ENABLED', 'paused'=>'CAMPAIGN_PAUSED', 'archived'=> 'CAMPAIGN_ARCHIVED'];
                        $row_data['adkeyword']['servingStatus'] = $servingStatusArr[$row['Campaign Status']];
                        $row_data['adkeyword']['lastUpdatedDate'] = time(); 

                        if($row['Match Type'] == "campaign negative exact" || $row['Match Type'] == "negative exact"){
                            $matchtype = 'negativeExact';
                        }else if($row['Match Type'] == "campaign negative phrase" || $row['Match Type'] == "negative phrase"){
                            $matchtype = 'negativePhrase';
                        }else if($row['Match Type'] == "broad"){
                            $matchtype = 'broad';
                        }else if($row['Match Type'] == "phrase"){
                            $matchtype = 'phrase';
                        }else if($row['Match Type'] == "exact"){
                            $matchtype = 'exact';
                        }else{
                            $matchtype = '';
                        }

                        $row_data['adkeyword']['matchType'] = $matchtype;
                        $adkeyword_data[] = $row_data['adkeyword'];

                    

                        $row_data['adkeywordsreport'] = [];

                        $row_data['adkeywordsreport']['seller_id'] = $seller_id;   
                        $row_data['adkeywordsreport']['country'] = $country;

                        $row_data['adkeywordsreport']['adGroupId'] = $adGroupId;
                        $row_data['adkeywordsreport']['keywordId'] = $row['Record ID'];
                                        
                        $row_data['adkeywordsreport']['seller_id'] = $seller_id;   
                        $row_data['adkeywordsreport']['country'] = $country;
                        $row_data['adkeywordsreport']['date'] = $report_date;
                        $row_data['adkeywordsreport']['currency'] = config ( 'app.countriesCurrency' )[$country];
                        $row_data['adkeywordsreport']['exchange_rate'] = CurrencyHelper::get_exchange_rate($report_date, $row_data['adkeywordsreport']['currency'], 'USD');
                                      
                        $row_data['adkeywordsreport']['campaignName'] = $row['Campaign'];
                        $row_data['adkeywordsreport']['campaignId'] = $row['Campaign ID'];
                        $row_data['adkeywordsreport']['keywordText'] = $row['Keyword or Product Targeting'];
                        $row_data['adkeywordsreport']['matchType'] = strtoupper($matchtype);
                        $row_data['adkeywordsreport']['adGroupName'] = $row['Ad Group']; 

                        if($method == "fix" || $method == "plus") {
                            $row_data['adkeywordsreport']['impressions'] = $row['Impressions'];
                            $row_data['adkeywordsreport']['clicks'] = $row['Clicks'];
                            $row_data['adkeywordsreport']['cost'] = $row['Spend'];
                            $row_data['adkeywordsreport']['attributedSales1d'] = $row['Sales'];
                            $row_data['adkeywordsreport']['attributedConversions1d'] = $row['Orders'];
                            $row_data['adkeywordsreport']['attributedUnitsOrdered1d'] = $country == "UK" ? $row['Total units'] : $row['Total Units'];
                        } elseif($method == "minus") {
                            $row_data['adkeywordsreport']['impressions_diff'] = $row['Impressions'];
                            $row_data['adkeywordsreport']['clicks_diff'] = $row['Clicks'];
                            $row_data['adkeywordsreport']['cost_diff'] = $row['Spend'];
                            $row_data['adkeywordsreport']['attributedSales1d_diff'] = $row['Sales'];
                            $row_data['adkeywordsreport']['attributedConversions1d_diff'] = $row['Orders'];
                            $row_data['adkeywordsreport']['attributedUnitsOrdered1d_diff'] = $country == "UK" ? $row['Total units'] : $row['Total Units'];
                        }

                        $adkeywordsreport_data[] = $row_data['adkeywordsreport'];

                }


                if ( $row['Record Type'] == "Ad" ) { 

                        $row_data['productad'] = [];

                        $row_data['productad']['seller_id'] = $seller_id;   
                        $row_data['productad']['country'] = $country;
                                      
                        $row_data['productad']['adId'] = $row['Record ID'];
                        $row_data['productad']['campaignId'] = $row['Campaign ID'];
                        $row_data['productad']['sku'] = $row['SKU'];

                        if($row['Ad Group']){
                            $adgroup = AdGroup::where('seller_id', $seller_id)->where('campaignId', $row['Campaign ID'])->where('name', trim($row['Ad Group']))->first();
                            $adGroupId = $adgroup['adGroupId'];
                            $maxbid = $adgroup['maxbid'];
                        }else{
                            $adGroupId = null;
                            $maxbid = null;
                        }          
                        
                        $row_data['productad']['adGroupId'] = $adGroupId;
                        $row_data['productad']['state'] = $row['Campaign Status'];
                        $servingStatusArr = ['enabled'=>'CAMPAIGN_STATUS_ENABLED', 'paused'=>'CAMPAIGN_PAUSED', 'archived'=> 'CAMPAIGN_ARCHIVED'];
                        $row_data['productad']['servingStatus'] = $servingStatusArr[$row['Campaign Status']];
                        $row_data['productad']['lastUpdatedDate'] = time();   

                        $productad_data[]= $row_data['productad'];
                    
                    try { // ad_reports

                        $adreport = AdReport::where('seller_id', $seller_id)->where('date', $report_date)->where('campaignName', $row['Campaign'])->where('adGroupName', $row['Ad Group'])->first();

                        if($adreport !== null) {

                            $adreport->adId = $row['Record ID'];
                            if($row['Ad Group']){
                                $adgroup = AdGroup::where('seller_id', $seller_id)->where('campaignId', $row['Campaign ID'])->where('name', trim($row['Ad Group']))->first();
                                $adGroupId = $adgroup['adGroupId'];
                            }else{
                                $adGroupId = null;
                            }          
                            $adreport->adGroupId = $adGroupId;
                            $adreport->campaignId = $row['Campaign ID'];
                                            
                            $adreport->save();
                        }
                    
                    } catch (\Exception $e) {
                        echo 'Caught exception: ',  $e->getMessage(), "\n";
                    }                

                }

            }   catch (\Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }    

        }


        try {
            AdCampaign::insertOrUpdate($adcampaign_data);
            AdCampaignsReport::insertOrUpdate($adcampaignreports_data);
            AdGroup::insertOrUpdate($adgroup_data);
            AdGroupsReport::insertOrUpdate($adgroupreport_data);
            AdKeyword::insertOrUpdate($adkeyword_data);
            AdKeywordsReport::insertOrUpdate($adkeywordsreport_data);         
            ProductAd::insertOrUpdate($productad_data);    
        } catch( \Exception $e ) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }           


    }
}
