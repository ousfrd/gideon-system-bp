<?php

namespace App\Jobs;

use App\Import;
use App\Order;
use App\OrderItem;
use App\FinanceEvent;
use App\OtherFee;
use App\SellerCoupon;
use App\SellerDeal;
use App\Account;
use App\ProductStatistic;
use Storage;
use App\Helpers\CurrencyHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportFinances implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $import;

    public $timeout = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Import $import)
    {
        $this->import = $import;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(!Storage::disk('local')->exists($this->import->filename)) {
            Storage::put($this->import->filename, Storage::disk('dropbox')->get($this->import->filename));
        }

        $csv_data = array_map('str_getcsv', file(storage_path('app/'.$this->import->filename)));

        $seller_id = $this->import->seller_id;
        $country = $this->import->country;

        $start = false;
        $prev_row = array();

        foreach ($csv_data as $key => $row) {
            if($row[0] == "date/time") {
                $header = $row;
                $start = true;
            }

            if(!$start || $row[0] == "date/time") {
                continue;
            }

            // echo "Start import row " . $key . "\n";

            try {
                $rowdata = [];

                foreach($row as $rowkey => $value) {
                    $rowdata[$header[$rowkey]] = $value;
                }

                if (empty($fromPurchaseDate)) {
                    $fromPurchaseDate = $rowdata['date/time'];
                }
                else {
                    if ($fromPurchaseDate > $rowdata['date/time']) {
                        $fromPurchaseDate = $rowdata['date/time'];
                    }
                }
                if (empty($toPurchaseDate)) {
                    $toPurchaseDate = $rowdata['date/time'];
                } else {
                    if ($toPurchaseDate < $rowdata['date/time']) {
                        $toPurchaseDate = $rowdata['date/time'];
                    }
                }

                if($rowdata['type'] == "Order") {
                
                // ask what is this
                    if (array_key_exists("Marketplace Facilitator Tax", $rowdata)) {
                        $marketplace_facilitator_tax = $rowdata['Marketplace Facilitator Tax'];
                    }
                    
                    $orderitem = OrderItem::where('AmazonOrderId', $rowdata['order id'])->where('SellerSKU', $rowdata['sku'])->where('seller_id', $seller_id)->first();

                    if($orderitem === null) {
                        continue;
                    }

                    if($prev_row && $prev_row['order id'] == $rowdata['order id'] && $prev_row['sku'] == $rowdata['sku']) {

                        $orderitem->QuantityOrdered += (int)$rowdata['quantity'];
                        $orderitem->ItemPrice += $rowdata['product sales'] ? (float)$rowdata['product sales'] : 0;
                        if (array_key_exists("shipping credits", $rowdata)) {
                            $orderitem->ShippingPrice += $rowdata['shipping credits'] ? (float)$rowdata['shipping credits'] : 0;
                        }
                        if (array_key_exists("postage credits", $rowdata)) {
                            $orderitem->ShippingPrice += $rowdata['postage credits'] ? (float)$rowdata['postage credits'] : 0;
                        }
                        $orderitem->giftwrap_fee += (float)$rowdata['gift wrap credits'];
                        $orderitem->total_promo += (float)$rowdata['promotional rebates'];
                        if (array_key_exists("sales tax collected", $rowdata)) {
                            $orderitem->tax = (float)$rowdata['sales tax collected'];
                        }else if (array_key_exists("product sales tax", $rowdata)) {
                            $orderitem->tax = (float)$rowdata['product sales tax'];
                        }
                        if (array_key_exists("Marketplace Facilitator Tax", $rowdata)) {
                            $orderitem->tax += (float)$rowdata['Marketplace Facilitator Tax'];
                        }
                        $orderitem->other_fee += (float)$rowdata['other transaction fees'];

                        $orderitem->fba_fee += (float)$rowdata['fba fees'];
                        $orderitem->commission += (float)$rowdata['selling fees'];   

                    } else {
                        $orderitem->QuantityOrdered = (int)$rowdata['quantity'];
                        $orderitem->ItemPrice = $rowdata['product sales'] ? (float)$rowdata['product sales'] : 0;
                        if (array_key_exists("shipping credits", $rowdata)) {
                            $orderitem->ShippingPrice = $rowdata['shipping credits'] ? (float)$rowdata['shipping credits'] : 0;
                        }
                        if (array_key_exists("postage credits", $rowdata)) {
                            $orderitem->ShippingPrice = $rowdata['postage credits'] ? (float)$rowdata['postage credits'] : 0;
                        }
                        $orderitem->giftwrap_fee = (float)$rowdata['gift wrap credits'];
                        $orderitem->total_promo = (float)$rowdata['promotional rebates'];
                        if (array_key_exists("sales tax collected", $rowdata)) {
                            $orderitem->tax = (float)$rowdata['sales tax collected'];
                        }else if (array_key_exists("product sales tax", $rowdata)) {
                            $orderitem->tax = (float)$rowdata['product sales tax'];
                        }
                        if (array_key_exists("Marketplace Facilitator Tax", $rowdata)) {
                            $orderitem->tax = (float)$rowdata['Marketplace Facilitator Tax'];
                        }
                        $orderitem->other_fee = (float)$rowdata['other transaction fees'];

                        $orderitem->fba_fee = (float)$rowdata['fba fees'];
                        $orderitem->commission = (float)$rowdata['selling fees'];                            
                    }

                    $orderitem->save();
                    $prev_row = $rowdata;
                }

                if($rowdata['type'] == "Refund") {
                    $orderitem = OrderItem::where('AmazonOrderId', $rowdata['order id'])->where('SellerSKU', $rowdata['sku'])->where('seller_id', $seller_id)->first();           
                    
                    if($orderitem === null) {
                        continue;
                    }

                    $orderitem->total_refund = (float)$rowdata['total'];
                    $orderitem->refund_item_quantity = $rowdata['quantity'];
                    $orderitem->save();  

                    try {
                        $finance_event = FinanceEvent::where('amazon_order_id', $rowdata['order id'])->where('sku', $rowdata['sku'])->where('type', 'Principal')->where('group', 'Refund')->first();
                        if($finance_event === null) {
                            $finance_event = new FinanceEvent();
                            $finance_event->amazon_order_id = $rowdata['order id'];
                            $finance_event->sku = $rowdata['sku'];
                            $finance_event->type = "Principal";
                            $finance_event->group = "Refund";
                            $finance_event->currency = config ( 'app.countriesCurrency' )[$country];
                        }

                        $finance_event->posted_date = date("Y-m-d H:i:s", strtotime($rowdata['date/time']));
                        $finance_event->amount = (float)$rowdata['total'];
                        $finance_event->save();
                    } catch (\Exception $e) {
                        echo 'Caught exception: ',  $e->getMessage(), "\n";
                        throw $e;
                    }
                }

                if($rowdata['type'] == "FBA Inventory Fee" || $rowdata['type'] == "Adjustment" || ($rowdata['type'] == "Service Fee" && $rowdata['description'] != "Cost of Advertising")) {
                    $posted_date = date("Y-m-d H:i:s", strtotime($rowdata['date/time']));
                    $fee_reason = $rowdata['type'];
                    $fee_type = $rowdata['description'];
                    $fee_amount = (float)$rowdata['total'];

                    $other_fee = OtherFee::where('seller_id', $seller_id)->where('posted_date', $posted_date)->where('fee_reason', $fee_reason)->where('fee_amount', $fee_amount)->first();
                    
                    if($other_fee === null) {
                        $other_fee = new OtherFee();
                        $other_fee->seller_id = $seller_id;
                        $other_fee->posted_date = $posted_date;
                        $other_fee->fee_reason = $fee_reason;
                        $other_fee->fee_type = $fee_type;
                        $other_fee->fee_amount = $fee_amount;
                        
                        $other_fee->currency = config ( 'app.countriesCurrency' )[$country];
                        $other_fee->exchange_rate = CurrencyHelper::get_exchange_rate(date("Y-m-d", strtotime($rowdata['date/time'])), $other_fee->currency, 'USD');
                        $other_fee->save();
                    }
                }

                if(empty($rowdata['type']) && $this->is_coupon_id($rowdata['order id'])) {
                    $coupon = SellerCoupon::where('coupon_id', $rowdata['order id'])->first();  
                    if ($coupon === NULL) {
                        $coupon = new SellerCoupon();
                        $coupon->seller_id = $seller_id;
                        $coupon->coupon_id = $rowdata['order id'];
                        $coupon->coupon_description = $rowdata['description'];
                        $coupon->posted_date = date("Y-m-d H:i:s", strtotime($rowdata['date/time']));
                        $coupon->fee_type = "CouponRedemptionFee";
                        $coupon->fee_amount = (float)$rowdata['other transaction fees'];
                        $coupon->redemption_count = (int)$rowdata['quantity'];
                        $coupon->currency = config ( 'app.countriesCurrency' )[$country];
                        $coupon->exchange_rate = CurrencyHelper::get_exchange_rate(date("Y-m-d", strtotime($rowdata['date/time'])), $coupon->currency, 'USD');
                        $coupon->total_amount = (float)$rowdata['total'];
                        $coupon->save();
                    } else {
                        if ($coupon->seller_id != $seller_id) {
                            $coupon->seller_id = $seller_id;
                            $coupon->save();
                        }
                    }
                }

                if($rowdata['type'] == "Lightning Deal Fee") {
                    $deal = SellerDeal::where('deal_id', $rowdata['order id'])->where('seller_id', $seller_id)->first();  
                    if($deal === null) {
                        $deal = new SellerDeal();
                        $deal->seller_id = $seller_id;
                        $deal->deal_id = $rowdata['order id'];
                        $deal->deal_description = $rowdata['description'];
                        $deal->posted_date = date("Y-m-d H:i:s", strtotime($rowdata['date/time']));
                        $deal->event_type = "SellerDealPayment";
                        $deal->fee_type = "RunLightningDealFee";
                        $deal->fee_amount = (float)$rowdata['other transaction fees'];
                        $deal->currency = config ( 'app.countriesCurrency' )[$country];
                        $deal->exchange_rate = CurrencyHelper::get_exchange_rate(date("Y-m-d", strtotime($rowdata['date/time'])), $deal->currency, 'USD');
                        $deal->total_amount = (float)$rowdata['total'];
                        $deal->save();
                    }
                }

                // echo "Saved row #" . $key . ", sku: " . $rowdata['sku'] . "\n";
            } catch (\Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
                throw $e;
            }

        }

        echo "Finish importing";

        // if (isset($fromPurchaseDate) && isset($toPurchaseDate)) {
        //     if ($fromPurchaseDate && $toPurchaseDate && $fromPurchaseDate <= $toPurchaseDate) {
        //         $seller = Account::where('seller_id', $seller_id)->first();
        //         ProductStatistic::buildStatisticsForSeller($seller, $fromPurchaseDate, $toPurchaseDate);
        //     }
        // }

        $this->import->status = "imported";
        $this->import->save();
    }

    public function is_coupon_id($id) {
        $parts = explode("-", $id);
        return count($parts) == 5 && strlen($parts[0]) == 8 && strlen($parts[1]) == 4;
    }
}
