<?php

namespace App\Jobs;

use App\Import;
use App\Order;
use App\OrderItem;
use App\FinanceEvent;
use App\OtherFee;
use App\SellerCoupon;
use App\SellerDeal;
use App\ProductDailyProfit;
use Storage;
use DB;
use App\Helpers\CurrencyHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportBusiness implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $import;

    public $timeout = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Import $import)
    {
        $this->import = $import;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(!Storage::disk('local')->exists($this->import->filename)) {
            Storage::put($this->import->filename, Storage::disk('dropbox')->get($this->import->filename));
        }

        $csv_data = array_map('str_getcsv', file(storage_path('app/'.$this->import->filename)));

        $seller_id = $this->import->seller_id;
        $country = $this->import->country;
        $report_date = $this->import->report_date;

        $start = false;
        
        foreach ($csv_data as $key => $row) {

            if($row[2] == "Title") {
                $header = $row;
                $start = true;
             }

            if(!$start || $row[2] == "Title") {
                 continue;
            }

          //  echo "Start import row " . $key . "\n";

            try {
                $rowdata = [];
                foreach($row as $rowkey => $value) {
                    $rowdata[$header[$rowkey]] = $value;
                }
                
            } catch (\Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }

            $product_daily_profit = ProductDailyProfit::where('asin', $rowdata['(Child) ASIN'])->where('date', $report_date)->where('seller_id', $seller_id)->where('country', $country)->first();
                    
            if($product_daily_profit === null) {
                continue;
            }

            $product_daily_profit->sessions = (float) str_replace(',', '', $rowdata['Sessions']);
            $product_daily_profit->page_views = (float) str_replace(',', '', $rowdata['Page Views']);

            $product_daily_profit->save();

        }
        
        echo "Finish importing";

        $this->import->status = "imported";
        $this->import->save();

    }

}
