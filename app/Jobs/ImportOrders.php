<?php

namespace App\Jobs;

use App\Import;
use App\Order;
use App\OrderItem;
use App\Product;
use App\ManagedReviewOrder;
use App\Account;
use App\ProductStatistic;
use App\Helpers\OrderReportImporter;

use Storage;
use App\Helpers\CurrencyHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportOrders implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $import;

    public $timeout = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Import $import)
    {
        $this->import = $import;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (TRUE) {
            $sellerId = $this->import->seller_id;
            $content = Storage::disk('dropbox')->get($this->import->filename);
            OrderReportImporter::importOrderReportFromString($content, $sellerId);

            $this->import->status = "imported";
            $this->import->save();

            return;
        }
        $lines = explode(PHP_EOL, Storage::disk('dropbox')->get($this->import->filename));
        $csv_data = array();
        foreach ($lines as $k => $line) {
            if($k == 0) {
                $line = trim(str_replace("\xEF\xBB\xBF",'',$line));
            }
            $csv_data[] = explode("\t", $line);
        }

        $seller_id = $this->import->seller_id;

        $header = $csv_data[0];
        $fromPurchaseDate = $toPurchaseDate = null;
        foreach ($csv_data as $key => $row) {
            if($key == 0) {
                continue;
            }

            // echo "Start import row " . $key . "\n";
            
            try {

                $rowdata = [];
                foreach($row as $rowkey => $value) {
                    $rowdata[$header[$rowkey]] = $value;
                }


                if(empty($rowdata['amazon-order-id']) || strlen($rowdata['amazon-order-id']) != 19) {
                    continue;
                }

                if($rowdata['sales-channel'] == "Non-Amazon") {
                    continue;
                }

                // don't import self created orders
                if ( !array_key_exists(strtolower($rowdata['sales-channel']), config ('app.marketplaceCountry')) ) {
                    continue;
                }

                // 006 US FBM are dropshipping orders.
                if ($rowdata['fulfillment-channel'] != "Amazon" && $seller_id == "A3FL3YBXJ4JWS" && strtolower($rowdata['sales-channel']) == 'amazon.com') {
                    continue;
                }

                if (empty($fromPurchaseDate)) {
                    $fromPurchaseDate = $rowdata['purchase-date'];
                }
                else {
                    if ($fromPurchaseDate > $rowdata['purchase-date']) {
                        $fromPurchaseDate = $rowdata['purchase-date'];
                    }
                }
                if (empty($toPurchaseDate)) {
                    $toPurchaseDate = $rowdata['purchase-date'];
                } else {
                    if ($toPurchaseDate < $rowdata['purchase-date']) {
                        $toPurchaseDate = $rowdata['purchase-date'];
                    }
                }

                $order = Order::where('AmazonOrderId', $rowdata['amazon-order-id'])->where('seller_id', $seller_id)->first();
                if($order === null) {
                    $order = new Order();
                    $order->seller_id = $seller_id;
                    $order->AmazonOrderId = $rowdata['amazon-order-id'];
                }
                $order->OrderStatus = $rowdata['order-status'];
                $order->PurchaseDate = $rowdata['purchase-date'];
                $order->SalesChannel = $rowdata['sales-channel'];
                $order->FulfillmentChannel = $rowdata['fulfillment-channel'] == "Amazon" ? "AFN" : "MFN";

                $order->save();

                $orderitem = OrderItem::where('AmazonOrderId', $rowdata['amazon-order-id'])->where('SellerSKU', $rowdata['sku'])->where('seller_id', $seller_id)->first();
                if($orderitem === null) {
                    $orderitem = new OrderItem();
                    $orderitem->seller_id = $seller_id;
                    $orderitem->AmazonOrderId = $rowdata['amazon-order-id'];
                    $orderitem->SellerSKU = $rowdata['sku'];
                }
                $orderitem->ASIN = $rowdata['asin'];
                $orderitem->Title = $rowdata['product-name'];
                $orderitem->QuantityOrdered = $rowdata['quantity'];
                $orderitem->Currency = $rowdata['currency'];
                $orderitem->ItemPrice = $rowdata['item-price'] ? (float)$rowdata['item-price'] : 0;
                $orderitem->ShippingPrice = $rowdata['shipping-price'] ? (float)$rowdata['shipping-price'] : 0;
                $orderitem->giftwrap_fee = (float)$rowdata['gift-wrap-price'];
                $orderitem->tax = (float)$rowdata['item-tax'] + (float)$rowdata['shipping-tax'] + (float)$rowdata['gift-wrap-tax']; 
                $orderitem->item_promo = (float)$rowdata['item-promotion-discount'];
                $orderitem->shipping_promo = (float)$rowdata['ship-promotion-discount'];
                $orderitem->total_promo = -1 * ($orderitem->item_promo + $orderitem->shipping_promo);

                // estimate commission and fba fee
                if($rowdata['order-status'] == "Pending") {
                    $orderitem->commission = -1 * round($orderitem->ItemPrice * 0.15, 2);
                    $orderitem->fba_fee = -1 * 2.99 * $orderitem->QuantityOrdered;                    
                }


                $time = strtotime($rowdata['purchase-date']);
                $date = date('Y-m-d',$time);
                $orderitem->exchange_rate = CurrencyHelper::get_exchange_rate($date, $rowdata['currency'], 'USD');
                $orderitem->ItemPriceUSD = $orderitem->ItemPrice * $orderitem->exchange_rate;
                $orderitem->ShippingPriceUSD = $orderitem->ShippingPrice * $orderitem->exchange_rate;
                $orderitem->country = config ( 'app.marketplaceCountry' )[strtolower($rowdata['sales-channel'])];
                $orderitem->region = config('app.countriesRegion')[config ( 'app.marketplaceCountry' )[strtolower($rowdata['sales-channel'])]];
                
                if($orderitem->region == "UK") {
                    $orderitem->vat_fee = ($orderitem->ShippingPriceUSD + $orderitem->ItemPriceUSD) * config('app.vat_fee_rate') * -1;
                }

                $product = Product::where('asin', $rowdata['asin'])->where('country', $orderitem->country)->first();
                if($product !== null) {
                    $orderitem->cost = $product->cost;
                    $orderitem->shipping_cost = $product->shipping_cost;                   
                }


                $orderitem->save();

                 // echo "Saved row #" . $key . ", order-id: " . $rowdata['amazon-order-id'] . "\n";
            } catch (\Exception $e) {
                throw $e;
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }
        ManagedReviewOrder::syncSelfOrdersWithOrderItem();
        echo "Finish importing";
        
        // if ($fromPurchaseDate && $toPurchaseDate && $fromPurchaseDate <= $toPurchaseDate) {
        //     $seller = Account::where('seller_id', $seller_id)->first();
        //     ProductStatistic::buildStatisticsForSeller($seller, $fromPurchaseDate, $toPurchaseDate);
        // }
        $this->import->status = "imported";
        $this->import->save();
    }
}
