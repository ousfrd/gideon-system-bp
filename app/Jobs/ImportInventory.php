<?php

namespace App\Jobs;

use Storage;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Import;
use App\Inventory;
use App\Helpers\InventoryReportImporter;


class ImportInventory implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $import;

    public $timeout = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Import $import)
    {
        $this->import = $import;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sellerId = $this->import->seller_id;
        $country = $this->import->country;

        // remove seller inventory product id and listing id
        Inventory::where([
            ['country', '=', $country],
            ['seller_id', '=', $sellerId]
        ])->update(['product_id' => null, 'listing_id' => null, 'status' => 0]);

        $content = Storage::disk('dropbox')->get($this->import->filename);
        InventoryReportImporter::importInventoryReportFromString($content, $sellerId, $country);

        $this->import->status = "imported";
        $this->import->save();
    }
}
