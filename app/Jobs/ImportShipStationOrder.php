<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Services\ShipStationOrderImportService;


class ImportShipStationOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $createDateStart;

    /**
     * Create a new job instance.
     *
     * @param String $createDateStart
     * @return void
     */
    public function __construct($createDateStart = NULL)
    {
        $this->createDateStart = $createDateStart;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $importService = resolve(ShipStationOrderImportService::class);
        $importService->refreshStores();
        $importService->importOrders($this->createDateStart);
    }
}
