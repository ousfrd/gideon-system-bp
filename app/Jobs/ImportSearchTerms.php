<?php

namespace App\Jobs;

use App\Import;
use App\Product;
use App\Listing;
use App\Account;
use App\AdReport;
use App\AdCampaign;
use App\AdSearchtermsReport;
use App\AdGroup;
use App\AdKeyword;


use Rap2hpoutre\FastExcel\FastExcel;
use Storage;
use App\Helpers\CurrencyHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportSearchTerms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $import;

    public $timeout = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Import $import)
    {
        $this->import = $import;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if(!Storage::disk('local')->exists($this->import->filename)) {
            Storage::put($this->import->filename, Storage::disk('dropbox')->get($this->import->filename));
        }

        $items = (new FastExcel)->import(storage_path('app/'.$this->import->filename));

        $seller_id = $this->import->seller_id;
        $country = $this->import->country;

        foreach ($items as $key => $row) {

            try {


                //campaignId
                $adcampaign = AdCampaign::where('seller_id', $seller_id)->where('country', $country)->where('name', $row['Campaign Name'])->first();                

                //adGroupId
                $adgroup = AdGroup::where('seller_id', $seller_id)->where('campaignId', $adcampaign['campaignId'])->where('country', $country)->where('name', $row['Ad Group Name'])->first();

                //keywordId
                $adkeyword = AdKeyword::where('seller_id', $seller_id)->where('campaignId', $adcampaign['campaignId'])->where('adGroupId', $adgroup['adGroupId'])->where('keywordText', $row['Targeting'])->where('matchType', strtolower($row['Match Type']))->first();
                
                if(trim($row['Targeting']) == "*"){

                    $keywordId = 1;

                }else{

                    $keywordId = $adkeyword['keywordId'];
                }

                $searchterm = AdSearchtermsReport::where('seller_id', $seller_id)->where('date', $row['Date']->format('Y-m-d'))->where('query', $row['Customer Search Term'])->where('keywordId', $keywordId)->first();


                if(!$searchterm['id']) {
                    $searchterm = new AdSearchtermsReport();
                }

                if(trim($row['Targeting']) == "*"){

                    $searchterm->keywordText = null;
                    $searchterm->matchType = null;

                }else{
                    $searchterm->keywordText = $row['Targeting'];
                    $searchterm->matchType = $row['Match Type'];
                }

                $searchterm->seller_id = $seller_id;   
                $searchterm->country = $country;

                $searchterm->keywordId = $keywordId;
                $searchterm->campaignId = $adcampaign['campaignId'];
                $searchterm->adGroupId = $adgroup['adGroupId'];                

                $searchterm->date = $row['Date']->format('Y-m-d');
                $searchterm->currency = $row['Currency'];
                $searchterm->campaignName = $row['Campaign Name'];
                $searchterm->adGroupName = $row['Ad Group Name'];                
                $searchterm->query = $row['Customer Search Term'];
                
                $searchterm->exchange_rate = CurrencyHelper::get_exchange_rate($searchterm->date, $row['Currency'], 'USD');                
                $searchterm->impressions = $row['Impressions'];
                $searchterm->clicks = $row['Clicks'];
                $searchterm->cost = $row['Spend'];
                $searchterm->attributedConversions7d = $row['7 Day Total Orders (#)'];  
                $searchterm->attributedUnitsOrdered7d = $row['7 Day Total Units (#)'];
                if (array_key_exists("7 Day Total Sales ($)", $row)) {
                    $searchterm->attributedSales7d = $row['7 Day Total Sales ($)'];
                }
                if (array_key_exists("7 Day Total Sales ", $row)) {
                    $searchterm->attributedSales7d = $row['7 Day Total Sales '];
                }
                if (array_key_exists("7 Day Advertised SKU Sales ($)", $row)) {
                    $searchterm->attributedSales7dSameSKU = $row['7 Day Advertised SKU Sales ($)'];
                }
                if (array_key_exists("7 Day Advertised SKU Sales ", $row)) {
                    $searchterm->attributedSales7dSameSKU = $row['7 Day Advertised SKU Sales '];
                }
               
                $searchterm->save();
            
            } catch (\Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }


        }

        echo "Finish importing";

        $this->import->status = "imported";
        $this->import->save();

    }
}
