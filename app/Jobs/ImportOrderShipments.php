<?php

namespace App\Jobs;

use App\Import;
use App\OrderItemMarketing;
use App\ManagedReviewOrder;
use App\Helpers\OrderShipmentReportImporter;

use Storage;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class ImportOrderShipments implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $import;

    public $timeout = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Import $import)
    {
        $this->import = $import;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sellerId = $this->import->seller_id;
        $content = Storage::disk('dropbox')->get($this->import->filename);

        OrderShipmentReportImporter::importOrderShipmentReportFromString($content, $sellerId);

        ManagedReviewOrder::syncSelfOrders();
        OrderItemMarketing::syncSelfBoughtOrder();

        $this->import->status = "imported";
        $this->import->save();
    }
}
