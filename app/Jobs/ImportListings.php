<?php

namespace App\Jobs;

use App\Import;
use App\Product;
use App\Listing;
use App\Account;
use App\Shipment;
use Storage;
use App\Helpers\CurrencyHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use App\Helpers\ListingReportImporter;

class ImportListings implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $import;

    public $timeout = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Import $import)
    {
        $this->import = $import;
    }

    private function get_body_lines($body_str) {
        $lines = [];
        while(true) {
            $next_active = strpos($body_str,"\tActive");
            $next_inactive = strpos($body_str,"\tInactive");

            $next_active = $next_active ? $next_active + strlen("\tActive") : 99999;
            $next_inactive = $next_inactive ? $next_inactive + strlen("\tInactive") : 99999;

            if($next_active == 99999 && $next_inactive == 99999) {
                break;
            } else {
                $next_pos = $next_active <= $next_inactive ? $next_active : $next_inactive;

                $lines[] = substr($body_str, 0, $next_pos);
                $body_str = substr($body_str, $next_pos);

                continue;
            }
        }

        return $lines;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $content = Storage::disk('dropbox')->get($this->import->filename);
        if (TRUE) {
            ListingReportImporter::importListingReportFromString(
                $content, $this->import->seller_id, $this->import->country);
        } else {
            $pos = strpos($content, "status") + strlen("status");
            $header_str = substr($content, 0, $pos);
            $body_str = substr($content, $pos);
            $body_lines = $this->get_body_lines($body_str);

            $products = [];
            $listingIds = [];
            $country = $this->import->country;

            $csv_data = array();
            foreach ($body_lines as $k => $line) {
                $csv_data[] = explode("\t", $line);
            }

            $seller_id = $this->import->seller_id;      
            
            $header = explode("\t", trim(str_replace("\xEF\xBB\xBF",'',$header_str)));
            foreach ($csv_data as $key => $row) {

                // echo "Start import row " . $key . "\n";

                try {
                    $rowdata = [];
                    foreach($row as $rowkey => $value) {
                        $rowdata[$header[$rowkey]] = $value;
                    }
                    $sku = trim($rowdata['seller-sku']);
                    $quantity = $rowdata['quantity'];
                    if (($quantity < 5) && (!empty($quantity))) {
                        continue;
                    }
                    $asin = $rowdata['asin1'];

                    if(empty($sku)) {
                        continue;
                    }

                    $realCountry = Shipment::getCountry($asin, $sku);
                    if ($realCountry) {
                        $country = $realCountry;
                    }

                    $account = Account::where('seller_id', $seller_id)->first();
                    $region = $account->region;
                    $product = Product::where('asin', $asin)->where('country', $country)->first();
                    if($product === null) {
                        $product = new Product();
                        $product->asin = $asin;
                        $product->earliest_listing_open_date = $rowdata['open-date'];
                    }
                    $product->country = $country;
                    $product->name = trim($rowdata['item-name']);
                    $product->save();
                    $listing = Listing::where('seller_id', $seller_id)->where('asin', $asin)->where('sku', $sku)->first();
                    if($listing === null) {
                        $listing = new Listing();
                        $listing->seller_id = $seller_id;
                        $listing->asin = $asin;
                        $listing->sku = $sku;
                    }
                    $listing->country = $country;
                    $listing->item_name = trim($rowdata['item-name']);
                    $listing->region = $region;
                    $listing->product_id = $product->id;
                    $listing->item_condition = (int)$rowdata['item-condition'];
                    $listing->fulfillment_channel = $rowdata['fulfillment-channel'] == 'DEFAULT' ? 'Merchant' : 'Amazon';
                    $listing->open_date = date('Y-m-d', strtotime($rowdata['open-date']));
                    $listing->price = (float)$rowdata['price'];
                    $listing->currency = config ( 'app.countriesCurrency' )[$country];
                    $date = date('Y-m-d');
                    $exchange_rate = CurrencyHelper::get_exchange_rate($date, $listing->currency, 'USD');
                    $listing->price_usd = $listing->price * $exchange_rate;
                    $listing->status = $rowdata['status'] == 'Active' ? 1 : 0;
                    $listing->updated_at = date("Y-m-d H:i:s");
                    $listing->save();

                    $products[] = $product;
                    $listingIds[] = $listing->id;
                     // echo "Saved row #" . $key . ", sku: " . $sku . "\n";
                 } catch (\Exception $e) {
                    throw $e;
                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                 }

            }

            // update product status
            foreach ($products as $product) {
                $product->updateStatus();
            }
            
            // listings not appear in the report are considered removed.
            Listing::where('seller_id', $seller_id)->where('country', $country)->whereNotIn('id', $listingIds)->update(['status' => -1]);
            
            echo "Finish importing";
        }
        
        $this->import->status = "imported";
        $this->import->save();
    }
}
