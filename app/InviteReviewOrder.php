<?php

namespace App;
use ManagedReviewOrder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class InviteReviewOrder extends ManagedReviewOrder
{
    protected $table = 'managed_review_orders';

    protected $attributes = [
        'owned_buyer_id' => 0,
        'owned_buyer_payment_method_id' => 0,
        'order_type' => ManagedReviewOrder::INVITE_REVIEW_ORDER
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order_type', function (Builder $builder) {
            $builder->where('order_type', ManagedReviewOrder::INVITE_REVIEW_ORDER);
        });
    }


}
