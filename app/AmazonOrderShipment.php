<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App;

use App\Helpers\Utils;


class AmazonOrderShipment {
	public $amazonOrderId;
	public $merchantOrderId;
	public $recipientName;
	public $shipAddress1;
	public $shipAddress2;
	public $shipAddress3;
	public $shipCity;
	public $shipState;
	public $shipPostalCode;
	public $shipCountry;
	public $sku;
	public $quantityShipped;
	public $shipmentId;
	public $shipmentItemId;
	public $amazonOrderItemId;
	public $merchantOrderItemId;
	public $purchaseDate;
	public $paymentsDate;
	public $shipmentDate;
	public $reportingDate;
	public $buyerEmail;
	public $buyerName;
	public $buyerPhoneNumber;
	public $productName;
	public $itemPrice;
	public $itemTax;
	public $shippingPrice;
	public $shippingTax;
	public $giftWrapPrice;
	public $giftWrapTax;
	public $shipPromotionDiscount;
	public $itemPromotionDiscount;
	public $shipServiceLevel;
	public $billAddress1;
	public $billAddress2;
	public $billAddress3;
	public $billCity;
	public $billState;
	public $billPostalCode;
	public $billCountry;
	public $carrier;
	public $trackingNumber;
	public $estimatedArrivalDate;
	public $fulfillmentCenterId;
	public $fulfillmentChannel;
	public $salesChannel;
	public $currency;

	protected static $propertyMapping = [
		'amazonOrderId' => ['amazon-order-id', 'Amazon Order Id'],
		'merchantOrderId' => ['merchant-order-id', 'Merchant Order Id'],
		'recipientName' => ['recipient-name', 'Recipient Name'],
		'shipAddress1' => ['ship-address-1', 'Shipping Address 1'],
		'shipAddress2' => ['ship-address-2', 'Shipping Address 2'],
		'shipAddress3' => ['ship-address-3', 'Shipping Address 3'],
		'shipCity' => ['ship-city', 'Shipping City'],
		'shipState' => ['ship-state', 'Shipping State'],
		'shipPostalCode' => ['ship-postal-code', 'Shipping Postal Code'],
		'shipCountry' => ['ship-country', 'Shipping Country Code'],
		'sku' => ['sku', 'Merchant SKU'],
		'quantityShipped' => ['quantity-shipped', 'Shipped Quantity'],
		'shipmentId' => ['shipment-id', 'Shipment ID'],
		'shipmentItemId' => ['shipment-item-id', 'Shipment Item Id'],
		'amazonOrderItemId' => ['amazon-order-item-id', 'Amazon Order Item Id'],
		'merchantOrderItemId' => ['merchant-order-item-id', 'Merchant Order Item Id'],
		'purchaseDate' => ['purchase-date', 'Purchase Date'],
		'paymentsDate' => ['payments-date', 'Payments Date'],
		'shipmentDate' => ['shipment-date', 'Shipment Date'],
		'reportingDate' => ['reporting-date', 'Reporting Date'],
		'buyerEmail' => ['buyer-email', 'Buyer Email'],
		'buyerName' => ['buyer-name', 'Buyer Name'],
		'buyerPhoneNumber' => ['buyer-phone-number', 'Buyer Phone Number'],
		'productName' => ['product-name', 'Title'],
		'itemPrice' => ['item-price', 'Item Price'],
		'itemTax' => ['item-tax', 'Item Tax'],
		'shippingPrice' => ['shipping-price', 'Shipping Price'],
		'shippingTax' => ['shipping-tax', 'Shipping Tax'],
		'giftWrapPrice' => ['gift-wrap-price', 'Gift Wrap Price'],
		'giftWrapTax' => ['gift-wrap-tax', 'Gift Wrap Tax'],
		'shipPromotionDiscount' => ['ship-promotion-discount', 'Shipment Promo Discount'],
		'itemPromotionDiscount' => ['item-promotion-discount', 'Item Promo Discount'],
		'shipServiceLevel' => ['ship-service-level', 'Ship Service Level'],
		'billAddress1' => ['bill-address-1', 'Billing Address 1'],
		'billAddress2' => ['bill-address-2', 'Billing Address 2'],
		'billAddress3' => ['bill-address-3', 'Billing Address 3'],
		'billCity' => ['bill-city', 'Billing City'],
		'billState' => ['bill-state', 'Billing State'],
		'billPostalCode' => ['bill-postal-code', 'Billing Postal Code'],
		'billCountry' => ['bill-country', 'Billing Country'],
		'carrier' => ['carrier', 'Carrier'],
		'trackingNumber' => ['tracking-number', 'Tracking Number'],
		'estimatedArrivalDate' => ['estimated-arrival-date', 'Estimated Arrival Date'],
		'fulfillmentCenterId' => ['fulfillment-center-id', 'FC'],
		'fulfillmentChannel' => ['fulfillment-channel', 'Fulfillment Channel'],
		'salesChannel' => ['sales-channel', 'Sales Channel'],
		'currency' => ['currency', 'Currency']
	];

	public static function fromOrderShipmentRecord($orderShipmentRecord) {
		$orderShipment = new self();
		foreach (self::$propertyMapping as $k => $v) {
			$orderShipment->$k = Utils::getValueByKeys($orderShipmentRecord, $v);
		}

		$orderShipment->quantityShipped = (int) $orderShipment->quantityShipped;
		$orderShipment->itemPrice = (float) $orderShipment->itemPrice;
		$orderShipment->itemTax = (float) $orderShipment->itemTax;
		$orderShipment->shippingPrice = (float) $orderShipment->shippingPrice;
		$orderShipment->shippingTax = (float) $orderShipment->shippingTax;
		$orderShipment->giftWrapPrice = (float) $orderShipment->giftWrapPrice;
		$orderShipment->giftWrapTax = (float) $orderShipment->giftWrapTax;
		$orderShipment->shipPromotionDiscount = (float) $orderShipment->shipPromotionDiscount;
		$orderShipment->itemPromotionDiscount = (float) $orderShipment->itemPromotionDiscount;

		$orderShipment->purchaseDate = date('Y-m-d H:i:s', strtotime($orderShipment->purchaseDate));
		$orderShipment->paymentsDate = date('Y-m-d H:i:s', strtotime($orderShipment->paymentsDate));
		$orderShipment->shipmentDate = date('Y-m-d H:i:s', strtotime($orderShipment->shipmentDate));
		$orderShipment->reportingDate = date('Y-m-d H:i:s', strtotime($orderShipment->reportingDate));
		$orderShipment->estimatedArrivalDate = date('Y-m-d H:i:s', strtotime($orderShipment->estimatedArrivalDate));

		return $orderShipment;
	}

	public static function fromArray($record) {
		$orderShipment = new self();
		foreach (self::$propertyMapping as $k => $v) {
			$orderShipment->$k = $record[$k] ?? NULL;
		}

		return $orderShipment;
	}
}
