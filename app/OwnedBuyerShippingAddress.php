<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OwnedBuyerShippingAddress extends Model
{
    protected $fillable = ['owned_buyer_id', 'ship_postal_code'];
}
