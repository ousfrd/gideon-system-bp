<?php

namespace App;

use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use DB;

use App\Mail\OrderGreeting;
use App\Mail\OrderDelivered;
use App\Mail\OrderReview;
use App\Mail\TestEmail;
use App\Helpers\DataHelper;
use App\Customer;
use App\OrderItem;
use App\Listing;
use App\Shipment;
use App\OrderItemMarketing;
use App\EmailList;

class Order extends Model {

	const ORDER_STATUS = [
		'Shipped',
		'Unshipped',
		'Shipping',
		'Pending',
		'Returned',
		'Partial Returned',
		'Cancelled'
	];

	protected $table = 'orders';
	
	protected $fillable = [
		'greeting_email_sent',
		'delivered_email_sent',
		'product_review_email_sent',
		'delivered_date',
		'seller_id',
		'AmazonOrderId',
		'PurchaseDate',
		'OrderTotal',
		'OrderTotalUSD',
		'ShippingAddressName',
		'ShippingAddressAddressLine1',
		'ShippingAddressAddressLine2',
		'ShippingAddressAddressLine3',
		'ShippingAddressCity',
		'ShippingAddressStateOrRegion',
		'ShippingAddressPostalCode',
		'ShippingAddressCountryCode',
		'ShippingAddressPhone',
		'BuyerName',
		'BuyerEmail',
		'PaymentMethod',
		'OrderStatus',
		'LastUpdateDate',
		'FulfillmentChannel',
		'SalesChannel',
		'Currency',
	];

	public $timestamps = false;

	public static $insertCardListings = NULL;

	// protected $fillable = ['asin','country','content','title','star'];
	function account() {
		return $this->belongsTo ( 'App\Account', 'seller_id', 'seller_id' );
	}
	
	/**
	 * Get the order items associated with the order.
	 */
	public function items() {
		// return $this->hasMany('App\OrderItem', 'AmazonOrderId', 'AmazonOrderId')
		// 	->where('order_items.seller_id', $this->seller_id);
		return $this->hasMany('App\OrderItem', 'AmazonOrderId', 'AmazonOrderId');
	}
	
	/**
	 * shipments associated with the order
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function shipment() {
		return $this->hasOne('App\Shipment', 'amazon_order_id', 'AmazonOrderId');
		// return $this->hasOne('App\Shipment', 'amazon_order_id', 'AmazonOrderId')
		// 	->where('order_shipments.seller_id', $this->seller_id);
	}

	/**
	 * redeem associated with the order
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function redeem() {
		return $this->hasOne('App\Redeem', 'AmazonOrderId', 'AmazonOrderId');
		// return $this->hasOne('App\Redeem', 'AmazonOrderId', 'AmazonOrderId')
		// 	->where('redeems.seller_id', $this->seller_id);
	}

	public function orderItemMarketing() {
		return $this->hasOne( 'App\OrderItemMarketing', 'oim_amazon_order_id', 'AmazonOrderId');
	}	

	public static function getOrderReports($params, $type='daily') {
		$query = self::getQuery($params);
		$orders = self::getOrders($query);
		$orderReports = self::generateOrderReports($orders, $type);
		if ($type == 'daily') {
			if (isset($params['purchase_date'])) {
				if (isset($params['purchase_date']['start_date'])) {
					$startDate = \Carbon\Carbon::createFromFormat(
						'Y-m-d H:i:s', $params['purchase_date']['start_date'], 'UTC');
				} else {
					$startDate = \Carbon\Carbon::now('UTC');
				}

				if (isset($params['purchase_date']['end_date'])) {
					$endDate = \Carbon\Carbon::createFromFormat(
						'Y-m-d H:i:s', $params['purchase_date']['end_date'], 'UTC');
				} else {
					$endDate = \Carbon\Carbon::now('UTC');
				}
				$endDate = $endDate->startOfDay();
				for ($date = $startDate->copy()->startOfDay(); $date->lte($endDate); $date->addDay()) {
					$dateKey = $date->format(DataHelper::DATE_METRICS['day']);
					if (!isset($orderReports[$dateKey]) || empty($orderReports[$dateKey])) {
						$orderReports[$dateKey] = self::getDefaultOrderReport();
					}
				}
			}
		}

		return $orderReports;
	}

	public static function getOrders($queryConds) {
		$orderObj = new Order();
		$orderItemObj = new OrderItem();
		$orderTable = $orderObj->getTable();
		$orderItemTable = $orderItemObj->getTable();

		$query = OrderItem::join($orderTable, function($join) {
			$orderObj = new Order();
			$orderItemObj = new OrderItem();
			$orderTable = $orderObj->getTable();
			$orderItemTable = $orderItemObj->getTable();

			$join->on($orderTable . '.AmazonOrderId', '=', $orderItemTable . '.AmazonOrderId');
			$join->on($orderTable . '.seller_id', '=', $orderItemTable . '.seller_id');
		})->select($orderTable . '.id as orderId', $orderTable . '.*', $orderItemTable . '.*');
		$normalQueryConds = array_filter($queryConds, function($queryCond) {
			return strtolower($queryCond[1]) != 'in';
		});
		$query = $query->where($normalQueryConds);

		$inQueryConds = array_filter($queryConds, function($queryCond) {
			return strtolower($queryCond[1]) == 'in';
		});
		if ($inQueryConds) {
			foreach ($inQueryConds as $inQueryCond) {
				$query = $query->whereIn($inQueryCond[0], $inQueryCond[2]);
			}
		}
		$notInQueryConds = array_filter($queryConds, function($queryCond) {
			return strtolower($queryCond[1]) == 'not in';
		});
		if ($notInQueryConds) {
			foreach ($notInQueryConds as $notInQueryCond) {
				$query = $query->whereNotIn($notInQueryCond[0], $notInQueryCond[2]);
			}
		}
		Log::debug(str_replace_array('?', $query->getBindings(), $query->toSql()));

		return $query->get();
	}

	public static function getQuery($params) {
		$query = [];

		$orderObj = new Order();
		$orderTable = $orderObj->getTable();

		if (isset($params['account_id']) && !empty($params['account_id']) && $params['account_id'] != 'all') {
			array_push($query, [$orderTable . '.seller_id', '=', $params['account_id']]);
		}

		if (isset($params['marketplace']) && $params['marketplace'] != 'all') {
			$marketplaceLower = strtolower($params['marketplace']);
			$marketplaceUpper = strtoupper($params['marketplace']);
			if (in_array($marketplaceLower, config('app.marketplaces'))) {
				$salesChannel = ucfirst($marketplaceLower);
			} else if (array_key_exists($marketplaceUpper, config('app.marketplaces'))) {
				$salesChannel = ucfirst(config('app.marketplaces')[$marketplaceUpper]);
			} else {
				$salesChannel = NULL;
			}
			if ($salesChannel) {
				$cond = [$orderTable . '.SalesChannel', '=', $salesChannel];
				array_push($query, $cond);
			}
		}

		if (isset($params['fulfillment_type']) && $params['fulfillment_type'] != 'all') {
			$cond = [
				$orderTable . '.FulfillmentChannel', '=',
				strtoupper($params['fulfillment_type'])
			];
			array_push($query, $cond);
		}

		if (isset($params['sku']) && !empty($params['sku'])) {
			array_push($query, ['SellerSKU', '=', $params['sku']]);
		} 

		if (isset($params['statuses']) && !empty($params['statuses'])) {
			array_push($query, ['OrderStatus', 'in', $params['statuses']]);
		}

		# if params contain both asin and country then search by region
		if (isset($params['country']) && !empty($params['country'])) {
			$countryExist = TRUE;
			array_push($query, ['country', '=', $params['country']]);
		} else {
			$countryExist = FALSE;
		}

		if (isset($params['region']) && !empty($params['region'])) {
			$regionExist = TRUE;
			array_push($query, ['region', '=', $params['region']]);
		} else {
			$regionExist = FALSE;
		}

		if (!$countryExist && !$regionExist) {
			if (isset($params['asin']) && !empty($params['asin'])) {
				array_push($query, ['ASIN', '=', $params['asin']]);
			}

			if (isset($params['products']) && !empty($params['products'])) {
				array_push($query, ['ASIN', 'in', $params['products']]);
			}
		}

		if (isset($params['purchase_date']) && (isset($params['purchase_date']['start_date']) || isset($params['purchase_date']['end_date']))) {
			// 2020-09-18
			if (isset($params['purchase_date']['start_date'])) {
				array_push($query, [$orderTable . '.PurchaseDate', '>=', $params['purchase_date']['start_date']]);
			}

			if (isset($params['purchase_date']['end_date'])) {
				array_push($query, [$orderTable . '.PurchaseDate', '<=', $params['purchase_date']['end_date']]);
			}
		}

		return $query;
	}

	public static function getDefaultOrderReport() {
		return (object) [ 
			'total_orders' => 0,
			'total_units' => 0,
			'revenue' => 0 
		];
	}

	public static function generateOrderReports($orders, $type='daily') {
		if (!in_array($type, ['daily', 'hourly'])) {
			return FALSE;
		}

		if ($type == 'daily') {
			$metric = 'day';
			$keyFormat = 'Y-m-d';
		} else {
			$metric = 'hour';
			$keyFormat = 'H';
		}

		$dateFormat = DataHelper::DATE_METRICS[$metric];
		$report = [];
		$groupedOrders = DataHelper::groupByDate(
			$orders, 'PurchaseDate', config('app.timezone'), $metric);
		foreach ($groupedOrders as $key => $ordersInType) {
			if ($keyFormat != $dateFormat) {
				$key = \DateTime::createFromFormat($dateFormat, $key)->format($keyFormat);
				if ($metric == 'hour') {
					$key = intval($key);
				}
			}
			if (array_key_exists($key, $report)) {
				$result = self::aggregateOrders($ordersInType);
				foreach ($result as $k => $v) {
					if (array_key_exists($k, $report[$key])) {
						$report[$key][$k] += $v;
					} else {
						$report[$key][$k] = $v;
					}
				}
			} else {
				$report[$key] = self::aggregateOrders($ordersInType);
			}
		}
		if ($metric == 'hour') {
			$aggregateOrders = $aggregateRevenue = 0;
			for ($hour = 0; $hour < 24; $hour++) {
				if (array_key_exists($hour, $report)) {
					$aggregateOrders += $report[$hour]['total_orders'];
					$aggregateRevenue += $report[$hour]['revenue']; 
					$report[$hour]['aggregate_orders'] = $aggregateOrders;
					$report[$hour]['aggregate_revenue'] = $aggregateRevenue;
				} else {
					$report[$hour] = (object) [
						'total_orders' => 0,
						'total_units' => 0,
						'revenue' => 0,
						'total_gross' => 0,
						'aggregate_orders' => $aggregateOrders,
						'aggregate_revenue' => $aggregateRevenue
					];
				}
			}
		}

		foreach ($report as $k => $v) {
			if (!is_object($v)) {
				$report[$k] = (object) $v;
			}
		}

		ksort($report);

		return $report;
	}

	public static function aggregateOrders($orders) {
		$result = [
			'total_units' => DataHelper::aggregate($orders, 'QuantityOrdered', 'sum'),
		];

		$insertCardListings = self::getInsertCardListings();

		$totalRefund = DataHelper::aggregate($orders, function($order) {
			return $order->total_refund * $order->exchange_rate;
		}, 'sum');
		$totalRefundCount = DataHelper::aggregate($orders, function($order) {
			return empty($order->refund_item_quantity) ? 0 : $order->refund_item_quantity;
		}, 'sum');
		$totalOrders = DataHelper::aggregate($orders, 'AmazonOrderId', 'count');
		$shippingFee = DataHelper::aggregate($orders, function($order) {
			return $order->ShippingPrice * $order->exchange_rate;
		}, 'sum');
		$totalGross = DataHelper::aggregate($orders, function($order) {
			return ($order->ItemPrice + $order->chargeback) * $order->exchange_rate;
		}, 'sum');
		$totalGrossPromo = DataHelper::aggregate($orders, function($order) {
			if ($order->PromotionId || $order->total_promo > 0) {
				$grossPromo = ($order->ItemPrice + $order->chargeback) * $order->exchange_rate;
			} else {
				$grossPromo = 0;
			}

			return $grossPromo;
		}, 'sum');
		$tax = DataHelper::aggregate($orders, function($order) {
			return $order->tax * $order->exchange_rate;
		}, 'sum');
		$commission = DataHelper::aggregate($orders, function($order) {
			return $order->commission * $order->exchange_rate;
		}, 'sum');
        $vatFee = DataHelper::aggregate($orders, function($order) {
            return $order->vat_fee * $order->exchange_rate;
        }, 'sum');
		$fbaFee = DataHelper::aggregate($orders, function($order) {
			if ($order->FulfillmentChannel == 'AFN') {
				$fee = $order->fba_fee * $order->exchange_rate;
			} else {
				$fee = 0;
			}
			return $fee;
		}, 'sum');
		$totalPromo = DataHelper::aggregate($orders, function($order) {
			return $order->total_promo * $order->exchange_rate;
		}, 'sum');
		$promoCount = DataHelper::aggregate($orders, function($order) {
			return ($order->PromotionId || $order->total_promo > 0) ? 1 : 0;
		}, 'sum');
		$totalUnitsPromo = DataHelper::aggregate($orders, function($order) {
			return ($order->PromotionId || $order->total_promo > 0) ? $order->QuantityOrdered : 0;
		}, 'sum');
		$totalCost = DataHelper::aggregate($orders, function($order) {
			return ($order->cost + $order->shipping_cost) * $order->QuantityOrdered;
		}, 'sum');
		$giftwrapFee = DataHelper::aggregate($orders, function($order) {
			return $order->giftwrap_fee * $order->exchange_rate;
		}, 'sum');
		$otherFee = DataHelper::aggregate($orders, function($order) {
			return $order->other_fee * $order->exchange_rate;
		}, 'sum');
        $orderStatuses = DataHelper::aggregate($orders, 'OrderStatus', 'count');
		$insertCardOrders = DataHelper::aggregate($orders, function($order) use ($insertCardListings) {
			$k = sprintf('%s-%s', $order->ASIN, $order->SellerSKU);
			return array_key_exists($k, $insertCardListings);
		}, 'sum');

		$result['insert_card_orders'] = $insertCardOrders;
		$result['total_refund'] = $totalRefund;
		$result['refund_count'] = $totalRefundCount;
		$result['total_orders'] = count($totalOrders);
		$result['shipping_fee'] = $shippingFee;
		$result['total_gross'] = $totalGross;
		$result['total_gross_promo'] = $totalGrossPromo;
		$result['total_gross_full'] = $totalGross - $totalGrossPromo;
		$result['tax'] = $tax;
		$result['commission'] = $commission;
		$result['fba_fee'] = $fbaFee;
		$result['vat_fee'] = $vatFee;
		$result['total_promo'] = $totalPromo;
		$result['promo_count'] = $promoCount;
		$result['total_units_promo'] = $totalUnitsPromo;
		$result['total_units_full'] = $result['total_units'] - $result['total_units_promo'];
		$result['total_cost'] = $totalCost;
		$result['giftwrap_fee'] = $giftwrapFee;
		$result['other_fee'] = $otherFee;
		$result['revenue'] = $totalGross + $shippingFee + $tax + $commission + $vatFee + $fbaFee + $totalPromo + $totalRefund + $giftwrapFee + $otherFee;
		$result['profit'] = $result['revenue'] - $totalCost - $tax;
        $result['pending_orders'] = isset($orderStatuses['Pending']) ? $orderStatuses['Pending'] : 0;
		return $result;
	}

	/**
	 * get order by id or AmazonOrderId
	 *
	 * @param int $order_id        	
	 * @return unknown
	 */
	static function getById($order_id) {
		if (ctype_digit ( $order_id )) {
			$order = Self::where ( 'id', '=', $order_id )->firstOrFail ();
		} else {
			$order = Self::where ( 'AmazonOrderId', '=', $order_id )->firstOrFail ();
		}
		
		return $order;
	}
	function simpleProductList($de = ", ") {
		$list = [ ];
		foreach ( $this->items as $item ) {
			$list [] = $item->QuantityOrdered . " x " . $item->Title . " (ASIN: " . $item->ASIN . ")";
		}
		return implode ( $de, $list );
	}
	function getShippingAddress($break = "<br/>") {
		// $address = $this->ShippingAddressName;
		$address = $this->ShippingAddressAddressLine1;
		if (! empty ( $this->ShippingAddressAddressLine2 )) {
			$address .= ", " . $this->ShippingAddressAddressLine2;
		}
		
		$address .= $break . $this->ShippingAddressCity;
		if (! empty ( $this->ShippingAddressStateOrRegion )) {
			$address .= "$break " . $this->ShippingAddressStateOrRegion;
		}
		$address .= ", " . $this->ShippingAddressPostalCode;
		// $address .= ", ".$this->ShippingAddressCountryCode;
		// $address .= "$break T: ".$this->ShippingAddressPhone;
		return $address;
	}
	
	static function buildQuery($query, $params = []) {
		$query->whereIn ( 'OrderStatus', [ 
				'Shipped',
				'Unshipped',
				'Shipping',
				'Pending',
				'Returned',
				'Partial Returned'
		] );
		// var_dump($params);
		if (isset ( $params ['account_id'] ) && ! empty ( $params ['account_id'] ) && $params ['account_id'] != 'all') {
			$query->where ( 'orders.seller_id', $params ['account_id'] );
		}
		
		if (isset ( $params ['marketplace'] ) && $params ['marketplace'] != 'all') {
			$marketplace = $params ['marketplace'];
			if (in_array ( strtolower ( $marketplace ), config ( 'app.marketplaces' ) )) {
				$query->where ( 'SalesChannel', $marketplace );
			}
		}
		
		if (isset ( $params ['fulfillment_type'] ) && $params ['fulfillment_type'] != 'all') {
			switch (strtolower ( $params ['fulfillment_type'] )) {
				case 'afn' :
					$query->where ( 'FulfillmentChannel', 'AFN' );
					break;
				case 'mfn' :
					$query->where ( 'FulfillmentChannel', 'MFN' );
					break;
			}
		}

		
		if (isset ( $params ['sku'] ) && ! empty ( $params ['sku'] )) {
			
			$query->where ( 'SellerSKU', $params ['sku'] );
		} 

		if (isset ( $params ['asin'] ) && ! empty ( $params ['asin'] )) {
			
			$query->where ( 'order_items.asin', $params ['asin'] );
		}


		if (isset ( $params ['products'] ) && ! empty ($params ['products'])) {
			
			$query->whereIn ( 'order_items.asin', $params ['products'] );
		}
		
		# if params contain both asin and country then search by region
		if (isset ( $params ['asin'] ) && ! empty ( $params ['asin'] ) && isset ( $params ['country'] ) && ! empty ( $params ['country'] ) ) {
			$query->where ( 'region', $params ['country'] );
		} elseif (isset ( $params ['country'] ) && ! empty ( $params ['country'] )) {
			$query->where ( 'country', $params ['country'] );
		}
		
		// $query->where('order_items.ItemPrice', '<=', '400');

		return $query;
	}
	static function orderCount($start_date, $end_date, $params = []) {
		$query = self::whereBetween ( 'PurchaseDate', [ 
				\DT::convertToUTC ( date ( 'Y-m-d 00:00:00', strtotime ( $start_date ) ) ),
				\DT::convertToUTC ( date ( 'Y-m-d 23:59:59', strtotime ( $end_date ) ) ) 
		] );
		
		$query->join( 'order_items', function($join) {
			$join->on('orders.AmazonOrderId', '=', 'order_items.AmazonOrderId');
			$join->on('orders.seller_id', '=', 'order_items.seller_id');
		});
		
		$query = self::buildQuery ( $query, $params );
		
		$query->distinct ()->count ( 'orders.AmazonOrderId' );
		
		return $query->count ();
	}

	static function refunds($start_date, $end_date, $params = []) {
		$query = FinanceEvent::whereBetween ( 'posted_date', [ 
				\DT::convertToUTC ( date ( 'Y-m-d 00:00:00', strtotime ( $start_date ) ) ),
				\DT::convertToUTC ( date ( 'Y-m-d 23:59:59', strtotime ( $end_date ) ) ) 
		] )->where ( 'group', 'Refund' )
		->select ( DB::raw ( 'sum(amount) as total_refund' )  );

		
		$query->join ( 'order_items', function($join) {
			$join->on('order_items.AmazonOrderId', '=', 'finance_events.amazon_order_id');
			$join->on('order_items.SellerSKU', '=', 'finance_events.sku');
		});
		$query->join( 'orders', function($join) {
			$join->on('orders.AmazonOrderId', '=', 'order_items.AmazonOrderId');
			$join->on('orders.seller_id', '=', 'order_items.seller_id');
		});

		$query = self::buildQuery ( $query, $params );
		
		$data = $query->first ();
		return $data;
	}

	static function refundCount($start_date, $end_date, $params = []) {
		$query = FinanceEvent::whereBetween ( 'posted_date', [ 
				\DT::convertToUTC ( date ( 'Y-m-d 00:00:00', strtotime ( $start_date ) ) ),
				\DT::convertToUTC ( date ( 'Y-m-d 23:59:59', strtotime ( $end_date ) ) ) 
		] )->where('type', 'Principal')->where ( 'group', 'Refund' )->select ( DB::raw ( 'count(*) as refund_count' ) );
		
		$query->join ( 'order_items', function($join) {
			$join->on('order_items.AmazonOrderId', '=', 'finance_events.amazon_order_id');
			$join->on('order_items.SellerSKU', '=', 'finance_events.sku');
		});
		$query->join( 'orders', function($join) {
			$join->on('orders.AmazonOrderId', '=', 'order_items.AmazonOrderId');
			$join->on('orders.seller_id', '=', 'order_items.seller_id');
		});

		$query = self::buildQuery ( $query, $params );
		
		$data = $query->first ();
		return $data->refund_count;
	}

	static function ppcCount($start_date, $end_date, $params = []) {
		$query = AdReport::whereBetween ( 'date', [ $start_date, $end_date ] )
		->select ( DB::raw ( 'sum(attributedConversions7d) as ppc_count' ) );
		
		if(isset ( $params ['asin'] ) && ! empty ( $params ['asin'] )) {
			$query->where('asin', $params['asin']);
		}
		if(isset ( $params ['sku'] ) && ! empty ( $params ['sku'] )) {
			$query->where('sku', $params['sku']);
		}
		if(isset ( $params ['account_id'] ) && ! empty ( $params ['account_id'] ) && $params ['account_id'] != 'all') {
			$query->where('seller_id', $params['account_id']);
		}
		if (isset ( $params ['marketplace'] ) && $params ['marketplace'] != 'all') {
			$marketplace = $params ['marketplace'];
			if (in_array ( strtolower ( $marketplace ), config ( 'app.marketplaces' ) )) {
				$country = array_search ( strtolower ( $marketplace ), config ( 'app.marketplaces' ));
				$query->where ( 'country', $country );
			}
		}

		// $query = self::buildQuery ( $query, $params );
		
		$data = $query->first ();
		return $data->ppc_count;
	}

	static function refundsByAsins($start_date, $end_date, $params = []) {
		$query = FinanceEvent::whereBetween ( 'posted_date', [ 
				\DT::convertToUTC ( date ( 'Y-m-d 00:00:00', strtotime ( $start_date ) ) ),
				\DT::convertToUTC ( date ( 'Y-m-d 23:59:59', strtotime ( $end_date ) ) ) 
		] )->where ( 'group', 'Refund' )
		->select ( ['order_items.asin', 'order_items.country', 'order_items.region', DB::raw ( 'sum(amount) as total_refund' ) ] );

		$query->join ( 'order_items', function($join) {
			$join->on('order_items.AmazonOrderId', '=', 'finance_events.amazon_order_id');
			$join->on('order_items.SellerSKU', '=', 'finance_events.sku');
		});
		$query->join( 'orders', function($join) {
			$join->on('orders.AmazonOrderId', '=', 'order_items.AmazonOrderId');
			$join->on('orders.seller_id', '=', 'order_items.seller_id');
		});
		
		$query->groupBy('order_items.asin', 'order_items.country', 'order_items.region');
		$query = self::buildQuery ( $query, $params );
		
		$data = $query->get ();
		return $data;
	}

	static function refundCountByAsins($start_date, $end_date, $params = []) {
		$query = FinanceEvent::whereBetween ( 'posted_date', [ 
				\DT::convertToUTC ( date ( 'Y-m-d 00:00:00', strtotime ( $start_date ) ) ),
				\DT::convertToUTC ( date ( 'Y-m-d 23:59:59', strtotime ( $end_date ) ) ) 
		] )->where('type', 'Principal')->where ( 'group', 'Refund' )
		->select ( ['order_items.asin', 'order_items.region', DB::raw ( 'count(*) as refund_count' )] );

		$query->join ( 'order_items', function($join) {
			$join->on('order_items.AmazonOrderId', '=', 'finance_events.amazon_order_id');
			$join->on('order_items.SellerSKU', '=', 'finance_events.sku');
		});
		$query->join( 'orders', function($join) {
			$join->on('orders.AmazonOrderId', '=', 'order_items.AmazonOrderId');
			$join->on('orders.seller_id', '=', 'order_items.seller_id');
		});
		$query->groupBy('order_items.asin', 'order_items.region');
		$query = self::buildQuery ( $query, $params );

		$data = $query->get ();
		return $data;
	}

	static function ppcCountByAsins($start_date, $end_date, $params = []) {
		$query = AdReport::whereBetween ( 'date', [ $start_date, $end_date ] )
		->select ( DB::raw ( 'asin, country, sum(attributedConversions7d) as ppc_count, sum(attributedSales7d) as ads_sales' ) );
		
		if(isset ( $params ['asin'] ) && ! empty ( $params ['asin'] )) {
			$query->where('asin', $params['asin']);
		}
		if(isset ( $params ['sku'] ) && ! empty ( $params ['sku'] )) {
			$query->where('sku', $params['sku']);
		}
		if(isset ( $params ['account_id'] ) && ! empty ( $params ['account_id'] ) && $params ['account_id'] != 'all') {
			$query->where('seller_id', $params['account_id']);
		}
		if (isset ( $params ['marketplace'] ) && $params ['marketplace'] != 'all') {
			$marketplace = $params ['marketplace'];
			if (in_array ( strtolower ( $marketplace ), config ( 'app.marketplaces' ) )) {
				$country = array_search ( strtolower ( $marketplace ), config ( 'app.marketplaces' ));
				$query->where ( 'country', $country );
			}
		}

		$query->groupBy('asin', 'country');

		$data = $query->get ();
		return $data;
	}

	static function refundsBySeller($start_date, $end_date, $params) {
		$query = FinanceEvent::whereBetween ( 'posted_date', [ 
				\DT::convertToUTC ( date ( 'Y-m-d 00:00:00', strtotime ( $start_date ) ) ),
				\DT::convertToUTC ( date ( 'Y-m-d 23:59:59', strtotime ( $end_date ) ) ) 
		] )->where ( 'group', 'Refund' )
		->select ( ['orders.seller_id', DB::raw ( 'sum(if((amount < 0), amount, 0)) as total_refund' ), DB::raw ( 'sum(if((amount >= 0), amount, 0)) as refund_credit' ) ] );

		$query->join ( 'order_items', function($join) {
			$join->on('order_items.AmazonOrderId', '=', 'finance_events.amazon_order_id');
			$join->on('order_items.SellerSKU', '=', 'finance_events.sku');
		});
		$query->join( 'orders', function($join) {
			$join->on('orders.AmazonOrderId', '=', 'order_items.AmazonOrderId');
			$join->on('orders.seller_id', '=', 'order_items.seller_id');
		});

		$query->groupBy('orders.seller_id');
		$query = self::buildQuery ( $query, $params );

		return $query->get ();
	}


	static function refundsByDateRange($start_date, $end_date, $params) {
		
		$timezoneOffset = \DT::timezoneUTCOffset (config('app.timezone'), date('Y-m-d 00:00:00',strtotime($start_date)));

		$query = FinanceEvent::whereBetween ( 'posted_date', [ 
				\DT::convertToUTC ( date ( 'Y-m-d 00:00:00', strtotime ( $start_date ) ) ),
				\DT::convertToUTC ( date ( 'Y-m-d 23:59:59', strtotime ( $end_date ) ) ) 
		] )->where ( 'group', 'Refund' )
		->select ( ['order_items.ASIN', 'order_items.seller_id', 'order_items.country', DB::raw ('DATE(DATE_ADD(posted_date,INTERVAL +' . $timezoneOffset . ' HOUR)) as pdate'), DB::raw ( 'sum(amount) as total_refund' ) ] );

		$query->join ( 'order_items', function($join) {
			$join->on('order_items.AmazonOrderId', '=', 'finance_events.amazon_order_id');
			$join->on('order_items.SellerSKU', '=', 'finance_events.sku');
		});
		$query->join( 'orders', function($join) {
			$join->on('orders.AmazonOrderId', '=', 'order_items.AmazonOrderId');
			$join->on('orders.seller_id', '=', 'order_items.seller_id');
		});

		$query->groupBy('pdate', 'order_items.ASIN', 'order_items.seller_id', 'order_items.country');
		$query = self::buildQuery ( $query, $params );

		return $query->get ();
	}


	static function refundsByGroup($start_date, $end_date, $params) {
		$query = FinanceEvent::whereBetween ( 'posted_date', [ 
				\DT::convertToUTC ( date ( 'Y-m-d 00:00:00', strtotime ( $start_date ) ) ),
				\DT::convertToUTC ( date ( 'Y-m-d 23:59:59', strtotime ( $end_date ) ) ) 
		] )->where ( 'group', 'Refund' )
		->select ( ['products.product_group', DB::raw ( 'sum(if((amount < 0), amount, 0)) as total_refund' ), DB::raw ( 'sum(if((amount >= 0), amount, 0)) as refund_credit' ) ] );

		$query->join ( 'order_items', function($join) {
			$join->on('order_items.AmazonOrderId', '=', 'finance_events.amazon_order_id');
			$join->on('order_items.SellerSKU', '=', 'finance_events.sku');
		});
		$query->join( 'orders', function($join) {
			$join->on('orders.AmazonOrderId', '=', 'order_items.AmazonOrderId');
			$join->on('orders.seller_id', '=', 'order_items.seller_id');
		});
		$query->join( 'products', function($join) {
			$join->on('products.asin', '=', 'order_items.ASIN');
			$join->on('products.country', '=', 'order_items.region');
		});
		$query->groupBy('products.product_group');
		$query = self::buildQuery ( $query, $params );

		return $query->get ();
	}

	static function orderReportsByDate($start_date, $end_date, $params = [], $grouped = True) {
		// $account_code=null,$marketplace = 'all',
		$query = OrderItem::join( 'orders', function($join) {
			$join->on('orders.AmazonOrderId', '=', 'order_items.AmazonOrderId');
			$join->on('orders.seller_id', '=', 'order_items.seller_id');
		});

		$query->whereBetween ( 'PurchaseDate', [
				// date ( 'Y-m-d 00:00:00', strtotime ( $start_date ) ),
				// date ( 'Y-m-d 23:59:59', strtotime ( $end_date ) )
				\DT::convertToUTC ( date ( 'Y-m-d 00:00:00', strtotime ( $start_date ) ) ),
				\DT::convertToUTC ( date ( 'Y-m-d 23:59:59', strtotime ( $end_date ) ) ) 
		] );

		$query = self::buildQuery ( $query, $params );
		
		$cols = [ 
				DB::raw ( 'sum(QuantityOrdered) as total_units' ),
				DB::raw ( 'count(orders.AmazonOrderId) as total_orders' ),
				DB::raw ( 'sum( (ItemPrice + chargeback)*exchange_rate ) as total_gross' ),
				DB::raw ( 'sum( ShippingPrice*exchange_rate) as shipping_fee '),
				DB::raw ( 'sum( tax*exchange_rate ) as tax' ),
				DB::raw ( 'sum(commission*exchange_rate) as commission' ),
				DB::raw ( 'sum(fba_fee*exchange_rate) as fba_fee' ),
				DB::raw ( 'sum(total_promo*exchange_rate) as total_promo' ),
				DB::raw ( 'sum(if((PromotionId IS NOT NULL or total_promo != 0), 1, 0)) as promo_count' ),
				// DB::raw ( 'sum(total_refund*exchange_rate) as total_refund' ),
				// DB::raw ( 'sum(if((total_refund != 0), 1, 0)) as refund_count' ),
				DB::raw ( 'sum((cost+shipping_cost)*QuantityOrdered ) as total_cost' ),
				DB::raw ( 'sum(giftwrap_fee*exchange_rate) as giftwrap_fee' ),
				DB::raw ( 'sum(other_fee*exchange_rate) as other_fee' ),
				DB::raw ( 'sum(vat_fee) as vat_fee' )
		];
		
		if ($grouped) {
			$timezoneOffset = \DT::timezoneUTCOffset (config('app.timezone'), date('Y-m-d 00:00:00',strtotime($start_date)));

			// if data range is only one day, show result by hour
			if ($start_date == $end_date) {
				$cols [] = DB::raw ( '(HOUR(PurchaseDate)+' . $timezoneOffset . ') as purchase_date' );
			} else {
				$cols [] = DB::raw ( 'DATE(DATE_ADD(PurchaseDate,INTERVAL +' . $timezoneOffset . ' HOUR)) as purchase_date' );
			}
		}
		
		$query->select ( $cols );

		// var_dump([\DT::convertToUTC(date('Y-m-d 00:00:00',strtotime($start_date))),\DT::convertToUTC(date('Y-m-d 23:59:59',strtotime($end_date)))]);
		// echo $query->toSql();
		Log::debug($query->toSql());

		if (! $grouped) {
			$data = $query->first ();

			$refunds = self::refunds($start_date, $end_date, $params);
			$data->total_refund = $refunds->total_refund;
			$data->refund_count = self::refundCount($start_date, $end_date, $params);

			$data->ppc_count = self::ppcCount($start_date, $end_date, $params);

			return $data;
		} else {
			$query->groupBy ( DB::raw ( 'purchase_date' ) );
			$rows = $query->get ();
			$data = [ ];
			// Log::debug($rows);

			foreach ( $rows as $row ) {
				$row->revenue = $row->total_gross + $row->shipping_fee + $row->tax + $row->commission + $row->vat_fee +$row->fba_fee + $row->total_promo + $row->total_refund - $row->total_cost;
				if ($start_date == $end_date) {
					$data [$row->purchase_date >= 0 ? $row->purchase_date : ($row->purchase_date + 24)] = $row;
				} else {
					$data [$row->purchase_date] = $row;
				}
			}

			if ($start_date == $end_date) {
				$aggregate_orders = $aggregate_revenue = 0;
				for($hour = 0; $hour < 24; $hour ++) {
					if (! isset ( $data [$hour] ) || empty ( $data [$hour] )) {
						$data [$hour] = ( object ) [ 
								'total_orders' => 0,
								'total_units' => 0,
								'revenue' => 0,
								'total_gross' => 0,
								'aggregate_orders' => $aggregate_orders,
								'aggregate_revenue' => $aggregate_revenue
						];
					} else {
						$aggregate_orders += $data [$hour]['total_orders'];
						$aggregate_revenue += $data [$hour]['revenue']; 
						$data [$hour]['aggregate_orders'] = $aggregate_orders;
						$data [$hour]['aggregate_revenue'] = $aggregate_revenue;
					}
				}
			} else {
				for($date = $start_date; $date <= date ( "Y-m-d", strtotime ( "$end_date +1 day" )); $date = date ( "Y-m-d", strtotime ( "$date +1 day" ) )) {

					if (! isset ( $data [$date] ) || empty ( $data [$date] )) {
						$data [$date] = ( object ) [ 
								'total_orders' => 0,
								'total_units' => 0,
								'revenue' => 0 
						];
					}
				}
			}
			
			ksort ( $data );

			return $data;
		}
	}

	static function orderReportsBySeller($start_date, $end_date, $params) {
		$query = OrderItem::join( 'orders', function($join) {
			$join->on('orders.AmazonOrderId', '=', 'order_items.AmazonOrderId');
			$join->on('orders.seller_id', '=', 'order_items.seller_id');
		});


		$query->whereBetween ( 'PurchaseDate', [ 
				\DT::convertToUTC ( date ( 'Y-m-d 00:00:00', strtotime ( $start_date ) ) ),
				\DT::convertToUTC ( date ( 'Y-m-d 23:59:59', strtotime ( $end_date ) ) ) 
		] );
		
		$query = self::buildQuery ( $query, $params );

		$cols = [ 
				DB::raw ( 'sum(QuantityOrdered) as total_units' ),
				DB::raw ( 'count(orders.AmazonOrderId) as total_orders' ),
				DB::raw ( 'sum( (ItemPrice + chargeback)*exchange_rate ) as total_gross' ),
				DB::raw ( 'sum( ShippingPrice*exchange_rate) as shipping_fee '),
				DB::raw ( 'sum( tax*exchange_rate ) as tax' ),
				DB::raw ( 'sum(commission*exchange_rate) as commission' ),
				DB::raw ( 'sum(fba_fee*exchange_rate) as fba_fee' ),
				DB::raw ( 'sum(total_promo*exchange_rate) as total_promo' ),
				DB::raw ( 'sum(if((PromotionId IS NOT NULL or total_promo != 0), 1, 0)) as promo_count' ),
				// DB::raw ( 'sum(total_refund*exchange_rate) as total_refund' ),
				// DB::raw ( 'sum(if((total_refund != 0), 1, 0)) as refund_count' ),
				DB::raw ( 'sum((order_items.cost+order_items.shipping_cost)*QuantityOrdered ) as total_cost' ),
				DB::raw ( 'sum(giftwrap_fee*exchange_rate) as giftwrap_fee' ),
				DB::raw ( 'sum(other_fee*exchange_rate) as other_fee' ),
				DB::raw ( 'sum(vat_fee) as vat_fee' ),
				'orders.seller_id',
		];


		$query->select($cols)->groupBy('orders.seller_id');

		$rows = $query->get ();

//		dd(DB::getQueryLog());

		return $rows;
	}


	static function orderReportsByDateRange($start_date, $end_date, $params) {
		$query = OrderItem::join( 'orders', function($join) {
			$join->on('orders.AmazonOrderId', '=', 'order_items.AmazonOrderId');
			$join->on('orders.seller_id', '=', 'order_items.seller_id')->where('orders.OrderStatus','!=','Cancelled');
		});


		$query->whereBetween ( 'PurchaseDate', [ 
				\DT::convertToUTC ( date ( 'Y-m-d 00:00:00', strtotime ( $start_date ) ) ),
				\DT::convertToUTC ( date ( 'Y-m-d 23:59:59', strtotime ( $end_date ) ) ) 
		] );
		
		$query = self::buildQuery ( $query, $params );

		$timezoneOffset = \DT::timezoneUTCOffset (config('app.timezone'), date('Y-m-d 00:00:00',strtotime($start_date)));
		$cols = [ 
				'order_items.ASIN', 'order_items.seller_id', 'order_items.country',
				DB::raw ('DATE(DATE_ADD(PurchaseDate,INTERVAL +' . $timezoneOffset . ' HOUR)) as pdate'),
				DB::raw ( 'sum(QuantityOrdered) as total_units' ),
				DB::raw ( 'count(orders.AmazonOrderId) as total_orders' ),
				DB::raw ( 'sum( (ItemPrice + chargeback)*exchange_rate ) as total_gross' ),
				DB::raw ( 'sum( ShippingPrice*exchange_rate) as shipping_fee '),
				DB::raw ( 'sum( tax*exchange_rate ) as tax' ),
				DB::raw ( 'sum(commission*exchange_rate) as commission' ),
				DB::raw ( 'sum(fba_fee*exchange_rate) as fba_fee' ),
				DB::raw ( 'sum(total_promo*exchange_rate) as total_promo' ),
				DB::raw ( 'sum(if((PromotionId IS NOT NULL or total_promo != 0), 1, 0)) as promo_count' ),
				// DB::raw ( 'sum(total_refund*exchange_rate) as total_refund' ),
				// DB::raw ( 'sum(if((total_refund != 0), 1, 0)) as refund_count' ),
				DB::raw ( 'sum((order_items.cost+order_items.shipping_cost)*QuantityOrdered ) as total_cost' ),
				DB::raw ( 'sum(giftwrap_fee*exchange_rate) as giftwrap_fee' ),
				DB::raw ( 'sum(other_fee*exchange_rate) as other_fee' ),
				DB::raw ( 'sum(vat_fee) as vat_fee' )
		];


		$query->select($cols)->groupBy('pdate', 'order_items.ASIN', 'order_items.seller_id', 'order_items.country');

		$rows = $query->get ();

		return $rows;
	}


	static function bestSellersByDate($start_date, $end_date, $params, $ajax = False, $offset = 0, $limit = 24) {

		$query = OrderItem::join( 'orders', function($join) {
			$join->on('orders.AmazonOrderId', '=', 'order_items.AmazonOrderId');
			$join->on('orders.seller_id', '=', 'order_items.seller_id');
		});

		$query->whereBetween ( 'PurchaseDate', [ 
				\DT::convertToUTC ( date ( 'Y-m-d 00:00:00', strtotime ( $start_date ) ) ),
				\DT::convertToUTC ( date ( 'Y-m-d 23:59:59', strtotime ( $end_date ) ) ) 
		] );
		
		$query = self::buildQuery ( $query, $params );

		$cols = [ 
				'ASIN', 'region', 'country', 
				DB::raw ( 'sum(QuantityOrdered) as total_units' ),
				DB::raw ( 'sum( if((PromotionId IS NULL and total_promo = 0), QuantityOrdered, 0)) as total_units_full' ),
				DB::raw ( 'sum( if((PromotionId IS NOT NULL or total_promo != 0), QuantityOrdered, 0)) as total_units_promo' ),
				DB::raw ( 'count(orders.AmazonOrderId) as total_orders' ),
				DB::raw ( 'sum( (ItemPrice + chargeback)*exchange_rate ) as total_gross' ),
				DB::raw ( 'sum( if((PromotionId IS NULL and total_promo = 0), (ItemPrice + chargeback)*exchange_rate, 0)) as total_gross_full' ),
				DB::raw ( 'sum( if((PromotionId IS NOT NULL or total_promo != 0), (ItemPrice + chargeback)*exchange_rate, 0)) as total_gross_promo' ),
				DB::raw ( 'sum( ShippingPrice*exchange_rate) as shipping_fee '),
				DB::raw ( 'sum( tax*exchange_rate ) as tax' ),
				DB::raw ( 'sum(commission*exchange_rate) as commission' ),
				DB::raw ( 'sum(fba_fee*exchange_rate) as fba_fee' ),
				DB::raw ( 'sum(total_promo*exchange_rate) as total_promo' ),
				DB::raw ( 'sum(if((PromotionId IS NOT NULL or total_promo != 0), 1, 0)) as promo_count' ),
				// DB::raw ( 'sum(total_refund*exchange_rate) as total_refund' ),
				// DB::raw ( 'sum(if((total_refund != 0), 1, 0)) as refund_count' ),
				DB::raw ( 'sum((cost+shipping_cost)*QuantityOrdered ) as total_cost' ),
				DB::raw ( 'sum(giftwrap_fee*exchange_rate) as giftwrap_fee' ),
				DB::raw ( 'sum(other_fee*exchange_rate) as other_fee' ),
				DB::raw ( 'sum(vat_fee) as vat_fee' ),
		];

		if($ajax) {

			array_push($cols, DB::raw ( 'sum( (ItemPrice + ShippingPrice + tax + chargeback + commission + vat_fee + fba_fee + total_promo + total_refund + giftwrap_fee + other_fee)*exchange_rate - (cost+shipping_cost)*QuantityOrdered ) as profit') );
			
			$query->select ( $cols )->orderBy('profit', 'desc')->groupBy ('order_items.ASIN', 'order_items.country', 'order_items.region')->offset($offset)
                ->limit($limit);

		} else {

			$query->select ( $cols )->orderBy('total_units', 'desc')->groupBy ('order_items.ASIN', 'order_items.country', 'order_items.region');
		}



		// var_dump($params);
		// var_dump($query->toSql());


		$rows = $query->get ();		

		
		return $rows;
	}

	static function bestSellersCountByDate($start_date, $end_date, $params) {
		$query = OrderItem::join( 'orders', function($join) {
			$join->on('orders.AmazonOrderId', '=', 'order_items.AmazonOrderId');
			$join->on('orders.seller_id', '=', 'order_items.seller_id');
		});

		$query->whereBetween ( 'PurchaseDate', [ 
				\DT::convertToUTC ( date ( 'Y-m-d 00:00:00', strtotime ( $start_date ) ) ),
				\DT::convertToUTC ( date ( 'Y-m-d 23:59:59', strtotime ( $end_date ) ) ) 
		] );
		
		$query = self::buildQuery ( $query, $params );

		$query->select ( 'ASIN', 'region' )->groupBy ('order_items.ASIN', 'order_items.region');
		
		$rows = $query->get ();		

		return count($rows);
	}



	static function newOrders() {
		
		$after_date = date('Y-m-d 00:00:00', strtotime("-5 days"));

		$orders = Self::where ( 'PurchaseDate', '>=', $after_date )->where ( 'OrderStatus', '=', 'Shipped' )->where('greeting_email_sent', '=', 0)->get ();
	
		return $orders;
	}

	static function deliveredOrders() {

		$after_date = date('Y-m-d 00:00:00', strtotime("-1 days"));

		$current = date('Y-m-d 00:00:00');

		$orders = Self::join('order_shipments', 'orders.AmazonOrderId', 'order_shipments.amazon_order_id')->where('OrderStatus', '=', 'Shipped')->where ( 'estimated_arrival_date', '>=', $after_date )->where ( 'estimated_arrival_date', '<=', $current )->where('delivered_email_sent', '=', 0)->orderBy('estimated_arrival_date', 'asc')->get ();
	
		return $orders;
	}

	static function pendingReviewOrders() {

		// $after_date = '2017-12-26 00:00:00';
		$after_date = date('Y-m-d 00:00:00', strtotime("-2 weeks"));

		$weekago = date('Y-m-d 00:00:00', strtotime('-1 week'));

		$orders = Self::join('order_shipments', 'orders.AmazonOrderId', 'order_shipments.amazon_order_id')->where('OrderStatus', '=', 'Shipped')->where ( 'estimated_arrival_date', '>=', $after_date )->where ( 'estimated_arrival_date', '<=', $weekago )->where('product_review_email_sent', '=', 0)->orderBy('estimated_arrival_date', 'asc')->get ();
	
		return $orders;
	}

	static function historyOrders() {
		$before_date = '2019-03-29 00:00:00';
		$after_date = '2019-03-10 00:00:00';

		$asins_scarremover = 'B07BVZYDC3';
		$orders = Self::join('order_items', 'orders.AmazonOrderId', 'order_items.AmazonOrderId')->where ( 'PurchaseDate', '<=', $before_date )->where ( 'PurchaseDate', '>=', $after_date )->where('BuyerEmail', '<>', '')->where('OrderStatus', '=', 'Shipped')->where('order_items.asin', $asins_scarremover)->where('product_review_email_sent', 0)->orderBy('PurchaseDate', 'desc')->get ();

		return $orders;


		// $orders_1 = Self::join('seller_accounts', 'orders.seller_id', 'seller_accounts.seller_id')->where ( 'PurchaseDate', '<=', $before_date )->where('BuyerEmail', '<>', '')->where('OrderStatus', '=', 'Shipped')->where('product_review_email_sent', '=', 0)->where('seller_accounts.smtp_host', 'like', '%amazonaws.com%')->where('seller_accounts.seller_id', '=', 'A2SPXBE5B98Z37')->orderBy('PurchaseDate', 'desc')->limit(1000)->get ();

		// $orders_2 = Self::join('seller_accounts', 'orders.seller_id', 'seller_accounts.seller_id')->where ( 'PurchaseDate', '<=', $before_date )->where('BuyerEmail', '<>', '')->where('OrderStatus', '=', 'Shipped')->where('product_review_email_sent', '=', 0)->where('seller_accounts.smtp_host', 'like', '%amazonaws.com%')->where('seller_accounts.seller_id', '=', 'A22JFGVJ6IFEY9')->orderBy('PurchaseDate', 'desc')->limit(1000)->get ();
	
		// return [$orders, $orders_1, $orders_2];
	}

	public static function shouldClaimReview($orders, $filterInnerPage = TRUE) {
		$classifiedOrders = [
			'doNotClaimReview' => [],
			'innerPage' => [],
			'claimReviewNeeded' => [
				'claimReviewSent' => ['innerPage' => [], 'normal' => []],
				'claimReviewToSend' => [],
				'missingShipment' => []
			]
		];

		// Filter block listed orders
		$blockListedEmails = Customer::getBlockListedEmails();

		// Get order related listings and inner page settings
		$innerPageSettings = [];
		$ks = [];
		$amazonOrderIds = [];
		foreach ($orders as $order) {
			$k = sprintf('%s-%s', $order->ASIN, $order->SellerSKU);
			if (!in_array($k, $ks)) {
				$ks[] = $k;
			}

			$amazonOrderIds[$order->AmazonOrderId] = NULL;
		}
		$amazonOrderIds = array_keys($amazonOrderIds);

		$queryCond = sprintf("CONCAT(asin, '-', sku) IN ('%s')", implode("','", $ks));
		$listings = Listing::whereRaw($queryCond)->get();
		foreach ($listings as $listing) {
			$k = sprintf('%s-%s', $listing->asin, $listing->sku);
			$innerPageSettings[$k] = $listing->insert_card > 0;
		}

		// Get order shipments
		$orderShipments = Shipment::whereIn('amazon_order_id', $amazonOrderIds)->get();
		$groupedOrderShipments = DataHelper::groupByVal($orderShipments, 'amazon_order_id');

		// Get OrderItemMarketings
		$orderItemMarketings = OrderItemMarketing::whereIn('oim_amazon_order_id', $amazonOrderIds)->get();
		$groupedOrderItemMarketings = DataHelper::groupByVal($orderItemMarketings, 'oim_amazon_order_id');

		// Get history order item marketings
		$buyerEmails = [];
		foreach ($orderShipments as $orderShipment) {
			$buyerEmails[$orderShipment->buyer_email] = NULL;
		}
		$buyerEmails = array_keys($buyerEmails);
		$historyOrderItemMarketings = OrderItemMarketing::whereIn('buyer_id', $buyerEmails)->get();
		$groupedHistoryOims = DataHelper::groupByVal($historyOrderItemMarketings, 'buyer_id');

		$orderGroups = [];
		$ordersProcessed = [];
		foreach ($orders as $order) {
			$amazonOrderId = $order->AmazonOrderId;
			if (isset($ordersProcessed[$amazonOrderId])) {
				continue;
			}
			$ordersProcessed[$amazonOrderId] = NULL;

			// Only shipped orders need to claim review
			if (strtolower($order->OrderStatus) != 'shipped') {
				$classifiedOrders['doNotClaimReview'][$amazonOrderId] = $order;
				continue;
			}

			// Only FBA orders need to claim review
			if ($order->FulfillmentChannel != 'AFN') {
				$classifiedOrders['doNotClaimReview'][$amazonOrderId] = $order;
				continue;
			}

			// order that amazon order ID starts with "S" does not need to claim review
			if (strtoupper(substr($amazonOrderId, 0, 1)) == "S") {
				$classifiedOrders['doNotClaimReview'][$amazonOrderId] = $order;
				continue;
			}

			// Orders that marked as do not claim review do not need to claim review
			if (isset($order->skip_review_claim) && $order->skip_review_claim) {
				$classifiedOrders['doNotClaimReview'][$amazonOrderId] = $order;
				continue;
			}

			if (array_key_exists($order->AmazonOrderId, $groupedOrderItemMarketings)) {
				$order->orderItemMarketings = $groupedOrderItemMarketings[$amazonOrderId];
			} else {
				$order->orderItemMarketings = NULL;
			}

			// Inner page orders
			$k = sprintf('%s-%s', $order->ASIN, $order->SellerSKU);
			$innerPageOrder = $innerPageSettings[$k];
			if ($innerPageOrder) {
				$classifiedOrders['innerPage'][$amazonOrderId] = $order;

				if ($order->orderItemMarketings) {
					$classifiedOrders['claimReviewNeeded']['claimreviewSent']['innerPage'][$amazonOrderId] = $order;
				}

				if ($filterInnerPage) {
					$classifiedOrders['doNotClaimReview'][$amazonOrderId] = $order;
					continue;
				}
			}

			if (array_key_exists($order->AmazonOrderId, $groupedOrderShipments)) {
				$order->orderShipments = $groupedOrderShipments[$order->AmazonOrderId];
			} else {
				$order->orderShipments = NULL;
			}

			if (!empty($order->orderShipments)) {
				// Orders that marked as do not claim review do not need to claim review
				$filteredShipments = array_filter($order->orderShipments, function($shipment) {
					return isset($shipment->skip_review_claim) && $shipment->skip_review_claim;
				});
				if (!empty($filteredShipments)) {
					$classifiedOrders['doNotClaimReview'][$amazonOrderId] = $order;
					continue;
				}

				// Orders that in block list do not need to claim review
				$filteredShipments = [];
				foreach ($order->orderShipments as $shipment) {
					if (!empty($shipment->buyer_email) && in_array($shipment->buyer_email, $blockListedEmails)) {
						$filteredShipments[] = $shipment;
					}
				}
				if (!empty($filteredShipments)) {
					$classifiedOrders['doNotClaimReview'][$amazonOrderId] = $order;
					continue;
				}
			} else {
				// Orders that missing shipment could not claim review
				$classifiedOrders['claimReviewNeeded']['missingShipment'][$amazonOrderId] = $order;
				continue;
			}

			if (empty($order->orderItemMarketings)) {
				$classifiedOrders['claimReviewNeeded']['claimReviewToSend'][$amazonOrderId] = $order;
				continue;
			}

			$filteredOims = array_filter($order->orderItemMarketings, function ($orderItemMarketing) {
				return $orderItemMarketing->is_sent;
			});
			if (!empty($filteredOims)) {
				$classifiedOrders['claimReviewNeeded']['claimReviewSent']['normal'][$amazonOrderId] = $order;
				continue;
			}

			$filteredOims = array_filter($order->orderItemMarketings, function ($orderItemMarketing) {
				return $orderItemMarketing->processed;
			});
			if (!empty($filteredOims)) {
				$classifiedOrders['doNotClaimReview'][$amazonOrderId] = $order;
				continue;
			}

			$asin = $order->ASIN;
			$sellerId = $order->seller_id;
			$orderHistoryOims = [];
			foreach ($order->orderShipments as $orderShipment) {
				if (empty($orderShipment->buyer_email)) {
					continue;
				}

				$email = $orderShipment->buyer_email;
				if (!array_key_exists($email, $groupedHistoryOims)) {
					continue;
				}

				foreach ($groupedHistoryOims[$email] as $oim) {
					if ($oim->asin == $asin || $oim->seller_id == $sellerId) {
						$orderHistoryOims[] = $oim;
					}
				}
			}
			if (empty($orderHistoryOims)) {
				$classifiedOrders['claimReviewNeeded']['claimReviewToSend'][$amazonOrderId] = $order;
			} else {
				$classifiedOrders['doNotClaimReview'][$amazonOrderId] = $order;
			}
		}

		return $classifiedOrders;
	}

	function need_review() {
		$need = false;
		$order = $this;
		foreach($order->items as $order_item) {
			$asin = $order_item->ASIN;
			$country = $order_item->region;
			if(Product::where('asin', $asin)->where('country', $country)->first()->need_review) {
				$need = true;
			}
		}
		return $need;
	}


	function sendGreetingEmail() {
		$to_email = $this->BuyerEmail;
		// disable gmail smtp sending

		if(!empty($to_email) and !empty($this->account->smtp_host) and $this->account->smtp_host != "smtp.gmail.com" and strpos($this->account->smtp_host, 'amazonaws.com') !== false and $this->greeting_email_sent == 0) {

			if(EmailList::where('type', 'Greeting')->where('seller_id', $this->seller_id)->where('amazon_order_id', $this->AmazonOrderId)->count() == 0) {
				try {
					Mail::to($to_email)->send(new OrderGreeting($this));

					$this->greeting_email_sent = 1;
					$this->save();
				} catch (\Exception $e) {
					// failed to send email, record email list as failed
					$email_list = EmailList::where('type', 'Greeting')->where('seller_id', $this->seller_id)->where('amazon_order_id', $this->AmazonOrderId)->orderBy('id', 'desc')->first();
					if($email_list) {
						$email_list->delete();
					}

					print $e->getMessage();
				}

			} else {
				if($this->greeting_email_sent == 0) {
					$this->greeting_email_sent = 1;
					$this->save();
				}
			}
		}	

	}

	function sendDeliveredEmail() {
		$to_email = $this->BuyerEmail;
		if(!empty($to_email) and !empty($this->account->smtp_host) and $this->account->smtp_host != "smtp.gmail.com" and strpos($this->account->smtp_host, 'amazonaws.com') !== false and $this->delivered_email_sent == 0) {

			if(EmailList::where('type', 'Delivered')->where('seller_id', $this->seller_id)->where('amazon_order_id', $this->AmazonOrderId)->count() == 0) {
				try {
					Mail::to($to_email)->send(new OrderDelivered($this));

					$this->delivered_email_sent = 1;
					$this->save();
				} catch (\Exception $e) {
					// failed to send email, record email list as failed
					$email_list = EmailList::where('type', 'Delivered')->where('seller_id', $this->seller_id)->where('amazon_order_id', $this->AmazonOrderId)->orderBy('id', 'desc')->first();
					if($email_list) {
						$email_list->delete();
					}

					print $e->getMessage();
				}

			} else {
				if($this->delivered_email_sent == 0) {
					$this->delivered_email_sent = 1;
					$this->save();
				}
			}
		}

	}

	function sendProductReviewEmail() {
		$to_email = $this->BuyerEmail;
		
		if(!empty($to_email) and !empty($this->account->smtp_host) and $this->account->smtp_host != "smtp.gmail.com" and strpos($this->account->smtp_host, 'amazonaws.com') !== false and $this->product_review_email_sent == 0) {

			if(EmailList::where('type', 'Product Review')->where('seller_id', $this->seller_id)->where('amazon_order_id', $this->AmazonOrderId)->count() == 0) {
				// var_dump($to_email); var_dump($this->AmazonOrderId);
				try {
			    	Mail::to($to_email)->send(new OrderReview($this));

					$this->product_review_email_sent = 1;
					$this->save();
				} catch (\Exception $e) {
					// failed to send email, record email list as failed
					$email_list = EmailList::where('type', 'Product Review')->where('seller_id', $this->seller_id)->where('amazon_order_id', $this->AmazonOrderId)->orderBy('id', 'desc')->first();
					if($email_list) {
						$email_list->delete();
					}
					print 'Can not send the email...';
					print $e->getMessage();

					// exit;
				}
				
			} else {
				if($this->product_review_email_sent == 0) {
					$this->product_review_email_sent = 1;
					$this->save();
				}
			}
		}

	}

	function sendTestGreetingEmail() {
		$to_email = $this->BuyerEmail;
		if(!empty($to_email) and !empty($this->account->smtp_host) and $this->account->smtp_host != "smtp.gmail.com" and strpos($this->account->smtp_host, 'amazonaws.com') !== false and $this->greeting_email_sent == 0) {

			if(EmailList::where('type', 'Greeting')->where('seller_id', $this->seller_id)->where('amazon_order_id', $this->AmazonOrderId)->count() == 0) {
				Mail::to($to_email)->send(new OrderGreeting($this));

				$this->greeting_email_sent = 1;
				$this->save();
			} else {
				if($this->greeting_email_sent == 0) {
					$this->greeting_email_sent = 1;
					$this->save();
				}
			}
		}	
		
	}


	function sendTestDeliveredEmail() {
		$to_email = $this->BuyerEmail;
		if(!empty($to_email) and !empty($this->account->smtp_host) and $this->account->smtp_host != "smtp.gmail.com" and strpos($this->account->smtp_host, 'amazonaws.com') !== false and $this->delivered_email_sent == 0) {

			if(EmailList::where('type', 'Delivered')->where('seller_id', $this->seller_id)->where('amazon_order_id', $this->AmazonOrderId)->count() == 0) {

				Mail::to($to_email)->send(new OrderDelivered($this));

				$this->delivered_email_sent = 1;
				$this->save();

			} else {
				if($this->delivered_email_sent == 0) {
					$this->delivered_email_sent = 1;
					$this->save();
				}
			}
		}
		
	}


	function sendTestGreetingEmailByMailgunSmtp() {
		$to_email = $this->BuyerEmail;
		if(!empty($to_email) and !empty($this->account->smtp_host) and $this->delivered_email_sent == 0) {

			if(EmailList::where('type', 'Greeting')->where('seller_id', $this->seller_id)->where('amazon_order_id', $this->AmazonOrderId)->count() == 0) {
				Mail::to($to_email)->send(new OrderGreeting($this));

				$this->greeting_email_sent = 1;
				$this->save();
			} else {
				if($this->greeting_email_sent == 0) {
					$this->greeting_email_sent = 1;
					$this->save();
				}
			}
		}
		
	}


	function sendTestDeliveredEmailByAsin() {
		// $to_email = $this->BuyerEmail;
		$to_email = "joshyanworks@gmail.com";
		if(!empty($to_email) and !empty($this->account->smtp_host) and $this->account->smtp_host != "smtp.gmail.com" and strpos($this->account->smtp_host, 'amazonaws.com') !== false and $this->delivered_email_sent == 0) {


			if(EmailList::where('type', 'Delivered')->where('seller_id', $this->seller_id)->where('amazon_order_id', $this->AmazonOrderId)->count() == 0) {


				try {
					Mail::to($to_email)->send(new OrderDelivered($this));
				} catch (\Exception $e) {
					print $e->getMessage();
				}

				// $this->greeting_email_sent = 1;
				// $this->save();
			} else {
				// if($this->greeting_email_sent == 0) {
				// 	$this->greeting_email_sent = 1;
				// 	$this->save();
				// }
			}
		}	
		
	}

	static function ajaxSendTestEmail($template_id) {
		// $to_email = $this->BuyerEmail;
		$to_email = auth()->user()->email;

		try {
			Mail::to($to_email)->send(new TestEmail($template_id));
		} catch (\Exception $e) {
			print $e->getMessage();
		}

		
	}

	public static function getInsertCardListings() {
		if (self::$insertCardListings) {
			return self::$insertCardListings;
		}

		// Log::debug('Initialize insert card listings.');

		$listings = Listing::where('insert_card', '>', 0)->get();
		self::$insertCardListings = [];
		foreach ($listings as $listing) {
			$k = sprintf('%s-%s', $listing->asin, $listing->sku);
			self::$insertCardListings[$k] = NULL;
		}
		// Log::debug('[InsertCardListingInitialized] ' . implode(', ', array_keys(self::$insertCardListings)));

		return self::$insertCardListings;
	}

	// public function delete(){
	// 	$this->items()->delete();
	// 	$this->shipment()->delete();
	// 	$this->redeem()->delete();
	// 	parent::delete();
	// }

	public function refundStatus() {
		$orderItems = $this->items;
		$orderCount = 0;
		$refundCount = 0;
		$status = null;
		foreach ($orderItems as $orderItem) {
			if ($orderItem->total_refund < 0) {
				$refundCount++;
			}
			$orderCount++;
		}
		if ($refundCount == $orderCount) {
			$status = "Refunded";
		} else if ( $refundCount > 0 && $refundCount < $orderCount) {
			$status = "Partial Refunded";
		}
		return $status;
	}

	public static function boot() {
		
		parent::boot();

		self::deleting(function($model) {
			$model->items()->delete();
			$model->shipment()->delete();
			$model->redeem()->delete();
		});

	}


}
