<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Mail;
use DB;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role','verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    

    
    function reports()
    {
        return $this->hasMany('App\Report','created_by');
    }
    
    function products() {
    	return $this->belongsToMany('App\Product', 'product_users', 'user_id', 'product_id');
    }
    
    function productList() {
    	$list = [];
    	foreach ($this->products as $p) {
    		$list[] = $p->asin;
    	}
    	
    	return $list;
    }

    function productIds() {
        return $this->products->map(function($item, $key) {
            return $item->id;
        })->all();
    }
    
    function accountList() {
    	$list = [];
    	foreach ($this->accounts as $p) {
    		$list[] = $p->seller_id;
    	}
    	
    	return $list;
    }
    
    
    function canManageProduct($product_id) {
    	return  $this->products()->where('product_id',$product_id)->count();
    }


    function is_admin() {
        $role = $this->role;
        if ($role) {
            $roles = explode(',', $role);
            return !empty(array_intersect($roles, ['system admin', 'admin']));
        } else {
            return false;
        }
    }

    function first_name() {
        $names =  explode(' ', $this->name);
        return $names[0];
    }

    public function isSelfReviewStaff() {
        $role = $this->role;
        if ($role) {
            $roles = explode(',', $role);
            return in_array('marketing_self_review', $roles);
        }else {
            return false;
        }
    }

    
    static function adminList($type=null) {
        $admin=  self::whereIn('role',['admin'])->where('verified',1)->orderBy('name','asc')->pluck('name','id');
        if ($type == 'id-list') {
            $adminSources = [];
            foreach ($admin as $id=>$name) {
                $adminSources[] = ['id'=>$id,'text'=>$name];
            }
            
            return $adminSources;
        }
        
        return $admin;
    }

    public static function selfReviewStaffs(){
        $users = User::visible()->get();
        $result = [];
        foreach ($users as $user) {
            $role = $user->role;
            $roles = explode(',', $role);
            $allowedRoles = ['marketing_self_review'];
            if (count(array_intersect($roles, $allowedRoles)) > 0) {
                $result[$user->name] = $user->id; 
            }
        }
        return $result;
    }

    static function idNameHash() {
        return self::select("id", "name")->get()->keyBy('id')->map(function($model) { return $model->name; });
    }

    static function accountManagers(){
        $roles = ['product manager','admin'];
        return self::usersHasRoles($roles);
    }
    
    static function accountManagerList($type=null){
        $roles = ['product manager','admin'];
        $account_managers = self::usersHasRoles($roles)->pluck('name','id');
        if ($type == 'id-list') {
            $managerSources = [];
            foreach ($account_managers as $id=>$name) {
                $managerSources[] = ['id'=>$id,'text'=>$name];
            }
            
            return $managerSources;
        }
        return $account_managers;
    }

    /**
     * params $roles: String Array
     */
    static function usersHasRoles($roles) {
        $allUsers = User::visible()->where('verified',1)->orderBy('name','asc')->get();
        $users = [];
        foreach ($allUsers as $user) {
            if ($user->role) {
                $userRoles = explode(',', strtolower($user->role));
                if (count(array_intersect($userRoles, $roles)) > 0) {
                    $users[] = $user;
                }
            }
            
        }
        return collect($users);
    }


    static function productManagers(){
        $roles = ['product manager'];
        return self::usersHasRoles($roles);
    }
    
    static function productManagerList($type=null){
        $roles = ['product manager','product manager assistant'];
        $product_managers = self::usersHasRoles($roles)->pluck('name','id');
    	if ($type == 'id-list') {
    		$managerSources = [['id'=>0,'text'=>'None']];
    		foreach ($product_managers as $id=>$name) {
    			$managerSources[] = ['id'=>$id,'text'=>$name];
    		}
    		
    		return $managerSources;
    	}
    	
    	return $product_managers;
    }
    
    
    
    
    function accounts() {
    	return $this->belongsToMany('App\Account', 'user_accounts', 'user_id', 'seller_account_id');
    }
    
    function canManageAccount($acount_id) {
    	return  $this->accounts()->where('seller_account_id',$acount_id)->count();
    }
    

    
    
    function sendAccountVerifiedEmail(){
    	
    	$user = $this;
    	Mail::send('user.email.verified', ['user' => $this,'site'=>config('app.name'),'link'=>route('login')], function ($m) use ($user) {
    		//$m->to("haiyanqu.ny@gmail.com", $order->getCustomerName());
    		$m->to($user->email, $user->name);
    		$m->subject("Your ".config('app.name')." account has been verified.");
    	});
    		
    		$this->welcome_email_sent = 1;
    		$this->save();
    }
    
    function sendAccountCreatedByAdminEmail($password){
    	
    	$user = $this;
    	Mail::send('user.email.created_by_admin', ['user' => $this,'password'=>$password,'site'=>config('app.name'),'link'=>route('login')], function ($m) use ($user) {
    		//$m->to("haiyanqu.ny@gmail.com", $order->getCustomerName());
    		$m->to($user->email, $user->name);
    		$m->subject("Your ".config('app.name')." account has been created.");
    	});
    	
    	$this->welcome_email_sent = 1;
    	$this->save();
    }
    
    
    static function findByName($name) {
    	return self::where('name',$name)->firstOrFail();
    }
    
    
    function orderReports($start_date,$end_date) {
    	
    	return Order::orderReportsByDate($start_date, $end_date,['user_id'=>$this->id,'products'=>$this->productList()]);
    	
    	
    }
    
    function sendNotificationEmails() {
    	$this->sendLowInventoryNotificationEmails();
    	$this->sendSlowSalesEmails();
    	$this->sendExcessInventoryEmails();
    }
    
    
    
    function sendExcessInventoryEmails(){
    	
    	$user = $this;
    	
    	$excessInventory = Excessnventory::listByUser($user);
    	
    	if(count($excessInventory) > 0) {
    		
    		$subject = count($excessInventory). " of your products are excess, actions needed!";
    		
    		Mail::send('user.email.excess', ['user' => $user,'excessInventory'=>$excessInventory], function ($m) use ($user,$subject) {
    			
    			$m->to($user->email, $user->name);
//     			$m->to("info@gnmart.com", $user->name);
    			
    			$m->subject($subject);
    			
    		});
    	}
    }
    
    function sendSlowSalesEmails(){
    	
    	$user = $this;
    	
    	$lowInventoryProducts = Product::slowProducts($user);
    	
    	if(count($lowInventoryProducts) > 0) {
    		$subject = count($lowInventoryProducts). " of your products are running slow";
    		Mail::send('user.email.notification', ['user' => $user,'lowInventoryProducts'=>$lowInventoryProducts], function ($m) use ($user,$subject) {
    			
    			$m->to($user->email, $user->name);
//     			$m->to("info@gnmart.com", $user->name);
    			
    			$m->subject($subject);
    			
    		});
    	}
    }
    function sendLowInventoryNotificationEmails(){
    	
    	$user = $this;
    	
    	$lowInventoryProducts = Product::lowOrOutOfStockingProducts($user,2);
    	
    	if(count($lowInventoryProducts) > 0) {
	    	$subject = count($lowInventoryProducts). " of your products are running out of stock";
	    	Mail::send('user.email.lownotification', ['user' => $user,'lowInventoryProducts'=>$lowInventoryProducts], function ($m) use ($user,$subject) {
	    
	    		$m->to($user->email, $user->name);
// 	    		$m->to("info@gnmart.com", $user->name);
	    		
	    		$m->subject($subject);
	    		
	    	});
    	}
    }

    public function scopeVisible($query)
    {
        return $query->where('visible', true);
    }
}
