<?php 

namespace App;
use App\InventoryHistory;
use App\Account;

class InventoryReportGenerator {
  static function prepareData($sellerId, $marketPlace, $date) {
    $_marketPlace = "all";
    if (strtolower($marketPlace) == "amazon.com") { $_marketPlace = "US"; }
    else if (strtolower($marketPlace) == "amazon.ca") { $_marketPlace = "CA"; }
    else if (strtolower($marketPlace) == "amazon.co.uk") { $_marketPlace = "UK" ;}
    $inventories = InventoryHistory::where("import_date", $date)->get();
    $inventories->load("listing");
    $accounts = Account::select("id", "name", "code", "seller_id")->get();
    $accountsHash = [];
    foreach($accounts as $account) {
      $accountsHash[$account->seller_id] = $account;
    }
    $rows = [];
    foreach ($inventories as $inventory) {
      $data = [];
      $listing = $inventory->listing;
      if ($sellerId && strtolower($sellerId) != "all" && $listing->seller_id != $sellerId) { continue; }
      if ($marketPlace && strtolower($marketPlace) != "all" && $listing->country != $_marketPlace) { continue; }
      $account = $accountsHash[$listing->seller_id];
      $data["account_name"] = $account->code . $listing->country;
      $data["store_name"] = $account->name;
      $data["item_name"] = $listing->item_name;
      $data["sku"] = $listing->sku;
      $data["asin"] = $listing->asin;
      $data["fulfillable_quantity"] = $inventory->fulfillable_quantity;
      $data["unsellable_quantity"] = $inventory->unsellable_quantity;
      $data["reserved_quantity"] = $inventory->reserved_quantity;
      $data["warehouse_quantity"] = $inventory->warehouse_quantity;
      $data["inbound_working_quantity"] = $inventory->inbound_working_quantity;
      $data["inbound_shipped_quantity"] = $inventory->inbound_shipped_quantity;
      $data["inbound_receiving_quantity"] = $inventory->inbound_receiving_quantity;
      $data["total_quantity"] = $inventory->total_quantity;
      // get cost
      $cost = $listing->cost;
      if (is_null($cost) || $cost == 0) {
        $cost = $listing->product->cost;
      }
      $data["product_cost"] = $cost;
      // get shipping cost
      $shipping_cost = $listing->shipping_cost;
      if (is_null($shipping_cost) || $shipping_cost == 0) {
        $shipping_cost = $listing->product->shipping_cost;
      }
      $data["shipping_cost"] = $shipping_cost;
      $data["selling_price"] = $listing->sale_price;
      $data["date"] = $inventory->import_date;
      $data["last_updated_at"] = $inventory->updated_at;
      $rows[] = $data;
    }
    return $rows;
  }
   
}




