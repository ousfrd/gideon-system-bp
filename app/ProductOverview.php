<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOverview extends Model {
    protected $guarded = ['id'];
}
