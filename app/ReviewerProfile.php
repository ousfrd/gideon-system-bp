<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewerProfile extends Model
{
    protected $table = 'buyer_profiles';
}
