<?php

namespace App;

use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use App\OrderItemMarketing;
use App\Shipment;
use App\Listing;
use App\Product;
use App\Order;
use App\Customer;
use App\ClickSendClient;
use App\Helpers\Click2MailHelper;
use App\Helpers\DataHelper;


class ClaimReviewCampaign extends Model
{
    const CHANNELS = [
        "website" => "Website",
        "email" => "Email"
    ];

    const MARKETPLACES = [
        "US" => "amazon.com",
        "CA" => "amazon.ca"
    ];

    const SUPPLIERS = [
        'click_send' => 'Click Send',
        'click2mail' => 'Click2Mail',
        'other' => 'Other'
    ];
    const CLICK_SEND_PROVIDER = 'click_send';
    const CLICK2MAIL_PROVIDER = 'click2mail';

    public function orderItemMarketings() {
        return $this->hasMany('App\OrderItemMarketing', 'claim_review_id');
    }

    public function giftCardTemplate() {
        return $this->belongsTo('App\GiftCardTemplate');
    }

    public function redeems() {
        return $this->hasMany('App\Redeem');
    }

    public function getNegtiveReviewQuantityAttribute() {
        return $this->response_quantity - $this->free_product_quantity - $this->gift_card_quantity;
    }


    public function getPromotionDiscountsArrayAttribute() {
        if ($this->promotion_discounts) {
            return json_decode($this->promotion_discounts);
        } else {
            return [];
        }
    }

    public function getExcludePromotionDiscountsArrayAttribute() {
        if ($this->cs_exclude_promotion_discounts) {
            return json_decode($this->cs_exclude_promotion_discounts);
        } else {
            return [];
        }
    }

    public function getEligibleOrderShipments() {
        $asin = $this->asin;
        $marketplace = $this->marketplace;
        $startDate = $this->start_date;
        $endDate = $this->end_date;
        $query = Listing::select("sku")->where("asin", $asin);
        if (strtoupper($marketplace) != "ALL") {
			$query->where("country", $marketplace);
        }
        $skus = $query->get()->pluck('sku');

        if ($this->insert_card_orders_filter > 0) {
            foreach ($skus as $k => $sku) {
                $listing =  Listing::where('sku', $sku)->first();
                if ($listing->insert_card > 0) {
                    unset($skus[$k]);
                }
            }
        }
        
        $rows = Shipment::select("order_shipments.amazon_order_id", "order_shipments.sku", "order_shipments.seller_id", "order_shipments.asin", "order_shipments.item_promotion_discount","order_shipments.buyer_email" , "order_shipments.fulfillment_channel", "order_item_marketings.processed")
                                ->leftJoin("order_item_marketings", "order_item_marketings.oim_amazon_order_id", "=", "order_shipments.amazon_order_id")
                                ->where('order_shipments.asin', $asin)
                                ->whereIn("sku", $skus)
                                ->whereBetween("shipment_date", [$startDate, $endDate])
                                ->where('skip_review_claim', FALSE)
                                ->get();
        $filteredAmazonOrderShipments = [];
        foreach ($rows as $row) {
            if (!$row->processed) {
                $filteredAmazonOrderShipments[] = $row;
            }
        }
        return $filteredAmazonOrderShipments;
    }

    public function getShipments() {
        $query = Shipment::select("*")
                    ->Join("order_item_marketings", "order_shipments.amazon_order_id", "=", "order_item_marketings.oim_amazon_order_id")
                    ->where("order_item_marketings.claim_review_id", $this->id)
                    ->where('order_shipments.skip_review_claim', FALSE)
                    ->groupBy('order_shipments.amazon_order_id');
        return $query;
    }

    public function campaignGetShipments() {
        $oim_amazon_order_ids = OrderItemMarketing::select('oim_amazon_order_id')->where("order_item_marketings.claim_review_id", $this->id)->get();
        $shipmentQuery = Shipment::select("*")
                            ->whereIn('amazon_order_id', $oim_amazon_order_ids)
                            ->where('skip_review_claim', FALSE)
                            ->groupBy('amazon_order_id');
        return $shipmentQuery;
    }

    public function getShipmentsWithOrderItemMarketing() {
        $query = Shipment::with('order_item_marketing')->select("*")
                    ->Join("order_item_marketings", "order_shipments.amazon_order_id", "=", "order_item_marketings.oim_amazon_order_id")
                    ->where("order_item_marketings.claim_review_id", $this->id)
                    ->groupBy('order_shipments.amazon_order_id');
        return $query;
    }

    public function getClickReviewSendOrderShipments() {
        $query = $this->getShipmentsWithOrderItemMarketing();
        $query->where('order_shipments.ship_country', $this->marketplace)
                ->where('order_shipments.recipient_name', 'NOT LIKE', '%Amazon%');
        if (count($this->exclude_promotion_discounts_array) > 0) {
            $query->whereNotIn('order_shipments.item_promotion_discount', $this->exclude_promotion_discounts_array);
        }
        return $query;
    }

    
    public static function shouldShipmentAddToCampaign($shipment, $blocklistedCustomers) {
        // Don't count blocklisted customers' order
        if ($blocklistedCustomers->contains($shipment->buyer_email)) {
            return false;
        }
        // Don't count non-FBA orders
        if ($shipment->fulfillment_channel != 'AFN') {
            return false;
        }
        $amazon_order_id = $shipment->amazon_order_id;
        if (strtoupper(substr($amazon_order_id, 0, 1)) == "S") { return false; }
        $sku = $shipment->sku;
        $asin = $shipment->asin;

        
        $buyer_id = $shipment->buyer_email;
        $seller_id = $shipment->seller_id;
        $shouldAdd = false;
        $orderItemMarketings = OrderItemMarketing::where(function($q) use($buyer_id, $asin) {
            $q->where('buyer_id', $buyer_id)
                ->where('asin', $asin);
        })->orWhere(function($q) use($buyer_id, $seller_id) {
            $q->where('buyer_id', $buyer_id)
                ->where('seller_id', $seller_id);
        })->get();
        foreach ($orderItemMarketings as $orderItemMarketing) {
            if ($orderItemMarketing->processed) {
                $shouldAdd = false;
                return $shouldAdd;
            }
        }
        $existingItemMarketing = OrderItemMarketing::where("oim_amazon_order_id", $amazon_order_id)->where("oim_sku", "!=", $sku)->first();
        if ($existingItemMarketing) {
            $shouldAdd = false;
        } else {
            $shouldAdd = true;
        }
        return $shouldAdd;
    }

    public function description() {
        return $this->asin . '-' . $this->marketplace . '-' . $this->start_date . '-' . $this->end_date;
    }

    public function updateResponse() {
        $response = $this->redeems;
        $giftCardResponse = $response->filter(function($model){
            return $model->how_to_help == "Amazon Gift Card";
        });
        $freeProductResponse = $response->filter(function($model){
            return $model->how_to_help == "Same Free Product";
        });
        $nagtiveToPositiveResponse = $response->filter(function($model){
            return $model->star <= 3 && $model->review_link;
        });

        $giftCartReviews = $giftCardResponse->filter(function($model) {
            return !empty($model->review_link);
        });
        $freeProductReviews = $freeProductResponse->filter(function($model) {
            return !empty($model->review_link);
        });

        $this->gift_card_reviews = count($giftCartReviews);
        $this->free_product_reviews = count($freeProductReviews);
        $this->response_quantity = count($response);
        $this->gift_card_quantity = count($giftCardResponse);
        $this->free_product_quantity = count($freeProductResponse);
        $this->negtive_to_positive_quantity = count($nagtiveToPositiveResponse);
        $this->save();
    }

    public function send() {
        if ($this->is_sending || $this->sent_at) {
            return;
        }

        if ($this->platform == self::CLICK_SEND_PROVIDER) {
            ClickSendClient::send($this);
        }

        if ($this->platform == self::CLICK2MAIL_PROVIDER) {
            Click2MailHelper::send($this);
        }
    }

    static function getCampaignListQuery() {
        $query = ClaimReviewCampaign::select('claim_review_campaigns.*', 'users.name as creator', 'gift_card_templates.front_design', 'gift_card_templates.back_design', 'gift_card_templates.id as gift_card_template_id', 'gift_card_templates.name as gift_card_template_name')
                    ->join('users', 'claim_review_campaigns.created_by', '=', 'users.id')
                    ->leftJoin('gift_card_templates', 'gift_card_templates.id', '=', 'claim_review_campaigns.gift_card_template_id');
        return $query;
    }

    static function updateResponseForAll() {
        $campaigns = ClaimReviewCampaign::all();
        foreach($campaigns as $campaign) {
            $campaign->updateResponse();
        }
    }

    public static function updateClick2MailCost() {
        $campaigns = self::where([
            ['platform', '=', 'click2mail']
        ])->get();
        foreach ($campaigns as $campaign) {
            if ($campaign->actual_sent_amount > 0) {
                $campaign->unit_price = $campaign->total_cost / $campaign->actual_sent_amount;
            } else {
                $campaign->unit_price = 0;
            }

            $campaign->save();
        }
    }

    public static function getClaimReviewStatistics($fromDate, $toDate) {
        $result = [];

        $fromDateStr = \Carbon\Carbon::createFromFormat(
            'Y-m-d', $fromDate, config('app.timezone'))->startOfDay()->format('Y-m-d H:i:s');
        $toDateStr = \Carbon\Carbon::createFromFormat(
            'Y-m-d', $toDate, config('app.timezone'))->endOfDay()->format('Y-m-d H:i:s');
        $campaigns = self::where([
            ['sent_at', '>=', $fromDateStr],
            ['sent_at', '<=', $toDateStr]
        ])->get();
        $campaignsByProduct = DataHelper::groupByVal($campaigns, function ($campaign) {
            return sprintf('%s-%s', strtoupper($campaign->marketplace), $campaign->asin);
        });
        foreach ($campaignsByProduct as $k => $campaignsGroup) {
            $totalSent = 0;
            $totalCost = 0;
            foreach ($campaignsGroup as $campaign) {
                $totalSent += $campaign->actual_sent_amount;
                $totalCost += $campaign->total_cost;
            }

            $result[$k] = [
                'totalSent' => $totalSent,
                'totalCost' => $totalCost
            ];
        }

        return $result;
    }

    public static function getDeafultClaimReviewStatistic() {
        return ['totalSent' => 0, 'totalCost' => 0];
    }

    public function updateClaimReviewItems() {
        $asin = $this->asin;
        $country = $this->marketplace;
        $startDate = $this->start_date;
        $endDate = $this->end_date;

        $product = Product::where([
            ['asin', '=', $asin],
            ['country', '=', $country]
        ])->first();
        if (!$product) {
            return;
        }

        $orders = $product->getOrdersByDate($startDate, $endDate);
        $classifiedOrders = Order::shouldClaimReview($orders, $this->insert_card_orders_filter > 0);
        $filteredOrders = $classifiedOrders['claimReviewNeeded']['claimReviewToSend'];

        $promotedOrderQuantity = 0;
        $promotionDiscounts = [];
        $oims = [];
        foreach ($filteredOrders as $order) {
            $shipment = current($order->orderShipments);

            $amazonOrderId = $shipment->amazon_order_id;
            $sku = $shipment->sku;
            $asin = $shipment->asin;
            $buyerEmail = $shipment->buyer_email;
            $sellerId = $shipment->seller_id;
            $discount = $shipment->item_promotion_discount;
            if ($discount != 0 && !in_array($discount, $promotionDiscounts)) {
                $promotionDiscounts[] = $discount;
            }
            if ($discount < 0) {
                $promotedOrderQuantity += 1;
            }

            $oims[] = [
                'seller_id' => $sellerId,
                'oim_amazon_order_id' => $amazonOrderId,
                'asin' => $asin,
                'oim_sku' => $sku,
                'buyer_id' => $buyerEmail,
                'claim_review_id' => $this->id
            ];
        }
        foreach (array_chunk($oims, 100) as $oimsGroup) {
            try {
                \DB::table('order_item_marketings')->insert($oimsGroup);
            } catch (\Exception $e) {
                Log::warning($e->getMessage());
                Log::info(json_encode($oimsGroup));
            }
        }

        $this->promoted_order_quantity = $promotedOrderQuantity;
        $this->order_quantity = count($filteredOrders);
        $this->promotion_discounts = json_encode($promotionDiscounts);
        $this->save();
    }


    public static function boot()
    {
        parent::boot();

        self::created(function($model) {
            if (TRUE) {
                $model->updateClaimReviewItems();
                return;
            }

            $filteredAmazonOrderShipments = $model->getEligibleOrderShipments();
            $orderCount = 0;
            $promotion_discounts = [];
            $buyerEmails = [];
            foreach($filteredAmazonOrderShipments as $shipment) {
                $buyerEmails[] = $shipment->buyer_email;
            }
            $blocklistedCustomers = Customer::whereIn('amazon_buyer_email', $buyerEmails)->where('blocklisted', true)->pluck('amazon_buyer_email');
            foreach($filteredAmazonOrderShipments as $shipment) {
                $amazon_order_id = $shipment->amazon_order_id;
                $sku = $shipment->sku;
                $asin = $shipment->asin;
                $buyer_id = $shipment->buyer_email;
                $seller_id = $shipment->seller_id;
                $discount = $shipment->item_promotion_discount;
                if ($discount != 0 && !in_array($discount, $promotion_discounts)) {
                    $promotion_discounts[] = $discount;
                }
                if (self::shouldShipmentAddToCampaign($shipment, $blocklistedCustomers)) {
                    $oim = OrderItemMarketing::firstOrNew(["oim_sku" => $sku, "oim_amazon_order_id" => $amazon_order_id]);
                    $oim->claim_review_id = $model->id;
                    $oim->asin = $asin;
                    $oim->buyer_id = $buyer_id;
                    $oim->seller_id = $seller_id;
                    $oim->save();
                    $orderCount++;
                }
            }
            $promotedOrderQuantity = $model->getShipments()->where("item_promotion_discount", "<", 0)->get()->count();
            $model->promoted_order_quantity = $promotedOrderQuantity;
            $model->order_quantity = $orderCount;
            $model->promotion_discounts = json_encode($promotion_discounts);
            $model->save();
        });

        self::saving(function($model) {
            if ($model->cs_exclude_promotion_discounts && is_array($model->cs_exclude_promotion_discounts)) {
                $model->cs_exclude_promotion_discounts = json_encode($model->cs_exclude_promotion_discounts);
            }
            if ($model->actual_sent_amount > 0) {
                $model->response_rate = $model->response_quantity / $model->actual_sent_amount;
                $model->positive_response_rate = ($model->free_product_quantity + $model->gift_card_quantity) / $model->actual_sent_amount;
                $model->positive_review_rate = ($model->gift_card_reviews + $model->free_product_reviews + $model->negtive_to_positive_quantity) / $model->actual_sent_amount;
            }
        });

        // self::deleting(function($model) {
            // OrderItemMarketing::where("claim_review_id", $model->id)->delete();
            // $orderItems = OrderItemMarketing::where("claim_review_id", $model->id)->get();
            // foreach($orderItems as $item) {
            //     $item->claim_review_id = 0;
            //     $item->is_sent = false;
            //     $item->click_send_response = null;
            //     $item->click_send_scheduled_send_at = null;
            //     $item->save();
            // }
        // });
    }


}
