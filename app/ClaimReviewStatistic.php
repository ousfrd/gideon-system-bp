<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Listing;
use App\Product;

class ClaimReviewStatistic extends Model
{
	protected $guarded = ['id'];

	public static function buildStatisticsForRecent1Week() {
		$days = 8;
		$fromDate = \Carbon\Carbon::now()->subDays($days)->startOfDay();
        $toDate = \Carbon\Carbon::now()->endOfDay();
        $fromDateStr = $fromDate->format('Y-m-d');
        $toDateStr = $toDate->format('Y-m-d');

        self::buildStatistics($fromDateStr, $toDateStr);
	}

	public static function buildStatisticsForRecent30Days() {
		$days = 30;
		$fromDate = \Carbon\Carbon::now()->subDays($days)->startOfDay();
        $toDate = \Carbon\Carbon::now()->endOfDay();
        $fromDateStr = $fromDate->format('Y-m-d');
        $toDateStr = $toDate->format('Y-m-d');

        self::buildStatistics($fromDateStr, $toDateStr);
	}

    public static function buildStatistics($fromDate, $toDate) {
    	$dateRange = [];

    	$startDate = \Carbon\Carbon::createFromFormat(
			'Y-m-d', $fromDate, config('app.timezone'))->startOfDay();
    	$endDate = \Carbon\Carbon::createFromFormat(
			'Y-m-d', $toDate, config('app.timezone'))->startOfDay();
    	for ($date = $startDate->copy(); $date->lte($endDate); $date->addDay()) {
    		$dateRange[] = $date->format('Y-m-d');
    	}

    	$products = Product::getProductsNeedClaimReview();
		foreach ($products as $product) {
			$statistics = $product->buildClaimReviewStatistics($fromDate, $toDate);
			foreach ($dateRange as $date) {
				$statistic = isset($statistics[$date]) ? $statistics[$date] : NULL;
				if (empty($statistic) || $statistic->orderCount <= 0) {
					continue;
				}

				$crs = self::firstOrNew([
					'asin' => $product->asin,
					'country' => $product->country,
					'date' => $date
				]);
				$crs->orders = $statistic->orderCount;
				$crs->pending_orders = $statistic->pendingOrderCount;
				$crs->shipped_orders = $statistic->shippedOrderCount;
				$crs->returned_orders = $statistic->returnedOrderCount;
				$crs->partial_returned_orders = $statistic->partialReturnedOrderCount;
				$crs->cancelled_orders = $statistic->cancelledOrderCount;
				$crs->inner_page_orders = $statistic->innerPageOrderCount;
				$crs->missing_shipment_cnt = $statistic->missingShipmentCount;
				$crs->to_claim_review_cnt = $statistic->toClaimReviewCount;
				$crs->claim_review_sent_cnt = $statistic->claimReviewSentCount;
				$crs->inner_page_claim_review_sent_cnt = $statistic->innerPageClaimReviewSentCount;
				$crs->claim_review_to_send_cnt = $statistic->claimReviewToSendCount;

				$crs->save();
			}
		}
    }
}
