<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DivvyTransactionRecord extends Model
{

    protected $fillable = ['transaction_id'];

    protected $primaryKey = 'transaction_id';

    public static function import($filename) {

    }

}
