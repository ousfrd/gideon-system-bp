<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;


class AdReportRequest extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'ad_report_requests';
	
	
	public function account(){
		return $this->belongsTo('App\Account','seller_id','seller_id');
	}
	

}
	