<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
class AdPayment extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'ad_payments';
	
	public function merchant(){
		return $this->belongsTo('App\Account','seller_id','seller_id');
	}
	
	
	
	static function amountByDate($start_date, $end_date,$params){
		
		$query = self::whereBetween('posted_date',[\DT::convertToUTC(date('Y-m-d 00:00:00',strtotime($start_date))),\DT::convertToUTC(date('Y-m-d 23:59:59',strtotime($end_date)))]);
		$query = self::buildQuery($query,$params);
		
		$query->select(DB::raw('sum(amount*exchange_rate) as total'));
		//echo $query->toSql();
		
		return 0-$query->first()->total;
		
	}
	
	static function amountBySeller($start_date, $end_date,$params){
		
		$query = self::whereBetween('posted_date',[\DT::convertToUTC(date('Y-m-d 00:00:00',strtotime($start_date))),\DT::convertToUTC(date('Y-m-d 23:59:59',strtotime($end_date)))]);
		$query = self::buildQuery($query,$params);
		
		$query->select([DB::raw('round(sum(amount*exchange_rate), 2) as total'), 'seller_id']);
		$query->groupBy('seller_id');
		
		return $query->get ();
		
	}


	static function buildQuery($query,$params=[]) {

		if(isset($params['marketplace']) && in_array ( strtolower ( $params['marketplace'] ), config ( 'app.marketplaces' ) )) {
			foreach(config('app.marketplaceRegions') as $key => $mkts) {
				if(in_array($params['marketplace'], $mkts)) {
					$region = $key;
				}
			}
		}

		if( (isset ( $params ['account_id'] ) && ! empty ( $params ['account_id'] ) && $params ['account_id'] != 'all') or isset($region)) {
			
			$query->whereHas('merchant',function($q) use ($params) {
				if(!empty($params['account_id']) && $params ['account_id'] != 'all') {
					$q->where('seller_id',$params['account_id']);
				}

				if(!empty($region)) {
					$q->where('region',$region);
				}
			});
		}

		
		return $query;
	}
	
}
	