<?php

namespace App;
use ClickSend\Configuration;
use ClickSend\Api\PostReturnAddressApi;
use ClickSend\Api\AccountApi;
use ClickSend\Api\PostLetterApi;
use ClickSend\Api\PostPostcardApi;
use ClickSend\Model\PostRecipient;
use ClickSend\Model\PostLetter;
use ClickSend\Model\PostPostcard;

use Exception;
use GuzzleHttp\Client;
use App\Shipment;
use App\ClickSendHistory;

class ClickSendClient {

  private static $config = null;

  private static function getConfig() {
    $apiUsername = env('CLICKSEND_USERNAME');
    $apiPassword = env('CLICKSEND_PASSWORD');
    if (empty($apiUsername) || empty($apiPassword)) {
      throw new Exception('Click Send Api Username or Password is empty'); 
    }
    $config = Configuration::getDefaultConfiguration()
            ->setUsername($apiUsername)
            ->setPassword($apiPassword);
    return $config;
  }

  public static function getAccount() {
    $config = self::getConfig();
    $apiInstance = new AccountApi(new Client(), $config);
    $result = $apiInstance->accountGet();
    return $result;
  }

  public static function getReturnAddresses() {
    $config = self::getConfig();
    $apiInstance = new PostReturnAddressApi(new Client(), $config);
    $page = 1;
    $limit = 50;
    $response = json_decode($apiInstance->postReturnAddressesGet($page, $limit), true);
    $result = $response['data']['data'];
    return $result;
  }

  public static function testSend(){
    $config = self::getConfig();
    $apiInstance = new PostLetterApi(new Client(), $config);
    $fileUrl = "https://storage.googleapis.com/300gedion/gift-card-template/b9EdiFFlmERSzqcAiRweK5HzmHps4OF7X2qR1ZeK.pdf";
    $post_recipient = new PostRecipient();
    $shipment = Shipment::find(10075);
    $post_recipient->setAddressName($shipment->recipient_name);
    $post_recipient->setAddressLine1($shipment->ship_address_1);
    $post_recipient->setAddressLine2($shipment->ship_address_2);
    $post_recipient->setAddressCity($shipment->ship_city);
    $post_recipient->setAddressState($shipment->ship_state);
    $post_recipient->setAddressPostalCode("11111");
    $post_recipient->setAddressCountry($shipment->ship_country);
    $post_recipient->setReturnAddressId(74463);
    $post_recipient->setSchedule(time() + 72000);
    
    $post_letter = new PostLetter();
    $post_letter->setFileUrl($fileUrl);
    $post_letter->setPriorityPost(0);
    $post_letter->setRecipients([$post_recipient]);
    $post_letter->setTemplateUsed(1);
    $post_letter->setDuplex(0);
    $post_letter->setColour(1);
    try {
      $result = $apiInstance->postLettersSendPost($post_letter);
      print_r($result);
    } catch (Exception $e) {
        echo 'Exception when calling PostLetterApi->postLettersSendPost: ', $e->getMessage(), PHP_EOL;
    }
  }

  // public static function send($claimReviewCampaign, $testMode = false) {
  //   $config = self::getConfig();
  //   $apiInstance = new PostLetterApi(new Client(), $config);
  //   $template = $claimReviewCampaign->giftCardTemplate;
  //   if ($template && $template->attachment1) {
  //     $shipments = $claimReviewCampaign->getClickReviewSendOrderShipments()->get();
  //     $post_recipients = [];
  //     $delay_hours = $claimReviewCampaign->cs_delay_hours;
  //     $scheduleBase = time() + 1 * 60 * 60;
  //     if ($delay_hours) {
  //       $scheduleBase = time() + $delay_hours * 60 * 60;
  //     }
  //     $counter = 0;
  //     $orderItemMarketingsArray = [];
  //     foreach ($shipments as $shipment) {
  //       $post_recipient= new PostRecipient();
  //       $post_recipient->setAddressName($testMode ? "Test ".$shipment->recipient_name : $shipment->recipient_name);
  //       $post_recipient->setAddressLine1($shipment->ship_address_1);
  //       $post_recipient->setAddressLine2($shipment->ship_address_2);
  //       $post_recipient->setAddressCity($shipment->ship_city);
  //       $post_recipient->setAddressState($shipment->ship_state);
  //       $post_recipient->setAddressPostalCode($testMode ? '11111' : $shipment->ship_postal_code);
  //       $post_recipient->setAddressCountry($shipment->ship_country);
  //       $post_recipient->setReturnAddressId($claimReviewCampaign->cs_return_address_id);
  //       $scheduled_at = $scheduleBase + $counter;
  //       $post_recipient->setSchedule($scheduled_at);
  //       $post_recipients[] = $post_recipient;
  //       $counter++;
  //       $oim = $shipment->order_item_marketing;
  //       $oim->click_send_scheduled_send_at = $scheduled_at;
  //       $orderItemMarketingsArray[] = $oim;
  //     }
  //     $orderItemMarketingsCollection = collect($orderItemMarketingsArray);
  //     $isPriority = $claimReviewCampaign->cs_priority_post ? 1 : 0;
  //     $isDuplex = $claimReviewCampaign->cs_duplex ? 1 : 0;
  //     $isColored = $claimReviewCampaign->cs_colour ? 1 : 0;
  //     $post_letter = new PostLetter();
  //     $post_letter->setFileUrl($template->attachment1_url);
  //     $post_letter->setPriorityPost($isPriority);
  //     $post_letter->setRecipients($post_recipients);
  //     $post_letter->setTemplateUsed(1);
  //     $post_letter->setDuplex($isDuplex);
  //     $post_letter->setColour($isColored);
      
  //     $result = $apiInstance->postLettersSendPost($post_letter);
  //     $response = json_decode($result, true);

  //     if ($response['http_code'] >= 200 && $response['http_code'] < 300) {
  //       $recipients = $response['data']['recipients'];
  //       $claimReviewCampaign->actual_sent_amount = $response['data']['total_count'];
  //       $claimReviewCampaign->sent_at = date('Y-m-d H:i:s');
  //       $claimReviewCampaign->save();
  //       foreach ($recipients as $recipient) {
  //         $oim = $orderItemMarketingsCollection->first(function ($value, $key) use($recipient) {
  //           return $value->click_send_scheduled_send_at == $recipient['schedule'];
  //         });
  //         $oim->is_sent = true;
  //         $oim->click_send_response = json_encode($recipient);
  //         $oim->save();
  //       }
  //     } else {
  //       throw new Exception($result);
  //     }

  //   }
    
  // }

  // TODO: Configure return address
  public static function getReturnAddressId($returnAddress) {
  }

  public static function getRecipients($addresses, $returnAddress, $scheduledAt) {
    $recipients = [];
    foreach ($addresses as $address) {
      $recipient = new PostRecipient();
      $recipient->setAddressName($address['name']);
      $recipient->setAddressLine1($address['address1']);
      $recipient->setAddressLine2($address['address2']);
      $recipient->setAddressCity($address['city']);
      $recipient->setAddressState($address['state']);
      $recipient->setAddressPostalCode($address['zip_code']);
      $recipient->setAddressCountry($address['country']);
      $recipient->setSchedule($scheduledAt);

      $returnAddressId = self::getReturnAddressId($returnAddress);
      $recipient->setReturnAddressId($returnAddressId);

      $recipients[] = $recipient;
    }

    return $recipients;
  }

  public static function composeLetter($recipients, $fileUrl, $options = []) {
    $postLetter = new PostLetter();
    $postLetter->setRecipients($recipients);

    $isPriority = isset($options['priority_post']) ? $options['priority_post'] : FALSE;
    $isDuplex = isset($options['duplex']) ? $options['duplex'] : FALSE;
    $isColored = isset($options['colored']) ? $options['colored'] : FALSE;
    $useTemplate = isset($options['template_used']) ? $options['template_used'] : 1;
    $postLetter->setPriorityPost($isPriority);
    $postLetter->setDuplex($isDuplex);
    $postLetter->setColour($isColored);
    $postLetter->setTemplateUsed($useTemplate);

    if (is_array($fileUrl)) {
      $fileUrl = current($fileUrl);
    }
    $postLetter->setFileUrl($fileUrl);

    return $postLetter;
  }

  public static function composePostCard($recipients, $fileUrls, $options = []) {
    $postCard = new PostPostcard();
    $postCard->setFileUrls($fileUrls);
    $postCard->setRecipients($recipients);

    return $postCard;
  }

  public static function sendLetter($addresses, $document, $returnAddress, $scheduledAt, $options = []) {
    $recipients = self::getRecipients($addresses, $scheduledAt);
    if (!isset($document['files']) || empty($document['files'])) {
      return FALSE;
    }
    $file = current($document['files']);
    if (!isset($file['url'])) {
      return FALSE;
    }
    $postLetter = self::composeLetter($recipients, $file['url'], $options);

    $config = self::getConfig();
    $apiInstance = new PostLetterApi(new Client(), $config);
    $result = $apiInstance->postLettersSendPost($postLetter);

    return json_decode($result, true);
  }

  public static function sendPostCard($addresses, $document, $returnAddress, $scheduledAt) {
    $recipients = self::getRecipients($addresses, $scheduledAt);
    if (!isset($document['files']) || empty($document['files'])) {
      return FALSE;
    }

    $fileUrls = [];
    foreach ($document['files'] as $file) {
      if (isset($file['url']) && !empty($file['url'])) {
        $fileUrls[] = $file['url'];
      }
    }
    if (empty($fileUrls)) {
      return FALSE;
    }

    $postCard = self::composePostCard($recipients, $fileUrls, $options);

    $config = self::getConfig();
    $apiInstance = new PostPostcardApi(new Client(), $config);
    $result = $apiInstance->postPostcardsSendPost($postCard);

    return json_decode($result, true);
  }

  // public static function send($addresses, $document, $returnAddress, $scheduledAt, $type) {
  // }

  public static function send($claimReviewCampaign, $testMode = false) {
    $claimReviewCampaign->is_sending = true;
    $claimReviewCampaign->save();
    $config = self::getConfig();
    $template = $claimReviewCampaign->giftCardTemplate;

    $result = self::getAccount();
    $accountGetResult = json_decode($result, true);
    $claimReviewCampaign->balance_before = $accountGetResult["data"]["balance"];
    if ($template && $template->attachment1) {
      $shipments = $claimReviewCampaign->getClickReviewSendOrderShipments()->get();
      $post_recipients = [];
      $delay_hours = $claimReviewCampaign->cs_delay_hours;
      $scheduleBase = time() + 1 * 60 * 60;
      if ($delay_hours) {
        $scheduleBase = time() + $delay_hours * 60 * 60;
      }
      $counter = 0;
      $orderItemMarketingsArray = [];
      foreach ($shipments as $shipment) {
        $oim = $shipment->order_item_marketing;
        if ($oim->is_sent) {
          continue;
        }
        $post_recipient= new PostRecipient();
        $post_recipient->setAddressName($testMode ? "Test ".$shipment->recipient_name : $shipment->recipient_name);
        $post_recipient->setAddressLine1($shipment->ship_address_1);
        $post_recipient->setAddressLine2($shipment->ship_address_2);
        $post_recipient->setAddressCity($shipment->ship_city);
        $post_recipient->setAddressState($shipment->ship_state);
        $post_recipient->setAddressPostalCode($testMode ? '11111' : $shipment->ship_postal_code);
        $post_recipient->setAddressCountry($shipment->ship_country);
        $post_recipient->setReturnAddressId($claimReviewCampaign->cs_return_address_id);
        $scheduled_at = $scheduleBase + $counter;
        $post_recipient->setSchedule($scheduled_at);
        $post_recipients[] = $post_recipient;
        $counter++;
        $oim->click_send_scheduled_send_at = $scheduled_at;
        $orderItemMarketingsArray[] = $oim;
      }
      $orderItemMarketingsCollection = collect($orderItemMarketingsArray);
      
      if ($template->template_type == "letter") {
        $apiInstance = new PostLetterApi(new Client(), $config);
        $isPriority = $claimReviewCampaign->cs_priority_post ? 1 : 0;
        $isDuplex = $claimReviewCampaign->cs_duplex ? 1 : 0;
        $isColored = $claimReviewCampaign->cs_colour ? 1 : 0;
        $post_letter = new PostLetter();
        $post_letter->setFileUrl($template->attachment1_url);
        $post_letter->setPriorityPost($isPriority);
        $post_letter->setRecipients($post_recipients);
        $post_letter->setTemplateUsed(1);
        $post_letter->setDuplex($isDuplex);
        $post_letter->setColour($isColored);
        $result = $apiInstance->postLettersSendPost($post_letter);
        $response = json_decode($result, true);
      } else if ($template->template_type == "post_card" && $template->attachment2) {
        $apiInstance = new PostPostcardApi(new Client(),$config);
        $post_postcards = new PostPostcard();
        $post_postcards->setFileUrls([$template->attachment1_url, $template->attachment2_url]);
        $post_postcards->setRecipients($post_recipients);
        $result = $apiInstance->postPostcardsSendPost($post_postcards);
        $response = json_decode($result, true);    
      }

      if ($response['http_code'] >= 200 && $response['http_code'] < 300) {
        $recipients = $response['data']['recipients'];
        $claimReviewCampaign->actual_sent_amount = $response['data']['total_count'];
        $accountResult = self::getAccount();
        $accountGetResult = json_decode($accountResult, true);
        $claimReviewCampaign->balance_after = $accountGetResult["data"]["balance"];
        $price = ((float)$claimReviewCampaign->balance_before - (float)$claimReviewCampaign->balance_after) / $claimReviewCampaign->actual_sent_amount;
        $claimReviewCampaign->unit_price = $price;
        $claimReviewCampaign->total_cost = (float)$claimReviewCampaign->balance_before - (float)$claimReviewCampaign->balance_after;
        $claimReviewCampaign->sent_at = date('Y-m-d H:i:s');
        $claimReviewCampaign->is_sending = false;
        $claimReviewCampaign->save();
        foreach ($recipients as $recipient) {
          $oim = $orderItemMarketingsCollection->first(function ($value, $key) use($recipient) {
            return $value->click_send_scheduled_send_at == $recipient['schedule'];
          });
          $oim->is_sent = true;
          $oim->click_send_response = json_encode($recipient);
          $oim->save();
        }
        
      } else {
        $claimReviewCampaign->is_sending = false;
        $claimReviewCampaign->save();

        throw new Exception($result);
      }
    }
  }

  public static function syncLetterHistory() {
    $page = 1;
    $limit = 100; // max 100
    $config = self::getConfig();
    $apiInstance = new PostLetterApi(new Client(), $config);
    while (true) {
      echo "page: ". $page. "\r\n";
      $result = json_decode($apiInstance->postLettersHistoryGet($page, $limit), true);
      if ($result['data'] && $result['data']['data']) {
        $data = $result['data']['data'];
        foreach ($data as $record) {
          $history = ClickSendHistory::firstOrNew(['message_id'=>$record['message_id']]);
          $history->type = ClickSendHistory::LETTER_TYPE;
          foreach ($record as $key => $value) {
            $history->$key = $value;
          }
          $history->save();
        }
        if (count($data) < $limit) {
          break;
        } else {
          $page += 1;
        }
      } else {
        break;
      }
    }
  }

  public static function syncPostcardHistory() {
    $page = 1;
    $limit = 100; // max 100
    $config = self::getConfig();
    $apiInstance = new PostPostcardApi(new Client(), $config);
    while (true) {
      echo "page: ". $page. "\r\n";
      $result = json_decode($apiInstance->postPostcardsHistoryGet($page, $limit), true);
      if ($result['data'] && $result['data']['data']) {
        $data = $result['data']['data'];
        foreach ($data as $record) {
          $history = ClickSendHistory::firstOrNew(['message_id'=>$record['message_id']]);
          $history->type = ClickSendHistory::POSTCARD_TYPE;
          foreach ($record as $key => $value) {
            $history->$key = $value;
          }
          $history->save();
        }
        if (count($data) < $limit) {
          break;
        } else {
          $page += 1;
        }
      } else {
        break;
      }
    }
  }
  
}