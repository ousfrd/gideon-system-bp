<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class ProductReviewOverview extends Model
{
    protected $fillable = ['asin', 'country', 'date'];

    public function product() {
        return $this->belongsTo('App\Product', 'asin', 'asin')->where('country', $this->country);
    }

    static function add($asin, $country, $review_quantity, $review_rating, $date) {
        $pro = ProductReviewOverview::firstOrNew([
            "asin" => $asin,
            "country" => $country,
            "date" => $date
        ]);
        $pro->review_quantity = $review_quantity;
        $pro->review_rating = $review_rating;
        $pro->save();
        return response('succeed', 200);
    }

    public static function boot()
    {
        parent::boot();

        self::created(function($model) {
            $product = $model->product;
            $product->review_quantity = $model->review_quantity;
            $product->review_rating = $model->review_rating;
            $product->save();
        });
    }
}
