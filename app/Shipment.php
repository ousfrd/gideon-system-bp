<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use App\Redeem;
use App\ManagedReviewOrder;
use Carbon\Carbon;

class Shipment extends Model
{
	protected $table = 'order_shipments';
	
	//protected $fillable = ['asin','country','content','title','star'];
	
	public $timestamps = false;

	function order_item() {
		return $this->hasOne('App\OrderItem', 'OrderItemId', 'amazon_order_item_id');
	}

	function order_item_marketing() {
		return $this->hasOne('App\OrderItemMarketing', 'oim_amazon_order_id', 'amazon_order_id');
	}

	public function managedReviewOrder() {
		return $this->hasOne('App\ManagedReviewOrder', 'amazon_order_id', 'amazon_order_id');
	}

	public function getRedeemAttribute() {
		return Redeem::where('AmazonOrderId', $this->amazon_order_id)->where('asin', $this->asin)->first();
	}

	function checkDelivered() {
		$carrier = $this->carrier;
		$tracking_number = $this->tracking_number;
	}

	function shippingAddress() {
		return $this->recipient_name . "\n" .
					 $this->ship_address_1 . ' ' . 
					 $this->ship_address_2 . ' ' .
					 $this->ship_address_3 . "\n" .
					 $this->ship_city . ', ' . 
					 $this->ship_state . ', ' .
					 $this->ship_postal_code . "\n" .
					 $this->ship_country;
	}

	function isSelfBought() {
		return !empty($this->managedReviewOrder);
	}

	static function getBy($asin, $marketplace, $startDate, $endDate) {
		$query1 = Listing::select("sku")->where("asin", $asin);
		if (strtoupper($marketplace) != "ALL") {
			$query1->where("country", $marketplace);
		}
		$skus = $query1->get()->pluck('sku');
		$query = Shipment::whereIn("sku", $skus)->whereBetween("payments_date", [$startDate, $endDate]);
		return $query;
	}

	static function getByClaimReviewCampaign($claimReviewCampaign) {
		$asin = $claimReviewCampaign->asin;
		$marketplace = $claimReviewCampaign->marketplace;
		$startDate = $claimReviewCampaign->start_date;
		$endDate = $claimReviewCampaign->end_date;
		$query = Shipment::getBy($asin, $marketplace, $startDate, $endDate);
    return $query;
	}

	static function getWithMarketingStatus() {
		$query = Shipment::select(
									"order_shipments.id as order_shipment_id",
									"order_shipments.amazon_order_id as amazon_order_id",
									"order_shipments.sku as sku",
									"order_shipments.payments_date",
									"order_shipments.purchase_date",
									"order_shipments.product_name",
									"order_shipments.recipient_name",
									"order_shipments.ship_address_1",
									"order_shipments.ship_address_2",
									"order_shipments.ship_address_3",
									"order_shipments.ship_city",
									"order_shipments.ship_state",
									"order_shipments.ship_postal_code",
									"order_shipments.ship_country",
									"order_item_marketings.invite_review",
									"order_item_marketings.self_review",
									"order_item_marketings.email_promotion_review",
									"order_item_marketings.claim_review_id",
									"order_item_marketings.customer_real_email",
									"order_item_marketings.customer_facebook_account",
									"order_item_marketings.customer_wechat_account",
									"seller_listings.asin as asin",
									"users.name as operator"
								)
								->leftJoin('order_item_marketings', function($join) {
									$join->on("order_shipments.amazon_order_id", "=", "order_item_marketings.oim_amazon_order_id");
									$join->on("order_shipments.sku", "=", "order_item_marketings.oim_sku");
								})
								->leftJoin('users', 'users.id', '=', 'order_item_marketings.operator_id')
								->leftJoin('seller_listings', "order_shipments.sku", "=", "seller_listings.sku")
								->orderBy('order_shipment_id', 'desc')
								->groupBy('amazon_order_id');
		return $query;
	}

	static function updateShipmentFieldsFor3Months() {
		$since = (new Carbon('3 months ago'))->format('Y-m-d');
		self::updateShipmentFields($since);
	}

	static function getCountry($asin, $sku) {
		if (empty($asin) || empty($sku)) {
			throw "asin and sku must be a string.";
		} else {
			$shipment = Shipment::where("asin", $asin)->where("sku", $sku)->first();
			$country = null;
			if ($shipment) {
				$salesChannel = strtolower(trim($shipment->sales_channel));
				$marketplaceCountries = array_flip(config("app.marketplaces"));
				if (array_key_exists($salesChannel, $marketplaceCountries)) {
					$country = $marketplaceCountries[$salesChannel];
				}
			}
			return $country;
		}
	}

	// update is_refunded, asin, seller_id
	static function updateShipmentFields($since=null) {
		$batchQuantity = 1000;
		$offset = 0;
		while(true) {
			$query = Shipment::with('order_item')
									->select('id', 'amazon_order_item_id', 'is_refunded');
			if ($since) {
				$query->where('purchase_date', '>', $since);
			}
			$shipments = $query->offset($offset)
												->limit($batchQuantity)
												->get();
			foreach ($shipments as $shipment) {
				$order_item = $shipment->order_item;
				if ($order_item) {
					$shipment->asin = $order_item->ASIN;
					$shipment->seller_id = $order_item->seller_id;
					$shipment->is_refunded = ($order_item->total_refund < 0);
					$shipment->save();
				}
			}
			if (count($shipments) < $batchQuantity) {
				break;
			} else {
				$offset = $offset + $batchQuantity;
			}
		}
		
	}

	public function updateManagedReviewOrder() {
		$managedReviewOrder = $this->managedReviewOrder;
		if ($managedReviewOrder) {
			$managedReviewOrder->fillInfo();
		}
	}

	public function markSkipReviewClaim($skipNote = '') {
		$this->skip_review_claim = TRUE;
		$this->note = $skipNote;
		$this->save();
	}

	public static function getShipments($amazonOrderId, $asin = NULL, $sku = NULL) {
		$queryCond = [['amazon_order_id', '=', $amazonOrderId]];

		if (!empty($asin)) {
			$queryCond[] = ['asin', '=', $asin];
		}

		if (!empty($sku)) {
			$queryCond[] = ['sku', '=', $sku];
		}

		return self::where($queryCond)->get();
	}

	public static function boot()
	{
			parent::boot();
			self::created(function($model) {
				$model->updateManagedReviewOrder();
			});
	}

	
}
