<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InviteReviewLetterTemplate extends Model
{
    protected $fillable = ['name', 'country', 'subject', 'body'];
}
