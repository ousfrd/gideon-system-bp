<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Console\Command;
use DB;
use App\Traits\MassInsertOrUpdate;

class AdKeyword extends Model
{

	use MassInsertOrUpdate;
	
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'ad_keywords';
	
	
	public function account(){
		return $this->belongsTo('App\Account','seller_id','seller_id');
	}

	public function ad_group() {
		return $this->belongsTo('App\AdGroup','adGroupId','adGroupId');
	}	

	public function ad_search_terms() {
		return $this->hasMany('App\AdSearchtermsReport','keywordId','keywordId');
	}
	
	public function ad_keywords_reports() {
		return $this->hasMany('App\AdKeywordsReport','keywordId','keywordId');
	}


	public static function get_data($params) {
		$data = [];

		if($params['seller_id']) {

			$query = AdKeyword::where('seller_id', $params['seller_id']);

			if($params['country']) {
				$query->where('country', $params['country']);
			}

			$adkeywords = $query->get();
		} else {
			$adkeywords = AdKeyword::all();
		}

		foreach ($adkeywords as $adkeyword) {
			$data[$adkeyword->keywordId]['data'] = $adkeyword;
			
			
			// $data[$adgroup->adGroupId]['campaign_name'] = $adgroup->ad_campaign->name;
		}


		$adgroups_query = DB::table('ad_keywords_reports')
                     ->select(DB::raw('adGroupId, campaignName, SUM(attributedSales30d) as total_sales, 
                     	SUM(clicks) as total_clicks, 
                     	SUM(impressions) as total_impressions, 
                     	SUM(cost * exchange_rate) as total_cost,
                     	SUM(attributedConversions30d) as total_orders'));

        if(isset($params['seller_id'])) {
			$adgroups_query->where('seller_id', $params['seller_id']);
		}

		if(isset($params['country'])) {
			$adgroups_query->where('country', $params['country']);
		}

		if(isset($params['start_date']) && isset($params['end_date'])) {	
			$adgroups_query->whereBetween('date', [$params['start_date'], $params['end_date']]);
		}

		$adgroups_data = $adgroups_query->groupBy('adGroupId')->orderBy('total_cost', 'desc')->get();


		foreach ($adgroups_data as $key => $value) {
			$data[$value->adGroupId]['campaign_name'] = $value->campaignName;
			$data[$value->adGroupId]['total_sales'] = $value->total_sales;
			$data[$value->adGroupId]['total_clicks'] = $value->total_clicks;
			$data[$value->adGroupId]['total_impressions'] = $value->total_impressions;
			$data[$value->adGroupId]['total_cost'] = round($value->total_cost,2);
			$data[$value->adGroupId]['total_orders'] = $value->total_orders;
		}

		return $data;


	}


	public static function getKeywords($params) {

		$dataProvider = AdKeyword::join('ad_keywords_reports', 'ad_keywords.keywordId', '=', 'ad_keywords_reports.keywordId')
						->join('ad_groups', 'ad_keywords.adGroupId', '=', 'ad_groups.adGroupId');

		if(isset($params['seller_id']) && $params['seller_id'] && $params['seller_id'] != 'all') {
			$dataProvider->where('ad_keywords.seller_id', $params['seller_id']);

		}

		if(isset($params['country']) && $params['country'] && $params['country'] != 'all') {	
			$dataProvider->where('ad_keywords.country', $params['country']);
		}

		if(isset($params['start_date']) && isset($params['end_date'])) {	
			$dataProvider->whereBetween('ad_keywords_reports.date', [$params['start_date'], $params['end_date']]);
		}

		// if(isset($params['campaignId']) && $params['campaignId']) {	
		// 	$dataProvider->where('ad_keywords.campaignId', $params['campaignId']);
		// }

		if(isset($params['campaignIds']) && $params['campaignIds']) {	
			if(count(json_decode($params['campaignIds'])) == 1) {
				$dataProvider->where('ad_keywords.campaignId', json_decode($params['campaignIds'])[0]);
			} else {
				$dataProvider->whereIn('ad_keywords.campaignId', json_decode($params['campaignIds']));
			}
		}

		if(isset($params['adGroupIds']) && $params['adGroupIds']) {	
			if(count(json_decode($params['adGroupIds'])) == 1) {
				$dataProvider->where('ad_keywords.adGroupId', json_decode($params['adGroupIds'])[0]);
			} else {
				$dataProvider->whereIn('ad_keywords.adGroupId', json_decode($params['adGroupIds']));
			}
		}

		if(isset($params['keywordIds']) && $params['keywordIds']) {	
			if(count(json_decode($params['keywordIds'])) == 1) {
				$dataProvider->where('ad_keywords.keywordId', json_decode($params['keywordIds'])[0]);
			} elseif(count(json_decode($params['keywordIds'])) > 1) {
				$dataProvider->whereIn('ad_keywords.keywordId', json_decode($params['keywordIds']));
			}
		}

		$dataProvider->where('ad_keywords.matchType', 'not like', 'negative%');


		$dataProvider->select(DB::raw('ad_keywords.*, ad_groups.defaultBid, ad_keywords_reports.keywordId, SUM(ad_keywords_reports.clicks) as total_clicks, SUM(ad_keywords_reports.impressions) as total_impressions, ROUND(SUM(ad_keywords_reports.cost * ad_keywords_reports.exchange_rate), 2) as total_cost, SUM(ad_keywords_reports.attributedSales30d) as total_sales, SUM(ad_keywords_reports.attributedConversions30d) as total_orders'));

		$dataProvider->groupBy('ad_keywords_reports.keywordId');


		$data = $dataProvider->get();

		foreach ($data as $key => $value) {
			$value->bid = (float)$value->bid;
			$value->total_clicks = (float)$value->total_clicks;
			$value->total_impressions = (float)$value->total_impressions;
			$value->total_cost = (float)$value->total_cost;
			$value->total_sales = (float)$value->total_sales;
			$value->total_orders = (float)$value->total_orders;

			$value->acos = (isset($value->total_sales) && $value->total_sales > 0) ? round(($value->total_cost / $value->total_sales) * 100, 2) : "0.0";
			$value->cpc = (isset($value->total_clicks) && $value->total_clicks > 0) ? round($value->total_cost / $value->total_clicks, 2) : 0;
			$value->ctr = (isset($value->total_impressions) && $value->total_impressions > 0) ? round(($value->total_clicks / $value->total_impressions) * 100, 2) : 0;
			$value->cr = (isset($value->total_clicks) && $value->total_clicks > 0) ? round(($value->total_orders / $value->total_clicks) * 100, 2) : 0;
		}

		return $data;

	}
}
	