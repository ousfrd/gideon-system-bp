<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\ClickSendClient;

class ClickSendHistory extends Model
{
    protected $guarded = ['id'];

    const LETTER_TYPE = "letter";
    const POSTCARD_TYPE = "postcard";
    
    public static function sync() {
        ClickSendClient::syncLetterHistory();
    }

    public static function boot()
    {
        parent::boot();

        self::saving(function($model) {
            if ($model->isDirty('schedule') && $model->schedule) {
                $model->schedule = Carbon::createFromTimestamp($model->schedule)->toDateTimeString();
            }
            if ($model->isDirty('date_added') && $model->date_added) {
                $model->date_added = Carbon::createFromTimestamp($model->date_added)->toDateTimeString();
            }
            if ($model->_return_address && is_array($model->_return_address)) {
                $model->_return_address = json_encode($model->_return_address);
            }
        });
    }
}
