<?php

namespace App;
use App\Product;
use Illuminate\Database\Eloquent\Model;

class WarehouseActivity extends Model
{
    const INBOUND = 'INBOUND';
    const OUTBOUND = 'OUTBOUND';
    const LOST = 'LOST';
    const DEFECTIVE = 'DEFECTIVE';
    const ACTIVITY_TYPES = [ WarehouseActivity::INBOUND, WarehouseActivity::OUTBOUND ];

    protected $fillable = ['product_id', 'quantity', 'activity_type'];
    protected $appends = ['operator_name'];
    protected $hidden = ['product', 'user'];
    
    public function product() {
        return $this->belongsTo('App\Product');
    }

    public function user() {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function getOperatorNameAttribute() {
        return $this->user->name;
    }

    public static function boot()
    {
        parent::boot();

        self::saving(function($model) {
            if ($model->activity_type != WarehouseActivity::INBOUND) {
                $model->quantity = -abs($model->quantity);
            }
        });

        self::saved(function($model) {
            $model->product->updateLocalWarehouseQyt();
        });

        self::deleted(function($model) {
            $model->product->updateLocalWarehouseQyt();
        });

        self::creating(function($model) {
            $model->created_by = auth()->user()->id;
        });

    }
}   
