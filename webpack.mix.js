const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');

mix.react('resources/assets/js/react_ads.js', 'public/js').version();
mix.react('resources/assets/js/react_daily_profit.js', 'public/js').version();
mix.react('resources/assets/js/react_import_daily_stats.js', 'public/js').version();
mix.react('resources/assets/js/react_seller_profit.js', 'public/js').version();
mix.react('resources/assets/js/ag_react_ads.js', 'public/js').version();
mix.react('resources/assets/js/react_disbursement_report.js', 'public/js').version();
mix.react('resources/assets/js/react_image_upload.js', 'public/js').version();

mix.styles([
   'public/css/dashboard.css',
   'public/css/simple-sidebar.css',
   'public/css/font-awesome-4.6.3/css/font-awesome.min.css',
   'public/js/multiselect/css/bootstrap-multiselect.css',
   'public/css/blessed.css',
   'public/css/app.css',
   'public/colorbox/colorbox.css',
   'public/css/select2.css',
   'public/css/c3.css'
], 'public/css/all.css').version();

mix.js([
   'public/js/main.js'
], 'public/js/all-app.js').version();

if (mix.inProduction()) {
   mix.version();
}

if (!mix.inProduction()) {
   mix.webpackConfig({devtool: 'inline-source-map'}).sourceMaps();
}