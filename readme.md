# 基本信息
* framework: laravel 5.6
* database: mysql
* dependencies: refer composer.json  

# 利润统计系统
* 基于Dover严浩开发的版本，做过一些调整

# 依赖软件

* php>=7.0.0
* composer

# 部署

1.安装PHP和Composer，可参考：[Install Composer](https://getcomposer.org/download/)

2.搭建Laravel运行环境，可参考：[Laravel Deployment](https://laravel.com/docs/5.6/deployment), [PHP Modules](https://laravel.com/docs/5.6/installation)

3.安装依赖的PHP程序库，进入基甸系统根目录后执行下面的命令

`composer install`

4.复制.env.deploy.example为.env.deploy，并将所有的设置信息填充好

`cp .env.deploy.example .env.deploy`

5.服务器端设置好访问项目版本库需要的配置，比如如果是私有版本库，需要设置访问版本库的Deploy Key; 创建共享的.env文件等

6.使用下面的命令进行部署
`./vendor/bin/dep deploy`
