<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

return [
	'api_key' => env('SHIPSTATION_API_KEY', ''),
	'api_secret' => env('SHIPSTATION_API_SECRET', ''),
	'retry_enabled' => env('SHIPSTATION_RETRY_ENABLED', true),
	'max_retry' => env('SHIPSTATION_MAX_RETRY', 10)
];
