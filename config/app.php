<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    */

    // 'name' => env('APP_NAME', 'PL Inventory System'),
    'name' => 'Gideon Warriors',

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'UTC',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => env('APP_LOG', 'single'),

    'log_level' => env('APP_LOG_LEVEL', 'debug'),

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Package Service Providers...
         */
        Laravel\Tinker\TinkerServiceProvider::class,

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,
    		
    	Collective\Html\HtmlServiceProvider::class,
    	Nayjest\Grids\ServiceProvider::class,
    	
    	App\Providers\HelperServiceProvider::class,
    	// Ultraleet\CurrencyRates\CurrencyRatesServiceProvider::class,
        Maatwebsite\Excel\ExcelServiceProvider::class,
        Telegram\Bot\Laravel\TelegramServiceProvider::class,
        // Barryvdh\Debugbar\ServiceProvider::class,
        Rebing\GraphQL\GraphQLServiceProvider::class,
        Barryvdh\Cors\ServiceProvider::class,
        App\Providers\DropboxServiceProvider::class,
        Superbalist\LaravelGoogleCloudStorage\GoogleCloudStorageServiceProvider::class,

        // ShipStation Provider
        App\Providers\ShipStationServiceProvider::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,
    		
		'Html' => Collective\Html\HtmlFacade::class,
		
		'Form' => Collective\Html\FormFacade::class,
		'Grids'     => Nayjest\Grids\Grids::class,
		'Input' => 'Illuminate\Support\Facades\Input',
		'DT'=>App\Helpers\DateHelper::class,
		'CurrencyHelper'=>App\Helpers\CurrencyHelper::class,
        'CurrencyRates' => Ultraleet\CurrencyRates\Facades\CurrencyRates::class,
        'Excel' => Maatwebsite\Excel\Facades\Excel::class,
        'Telegram' => Telegram\Bot\Laravel\Facades\Telegram::class,
        'Debugbar' => Barryvdh\Debugbar\Facade::class,
        'GraphQL' => 'Rebing\GraphQL\Support\Facades\GraphQL',
    ],
		
	'roles'=>[
        'system admin'=>'system admin',
        'admin'=>'admin',
        'product manager'=>'product manager',
        'product manager assistant'=>'product manager assistant',
        'cs'=>'cs',
        'ads manager'=>'ads manager',
        'marketing'=>'marketing',
        'marketing claim review'=>'marketing_claim_review', // 索评
        'marketing invite review'=>'marketing_invite_review', // 测评
        'marketing self review'=>'marketing_self_review', // 自刷单留评
        'marketing email promotion review'=>'marketing_email_review',
        'outsource customer service' => 'outsource_customer_service',
        // 'outsource customer order service' => 'outsource customer order service'
    ],
	'mws'=>['access_key'=>'AKIAIG5ZTZXGUCD7SP5Q',
			'secret_key'=>'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'],
	
	'countries'=>['US'=>'US','UK'=>'UK','DE'=>'DE','FR'=>'FR','ES'=>'ES','IT'=>'IT','CA'=>'CA','MX'=>'MX','JP'=>'JP','AU'=>'AU'],
    'countriesRegion'=>['US'=>'US','UK'=>'UK','DE'=>'UK','FR'=>'UK','ES'=>'UK','IT'=>'UK','CA'=>'CA','MX'=>'MX','JP'=>'JP','AU'=>'AU'],
	'regions'=>['US'=>'US','UK'=>'EU','JP'=>'JP','AU'=>'AU'],
	'currencies'=>['USD','CAD','GBP','EUR','JPY','AUD'],
	'marketplaceIds'=>['US'=>'ATVPDKIKX0DER',
		'BR'=>'A2Q3Y263D00KWC',
		'UK'=>'A1F83G8C2ARO7P',
		'DE'=>'A1PA6795UKMFR9',
		'FR'=>'A13V1IB3VIYZZH',
		'ES'=>'A1RKKUPIHCS9HS',
		'IT'=>'APJ6JRA9NG5V4',
		'CA'=>'A2EUQ1WTGCTBG2',
		'MX'=>'A1AM78C64UM0Y8',
		'JP'=>'A1VC38T7YXB528',
		'IN'=>'A21TJRUUN4KGV',
		'CN'=>'AAHKV2X7AFYLW',
        'AU'=>'A39IBJ37TRP1C6'
	],
    'countriesCurrency' => [
        'US'=>'USD',
        'CA'=>'CAD',
        'MX'=>'MXN',
        'UK'=>'GBP',
        'DE'=>'EUR',
        'FR'=>'EUR',
        'ES'=>'EUR',
        'IT'=>'EUR',
        'JP'=>'JPY',
        'AU'=>'AUD'
    ],
	'currencySymbol' => [
        'USD'=>'$',
        'CAD'=>'C$',
        'MXN'=>'M$',
        'GBP'=>'£',
        'EUR'=>'€',
        'JPY'=>'¥',
        'AUD'=>'A$'
    ],	
	'marketplaces'=> [
				'US'=>'amazon.com',
				'CA'=>'amazon.ca',
				'MX'=>'amazon.com.mx',
				'UK'=>'amazon.co.uk',
				'DE'=>'amazon.de',
				'ES'=>'amazon.es',
				'FR'=>'amazon.fr',
				'IT'=>'amazon.it',
				'JP'=>'amazon.co.jp',
                'AU'=>'amazon.com.au',
	],
		
    'marketplaceRegions' => [
        'US' => ['US'=>'amazon.com','CA'=>'amazon.ca','MX'=>'amazon.com.mx'],
        'UK' => ['UK'=>'amazon.co.uk','DE'=>'amazon.de','ES'=>'amazon.es','FR'=>'amazon.fr','IT'=>'amazon.it'],
        'JP' => ['JP'=>'amazon.co.jp'],
        'CN' => ['CN'=>'amazon.cn'],
        'BR' => ['BR'=>'amazon.com.br'],
        'IN' => ['IN'=>'amazon.in'],
        'AU' => ['AU'=>'amazon.com.au'],
    ],

    'marketplaceCountry' => [
                'amazon.com'=>'US',
                'amazon.ca'=>'CA',
                'amazon.com.mx'=>'MX',
                'amazon.co.uk'=>'UK',
                'amazon.de'=>'DE',
                'amazon.es'=>'ES',
                'amazon.fr'=>'FR',
                'amazon.it'=>'IT',
                'amazon.co.jp'=>'JP',
                'amazon.com.au'=>'AU',
    ],

   'amzLinks'=> [
			'US'=>'https://www.amazon.com/',
			'CA'=>'https://www.amazon.ca/',
			'MX'=>'https://www.amazon.com.mx/',
			'UK'=>'https://www.amazon.co.uk/',
			'DE'=>'https://www.amazon.de/',
			'ES'=>'https://www.amazon.es/',
			'FR'=>'https://www.amazon.fr/',
			'IT'=>'https://www.amazon.it/',
			'JP'=>'https://www.amazon.co.jp/',
            'AU'=>'https://www.amazon.com.au/',
	],
	'timezone'=>'America/Los_Angeles',

    'use_advertising_api' =>  env('USE_ADVERTISING_API', true),
    'lwa_client_id'=> env('LWA_CLIENT_ID'),
    'lwa_client_secret'=> env('LWA_CLIENT_SECRET'),

    'use_asin_watcher' =>  env('USE_ASIN_WATCHER', false),
    'group_chat_id' =>  env('TELEGRAM_GROUP_CHAT_ID'),  

    'use_mailman_service' =>  env('USE_MAILMAN_SERVICE', false),

    'vat_fee_rate'=> 0.167,

    'giftcard_websites'=> [
        'giftandwarranty.com',
        'redeemurgift.com',
        'evaglosscare.com',
    ],    
];
