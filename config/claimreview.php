<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

return [
    'channels' => [
    	env('CLAIM_REVIEW_WEBSITE', 'website') => env('CLAIM_REVIEW_WEBSITE_TITLE', 'Website'),
        // "thank2u.com" => "Website - thank2u.com",
        "email" => "Email"
    ]
];
