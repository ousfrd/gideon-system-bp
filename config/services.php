<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN', 'wkbmail.com'),
        'secret' => env('MAILGUN_SECRET', 'key-f3ef0e61e810aa65793a12d79245fcac'),
    ],

    'ses' => [
        'key' => env('SES_KEY', 'AKIAJXSTSPQT6CZY26KQ'),
        'secret' => env('SES_SECRET', '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'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

];
