<?php
/**
 *  this is a template, you should create a file named tenant.php
 *  under config directory, and copy the content of this file. Then
 *  customize as you want.
 * */ 
return [
  'gateway' => [
    'orders_page' => true,
    'products_page' => true,
    'listings_page' => true,
    'customers_page' => true,
    'reports_page' => true,
    'marketing_page' => true,
    'mrs_page' => true,
    'redeems_page' => true,
    'imports_page' => true,
    'users_page' => true,
    'seller_acounts' => true
  ],
  'app' => [
    'name' => 'Gideon Warriors'
  ],
  'payment' => [
    'until' => '-1', // payment expiration date. format: yyyy-mm-dd. -1 indicates unlimited.
  ]
];