<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

return [
	'storage' => env('GIFTCARD_STORAGE', 'gcs'),
	'invite_review_product_count' => env('INVITE_REVIEW_PRODUCT_COUNT', 4)
];
