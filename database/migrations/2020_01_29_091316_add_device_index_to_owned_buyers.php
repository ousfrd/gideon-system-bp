<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeviceIndexToOwnedBuyers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owned_buyers', function (Blueprint $table) {
            $table->index(["created_by", "device_name"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owned_buyers', function (Blueprint $table) {
            $table->dropIndex(["created_by", "device_name"]);
        });
    }
}
