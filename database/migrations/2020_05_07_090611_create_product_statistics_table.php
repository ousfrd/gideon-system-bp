<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_statistics', function (Blueprint $table) {
            $table->increments('id');

            $table->string('seller_id');
            $table->integer('product_id');
            $table->string('asin');
            $table->string('country');
            $table->date('date');

            $table->integer('total_orders')->default(0);
            $table->integer('fba_orders')->default(0);
            $table->integer('fbm_orders')->default(0);

            $table->integer('organic_orders')->default(0);
            $table->integer('ad_orders')->default(0);

            $table->integer('total_units')->default(0);
            $table->integer('fba_units')->default(0);
            $table->integer('fbm_units')->default(0);

            $table->integer('promo_units')->default(0);

            $table->integer('total_refunded_units')->default(0);
            $table->integer('fba_refunded_units')->default(0);
            $table->integer('fbm_refunded_units')->default(0);

            $table->float('total_gross_sales')->default(0);
            $table->float('fba_gross_sales')->default(0);
            $table->float('fbm_gross_sales')->default(0);

            $table->float('total_shipping')->default(0);
            $table->float('fba_shipping')->default(0);
            $table->float('fbm_shipping')->default(0);

            $table->float('total_refer_fee')->default(0);
            $table->float('fba_refer_fee')->default(0);
            $table->float('fbm_refer_fee')->default(0);

            $table->float('fba_fee')->default(0);
            $table->float('ads_cost')->default(0);
            $table->float('promo_value')->default(0);
            $table->float('other_fee')->default(0);
            $table->float('tax')->default(0);

            $table->float('product_unit_cost')->default(0);
            $table->float('unit_ship_to_amz_cost')->default(0);
            $table->float('fbm_unit_shipping_cost')->default(0);

            $table->integer('self_review_orders')->default(0);
            $table->integer('self_reviews')->default(0);
            $table->float('self_review_cost')->default(0);

            $table->integer('invite_review_orders')->default(0);
            $table->integer('invite_reviews')->default(0);
            $table->float('invite_review_cost')->default(0);

            $table->integer('claim_review_orders')->default(0);
            $table->integer('claim_reviews')->default(0);
            $table->integer('insert_card_reviews')->default(0);
            $table->float('claim_review_cost')->default(0);
            
            $table->timestamps();

            $table->index(['date', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_statistics');
    }
}
