<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateClaimReviewStatisticsColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_review_statistics', function (Blueprint $table) {
            $table->dropColumn(['seller_id', 'sku', 'shipping_orders', 'unshipped_orders']);
            $table->integer('inner_page_claim_review_sent_cnt')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_review_statistics', function (Blueprint $table) {
            $table->dropColumn('inner_page_claim_review_sent_cnt');

            $table->string('seller_id')->nullable(FALSE);
            $table->string('sku')->nullable(FALSE);
            $table->integer('shipping_orders')->unsigned()->nullable(FALSE)->default(0);
            $table->integer('unshipped_orders')->unsigned()->nullable(FALSE)->default(0);
        });
    }
}
