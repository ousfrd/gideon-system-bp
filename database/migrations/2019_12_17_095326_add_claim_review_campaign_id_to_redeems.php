<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClaimReviewCampaignIdToRedeems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('redeems', function (Blueprint $table) {
            $table->integer('claim_review_campaign_id')->nullable();
            $table->string('claim_review_campaign_name')->nullable();
            $table->index('claim_review_campaign_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('redeems', function (Blueprint $table) {
            $table->dropIndex('claim_review_campaign_id');
            $table->dropColumn('claim_review_campaign_id');
            $table->dropColumn('claim_review_campaign_name');
        });
    }
}
