<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInboundShipmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inbound_shipments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('seller_id', 20)->nullable();
			$table->string('shipment_id', 20)->nullable();
			$table->string('shipment_name')->nullable();
			$table->string('shipment_status', 30)->nullable();
			$table->string('label_prep_type', 20)->nullable();
			$table->string('seller_sku', 100)->nullable();
			$table->integer('quantity_shipped')->nullable()->default(0);
			$table->integer('quantity_in_case')->nullable()->default(0);
			$table->integer('quantity_received')->nullable()->default(0);
			$table->string('fulfillment_network_sku', 50)->nullable();
			$table->date('reelease_date')->nullable();
			$table->date('date_updated')->nullable();
			$table->dateTime('date_created')->nullable();
			$table->date('date_shiped')->nullable();
			$table->date('date_closed')->nullable();
			$table->dateTime('updated_at')->nullable();
			$table->decimal('unit_cost', 9)->nullable()->default(0.00);
			$table->decimal('shipping_cost', 9)->nullable()->default(0.00);
			$table->unique(['shipment_id','seller_sku'], 'shipment_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inbound_shipments');
	}

}
