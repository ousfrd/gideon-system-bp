<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGiftwrapOtherFeeToOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('order_items', function (Blueprint $table) {
            
            $table->decimal('other_fee', 9)->nullable()->default(0.00)->after('chargeback');
            $table->decimal('giftwrap_fee', 9)->nullable()->default(0.00)->after('chargeback');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
