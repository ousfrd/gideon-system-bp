<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToOrderShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_shipments', function (Blueprint $table) {
            $table->string('merchant_order_id')->nullable()->after('amazon_order_id');
            $table->date('payments_date')->nullable()->after('amazon_order_item_id');
            $table->date('purchase_date')->nullable()->after('amazon_order_item_id');
            $table->string('merchant_order_item_id')->nullable()->after('amazon_order_item_id');


            $table->string('product_name')->nullable()->after('reporting_date');
            $table->string('sku')->nullable()->after('reporting_date');
            $table->string('buyer_phone_number')->nullable()->after('reporting_date');
            $table->string('buyer_name')->nullable()->after('reporting_date');
            $table->string('buyer_email')->nullable()->after('reporting_date');

            $table->decimal('gift_wrap_tax', 9)->default(0)->after('quantity_shipped'); 
            $table->decimal('gift_wrap_price', 9)->default(0)->after('quantity_shipped');   
            $table->decimal('shipping_tax', 9)->default(0)->after('quantity_shipped'); 
            $table->decimal('shipping_price', 9)->default(0)->after('quantity_shipped'); 
            $table->decimal('item_tax', 9)->default(0)->after('quantity_shipped');  
            $table->decimal('item_price', 9)->default(0)->after('quantity_shipped');
            

            $table->string('bill_country')->nullable()->after('item_promotion_discount');
            $table->string('bill_postal_code')->nullable()->after('item_promotion_discount');
            $table->string('bill_state')->nullable()->after('item_promotion_discount');
            $table->string('bill_city')->nullable()->after('item_promotion_discount');
            $table->string('bill_address_3')->nullable()->after('item_promotion_discount');
            $table->string('bill_address_2')->nullable()->after('item_promotion_discount');
            $table->string('bill_address_1')->nullable()->after('item_promotion_discount');

            $table->string('ship_country')->nullable()->after('item_promotion_discount');
            $table->string('ship_postal_code')->nullable()->after('item_promotion_discount');
            $table->string('ship_state')->nullable()->after('item_promotion_discount');
            $table->string('ship_city')->nullable()->after('item_promotion_discount');
            $table->string('ship_address_3')->nullable()->after('item_promotion_discount');
            $table->string('ship_address_2')->nullable()->after('item_promotion_discount');
            $table->string('ship_address_1')->nullable()->after('item_promotion_discount');

            $table->string('ship_service_level')->nullable()->after('item_promotion_discount');
            $table->string('recipient_name')->nullable()->after('item_promotion_discount');


            $table->string('sales_channel')->nullable()->after('fulfillment_center_id');
            $table->string('fulfillment_channel')->nullable()->after('fulfillment_center_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_shipments', function (Blueprint $table) {
            //
        });
    }
}
