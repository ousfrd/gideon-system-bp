<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInsertCardOrdersFilterToCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('claim_review_campaigns', function (Blueprint $table) {
            $table->boolean('insert_card_orders_filter')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('claim_review_campaigns', function (Blueprint $table) {
            $table->dropColumn('insert_card_orders_filter');
        });
    }
}
