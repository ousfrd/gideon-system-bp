<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalGrossPromoAndTotalGrossUnitsToProductStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('product_statistics', function (Blueprint $table) {
            $table->float('total_gross_promo', 8, 2)->nullable(FALSE)->default(0);
            $table->integer('total_gross_units')->unsigned()->nullable(FALSE)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_statistics', function (Blueprint $table) {
            $table->dropColumn('total_gross_promo');
            $table->dropColumn('total_gross_units');
        });
    }
}
