<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerExportorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_exportors', function (Blueprint $table) {
            $table->increments('id');
            $table->text('seller_accounts')->nullable();
            $table->text('asins')->nullable();
            $table->boolean('is_repeated')->default(false);
            $table->boolean('is_positive_reviewer')->default(false);
            $table->boolean('is_negative_reviewer')->default(false);
            $table->date('first_purchase_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_exportors');
    }
}
