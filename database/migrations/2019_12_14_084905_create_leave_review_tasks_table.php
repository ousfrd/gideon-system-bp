<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveReviewTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_review_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('asin', 40);
            $table->integer('total_review_goal');
            $table->integer('owned_buyer_review_goal')->default(0);
            $table->integer('invite_review_goal')->default(0);
            $table->integer('owned_buyer_approved_reviews_count')->default(0);
            $table->integer('owned_buyer_rejected_reviews_count')->default(0);
            $table->integer('invite_review_approved_reviews_count')->default(0);
            $table->integer('invite_review_rejected_reviews_count')->default(0);
            $table->date('deadline');
            $table->integer('created_by');
            $table->timestamps();

            $table->index('asin');
            $table->index('deadline');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_review_tasks');
    }
}
