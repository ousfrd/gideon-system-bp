<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReviewLinkToManagedReviewOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('managed_review_orders', function (Blueprint $table) {
            $table->datetime('estimated_delivery_date')->nullable();
            $table->string('review_link')->nullable();
            $table->timestamp('left_review_at')->nullable();
            $table->integer('left_review_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('managed_review_orders', function (Blueprint $table) {
            $table->dropColumn('estimated_delivery_date');
            $table->dropColumn('review_link');
            $table->dropColumn('left_review_at');
            $table->dropColumn('left_review_by');
        });
    }
}
