<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToAdReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ad_reports', function (Blueprint $table) {
            $table->string('campaignName')->nullable()->after('campaignId');
            $table->string('adGroupName')->nullable()->after('adGroupId');
            $table->integer('attributedUnitsOrdered1d')->nullable();
            $table->integer('attributedUnitsOrdered7d')->nullable();
            $table->integer('attributedUnitsOrdered14d')->nullable();
            $table->integer('attributedUnitsOrdered30d')->nullable();

            $table->renameColumn('spend', 'cost');

            // $table->dropUnique('idx_ad_reports_date_seller_id_sku_adId');
            $table->unique(['date','seller_id','adId'], 'idx_ad_reports');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
