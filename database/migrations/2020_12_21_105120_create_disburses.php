<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisburses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disburses', function (Blueprint $table) {
            $table->increments('id');
            $table->float('amount')->nullable(FALSE)->default(0);
            $table->float('total_balance')->nullable(FALSE)->default(0);
            $table->float('unavailable_balance')->nullable(FALSE)->default(0);
            $table->boolean('request_transfer_available')->nullable(FALSE)->default(TRUE);
            $table->dateTime('disburse_date')->nullable(FALSE);
            $table->string('seller_id')->nullable(FALSE);
            $table->string('marketplace')->nullable(FALSE);
            $table->string('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disburses');
    }
}
