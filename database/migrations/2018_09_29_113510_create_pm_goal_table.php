<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePmGoalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pm_goal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('goal')->nullable()->default(50000);
            $table->string('year')->nullable();
            $table->string('month')->nullable();
            $table->timestamps();

            $table->unique(['user_id','year','month'], 'unique_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pm_goal2');
    }
}
