<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingPhoneToOwnedBuyerShippingAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owned_buyer_shipping_addresses', function (Blueprint $table) {
            $table->string('shipping_phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owned_buyer_shipping_addresses', function (Blueprint $table) {
            $table->dropColumn('shipping_phone');
        });
    }
}
