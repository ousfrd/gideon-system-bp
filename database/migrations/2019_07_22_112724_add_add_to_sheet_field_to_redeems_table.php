<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddToSheetFieldToRedeemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('redeems', function (Blueprint $table) {
            $table->boolean('is_warehouse_request')->nullable()->after('is_replacement_sent')->default(0); 
            $table->boolean('is_tracking_send')->nullable()->after('is_warehouse_request')->default(0); 
            $table->boolean('is_warehouse_instock')->nullable()->after('is_warehouse_request')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('redeems', function (Blueprint $table) {
            $table->dropColumn('is_warehouse_request');
            $table->dropColumn('is_tracking_send');
            $table->dropColumn('is_warehouse_instock');
        });
    }
}
