<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UnifyProductCostModelsCostColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_shipping_costs', function (Blueprint $table) {
            $table->renameColumn('shipping_cost_per_unit', 'cost_per_unit');
        });

        Schema::table('product_fbm_shipping_costs', function (Blueprint $table) {
            $table->renameColumn('fbm_shipping_cost_per_unit', 'cost_per_unit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_shipping_costs', function (Blueprint $table) {
            $table->renameColumn('cost_per_unit', 'shipping_cost_per_unit');
        });

        Schema::table('product_fbm_shipping_costs', function (Blueprint $table) {
            $table->renameColumn('cost_per_unit', 'fbm_shipping_cost_per_unit');
        });
    }
}
