<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInsertCardReviewCostToSellerListings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('seller_listings', function (Blueprint $table) {
            $table->integer('insert_card_review_cost');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('seller_listings', function (Blueprint $table) {
            $table->dropColumn('insert_card_review_cost');
        });
    }
}
