<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAiAdsToListing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seller_listings', function (Blueprint $table) {
            $table->boolean('ai_ads_open')->default(false);
            $table->float('ai_ads_commission_rate')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seller_listings', function (Blueprint $table) {
            $table->dropColumn('ai_ads_open');
            $table->dropColumn('ai_ads_commission_rate');
        });
    }
}
