<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultToClaimReviewReturnAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_review_return_addresses', function (Blueprint $table) {
            $table->boolean('default')->nullable(FALSE)->default(FALSE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_review_return_addresses', function (Blueprint $table) {
            $table->dropColumn('default');
        });
    }
}
