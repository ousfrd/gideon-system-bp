<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPromotionDiscountsToClaimReviewCampaigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_review_campaigns', function (Blueprint $table) {
            $table->string('promotion_discounts')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_review_campaigns', function (Blueprint $table) {
            $table->dropColumn('promotion_discounts');
        });
    }
}
