<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCancelledOrdersToClaimReviewStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_review_statistics', function (Blueprint $table) {
            $table->integer('cancelled_orders')->unsigned()->nullable(FALSE)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_review_statistics', function (Blueprint $table) {
            $table->dropColumn('cancelled_orders');
        });
    }
}
