<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderPromotionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_promotions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->date('shipment_date')->nullable();
			$table->char('currency', 3)->nullable();
			$table->decimal('item_promotion_discount', 9)->nullable()->default(0.00);
			$table->string('item_promotion_id', 20)->nullable();
			$table->string('description')->nullable();
			$table->integer('promotion_rule_value')->nullable();
			$table->string('amazon_order_id', 20)->nullable()->index('amazon-order-id');
			$table->string('shipment_id', 10)->nullable();
			$table->string('shipment_item_id', 10)->nullable();
			$table->float('exchange_rate', 10, 0)->nullable()->default(1);
			$table->unique(['item_promotion_id','shipment_item_id'], 'item-promotion-id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_promotions');
	}

}
