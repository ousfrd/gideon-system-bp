<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubTaskJsonToLeaveReviewTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave_review_tasks', function (Blueprint $table) {
            $table->text('sub_tasks_json')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave_review_tasks', function (Blueprint $table) {
            $table->dropColumn('sub_tasks_json');
        });
    }
}
