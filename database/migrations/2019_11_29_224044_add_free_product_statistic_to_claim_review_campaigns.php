<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFreeProductStatisticToClaimReviewCampaigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_review_campaigns', function (Blueprint $table) {
            $table->integer('free_product_quantity')->default(0);
            $table->integer('gift_card_quantity')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_review_campaigns', function (Blueprint $table) {
            $table->dropColumn('free_product_quantity');
            $table->dropColumn('gift_card_quantity');
        });
    }
}
