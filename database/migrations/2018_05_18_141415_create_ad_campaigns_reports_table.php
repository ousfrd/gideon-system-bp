<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdCampaignsReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_campaigns_reports', function(Blueprint $table)
        {
            $table->increments('id');
            $table->date('date')->nullable();
            $table->string('seller_id', 20)->nullable()->index('seller_id');
            $table->string('country')->nullable();
            $table->char('currency', 3)->nullable()->default('USD');
            $table->float('exchange_rate', 10, 0)->nullable()->default(1);
            
            $table->string('campaignName')->nullable();
            $table->string('campaignId')->nullable();
            $table->string('campaignStatus')->nullable();
            $table->decimal('campaignBudget', 9, 2)->nullable()->default(0.00);
            $table->integer('impressions')->nullable()->default(0);
            $table->integer('clicks')->nullable()->default(0);
            $table->decimal('cost', 9, 2)->nullable()->default(0.00);

            $table->integer('attributedConversions1d')->nullable();
            $table->integer('attributedConversions7d')->nullable();
            $table->integer('attributedConversions14d')->nullable();
            $table->integer('attributedConversions30d')->nullable();
            $table->integer('attributedConversions1dSameSKU')->nullable();
            $table->integer('attributedConversions7dSameSKU')->nullable();
            $table->integer('attributedConversions14dSameSKU')->nullable();
            $table->integer('attributedConversions30dSameSKU')->nullable();
            $table->integer('attributedUnitsOrdered1d')->nullable();
            $table->integer('attributedUnitsOrdered7d')->nullable();
            $table->integer('attributedUnitsOrdered14d')->nullable();
            $table->integer('attributedUnitsOrdered30d')->nullable();
            $table->decimal('attributedSales1d')->nullable();
            $table->decimal('attributedSales7d')->nullable();
            $table->decimal('attributedSales14d')->nullable();
            $table->decimal('attributedSales30d')->nullable();
            $table->decimal('attributedSales1dSameSKU')->nullable();
            $table->decimal('attributedSales7dSameSKU')->nullable();
            $table->decimal('attributedSales14dSameSKU')->nullable();
            $table->decimal('attributedSales30dSameSKU')->nullable();
            
            $table->unique(['date','seller_id','campaignId'], 'idx_ad_campaigns_reports');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}