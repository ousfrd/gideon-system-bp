<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivvyTransactionRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('divvy_transaction_records', function (Blueprint $table) {
            $table->string('transaction_id')->unique();
            $table->string('split_from')->nullable();
            $table->string('date')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('merchant')->nullable();
            $table->string('clean_merchant_name')->nullable();
            $table->string('amount');
            $table->decimal('amount_in_number', 6, 2);
            $table->string('card_name')->nullable();
            $table->string('card_type')->nullable();
            $table->string('card_last_4');
            $table->string('card_exp_date')->nullable();
            $table->string('reviews')->nullable();
            $table->string('receipt_filename')->nullable();
            $table->string('budget_id')->nullable();
            $table->string('card_id')->nullable();
            $table->string('user_id')->nullable();
            $table->string('authorized_at')->nullable();
            $table->string('budget')->nullable();
            $table->string('has_receipt')->nullable();
            $table->timestamps();

            $table->index('card_last_4');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('divvy_transaction_records');
    }
}
