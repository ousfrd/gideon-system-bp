<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChargeMethodIdToOwnedBuyerOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owned_buyer_orders', function (Blueprint $table) {
            $table->integer('owned_buyer_payment_method_id');
            $table->index('owned_buyer_payment_method_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owned_buyer_orders', function (Blueprint $table) {
            $table->dropIndex('owned_buyer_payment_method_id');
            $table->dropColumn('owned_buyer_payment_method_id');
        });
    }
}
