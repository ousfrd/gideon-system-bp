<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClickSendHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('click_send_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('message_id');
            $table->string('type');
            $table->integer('user_id');
            $table->integer('subaccount_id')->nullable();
            $table->string('list_id')->nullable();
            $table->string('address_name');
            $table->string('address_line_1');
            $table->string('address_line_2')->nullable();;
            $table->string('address_city');
            $table->string('address_state');
            $table->string('address_postal_code');
            $table->string('address_country');
            $table->integer('return_address_id');
            $table->string('custom_string')->nullable();
            $table->timestamp('schedule')->nullable();
            $table->string('source')->nullable();
            $table->integer('colour')->nullable();
            $table->integer('duplex')->nullable();
            $table->integer('priority_post')->nullable();
            $table->integer('post_pages')->nullable();
            $table->float('post_price')->nullable();
            $table->timestamp('date_added')->nullable();
            $table->string('status_code')->nullable();
            $table->string('status_text')->nullable();
            $table->string('status')->nullable();
            $table->string('_file_url')->nullable();
            $table->text('_return_address')->nullable();
            $table->string('_api_username');
            $table->timestamps();
            $table->index('message_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('click_send_histories');
    }
}
