<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameNoteToGiftCardTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gift_card_templates', function (Blueprint $table) {
            $table->string("name");
            $table->text("note");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gift_card_templates', function (Blueprint $table) {
            $table->dropColumn("name");
            $table->dropColumn("note");
        });
    }
}
