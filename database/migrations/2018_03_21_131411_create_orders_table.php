<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('seller_id', 20)->nullable()->index('seller_id');
			$table->string('AmazonOrderId', 20)->nullable()->index('AmazonOrderId');
			$table->string('OrderStatus', 10)->nullable()->index('OrderStatus');
			$table->dateTime('PurchaseDate')->nullable()->index('PurchaseDate');
			$table->dateTime('LastUpdateDate')->nullable();
			$table->string('MarketplaceId', 15)->nullable();
			$table->string('SalesChannel', 20)->nullable();
			$table->decimal('OrderTotal', 9)->nullable();
			$table->decimal('OrderTotalUSD', 9)->nullable();
			$table->string('Currency', 10)->nullable();
			$table->string('ShippingAddressName', 100)->nullable();
			$table->string('ShippingAddressAddressLine1')->nullable();
			$table->string('ShippingAddressAddressLine2')->nullable();
			$table->string('ShippingAddressAddressLine3')->nullable();
			$table->string('ShippingAddressCity', 100)->nullable();
			$table->string('ShippingAddressStateOrRegion', 100)->nullable();
			$table->string('ShippingAddressPostalCode', 50)->nullable();
			$table->string('ShippingAddressCountryCode', 2)->nullable();
			$table->string('ShippingAddressDistrict', 100)->nullable();
			$table->string('ShippingAddressCounty', 100)->nullable();
			$table->string('ShippingAddressPhone', 20)->nullable();
			$table->string('ShipmentServiceLevelCategory', 20)->nullable();
			$table->string('ShipServiceLevel', 50)->nullable();
			$table->dateTime('EarliestShipDate')->nullable();
			$table->dateTime('LatestShipDate')->nullable();
			$table->string('OrderType', 20)->nullable();
			$table->string('BuyerName', 100)->nullable();
			$table->string('BuyerEmail', 100)->nullable();
			$table->string('FulfillmentChannel', 20)->nullable()->index('FulfillmentChannel');
			$table->integer('NumberOfItemsShipped')->nullable()->default(0);
			$table->integer('NumberOfItemsUnshipped')->nullable()->default(0);
			$table->string('PaymentMethod', 20)->nullable();
			$table->boolean('IsBusinessOrder')->nullable()->default(0);
			$table->string('PurchaseOrderNumber', 10)->nullable();
			$table->boolean('IsPrime')->nullable()->default(0);
			$table->boolean('IsPremiumOrder')->nullable()->default(0);
			$table->boolean('NumberOfItemFulfilled')->nullable()->default(0);
			$table->boolean('new_fetched')->nullable()->default(1);
			$table->boolean('greeting_email_sent')->nullable()->default(0);
			$table->boolean('delivered_email_sent')->nullable()->default(0);
			$table->boolean('product_review_email_sent')->nullable()->default(0);
			$table->date('delivered_date')->nullable();
			$table->unique(['seller_id','AmazonOrderId'], 'unique_seller_id_AmazonOrderId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
