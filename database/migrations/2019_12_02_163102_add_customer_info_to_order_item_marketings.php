<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomerInfoToOrderItemMarketings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_item_marketings', function (Blueprint $table) {
            $table->string("customer_real_email", 255)->nullable();
            $table->string("customer_facebook_account", 255)->nullable();
            $table->string("customer_wechat_account", 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_item_marketings', function (Blueprint $table) {
            $table->dropColumn("customer_real_email");
            $table->dropColumn("customer_facebook_account");
            $table->dropColumn("customer_wechat_account");
        });
    }
}
