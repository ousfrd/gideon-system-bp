<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSellerDealsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('seller_deals', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('seller_id', 20)->nullable();
			$table->string('deal_id', 50)->nullable()->unique();
			$table->text('deal_description', 16777215)->nullable();
			$table->dateTime('posted_date')->nullable();
			$table->string('event_type', 50)->nullable();
			$table->string('fee_type', 50)->nullable();
			$table->decimal('fee_amount', 9)->nullable()->default(0.00);
			$table->decimal('tax_amount', 9)->nullable()->default(0.00);
			$table->decimal('total_amount', 9)->nullable()->default(0.00);
			$table->char('currency', 3)->nullable()->default('USD');
			$table->decimal('exchange_rate', 9, 4)->nullable()->default(1.0000);
		});

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('seller_deals');
	}

}
