<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOverviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_overviews', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('marketplace')->nullable(FALSE);
            $table->string('asin')->nullable(FALSE);

            $table->float('total_revenue')->default(0);
            $table->float('total_cost')->default(0);
            $table->float('total_gross')->default(0);
            $table->float('shipping_fee')->default(0);
            $table->float('vat_fee')->default(0);
            $table->float('fba_fee')->default(0);
            $table->float('total_refund')->default(0);
            $table->float('tax')->default(0);
            $table->float('commission')->default(0);
            $table->float('giftwrap_fee')->default(0);
            $table->float('other_fee')->default(0);
            $table->float('profit')->default(0);
            $table->float('payout')->default(0);
            $table->float('ad_expenses')->default(0);
            $table->float('ads_sales')->default(0);
            $table->integer('total_orders')->default(0);
            $table->integer('total_units')->default(0);
            $table->integer('promo_units')->default(0);
            $table->integer('ppc_count')->default(0);

            $table->date('date')->nullable(FALSE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_overviews');
    }
}
