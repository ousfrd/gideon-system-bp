<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubjectAndRedeemIdToInviteReviewLetters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invite_review_letters', function (Blueprint $table) {
            $table->string('subject');
            $table->integer('redeem_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invite_review_letters', function (Blueprint $table) {
            $table->dropColumn('subject');
            $table->dropColumn('redeem_id');
        });
    }
}
