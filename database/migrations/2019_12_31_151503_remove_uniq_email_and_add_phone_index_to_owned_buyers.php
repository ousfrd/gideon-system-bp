<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUniqEmailAndAddPhoneIndexToOwnedBuyers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owned_buyers', function (Blueprint $table) {
            $table->dropUnique(['email']);
            $table->index('phone')->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owned_buyers', function (Blueprint $table) {
            $table->index('email')->unique();
            $table->dropUnique(['phone']);
        });
    }
}
