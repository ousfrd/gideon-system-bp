<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ad_payments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('invoice_id', 20)->nullable();
			$table->string('seller_id', 20)->nullable();
			$table->dateTime('posted_date')->nullable();
			$table->string('type', 20)->nullable();
			$table->decimal('amount', 9)->nullable();
			$table->char('currency', 3)->nullable()->default('USD');
			$table->decimal('exchange_rate', 9, 4)->nullable()->default(1.0000);
			$table->unique(['invoice_id','type'], 'invoice_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ad_payments');
	}

}
