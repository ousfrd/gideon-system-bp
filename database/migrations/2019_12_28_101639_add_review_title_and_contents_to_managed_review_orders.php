<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReviewTitleAndContentsToManagedReviewOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('managed_review_orders', function (Blueprint $table) {
            $table->string('review_title')->nullable();
            $table->text('review_content')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('managed_review_orders', function (Blueprint $table) {
            $table->dropColumn('review_title');
            $table->dropColumn('review_content');
        });
    }
}
