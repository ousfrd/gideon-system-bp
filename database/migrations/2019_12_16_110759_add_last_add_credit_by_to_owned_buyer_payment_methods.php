<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastAddCreditByToOwnedBuyerPaymentMethods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owned_buyer_payment_methods', function (Blueprint $table) {
            $table->integer('last_add_credit_by')->nullable();
            $table->index('owned_buyer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owned_buyer_payment_methods', function (Blueprint $table) {
            $table->dropColumn('last_add_credit_by');
            $table->dropIndex('owned_buyer_id');
        });
    }
}
