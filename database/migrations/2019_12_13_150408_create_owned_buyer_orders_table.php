<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnedBuyerOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owned_buyer_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('owned_buyer_id');
            $table->string('amazon_order_id');
            $table->string('order_type'); // other sellers, or our own sellers
            $table->decimal('order_cost', 8, 2)->nullable();
            $table->boolean('status')->default(0); // 0: not determined, order report not downloaded yet.
            $table->boolean('review_submitted')->default(0);
            $table->string('seller_id')->nullable();
            $table->string('asin')->nullable();
            $table->timestamps();
            $table->index('owned_buyer_id');
            $table->index('amazon_order_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owned_buyer_orders');
    }
}
