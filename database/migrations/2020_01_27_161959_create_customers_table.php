<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('amazon_buyer_email')->nullable();
            $table->string('email')->nullable();
            $table->string('name')->nullable();
            $table->string('facebook_account')->nullable();
            $table->string('wechat_account')->nullable();
            $table->text('bought_orders')->nullable();
            $table->integer('bought_orders_count')->default(0);
            $table->text('leave_reviews')->nullable();
            $table->integer('leave_reviews_count')->default(0);
            $table->text('leave_possitive_reviews')->nullable();
            $table->integer('leave_possitive_reviews_count')->default(0);
            $table->text('leave_negative_reviews')->nullable();
            $table->integer('leave_negative_reviews_count')->default(0);
            $table->text('claim_reviews')->nullable();
            $table->integer('claim_reviews_count')->default(0);
            $table->text('invite_reviews')->nullable();
            $table->integer('invite_reviews_count')->default(0);
            $table->timestamps();
            $table->index('email');
            $table->index('amazon_buyer_email');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
