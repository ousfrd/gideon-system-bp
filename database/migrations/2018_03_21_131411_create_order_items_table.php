<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_items', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('AmazonOrderId', 20)->nullable()->index('AmazonOrderId');
			$table->string('ASIN', 10)->nullable()->index('ASIN');
			$table->string('OrderItemId', 15)->nullable();
			$table->string('SellerSKU', 100)->nullable()->index('SellerSKU');
			$table->string('Title', 1000)->nullable();
			$table->integer('QuantityOrdered')->nullable()->default(0);
			$table->integer('QuantityShipped')->nullable()->default(0);
			$table->decimal('ItemPrice', 9)->nullable();
			$table->string('Currency', 10)->nullable();
			$table->decimal('ShippingPrice', 9)->nullable();
			$table->decimal('Revenue', 9)->nullable();
			$table->string('AMZ_Link')->nullable();
			$table->decimal('AMZ_Price', 9)->nullable();
			$table->dateTime('Last_Updated_Date')->nullable();
			$table->dateTime('ScheduledDeliveryEndDate')->nullable();
			$table->dateTime('ScheduledDeliveryStartDate')->nullable();
			$table->string('GiftMessageText')->nullable();
			$table->decimal('cost', 9)->nullable()->default(0.00);
			$table->decimal('shipping_cost', 9)->nullable()->default(0.00);
			$table->decimal('ItemPriceUSD', 9)->nullable();
			$table->decimal('ShippingPriceUSD', 9)->nullable();
			$table->string('PromotionId', 100)->nullable();
			$table->char('country', 2)->nullable()->index('country');
			$table->decimal('fba_fee', 9)->nullable()->default(0.00);
			$table->decimal('tax', 9)->nullable()->default(0.00);
			$table->float('exchange_rate', 10, 0)->nullable()->default(1);
			$table->decimal('shipping_promo', 9)->nullable()->default(0.00);
			$table->decimal('item_promo', 9)->nullable()->default(0.00);
			$table->decimal('total_promo', 9)->nullable()->default(0.00);
			$table->decimal('total_refund', 9)->nullable()->default(0.00);
			$table->char('region', 2)->nullable()->default('US')->index('region');
			$table->decimal('commission', 9)->nullable()->default(0.00);
			$table->decimal('chargeback', 9)->nullable()->default(0.00);
			$table->unique(['AmazonOrderId','OrderItemId'], 'unique_AmazonOrderId_OrderItemId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_items');
	}

}
