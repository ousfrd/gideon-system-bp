<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnsToOrderItemMarketing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_item_marketings', function (Blueprint $table) {
            $table->renameColumn('sku', 'oim_sku');
            $table->renameColumn('amazon_order_id', 'oim_amazon_order_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_item_marketings', function (Blueprint $table) {
            $table->renameColumn('oim_sku', 'sku');
            $table->renameColumn('oim_amazon_order_id', 'amazon_order_id');
        });
    }
}
