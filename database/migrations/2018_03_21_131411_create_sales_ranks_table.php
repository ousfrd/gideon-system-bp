<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSalesRanksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales_ranks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('asin', 10)->nullable();
			$table->text('sales_ranks')->nullable();
			$table->date('date')->nullable();
			$table->unique(['asin','date'], 'unique_asin_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
