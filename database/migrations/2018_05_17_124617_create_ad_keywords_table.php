<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_keywords', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('seller_id', 20)->nullable()->index('seller_id');
            $table->string('country')->nullable();
            $table->string('keywordId')->nullable();
            $table->string('adGroupId')->nullable()->index('adGroupId');
            $table->string('campaignId')->nullable()->index('campaignId');
            $table->string('keywordText')->nullable();
            $table->string('matchType')->nullable();
            $table->string('state')->nullable();
            $table->timestamps();
            $table->unique(['keywordId', 'adGroupId', 'campaignId'], 'unique_adkeyword_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
