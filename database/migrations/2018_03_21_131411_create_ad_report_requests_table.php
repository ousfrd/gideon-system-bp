<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdReportRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ad_report_requests', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('seller_id');
			$table->string('country');
			$table->string('recordType');
			$table->string('campaignType')->default('sponsoredProducts');
			$table->string('segment')->nullable();
			$table->date('reportDate');
			$table->string('metrics', 1000);
			$table->string('reportId');
			$table->timestamps();
			$table->unique(['seller_id','country','recordType','reportDate'], 'idx_ad_report_requests');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ad_report_requests');
	}

}
