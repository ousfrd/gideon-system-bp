<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNegtiveToPositiveQuantity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_review_campaigns', function (Blueprint $table) {
            $table->integer('negtive_to_positive_quantity')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_review_campaigns', function (Blueprint $table) {
            $table->dropColumn('negtive_to_positive_quantity');
        });
    }
}
