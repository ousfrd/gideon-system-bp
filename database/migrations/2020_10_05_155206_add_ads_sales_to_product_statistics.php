<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdsSalesToProductStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_statistics', function (Blueprint $table) {
            $table->float('ads_sales', 8, 2)->nullable(FALSE)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_statistics', function (Blueprint $table) {
            $table->dropColumn('ads_sales');
        });
    }
}
