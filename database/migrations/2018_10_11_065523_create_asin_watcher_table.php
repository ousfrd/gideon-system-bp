<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsinWatcherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asin_watcher', function (Blueprint $table) {
            $table->increments('id');

            $table->string('asin', 10)->nullable()->index('asin_idx');
            $table->char('country', 2)->nullable()->index('country_idx');
            $table->date('date')->nullable()->index('date_idx');

            $table->integer('reviews')->nullable();
            $table->decimal('stars', 2, 1)->nullable();
            $table->string('buybox')->nullable();
            $table->decimal('price', 8, 2)->nullable();
            $table->integer('critical_reviews')->nullable();


            $table->timestamps();
            $table->unique(['asin','country','date'], 'asin_country_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asin_watcher');
    }
}
