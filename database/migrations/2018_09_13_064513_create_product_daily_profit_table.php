<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDailyProfitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_daily_profit', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('asin', 10)->nullable()->index('asin_idx');
            $table->char('country', 2)->nullable()->index('country_idx');
            $table->char('region', 2)->nullable();
            $table->string('seller_id', 20)->nullable()->index('seller_id_idx');
            $table->date('date')->nullable()->index('date_idx');
            $table->integer('orders')->nullable()->default(0);
            $table->decimal('profit', 9)->nullable()->default(0.00);
            $table->decimal('total_gross', 9)->nullable()->default(0.00);
            $table->decimal('refund', 9)->nullable()->default(0.00);
            $table->decimal('payout', 9)->nullable()->default(0.00);
            $table->decimal('product_cost', 9)->nullable()->default(0.00);
            $table->decimal('ad_expenses', 9)->nullable()->default(0.00);
            $table->decimal('total_cost', 9)->nullable()->default(0.00);

            $table->timestamps();
            $table->unique(['asin','country','date','seller_id'], 'seller_asin_country_date');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_daily_profit');
    }
}
