<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveReviewSubTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_review_sub_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('leave_review_task_id');
            $table->integer('user_id');
            $table->integer('goal')->default(0);
            $table->integer('progress')->default(0);
            $table->timestamps();
            $table->index('leave_review_task_id');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_review_sub_tasks');
    }
}
