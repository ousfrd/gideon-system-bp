<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReviewRelatedFieldsToOrderItemMarketings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_item_marketings', function (Blueprint $table) {
            $table->string('review_title')->nullable();
            $table->text('review_content')->nullable();
            $table->string('review_link', 1000)->nullable();
            $table->timestamp('left_review_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_item_marketings', function (Blueprint $table) {
            $table->dropColumn('review_title');
            $table->dropColumn('review_content');
            $table->dropColumn('review_link', 1000);
            $table->dropColumn('left_review_at');
        });
    }
}
