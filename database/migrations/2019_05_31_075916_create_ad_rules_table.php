<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_rules', function (Blueprint $table) {

            $table->increments('id');            
            $table->json('conditions')->nullable();
            $table->string('exec_time')->nullable();
            $table->boolean('rule_active')->default(False);            
            $table->boolean('change_campaign_open')->default(True);            
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_rules');
    }
}
