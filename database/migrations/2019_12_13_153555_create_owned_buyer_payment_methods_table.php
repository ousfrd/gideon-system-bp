<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnedBuyerPaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owned_buyer_payment_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('owned_buyer_id');
            $table->string('card_number');
            $table->string('bank_name')->nullable();
            $table->boolean('status')->default(1);
            $table->decimal('total_spent', 8, 2)->default(0);
            $table->integer('available_balance')->default(0);
            $table->integer('add_credit')->default(0);
            $table->timestamp('last_add_credit_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owned_buyer_payment_methods');
    }
}
