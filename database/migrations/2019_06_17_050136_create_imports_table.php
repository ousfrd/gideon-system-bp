<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('seller_id', 20)->nullable();
            $table->string('type')->nullable();
            $table->string('filename')->nullable();
            $table->string('country', 10)->nullable();
            $table->string('status')->nullable()->default("pending");
            $table->timestamps();
            $table->unique(['seller_id', 'type', 'filename'], 'imports_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imports');
    }
}
