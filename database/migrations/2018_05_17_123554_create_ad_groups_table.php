<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_groups', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('seller_id', 20)->nullable()->index('seller_id');
            $table->string('country')->nullable();
            $table->string('adGroupId')->nullable()->index('adGroupId');
            $table->string('name')->nullable();
            $table->string('campaignId')->nullable()->index('campaignId');
            $table->decimal('defaultBid', 6, 2)->nullable();
            $table->string('state')->nullable();
            $table->timestamps();
            $table->unique(['adGroupId', 'campaignId'], 'unique_adgroup_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
