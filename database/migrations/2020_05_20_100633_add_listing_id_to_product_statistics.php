<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddListingIdToProductStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_statistics', function (Blueprint $table) {
            $table->integer('listing_id')->nullable(true)->after('product_id');
            $table->string('sku')->nullable(true)->after('asin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_statistics', function (Blueprint $table) {
            $table->dropColumn('listing_id');
            $table->dropColumn('sku');
        });
    }
}
