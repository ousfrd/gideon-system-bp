<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductExcessInventoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_excess_inventory', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('seller_id', 20)->nullable()->index('seller_id');
			$table->string('sku', 100)->nullable();
			$table->char('country', 2)->nullable();
			$table->date('date')->nullable();
			$table->integer('listing_id')->nullable()->index('listing_id');
			$table->string('asin', 10)->nullable();
			$table->integer('qty')->nullable()->default(0);
			$table->integer('estimated_excess')->nullable()->default(0);
			$table->integer('days_of_supply')->nullable()->default(0);
			$table->integer('excess_threshold')->nullable()->default(0);
			$table->integer('unit_sold_7')->nullable()->default(0);
			$table->integer('unit_sold_30')->nullable()->default(0);
			$table->integer('unit_sold_60')->nullable()->default(0);
			$table->integer('unit_sold_90')->nullable()->default(0);
			$table->string('alert')->nullable();
			$table->string('recommended_action')->nullable();
			$table->integer('healthy_inventory_level')->nullable();
			$table->string('your_price', 50)->nullable();
			$table->decimal('recommended_sales_price', 9)->nullable()->default(0.00);
			$table->integer('recommended_sale_duration')->nullable()->default(0);
			$table->decimal('estimated_total_storage_cost', 9)->nullable()->default(0.00);
			$table->integer('recommended_removal_quantity')->nullable()->default(0);
			$table->decimal('estimated_cost_savings_of_removal', 9)->nullable()->default(0.00);
			$table->integer('total_qty')->nullable()->default(0);
			$table->unique(['sku','country'], 'sku');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_excess_inventory');
	}

}
