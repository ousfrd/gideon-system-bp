<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSellerIdToRedeemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('redeems', function (Blueprint $table) {

            $table->string('seller_id', 20)->nullable()->after('requestDate');
            $table->string('amount')->nullable()->after('asin');
            $table->string('source')->nullable()->after('asin');
            $table->string('country', 2)->nullable()->after('asin');
            $table->boolean('is_reviewed')->nullable()->after('newsletter')->default(0);
            $table->boolean('is_giftcard_sent')->nullable()->after('is_reviewed')->default(0);
            $table->boolean('is_refunded')->nullable()->after('is_giftcard_sent')->default(0);
            $table->boolean('is_replied')->nullable()->after('is_refunded')->default(0);
            $table->boolean('is_replacement_sent')->nullable()->after('is_replied')->default(0);
            $table->string('tracking_number')->nullable()->after('is_replacement_sent');
            $table->string('handling_status')->nullable()->after('tracking_number');
            $table->text('note')->nullable()->after('handling_status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('redeems', function (Blueprint $table) {
            //
        });
    }
}
