<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInstantTransferBalanceToDisburse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('disburses', function (Blueprint $table) {
            $table->float('instant_transfer_balance')->nullable(FALSE)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('disburses', function (Blueprint $table) {
            $table->dropColumn('instant_transfer_balance');
        });
    }
}
