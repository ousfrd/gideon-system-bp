<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRealAccountsFiltersToCustomerExportors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_exportors', function (Blueprint $table) {
            $table->boolean('has_real_email')->default(false);
            $table->boolean('has_wechat_account')->default(false);
            $table->boolean('has_facebook_account')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_exportors', function (Blueprint $table) {
            $table->dropColumn('has_real_email');
            $table->dropColumn('has_wechat_account');
            $table->dropColumn('has_facebook_account');
        });
    }
}
