<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductInventoryDailyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_inventory_daily', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('listing_id')->nullable()->index('listing_id');
			$table->string('sku', 100)->nullable();
			$table->string('asin', 10)->nullable()->index('asin_idx');
			$table->char('country', 2)->nullable();
			$table->string('seller_id', 20)->nullable();
			$table->boolean('status')->nullable()->default(1);
			$table->string('item_condition', 20)->nullable();
			$table->string('varehouse_condition_code', 20)->nullable();
			$table->string('fnsku', 100)->nullable()->default('Amazon');
			$table->integer('warehouse_quantity')->nullable()->default(0);
			$table->integer('fulfillable_quantity')->nullable()->default(0);
			$table->integer('unsellable_quantity')->nullable()->default(0);
			$table->integer('reserved_quantity')->nullable()->default(0);
			$table->integer('total_quantity')->nullable()->default(0);
			$table->integer('inbound_working_quantity')->nullable()->default(0);
			$table->integer('inbound_shipped_quantity')->nullable()->default(0);
			$table->integer('inbound_receiving_quantity')->nullable()->default(0);
			$table->decimal('price', 9)->nullable()->default(0.00);
			$table->string('afn_listing_exists', 20)->nullable();
			$table->date('date')->nullable();
			$table->dateTime('last_updated_at')->nullable();
			$table->unique(['sku','country','date'], 'sku');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_inventory_daily');
	}

}
