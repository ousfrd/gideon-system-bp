<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderShipmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_shipments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('amazon_order_id', 20)->nullable()->index('amazon_order_id');
			$table->string('shipment_id', 10)->nullable();
			$table->string('shipment_item_id', 10)->nullable();
			$table->string('amazon_order_item_id', 20)->nullable()->index('amazon_order_item_id');
			$table->dateTime('shipment_date')->nullable();
			$table->dateTime('reporting_date')->nullable();
			$table->integer('quantity_shipped')->nullable()->default(0);
			$table->decimal('ship_promotion_discount', 9)->nullable()->default(0.00);
			$table->decimal('item_promotion_discount', 9)->nullable()->default(0.00);
			$table->string('carrier', 50)->nullable();
			$table->string('tracking_number', 100)->nullable();
			$table->dateTime('estimated_arrival_date')->nullable();
			$table->string('fulfillment_center_id', 10)->nullable();
			$table->char('currency', 3)->nullable();
			$table->float('exchange_rate', 10, 0)->nullable()->default(1);
			$table->unique(['shipment_item_id','shipment_id'], 'shipment_item_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_shipments');
	}

}
