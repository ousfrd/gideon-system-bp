<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductReviewOverviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_review_overviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('asin');
            $table->string('country');
            $table->integer('review_quantity')->default(0);
            $table->float('review_rating');
            $table->date('date');
            $table->timestamps();
            $table->index(['asin', 'country', 'date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_review_overviews');
    }
}
