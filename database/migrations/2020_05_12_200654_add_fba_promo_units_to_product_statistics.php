<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFbaPromoUnitsToProductStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_statistics', function (Blueprint $table) {
            $table->dropColumn('promo_units');
            $table->dropColumn('promo_value');
            $table->dropColumn('other_fee');
            $table->dropColumn('tax');

            $table->integer('fba_promo_orders')->default(0);
            $table->integer('fbm_promo_orders')->default(0);
            $table->integer('total_promo_orders')->default(0);

            $table->integer('fba_promo_value')->default(0);
            $table->integer('fbm_promo_value')->default(0);
            $table->integer('total_promo_value')->default(0);

            $table->integer('fba_other_fee')->default(0);
            $table->integer('fbm_other_fee')->default(0);
            $table->integer('total_other_fee')->default(0);

            $table->integer('fba_tax')->default(0);
            $table->integer('fbm_tax')->default(0);
            $table->integer('total_tax')->default(0);

            $table->integer('fba_vat_fee')->default(0);
            $table->integer('fbm_vat_fee')->default(0);
            $table->integer('total_vat_fee')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_statistics', function (Blueprint $table) {
            $table->integer('promo_units')->default(0);
            $table->integer('promo_value')->default(0);
            $table->integer('other_fee')->default(0);
            $table->integer('tax')->default(0);

            $table->dropColumn('fba_promo_orders');
            $table->dropColumn('fbm_promo_orders');
            $table->dropColumn('total_promo_orders');

            $table->dropColumn('fba_promo_value');
            $table->dropColumn('fbm_promo_value');
            $table->dropColumn('total_promo_value');

            $table->dropColumn('fba_other_fee');
            $table->dropColumn('fbm_other_fee');
            $table->dropColumn('total_other_fee');

            $table->dropColumn('fba_tax');
            $table->dropColumn('fbm_tax');
            $table->dropColumn('total_tax');

            $table->dropColumn('fba_vat_fee');
            $table->dropColumn('fbm_vat_fee');
            $table->dropColumn('total_vat_fee');
        });
    }
}
