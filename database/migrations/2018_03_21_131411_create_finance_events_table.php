<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFinanceEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('finance_events', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('amazon_order_id', 20)->nullable()->index('amazon_order_id_index');
			$table->string('sku', 100)->nullable();
			$table->dateTime('posted_date')->nullable()->index('posted_date_index');
			$table->string('type', 50)->nullable()->index('type_index');
			$table->decimal('amount', 9)->nullable()->default(0.00);
			$table->char('currency', 3)->nullable();
			$table->string('group', 50)->nullable()->index('group_index');
			$table->unique(['amazon_order_id','sku','posted_date','type','group'], 'amazon_order_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('finance_events');
	}

}
