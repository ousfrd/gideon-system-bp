<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLast4DigitToOwnedBuyerPaymentMethods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owned_buyer_payment_methods', function (Blueprint $table) {
            $table->string('last_4_digits', 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owned_buyer_payment_methods', function (Blueprint $table) {
            $table->dropColumn('last_4_digits');
        });
    }
}
