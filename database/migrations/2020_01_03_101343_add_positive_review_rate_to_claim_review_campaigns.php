<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPositiveReviewRateToClaimReviewCampaigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_review_campaigns', function (Blueprint $table) {
            $table->integer('free_product_reviews')->default(0);
            $table->integer('gift_card_reviews')->default(0);
            $table->decimal('positive_review_rate', 6, 4)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_review_campaigns', function (Blueprint $table) {
            $table->dropColumn('free_product_reviews');
            $table->dropColumn('gift_card_reviews');
            $table->dropColumn('positive_review_rate');
        });
    }
}
