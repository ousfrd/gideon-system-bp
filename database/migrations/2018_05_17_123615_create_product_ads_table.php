<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_ads', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('seller_id', 20)->nullable()->index('seller_id');
            $table->string('country')->nullable();
            $table->string('adId')->nullable()->index('adId');
            $table->string('adGroupId')->nullable()->index('adGroupId');
            $table->string('campaignId')->nullable()->index('campaignId');
            $table->string('sku')->nullable();
            $table->string('state')->nullable();
            $table->timestamps();
            $table->unique(['adId', 'adGroupId', 'campaignId'], 'unique_productad_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
