<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellerCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seller_coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('seller_id', 20)->nullable();
            $table->string('coupon_id', 50)->nullable()->unique();
            $table->text('coupon_description', 16777215)->nullable();
            $table->dateTime('posted_date')->nullable();
            $table->string('fee_type', 50)->nullable();
            $table->decimal('fee_amount', 9)->nullable();
            $table->decimal('tax_amount', 9)->nullable();
            $table->decimal('total_amount', 9)->nullable();
            $table->char('currency', 3)->nullable()->default('USD');
            $table->decimal('exchange_rate', 9, 4)->nullable()->default(1.0000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seller_coupons');
    }
}
