<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDataTypeOfProductStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_statistics', function (Blueprint $table) {
            $table->float('fba_promo_value', 8, 2)->default(0)->change();
            $table->float('fbm_promo_value', 8, 2)->default(0)->change();
            $table->float('total_promo_value', 8, 2)->default(0)->change();

            $table->float('fba_other_fee', 8, 2)->default(0)->change();
            $table->float('fbm_other_fee', 8, 2)->default(0)->change();
            $table->float('total_other_fee', 8, 2)->default(0)->change();

            $table->float('fba_tax', 8, 2)->default(0)->change();
            $table->float('fbm_tax', 8, 2)->default(0)->change();
            $table->float('total_tax', 8, 2)->default(0)->change();

            $table->float('fba_vat_fee', 8, 2)->default(0)->change();
            $table->float('fbm_vat_fee', 8, 2)->default(0)->change();
            $table->float('total_vat_fee', 8, 2)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_statistics', function (Blueprint $table) {
            $table->integer('fba_promo_value')->default(0)->change();
            $table->integer('fbm_promo_value')->default(0)->change();
            $table->integer('total_promo_value')->default(0)->change();

            $table->integer('fba_other_fee')->default(0)->change();
            $table->integer('fbm_other_fee')->default(0)->change();
            $table->integer('total_other_fee')->default(0)->change();

            $table->integer('fba_tax')->default(0)->change();
            $table->integer('fbm_tax')->default(0)->change();
            $table->integer('total_tax')->default(0)->change();

            $table->integer('fba_vat_fee')->default(0)->change();
            $table->integer('fbm_vat_fee')->default(0)->change();
            $table->integer('total_vat_fee')->default(0)->change();
        });
    }
}
