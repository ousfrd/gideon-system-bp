<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExcludeRefundOrdersToCustomerExportors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_exportors', function (Blueprint $table) {
            $table->boolean('excludes_refund_orders')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_exportors', function (Blueprint $table) {
            $table->dropColumn('excludes_refund_orders');
        });
    }
}
