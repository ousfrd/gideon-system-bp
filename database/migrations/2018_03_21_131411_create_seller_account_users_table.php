<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSellerAccountUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('seller_account_users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('seller_account_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->unique(['seller_account_id','user_id'], 'seller_account_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('seller_account_users');
	}

}
