<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSellerListingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('seller_listings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('seller_id', 20)->nullable()->index('seller_id');
			$table->string('sku', 100);
			$table->string('asin', 10)->default('');
			$table->string('item_name', 500)->nullable();
			$table->char('country', 2)->default('US');
			$table->char('region', 2)->nullable()->default('US');
			$table->integer('product_id')->nullable();
			$table->date('open_date')->nullable();
			$table->integer('item_condition')->nullable()->default(11);
			$table->string('fulfillment_channel', 20)->nullable()->default('Amazon')->index('fulfillment_channel');
			$table->decimal('price', 9)->nullable()->default(0.00);
			$table->decimal('sale_price', 9)->nullable()->default(0.00);
			$table->char('currency', 3)->default('USD');
			$table->decimal('price_usd', 9)->nullable();
			$table->integer('qty')->default(0);
			$table->integer('total_qty')->nullable()->default(0);
			$table->boolean('status')->nullable()->default(1);
			$table->integer('last_7_days_orders')->nullable()->default(0);
			$table->integer('last_14_days_orders')->nullable()->default(0);
			$table->integer('last_21_days_orders')->nullable()->default(0);
			$table->integer('last_30_days_orders')->nullable()->default(0);
			$table->integer('today_so_far_orders')->nullable()->default(0);
			$table->integer('this_month_orders')->nullable()->default(0);
			$table->float('daily_ave_orders', 10, 0)->nullable()->default(0);
			$table->boolean('discontinued')->nullable()->default(0)->index('discontinued');
			$table->dateTime('updated_at')->nullable();
			$table->decimal('cost', 9)->nullable()->default(0.00);
			$table->decimal('shipping_cost', 9)->nullable()->default(0.00);
			$table->integer('manufacture_days')->nullable()->default(30);
			$table->integer('cn_to_amz_shipping_days')->nullable()->default(14);
			$table->boolean('peak_season_mode')->nullable()->default(0);
			$table->integer('ps_manufacture_days')->nullable()->default(30);
			$table->integer('ps_cn_to_amz_shipping_days')->nullable()->default(14);
			$table->integer('inventory_id')->nullable();
			$table->unique(['sku','country','seller_id','asin'], 'sku');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('seller_listings');
	}

}
