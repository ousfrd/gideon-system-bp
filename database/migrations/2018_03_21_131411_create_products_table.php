<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('asin', 10)->nullable();
			$table->char('country', 2)->nullable();
			$table->string('name', 1000)->nullable();
			$table->string('fulfillment_channel', 45)->nullable()->default('Amazon');
			$table->decimal('cost', 9)->nullable()->default(0.00);
			$table->decimal('shipping_cost', 9)->nullable()->default(0.00);
			$table->integer('warehouse_qty')->nullable()->default(0);
			$table->string('product_group', 100)->nullable();
			$table->string('amz_product_group', 100)->nullable();
			$table->string('amz_product_type', 100)->nullable();
			$table->boolean('isclothing')->nullable()->default(0);
			$table->boolean('ismedia')->nullable();
			$table->decimal('handling', 9)->nullable()->default(1.00);
			$table->decimal('pickpack', 9)->nullable()->default(1.06);
			$table->decimal('weighthandling', 9)->nullable()->default(0.96);
			$table->decimal('weight', 9)->nullable()->default(0.00);
			$table->decimal('storage', 9)->nullable()->default(0.00);
			$table->decimal('refer_percent', 9)->nullable()->default(0.15);
			$table->decimal('new_fba_fee', 9)->nullable()->default(2.99);
			$table->string('parent_asin', 10)->nullable();
			$table->timestamps();
			$table->string('sales_ranks', 2000)->nullable();
			$table->string('small_image')->nullable();
			$table->integer('offers')->nullable()->default(0);
			$table->boolean('status')->nullable()->default(1);
			$table->boolean('need_review')->nullable();
			$table->dateTime('info_last_checked')->nullable();
			$table->string('brand', 100)->nullable();
			$table->boolean('status_changed')->nullable()->default(0);
			$table->boolean('status_changed_email_sent')->nullable()->default(0);
			$table->decimal('lwh_total', 9)->nullable()->default(0.00);
			$table->decimal('length', 9)->nullable()->default(0.00);
			$table->decimal('height', 9)->nullable()->default(0.00);
			$table->decimal('width', 9)->nullable()->default(0.00);
			$table->boolean('fba_fee_caled')->nullable()->default(0);
			$table->integer('total_qty')->nullable()->default(0);
			$table->integer('qty')->nullable()->default(0);
			$table->integer('last_7_days_orders')->nullable()->default(0);
			$table->integer('last_14_days_orders')->nullable()->default(0);
			$table->integer('last_21_days_orders')->nullable()->default(0);
			$table->integer('last_30_days_orders')->nullable()->default(0);
			$table->integer('today_so_far_orders')->nullable()->default(0);
			$table->integer('this_month_orders')->nullable()->default(0);
			$table->float('daily_ave_orders', 10, 0)->nullable()->default(0)->index('daily_ave_orders');
			$table->boolean('discontinued')->nullable()->default(0)->index('discontinued');
			$table->integer('manufacture_days')->nullable()->default(30);
			$table->integer('cn_to_amz_shipping_days')->nullable()->default(14);
			$table->boolean('peak_season_mode')->nullable()->default(0)->index('peak_season_mode');
			$table->integer('ps_manufacture_days')->nullable()->default(30);
			$table->integer('ps_cn_to_amz_shipping_days')->nullable()->default(14);
			$table->decimal('avg_price_usd', 9)->nullable()->default(0.00);
			$table->decimal('lifetime_profit', 11)->nullable()->default(0.00);
			$table->decimal('lifetime_gross_sales', 11)->unsigned()->nullable()->default(0.00);
			$table->decimal('lifetime_product_cost', 11)->nullable()->default(0.00);
			$table->decimal('lifetime_ad_expense', 11)->nullable()->default(0.00);
			$table->decimal('lifetime_other_expense', 11)->nullable()->default(0.00);
			$table->integer('lifetime_units_sold')->nullable()->default(0);
			$table->integer('fba_listings')->nullable()->default(0);
			$table->integer('fbm_listings')->nullable()->default(0);
			$table->decimal('lifetime_refund', 9)->nullable()->default(0.00);
			$table->boolean('replenished')->nullable()->default(0);
			$table->decimal('lifetime_review_expense', 11)->nullable()->default(0.00);
			$table->decimal('lifetime_commission', 11)->nullable()->default(0.00);
			$table->decimal('lifetime_promotions', 11)->nullable()->default(0.00);
			$table->decimal('lifetime_fba_fee', 11)->nullable()->default(0.00);
			$table->unique(['asin','country'], 'asin');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
