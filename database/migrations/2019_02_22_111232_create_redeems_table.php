<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRedeemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redeems', function (Blueprint $table) {
            $table->increments('id');
            $table->date('requestDate')->nullable();
            $table->string('AmazonOrderId', 20)->nullable()->index('AmazonOrderId');
            $table->string('asin', 10)->nullable()->index('asin_idx');
            
            $table->string('usingTime')->nullable();
            $table->integer('star')->nullable();
            $table->string('how_to_help')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->boolean('newsletter')->nullable()->default(1);

            $table->timestamps();
            $table->unique(['AmazonOrderId'], 'AmazonOrderId_Unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redeems');
    }
}
