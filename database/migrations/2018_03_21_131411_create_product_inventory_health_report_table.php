<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductInventoryHealthReportTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_inventory_health_report', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('seller_id', 20)->nullable();
			$table->integer('listing_id')->nullable()->index('listing_id');
			$table->date('snapshot_date')->nullable();
			$table->string('sku', 100)->nullable();
			$table->char('country', 2)->nullable()->default('US');
			$table->string('fnsku', 50)->nullable();
			$table->string('asin', 10)->nullable();
			$table->integer('sales_rank')->nullable();
			$table->string('condition', 20)->nullable();
			$table->string('product_group', 100)->nullable();
			$table->integer('total_quantity')->nullable()->default(0);
			$table->integer('sellable_quantity')->nullable()->default(0);
			$table->integer('unsellable_quantity')->nullable()->default(0);
			$table->integer('inv_age_0_to_90_days')->nullable()->default(0);
			$table->integer('inv_age_91_to_180_days')->nullable()->default(0);
			$table->integer('inv_age_181_to_270_days')->nullable()->default(0);
			$table->integer('inv_age_271_to_365_days')->nullable()->default(0);
			$table->integer('inv_age_365_plus_days')->nullable()->default(0);
			$table->integer('units_shipped_last_24_hrs')->nullable()->default(0);
			$table->integer('units_shipped_last_7_days')->nullable()->default(0);
			$table->integer('units_shipped_last_90_days')->nullable()->default(0);
			$table->integer('units_shipped_last_180_days')->nullable()->default(0);
			$table->integer('units_shipped_last_30_days')->nullable()->default(0);
			$table->integer('units_shipped_last_365_days')->nullable()->default(0);
			$table->string('weeks_of_cover_t7', 20)->nullable()->default('0');
			$table->string('weeks_of_cover_t30', 20)->nullable()->default('0');
			$table->string('weeks_of_cover_t90', 20)->nullable()->default('0');
			$table->string('weeks_of_cover_t180', 20)->nullable()->default('0');
			$table->string('weeks_of_cover_t365', 20)->nullable()->default('0');
			$table->integer('num_afn_new_sellers')->nullable()->default(0);
			$table->integer('num_afn_used_sellers')->nullable()->default(0);
			$table->char('currency', 3)->nullable();
			$table->decimal('your_price', 9)->nullable()->default(0.00);
			$table->decimal('sales_price', 9)->nullable()->default(0.00);
			$table->decimal('lowest_afn_new_price', 9)->nullable()->default(0.00);
			$table->decimal('lowest_afn_used_price', 9)->default(0.00);
			$table->decimal('lowest_mfn_new_price', 9)->nullable()->default(0.00);
			$table->decimal('lowest_mfn_used_price', 9)->nullable()->default(0.00);
			$table->integer('qty_to_be_charged_ltsf_6_mo')->nullable()->default(0);
			$table->integer('qty_to_be_charged_ltsf_12_mo')->nullable()->default(0);
			$table->integer('qty_in_long_term_storage_program')->nullable()->default(0);
			$table->integer('qty_with_removals_in_progress')->nullable()->default(0);
			$table->decimal('projected_ltsf_6_mo', 9)->nullable()->default(0.00);
			$table->decimal('projected_ltsf_12_mo', 9)->nullable()->default(0.00);
			$table->float('per_unit_volume', 10, 0)->nullable();
			$table->char('is_hazmat', 1)->nullable()->default('N');
			$table->integer('in_bound_quantity')->nullable()->default(0);
			$table->string('asin_limit', 20)->nullable();
			$table->integer('inbound_recommend_quantity')->nullable()->default(0);
			$table->unique(['seller_id','sku','snapshot_date'], 'seller_id');
			$table->index(['sku','country'], 'sku');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_inventory_health_report');
	}

}
