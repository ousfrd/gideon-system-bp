<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGiftwrapFeeToProductStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_statistics', function (Blueprint $table) {
            $table->float('total_giftwrap_fee')->default(0);
            $table->float('fba_giftwrap_fee')->default(0);
            $table->float('fbm_giftwrap_fee')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_statistics', function (Blueprint $table) {
            $table->dropColumn('total_giftwrap_fee');
            $table->dropColumn('fba_giftwrap_fee');
            $table->dropColumn('fbm_giftwrap_fee');
        });
    }
}
