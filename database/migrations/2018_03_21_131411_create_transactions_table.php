<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transactions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('settlement_id', 20)->nullable();
			$table->dateTime('settlement_start_date')->nullable();
			$table->dateTime('settlement_end_date')->nullable();
			$table->dateTime('deposit_date')->nullable();
			$table->decimal('total_amount', 9, 2)->nullable()->default(0.00);
			$table->string('currency', 3)->nullable();
			$table->string('transaction_type', 30)->nullable()->index('transaction_type');
			$table->string('order_id', 50)->nullable()->index('order_id');
			$table->string('merchant_order_id', 50)->nullable();
			$table->string('adjustment_id', 50)->nullable();
			$table->dateTime('posted_date')->nullable();
			$table->string('shipment_id', 50)->nullable();
			$table->string('marketplace_name', 30)->nullable();
			$table->string('amount_type', 50)->nullable();
			$table->string('amount_description', 250)->nullable();
			$table->decimal('amount', 9, 2)->nullable()->default(0.00);
			$table->string('fulfillment_id', 20)->nullable();
			$table->dateTime('posted_date_time')->nullable();
			$table->string('order_item_code', 20)->nullable()->index('order_item_code');
			$table->string('promotion_id', 100)->nullable();
			$table->string('merchant_order_item_id', 50)->nullable();
			$table->string('merchant_adjustment_item_id', 20)->nullable();
			$table->string('sku', 50)->nullable();
			$table->integer('quantity_purchased')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transactions');
	}

}
