<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInsertCardOrdersToProductStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('product_statistics', function (Blueprint $table) {
            $table->integer('fba_insert_card_orders')->nullable()->default(0);
            $table->integer('fbm_insert_card_orders')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('product_statistics', function (Blueprint $table) {
            $table->dropColumn('fba_insert_card_orders');
            $table->dropColumn('fbm_insert_card_orders');
        });
    }
}
