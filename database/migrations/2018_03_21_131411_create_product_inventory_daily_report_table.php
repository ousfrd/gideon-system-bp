<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductInventoryDailyReportTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_inventory_daily_report', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('listing_id')->nullable()->index('listing_id');
			$table->string('seller_id', 20)->nullable();
			$table->date('snapshot-date')->nullable();
			$table->string('sku', 100)->nullable();
			$table->string('fnsku', 50)->nullable();
			$table->integer('quantity')->nullable()->default(0);
			$table->string('fulfillment-center-id', 20)->nullable();
			$table->string('detailed-disposition', 20)->nullable();
			$table->string('country', 3)->nullable();
			$table->unique(['snapshot-date','sku','fulfillment-center-id','detailed-disposition'], 'snapshot-date');
			$table->index(['sku','country'], 'sku');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_inventory_daily_report');
	}

}
