<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderStatisticToOwnedBuyers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owned_buyers', function (Blueprint $table) {
            $table->integer('own_orders')->default(0);
            $table->integer('other_orders')->default(0);
            $table->integer('total_orders')->default(0);
            $table->float('own_other_orders_rate')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owned_buyers', function (Blueprint $table) {
            $table->dropColumn('own_orders');
            $table->dropColumn('other_orders');
            $table->dropColumn('total_orders');
            $table->dropColumn('own_other_orders_rate');
        });
    }
}
