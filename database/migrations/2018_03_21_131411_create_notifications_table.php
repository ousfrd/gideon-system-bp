<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notifications', function(Blueprint $table)
		{
			$table->increments('id')->comment('Notification id');
			$table->smallInteger('severity')->unsigned()->default(0)->index('ADMINNOTIFICATION_INBOX_SEVERITY')->comment('Problem type');
			$table->timestamp('date_created')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('Create date');
			$table->string('title')->comment('Title');
			$table->text('description', 65535)->nullable()->comment('Description');
			$table->string('url')->nullable()->comment('Url');
			$table->string('seller_id', 20)->nullable();
			$table->string('asin', 10)->nullable();
			$table->string('sku', 100)->nullable();
			$table->boolean('is_remove')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notifications');
	}

}
