<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndicesToInviteReviewLetters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invite_review_letters', function (Blueprint $table) {
            $table->index(['redeem_id']);
            $table->index(['buyer_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invite_review_letters', function (Blueprint $table) {
            $table->dropIndex(['redeem_id']);
            $table->dropIndex(['buyer_id']);
        });
    }
}
