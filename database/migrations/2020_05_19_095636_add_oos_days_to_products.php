<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOosDaysToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('last_90_oos_days')->default(0);
            $table->integer('last_90_in_stock_days')->default(0);
            $table->integer('last_30_oos_days')->default(0);
            $table->integer('last_30_in_stock_days')->default(0);
            $table->integer('last_21_oos_days')->default(0);
            $table->integer('last_21_in_stock_days')->default(0);
            $table->integer('last_14_oos_days')->default(0);
            $table->integer('last_14_in_stock_days')->default(0);
            $table->integer('last_7_oos_days')->default(0);
            $table->integer('last_7_in_stock_days')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('last_90_oos_days');
            $table->dropColumn('last_90_in_stock_days');
            $table->dropColumn('last_30_oos_days');
            $table->dropColumn('last_30_in_stock_days');
            $table->dropColumn('last_21_oos_days');
            $table->dropColumn('last_21_in_stock_days');
            $table->dropColumn('last_14_oos_days');
            $table->dropColumn('last_14_in_stock_days');
            $table->dropColumn('last_7_oos_days');
            $table->dropColumn('last_7_in_stock_days');
        });
    }
}
