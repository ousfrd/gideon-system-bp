<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSellerAccountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('seller_accounts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name')->nullable();
			$table->string('code', 50)->nullable()->unique('code');
			$table->string('seller_id', 20)->nullable();
			$table->string('mws_auth', 150)->nullable();
			$table->string('seller_email')->nullable();
			$table->string('smtp_host')->nullable();
			$table->string('smtp_password')->nullable();
			$table->string('smtp_username')->nullable();
			$table->text('adv_access_token', 65535)->nullable();
			$table->text('adv_refresh_token', 65535)->nullable();
			$table->text('adv_profile_ids', 65535)->nullable();
			$table->char('country', 2)->nullable();
			$table->char('region', 2)->nullable();
			$table->string('marketplaces', 100)->nullable();
			$table->char('currency', 3)->nullable()->default('USD');
			$table->boolean('only_fba')->nullable();
			$table->dateTime('last_order_updated')->nullable();
			$table->dateTime('last_refund_updated')->nullable();
			$table->dateTime('last_inventory_report_updated')->nullable();
			$table->dateTime('last_listing_report_updated')->nullable();
			$table->dateTime('last_ad_report_updated')->nullable();
			$table->integer('campaign_warning_threshold')->nullable()->default(0);
			$table->integer('campaign_danger_threshold')->unsigned()->nullable()->default(0);
			$table->boolean('show_sponsored_ad_cost')->nullable()->default(1);
			$table->string('payout_heading', 10)->nullable()->default('name');
			$table->string('chart_type', 10)->nullable()->default('spline');
			$table->timestamps();
			$table->integer('timezone')->nullable()->default(-4);
			$table->string('timezone_name', 20)->nullable()->default('America/New_York');
			$table->string('status', 20)->nullable()->default('1');
			$table->string('mws_status', 20)->nullable()->default('Green');
			$table->dateTime('last_shipment_updated')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('seller_accounts');
	}

}
