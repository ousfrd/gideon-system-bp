<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateListingExpensesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('listing_expenses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('listing_id')->nullable()->index('listing_id');
			$table->string('category', 20)->nullable();
			$table->decimal('amount', 9)->nullable()->default(0.00);
			$table->date('posted_date')->nullable();
			$table->string('by', 100)->nullable();
			$table->text('notes')->nullable();
			$table->string('created_by', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('listing_expenses');
	}

}
