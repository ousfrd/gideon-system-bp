<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimReviewCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_review_campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->nullable();
            $table->string('platform', 255);
            $table->decimal('cashback', 7, 2)->nullable()->default(0.00);
            $table->string('marketplace', 50);
            $table->string('asin', 50);
            $table->integer('order_quantity')->default(0);
            $table->integer('response_quantity')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claim_review_campaigns');
    }
}
