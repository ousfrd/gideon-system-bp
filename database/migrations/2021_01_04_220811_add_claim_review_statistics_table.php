<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClaimReviewStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_review_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('seller_id')->nullable(FALSE);
            $table->string('asin')->nullable(FALSE);
            $table->string('sku')->nullable(FALSE);
            $table->string('country')->nullable(FALSE);
            $table->integer('orders')->unsigned()->nullable(FALSE)->default(0);
            $table->integer('pending_orders')->unsigned()->nullable(FALSE)->default(0);
            $table->integer('shipped_orders')->unsigned()->nullable(FALSE)->default(0);
            $table->integer('returned_orders')->unsigned()->nullable(FALSE)->default(0);
            $table->integer('partial_returned_orders')->unsigned()->nullable(FALSE)->default(0);
            $table->integer('shipping_orders')->unsigned()->nullable(FALSE)->default(0);
            $table->integer('unshipped_orders')->unsigned()->nullable(FALSE)->default(0);
            $table->integer('inner_page_orders')->unsigned()->nullable(FALSE)->default(0);
            $table->integer('missing_shipment_cnt')->unsigned()->nullable(FALSE)->default(0);
            $table->integer('to_claim_review_cnt')->unsigned()->nullable(FALSE)->default(0);
            $table->integer('claim_review_sent_cnt')->unsigned()->nullable(FALSE)->default(0);
            $table->integer('claim_review_to_send_cnt')->unsigned()->nullable(FALSE)->default(0);
            $table->date('date')->nullable(FALSE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claim_review_statistics');
    }
}
