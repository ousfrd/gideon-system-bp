<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdSnapshotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_snapshots', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('seller_id', 20)->nullable()->index('seller_id');
            $table->string('country')->nullable();
            $table->string('snapshotId')->nullable();
            $table->string('recordType')->nullable();
            $table->string('campaignType')->nullable()->default('sponsoredProducts');
            $table->string('stateFilter')->nullable()->default('enabled,paused,archived');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
