<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSellerDailyInventoryReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('seller_daily_inventory_reports', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('seller_id', 100);
			$table->date('date');
			$table->integer('inventory');
			$table->decimal('inventory_cost', 9);
			$table->unique(['seller_id','date'], 'unique_index');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('seller_daily_inventory_reports');
	}

}
