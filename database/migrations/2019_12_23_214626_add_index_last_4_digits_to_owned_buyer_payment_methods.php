<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexLast4DigitsToOwnedBuyerPaymentMethods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owned_buyer_payment_methods', function (Blueprint $table) {
            $table->index('last_4_digits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owned_buyer_payment_methods', function (Blueprint $table) {
            $table->dropColumn();
        });
    }
}
