<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGiftCardTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gift_card_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('asin', 50);
            $table->decimal('amount', 8, 2);
            $table->string('version', 255)->default('1.0');
            $table->string('front_design', 500)->nullable();
            $table->string('back_design', 500)->nullable();
            $table->timestamps();
            $table->index('asin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gift_card_templates');
    }
}
