<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotSyncedOrderIdToOwnedBuyers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owned_buyers', function (Blueprint $table) {
            $table->string('not_synced_order_id')->nullable();
            $table->index('not_synced_order_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owned_buyers', function (Blueprint $table) {
            $table->dropColumn('not_synced_order_id');
        });
    }
}
