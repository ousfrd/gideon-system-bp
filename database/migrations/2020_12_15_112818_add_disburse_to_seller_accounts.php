<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDisburseToSellerAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seller_accounts', function (Blueprint $table) {
            $table->boolean('disburse_enabled')->nullable(TRUE)->default(TRUE);
            $table->float('min_disburse_amount')->nullable(FALSE)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seller_accounts', function (Blueprint $table) {
            $table->dropColumn(['disburse_enabled', 'min_disburse_amount']);
        });
    }
}
