<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_campaigns', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('seller_id', 20)->nullable()->index('seller_id');
            $table->string('country')->nullable();
            $table->string('campaignId')->nullable()->index('campaignId');
            $table->string('name')->nullable();
            $table->string('campaignType')->nullable()->default('sponsoredProducts');
            $table->string('targetingType')->nullable();
            $table->boolean('premiumBidAdjustment')->nullable()->default(false);
            $table->decimal('dailyBudget', 6, 2)->nullable();
            $table->date('startDate')->nullable();
            $table->string('state')->nullable();
            $table->timestamps();
            $table->unique(['campaignId'], 'unique_campaign_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
