<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ad_reports', function(Blueprint $table)
		{
			$table->increments('id');
			$table->date('date')->nullable();
			$table->string('seller_id', 20)->nullable()->index('seller_id');
			$table->string('asin', 20)->nullable();
			$table->string('sku', 100)->nullable();
			$table->integer('clicks')->nullable()->default(0);
			$table->integer('impressions')->nullable()->default(0);
			$table->char('currency', 3)->nullable()->default('USD');
			$table->float('exchange_rate', 10, 0)->nullable()->default(1);
			$table->decimal('spend', 9)->nullable()->default(0.00);
			$table->string('product_group')->nullable();
			$table->string('country')->nullable();
			$table->string('adId')->nullable();
			$table->string('adGroupId')->nullable();
			$table->string('campaignId')->nullable();
			$table->integer('attributedConversions1dSameSKU')->nullable();
			$table->integer('attributedConversions1d')->nullable();
			$table->decimal('attributedSales1dSameSKU')->nullable();
			$table->decimal('attributedSales1d')->nullable();
			$table->integer('attributedConversions7dSameSKU')->nullable();
			$table->integer('attributedConversions7d')->nullable();
			$table->decimal('attributedSales7dSameSKU')->nullable();
			$table->decimal('attributedSales7d')->nullable();
			$table->integer('attributedConversions14dSameSKU')->nullable();
			$table->integer('attributedConversions14d')->nullable();
			$table->decimal('attributedSales14dSameSKU')->nullable();
			$table->decimal('attributedSales14d')->nullable();
			$table->integer('attributedConversions30dSameSKU')->nullable();
			$table->integer('attributedConversions30d')->nullable();
			$table->decimal('attributedSales30dSameSKU')->nullable();
			$table->decimal('attributedSales30d')->nullable();
			$table->unique(['date','seller_id','sku','adId'], 'idx_ad_reports_date_seller_id_sku_adId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ad_reports');
	}

}
