<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFieldsToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->boolean('verified')->nullable()->default(0);
			$table->string('role', 50)->nullable();
			$table->dateTime('last_login')->nullable();
			$table->string('last_login_ip', 50)->nullable();
			$table->boolean('welcome_email_sent')->nullable()->default(0);
			$table->unique(['name','email'], 'idx_users');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
