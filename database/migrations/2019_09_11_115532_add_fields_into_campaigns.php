<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsIntoCampaigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // public function up()
    // {
    //     Schema::table('ad_campaigns_reports', function (Blueprint $table) {

    //         $table->integer('impressions_diff')->nullable()->default(0)->after('impressions');
    //         $table->integer('clicks_diff')->nullable()->default(0)->after('clicks');
    //         $table->decimal('cost_diff', 9, 2)->nullable()->default(0.00)->after('cost');
    //         $table->decimal('attributedSales1d_diff')->nullable()->default(0)->after('attributedSales1d');
    //         $table->integer('attributedConversions1d_diff')->nullable()->default(0)->after('attributedConversions1d');
    //         $table->integer('attributedUnitsOrdered1d_diff')->nullable()->default(0)->after('attributedUnitsOrdered1d');

    //     });

    //     Schema::table('ad_groups_reports', function (Blueprint $table) {

    //         $table->integer('impressions_diff')->nullable()->default(0)->after('impressions');
    //         $table->integer('clicks_diff')->nullable()->default(0)->after('clicks');
    //         $table->decimal('cost_diff', 9, 2)->nullable()->default(0.00)->after('cost');
    //         $table->decimal('attributedSales1d_diff')->nullable()->default(0)->after('attributedSales1d');
    //         $table->integer('attributedConversions1d_diff')->nullable()->default(0)->after('attributedConversions1d');
    //         $table->integer('attributedUnitsOrdered1d_diff')->nullable()->default(0)->after('attributedUnitsOrdered1d');

    //     });

    //     Schema::table('ad_keywords_reports', function (Blueprint $table) {

    //         $table->integer('impressions_diff')->nullable()->default(0)->after('impressions');
    //         $table->integer('clicks_diff')->nullable()->default(0)->after('clicks');
    //         $table->decimal('cost_diff', 9, 2)->nullable()->default(0.00)->after('cost');
    //         $table->decimal('attributedSales1d_diff')->nullable()->default(0)->after('attributedSales1d');
    //         $table->integer('attributedConversions1d_diff')->nullable()->default(0)->after('attributedConversions1d');
    //         $table->integer('attributedUnitsOrdered1d_diff')->nullable()->default(0)->after('attributedUnitsOrdered1d');

    //     });


    // }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
