<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemMarketingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_item_marketings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('amazon_order_id', 50)->nullable(false)->unique();
            $table->string('sku', 50)->nullable(false);
            $table->boolean('processed')->default(false);
            $table->boolean('invite_review')->default(false);
            $table->boolean('self_review')->default(false);
            $table->boolean('email_promotion_review')->default(false);
            $table->integer('claim_review_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_item_marketings');
    }
}
