<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimReviewReturnAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_review_return_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('return_address_id');
            $table->integer('user_id')->nullable();
            $table->string('platform');
            $table->string('address_name');
            $table->string('address_company')->nullable();
            $table->string('address_line_1');
            $table->string('address_line_2')->nullable();
            $table->string('address_city');
            $table->string('address_state')->nullable();
            $table->string('address_postal_code');
            $table->string('address_country');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claim_review_return_addresses');
    }
}
