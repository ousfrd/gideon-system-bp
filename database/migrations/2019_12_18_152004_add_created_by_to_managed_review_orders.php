<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatedByToManagedReviewOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('managed_review_orders', function (Blueprint $table) {
            $table->integer('created_by');
            $table->index('created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('managed_review_orders', function (Blueprint $table) {
            $table->dropIndex('created_by');
            $table->dropColumn('created_by');
        });
    }
}
