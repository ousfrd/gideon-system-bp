<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClickSendConfigsToClaimReviewCampaigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_review_campaigns', function (Blueprint $table) {
            $table->integer('cs_return_address_id')->nullable();
            $table->boolean('cs_duplex')->default(0);
            $table->boolean('cs_colour')->default(1);
            $table->boolean('cs_priority_post')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_review_campaigns', function (Blueprint $table) {
            $table->dropColumn('cs_return_address_id');
            $table->dropColumn('cs_duplex');
            $table->dropColumn('cs_colour');
            $table->dropColumn('cs_priority_post');
        });
    }
}
