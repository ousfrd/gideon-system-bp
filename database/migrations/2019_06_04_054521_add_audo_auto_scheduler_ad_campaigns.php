<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAudoAutoSchedulerAdCampaigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ad_campaigns', function (Blueprint $table) {
            $table->string('auto_scheduler',100)->default('disabled')->after('state');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ad_campaigns', function (Blueprint $table) {
            $table->dropColumn('auto_scheduler');
        });
    }
}
