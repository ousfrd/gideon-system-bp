<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReviewCostToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->float('claim_review_order_cost')->default(30);
            $table->float('invite_review_order_cost')->default(30);
            $table->float('self_review_order_cost')->default(30);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('claim_review_order_cost');
            $table->dropColumn('invite_review_order_cost');
            $table->dropColumn('self_review_order_cost');
        });
    }
}
