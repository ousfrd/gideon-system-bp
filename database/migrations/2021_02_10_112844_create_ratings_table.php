<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('asin')->nullable(FALSE);
            $table->date('date')->nullable(FALSE);

            $table->integer('rating_count')->nullable(FALSE)->default(0);
            $table->float('rating')->nullable(FALSE)->default(0);
            $table->integer('star5_rating')->nullable(FALSE)->default(0);
            $table->integer('star4_rating')->nullable(FALSE)->default(0);
            $table->integer('star3_rating')->nullable(FALSE)->default(0);
            $table->integer('star2_rating')->nullable(FALSE)->default(0);
            $table->integer('star1_rating')->nullable(FALSE)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratings');
    }
}
