<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttachmentsToGiftCardTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gift_card_templates', function (Blueprint $table) {
            $table->string('attachment1')->nullable();
            $table->string('attachment2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gift_card_templates', function (Blueprint $table) {
            $table->dropColumn('attachment1');
            $table->dropColumn('attachment2');
        });
    }
}
