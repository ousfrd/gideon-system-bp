<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInventoryAttributesToSellerListings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seller_listings', function (Blueprint $table) {
            $table->string('mfn_listing_exists')->nullable();	
            $table->integer('mfn_fulfillable_quantity')->default(0);
            $table->string('afn_listing_exists')->nullable();	
            $table->integer('afn_warehouse_quantity');
            $table->integer('afn_fulfillable_quantity');	
            $table->integer('afn_unsellable_quantity');
            $table->integer('afn_reserved_quantity');
            $table->integer('afn_total_quantity');
            $table->float('per_unit_volume')->nullable();
            $table->integer('afn_inbound_working_quantity');
            $table->integer('afn_inbound_shipped_quantity');
            $table->integer('afn_inbound_receiving_quantity');
            $table->integer('afn_researching_quantity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seller_listings', function (Blueprint $table) {
            //
        });
    }
}
