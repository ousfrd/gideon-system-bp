<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('asin');
            $table->string('country');
            $table->date('review_date');
            $table->string('profile_name')->nullable();
            $table->string('review_title')->nullable();
            $table->text('review_text')->nullable();
            $table->integer('review_rating')->nullable();
            $table->string('review_id');
            $table->string('review_link')->nullable();
            $table->boolean('verified_purchase')->default(1);
            $table->boolean('review_exist')->default(1);

            $table->timestamps();
            $table->unique(['asin','country','review_id'], 'asin_country_review');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
