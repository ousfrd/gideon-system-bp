<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryCountryToSalesRanks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_ranks', function (Blueprint $table) {
            $table->string('category')->nullable();
            $table->string('country')->nullable();
            $table->integer('sales_rank')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_ranks', function (Blueprint $table) {
            $table->dropColumn('category');
            $table->dropColumn('country');
            $table->dropColumn('sales_rank');
        });
    }
}
