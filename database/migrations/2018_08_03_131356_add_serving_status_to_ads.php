<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServingStatusToAds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ad_campaigns', function (Blueprint $table) {
            $table->date('endDate')->nullable()->after('startDate');
            
            $table->string('lastUpdatedDate')->nullable()->after('state');
            $table->string('creationDate')->nullable()->after('state');
            $table->string('servingStatus')->nullable()->after('state');
            $table->string('placement')->nullable()->after('state');

        });

        Schema::table('ad_groups', function (Blueprint $table) {
            
            $table->string('lastUpdatedDate')->nullable()->after('state');
            $table->string('creationDate')->nullable()->after('state');
            $table->string('servingStatus')->nullable()->after('state');

        });

        Schema::table('ad_keywords', function (Blueprint $table) {
            
            $table->string('lastUpdatedDate')->nullable()->after('bid');
            $table->string('creationDate')->nullable()->after('bid');
            $table->string('servingStatus')->nullable()->after('bid');

        });        

        Schema::table('product_ads', function (Blueprint $table) {
            
            $table->string('lastUpdatedDate')->nullable()->after('state');
            $table->string('creationDate')->nullable()->after('state');
            $table->string('servingStatus')->nullable()->after('state');

        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
