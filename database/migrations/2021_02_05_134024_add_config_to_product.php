<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfigToProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('products', function (Blueprint $table) {
            $table->boolean('negtive_review_remove')->default(FALSE); 
            $table->boolean('self_review')->default(FALSE); 
            $table->integer('expected_monthly_profit')->default(0); 
            $table->string('profit_level')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('negtive_review_remove');
            $table->dropColumn('self_review');
            $table->dropColumn('expected_monthly_profit');
            $table->dropColumn('profit_level');
        });
    }
}
