<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLetterSendFeedbackToOrderItemMarketings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_item_marketings', function (Blueprint $table) {
            $table->boolean('is_sent')->default(false);
            $table->text('click_send_response')->nullable();
            $table->string('click_send_scheduled_send_at')->nullable();
            $table->index('click_send_scheduled_send_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_item_marketings', function (Blueprint $table) {
            $table->dropColumn('is_sent');
            $table->dropColumn('click_send_response');
            $table->dropColumn('click_send_scheduled_send_at');
        });
    }
}
