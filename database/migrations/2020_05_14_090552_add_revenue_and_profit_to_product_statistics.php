<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRevenueAndProfitToProductStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_statistics', function (Blueprint $table) {
            $table->float('fba_revenue', 8, 2)->default(0);
            $table->float('fbm_revenue', 8, 2)->default(0);
            $table->float('total_revenue', 8, 2)->default(0);

            $table->float('fba_profit', 8, 2)->default(0);
            $table->float('fbm_profit', 8, 2)->default(0);
            $table->float('total_profit', 8, 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_statistics', function (Blueprint $table) {
            $table->dropColumn('fba_revenue');
            $table->dropColumn('fbm_revenue');
            $table->dropColumn('total_revenue');

            $table->dropColumn('fba_profit');
            $table->dropColumn('fbm_profit');
            $table->dropColumn('total_profit');

        });
    }
}
