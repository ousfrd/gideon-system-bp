<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropIndexOfSalesRanks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_ranks', function (Blueprint $table) {
            $table->dropUnique('unique_asin_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_ranks', function (Blueprint $table) {
            $table->unique(['asin','date'], 'unique_asin_date');
        });
    }
}
