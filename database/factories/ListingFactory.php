<?php

use Faker\Generator as Faker;

use App\Listing;

$factory->define(Listing::class, function (Faker $faker) {
    return [
        'asin' => str_random(10)
    ];
});
