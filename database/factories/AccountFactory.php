<?php

use Faker\Generator as Faker;

use App\Account;

$factory->define(Account::class, function (Faker $faker) {
    return [
        'seller_id' => str_random(13)
    ];
});
