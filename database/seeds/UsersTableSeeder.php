<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Test',
            'email' => 'neal.wkacc@gmail.com',
            'password' => bcrypt('q1w2e3'),
            'role' => 'system admin',
            'verified' => 1
        ]);
    }
}
