<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Helpers\ListingReportImporter;
use App\Account;
use App\Product;
use App\Listing;

class ListingReportImporterTest extends ListingReportBaseTest
{
	use RefreshDatabase;

    /**
     * @dataProvider getListingRecords
     *
     * @return void
     */
    public function testImportListing($listingRecords, $targetListingRecord)
    {
    	$country = 'US';
    	$sellerId = 'ABCDEFGHIJK';
    	$sellerCode = '99999';
    	$account = Account::where('seller_id', $sellerId)->first();
		if ($account === NULL) {
			factory(Account::class)->create([
				'name' => $sellerId,
				'code' => $sellerCode,
				'seller_id' => $sellerId,
				'region' => 'US'
			]);
		}

		$result = ListingReportImporter::importListing(
			$targetListingRecord, $sellerId, $country);
        $this->assertTrue(is_array($result));

        $productId = $result['productId'];
        $product = Product::where('id', $productId)->first();
        $this->assertTrue($product !== NULL);
        $this->assertEquals($product->asin, $targetListingRecord['asin1']);

        $listingId = $result['listingId'];
        $listing = Listing::where('id', $listingId)->first();
        $this->assertTrue($listing !== NULL);
        $this->assertEquals($listing->sku, $targetListingRecord['seller-sku']);
    }

    /**
     * @dataProvider getSkus
     *
     * @return void
     */
    public function testIsDsListing($skus, $targetDsSkus) {
    	$dsSkus = [];
    	foreach ($skus as $sku) {
    		if (ListingReportImporter::isDsListing($sku)) {
    			array_push($dsSkus, $sku);
    		}
    	}

    	$this->assertEquals(count($dsSkus), count($targetDsSkus));
    	foreach ($dsSkus as $dsSku) {
    		$this->assertTrue(in_array($dsSku, $targetDsSkus));
    	}
    }
}
