<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\AmazonFBAInventory;
use App\Helpers\InventoryReportReader;


class AmazonFBAInventoryTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $fbaInventories = [];
    	foreach ($this->getFBAInventories() as $fbaInventory) {
    		if (in_array($fbaInventory->asin, ['B07WWFG68G', 'B083F93PJR'])) {
    			var_dump($fbaInventory);
    		}

    		array_push($fbaInventories, $fbaInventory);
    	}

    	foreach ($this->getTargetFBAInventories() as $targetFbaInventory) {
    		$this->assertTrue(in_array($targetFbaInventory, $fbaInventories));
    	}
    }

    public function getFBAInventories() {
    	$FBAInventoryReportPathes = [
    		__DIR__ . '/../data/amazon/fba-inventory.txt',
    		__DIR__ . '/../data/amazon/fba-inventory-2.txt',
    	];
    	foreach ($FBAInventoryReportPathes as $FBAInventoryReportPath) {
    		$inventories = InventoryReportReader::getInventoryRecords($FBAInventoryReportPath);
    		foreach ($inventories as $inventory) {
    			yield $inventory;
    		}
    	}
    }

    public function getTargetFBAInventories() {
    	return [
    		AmazonFBAInventory::fromArray([
    			'asin' => 'B07WWFG68G',
    			'sku' => '4A-YMZI-P4N3',
    			'condition' => 'New',
    			'mfnListingExists' => 'No',
    			'mfnFulfillableQuantity' => 0,
    			'afnListingExists' => 'Yes',
    			'afnWarehouseQuantity' => 0,
    			'afnFulfillableQuantity' => 0,
    			'afnUnsellableQuantity' => 0,
    			'afnReservedQuantity' => 0,
    			'afnTotalQuantity' => 0,
    			'perUnitVolume' => 0.01,
    			'afnInboundWorkingQuantity' => 0,
    			'afnInboundShippedQuantity' => 0,
    			'afnInboundReceivingQuantity' => 0,
    			'afnResearchingQuantity' => 0
    		]),
    		AmazonFBAInventory::fromArray([
    			'asin' => 'B083F93PJR',
    			'sku' => '0K-KKXP-YOFN',
    			'condition' => 'New',
    			'mfnListingExists' => 'No',
    			'mfnFulfillableQuantity' => 0,
    			'afnListingExists' => 'Yes',
    			'afnWarehouseQuantity' => 340,
    			'afnFulfillableQuantity' => 216,
    			'afnUnsellableQuantity' => 0,
    			'afnReservedQuantity' => 123,
    			'afnTotalQuantity' => 340,
    			'perUnitVolume' => 0.03,
    			'afnInboundWorkingQuantity' => 0,
    			'afnInboundShippedQuantity' => 0,
    			'afnInboundReceivingQuantity' => 0,
    			'afnResearchingQuantity' => 1
    		]),
    	];
    }
}
