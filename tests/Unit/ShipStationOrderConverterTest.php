<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use ShipStation\ObjectSerializer;
use ShipStation\Model\ListStoresresponse;
use ShipStation\Model\Order;
use App\Helpers\ShipStationOrderConverter;


class ShipStationOrderConverterTest extends ShipStationBaseTest
{
    /**
     * @dataProvider shipStationOrders
     *
     * @return void
     */
    public function testConvert($shipStationStore, $shipStationOrder, $targetOrder)
    {
    	$converter = new ShipStationOrderConverter();
    	$order = $converter->convert($shipStationStore, $shipStationOrder);

        $this->assertEquals($order, $targetOrder);
    }
}
