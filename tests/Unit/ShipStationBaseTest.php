<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use ShipStation\ObjectSerializer;
use ShipStation\Model\ListStoresresponse;
use ShipStation\Model\Order;
use ShipStation\Model\ListOrderswithparametersresponse;


class ShipStationBaseTest extends TestCase
{
	public function testPlaceholder()
	{
        $this->assertTrue(true);
	}

	public function shipStationOrders()
    {
    	$items = [];

    	$orderPath = __DIR__ . '/../data/shipstation/shipstation_order.json';
    	$ordersData = json_decode(file_get_contents($orderPath), TRUE);
    	foreach ($ordersData as $orderData) {
    		// $shipStationStore = new ListStoresresponse($orderData['source']['store']);
    		// $shipStationOrder = new Order($orderData['source']['order']);
    		$storeObj = json_decode(json_encode($orderData['source']['store']));
    		$orderObj = json_decode(json_encode($orderData['source']['order']));
    		$shipStationStore = ObjectSerializer::deserialize(
    			$storeObj, ListStoresresponse::class);
    		$shipStationOrder = ObjectSerializer::deserialize(
    			$orderObj, Order::class);
    		$order = $orderData['target'];
    		array_push($items, [$shipStationStore, $shipStationOrder, $order]);
    	}

    	return $items;
    }

    public function shipStationListOrdersResp()
    {
        $storePath = __DIR__ . '/../data/shipstation/shipstation_store.json';
        $storeObj = json_decode(file_get_contents($storePath));
        $shipStationStore = ObjectSerializer::deserialize(
            $storeObj, ListStoresresponse::class);

        $listOrdersRespPath = __DIR__ . '/../data/shipstation/shipstation_list_orders_resp.json';
        $listOrdersRespData = json_decode(file_get_contents($listOrdersRespPath));
        $listOrdersResp = ObjectSerializer::deserialize(
            $listOrdersRespData, ListOrderswithparametersresponse::class);

        return [[$shipStationStore, $listOrdersResp]];
    }
}
