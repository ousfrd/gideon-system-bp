<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Account;
use App\Listing;
use App\Product;
use App\Order;
use App\OrderItem;
use App\Services\ShipStationOrderImportService;

class ShipStationOrderImportServiceTest extends ShipStationBaseTest
{
	use RefreshDatabase;

    /**
     * @dataProvider shipStationOrders
     *
     * @return void
     */
    public function testImportOrder($shipStationStore, $shipStationOrder, $targetOrder)
    {
    	$importService = resolve(ShipStationOrderImportService::class);

    	foreach ($targetOrder['items'] as $item) {
    		$account = $importService->getAccount($item['seller_id']);
    		if ($account === NULL) {
    			$account = factory(Account::class)->create([
    				'name' => $item['seller_id'],
    				'code' => $importService->getAccountCode($item['seller_id']),
    			]);
    		}

    		$sellerId = $account->seller_id;

    		$listing = $importService->getListing($sellerId, $item['SellerSKU']);
    		if ($listing !== NULL) {
    			continue;
    		}

	    	$listing = factory(Listing::class)->create([
	    		'seller_id' => $sellerId,
	    		'sku' => $item['SellerSKU'],
	    		'item_name' => $item['Title'],
	    		'country' => $item['country'],
	    		'region' => $item['region'],
	    	]);
            $product = Product::where([
            	['asin', '=', $listing->asin],
            	['country', '=', $listing->country]
            ])->first();
            if ($product === NULL) {
		    	$product = factory(Product::class)->create([
		    		'asin' => $listing->asin,
		    		'country' => $item['country'],
		    		'name' => $item['Title'],
		    		'cost' => 20.0,
		    		'shipping_cost' => 3.99
		    	]);
            }
            $listing->product_id = $product->id;
            $listing->save();
    	}

    	$importService->importOrder($targetOrder);

    	$account = $importService->getAccount($targetOrder['seller_id']);
    	$sellerId = $account->seller_id;

    	$orderItem = new OrderItem();
    	foreach ($targetOrder['items'] as $item) {
    		$orderItemFilter = [
    			'seller_id' => $sellerId,
    			'AmazonOrderId' => $item['AmazonOrderId'],
    			'SellerSKU' => $item['SellerSKU']
    		];
    		$this->assertDatabaseHas($orderItem->getTable(), $orderItemFilter);
    	}

    	$orderFilter = [
			'seller_id' => $sellerId,
			'AmazonOrderId' => $targetOrder['AmazonOrderId']
    	];
    	$order = new Order();
    	$this->assertDatabaseHas($order->getTable(), $orderFilter);

    	$purchaseDate = \DateTime::createFromFormat(
			\DateTimeInterface::RFC3339, $targetOrder['PurchaseDate']);
		$targetPurchaseDate = $purchaseDate->format('Y-m-d H:i:s');
		$query = [];
		foreach ($orderFilter as $k => $v) {
			array_push($query, [$k, '=', $v]);
		}
		$dbOrder = Order::where($query)->first();
		$this->assertEquals($dbOrder->PurchaseDate, $targetPurchaseDate);
    }

    /**
     * @dataProvider shipStationListOrdersResp
     *
     * @return void
     */
    public function testImportShipStationOrders($shipStationStore, $listOrdersResp)
    {
    	$importService = resolve(ShipStationOrderImportService::class);

    	$importableCnt = $nonImportableCnt = 0;
    	$ordersNonImportable = [];
    	foreach ($listOrdersResp->getOrders() as $shipStationOrder) {
			$order = $importService->orderConverter->convert($shipStationStore, $shipStationOrder);
			if ($order['items']) {
				$importableCnt += 1;
			} else {
				$nonImportableCnt += 1;

				array_push($ordersNonImportable, $order['AmazonOrderId']);
			}
			foreach ($order['items'] as $item) {
	    		$account = $importService->getAccount($item['seller_id']);
	    		if ($account === NULL) {
	    			$account = factory(Account::class)->create([
	    				'name' => $item['seller_id'],
	    				'code' => $importService->getAccountCode($item['seller_id']),
	    			]);
	    		}

	    		$sellerId = $account->seller_id;

				$listing = $importService->getListing($sellerId, $item['SellerSKU']);
	    		if ($listing !== NULL) {
	    			continue;
	    		}

		    	$listing = factory(Listing::class)->create([
		    		'seller_id' => $sellerId,
		    		'sku' => $item['SellerSKU'],
		    		'item_name' => $item['Title'],
		    		'country' => $item['country'],
		    		'region' => $item['region'],
		    	]);
	            $product = Product::where([
	            	['asin', '=', $listing->asin],
	            	['country', '=', $listing->country]
	            ])->first();
	            if ($product === NULL) {
			    	$product = factory(Product::class)->create([
			    		'asin' => $listing->asin,
			    		'country' => $item['country'],
			    		'name' => $item['Title'],
			    		'cost' => 20.0,
			    		'shipping_cost' => 3.99
			    	]);
	            }
	            $listing->product_id = $product->id;
	            $listing->save();
			}
    	}

    	$orderItem = new OrderItem();
    	$order = new Order();

    	$ordersCnt = count($listOrdersResp->getOrders());
    	$result = $importService->importShipStationOrders($shipStationStore, $listOrdersResp);
    	$this->assertEquals($result['total'], $ordersCnt);
    	$this->assertEquals($result['succeed'], $importableCnt);
    	$this->assertEquals($result['failed'], $nonImportableCnt);

    	foreach ($listOrdersResp->getOrders() as $shipStationOrder) {
			$convertedOrder = $importService->orderConverter->convert($shipStationStore, $shipStationOrder);
			$account = $importService->getAccount($convertedOrder['seller_id']);
			$sellerId = $account->seller_id;
			foreach ($convertedOrder['items'] as $item) {
				$orderItemFilter = [
	    			'seller_id' => $sellerId,
	    			'AmazonOrderId' => $item['AmazonOrderId'],
	    			'SellerSKU' => $item['SellerSKU']
	    		];
	    		$this->assertDatabaseHas($orderItem->getTable(), $orderItemFilter);
			}

			$orderFilter = [
				'seller_id' => $sellerId,
				'AmazonOrderId' => $convertedOrder['AmazonOrderId']
	    	];
			if (in_array($convertedOrder['AmazonOrderId'], $ordersNonImportable)) {
				$this->assertDatabaseMissing($order->getTable(), $orderFilter);
			} else {
	    		$this->assertDatabaseHas($order->getTable(), $orderFilter);

				$purchaseDate = \DateTime::createFromFormat(
					\DateTimeInterface::RFC3339, $convertedOrder['PurchaseDate']);
	    		$targetPurchaseDate = $purchaseDate->format('Y-m-d H:i:s');
	    		$query = [];
	    		foreach ($orderFilter as $k => $v) {
	    			array_push($query, [$k, '=', $v]);
	    		}
	    		$dbOrder = Order::where($query)->first();
	    		$this->assertEquals($dbOrder->PurchaseDate, $targetPurchaseDate);
			}
		}
    }
}
