<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\AmazonOrderShipment;
use App\Helpers\OrderShipmentReportReader;


class AmazonOrderShipmentTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testFromOrderShipmentRecord() {
    	$orderShipments = [];
    	foreach ($this->getOrderShipments() as $orderShipment) {
    		if (in_array($orderShipment->amazonOrderId, ['111-2403454-1067405', '702-3957019-6105034'])) {
    			var_dump($orderShipment);
    		}
    		array_push($orderShipments, $orderShipment);
    	}

    	foreach ($this->getTargetOrderShipments() as $targetOrderShipment) {
    		$this->assertTrue(in_array($targetOrderShipment, $orderShipments));
    	}
    }

    public function getOrderShipments() {
    	$orderShipmentReportPathes = [
    		__DIR__ . '/../data/amazon/amazon-fulfillied-shipment.txt',
    		__DIR__ . '/../data/amazon/amazon-fulfillied-shipment-2.txt',
    	];
    	foreach ($orderShipmentReportPathes as $orderShipmentReportPath) {
    		$orderShipments = OrderShipmentReportReader::getOrderShipments($orderShipmentReportPath);
    		foreach ($orderShipments as $orderShipment) {
    			yield $orderShipment;
    		}
    	}
    }

    public function getTargetOrderShipments() {
    	return [
    		AmazonOrderShipment::fromArray([
    			'amazonOrderId' => '111-2403454-1067405',
				'merchantOrderId' => '',
				'recipientName' => 'Cesar Solache',
				'shipAddress1' => '5307 W FLETCHER ST 1',
				'shipAddress2' => '',
				'shipAddress3' => '',
				'shipCity' => 'CHICAGO',
				'shipState' => 'IL',
				'shipPostalCode' => '60641-4945',
				'shipCountry' => 'US',
				'sku' => 'S6-ILIR-R6JR',
				'quantityShipped' => 1,
				'shipmentId' => 'DxqHyyDF0',
				'shipmentItemId' => 'Dg2Gt7C9R',
				'amazonOrderItemId' => '60297345654490',
				'merchantOrderItemId' => '',
				'purchaseDate' => '2020-09-11 08:59:48',
				'paymentsDate' => '2020-09-12 00:35:06',
				'shipmentDate' => '2020-09-11 23:28:58',
				'reportingDate' => '2020-09-12 01:29:04',
				'buyerEmail' => 'zdxrhx55ltmwynn@marketplace.amazon.com',
				'buyerName' => 'Cesar',
				'buyerPhoneNumber' => '',
				'productName' => 'Derma Roller for Beard Growth, Beard Growth Serum, Microneedle Roller and Natural Hair Growth Serum for Men with Patchy Facial Growth, Stimulate Beard',
				'itemPrice' => 28.97,
				'itemTax' => 1.81,
				'shippingPrice' => 0,
				'shippingTax' => 0,
				'giftWrapPrice' => 0,
				'giftWrapTax' => 0,
				'shipPromotionDiscount' => 0,
				'itemPromotionDiscount' => 0,
				'shipServiceLevel' => 'Expedited',
				'billAddress1' => '',
				'billAddress2' => '',
				'billAddress3' => '',
				'billCity' => '',
				'billState' => '',
				'billPostalCode' => '',
				'billCountry' => '',
				'carrier' => 'AMZN_US',
				'trackingNumber' => 'TBA145896307101',
				'estimatedArrivalDate' => '2020-09-12 20:00:00',
				'fulfillmentCenterId' => 'MDW4',
				'fulfillmentChannel' => 'AFN',
				'salesChannel' => 'Amazon.com',
				'currency' => 'USD'
    		]),
    		AmazonOrderShipment::fromArray([
    			'amazonOrderId' => '702-3957019-6105034',
				'merchantOrderId' => '',
				'recipientName' => 'Kathleen Cabales',
				'shipAddress1' => '1393 60th Avenue East',
				'shipAddress2' => '',
				'shipAddress3' => '',
				'shipCity' => 'Vancouver',
				'shipState' => 'British Columbia',
				'shipPostalCode' => 'V5X 2A8',
				'shipCountry' => 'CA',
				'sku' => '1R-F3C1-M7H8',
				'quantityShipped' => 1,
				'shipmentId' => 'DthGGn1yl',
				'shipmentItemId' => 'DDCyVZdDR',
				'amazonOrderItemId' => '20877156951338',
				'merchantOrderItemId' => '',
				'purchaseDate' => '2020-11-03 09:23:14',
				'paymentsDate' => '2020-11-04 04:04:33',
				'shipmentDate' => '2020-11-04 04:15:32',
				'reportingDate' => '2020-11-04 05:26:27',
				'buyerEmail' => '0tt53b0b71xt3xg@marketplace.amazon.ca',
				'buyerName' => 'Kathleen Cabales',
				'buyerPhoneNumber' => '',
				'productName' => 'Visismile Lash lift Kit, Professional Eyelash Perm Kit,Lash Curling, Semi-Permanent Curling Perming Wave Liquid Set Lash Lifting tools Eye patch, Micr',
				'itemPrice' => 22.99,
				'itemTax' => 0,
				'shippingPrice' => 0.88,
				'shippingTax' => 0,
				'giftWrapPrice' => 0,
				'giftWrapTax' => 0,
				'shipPromotionDiscount' => -0.88,
				'itemPromotionDiscount' => 0,
				'shipServiceLevel' => 'Expedited',
				'billAddress1' => '',
				'billAddress2' => '',
				'billAddress3' => '',
				'billCity' => '',
				'billState' => '',
				'billPostalCode' => '',
				'billCountry' => '',
				'carrier' => 'AMZN_CA',
				'trackingNumber' => 'TBC138481669009',
				'estimatedArrivalDate' => '2020-11-05 20:00:00',
				'fulfillmentCenterId' => 'YYC1',
				'fulfillmentChannel' => 'AFN',
				'salesChannel' => 'Amazon.ca',
				'currency' => 'CAD'
    		]),
    	];
    }
}
