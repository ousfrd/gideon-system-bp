<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ListingReportReaderTest extends ListingReportBaseTest
{
    /**
     * @dataProvider getListingRecords
     *
     * @return void
     */
    public function testGetListings($listingRecords, $targetListingRecord)
    {
        $listingRecordsArr = [];
        foreach ($listingRecords as $listingRecord) {
            array_push($listingRecordsArr, $listingRecord);
        }
        $this->assertTrue(in_array($targetListingRecord, $listingRecordsArr));
    }
}
