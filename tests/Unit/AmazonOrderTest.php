<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Helpers\OrderReportReader;
use App\AmazonOrder;

class AmazonOrderTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testFromOrderRecord()
    {
        $orders = [];
    	foreach ($this->getOrders() as $order) {
    		if (in_array($order->amazonOrderId, ['111-2403454-1067405', '702-3957019-6105034'])) {
    			var_dump($orderShipment);
    		}
    		array_push($orders, $order);
    	}

    	foreach ($this->getTargetOrders() as $targetOrder) {
    		$this->assertTrue(in_array($targetOrder, $orders));
    	}
    }

    public function getOrders() {
    	$orderReportPathes = [
    		__DIR__ . '/../data/amazon/orders.txt',
    	];
    	foreach ($orderReportPathes as $orderReportPath) {
    		$orders = OrderReportReader::getOrders($orderReportPath);
    		foreach ($orders as $order) {
    			yield $order;
    		}
    	}
    }

    public function getTargetOrders() {
    	return [
    		AmazonOrder::fromArray([
    			'amazonOrderId' => '702-4346718-9104228',
				'purchaseDate' => '2020-11-02T06:14:47+00:00',
				'orderStatus' => 'Shipped',
				'sku' => '5S-B3BH-P9AK',
				'asin' => 'B00FPKPBAE',
				'productName' => 'Econobum Exfoliating Foot Peel Mask 2 Pack, Peeling Away Calluses and Dead Skin cells, Get Soft Baby Feet in 1-2 Weeks',
				'quantity' => 1,
				'currency' => 'CAD',
				'itemPrice' => 19.99,
				'itemTax' => 0,
				'shippingPrice' => 2.74,
				'shippingTax' => 0,
				'giftWrapPrice' => 0,
				'giftWrapTax' => 0,
				'itemPromotionDiscount' => 0,
				'shipPromotionDiscount' => 0,
				'salesChannel' => 'Amazon.ca',
				'fulfillmentChannel' => 'Amazon',
    		]),
    	];
    }
}
