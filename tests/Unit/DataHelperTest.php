<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Helpers\DataHelper;

class DataHelperTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGroupByVal()
    {
    	$data = [];
    	$person1 = (object)['name' => 'David', 'email' => 'david@example.com'];
    	$person2 = (object)['name' => 'Neal', 'email' => 'neal@example.com'];
    	$person3 = (object)['name' => 'Steve', 'email' => 'steve@example.com'];
    	$person4 = (object)['name' => 'Amy', 'email' => 'amy@example.com'];
    	$person5 = (object)['name' => 'Yolanda', 'email' => 'yolanda@example.com'];
    	$person6 = (object)['name' => 'Yolanda', 'email' => 'yolanda2@example.com'];
    	$person7 = (object)['name' => 'Amy', 'email' => 'amy2@example.com'];
    	for ($i = 1; $i <= 7; $i++) {
    		array_push($data, ${"person" . $i});
    	}

    	$names = array_column($data, 'name');
    	$groupedData = DataHelper::groupByVal($data, 'name');
    	foreach ($groupedData as $k => $v) {
    		$this->assertTrue(in_array($k, $names));
    	}

    	$this->assertEquals(count($groupedData['David']), 1);
    	$this->assertTrue(in_array($person1, $groupedData['David']));

    	$this->assertEquals(count($groupedData['Yolanda']), 2);
    	$this->assertTrue(in_array($person5, $groupedData['Yolanda']));
    	$this->assertTrue(in_array($person6, $groupedData['Yolanda']));

    	$this->assertEquals(count($groupedData['Amy']), 2);
    	$this->assertTrue(in_array($person4, $groupedData['Amy']));
    	$this->assertTrue(in_array($person7, $groupedData['Amy']));
    }

    public function testGroupByDate()
    {
    	$data = [];
    	$person1 = (object)['name' => 'David', 'email' => 'david@example.com', 'birthday' => '2020-09-21 08:33'];
    	$person2 = (object)['name' => 'Neal', 'email' => 'neal@example.com', 'birthday' => '2020-09-21 08:33'];
    	$person3 = (object)['name' => 'Steve', 'email' => 'steve@example.com', 'birthday' => '2020-09-20 07:33'];
    	$person4 = (object)['name' => 'Amy', 'email' => 'amy@example.com', 'birthday' => '2020-09-20 08:33'];
    	$person5 = (object)['name' => 'Yolanda', 'email' => 'yolanda@example.com', 'birthday' => '2020-09-21 07:33'];
    	$person6 = (object)['name' => 'Yolanda', 'email' => 'yolanda2@example.com', 'birthday' => '2020-09-21 07:33'];
    	$person7 = (object)['name' => 'Amy', 'email' => 'amy2@example.com', 'birthday' => '2020-09-21 09:33'];
    	for ($i = 1; $i <= 7; $i++) {
    		array_push($data, ${"person" . $i});
    	}

    	$groupedData = DataHelper::groupByDate($data, 'birthday', 'America/Los_Angeles', 'day');
    	$this->assertTrue(array_key_exists('2020-09-20', $groupedData));
    	$this->assertTrue(in_array($person3, $groupedData['2020-09-20']));
    	$this->assertTrue(in_array($person4, $groupedData['2020-09-20']));

    	$this->assertTrue(array_key_exists('2020-09-21', $groupedData));
    	$this->assertTrue(in_array($person1, $groupedData['2020-09-21']));
    	$this->assertTrue(in_array($person2, $groupedData['2020-09-21']));
    	$this->assertTrue(in_array($person5, $groupedData['2020-09-21']));
    	$this->assertTrue(in_array($person6, $groupedData['2020-09-21']));
    	$this->assertTrue(in_array($person7, $groupedData['2020-09-21']));

    	$groupedData = DataHelper::groupByDate($data, 'birthday', 'America/Los_Angeles', 'hour');
    	$this->assertTrue(array_key_exists('2020-09-20T00', $groupedData));
    	$this->assertTrue(in_array($person3, $groupedData['2020-09-20T00']));

    	$this->assertTrue(array_key_exists('2020-09-20T01', $groupedData));
    	$this->assertTrue(in_array($person4, $groupedData['2020-09-20T01']));

    	$this->assertTrue(array_key_exists('2020-09-21T00', $groupedData));
    	$this->assertTrue(in_array($person5, $groupedData['2020-09-21T00']));
    	$this->assertTrue(in_array($person6, $groupedData['2020-09-21T00']));

    	$this->assertTrue(array_key_exists('2020-09-21T01', $groupedData));
    	$this->assertTrue(in_array($person1, $groupedData['2020-09-21T01']));
    	$this->assertTrue(in_array($person2, $groupedData['2020-09-21T01']));

    	$this->assertTrue(array_key_exists('2020-09-21T02', $groupedData));
    	$this->assertTrue(in_array($person7, $groupedData['2020-09-21T02']));
    }

    public function testAggregate()
    {
    	$data = [
    		(object)['name' => 'David', 'email' => 'david@example.com', 'age' => 1],
    		(object)['name' => 'Neal', 'email' => 'neal@example.com', 'age' => 2],
    		(object)['name' => 'Steve', 'email' => 'steve@example.com', 'age' => 3],
    		(object)['name' => 'Amy', 'email' => 'amy@example.com', 'age' => 4],
    		(object)['name' => 'Yolanda', 'email' => 'yolanda@example.com', 'age' => 5],
    		(object)['name' => 'Yolanda', 'email' => 'yolanda2@example.com', 'age' => 6],
    		(object)['name' => 'Amy', 'email' => 'amy2@example.com', 'age' => 7]
    	];

    	$ageTotal = DataHelper::aggregate($data, 'age', 'sum');
    	$this->assertEquals(28, $ageTotal);

    	$maxAge = DataHelper::aggregate($data, 'age', 'max');
    	$this->assertEquals(7, $maxAge);

    	$minAge = DataHelper::aggregate($data, 'age', 'min');
    	$this->assertEquals(1, $minAge);

    	$avgAge = DataHelper::aggregate($data, 'age', 'avg');
    	$this->assertEquals(4, $avgAge);

    	$countAge = DataHelper::aggregate($data, 'age', 'count');
    	$this->assertEquals([1 => 1, 2 => 1, 3 => 1, 4 => 1, 5 => 1, 6 => 1, 7 => 1], $countAge);
    }
}
