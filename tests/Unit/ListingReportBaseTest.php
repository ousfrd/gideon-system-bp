<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Helpers\ListingReportReader;

class ListingReportBaseTest extends TestCase
{
    public function getListingRecords()
    {
        $result = [];

        $listingReportPath = __DIR__ . '/../data/amazon/All+Listings+Report+09-11-2020.txt';
        $targetListingRecord = [
            'item-name' => "Derma Roller for Beard Growth, Beard Growth Serum, Microneedle Roller & Natural Hair Growth Serum & Balm Wax & Comb for Men with Facial Hair Growth, Promote Hair Regrowth (4 in 1)",
            'item-description' => "<p><b>Don't spend thousands of dollars to improve your skin and facial hair!!! Try our beard care tool + growth serum!</b></p> <p>Our beard care tool + beard growth serum for men is designed to improve the elasticity and vibrancy of the skin while stimulating hair growth! The tool size with a 0.3 mm needle is considered the safest size and most effective size for home use. Our skin roller is the best option for new users who want to maintain and improve their facial hair and overall skin health. The growth serum is designed to moisturize and nourish hair while promoting hair follicle stimulation to fill in patches and thicken hair. Please use our skin roller with our beard growth serum for better results.</p> <p>Our beard care tool and growth serum are manufactured with the highest quality materials and quality-control. </p> <p><b>✔ BENEFITS:</b></p> <p>1. Increases the absorption of topical skincare products over 500%, maximizing their effectiveness.</p> <p>2. Improve skin tone and stimulate hair growth.</p> <p>3. Safe and painless (no bleeding).</p> <p><b>✔ DIRECTIONS:</b></p> <p>1. Sterilize tool head in alcohol solution BEFORE and AFTER each use.</p> <p>2. Wash your face.</p> <p>3. Apply gentle pressure on your skin and area to stimulate hair growth. This beard care tool is designed to puncture the skin.</p> <p>4. Apply serum after rolling.</p> <p>5. Use twice a day for optimal results.</p> <p>6. Replace roller once every 2-3 months.</p> <p><b>✔ AVOID USE:</b></p> <p>1. On acne or irritated skin.</p> <p>2. Under eyes or on lips.</p> <p>3. If irritation occurs, discontinue use.</p> <p>4. Do not share it with others.</p> <p>5. Please keep away from children.</p> <p><b>✔ What’s Included?</b></p> <p>x1 Beard care roller</p> <p>x1 Beard growth serum</p><p>x1 Beard care balm</p><p>x1 Beard comb</p>",
            'listing-id' => '0804XQXO64U',
            'seller-sku' => 'AD-ZOBS-VJ4X',
            'price' => '35.99',
            'quantity' => '',
            'open-date' => '2020-08-04 15:05:45 PDT',
            'image-url' => '',
            'item-is-marketplace' => 'y',
            'product-id-type' => '1',
            'zshop-shipping-fee' => '',
            'item-note' => '',
            'item-condition' => '11',
            'zshop-category1' => '',
            'zshop-browse-path' => '',
            'zshop-storefront-feature' => '',
            'asin1' => 'B08C6XBPVN',
            'asin2' => '',
            'asin3' => '',
            'will-ship-internationally' => '',
            'expedited-shipping' => '',
            'zshop-boldface' => '',
            'product-id' => 'B08C6XBPVN',
            'bid-for-featured-placement' => '',
            'add-delete' => '',
            'pending-quantity' => '',
            'fulfillment-channel' => 'AMAZON_NA',
            'merchant-shipping-group' => 'Migrated Template',
            'status' => 'Active'
        ];
        $result[] = [ListingReportReader::getListings($listingReportPath), $targetListingRecord];

        $listingReportPath = __DIR__ . '/../data/amazon/All+Listings+Report+851.txt';
        $targetListingRecord = [
            'item-name' => 'LUELLI Teeth Whitening Kit - 5X LED Light Tooth Whitener with 35% Carbamide Peroxide, Mouth Trays, Remineralizing Gel',
            'item-description' => '',
            'listing-id' => '1019X5MD4I7',
            'seller-sku' => 'LN-LKUM-RMJU',
            'price' => '54.99',
            'quantity' => '',
            'open-date' => '2020-10-19 09:03:26 PDT',
            'image-url' => '',
            'item-is-marketplace' => 'y',
            'product-id-type' => '1',
            'zshop-shipping-fee' => '',
            'item-note' => '',
            'item-condition' => '11',
            'zshop-category1' => '',
            'zshop-browse-path' => '',
            'zshop-storefront-feature' => '',
            'asin1' => 'B088Q5RPZ9',
            'asin2' => '',
            'asin3' => '',
            'will-ship-internationally' => '',
            'expedited-shipping' => '',
            'zshop-boldface' => '',
            'product-id' => 'B088Q5RPZ9',
            'bid-for-featured-placement' => '',
            'add-delete' => '',
            'pending-quantity' => '',
            'fulfillment-channel' => 'AMAZON_NA',
            'merchant-shipping-group' => 'Migrated Template',
            'status' => 'Active'
        ];
        $result[] = [ListingReportReader::getListings($listingReportPath), $targetListingRecord];

        return $result;
    }

    public function getSkus() {
        $skusPath = __DIR__ . '/../data/amazon/skus.txt';
        $skus = file($skusPath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        $dsSkusPath = __DIR__ . '/../data/amazon/ds_skus.txt';
        $dsSkus = file($dsSkusPath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        return [[$skus, $dsSkus]];
    }
}
