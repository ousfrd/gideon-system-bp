<form class="" method="Post" action="{{route('account.add',[$account->id])}}">
{{Form::token()}}


<div class="row">
	<div class="col-md-4">
		<div class="form-group">
		    {{ Form::label("Seller ID", null, ['class' => 'control-label']) }}
		   
		    {{ Form::text('data[seller_id]', $account->seller_id, array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
		    
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="form-group">
	    {{ Form::label("Region", null, ['class' => 'control-label col-md-4']) }}
	   
	    {{ Form::select('data[region]', config('app.regions'), "",['class' => 'form-control','required']) }}
	    
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="form-group">
	    {{ Form::label("Marketplaces", null, ['class' => 'control-label col-md-4']) }}

	     @foreach(config('app.marketplaces') as $key => $value)

	     <input class="form-check-input" type="checkbox" name="data[marketplaces][]" value="{{$key}}" style="padding-left: 5px;">{{$key}}

	    @endforeach
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group">
	    {{ Form::label("Storefront Name", null, ['class' => 'control-label']) }}
	   
	    {{ Form::text('data[name]', $account->name, array_merge(['class' => 'form-control','required','placeholder'=>'Name'])) }}
	   
	</div>
	</div>
</div>

<div class="row">
	<div class="col-md-4">
	<div class="form-group">
    {{ Form::label("Seller Email", null, ['class' => 'control-label col-md-4']) }}
   
    {{ Form::text('data[seller_email]', $account->seller_email, array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
   
	</div>	
	</div>


	<div class="col-md-4">
	<div class="form-group">
    {{ Form::label("SMTP Host", null, ['class' => 'control-label col-md-4']) }}
   
   	{{ Form::text('data[smtp_host]', $account->smtp_host, array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
	</div>
	</div>


	<div class="col-md-4">
	<div class="form-group">
    {{ Form::label("SMTP Username", null, ['class' => 'control-label col-md-4']) }}
   
   	{{ Form::text('data[smtp_username]', $account->smtp_username, array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
   
	</div>	
	</div>
</div>

<div class="row">
	<div class="col-md-4">
	<div class="form-group">
    {{ Form::label("SMTP Password", null, ['class' => 'control-label col-md-4']) }}
   
   	{{ Form::text('data[smtp_password]', $account->smtp_password, array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
   
	</div>	
	</div>


	<div class="col-md-4">
		<div class="form-group">
		     {{ Form::label("Auth Token", null, ['class' => 'control-label']) }}
    
    		 {{ Form::text('data[mws_auth]', $account->mws_auth, array_merge(['class' => 'form-control','placeholder'=>''])) }}
    
		    
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="form-group">
		  
		  {{ Form::label("Manager", null, ['class' => 'control-label']) }}
   		 <div>
    	 {{ Form::select('data[managers][]',  $all_managers,$account_managers,['class' => 'form-control','multiple' => 'multiple']) }}
    	 </div>
		    
		</div>
	</div>
	
</div>


<div class="row">
	<div  class="col-md-4">
		<div class="form-group">
		    {{ Form::label("Status", null, ['class' => 'control-label']) }}
		   
		    {{ Form::select('data[status]', ['Active'=>'Active','Suspended'=>'Suspended','Blocked'=>'Blocked'],$account->status,['class' => 'form-control','required']) }}
		 
		</div>
	</div>
	

</div>








<div class="row">
    <div class="pull-right">
<!-- 	<button class="btn btn-default cancelSidebarForm btn btn-default close-animatedModal" type="button"><i class="fa fa-times-circle"></i> Cancel</button> -->
	<button class="btn btn-orange saveSidebarForm btn btn-default" data-cbfunction="settingsEdit" type="submit"><i class="fa fa-save"></i> Save</button>
	</div>
</div>







<script type="text/javascript">
$(document).ready(function(){
	$('form').submit(function(){
		
		//$('#animatedModal').css({'z-index':0});
		$('.modal-loading').fadeIn()

		
	})
})
</script>

</form>