<form class="" method="Post" action="{{route('account.edit',[$account->id])}}">
{{Form::token()}}

<div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Account {{$account->code}}</h4>
      </div>
<div class="modal-body">



		<div class="form-group row">
		    {{ Form::label("Seller ID", null, ['class' => 'control-label col-md-4']) }}
		   	<div class="col-md-6">
		    {{ Form::text('data[seller_id]', $account->seller_id, array_merge(['class' => 'form-control','disabled','placeholder'=>''])) }}
		    </div>
		    
		</div>
	
	
		<div class="form-group row">
	    {{ Form::label("Region", null, ['class' => 'control-label col-md-4']) }}
	   
	     <div class="col-md-6">{{ Form::select('data[region]', config('app.regions'), $account->region,['class' => 'form-control','disabled']) }}</div>
	    
		</div>
	

		<div class="form-group row">
	    {{ Form::label("Marketplaces", null, ['class' => 'control-label col-md-4']) }}

	    <div class="col-md-6">
	     <div class="marketplaces">

	     	@foreach(config('app.marketplaceRegions')[$account->region] as $key => $value)
	     		<input class="form-check-input" type="checkbox" name="data[marketplaces][]" value="{{$key}}" style="padding-left: 5px;" {{ in_array($key, explode(',', $account->marketplaces)) ? 'checked' : ''}}>{{$key}}
	     	@endforeach

	 	</div>
	    </div>

		</div>


		<div class="form-group row">
	    {{ Form::label("Storefront Name", null, ['class' => 'control-label col-md-4']) }}
	   
	    <div class="col-md-6">{{ Form::text('data[name]', $account->name, array_merge(['class' => 'form-control','required','placeholder'=>'Name'])) }}</div>
	   
		</div>


		<div class="form-group row">
	    {{ Form::label("Alias (for innner use)", null, ['class' => 'control-label col-md-4']) }}
	   
	    <div class="col-md-6">{{ Form::text('data[code]', $account->code, array_merge(['class' => 'form-control ','required','placeholder'=>'Code'])) }}</div>
	   
		</div>

		<div class="form-group row">
	    {{ Form::label("Only FBA(For dropshipping account)", null, ['class' => 'control-label col-md-4']) }}
	   
	    <div class="col-md-6">
	    	<input name="data[only_fba]" type="checkbox" value="Yes" {{ $account->only_fba == 1 ? "checked" : "" }} /></div>
	   
		</div>	

		<div class="form-group row">
	    {{ Form::label("Seller Email", null, ['class' => 'control-label col-md-4']) }}
	   
	    <div class="col-md-6">{{ Form::text('data[seller_email]', $account->seller_email, array_merge(['class' => 'form-control','required','placeholder'=>''])) }}</div>
	   
		</div>	


		<div class="form-group row">
	    {{ Form::label("SMTP Host", null, ['class' => 'control-label col-md-4']) }}
	   
	    <div class="col-md-6">
	    	<input name="data[smtp_host]" type="text" class="form-control" value="{{$account->smtp_host}}" /></div>
	   
		</div>	

		<div class="form-group row">
	    {{ Form::label("SMTP Username", null, ['class' => 'control-label col-md-4']) }}
	   
	    <div class="col-md-6">
	    	<input name="data[smtp_username]" type="text" class="form-control" value="{{$account->smtp_username}}" /></div>
	   
		</div>	

		<div class="form-group row">
	    {{ Form::label("SMTP Password", null, ['class' => 'control-label col-md-4']) }}
	   
	    <div class="col-md-6">
	    	<input name="data[smtp_password]" type="password" class="form-control" value="{{$account->smtp_password}}" /></div>
	   
		</div>	
	
		<div class="form-group row">
		     {{ Form::label("Auth Token", null, ['class' => 'control-label col-md-4']) }}
    
    		<div class="col-md-6"> {{ Form::text('data[mws_auth]',$account->mws_auth, array_merge(['class' => 'form-control','placeholder'=>''])) }}</div>
    
		    
		</div>
	
<!-- 		<div class="form-group row"> -->
		  
<!-- 		  {{ Form::label("Default Currency", null, ['class' => 'control-label col-md-4']) }} -->
<!--    		 <div class="col-md-6"> -->
<!--    		 {{ Form::select('data[currency]', config('app.currencies') , $account->currency?$account->currency:'USD',['class' => 'form-control role-field','required']) }}</div> -->
    	    
<!-- 		</div> -->
		
		<div class="form-group row">
			
		  {{ Form::label("Manager", null, ['class' => 'control-label col-md-4']) }}
   		 <div class="col-md-6">
   		 <select name="data[managers][]" class="products-managed form-control role-field " multiple="multiple" style="width: 100% !important">
			@foreach($all_managers as $id=> $manager)
			<option value="{{$id}}" @if(array_key_exists($id,$account_managers)) selected @endif >{{$manager}}</option>
			@endforeach
		 </select></div>
    	    
		</div>
	
		<div class="form-group row">
		    {{ Form::label("Status", null, ['class' => 'control-label col-md-4']) }}
		   
		   <div class="col-md-6"> {{ Form::select('data[status]', ['Active'=>'Active','Suspended'=>'Suspended','Blocked'=>'Blocked'],$account->status,['class' => 'form-control','required']) }}</div>
		 
		</div>

		<div class="form-group row">
	    	{{ Form::label("Enable Disburse", null, ['class' => 'control-label col-md-4']) }}
		    <div class="col-md-6">
		    	<input name="data[disburse_enabled]" type="checkbox" value="1" checked />
		    </div>
		</div>

		<div class="form-group row">
		    {{ Form::label("Minimum Disburse Amount", null, ['class' => 'control-label col-md-4']) }}
		   
		    <div class="col-md-6">
		    	{{ Form::text('data[min_disburse_amount]', $account->min_disburse_amount, ['class' => 'form-control ','required','placeholder'=>'0']) }}
		    </div>
		</div>
</div>

<div class="modal-footer">
      	
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" id="newuserModelBtn" class="btn btn-primary">Submit</button>
      </div>
      
  <div class="modal-spinner" style="display: none"><div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div></div>    
{!! Form::close() !!}







<script type="text/javascript">
$(document).ready(function(){
	$('form').submit(function(){
		
		//$('#animatedModal').css({'z-index':0});
		$('.modal-spinner').show()

		
	})

	
})

 $('select[multiple="multiple"]').multiselect();
</script>

</form>