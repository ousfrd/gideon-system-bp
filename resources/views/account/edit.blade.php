@extends('layouts.master')
@section('title', 'Edit Account')
@section('content')
<div class="btn-actions">
<button class="back-btn" onclick="window.location='{{url()->previous()}}'"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true">Back</span></button>
</div>

@include('account.form.edit')


<script>
  $( function() {
    
  } );
  </script>
  
@endsection