@extends('layouts.master')
@section('title', "Manage Accounts")
@section('content')
<div class="btn-actions">
<a class="btn btn-default pull-right"  data-toggle="modal" data-target="#addAccountModal"><span class="" >Add New Account</span></a>
</div>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$("#Emails tr").each(function(){
		$(this).addClass($(this).find(".column-status").text())
	})

	$('.checkmws').click(function(){
		var elm = $(this)
		var span = $(this).parent().find('span')
		
		elm.hide()
		span.html('Checking...')
		
		$.get($(this).attr('href'),function(r){
			span.html(r)
			elm.show()
		})
		return false;
	})
})
//-->
</script>






<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"  style="background: #fff">
        <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
        </div> <!-- /.modal-content -->
        
      
      <div class="modal-spinner"><div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div></div>
    </div> <!-- /.modal-dialog -->
</div> 

<script>
$(document).ready(function(){
	$('body').on('hidden.bs.modal', '#editModal', function () {
	    $(this).removeData('bs.modal');
		$('.modal-content', this).html('<div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>');
	});

	$('.editable').editable();
})

</script>

@include('account.parts.newmodal')

{!! $grid !!}
@endsection