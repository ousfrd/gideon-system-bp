<!-- Modal -->
<div class="modal fade" id="addAccountModal" tabindex="-1" role="dialog" aria-labelledby="addUserModal">
  <form class="form-horizontal" id="addAccountForm">
  <div class="modal-dialog" role="document">
    <div class="modal-content"  style="background: #fff">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Account</h4>
      </div>
      <div class="modal-body">
      	<p class="text-danger msg" style="display:none"></p>
      	

{{Form::token()}}


	
		<div class="form-group row">
		    {{ Form::label("Seller ID", null, ['class' => 'control-label col-md-4']) }}
		   	<div class="col-md-6">
		    {{ Form::text('data[seller_id]', "", array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
		    </div>
		    
		</div>
	
	
		<div class="form-group row">
	    {{ Form::label("Region", null, ['class' => 'control-label col-md-4']) }}
	   
	     <div class="col-md-6">{{ Form::select('data[region]', config('app.regions'), "",['class' => 'form-control regionSelect','required']) }}</div>
	    
		</div>
	
		<div class="form-group row">
	    {{ Form::label("Marketplaces", null, ['class' => 'control-label col-md-4']) }}

	    <div class="col-md-6">
	     @foreach(config('app.regions') as $region => $value)
	     <div class="marketplaces marketplaces{{$region}}">

	     	@foreach(config('app.marketplaceRegions')[$region] as $key => $value)
	     		<input class="form-check-input" type="checkbox" name="data[marketplaces][]" value="{{$key}}" style="padding-left: 5px;">{{$key}}
	     	@endforeach

	 	</div>
	    @endforeach
	    </div>

		</div>


		<div class="form-group row">
	    {{ Form::label("Storefront Name", null, ['class' => 'control-label col-md-4']) }}
	   
	    <div class="col-md-6">{{ Form::text('data[name]', "", array_merge(['class' => 'form-control','required','placeholder'=>'Name'])) }}</div>
	   
		</div>
	

		<div class="form-group row">
	    {{ Form::label("Alias (for innner use)", null, ['class' => 'control-label col-md-4']) }}
	   
	    <div class="col-md-6">{{ Form::text('data[code]', "", array_merge(['class' => 'form-control ','required','placeholder'=>'Code'])) }}</div>
	   
		</div>

		<div class="form-group row">
	    {{ Form::label("Only FBA(For dropshipping account)", null, ['class' => 'control-label col-md-4']) }}
	   
	    <div class="col-md-6">
	    	<input name="data[only_fba]" type="checkbox" value="Yes" /></div>
	   
		</div>	

		<div class="form-group row">
		     {{ Form::label("Seller Email", null, ['class' => 'control-label col-md-4']) }}
    
    		<div class="col-md-6"> {{ Form::text('data[seller_email]',"", array_merge(['class' => 'form-control','placeholder'=>''])) }}</div>
    
		    
		</div>

		<div class="form-group row">
	    {{ Form::label("SMTP Host", null, ['class' => 'control-label col-md-4']) }}
	   
	    <div class="col-md-6">
	    	<input name="data[smtp_host]" type="text" class="form-control" value="" /></div>
	   
		</div>	

		<div class="form-group row">
	    {{ Form::label("SMTP Username", null, ['class' => 'control-label col-md-4']) }}
	   
	    <div class="col-md-6">
	    	<input name="data[smtp_username]" type="text" class="form-control" value="" /></div>
	   
		</div>	

		<div class="form-group row">
	    {{ Form::label("SMTP Password", null, ['class' => 'control-label col-md-4']) }}
	   
	    <div class="col-md-6">
	    	<input name="data[smtp_password]" type="password" class="form-control" value="" /></div>
	   
		</div>	
	
		<div class="form-group row">
		     {{ Form::label("Auth Token", null, ['class' => 'control-label col-md-4']) }}
    
    		<div class="col-md-6"> {{ Form::text('data[mws_auth]',"", array_merge(['class' => 'form-control','placeholder'=>''])) }}</div>
    
		    
		</div>
	
		<div class="form-group row">
		  
		  {{ Form::label("Manager", null, ['class' => 'control-label col-md-4']) }}
   		 <div class="col-md-6">
   		 <select name="data[managers][]" class="products-managed form-control role-field " multiple="multiple" style="width: 100% !important">
			@foreach($all_managers as $id=> $manager)
			<option value="{{$id}}">{{$manager}}</option>
			@endforeach
		 </select></div>
    	    
		</div>
	
		<div class="form-group row">
		    {{ Form::label("Status", null, ['class' => 'control-label col-md-4']) }}
		   
		   <div class="col-md-6"> {{ Form::select('data[status]', ['Active'=>'Active','Suspended'=>'Suspended','Blocked'=>'Blocked'],"",['class' => 'form-control','required']) }}</div>
		 
		</div>

		<div class="form-group row">
	    	{{ Form::label("Enable Disburse", null, ['class' => 'control-label col-md-4']) }}
		    <div class="col-md-6">
		    	<input name="data[disburse_enabled]" type="checkbox" value="1" checked />
		    </div>
		</div>

		<div class="form-group row">
		    {{ Form::label("Minimum Disburse Amount", null, ['class' => 'control-label col-md-4']) }}
		   
		    <div class="col-md-6">
		    	{{ Form::text('data[min_disburse_amount]', "", array_merge(['class' => 'form-control ','required','placeholder'=>'0'])) }}
		    </div>
		</div>
 </div>
      <div class="modal-footer">
      	
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" id="newuserModelBtn" class="btn btn-primary">Submit</button>
      </div>
      
      <div class="modal-spinner"><div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div></div>
      
    </div>
  </div>
  </form>
		 
</div>

<script>
 		$('.marketplaces').hide();$('.marketplacesUS').show();
 		$('.regionSelect').change(function() {
 			var region = $(this).val();
			$('.marketplaces').hide();$('.marketplaces'+region).show();
 		})

  		$('#addAccountForm').submit(function(){
  			$('.msg').html('').hide()
			$(this).attr('disabled',true)
			$('.modal-spinner').show()
			$.post('{{route('account.add')}}',$('#addAccountForm').serialize(),function(response){
				if(response == 1) {
					window.location.reload()
				}
			}).fail(function(xhr, status, error) {
		        var ul = $('<ul>')
				$.each($.parseJSON(xhr.responseText), function(k, v) {
					ul.append('<li>'+k+': '+v[0]+'</li>')
				});
				
				$('.msg').append(ul).show()
		        $(this).attr('disabled',false)
				$('.modal-spinner').hide()

				
		    })

			return false
  	  	})
  	  	
  // 	  	$(".products-managed").select2({
		//   // tags: true
		// })



  </script>
  
  <style>
 
  </style>



