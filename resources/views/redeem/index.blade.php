@extends('layouts.master')
@section('title', $title)
@section('content')
@if (isset($redeemType))
<div class="btn-actions">
  <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#newRedeemModal">New</button>
</div>
@include("redeem.parts.form", ['redeemType' => $redeemType])
@endif
<div class="nayjest-grid">
  {!! $grid !!}
</div>
<script type="text/javascript">
$('.pUpdate').editable({
	//showbuttons:false,
	//onblur:'submit',
	savenochange:false,
    type: 'text',
    url:'{{route('redeem.ajaxsave')}}',  
    //title: 'Enter value',
    placement: 'top', 
    send:'auto',
    ajaxOptions: {
   		 dataType: 'json'
    },
    success: function(response, newValue) {
      
    } 
})

$('.handling_status').each(function(){
	$(this).editable({
        inputclass: 'input-large',
        type: 'select',
        url:'{{route('redeem.ajaxsave')}}',
        send:'auto',
        width: '250px',
        source: ['In Progress', 'Done']
    })

});
</script>
@endsection