@extends('layouts.master')
@section('title', $title)
@section('content')
<div class="row" id="redeem-dashboard">
  <div class="box col-lg-3 col-md-4 col-sm-4 col-xs-12" data-url="{{route('redeems.freeProduct')}}">
    <div class="small-box bg-background-0">
        <div class="inner">
          <p>Free Products Requests</p>
        </div>
        <a href="{{route('redeems.freeProduct')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
  </div>

  <div class="box col-lg-3 col-md-4 col-sm-4 col-xs-12" data-url="{{route('redeems.giftcard')}}">
    <div class="small-box bg-background-1">
        <div class="inner">
          <p>Gift Card Requests</p>
        </div>
        <a href="{{route('redeems.giftcard')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
  </div>

  <div class="box col-lg-3 col-md-4 col-sm-4 col-xs-12" data-url="{{route('redeems.other')}}">
    <div class="small-box bg-background-2">
        <div class="inner">
          <p>Negative Feedbacks</p>
        </div>
        <a href="{{route('redeems.other')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
  </div>

  <div class="box col-lg-3 col-md-4 col-sm-4 col-xs-12" data-url="{{route('redeems.list-invite-reviews')}}">
    <div class="small-box bg-background-3">
        <div class="inner">
          <p>Invite Review Request</p>
        </div>
        <a href="{{route('redeems.list-invite-reviews')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    $('#redeem-dashboard .box').on('click', function() {
      window.location = $(this).data('url');
    });
  });
</script>
@endsection
