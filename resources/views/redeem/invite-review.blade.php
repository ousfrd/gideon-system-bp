@extends('layouts.master')
@section('title', $title)
@section('content')
<section id="redeem-invite-review-letter">
  <div class="row">
    <div class="panel panel-info col-sm-6 col-xs-12">
      <div class="panel-heading">Basic Info</div>
      <div class="panel-body">
        <table class="table table-striped">
          <tbody>
            <tr>
              <th>Request Date</th>
              <td>{{ $redeem->created_at }}</td>
            </tr>
            <tr>
              <th>Name</th>
              @if ($redeem->orderShipment)
              <td><a id="user-name" target="_blank" href="{{route('customers.show', ['id' => $redeem->orderShipment->buyer_email])}}">{{ $redeem->name }}</a></td>
              @else
              <td>{{ $redeem->name }}</td>
              @endif
            </tr>
            <tr>
              <th>Email</th>
              <td id="user-email">{{ $redeem->email }}</td>
            </tr>
            <tr>
              <th>Order #</th>
              <td id="order-number">{{ $redeem->AmazonOrderId }}</td>
            </tr>
            <tr>
              <th>Product</th>  
              <td id="product-name">{{ $redeem->product->name }}</td>
            </tr>
            <tr>
              <th>ASIN</th>
              <td>{{ $redeem->asin }}</td>
            </tr>
            <tr>
              <th>Brand</th>
              <td>{{ $redeem->product->brand }}</td>
            </tr>
            <tr>
              <th>Shipment Date</th>
              @if ($redeem->orderShipment)
              <td>{{ $redeem->orderShipment->shipment_date }}</td>
              @else
              <td>N/A</td>
              @endif
            </tr>
          </tbody>
        </table>
      </div>
      
      <div class="panel-heading">Sent Letter</div>
      <div id='sent-letters' class="panel-body">
        @if ($redeem->inviteReviewLetters)
          <ul>
          @foreach ($redeem->inviteReviewLetters as $letter)
              <li><a href="javascript:void(0)" data-letter-id="{{$letter->id}}" class="modal-opener">{{$letter->subject}} - {{$letter->created_at}}</a></li>
            @endforeach
          </ul>
        @endif
      </div>
    </div>

    <div class="panel panel-primary col-sm-6 col-xs-12">
      <div class="panel-heading">Eligible Products</div>
      <div class="panel-body">
        @if(count($eligibleProducts) == 0)
        <div class="alert alert-warning" role="alert">
          No Products are available.
        </div>
        @else
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Image</th>
              <th>Asin</th>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            @foreach($eligibleProducts as $product)
              <tr>
                <td><img src="{{ $product->small_image }}" alt=""></td>
                <td> {{$product->asin }} </td>
                <td> {{$product->short_name }} </td>
              </tr>
            @endforeach
          </tbody>
        </table>
        @endif
      </div>
    </div>
  </div>

  <div class="panel panel-success">
    <div class="panel-heading">Letter</div>
    <div class="panel-body">
      {{ Form::open(array(
          'url' => route('redeems.send-invite-review-letter', ['id' => $redeem->id]), 
          'method'=>'POST',
          'class'=>'form-inline',
          'id' => 'letter-from'
        )) 
      }}
        <div class="form-group">
          <label for="">Letter Template</label>
          <select required class="form-control" name="invite_review_letter_template_id" id="letter-template">
            <option value="">--- Select ---</option>
          </select>
        </div>
        
        <div class="form-group">
          <label for="">Bonus</label>
          <input type="number" id='bonus' class="form-control">
        </div>

        <div class="form-group">
          <label for="">Sender Name</label>
          <input type="text" id='sender-name' class="form-control">
        </div>

        <div class="form-group">
          <label for="">&nbsp;</label>
          <button id="mail" type="button" class="btn btn-success">Mail</button>
        </div>

        @if ($redeem->orderShipment)
        <input type="hidden" name="buyer_id" value="{{$redeem->orderShipment->buyer_email}}">
        @else
        <input type="hidden" name="buyer_id" value="">
        @endif
        <input type="hidden" name="products" value="{{$eligibleProductsJson}}">
        <input id="letter-subject" type="hidden" name="subject">
        <input id="letter-content" type="hidden" name="content"> 
        
      {{ Form::close() }}
      <div>
        <h3>Subject</h3>
        <div id="letter-subject-preview"></div>
        <h3>Body</h3>
        <div id="letter-body-preview"></div>
      </div>
      <hr>
      <div>
        <button id="mark-as-sent" type="button" class="btn btn-primary">Mark as Sent</button>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="letter-display-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">To: <span>{{$redeem->email}}</span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h3>Subject: </h3>
          <p id="modal-subject"></p>
          <hr>
          <h3>Body: </h3>
          <div id="modal-letter-body"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</section>


<script id="productsData" type="application/json">
{!! $eligibleProducts->toJson() !!}
</script>

<script id="letterTemplateData" type="application/json">
{!! $letterTemplates !!}
</script>

<script id="lettersData" type="application/json">
{!! $redeem->inviteReviewLetters->toJson() !!}
</script>

<script>
  (function(){
    const products = JSON.parse(productsData.text);
    const letterTemplates = JSON.parse(letterTemplateData.text);
    const letters = JSON.parse(lettersData.text);
    const letterTemplateSelect = $('#letter-template');
    const letterSubjectPreviewDiv = $('#letter-subject-preview');
    const letterBodyPreviewDiv = $('#letter-body-preview');
    const customerName = $('#user-name').text();
    const productName = $('#product-name').text();
    const orderNumber = $('#order-number').text();
    const userEmail = $('#user-email').text();
    const bonusEle = $('#bonus');
    const senderNameEle = $('#sender-name');
    const letterSubject = $('#letter-subject');
    const letterContent = $('#letter-content');
    const letterForm = $('#letter-from');
        
    $('#letter-template, #bonus, #sender-name').on('change', () => {
      const templateId = letterTemplateSelect.val();
      if (templateId) {
        const templateSelected = letterTemplates.find((ele)=> ele.id == letterTemplateSelect.val());
        const body = fillPlaceholders(templateSelected.body);
        const subject = fillPlaceholders(templateSelected.subject);
        letterSubjectPreviewDiv.html(subject);
        letterBodyPreviewDiv.html(body);
      } else {
        letterSubjectPreviewDiv.html('');
        letterBodyPreviewDiv.html('');
      }
    });

    $('#mail').on('click', ()=> {
      if (letterTemplateSelect.val()) {
        const mail = document.createElement("a");
        mail.target = "_blank";
        mail.href = `mailto:${userEmail}?subject=${letterSubjectPreviewDiv.text()}`;
        mail.click();
      } else {
        alert('Please select a template');
      }
    });

    $('#mark-as-sent').on('click', ()=> {
      if (letterTemplateSelect.val()) {
        letterSubject.val(letterSubjectPreviewDiv.html());
        letterContent.val(letterBodyPreviewDiv.html());
        letterForm.submit();
      } else {
        alert('Please select a template');
      }
    });

    $('.modal-opener').on('click', function(){
      const letterId = $(this).data('letterId');
      const letter = letters.find((item)=>item.id == letterId);
      console.log(letter);
      $('#modal-subject').text(letter.subject);
      $('#modal-letter-body').html(letter.content);
      $('#letter-display-modal').modal();
    });

    addOptionsToLetterTemplateSelect();

    function addOptionsToLetterTemplateSelect(){
      for(const template of letterTemplates) {
        let ele = $(`<option value="${template.id}">${template.name}</option>`);
        letterTemplateSelect.append(ele);
      }
    }

    function fillPlaceholders(text) {
      const productList = generateProductList(products);
      const senderName = senderNameEle.val();
      const bonus = bonusEle.val();
      let newText = text.replace('[[products]]', productList[0].outerHTML);
      newText = newText.replace('[[customer_name]]', customerName);
      newText = newText.replace('[[sender_name]]', senderName);
      newText = newText.replace('[[bonus]]', bonus);
      newText = newText.replace('[[product_name]]', productName);
      newText = newText.replace('[[order_number]]', orderNumber);
      return newText;
    }

    function generateProductList(products) {
      let ul = $('<ol></ol>');
      for(const product of products) {
        let li = $('<li></li>');
        let title = $(`<p><a href="${product.amzLink}" target="_blank">${product.short_name}</a></p>`);
        let img = $(`<div><a href="${product.amzLink}" target="_blank"><img src="${product.medium_image}" alt="${product.short_name}"/></a></div>`);
        li.append(title);
        li.append(img);
        ul.append(li);
      }
      return ul;
    }
  })()
  
</script>
@endsection