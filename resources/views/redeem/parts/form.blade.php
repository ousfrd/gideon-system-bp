<!-- Modal -->
<div class="modal fade" id="newRedeemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Create New {{$redeemType}} Redeem</h4>
      </div>
      <div class="modal-body">
        {{Form::open(['url'=> route('redeems.store')])}}
          <input type="hidden" name="how_to_help" value="{{$redeemType}}">
          @if ($redeemType == "Same Free Product")
            <input type="hidden" name="redirect_to" value="{{route('redeems.freeProduct')}}">
          @endif
          @if ($redeemType == "Amazon Gift Card")
            <input type="hidden" name="redirect_to" value="{{route('redeems.giftcard')}}">
          @endif
          @if ($redeemType == "Other Redeems")
            <input type="hidden" name="redirect_to" value="{{route('redeems.other')}}">
          @endif
          @if ($redeemType == "INVITE_REVIEW_REDEEM")
            <input type="hidden" name="is_follow_up" value="1">
            <input type="hidden" name="redirect_to" value="{{route('redeems.list-invite-reviews')}}">
          @endif
          <div class="form-group">
            <label for="">Amazon Order Id *</label>
            <input required id="amazon-order-id" pattern="\d{3}-\d{7}-\d{7}\s*" title="amazon order id should in the format of 123-1234567-1234567" type="text" class="form-control" name="AmazonOrderId">
            <p class="text-danger hidden" id="order-not-found-text">Order Not Found</p>
            <p class="text-danger hidden" id="order-redeemed-text">Order Has Been Redeemed</p>
          </div>
          <div id="loading" class="hidden">
            <i class="fa fa-circle-o-notch fa-spin" style="font-size:45px"></i>
          </div>
          <div id="more-info" class="hidden">
            <div class="form-group">
              <label for="">Select Order Item *</label>
              <div id="order-items"></div>
            </div>
            @if ($redeemType == "INVITE_REVIEW_REDEEM")
            <div class="form-group">
              <label for="">Source *</label>
              <select required name="source" id="" class="form-control">
                <option value="">--select--</option>
                  @foreach (App\Redeem::INVITE_REVIEW_SOURCES as $val=>$text)
                    <option value="{{$val}}">{{$text}}</option>
                  @endforeach
              </select>
            </div>
            @endif
            <div class="form-group">
              <label for="">Name *</label>
              <input required type="text" class="form-control" name="name">
            </div>
            <div class="form-group">
              <label for="">Email</label>
              <input required type="email" class="form-control" name="email">
            </div>
            @if ($redeemType == "INVITE_REVIEW_REDEEM") 
              <div class="form-group">
                <label for="">Wechat Account</label>
                <input required type="text" class="form-control" name="customer_wechat_account">
              </div>
              <div class="form-group">
                <label for="">Facebook Account</label>
                <input required type="text" class="form-control" name="customer_facebook_account">
              </div>
            @endif
            <div class="form-group">
              <label for="">Star *</label>
              @if ($redeemType == "Amazon Gift Card" || $redeemType == "Same Free Product" || $redeemType == "INVITE_REVIEW_REDEEM")
                <input required type="number" min="1" max="5" class="form-control" name="star" value="5">
              @else
                <input required type="number" min="1" max="5" class="form-control" name="star" value="3">
              @endif
            </div>
            <div class="form-group">
              <label for="">Request Date *</label>
              <input required type="text" class="form-control" name="requestDate"/>
            </div>
            <div class="form-group">
              <label for="">Note</label>
              <textarea name="note" id="" cols="30" rows="5" class="form-control"></textarea>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Create</button>
            </div>
          </div>
        {{Form::close() }}
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  function formInit() {
    resetForm($('form'));
    $('#order-not-found-text').addClass('hidden');
    $('#order-redeemed-text').addClass('hidden');
    $('#loading').addClass('hidden');
    $('#more-info').addClass('hidden');
    $('#order-items').html('');
  }

  function resetForm($form) {
    $form.find('input:text:not(#amazon-order-id), input:password, input:file, select, textarea').val('');
    $form.find('input:radio, input:checkbox')
         .removeAttr('checked').removeAttr('selected');
  }

  function getItemsOfAmazonOrder(amazonOrderId) {
    const url = `/orders/${amazonOrderId}/order-items.json`;
    return $.get(url);
  }

  function checkRedeemExists(amazonOrderId) {
    const url = `/redeems/${amazonOrderId}/check-exists`;
    return $.get(url)
  }

  function addOrderItems(orderItems) {
    let html = '';
    for(const orderItem of orderItems) {
      const value = orderItem.ASIN;
      const text = `${orderItem.ASIN} - ${orderItem.Title}`;
      html += getOrderItemRadio(value, text);
    }
    $('#order-items').html(html);
  }

  function getOrderItemRadio(value, text) {
    const template = `<div class="radio">
                <label>
                  <input required type="radio" name="asin" value="${value}">${text}
                </label>
              </div>`;
    return template;
  }

  $('#amazon-order-id').on('change', function(e) {
    formInit();
    $('#loading').removeClass('hidden');
    if (this.checkValidity()) {
      const amazonOrderId = ($(this).val()).trim();
      checkRedeemExists(amazonOrderId).done(function(exists){
        console.log(exists);
        
        if (exists == 1) {
          $('#loading').addClass('hidden');
          $('#order-redeemed-text').removeClass('hidden');
        } else {
          $('#order-redeemed-text').addClass('hidden');
          getItemsOfAmazonOrder(amazonOrderId).done(function(data){
            $('#loading').addClass('hidden');
            if (data.length > 0) {
              $('#order-not-found-text').addClass('hidden');
              addOrderItems(data);
              $('#more-info').removeClass('hidden');
              $('input[name="requestDate"]').val(moment().format('YYYY-MM-DD'));
            } else {
              $('#order-not-found-text').removeClass('hidden');
            }
          }).fail(function(e){
            console.log(e);
          });
        }
      }).fail(function(e){
        console.log(e);
      })
      
    } else {
      resetForm($('form'));
      $('#more-info').addClass('hidden');
    }
  })


  $('#newRedeemModal').on('hidden.bs.modal', function (e) {
    formInit();
    $('#amazon-order-id').val('');
  });

  $('input[name="requestDate"]').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      drops: "up",
      maxDate: moment(),
      startDate: moment(),
      locale: {
        format: "YYYY-MM-DD"
      },
    }, function(start, end, label) {
      this.element.val(start.format('YYYY-MM-DD'));
    }
  );

  var $inputs = $('input[name=email],input[name=customer_wechat_account],input[name=customer_facebook_account]');
  $inputs.on('input', function () {
      // Set the required property of the other input to false if this input is not empty.
      $inputs.not(this).prop('required', !$(this).val().length);
  });
})
</script>