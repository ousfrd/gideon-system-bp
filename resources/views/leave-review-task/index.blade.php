@extends('layouts.master')
@section('title', "Leave Review Tasks")
@section('content')
<div class="btn-actions">
  <a href="{{route('leave-review-task.create')}}" class="btn btn-primary pull-right"><span class="" >Create Tasks</span></a>
</div>
{!! $grid !!}

@include('parts.delete-confirmation')

@endsection