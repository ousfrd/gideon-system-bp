@extends('layouts.master')
@section('title', "New Task")
@section('content')
<div class="row">
	 <div class="col-md-4 mx-auto">
     {{ Form::model($leaveReviewTask, ['action' => 'LeaveReviewTaskController@store']) }}
      <div class="form-group">
        {!! Form::label('name', 'Name *') !!}
        {!! Form::text('name', '', ['class' => 'form-control', 'required'=>true]) !!}
      </div>
      <div class="form-group">
        {!! Form::label('asin', 'ASIN *') !!}
        {!! Form::text('asin', '', ['class' => 'form-control', 'required'=>true]) !!}
      </div>
      <div class="form-group">
        {!! Form::label('total_review_goal', 'Total Review Goal *') !!}
        {!! Form::number('total_review_goal', '', ['class' => 'form-control', 'required'=>true]) !!}
      </div>
      <div class="form-group">
        {!! Form::label('owned_buyer_review_goal', 'Owned Buyer(自建buyer) Review Goal') !!}
        {!! Form::number('owned_buyer_review_goal', '', ['class' => 'form-control']) !!}
      </div>

      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Self Review Staff Name</th>
            <th>Goal</th>
          </tr>
        </thead>
        <tbody>
        @foreach($selfReviewStaffs as $name=>$id)
          <tr>
            <td>{{$name}}</td>
            <td>
              {!! Form::number('owned_buyer_staff_goal_'.$id, '', ['class' => 'form-control']) !!}           
            </td>
          </tr>
        @endforeach
        </tbody>
      </table>
      <div class="form-group">
        {!! Form::label('invite_review_goal', 'Invite Review(测评) Goal') !!}
        {!! Form::number('invite_review_goal', '', ['class' => 'form-control']) !!}
      </div>
      <div class="form-group">
        {!! Form::label('deadline', 'Due Date *') !!}
        {!! Form::text('deadline', '', ['class' => 'form-control date', 'required' => true]) !!}
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    {{ Form::close() }}
  </div>
</div>
<script>
  $(document).ready(function() {

    $('input[name="deadline"]').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        format: "YYYY-MM-DD"
      },
    }, function(start, end, label) {
    }
    );
  
});
</script>
@endsection