@extends('layouts.master')
@section('title', "Leave Review Task " . $leaveReviewTask->name)
@section('content')
<div class="panel panel-default">
  <div class="panel-heading">
    Info
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-xs-12 col-md-6">
        <table class="table table-striped">
          <tr>
            <td>Name</td>
            <td>{{$leaveReviewTask -> name }}</td>
          </tr>
          <tr>
            <td>ASIN</td>
            <td>{{$leaveReviewTask -> asin }}</td>
          </tr>
          <tr>
            <td>Total Review Goal</td>
            <td>{{$leaveReviewTask -> total_review_goal }}</td>
          </tr>
          <tr>
            <td>Owned Buyer Review Goal</td>
            <td>{{$leaveReviewTask -> owned_buyer_review_goal }}</td>
          </tr>
          <tr>
            <td>Invite Review Goal</td>
            <td>{{$leaveReviewTask -> invite_review_goal }}</td>
          </tr>
          <tr>
            <td>Owned Buyer Review Count</td>
            <td>{{$leaveReviewTask -> owned_buyer_approved_reviews_count }}</td>
          </tr>
          <tr>
            <td>Invite Review Count</td>
            <td>{{$leaveReviewTask -> invite_review_approved_reviews_count }}</td>
          </tr>
          <tr>
            <td>Deadline</td>
            <td>{{$leaveReviewTask -> deadline }}</td>
          </tr>
          <tr>
            <td>Created By</td>
            <td>{{$leaveReviewTask -> created_by }}</td>
          </tr>
          <tr>
            <td>Created At</td>
            <td>{{$leaveReviewTask -> created_at }}</td>
          </tr>
        </table>
      </div>
      <div class="col-xs-12 col-md-6">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Self Review Staff</th>
              <th>Goal</th>
              <th>Progress</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($subTasks as $subTask)
            <tr>
              <td>{{$selfReviewStaffs[$subTask->user_id]}}</td>
              <td>{{$subTask->goal }}</td>
              <td>{{$subTask->progress }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    Order Reviews
  </div>
  <div class="panel-body">
    {!! $grid !!}
  </div>
</div>
@endsection