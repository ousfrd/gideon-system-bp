<?php
/** @var Nayjest\Grids\Filter $filter */
?>

<input
	type="date"
    class="form-control input-sm"
    name="<?= $filter->getInputName() ?>"
    value="<?= $filter->getValue() ?>"
    />
<?php if($label): ?>
    <span><?= $label ?></span>
<?php endif ?>
