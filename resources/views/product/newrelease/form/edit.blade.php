@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Create New Release</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('products.createnewreleases.save') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
            
                            <div class="form-group">
                                
                                    <label for="website" class="col-md-2 control-label">Website<br/>(Ctrl + Click)</label>
                                        
                                    <div class="col-md-6">
                                        <select name="website[]" required class="form-control" multiple>
                                            @foreach($giftcard_websites as $giftcard_website)
                                            <option value="{{$giftcard_website}}">{{$giftcard_website}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                            </div>
<!--
                            <div class="form-group{{ $errors->has('product_id') ? ' has-error' : '' }}">
                                <label for="product_id" class="col-md-4 control-label">Product ID</label>

                                <div class="col-md-6">
                                    <input id="product_id" type="text" class="form-control" name="product_id" >
                                </div>
                            </div>
-->               
                            <div class="form-group{{ $errors->has('product_id') ? ' has-error' : '' }}">
                                <label for="product_id" class="col-md-2 control-label">Product ID</label>

                                <div class="col-md-6">
                                    <input id="product_id" type="text" class="form-control" name="product_id" >
                                </div>
                            </div>  

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title" class="col-md-2 control-label">Title</label>

                                <div class="col-md-10">
                                    <input id="title" type="text" class="form-control" name="title" >
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                <label for="image" class="col-md-2 control-label">Image URL</label>

                                <div class="col-md-10">
                                    <input id="image" type="text" class="form-control" name="image" >
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
                                <label for="link" class="col-md-2 control-label">Product Link</label>

                                <div class="col-md-10">
                                    <input id="link" type="text" class="form-control" name="link" >
                                </div>
                            </div>     
                            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                <label for="price" class="col-md-2 control-label">Price (USD)</label>

                                <div class="col-md-6">
                                    <input id="price" type="text" class="form-control" name="price" >
                                </div>
                            </div>                                                   


                            <div class="form-group">
                                <div class="col-md-12 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">
                                        Import
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection