@extends('layouts.master')
@section('title', $title)
@section('content')
<div class="nayjest-grid">
{!! $grid !!}
</div>

<script type="text/javascript">

$(document).ready(function(){
//edit form style - popup or inline
//$.fn.editable.defaults.mode = 'inline';


$('.column-status').each(function(){
	if($(this).find('.low-stock').length>0){
		$(this).parents('tr').addClass('bg-warning')
	}
	
	
})

$('.editable').editable();



$('.pUpdate').editable({
    validate: function(value) {
        if($.trim(value) == '') {
            return 'Value is required.';
        }
	},
	//showbuttons:false,
	//onblur:'submit',
	savenochange:false,
    type: 'text',
    url:'{{route('product.ajaxsave')}}',  
    //title: 'Enter value',
    placement: 'top', 
    send:'auto',
    ajaxOptions: {
   		 dataType: 'json'
    },
    success: function(response, newValue) {
    } 
	})

	$('.pUpdate.unit-cost').on('shown', function(e, editable) {
		var checkbox = $('<div class="input form-group checkbox-success checkbox" style="margin-top: 5px;"><input name="isUnitUpdateOrders" value="1" class="UpdateALLOrders" id="isunitupdateorders" type="checkbox"><label for="isunitupdateorders">Update ALL Orders</label></div>');
		
	    editable.input.$input.parents('form').find('.control-group.form-group').append(checkbox);
	})
	
	$('.pUpdate.unit-cost').editable('option', 'params', function (params) {
		
	    var $isUnitUpdateOrders = $('#isunitupdateorders');
	
	    if($isUnitUpdateOrders.is(':checked')) {
	        params.isUnitUpdateOrders = 1;
	    }
	
	    return params;
	});


var productStatus = ['Inactive', 'Active', 'Blocked']
$('.product-status').each(function(){
	$(this).editable({
        inputclass: 'input-large',
        type: 'select',
        url:'{{route('product.ajaxsave')}}',
        send:'auto',
        width: '250px',
		source: productStatus
	})
});

var dataSource = {!!json_encode($managerSources)!!}
var sourceArray = {}
var tags = []
$.each(dataSource,function(id,t){
	sourceArray[parseInt(t.id)] = t.text
	tags.push(t.text)
})
console.log(tags)
$('.pmanagers').each(function(){
	$(this).editable({
        inputclass: 'input-large',
        type: 'select',
        url:'{{route('product.ajaxsave')}}',
        send:'auto',
        width: '250px',
        source: tags
    })
}); 

var dataSource = {!!json_encode($categories)!!}
var sourceArray = {}
var tags = []
tags.push('None')
$.each(dataSource,function(id,t){
	sourceArray[parseInt(t.id)] = t.category
	tags.push(t.category)
})

	$('.pCategories').each(function(){
		$(this).editable({
			inputclass: 'input-large',
			type: 'select',
			url:'{{route('product.ajaxsave')}}',
			send:'auto',
			width: '250px',
			source: tags
		
		})
	});
	console.log(tags)
})

</script>
@endsection