<div class="mb20"></div>
<div class="row">
<div class="col-sm-10"><h4>Inbound Shipments</h4></div>
<div class="col-sm-2"><a class="pull-right btn btn-default"  data-toggle="modal" data-target="#inboundshipmentModal" >Create Inbound Shipment Plan</a>
</div>
</div>



 <table class="table table-striped" style="table-layout: fixed;"><tbody>
 <tr>
 <th>SKU</th>
 <th>Updated Date</th>
 <th>Shipment ID</th><th>Shipment Name</th><th>Shipment Status</th>
 <th>Label Prep Type</th><th>Qty Shipped</th>
 <th>Qty In Case</th>
 <th>Qty Received</th>
 <th>Receiving Qty</th>
 <th>Unit Cost</th>
 <th>Shipping Cost</th>
 </tr>
 @foreach($product->inbound_shipments() as $shipment)
 <tr>
 <td>{{$shipment->seller_sku}}</td>
 <td>{{$shipment->date_updated}}</td>
 <td>{{$shipment->shipment_id}}</td>
 <td>{{$shipment->shipment_name}}</td>
 <td>{{$shipment->shipment_status}}</td>
 <td>{{$shipment->label_prep_type}}</td>
 <td>{{$shipment->quantity_shipped}}</td>
 <td>{{$shipment->quantity_in_case}}</td>
 <td>{{$shipment->quantity_received}}</td>
 <td>{{$shipment->shipment_status != 'CLOSED'?$shipment->quantity_shipped-$shipment->quantity_received:0}}</td>
 <td>{!!'<a  class="sUpdate"  data-pk="unit_cost-' . $shipment->id . '"  data-value="' . $shipment->unit_cost . '">' . $shipment->unit_cost . '</a>'!!}</td>
 <td>{!!'<a  class="sUpdate"  data-pk="shipping_cost-' . $shipment->id . '"  data-value="' . $shipment->shipping_cost . '">' . $shipment->shipping_cost. '</a>'!!}</td>
 
 </tr>
 @endforeach
 </tbody></table>
 
 
 


<script>
 	 
  		$('#inboundshipmentForm').submit(function(){
			$(this).attr('disabled',true)
			$('.modal-spinner').show()
			$.post('{{route('inbound_shipment.create_plan')}}',$('#inboundshipmentForm').serialize(),function(){
				window.location.reload()
			})

			return false
  	  	})
  	  	
  	  	
  	  	$('.sUpdate').editable({
		    validate: function(value) {
		        if($.trim(value) == '') {
		            return 'Value is required.';
		        }
			},
			//showbuttons:false,
			//onblur:'submit',
			savenochange:false,
		    type: 'text',
		    url:'{{route('shipment.ajaxsave')}}',  
		    //title: 'Enter value',
		    placement: 'top', 
		    send:'auto',
		    ajaxOptions: {
		   		 dataType: 'json'
		    },
		    success: function(response, newValue) {
		    } 
			})
  </script>
 