<div class="mb20"></div>
<div class="row">
	<div class="col-sm-10"><h4>Product Review</h4></div>
	<div class="col-sm-2">
		<a class="btn btn-default" href="{{ route('reviews', [$asin]) }}" target="_blank">Review List </a>
	</div>
	<div class="col-sm-2">
		<!-- <a href="http://www.google.com" class="button" data-info="The worlds most popular search engine">Google</a>
		<button id="categoryUpdate1" data-size="small" type="button" data-url="route('review.updateReviewCategory')">Update Review Category</button> -->
		<button id="categoryUpdate" type="button" class="btn btn-default" >Update Review Category</button>
	</div>
</div>
<div class="row">
	 <div style="width:100%;height:450px;margin:0 auto">
        <div id="review" style="width: 100%;height:100%" ></div>
    </div>
	<div style="width:100%;height:450px;margin:0 auto">
        <div id="review_pie" style="width: 100%;height:100%" ></div>
    </div>
</div>

<?php
	$columns = [];
	$xline1 = ['x1'];
	$xline2 = ['x2'];
	$json_data = [];
	$review_total = ['review_total'];
	$sales_rank = ['sales_rank'];
	$rating[] = ['rating'];
	$review_date_num_info = DB::table('reviews')
			 ->select('review_date', DB::raw('count(*) as total'))
			 ->where('review_date', '<', $end_date)
			 ->where('asin', $asin)
             ->groupBy('review_date')
			 ->get();

	$stimestamp = strtotime($start_date);
	$etimestamp = strtotime($end_date);

	// 计算日期段内有多少天
	$days = ($etimestamp-$stimestamp)/86400+1;

	// 保存每天日期
	$date = array();

	for($i=0; $i<$days; $i++){

		$review_total_date = date('Y-m-d', $stimestamp+(86400*$i));
		$date[] = $review_total_date;
		$review_total_num = 0;
		foreach($review_date_num_info as $review) {
		
			$new_json_data = [];
			if($review->review_date <= $review_total_date) {
				$review_total_num += $review->total;
			}
		}
		$xline1[] = $review_total_date;
		$new_json_data['review_total'] = array(
			'date' => date('Y-m-d',strtotime($review_total_date) ),
			'reviews' => $review_total_num
		);
		$review_total[] = $review_total_num;
		$json_data[] =  $new_json_data;

		$sales_rank_info = DB::table('sales_ranks')
			->where('asin', $asin)
			->where('date', $review_total_date)
			->get();
		if (!$sales_rank_info->isEmpty()) {
			$xline2[] = $review_total_date;
			$sales_rank[] = $sales_rank_info[0]->sales_ranks;
			$rating[] = $sales_rank_info[0]->rating;
		}
	}
?>


<script type="text/javascript">

	$('.button').click(function(e) {
		e.preventDefault();
		thisdata = $(this).attr('data-info');
		console.log(thisdata);
	});

	$.ajaxSetup({

		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}

	});

	$(document).ready(function(){
		$('#categoryUpdate').click(function(){
			$.post('http://homestead.test/updateReviewCategory', {'asin': '<?php echo $asin;?>'})
		})
	});

	var columns = [];

	columns.push(<?php echo json_encode($xline1); ?>, 
				 <?php echo json_encode($xline2); ?>,
				 <?php echo json_encode($review_total); ?>, 
				 <?php echo json_encode($rating); ?>, 
				 <?php echo json_encode($sales_rank); ?>);

	var chart = c3.generate({
	    bindto: '#review',
	    data: {
	      	xs: {
				  'review_total':'x1',
				  'sales_rank':'x2',
				  'rating':'x2',
			  },
    		xFormat: '%Y-%m-%d',
			columns: columns,
			axes: {
				sales_rank: 'y2',
				rating: 'y'
			},
			// types: {
			// 	sales_rank: 'bar' // ADD
			// }
	      	type: 'spline'
	    },
	    axis: {
	        x: {
	            type: 'timeseries',
	        	tick: {
	                format: '%b %d,%Y'
	            }
	        },
	        y: {
				// padding: {bottom:0}
				label: { // ADD
					text: 'Y Label',
					position: 'outer-middle'
				},
			y2: {
				show: true,
				label: {
					text: 'Y2 Label',
					position: 'outer-middle'
				}
      }
	        }
	    },

	});

	var chart = c3.generate({
	bindto: '#review_pie',
    data: {
        columns: [
			['claim_count', {{ $categorizedReviewQuantity['CLAIM_REVIEW'] }} ],
			['self_count', {{ $categorizedReviewQuantity['SELF_REVIEW'] }} ],
			['invite_count', {{ $categorizedReviewQuantity['INVITE_REVIEW'] }} ],
			['organic_count', {{ $categorizedReviewQuantity['OTHER_REVIEW'] }} ]
        ],
        type: 'pie'
    },
    pie: {
        label: {
            format: function (value, ratio, id) {
                return d3.format()(value);
            }
        }
    }
});

</script>
<style>

</style>