<div class="mb20"></div>
<h4>Product Basic Information</h4>
<div class="row">
        <div class="col-xs-6  col-sm-4  ">
          <table class="table table-striped" style="table-layout: fixed;">
            <tbody>
              <tr>
                <th>ASIN</th>
                <td class="text-right"><a target="_blank" href="{{$product->amazonLink()}}">{{$product->asin}}</a></td>
              </tr>
                      
              <tr>
                <th>Status</th>
                <td class="text-right">
                <?php $productStatus = [
                  0 => 'Inactive',
                  1 => 'Active',
                  2 => 'Blocked'
                ]?>
                <a href="#" @if(auth()->user()->can('manage-product-manager')) data-type="select" class="product-status"  data-pk="productStatus-{{$product->id}}" data-title="" @endif>{{$productStatus[$product->status]}}</a></td>
              </tr>

              <tr>
                <th>Title</th>
                <td class="text-right">{{$product->name}}</td>
              </tr>

              <tr>
                <th>Image</th>
                @if($product->small_image)
                <td class="text-right"><img src="{{$product->small_image}}" height="80" /></td>
                @else
                <td class="text-right">

                  <div id="imageBox"></div>

                </td>
                @endif
              </tr>  

              <tr>
                <th>Alias</th>
                <td class="text-right"><a  class="pUpdate alias"  data-pk="alias-{{$product->id}}"  data-value="{{$product->alias}}">{{$product->alias}}</a></td>
              </tr>

              <tr>
                <th>Brand</th>
                <td class="text-right">{{$product->brand}}</td>
              </tr>

              <tr>
                <th>Weight</th>
                <td class="text-right">{{$product->weight}} lb</td>
              </tr>
              
              <tr>
                <th>Size</th>
                <td class="text-right">{{$product->size()}}</td>
              </tr>

              <tr>
                <th>Sales Rank</th>
                <td class="text-right">{{$product->sales_rank ? number_format($product->sales_rank) : 'NA' }}</td>
              </tr>

              <tr>
                <th>Reviews</th>
                <td class="text-right">{{$product->review_quantity ? number_format($product->review_quantity) : 'NA' }}</td>
              </tr>

              <tr>
                <th>Rating</th>
                <td class="text-right">{{$product->review_rating ? number_format($product->review_rating, 1) : 'NA' }}</td>
              </tr>
              
               <tr>
                <th>Peak Season Mode</th>
                <td class="text-right"><input value="1" data-size="small" data-pk="peak_season_mode-{{$product->id}}" data-url="{{route('product.ajaxsave')}}" @if($product->peak_season_mode) checked @endif type="checkbox" data-toggle="toggle"  /></td>
              </tr>

             <tr>
                <th>Is Newrelease?</th>
                <td class="text-right"><input value="1" data-size="small" data-pk="is_newrelease-{{$product->id}}" data-url="{{route('product.ajaxsave')}}" @if($product->is_newrelease) checked @endif type="checkbox" data-toggle="toggle"  /></td>
               </tr>

           	 <tr>
                <th>Discontinued</th>
                <td class="text-right"><input value="1" data-size="small" data-pk="discontinued-{{$product->id}}" data-url="{{route('product.ajaxsave')}}" @if($product->discontinued) checked @endif type="checkbox" data-toggle="toggle"  /></td>
               </tr>
              
                           
              <tr>
                <th>Managers</th>
                <td class="text-right">
                
                <a href="#" @if(auth()->user()->can('manage-product-manager')) data-type="select" class="pmanagers"  data-pk="manager-{{$product->id}}" data-title="" @endif>{{implode(', ', $managers)}}</a></td>
              </tr>
        </tbody>
        </table>
       </div>
       
       <div class="col-xs-6  col-sm-4  ">
          <table class="table table-striped" style="table-layout: fixed;">
            <tbody>
            <tr>
                <th>AI Ads Open?</th>
                @can('manage-ads')
                  <td class="text-right">
                    <input value="1" 
                          data-size="small" 
                          type="checkbox" 
                          data-toggle="toggle"
                          id="ai_ads_open"
                          data-pk="{{$product->id}}"
                          data-url="{{route('product.update-field', ['id'=> $product->id])}}" 
                          {{ $product->ai_ads_open ? 'checked' : ''}}
                          data-on="Yes" 
                          data-off="No" />
                  </td>
                @else
                  <td>{{$product->ai_ads_open ? "Yes" : "No"}}</td>
                @endcan
              </tr>

              <tr>
                <th>AI Ads Commission Rate(%) <br> 
                  (Based On Ads Sales)
                </th>
                <td class="text-right">
                  @can('manage-ads')
                  <a data-type="text" class="editable" 
                    data-pk="{{$product->id}}" 
                    data-url="{{route('product.update-field', ['id'=> $product->id])}}"
                    data-title="AI Ads Commission Rate (%)"
                    id="ai_ads_commission_rate"
                    >{{$product->ai_ads_commission_rate}}</a>
                  @else
                    {{$product->ai_ads_commission_rate}}
                  @endcan
                </td>
              </tr>

              <tr>
                <th>AI Ads Commission</th>
                <td class="text-right">
                  ${{ $adsSummary ?  number_format(0.01 * $product->ai_ads_commission_rate * $adsSummary->ads_sales, 2) : "NA"}}
                </td>
              </tr>
             
              <tr>
                <th>Daily Average Orders</th>
                <td class="text-right">{{number_format($product->dailyAverageOrders(), 2)}}</td>
              </tr>

              <tr>
                <th>Earliest Listing Open Date</th>
                <td class="text-right">{{$product->earliest_listing_open_date}}</td>
              </tr>

              <tr>
                <th>First Order Date</th>
                <td class="text-right">{{$product->first_order_date}}</td>
              </tr>
              
              <tr>
                <th>ManufactureDays</th>
                <td class="text-right">
                Normal: <a  class="pUpdate" data-pk="manufacture_days-{{$product->id}}"  data-value="{{$product->manufacture_days}}">{{$product->manufacture_days}}</a> days<br>
                Peak Season: <a  class="pUpdate" data-pk="ps_manufacture_days-{{$product->id}}"  data-value="{{$product->ps_manufacture_days}}">{{$product->ps_manufacture_days}}</a> days</td>
              </tr>
              
              <tr>
                <th>ToAMZShippingDays</th>
                <td class="text-right">
                Normal: <a  class="pUpdate" data-pk="cn_to_amz_shipping_days-{{$product->id}}"  data-value="{{$product->cn_to_amz_shipping_days}}">{{$product->cn_to_amz_shipping_days}}</a> days<br>
                Peak Season: <a  class="pUpdate" data-pk="ps_cn_to_amz_shipping_days-{{$product->id}}"  data-value="{{$product->ps_cn_to_amz_shipping_days}}">{{$product->ps_cn_to_amz_shipping_days}}</a> days
                </td>
              </tr>
              
              <tr>
                <th>Cost Per Unit</th>
                <td class="text-right"> {{$product->current_product_cost}}
                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#productCostModal">
                    <i class="fa fa-edit"></i>
                  </button>
                </td>
              </tr>
              
              <tr>
                <th>Shipping Cost Per Unit</th>
                <td class="text-right">
                  {{$product->current_shipping_cost}}
                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#shippingCostModal">
                    <i class="fa fa-edit"></i>
                  </button>
                </td>
              </tr>

              <tr>
                <th>FBM Shipping Cost Per Unit</th>
                <td class="text-right">
                  {{$product->current_fbm_shipping_cost}}
                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#fbmShippingCostModal">
                    <i class="fa fa-edit"></i>
                  </button>
                </td>
              </tr>

              <tr>
                <th>Cliam Review Cost / Order</th>
                <td class="text-right">
                @can('manage-claim-review')
                  <a data-type="number" class="editable" 
                    data-pk="{{$product->id}}" 
                    data-url="{{route('product.update-field', ['id'=> $product->id])}}"
                    data-title="Cliam Review Cost per Order ($)"
                    id="claim_review_order_cost"
                    >{{$product->claim_review_order_cost}}</a>
                @else
                  {{$product->claim_review_order_cost}}
                @endcan
                </td>
              </tr>

              <tr>
                <th>Invite Review Cost / Order</th>
                <td class="text-right">
                @can('invite-review')
                  <a data-type="number" class="editable" 
                    data-pk="{{$product->id}}" 
                    data-url="{{route('product.update-field', ['id'=> $product->id])}}"
                    data-title="Invite Review Cost per Order ($)"
                    id="invite_review_order_cost"
                    >{{$product->invite_review_order_cost}}</a>
                @else
                  {{$product->invite_review_order_cost}}
                @endcan
                </td>
              </tr>

              <tr>
                <th>Self Review Cost / Order</th>
                <td class="text-right">
                  @can('self-review')
                  <a data-type="number" class="editable" 
                    data-pk="{{$product->id}}" 
                    data-url="{{route('product.update-field', ['id'=> $product->id])}}"
                    data-title="Self Review Cost per Order ($)"
                    id="self_review_order_cost"
                    >{{$product->self_review_order_cost}}</a>
                  @else
                    {{$product->self_review_order_cost}}
                  @endcan
                </td>
              </tr>

              <tr>
                <th>Should Invite Review</th>
                <td class="text-right">
                  @can('edit-product-info', $product)
                  <input value="1" 
                          data-size="small" 
                          type="checkbox" 
                          data-toggle="toggle"
                          id="should_invite_review"
                          data-pk="{{$product->id}}"
                          data-url="{{route('product.update-field', ['id'=> $product->id])}}" 
                          {{ $product->should_invite_review ? 'checked' : ''}}
                          data-on="Yes" 
                          data-off="No" />
                  @else
                    {{ $product->should_invite_review ? 'YES' : 'NO'}}
                  @endcan
                </td>
              </tr>
              <tr>
                <th>Should Claim Review</th>
                <td class="text-right">
                  @can('edit-product-info', $product)
                  <input value="1" 
                          data-size="small" 
                          type="checkbox" 
                          data-toggle="toggle"
                          id="should_claim_review"
                          data-pk="should_claim_review-{{$product->id}}"
                          data-url="{{route('product.ajaxsave')}}" 
                          {{ $product->should_claim_review ? 'checked' : ''}}
                          data-on="Yes" 
                          data-off="No" />
                  @else
                    {{ $product->should_claim_review ? 'YES' : 'NO'}}
                  @endcan
                </td>
              </tr>
        </tbody>
        </table>
       </div>
       
       <div class="col-xs-6  col-sm-4  ">
          <table class="table table-striped" style="table-layout: fixed;">
            <tbody>
            </tbody>
          </table>

          <table class="table table-striped" style="table-layout: fixed;">
            <tbody>
            <tr>
                <th>Replenishment Cycle</th>
                <td class="text-right">{{$product->totalDayToReinstock()}} days</td>
              </tr>
               
               <tr>
                <th>Est. Stock Days</th>
                <td class="text-right">{{$product->daysToOutofStock()}} days</td>
              </tr>
               <tr>
                <th>Replenishment Needed</th>
                <td class="text-right">{!! ($product->replenishNeeded() ? '<span class="text-danger alert-qty">Yes</span>':'<span  class="text-success">No</span>') !!}</td>
              </tr>
               <tr>
                <th>Days to Replenish</th>
                <td class="text-right">
                <span class="@if($product->daysToReplenish() > 7) text-success @elif($product->daysToReplenish() > 1) text-warning @else text-danger @endif">
                {{ $product->daysToReplenish()}} days</span>
                
                <br/>
                
                
                </td>
              </tr> 
              <tr>
                <td colspan="2">
                  <hr>
                </td>
              </tr>
             @foreach($qtys as $k=>$v)
              <tr>
                <th>{{$k}}</th>
                <td class="text-right">{{$v}}</td>
              </tr>
              
              @endforeach
			   <tr>
                <th>Safe Stock</th>
                <td class="text-right">{{$product->safeStockQty()}}</td>
              </tr>
              
        </tbody>
        </table>
       </div>
       
      
 </div>
<!-- Product Cost Modal -->
<div class="modal fade" id="productCostModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="">Product Cost Per Unit</h4>
      </div>
      <div class="modal-body">
        <table class="table talbe-striped">
          <thead>
            <tr>
              <th>start date</th>
              <th>end date</th>
              <th>cost</th>
              <th>action</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($product->productCosts as $pCost)
            <tr data-id="{{ $pCost->id }}">
              <td>{{ $pCost->start_date }}</td>
              <td>{{ $pCost->end_date }}</td>
              <td>{{ $pCost->cost_per_unit }}</td>
              <td>
                @if($pCost->future)
                  <button data-url="{{route('product-cost.destroy', ['id' => $pCost->id])}}" class="btn btn-danger delete">delete</button>
                @endif
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        <form class="form-inline" data-url="{{ route('product-cost.store') }}">
          <input type="hidden" name="product_id" value="{{$product->id}}">
          <div class="form-group">
            <label for="">Start Date</label>
            <input name="start_date" type="text" class="form-control singleDate" value="<?= date("Y-m-d"); ?>">
          </div>
          <div class="form-group">
            <label for="">Cost Per Unit</label>
            <input required name="cost" type="number" step="0.01" min="0" class="form-control">
          </div>
          <div class="form-group">
            <label for="">&nbsp;</label>
            <button required type="submit" class="btn btn-primary">Add</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Product Cost Modal -->

<!-- Shipping Cost Modal -->
<div class="modal fade" id="shippingCostModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="">Shipping to Amazon Cost Per Unit</h4>
      </div>
      <div class="modal-body">
        <table class="table talbe-striped">
          <thead>
            <tr>
              <th>start date</th>
              <th>end date</th>
              <th>cost</th>
              <th>action</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($product->productShippingCosts as $pCost)
            <tr data-id="{{ $pCost->id }}">
              <td>{{ $pCost->start_date }}</td>
              <td>{{ $pCost->end_date }}</td>
              <td>{{ $pCost->cost_per_unit }}</td>
              <td>
                @if($pCost->future)
                  <button data-url="{{route('product-shipping-cost.destroy', ['id' => $pCost->id])}}" class="btn btn-danger delete">delete</button>
                @endif
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        <form class="form-inline" data-url="{{ route('product-shipping-cost.store') }}">
          <input type="hidden" name="product_id" value="{{$product->id}}">
          <div class="form-group">
            <label for="">Start Date</label>
            <input name="start_date" type="text" class="form-control singleDate" value="<?= date("Y-m-d"); ?>">
          </div>
          <div class="form-group">
            <label for="">Cost Per Unit</label>
            <input required name="cost" type="number" step="0.01" min="0" class="form-control">
          </div>
          <div class="form-group">
            <label for="">&nbsp;</label>
            <button required type="submit" class="btn btn-primary">Add</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Shipping Cost Modal -->


<!-- FBM Shipping Cost Modal -->
<div class="modal fade" id="fbmShippingCostModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="">FBM Shipping Cost Per Unit</h4>
      </div>
      <div class="modal-body">
        <table class="table talbe-striped">
          <thead>
            <tr>
              <th>start date</th>
              <th>end date</th>
              <th>cost</th>
              <th>action</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($product->productFbmShippingCosts as $pCost)
            <tr data-id="{{ $pCost->id }}">
              <td>{{ $pCost->start_date }}</td>
              <td>{{ $pCost->end_date }}</td>
              <td>{{ $pCost->cost_per_unit }}</td>
              <td>
                @if($pCost->future)
                  <button data-url="{{route('product-fbm-shipping-cost.destroy', ['id' => $pCost->id])}}" class="btn btn-danger delete">delete</button>
                @endif
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        <form class="form-inline" data-url="{{ route('product-fbm-shipping-cost.store') }}">
          <input type="hidden" name="product_id" value="{{$product->id}}">
          <div class="form-group">
            <label for="">Start Date</label>
            <input name="start_date" type="text" class="form-control singleDate" value="<?= date("Y-m-d"); ?>">
          </div>
          <div class="form-group">
            <label for="">Cost Per Unit</label>
            <input required name="cost" type="number" step="0.01" min="0" class="form-control">
          </div>
          <div class="form-group">
            <label for="">&nbsp;</label>
            <button required type="submit" class="btn btn-primary">Add</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!--FBM Shipping Cost Modal -->

 <script>
$(document).ready(function(){
  $('.editable').editable();

	 $('.pUpdate').editable({
	        validate: function(value) {
	            if($.trim(value) == '') {
	                return 'Value is required.';
	            }
	    	  },
	    	  //showbuttons:false,
	    	  //onblur:'submit',
	    	  savenochange:false,
	        type: 'text',
	        url:'{{route('product.ajaxsave')}}',  
	        //title: 'Enter value',
	        placement: 'top', 
	        send:'auto',
	        ajaxOptions: {
	       		 dataType: 'json'
	        },
	        success: function(response, newValue) {
	        } 
	})

	$('.pUpdate.unit-cost').on('shown', function(e, editable) {
		var checkbox = $('<div class="input form-group checkbox-success checkbox" style="margin-top: 5px;"><input name="isUnitUpdateOrders" value="1" class="UpdateALLOrders" id="isunitupdateorders" type="checkbox"><label for="isunitupdateorders">Update ALL Orders</label></div>');
		
	    editable.input.$input.parents('form').find('.control-group.form-group').append(checkbox);
	})
	
	$('.pUpdate.unit-cost').editable('option', 'params', function (params) {
		console.log(params)
	    var $isUnitUpdateOrders = $('#isunitupdateorders');
	
	    if($isUnitUpdateOrders.is(':checked')) {
	        params.isUnitUpdateOrders = 1;
	    }
	
	    return params;
	});
  var productStatus = ['None', 'Inactive', 'Active', 'Blocked']
  $('.product-status').each(function(){
    $(this).editable({
          inputclass: 'input-large',
          type: 'select',
          url:'{{route('product.ajaxsave')}}',
          send:'auto',
          width: '250px',
      source: productStatus
    })
  });

   var dataSource = {!!json_encode($managerSources)!!}
    var sourceArray = {}
    var tags = []
    $.each(dataSource,function(id,t){
      sourceArray[parseInt(t.id)] = t.text
      tags.push(t.text)
     })
   $('.pmanagers').editable({
            inputclass: 'input-large',
            type: 'select',
            url:'{{route('product.ajaxsave')}}',
            send:'auto',
            width: '250px',
            source: tags

      }); 
  
    $('input.singleDate').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      drops: "up",
      // minDate: moment().add(1, 'days'),
      startDate: moment().add(1, 'days'),
      locale: {
        format: "YYYY-MM-DD"
      },
    }, function(start, end, label) {
      this.element.val(start.format('YYYY-MM-DD'));
    });

    $('.modal form').on('submit', function(e) {
      e.preventDefault();
      let url = $(this).data('url');
      $(this).find('button').prop('disabled', true);
      $.ajax({
        type: "POST",
        url: url,
        data: $(this).serialize(), // serializes the form's elements.
        success: (function(data)
        {
          let tr = `
            <tr data-id="${data.id}">
              <td>${ data.start_date }</td>
              <td></td>
              <td>${ data.cost_per_unit }</td>
          `;
          if (data.future) {
            tr += `<td><button data-url="${data.link}" class="btn btn-danger delete">delete</button></td>`
          }
          tr += '</tr>'
          $(this).closest('.modal').find('tbody').append(tr);
          $(this).trigger("reset");
          $(this).find('button').prop('disabled', false);
        }).bind(this)
      });
    });

    $('.modal').on('click', 'button.delete', function(e) {
      let btn = $(e.target);
      btn.prop('disabled', true);
      let url = btn.data('url');
      $.ajax({
        type: "DELETE",
        url: url,
        success: (function() {
          btn.closest('tr').remove();
        })
      })
    })
})
 </script>

@if(empty($product->small_image))
<script>
        let product_id = '<?php echo $product->id; ?>';
        let insertImage_url = '<?php echo route('utils.ajax.insert-product-image'); ?>';
  </script>

<!-- Load our React component. -->
{{ Html::script(mix('js/react_image_upload.js')) }}
@endif