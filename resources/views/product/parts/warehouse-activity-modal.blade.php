<div class="modal fade" id="warehouse-activity-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form name="warehouse-activity-form" class="form-inline">
          <input type="hidden" name="product_id">
          <div class="form-group">
            <label for="">Quantity</label>
            <input required type="number" name="quantity" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Type</label>
            <select required name="activity_type" class="form-control">
              <option value="">-- Select --</option>
              <option value="INBOUND">Inbound</option>
              <option value="OUTBOUND">Outbound</option>
              <option value="LOST">Lost</option>
              <option value="DEFECTIVE">Defective</option>
            </select>
          </div>
          <div class="form-group pull-right">
            <label for="">&nbsp;</label>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  (function(){
    const modalTitle = $('#warehouse-activity-modal .modal-title');
    const productIdEle = $('#warehouse-activity-modal input[name="product_id"]');
    const quantityEle = $('#warehouse-activity-modal input[name="quantity"]');
    const activityTypeEle = $('#warehouse-activity-modal select[name="activity_type"]');

    $('#warehouse-activity-modal').on('show.bs.modal', function (e) {
      const btn = $(e.relatedTarget);
      const title = `${btn.data('type')} : ${btn.data('asin')} - ${btn.data('title')}`;
      modalTitle.text(title);
      activityTypeEle.val(btn.data('type'));
      quantityEle.val(0);
      productIdEle.val(btn.data('productId'));
    });

    $('#warehouse-activity-modal form').on('submit', function(e) {
      e.preventDefault();
      e.stopPropagation();
      const quantity = quantityEle.val();
      const activity_type =  activityTypeEle.val();
      const product_id = productIdEle.val();
      $.ajax({
          url: "{{route('warehouse-activity.store')}}",
          type:"POST",
          data:{
            "_token": "{{ csrf_token() }}",
            quantity: quantity,
            activity_type: activity_type,
            product_id: product_id
          },
          success:function(response){
            $('#warehouse-activity-modal').modal('hide')
            location.reload();
          },
      });
      return false;
    })
  })();
</script>