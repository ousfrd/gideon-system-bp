<div class="mb20"></div>
@if($start_date != $end_date)
@include('product.parts.inventoryhistory')
@endif

<div class="row">
  <div class="col-xs-12 col-md-5">
    <div class="panel panel-info">
      <div class="panel-heading">
        Current Inventory Summary
      </div>
      <div class="panel-body">
        <h4>FBA Inventory</h4>
        <table class="table table-striped" style="table-layout: fixed;">

          <tbody>
            <tr>
              <th>Fulfillment Channel</th>
              <td class="text-right">Amazon</td>
              <th>Fulfillable Qty</th>
              <td class="text-right">{{$qtys['Fulfillable Qty']}}</td>
              
            </tr>
    
            <tr>
              <th>Unsellable Qty</th>
              <td class="text-right">{{$qtys['Unsellable Qty']}}</td>
              <th>Reserved Qty</th>
              <td class="text-right">{{$qtys['Reserved Qty']}}</td>
            </tr>
    
            <tr>
              <th>Warehouse Total Qty</th>
              <td class="text-right">{{$qtys['Warehouse Total Qty']}}</td>
              <th>Inbound Working Qty</th>
              <td class="text-right">{{$qtys['Inbound Working Qty']}}</td>
            </tr>
    
    
            <tr>
              <th>Inbound Shipped Qty</th>
              <td class="text-right">{{$qtys['Inbound Shipped Qty']}}</td>
              <th>Inbound Receiving Qty</th>
              <td class="text-right">{{$qtys['Inbound Receiving Qty']}}</td>
            </tr>

            <tr>
              <th>Total Qty</th>
              <td class="text-right">{{$qtys['Total Qty']}}</td>
            </tr>
    
          </tbody>
        </table>
        <hr>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-md-7">
    @include('product.parts.warehouse-activities')
  </div>
</div>


