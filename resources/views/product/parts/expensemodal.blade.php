<!-- Modal -->
<div class="modal fade" id="expenseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <form class="form-horizontal" id="expenseForm">
  <div class="modal-dialog" role="document">
    <div class="modal-content"  style="background: #fff">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Expense</h4>
      </div>
      <div class="modal-body">
       	  
       	  <div class="form-group">
		    <label for="" class="col-sm-2 control-label">Listing</label>
		    <div class="col-sm-10">
		      <select class="form-control"  name="data[id]">
		      	@foreach($product->listings as $listing)
		      	<option value="{{$listing->id}}"> {{$listing->sku}}</option>
		      	@endforeach
		      </select>
		    </div>
		  </div>
		  
		   @include('listing.parts.expense_form')
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" id="expenseModelBtn" class="btn btn-primary">Submit</button>
      </div>
      
      <div class="modal-spinner"><div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div></div>
      
    </div>
  </div>
  </form>
</div>

<script>
 	  var availableTags = ["Product Cost","Shipping","Ads","Reviews"];
// 	  $('#expenseModal').on('shown.bs.modal', function (e) {
// 		  console.log(1)
		  $( "#category" ).autocomplete({
		      source: availableTags,
		      minLength: 0,
		      delay:0
		    }).focus(function(){            
	            $(this).data("uiAutocomplete").search($(this).val());
	        });
		//})
	    
  		$( "#category" ).autocomplete( "option", "appendTo", "#expenseForm" );
  		$( "#category" ).autocomplete( "option", "minLength", 0 );
  		$( "#category" ).autocomplete( "option", "autoFocus", true );
  		$( "#category" ).autocomplete( "option", "delay", 0 );

  		$('#expenseForm').submit(function(){
			$(this).attr('disabled',true)
			$('.modal-spinner').show()
			$.post('{{route('listing.addexpense')}}',$('#expenseForm').serialize(),function(){
				window.location.reload()
			})

			return false
  	  	})
  </script>
  
  