<div class="mb20"></div>
<div class="row">
<div class="col-sm-10"><h4>Expensess</h4></div>
<div class="col-sm-2"><button type="button" class="btn btn-default" data-toggle="modal" data-target="#expenseModal">  Add Expense</button>
</div>
</div>



 <table class="table table-striped" style="table-layout: fixed;"><tbody>
 <tr>
 <th>SKU</th>
 <th>Posted Date</th><th>Posted By</th><th>Category</th>
 <th>Amount</th><th>Notes</th>
 <th>Spent By</th>
 <th></th>
 </tr>
 @foreach($product->expenses() as $expense)
 <tr>
 <td>{{$expense->listing->sku}}</td>
 <td>{{$expense->posted_date}}</td>
 <td>{{$expense->created_by}}</td>
 <td>{{$expense->category}}</td>
 <td>${{$expense->amount}}</td>
 <td>{{$expense->notes}}</td>
 <td>{{$expense->by}}</td>
 <td><a data-toggle="modal"  data-target="#editExpenseModal" class="modal-a"  href="{{route('listing.editexpense',[$expense->id])}}">Edit</a> | 
 <a class="wait-loading" onclick="return confirm('Are you sure to delete this record?')"  href="{{route('listing.deleteexpense',[$expense->id])}}">Delete</a> </td>
 </tr>
 @endforeach
 </tbody></table>
 
 
 
 <div class="modal fade" id="editExpenseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"  style="background: #fff">
        <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
        </div> <!-- /.modal-content -->
        
      
      <div class="modal-spinner"><div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div></div>
    </div> <!-- /.modal-dialog -->
</div> 
 
 
 