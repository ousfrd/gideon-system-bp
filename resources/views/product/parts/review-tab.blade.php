<div>
  <h3 class="text-center">reviews & ratings chart</h3>
  <div id="review-overview-chart" data-url="{{route('product.review-overview', ['id' => $product->id])}}">

  </div>
</div>

<div>
  <h3 class="text-center">sales rank chart</h3>
  <div id="sales-rank-chart" data-url="{{route('product.sales-rank-history', ['id' => $product->id])}}">

  </div>
</div>

<div>
  <h3 class="text-center">reviews types</h3>
  <div id="review_pie" style="width: 100%;height:100%" ></div>
</div>

<script>
  (function(){
    const urlParams = new URLSearchParams(window.location.search);
    let startDate = urlParams.get('start_date');
    let endDate = urlParams.get('end_date');
    if (startDate && endDate) {
      if (moment(endDate).diff(moment(startDate), 'days') < 7) {
        startDate = moment(endDate).subtract(7, 'days').format('Y-MM-DD');
      }
    } else {
      startDate = moment().subtract(7, 'days').format('Y-MM-DD');
      endDate = moment().format('Y-MM-DD');
    }

    showSalesRankChart();
    showReviewChart();
    showPieChart();

    function showSalesRankChart() {
      const base = $('#sales-rank-chart').data('url');
      const url = `${base}?from_date=${startDate}&to_date=${endDate}`;
      $.get(url, (data) => {
        const columns = [];
        let dates = [];
        for (key in data) {
          if (dates.length == 0) {
            dates = data[key].map((item)=>item.date)
            columns.push(['x', ...dates]);
          }
          sales_ranks = data[key].map((item)=>item.sales_rank)
          columns.push([key, ...sales_ranks]);
        }
        const chart = c3.generate({
          bindto: '#sales-rank-chart',
          data: {
            x: 'x',
            columns: columns
          },
          axis: {
            x: {
                type: 'timeseries',
                tick: {
                    format: '%Y-%m-%d'
                }
            }
          }
        });
      });
    }
    
    function showReviewChart() {
      const base = $('#review-overview-chart').data('url');
      const url = `${base}?from_date=${startDate}&to_date=${endDate}`;
      $.get(url, (data) => {
        const dates = data.map((item) => item.date);
        const reviews = data.map((item) => item.review_quantity);
        const ratings = data.map((item) => item.review_rating);
        const chart = c3.generate({
          bindto: '#review-overview-chart',
          data: {
            x: 'x',
            columns: [
              ['x', ...dates],
              ['reviews', ...reviews],
              ['ratings', ...ratings]
            ],
            axes: {
              reviews: 'y',
              ratings: 'y2'
            }
          },
          axis: {
            x: {
                type: 'timeseries',
                tick: {
                    format: '%Y-%m-%d'
                }
            },
            y: {
              label: {
                text: 'Reviews',
                position: 'outer-middle'
              }
            },
            y2: {
              label: {
                text: 'Ratings',
                position: 'outer-middle'
              },
              min: 4,
              max: 5,
              show: true
            }
          }
        });
      });
    }

    function showPieChart() {
      const chart = c3.generate({
        bindto: '#review_pie',
        data: {
            columns: [
          ['claim_count', {{ $categorizedReviewQuantity['CLAIM_REVIEW'] }} ],
          ['self_count', {{ $categorizedReviewQuantity['SELF_REVIEW'] }} ],
          ['invite_count', {{ $categorizedReviewQuantity['INVITE_REVIEW'] }} ],
          ['organic_count', {{ $categorizedReviewQuantity['OTHER_REVIEW'] }} ]
            ],
            type: 'pie'
        },
        pie: {
            label: {
                format: function (value, ratio, id) {
                    return d3.format()(value);
                }
            }
        }
      });
    }
  })();
</script>