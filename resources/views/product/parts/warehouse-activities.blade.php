<div class="panel panel-default">
  <div class="panel-heading">
    Warehouse Activities
    <div class="pull-right">
      total qty: {{$product->local_warehouse_qty}}
    </div>
  </div>
  <div class="panel-body">
    <div class="pull-right">
      <button class="btn btn-primary" data-toggle="modal" data-target="#warehouse-activity-modal" data-type="INBOUND" data-product-id="{{$product->id}}" data-asin="{{$product->asin}}" data-title="{{$product->short_name}}">create inbound</button>
      <button class="btn btn-warning" data-toggle="modal" data-target="#warehouse-activity-modal" data-type="OUTBOUND" data-product-id="{{$product->id}}" data-asin="{{$product->asin}}" data-title="{{$product->short_name}}">create outbound</button>
      <button class="btn btn-default" data-toggle="modal" data-target="#warehouse-activity-modal" data-type="Others" data-product-id="{{$product->id}}" data-asin="{{$product->asin}}" data-title="{{$product->short_name}}">create others</button>
    </div>
    <table class="table table-strips" id="warehouse-activity-table">
      <thead>
        <tr>
          <th>Operation</th>
          <th>Quantity</th>
          <th>Operator</th>
          <th>Time</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($warehouseActivities as $activity)
          <tr>
            <td><span class="activity-type {{$activity->activity_type}}" for="">{{$activity->activity_type}}</span></td>
            <td>{{$activity->quantity}}</td>
            <td>{{$activity->user->name}}</td>
            <td>{{$activity->created_at}}</td>
            <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete" data-url="{{route('warehouse-activity.destroy', ["id" => $activity->id])}}" data-key-name="" data-key-value="delete" >Delete</button></td>
          </tr>
        @endforeach
      </tbody>
    </table>
    
  </div>
</div>

@include('product.parts.warehouse-activity-modal')
@include('parts.delete-confirmation')