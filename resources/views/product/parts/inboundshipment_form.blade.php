 <div class="modal fade" id="inboundshipmentModal" tabindex="-1" role="dialog" aria-labelledby="inboundshipmentModal">
<form class="" method="Post" id="inboundshipmentForm">
{{Form::token()}}  
  <div class="modal-dialog" role="document">
    <div class="modal-content"  style="background: #fff">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Create Inbound Shipment Plan</h4>
      </div>
      <div class="modal-body">
       	  <div class="form-group row">
		    <label for="" class="col-sm-3 control-label">Listing</label>
		    <div class="col-sm-9">
		      <select class="form-control"  name="data[id]">
		      	@foreach($product->listings as $listing)
		      	<option value="{{$listing->id}}"> {{$listing->sku}}</option>
		      	@endforeach
		      </select>
		    </div>
		  </div>
		  @include('shipments.parts.form')
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" id="expenseModelBtn" class="btn btn-primary">Submit</button>
      </div>
      
      <div class="modal-spinner"><div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div></div>
      
    </div>
  </div>
</form>
</div>