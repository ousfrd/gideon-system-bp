@extends('layouts.master')
@section('title', $product->name)
@section('content')
<div class="btn-actions">
<button class="back-btn" onclick="window.location='{{url()->previous()}}'"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true">Back</span></button>


</div>
<div class="product dashboard">

<!-- Button trigger modal -->


<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#charts"><i class="fa fa-line-chart" aria-hidden="true"></i> Sales Report</a></li>
  <li><a data-toggle="tab" href="#info"><i class="fa " aria-hidden="true"></i>Product Info</a></li>
 
  <li><a data-toggle="tab" href="#listings"><i class="fa " aria-hidden="true"></i>Listings Info</a></li>
  
  <li><a data-toggle="tab" href="#inventory"><i class="fa " aria-hidden="true"></i>Inventory Report</a></li>
  <li><a data-toggle="tab" href="#ads"><i class="fa " aria-hidden="true"></i>Ads Report</a></li>
  <li><a data-toggle="tab" href="#expenses"><i class="fa " aria-hidden="true"></i>Expenses</a></li>
  <!-- <li><a data-toggle="tab" href="#inbound"><i class="fa " aria-hidden="true"></i>Inbound Shipments</a></li> -->
  <li><a data-toggle="tab" href="#reviews"><i class="fa " aria-hidden="true"></i>Reviews</a></li>
  
 
   <li class="pull-right" style="padding-left:20px">@include('listing.parts.filter')</li>
   
 </ul>
 
<div class="tab-content">
	<div id="charts" class="tab-pane active">
		@include('listing.parts.boxes')
		
		
		<div class="row" id="current-bsr-stats">
                <div class="col-md-12">
                    <div class="panel panel-default dark-bottom panel-stat">
                        <div class="panel-body">
                           
                            <strong>Current Best Sellers Rank (BSR): <?php echo json_encode($product->sales_ranks); ?></strong>
							
                            <!-- @if($product->sales_ranks)
                            <?php $ranks = json_decode($product->sales_ranks,true);?>

	                            @if(is_array($ranks))
		                            @foreach($ranks as $cat)
		                            	@foreach($cat as $rank => $subcat)
			                            #{{number_format($rank)}} in 
			                            	@if(is_array($subcat))
			                            		<?php $count = 1; $length = count($subcat); ?>
			   									@foreach (array_reverse($subcat) as $catname)
			   										<?php $count += 1 ?>
			   										@if(reset($catname) == 'Products')
			   										@continue
			   										@endif
			   										{{reset($catname)}}
			   										@if($count < $length)
			   										 > 
			   										@endif
			   									@endforeach
			                            	@else
			                            	{{ $subcat }}
			                            	@endif
		                            	@endforeach
		                            	<br/>
		                            @endforeach
		                        @else
	                            N/A
	                            @endif
                            
                            @else
                            N/A
                            @endif -->
						
							</div>
                    </div>
                </div>
        </div>
            
            
		@include('index.parts.charts') 
		
	</div>
	<div id="info" class="tab-pane fade">
		@include('product.parts.info')	
	</div>
	
	<div id="listings" class="tab-pane fade" >
		<div class="table-responsive">
		{!! $listing_grid->render() !!}
		</div>
	</div>
	
	<div id="inventory" class="tab-pane fade">
		@include('product.parts.inventory')
	</div>
	
	<div id="ads" class="tab-pane fade">
		@include('product.parts.ads')
	</div>
	
	
	<div id="expenses" class="tab-pane fade">
		@include('product.parts.expenses')
	</div>
	
	<div id="inbound" class="tab-pane fade">
		@include('product.parts.inboundshipments')
	</div>

	<div id="reviews" class="tab-pane fade">
		@include('product.parts.review-tab', ['product' => $product])
	</div>
	
</div>
 
 
@include('product.parts.expensemodal')
@include('product.parts.inboundshipment_form')




         
            



</div>



@endsection