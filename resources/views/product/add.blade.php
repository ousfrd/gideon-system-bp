@extends('layouts.master')
@section('title', 'Add New Product')
@section('content')
<div class="btn-actions">
<button class="back-btn" onclick="window.location='{{url()->previous()}}'"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true">Back</span></button>
</div>


<form class="form-horizontal" method="Post">
{{Form::token()}}
<div style="max-width:500px">
<div class="form-group row">
    {{ Form::label("ASIN", null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
    {{ Form::text('data[asin]', $product->asin, array_merge(['class' => 'form-control','required','placeholder'=>'ASIN','maxlength'=>"10"])) }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label("Country", null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
     {{ Form::select('data[country]', config('app.countries'), $product->country,['class' => 'form-control','required']) }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label("Name", null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
    {{ Form::text('data[code]', $product->code, array_merge(['class' => 'form-control','required','placeholder'=>'Name'])) }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label("Type", null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
     {{ Form::select('data[type]', ['general'=>'general','health'=>'health','beauty'=>'beauty'], $product->type,['class' => 'form-control']) }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label("Keywords", null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
    {{ Form::text('data[keywords]', $product->keywords, array_merge(['class' => 'form-control','placeholder'=>'Keywords'])) }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label("Group", null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
    {{ Form::text('data[product_group]', $product->group, array_merge(['class' => 'form-control','required','placeholder'=>'group','id'=>'product-group'])) }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label("Manager", null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
     {{ Form::select('data[managers][]', $managers, [],['class' => 'form-control','multiple' => 'multiple','required']) }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label("Status", null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
    {{ Form::select('data[status]', ['Active'=>'Active','Inactive'=>'Inactive'],$product->status,['class' => 'form-control','required']) }}
    </div>
</div>



<div class="form-group row">
    {{ Form::label("Old ASIN", null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
    {{ Form::text('data[old_asin]', $product->old_asin, array_merge(['class' => 'form-control','placeholder'=>'Old ASIN','maxlength'=>"10"])) }}
    </div>
</div>


<div class="form-group row">
    {{ Form::label("", null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
     <button type="submit" class="btn btn-default">Submit</button>
    </div>
</div>
<br/>
<br/><br/><br/><br/><br/>

</div>
</form>

<script>
  $( function() {
    var availableTags = {!! json_encode(array_keys($groups)) !!};
    $( "#product-group" ).autocomplete({
      source: availableTags
    });
  } );
  </script>
  
@endsection