@extends('layouts.master')
@section('title', "Gift Card Template: " . $giftCardTemplate->name)
@section('content')
<div class="btn-actions">
  <a href="{{route('gift-card-template.edit', ['giftCardTemplate'=>$giftCardTemplate])}}" class="btn btn-success pull-right"><span class="" >Edit</span></a>
</div>
<ol class="breadcrumb">
  <li><a href="{{route('gift-card-template.index')}}">Gift Card Templates List</a></li>
  <li class="active">{{$giftCardTemplate->name}}</li>
</ol>
<div class="panel panel-default">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-6 mx-auto col-md-offset-3">
          <div class="form-group">
            <label for="name">Name: <strong class="text-danger">*</strong></label>
            <p>{{$giftCardTemplate->name}}</p>
          </div>
          <hr>
          <div class="form-group">
            <label for="asin">ASIN: <strong class="text-danger">*</strong></label>
            <p>{{$giftCardTemplate->asin}}</p>
          </div>
          <hr>
          <div class="form-group">
            <label for="amount">Gift Card Amount: <strong class="text-danger">*</strong> </label>
            <p>{{$giftCardTemplate->amount}}</p>
          </div>
          <hr>
          <div class="form-group">
            <label for="channel">Channel: <strong class="text-danger">*</strong> </label>
            <p>{{$giftCardTemplate->channel}}</p>
          </div>
          <div class="form-group">
            <label for="channel_detail">Channel Detail: <strong class="text-danger">*</strong> </label>
            <p>{{$giftCardTemplate->channel_detail}}</p>
          </div>
          <hr>
          <div class="form-group">
            <label for="version">Version: <strong class="text-danger">*</strong> </label>
            <p>{{$giftCardTemplate->version}}</p>
          </div>
          <hr>
          <div class="form-group">
            <label for="front-design">Front Design: </label>
            <img class="preview-image thumbnail" src="{{$giftCardTemplate->getFrontDesignUrlAttribute()}}" alt="Front Design" />
          </div>
          <hr>
          <div class="form-group">
            <label for="back-design">Back Design:</label>
            <img class="preview-image thumbnail" src="{{$giftCardTemplate->getBackDesignUrlAttribute()}}" alt="Back Design" />
          </div>
          <hr>
          <div class="form-group">
            <label for="attachment1">attachment 1 </label>
            @if ($giftCardTemplate->attachment1)
              <a target="_blank" href="{{$giftCardTemplate->attachment1Url}}">attachment 1</a>
            @endif
          </div>
          <hr>
          <div class="form-group">
            <label for="attachment2">attachment 2 </label>
            @if ($giftCardTemplate->attachment2)
              <a target="_blank" href="{{$giftCardTemplate->attachment2Url}}">attachment 2</a>
            @endif
          </div>
          <hr>
          <div class="form-group">
            <label for="note">Note:</label>
            <p class="column-note">{{$giftCardTemplate->note}}</p>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection