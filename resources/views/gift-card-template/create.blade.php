@extends('layouts.master')
@section('title', "New Gift Card Template")
@section('content')
<ol class="breadcrumb">
  <li><a href="{{route('gift-card-template.index')}}">Gift Card Templates List</a></li>
  <li class="active">Create</li>
</ol>
<div class="row">
	 <div class="col-md-6 mx-auto col-md-offset-3">
    {{ Form::open(array('url' => route('gift-card-template.store'),'files'=>'true')) }}
      {{Form::token()}}
      <div class="form-group">
        <label for="name">Name: <strong class="text-danger">*</strong></label>
        <input required type="text" name="name" class="form-control" id="name">
      </div>
      <div class="form-group">
        <label for="asin">ASIN: <strong class="text-danger">*</strong></label>
        <input required type="text" name="asin" class="form-control" id="asin">
      </div>
      <div class="form-group">
        <label for="amount">Gift Card Amount: <strong class="text-danger">*</strong> </label>
        <input required type="number" name="amount" class="form-control" id="amount">
      </div>
      <div class="form-group">
        <label for="channel">Channel *:</label>
        <select required name="channel" id="channel" class="form-control">
          <option value="">--select--</option>
          <?php
          foreach ($channels as $val => $text)
            if ($val == "thank2u.com") {
              echo('<option selected="selected" value='.$val.'>'.$text.'</option>');
            } else {
              echo('<option value='.$val.'>'.$text.'</option>');
            }
          ?>
        </select>
      </div>
      <div class="form-group">
        <label for="channel_detail">Channel Detail: <strong class="text-danger">*</strong> </label>
        <input required type="text" name="channel_detail" class="form-control" id="channel_detail">
      </div>
      <div class="form-group">
        <label for="version">Version: <strong class="text-danger">*</strong> </label>
        <input required type="text" name="version" class="form-control" id="version">
      </div>
      <div class="form-group">
        <label for="template_type"> Type: <strong class="text-danger">*</strong> </label>
        <select required class="form-control"  name="template_type" id="template_type">
          @foreach(App\GiftCardTemplate::$templateTypes as $value=>$text)
            <option value="{{$value}}">{{$text}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="">
          Front Design Image: <span class="fa fa-info-circle" tabindex="0" role="button" data-toggle="popover" data-element="#design-image-hint" data-trigger="focus" data-placement="top" data-original-title="" title=""></span>
        </label>
        <img class="preview-image thumbnail" src="/images/noImageUploaded.png" alt="Front Design" />
        <input class="previe-image-upload" type="file" name="front-design-file" class="form-control" id="front-design" accept="image/*,.pdf">
        <p id="design-image-hint" class="hidden">The image uploaded here will show on list page and campaign's select options for better recognization</p>
      </div>
      <div class="form-group">
        <label for="">Back Design Image: <span class="fa fa-info-circle" tabindex="0" role="button" data-toggle="popover" data-element="#design-image-hint" data-trigger="focus" data-placement="top" data-original-title="" title=""></span>
        </label>
        <img class="preview-image thumbnail" src="/images/noImageUploaded.png" alt="Back Design" />
        <input class="previe-image-upload" type="file" name="back-design-file" class="form-control" id="back-design" accept="image/*,.pdf">
      </div>
      <div class="form-group">
        <label for="attachment1">attachment 1 </label>
        <input type="file" name="attachment1-file" class="form-control" id="attachment1" accept="image/*,.pdf">
      </div>
      <div class="form-group">
        <label for="attachment2">attachment 2 </label>
        <input type="file" name="attachment2-file" class="form-control" id="attachment2" accept="image/*,.pdf">
      </div>
      <div class="form-group">
        <label for="note">Note:</label>
        <textarea class="form-control" rows="10" name="note" id="note"></textarea>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary btn-lg btn-submit">Submit</button>
      </div>
    {{ Form::close() }}
  </div>
</div>
<script>
$(document).ready(function() {

  $('[data-toggle="popover"]').popover({
    content:function(){
      var elmt = $(this).attr('data-element')
      return $(elmt).html()

    }
  })

  $("input.previe-image-upload").change(function() {
    readURL(this);
  });
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $(input).siblings('img.preview-image').attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }

})

</script>
@endsection