@extends('layouts.master')
@section('title', "Gift Card Templates")
@section('content')
<div class="btn-actions">
  <a href="{{route('gift-card-template.create')}}" class="btn btn-primary pull-right"><span class="" >Create New Gift Card Template</span></a>
</div>

{!! $grid !!}

<!-- Modal -->
<div class="modal fade" id="confirmDelete" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Are you sure you want to delete <b id="compaignName"></b>?</h4>
      </div>
      <div class="modal-body">
        <p class="text-default">Please input the ASIN below and then click delete if you really want to delete this campaign</p>
        <input type="text" id="confimName" class="form-control">
        <p id="wrong-name" class="text-danger">Your input is not exactly the same as the ASIN</p>
      </div>
      <div class="modal-footer">
        <i id="spinner" class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>
        <div id="btn-group">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger" id="deleteBtn">Delete</button>
        </div>
      </div>
    </div>
    
  </div>
</div>
<script>

  $('#confirmDelete').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget);
    let id = button.data('id'); // Extract info from data-* attributes
    let name = button.data('name');
    let url = button.data('url');
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    let modal = $(this);
    modal.find('#wrong-name').hide();
    modal.find('#btn-group').show();
    modal.find('#spinner').hide();
    modal.find('#compaignName').text(name);
    let input = modal.find('#confimName');
    $('#deleteBtn').on('click', function() {
      if (input.val().trim() == name.trim()) {
        modal.find('#wrong-name').hide();
        modal.find('#btn-group').hide();
        modal.find('#spinner').show();
        $.ajax({
          url: url,
          type: 'DELETE',
          success: function(res){
            console.log(res);
            button.closest('tr').remove();
            modal.modal('hide');
          },
          error: function(err) {
            modal.find('#btn-group').show();
            modal.find('#spinner').hide();
            console.log(err);
          }
        });
        
      } else {
        modal.find('#wrong-name').show();
      }
    })
  });

  $("#confirmDelete").on('hide.bs.modal' , function (event) {
    let modal = $(this);
    modal.find('#confimName').val("");
  })
</script>
@endsection