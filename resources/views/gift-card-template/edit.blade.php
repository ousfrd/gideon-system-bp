@extends('layouts.master')
@section('title', "Edit Gift Card Template: " . $giftCardTemplate->name)
@section('content')
<ol class="breadcrumb">
  <li><a href="{{route('gift-card-template.index')}}">Gift Card Templates List</a></li>
  <li><a href="{{route('gift-card-template.show', ['giftCardTemplate' => $giftCardTemplate])}}">{{$giftCardTemplate->name}}</a></li>
  <li class="active">Edit</li>
</ol>

<div class="row">
	 <div class="col-md-6 mx-auto col-md-offset-3">
    {{ Form::open(array('url' => route('gift-card-template.update', ['giftCardTemplate' => $giftCardTemplate]),'files'=>'true', 'method'=>'put')) }}
      <div class="form-group">
        <label for="name">Name: <strong class="text-danger">*</strong></label>
        <input required type="text" name="name" class="form-control" id="name" value="{{$giftCardTemplate->name}}">
      </div>
      <div class="form-group">
        <label for="asin">ASIN: <strong class="text-danger">*</strong></label>
        <input required type="text" name="asin" class="form-control" id="asin" value="{{$giftCardTemplate->asin}}">
      </div>
      <div class="form-group">
        <label for="amount">Gift Card Amount: <strong class="text-danger">*</strong> </label>
        <input required type="number" name="amount" class="form-control" id="amount" value="{{$giftCardTemplate->amount}}">
      </div>
      <div class="form-group">
        <label for="channel">Channel *:</label>
        <select required name="channel" id="channel" class="form-control">
          <option value="">--select--</option>
          <?php
          foreach ($channels as $val => $text)
            if ($val == "website") {
              echo('<option selected="selected" value='.$val.'>'.$text.'</option>');
            } else {
              echo('<option value='.$val.'>'.$text.'</option>');
            }
          ?>
        </select>
      </div>
      <div class="form-group">
        <label for="channel_detail">Channel Detail: <strong class="text-danger">*</strong> </label>
        <input required type="text" name="channel_detail" class="form-control" id="channel_detail" value="{{$giftCardTemplate->channel_detail}}">
      </div>
      <div class="form-group">
        <label for="version">Version: <strong class="text-danger">*</strong> </label>
        <input required type="text" name="version" class="form-control" id="version" value="{{$giftCardTemplate->version}}">
      </div>
      <div class="form-group">
        <label for="template_type"> Type: <strong class="text-danger">*</strong> </label>
        <select required class="form-control"  name="template_type" id="template_type">
          @foreach(App\GiftCardTemplate::$templateTypes as $value=>$text)
            <option value="{{$value}}" {{$giftCardTemplate->template_type ? "selected" :  ""}} >{{$text}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="front-design">Front Design:  <strong class="text-danger">*</strong></label>
        <img class="preview-image thumbnail" src="{{$giftCardTemplate->getFrontDesignUrlAttribute()}}" alt="Front Design" />
        <input class="previe-image-upload" type="file" name="front-design-file" class="form-control" id="front-design" accept="image/*,.pdf">
      </div>
      <div class="form-group">
        <label for="back-design">Back Design:  <strong class="text-danger">*</strong></label>
        <img class="preview-image thumbnail" src="{{$giftCardTemplate->getBackDesignUrlAttribute()}}" alt="Back Design" />
        <input class="previe-image-upload" type="file" name="back-design-file" class="form-control" id="back-design" accept="image/*,.pdf">
      </div>
      <div class="form-group">
        <label for="attachment1">attachment 1 </label>
        @if ($giftCardTemplate->attachment1)
          <a target="_blank" href="{{$giftCardTemplate->attachment1Url}}">attachment 1</a>
        @endif
        <input type="file" name="attachment1-file" class="form-control" id="attachment1" accept="image/*,.pdf">
      </div>
      <div class="form-group">
        <label for="attachment2">attachment 2 </label>
        @if ($giftCardTemplate->attachment2)
          <a target="_blank" href="{{$giftCardTemplate->attachment2Url}}">attachment 2</a>
        @endif
        <input type="file" name="attachment2-file" class="form-control" id="attachment2" accept="image/*,.pdf">
      </div>
      <div class="form-group">
        <label for="note">Note:</label>
        <textarea class="form-control" rows="10" name="note" id="note">{{$giftCardTemplate->note}}</textarea>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary btn-lg btn-submit">Update</button>
      </div>
    {{ Form::close() }}
  </div>
</div>
<script>
$(document).ready(function() {
  $("input.previe-image-upload").change(function() {
    readURL(this);
  });
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $(input).siblings('img.preview-image').attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }

})

</script>
@endsection