@extends('layouts.master')
@section('title', "Customer Relationship Management")
@section('content')
  <div class="row country-grid">
  <?php $counter = 0; ?>
  @foreach ($customersCountByCountry as $country => $count)
    <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
      <div class="small-box bg-background-{{$counter % 5}}">
        <div class="inner">
          <img src="{{asset('images/country-flags/' .strtolower($country). '.png')}}" alt="{{$country}}">
          <p>{{ Locale::getDisplayRegion('-'.$country, 'us') }}</p>
          <h3>{{number_format($count)}}</h3>
          <p>Customers</p>
        </div>
        <a href="{{route('customers.country-dashboard', ['country' => $country])}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <?php $counter++; ?>
  @endforeach
  </div>
@endsection
