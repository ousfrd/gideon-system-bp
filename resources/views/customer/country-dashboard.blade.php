@extends('layouts.master')
@section('title', "Customers")
@section('content')
<ol class="breadcrumb">
  <li><a href="{{route('customers.dashboard')}}">Dashboard</a></li>
  <li class="active">{{Locale::getDisplayRegion('-'.$country, 'us')}}</li>
</ol>

<div class="row">
  <div class="col-sm-3 col-xs-12">
    <div class="small-box bg-info">
      <div class="inner">
        <h3>{{number_format($customersCount)}}</h3>

        <p>Customers</p>
      </div>
      <a href="{{route('customers.all', ['country'=>$country])}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-sm-3 col-xs-12">
    <div class="small-box bg-success">
      <div class="inner">
        <h3>{{number_format($repeatBuyersCount)}}</h3>

        <p>Repeat Buyers</p>
      </div>
      <a href="{{route('customers.repeat-buyers', ['country'=>$country])}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-sm-3 col-xs-12">
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{number_format($realEmailsCount) }}</h3>

        <p>Real Emails</p>
      </div>
      <a href="{{route('customers.real-email-buyers', ['country'=>$country])}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-sm-3 col-xs-12">
    <div class="small-box bg-brown">
      <div class="inner">
        <h3>{{number_format($positiveReviewersCount) }}</h3>

        <p>Positive Reviewers</p>
      </div>
      <a href="{{route('customers.positive-reviewer', ['country'=>$country])}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-3 col-xs-12">
    <div class="small-box bg-orange">
      <div class="inner">
        <h3>{{number_format($negativeReviewersCount) }}</h3>

        <p>Negative Reviewers</p>
      </div>
      <a href="{{route('customers.negative-reviewer', ['country'=>$country])}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
</div>
<hr>
<h4>Customers By State</h4>
<hr>
<table class="table table-strip">
  <thead>
    <tr>
      <th>State</th>
      <th>Customers</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($stateCustomersCount as $stateCount)
      <tr>
        <td>{{ $stateCount->state }}</td>
        <td>{{ $stateCount->amount }}</td>
      </tr>
    @endforeach
  </tbody>
</table>

@endsection