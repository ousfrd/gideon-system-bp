@extends('layouts.master')
@section('title', "Customer Detail ". $customer->name)
@section('content')
<ol class="breadcrumb">
  <li><a href="{{route('customers.dashboard')}}">Dashboard</a></li>
  <li class="active">{{$customer->name}}</li>
</ol>
<div class="panel panel-info">
  <div class="panel-heading">
    Profile
  </div>
  <div class="panel-body {{$customer->blocklisted ? 'blocklisted' : ''}}">
    <div class="row">
      <div class="col-xs-12 col-md-5">
        <table class="table table-striped">
          <tbody>
            <tr>
              <td>Blocklisted</td>
              <td>
                @can('edit-customer-info')
                  <input value="1" id="blocklisted"
                        data-size="small" 
                        data-pk="{{$customer->id}}"
                        data-method="PUT"
                        data-url="{{route('customers.update', ['id' => $customer->id])}}"
                        {{$customer->blocklisted ? 'checked' : ''}}
                        type="checkbox"
                        data-toggle="toggle">
                @endcan
              </td>
            </tr>
            <tr>
              <td>Name</td>
              <td>
                @can('edit-customer-info')
                <a href="#" class="editable" id="name" 
                    data-type="text" 
                    data-pk="{{$customer->id}}" 
                    data-url="{{route('customers.update', ['id' => $customer->id])}}" 
                    data-title="Customer Name">{{$customer->name}}</a>
                @else
                  {{$customer->name}}
                @endcan
                </td>
            </tr>
            <tr>
              <td>Real Email</td>
              <td>
              @can('edit-customer-info')
                <a href="#" class="editable" id="email" 
                    data-type="text" 
                    data-pk="{{$customer->id}}" 
                    data-url="{{route('customers.update', ['id' => $customer->id])}}" 
                    data-title="Customer Name">{{$customer->email}}</a>
              @else
                {{$customer->email}}
              @endcan
              </td>
            </tr>
            <tr>
              <td>Amazon Email</td>
              <td>{{$customer->amazon_buyer_email}}</td>
            </tr>
            <tr>
              <td>Facebook Account</td>
              <td>
              @can('edit-customer-info')
                <a href="#" class="editable" id="facebook_account" 
                    data-type="text" 
                    data-pk="{{$customer->id}}" 
                    data-url="{{route('customers.update', ['id' => $customer->id])}}" 
                    data-title="Customer Facebook">{{$customer->facebook_account}}</a>
              @else
                {{$customer->facebook_account}}
              @endcan
              </td>
            </tr>
            <tr>
              <td>Wechat Account</td>
              <td>
              @can('edit-customer-info')
                <a href="#" class="editable" id="wechat_account" 
                    data-type="text" 
                    data-pk="{{$customer->id}}" 
                    data-url="{{route('customers.update', ['id' => $customer->id])}}" 
                    data-title="Customer Wechat">{{$customer->wechat_account}}</a>
                @else
                  {{$customer->wechat_account}}
                @endcan
              </td>
            </tr>
            <tr>
              <td>Whatsapp Account</td>
              <td>
              @can('edit-customer-info')
                <a href="#" class="editable" id="whatsapp_account" 
                    data-type="text" 
                    data-pk="{{$customer->id}}" 
                    data-url="{{route('customers.update', ['id' => $customer->id])}}" 
                    data-title="Customer Whatapp">{{$customer->whatsapp_account}}</a>
              @else
                {{$customer->whatsapp_account}}
              @endcan
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="col-xs-12 col-md-4">
        <table class="table table-striped">
          @php
          if ($defaultShipment) {
            $address1 = $customer->address1 ?? $defaultShipment->ship_address_1;
            $address2 = $customer->address2 ?? $defaultShipment->ship_address_2;
          } else {
            $address1 = $customer->address1;
            $address2 = $customer->address2;
          }
          @endphp
          <tr>
            <td>Address1</td>
            <td>
            @can('edit-customer-info')
              <a href="#" class="editable" id="address1" 
                    data-type="text" 
                    data-pk="{{$customer->id}}" 
                    data-url="{{route('customers.update', ['id' => $customer->id])}}" 
                    data-title="Customer Address1">{{$address1}}
              </a>
              @else
                {{$customer->city}}
              @endcan
            </td>
          </tr>
          <tr>
            <td>Address2</td>
            <td>
            @can('edit-customer-info')
              <a href="#" class="editable" id="address2" 
                    data-type="text" 
                    data-pk="{{$customer->id}}" 
                    data-url="{{route('customers.update', ['id' => $customer->id])}}" 
                    data-title="Customer Address2">{{$address2}}
              </a>
              @else
                {{$customer->city}}
              @endcan
            </td>
          </tr>
          <tr>
            <td>City</td>
            <td>
            @can('edit-customer-info')
              <a href="#" class="editable" id="city" 
                    data-type="text" 
                    data-pk="{{$customer->id}}" 
                    data-url="{{route('customers.update', ['id' => $customer->id])}}" 
                    data-title="Customer City">{{$customer->city}}
              </a>
              @else
                {{$customer->city}}
              @endcan
            </td>
          </tr>
          <tr>
            <td>State</td>
            <td>
            @can('edit-customer-info')
              <a href="#" class="editable" id="state" 
                    data-type="text" 
                    data-pk="{{$customer->id}}" 
                    data-url="{{route('customers.update', ['id' => $customer->id])}}" 
                    data-title="Customer State">{{$customer->state}}</a>
                @else
                  {{$customer->state}}
                @endcan
            </td>
          </tr>
          <tr>
            <td>Country</td>
            <td>
              @can('edit-customer-info')
              <a href="#" class="editable" id="country" 
                    data-type="text" 
                    data-pk="{{$customer->id}}" 
                    data-url="{{route('customers.update', ['id' => $customer->id])}}" 
                    data-title="Customer Country">{{$customer->country}}</a>
              @else
                {{$customer->country}}
              @endcan
            </td>
          </tr>
          <tr>
            <td>Postal Code</td>
            <td>
            @can('edit-customer-info')
              <a href="#" class="editable" id="postal_code" 
                    data-type="text" 
                    data-pk="{{$customer->id}}" 
                    data-url="{{route('customers.update', ['id' => $customer->id])}}" 
                    data-title="Customer Name">{{$customer->postal_code}}</a>
            @else
              {{$customer->postal_code}}
            @endcan
            </td>
          </tr>
        </table>
      </div>
      <div class="col-xs-12 col-md-3">
        <table class="table table-striped">
          <tr>
            <td>Orders</td>
            <td>{{$customer->bought_orders_count}}</td>
          </tr>
          <tr>
            <td class="text-danger">Refunded Orders</td>
            <td class="text-danger">{{$customer->refunded_orders_count}}</td>
          </tr>
          <tr>
            <td>Postive Reviews</td>
            <td>{{$customer->leave_possitive_reviews_count}}</td>
          </tr>
          <tr>
            <td class="text-danger">Negative Reviews</td>
            <td class="text-danger">{{$customer->leave_negative_reviews_count}}</td>
          </tr>
          <tr>
            <td>Claim Reviews</td>
            <td>{{$customer->claim_reviews_count}}</td>
          </tr>
          <tr>
            <td>Invite Reviews</td>
            <td>{{$customer->invite_reviews_count}}</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    Notes
  </div>
  <div class="panel-body">
    <ul>
    @foreach ($customer->customerNotes as $note)
      <li>
        <span>{{$userIdNameHash[$note->created_by]}}</span> Added At <time>{{$note->created_at}}</time> 
        <p>
        @can('edit-customer-info')
          <a href="#" class="editable" id="note" 
                data-type="textarea" 
                data-pk="{{$note->id}}" 
                data-url="{{route('customers.customer-notes.update', ['customer_id' => $customer->id, 'id' => $note->id])}}" 
                data-title="Customer Note">{{$note->note}}</a>
        @else
          {{$note->note}}
        @endcan
        </p>
      </li>
    @endforeach
    </ul>
    {{ Form::open(array('method'=>'post','url' => route('customers.customer-notes.store', ['customer_id' => $customer->id]), 'id'=>'new-note')) }}
      <input type="hidden" name="redirect" value="{{route('customers.show', ['id'=>$customer->id])}}">
      <div class="form-group">  
        <label for="note">Note:</label>
        <textarea required class="form-control" name="note" id="note" cols="15" rows="3"></textarea>
      </div>
      <button class="btn btn-primary">Add</button>
    {{ Form::close() }}
  </div>
</div>
<hr>
<section>
  <h3>Orders</h3>
  {!! $shipmentGrid !!}
  <hr>
</section>

<section>
  <h3>Claim Review Sent Out</h3>
  <table class="table table-strip">
    <thead>
      <tr>
        <th>Order Id</th>
        <th>Asin</th>
        <th>Campaign Name</th>
        <th>Supplier</th>
        <th>Gift Card Amount</th>
        <th>Campaign Created At</th>
        <th>Campaign Sent At</th>
        <th>Letter Design Front</th>
        <th>Letter Design Back</th>
        <th>Click Send Response</th>
        <th>Click Send Schedule Date</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($orderItemMarketings as $oim)
        <tr>
          <td>{{ $oim->oim_amazon_order_id }}</td>
          <td>{{ $oim->asin }}</td>
          <td>{{ $oim->claimReviewCampaign->name }}</td>
          <td>{{ $oim->claimReviewCampaign->platform }}</td>
          <td>{{ $oim->claimReviewCampaign->cashback }}</td>
          <td>{{ $oim->created_at }}</td>
          <td>{{ $oim->sent_at }}</td>
          <td>
            @if ($oim->claimReviewCampaign->giftCardTemplate)
              <img class="small-thumbnail" src="{{$oim->claimReviewCampaign->giftCardTemplate->front_design_url}}" alt="">
            @endif
          </td>
          <td>
            @if ($oim->claimReviewCampaign->giftCardTemplate)
              <img class="small-thumbnail" src="{{$oim->claimReviewCampaign->giftCardTemplate->back_design_url}}" alt="">
            @endif
          </td>
          <th><pre>{{ $oim->click_send_response }}</pre></th>
          <th>{{ $oim->click_send_scheduled_send_at }}</th>
        </tr>
      @endforeach
    </tbody>
  </table>
  <hr>
</section>

<section>
<h3>Redeems</h3>
{!! $redeemGrid !!}
</section>

<script>
  $(document).ready(function() {
      $('.editable').editable({
        ajaxOptions: {
          type: 'put'
        },
        success: function () {console.log("Success Response from server")},
        error: function() {console.log("Error response form server")}
      });

      $('#blocklisted').on('change', function() {
        if (this.checked) {
          $(this).closest('.panel-body').addClass('blocklisted');
        } else {
          $(this).closest('.panel-body').removeClass('blocklisted');
        }
      });
  });
</script>
@endsection

