@extends('layouts.master')
@section('title', "Customers Export")
@section('content')
<div class="panel-group product-filter-form filter-form">
    <div class="panel panel-default">
      <div class="panel-heading">
        Filter Form
      </div>
      <div class="panel-body">
      {{ Form::open(array('url' => route('customers.export-generate'))) }}
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter padding-sm">
              <div class="checkbox">
                <label>
                  <input type="checkbox" value="1" name="has_real_email"> Has Real Email
                </label>
              </div>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter padding-sm">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="has_wechat_account" value="1"> Has Wechat Account
                </label>
              </div>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter padding-sm">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="has_facebook_account" value="1"> Has Facebook Account
                </label>
              </div>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter padding-sm">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="excludes_refund_orders" value="1"> Exclude Refund Orders
                </label>
              </div>
            </div>
          </div>
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter padding-sm">
              <div class="checkbox">
                <label>
                  <input type="checkbox" value="1" name="is_repeated"> Repeat Buyer
                </label>
              </div>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter padding-sm">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="is_positive_reviewer" value="1"> Leave Possitive Review
                </label>
              </div>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter padding-sm">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="is_negative_reviewer" value="1"> Leave Negative Review
                </label>
              </div>
            </div>
          </div>
          <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter padding-sm">
                <h5>Select Account:</h5>
                    <div class="input form-group select">
                      <select  name="seller_accounts[]" style="width: 100%" class="multiple-select" multiple="true" data-minimum-input-length = 0 data-placeholder="Select Account" removeDefaultClass="1">
                        <option value="select-all">Select All</option>
                        <option value="deselect-all">Deselect All</option>
                        @foreach ($sellers as $value=>$text)
                          <option class="option" value="{{$value}}">{{$text}}</option>
                        @endforeach
                      </select>
                      </div>
                </div>

              <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter salesChannelCont padding-sm">
                <h5>Asins:</h5>
                    <div class="input form-group">
                      <input type="text" name="asins" class="form-control">
                    </div>
              </div>

              <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter padding-sm">
                  <h5>Select First Order Date</h5>
                  <div class="input form-group">
                    <input required type="text" class="form-control" name="first_purchase_date">
                  </div>
              </div>
          </div>

            <div class="row" style="padding: 20px 0 0;">

                <div class="col-md-2 col-sm-2 col-xs-12">
                    <div class="submit">
                      <button type="submit" class="btn btn-success btn-block" id="generateReportBtn">Generate Report</button>
                    </div>                        
                </div>
            </div>

      </div>

    {{ Form::close() }}
  </div>
</div>
<div>
  <table class="table table-striped">
    <thead>
      <th>Date Requested</th>
      <th>Seller Accounts</th>
      <th>Asins</th>
      <th>Real Email</th>
      <th>Wechat Account</th>
      <th>Facebook</th>
      <th>is repeated</th>
      <th>is positive reviewer</th>
      <th>is negative reviewer</th>
      <th>First Purchase Date</th>
      <th>Download</th>
    </thead>
    <tbody>
    @foreach($exportors as $exportor)
      <tr>
        <td>{{ $exportor->created_at }}</td>
        <td>
          @if ($exportor->seller_accounts_array)
            <ul class="list-group">
              @foreach ($exportor->seller_accounts_array as $seller_account)
              <li class="list-group-item">{{ $sellers[$seller_account] }}</li>
              @endforeach
            </ul>
          @endif
        </td>
        <td>
        @if ($exportor->asins_array)
          <ul class="list-group">
              @foreach ($exportor->asins_array as $asin)
              <li class="list-group-item">{{ $asin }}</li>
              @endforeach
            </ul>
        @endif
        </td>
        <td>{{ $exportor->has_real_email ? "Yes" : "Who Cares" }}</td>
        <td>{{ $exportor->has_wechat_account ? "Yes" : "Who Cares" }}</td>
        <td>{{ $exportor->has_facebook_account ? "Yes" : "Who Cares" }}</td>
        <td>{{ $exportor->excludes_refund_orders ? "Yes" : "No" }}</td>
        <td>{{ $exportor->is_repeated ? "Yes" : "Who Cares" }}</td>
        <td>{{ $exportor->is_positive_reviewer ? "Yes" : "Who Cares" }}</td>
        <td>{{ $exportor->is_negative_reviewer ? "Yes" : "Who Cares" }}</td>
        <td>{{ $exportor->first_purchase_date }}</td>
        <td>
          @if ($exportor->file)
            <a target="_blank" href="{{$exportor->file_url}}">download</a>
          @else

          @endif
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>
</div>
<script>
$(document).ready(function() {
  $('input[name="first_purchase_date"]').daterangepicker({
      singleDatePicker: true,
      maxDate: moment(),
      locale:{
        format:'YYYY-MM-DD'
      },
    }, ((start, end, label) => {
  }));

  $('.multiple-select').select2({
    allowClear: true
  });

  $('.multiple-select').on('select2-selecting', function(event) {
    if (event.val == 'deselect-all') {
      event.preventDefault();
      $(this).val(null).trigger('change');
    }
    if (event.val == 'select-all') {
      event.preventDefault();
      var allSellers = $(this).find('option').map((i, e) => e.value);
      allSellers.splice(0,2);
      $(this).val(allSellers).trigger('change');
    }
  });

});
</script>
@endsection
