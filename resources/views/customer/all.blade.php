@extends('layouts.master')
@section('title', "All Customers")
@section('content')
<ol class="breadcrumb">
  <li><a href="{{route('customers.dashboard')}}">Dashboard</a></li>
  @if (isset($country) && $country)
  <li><a href="{{route('customers.country-dashboard', ['country'=>$country])}}">{{$country}}</a></li>
  @endif
  <li class="active">All Customers</li>
</ol>
{!! $grid !!}
@endsection
