      
{!! Form::model($user, ['route' => ['user.edit', $user->id], 'class' => 'form-horizontal']) !!}

<div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit User {{$user->name}}</h4>
      </div>
<div class="modal-body">
<div class="form-group row">
    {{ Form::label("Name", null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
    {{ Form::text('name', $user->name, array_merge(['class' => 'form-control','required','placeholder'=>'Name'])) }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label("Email", null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
    {{ Form::email('email', $user->email, array_merge(['class' => 'form-control','required','placeholder'=>'Email'])) }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label("Send Email Notification", null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
    {{ Form::select('email_notice', ['0' => 'No', '1' => 'Yes'], $user->email_notice,['class' => 'form-control','required']) }}
    </div>
</div>


<div class="form-group row">
    {{ Form::label("Verified", null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
    {{ Form::select('verified', ['0' => 'No', '1' => 'Yes'], $user->verified,['class' => 'form-control','required']) }}
    </div>
</div>



<div class="form-group row">
    {{ Form::label("Role", null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
    @foreach (config('app.roles') as $key=>$value)
        <label class="checkbox-inline">
            {{ Form::checkbox('role[]', $value, in_array($value, $userRoles), ['class' => 'form-check-input'])}}
            {{  $key }}
        </label>
    @endforeach
    </div>
</div>

<div class="form-group row">
	<label for="Role" class="col-sm-2 control-label">Managed Products</label>
	<div class="col-sm-10">
		<!-- <select multiple="multiple" name="products[]" class="products-managed form-control"></select> -->
    <input type="hidden" name="products" id="products-managed" style="width:600px" value="{{count($user->products) > 0 ? $user->products[0]->id : ''}}" />

	</div>
</div>


</div>

<div class="modal-footer">
      	
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" id="newuserModelBtn" class="btn btn-primary">Submit</button>
      </div>
      
  <div class="modal-spinner"><div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div></div>    
{!! Form::close() !!}

<script type="text/javascript">
	$('form').submit(function(){
		
		//$('#animatedModal').css({'z-index':0});
		//$('.modal-loading').fadeIn()
		$('.modal-spinner').show()
		
	})

$(document).ready(function() {

$("#products-managed").select2({
    width: '100%',
    allowClear: true,
    multiple: true,
    minimumInputLength: 3,
    ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
        placeholder: "Select a Product",
        url: "{{route('utils.ajax.products')}}",
        dataType: 'json',
        quietMillis: 250,
        data: function (term, page) {
            return {
                q: term, // search term
                page: page
            };
        },
        results: function(data, page) {
              var more = (page * 15) < data.total_count;

              return {results: data.results, more: more};
        },
        cache: true
    },

    initSelection : function (element, callback) {
        var data = [];
        @foreach($user->products as $p)
          data.push({id: '{{$p->id}}', text: '{{$p->asin}} - {{$p->country}} - {{$p->name}}', short_text: '{{$p->asin}} - {{$p->country}}'});
        @endforeach

        callback(data);
    },

    formatResult: function(element){
        return "<div>" + element.text + "</div>";
    },
    formatSelection: function(element){
        return element.short_text;
    },
    escapeMarkup: function(m) {
        return m;
    }

})

})

</script>
