@extends('layouts.email')
@section('content')
<p>Dear {{$user->name}}</p>

<p>Below is your products summary report on {{date('l, M d, Y')}}</p>

<!-- <h3>{{count($lowInventoryProducts)}} of your products are out of stock or running out.</h3> -->
@if(count($lowInventoryProducts) > 0 ) 
<style>
.wrapper td, .wrapper th{}
</style>
<table class="wrapper" width="100%" cellpadding="4" cellspacing="1" style="background: #ccc">
<thead>
<tr>
<th style="background:#fff;padding:7px 0"></th>
<th style="background:#fff;padding:7px 0">ASIN</th>
<th style="background:#fff;padding:7px 0">Manager</th>
<th style="background:#fff;padding:7px 0">Daily AVG Orders</th>
<th style="background:#fff;padding:7px 0">Total Warehourse Qty</th>
<th style="background:#fff;padding:7px 0">Warehourse Qty Running out days</th>

<th style="background:#fff;padding:7px 0">Total Qty</th>
<th style="background:#fff;padding:7px 0">Est. Running out days </th>
<th style="background:#fff;padding:7px 0">Replenishment Cycle (days)</th>
</tr>
</thead>
@foreach($lowInventoryProducts as $product) 
<tr align="center" >
<td style="background:#fff"><a title="{{$product->name}}" href="{{$product->amazonLink () }}" target="_blank" ><img src="{{$product->small_image}}" style="max-width:100px;max-height:100px"/></a></td>

<td style="background:#fff">{{$product->asin}} {{$product->country}}</td>
<td  style="background:#fff">{{implode(', ', $product->managerList())}}</td>

<td style="background:#fff">{{round($product->dailyAverageOrders())}}</td>
<td style="background:#fff">{{$product->wareHouseTotalQty()}}</td>
<td style="background:#fff">{{$product->warehouseQtyDaysToOutofStock()}}</td>

<td style="background:#fff">{{$product->totalQty()}}</td>

<td style="background:#fff">{{$product->daysToOutofStock()}}</td>
<td style="background:#fff">{{$product->totalDayToReinstock()}}</td>
</tr>
@endforeach
</table>
@endif
<br/>
<p>Best,</p>
@endsection