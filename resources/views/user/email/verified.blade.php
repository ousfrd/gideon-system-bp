@extends('layouts.email')
@section('content')
<p>Dear {{$user->name}}</p>

<p>Your {{$site}} account has been verified. You can now use your login information below to log into the system.</p>
<p>URL: {{$link}}<br/>
Username: {{$user->email}}<br/>
Password: as you set<br/>
</p>

<p>If you forgot your password, you can use the following link to reset your password.<br/>
<a href="{{route('password.request')}}">{{route('password.request')}}</a>
<br/>
<p>Best,</p>
@endsection