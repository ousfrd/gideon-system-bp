<?php
use App\Product;
?>
@extends('layouts.email')
@section('content')
<p>Dear {{$user->name}}</p>

<p>Below is your products summary report on {{date('l, M d, Y')}}</p>
@if(count($excessInventory) > 0 ) 
<style>
.wrapper td, .wrapper th{}
</style>
<table class="wrapper" width="100%" cellpadding="4" cellspacing="1" style="background: #ccc">
<thead>
<tr>
<th style="background:#fff;padding:7px 0"></th>
<th style="background:#fff;padding:7px 0">Merchant</th>
<th style="background:#fff;padding:7px 0">Report Date</th>

<th style="background:#fff;padding:7px 0">Manager</th>
<th style="background:#fff;padding:7px 0">ASIN</th>

<th style="background:#fff;padding:7px 0">Estimated Excess</th>

<th style="background:#fff;padding:7px 0">Days of Supply</th>
<th style="background:#fff;padding:7px 0">Estimated total storage cost</th>
<th style="background:#fff;padding:7px 0">Recommended Removal Quantity</th>
<th style="background:#fff;padding:7px 0">Estimated cost savings of removal</th>
<th style="background:#fff;padding:7px 0">Alert</th>
</tr>
</thead>
@foreach($excessInventory as $ei) 
<tr align="center" >
<td style="background:#fff">
@if (!empty ( $ei->listing->product)) 
<?php $product = $ei->listing->product?>
<a title="{{$product->name}}" href="{{$product->amazonLink () }}" target="_blank" >@if($product->small_image)<img src="{{$product->small_image}}" style="max-width:100px;max-height:100px"/>@else {{$product->asin }} @endif</a></td>
@else
<a href="{{ Product::amzLink($ei->asin, $ei->country)}}">{{$ei->asin}} <br/>{{$ei->country}}</a>
@endif
<td style="background:#fff">{{$ei->merchant->name}}</td>
<td style="background:#fff">{{$ei->date}}</td>

<td  style="background:#fff">@if(isset($product)) {{implode(', ', $product->managerList())}} @endif</td>

<td style="background:#fff">{{$ei->sku}} <br/>{{$ei->asin}} {{$ei->country}}</td>


<td style="background:#fff">{{($ei->estimated_excess)}}</td>
<td style="background:#fff">{{($ei->days_of_supply)}}</td>
<td style="background:#fff">{{($ei->estimated_total_storage_cost)}}</td>
<td style="background:#fff">{{($ei->recommended_removal_quantity)}}</td>

<td style="background:#fff">{{($ei->estimated_cost_savings_of_removal)}}</td>
<td style="background:#fff">{{($ei->alert)}}</td>

</tr>
@endforeach
</table>
@endif
<br/>
<p>Best,</p>
@endsection