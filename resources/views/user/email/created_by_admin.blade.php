@extends('layouts.email')
@section('content')
<p>Dear {{$user->name}}</p>

<p>A new account on {{$site}} has been created. Below is your login information.</p>
<p>URL: {{$link}}<br/>
Username: {{$user->email}}<br/>
Password: {{$password}}<br/>
</p>
<br/>
<p>Best,</p>
@endsection