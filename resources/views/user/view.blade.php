@extends('layouts.master')
@section('title', "View ".$user->name)
@section('content')
<div class="btn-actions">
<a class="btn btn-default pull-right"  href="{{url()->previous()}}"><span class="" >Back</span></a>
</div>



<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#charts"><i class="fa fa-line-chart" aria-hidden="true"></i> Sales Report</a></li>
  <li><a data-toggle="tab" href="#products"><i class="fa " aria-hidden="true"></i>Products</a></li>
  <li><a data-toggle="tab" href="#info"><i class="fa " aria-hidden="true"></i>User Info</a></li>
 
  <li class="pull-right" style="padding-left:10px">@include('listing.parts.filter')</li>
  
 </ul>
 
<div class="tab-content">
	<div id="charts" class="tab-pane active">
		@include('listing.parts.boxes')
		@include('listing.parts.orders') 
	</div>
	
	<div id="products" class="tab-pane">
		@include('user.parts.products')
		
	</div>
	
	<div id="info" class="tab-pane">
		@include('user.parts.info')
		
	</div>
</div>	
	






@endsection