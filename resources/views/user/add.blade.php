@extends('layouts.master')
@section('title', "Add New User ")
@section('content')

<div class="btn-actions">
<a class="btn btn-default pull-right" onclick="return confirm('Are you sure to go back without saving?')" href="{{url()->previous()}}"><span class="" >Back</span></a>
</div>



<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add New User</div>
                <div class="panel-body">
                
                
					<div style="max-width:500px">
					
					{!! Form::model($user, ['route' => ['user.add', $user->id], 'class' => 'form-horizontal']) !!}
					
					 <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required minlength="8">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required minlength="8">
                            </div>
                        </div>
                        
					
					<div class="form-group row">
					    {{ Form::label("Active", null, ['class' => 'col-sm-4 control-label']) }}
					    <div class="col-sm-6">
					    {{ Form::select('verified', ['0' => 'No', '1' => 'Yes'], $user->verified,['class' => 'form-control','required']) }}
					    </div>
					</div>

                    <div class="form-group row">
                        {{ Form::label("Send Email Notification", null, ['class' => 'col-sm-4 control-label']) }}
                        <div class="col-sm-6">
                        {{ Form::select('email_notice', ['0' => 'No', '1' => 'Yes'], $user->email_notice,['class' => 'form-control','required']) }}
                        </div>
                    </div>
                    					
					<div class="form-group row">
					    {{ Form::label("Role", null, ['class' => 'col-sm-4 control-label']) }}
					    <div class="col-sm-6">
                            @foreach (config('app.roles') as $key=>$value)
                                <label class="checkbox-inline">
                                    {{ Form::checkbox('role[]', $value, false, ['class' => 'form-check-input'])}}
                                    {{  $key }}
                                </label>
                            @endforeach
					    </div>
					</div>
					
					
					<div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                    </div>
					
					{!! Form::close() !!}
					</div>
					
  				</div>
            </div>
        </div>
    </div>
</div>



@endsection