@extends('layouts.master')
@section('title', "Change Passwords")
@section('content')
@if($request->session()->has('alert'))
  <p class="text-danger">{{$request->session()->get('alert')}}</p>
@endif
@if($request->session()->has('success'))
  <p class="text-success">{{$request->session()->get('success')}}</p>
@endif

{{ Form::open(array('url' => route('user.do-change-password'), 'class' => 'form-horizontal', 'method' => 'POST')) }}
  <div class="form-group">
    <label for="currentPassword" class="col-sm-2 control-label">Current Password</label>
    <div class="col-sm-6">
      <input required minlength="8" name="currentPassword" type="password" class="form-control" id="currentPassword" placeholder="Current Password">
    </div>
  </div>
  <div class="form-group">
    <label for="newPassword" class="col-sm-2 control-label">New Password</label>
    <div class="col-sm-6">
      <input required minlength="8" name="newPassword" type="password" class="form-control" id="newPassword" placeholder="New Password">
    </div>
  </div>
  <div class="form-group">
    <label for="confirmNewPassword" class="col-sm-2 control-label">Confirm New Password</label>
    <div class="col-sm-6">
      <input required minlength="8" name="confirmNewPassword" type="password" class="form-control" id="confirmNewPassword" placeholder="Confirm New Password">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </div>
{{ Form::close() }}
@endsection