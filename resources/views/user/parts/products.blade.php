@if($user->products)

<h4>Products</h4>
<div class="table-responsive">
{!! $product_grid->render() !!}
</div>
@endif
