	
<div class="row placeholders">
    <div class="col-xs-6  col-sm-4  ">
		<table class="table table-striped" style="table-layout: fixed;"><tbody>
		<tr><th>Name</th><td class="text-right"> {{$user->name}}</td></tr>
		<tr><th>Email</th><td class="text-right"> {{$user->email}}</td></tr>
		<tr><th>Role</th><td class="text-right"> {{$user->role}}</td></tr>
		<tr><th>Last Login</th><td class="text-right">{{$user->last_login}}</td></tr>
		
		
		</tbody>
		</table>
	</div>
</div>