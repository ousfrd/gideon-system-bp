@extends('layouts.master')
@section('title', "Edit ".$user->name)
@section('content')
<div class="btn-actions">
<a class="btn btn-default pull-right" onclick="return confirm('Are you sure to go back without saving?')" href="{{url()->previous()}}"><span class="" >Back</span></a>
</div>

@include('user.form.edit')

@endsection