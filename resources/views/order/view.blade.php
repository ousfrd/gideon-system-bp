@extends('layouts.master')
@section('title', "View Order ".$order->AmazonOrderId)
@section('content')

<div class="btn-actions">
<button class="back-btn" onclick="window.location='{{url()->previous()}}'"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true">Back</span></button>
</div>


<div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Order Details</h4>
                                    </div>
                                    <div class="panel-body">
                                        <dl class="dl-horizontal margin-bottom-0">
                                            
                                            <dt>Merchant</dt>
                                            <dd>
                                               {{$order->account->name}} ({{$order->account->code}}) &nbsp;
                                            </dd>
                                            <dt>Amazon Order ID</dt>
                                            <dd>
											   {{$order->AmazonOrderId}}                                                &nbsp;
                                            </dd>
                                           
                                            <dt>Order Date</dt>
                                            <dd>
                                                {{\DT::convertUTCToTimezone($order->PurchaseDate,$order->account->timezone_name)}}                                                &nbsp;
                                            </dd>
                                            <dt>Status</dt>
                                            <dd>
                                                {{$order->OrderStatus}} / {{ $order->refundStatus()}}                                             &nbsp;
                                            </dd>
                                            <dt>Fulfill Channel</dt>
                                            <dd>
                                                {{$order->FulfillmentChannel}}                                               &nbsp;
                                            </dd>
                                            <dt>Sales Channel</dt>
                                            <dd>
                                                {{$order->SalesChannel}}                                               &nbsp;
                                            </dd>
                                            
                                           
                                            <dt>Last Updated on</dt>
                                            <dd>
                                               {{\DT::convertUTCToTimezone($order->LastUpdateDate,$order->account->timezone_name)}}                                                &nbsp;
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Shipping Details</h4>
                                    </div>
                                    <div class="panel-body">
                                        <dl class="dl-horizontal margin-bottom-0">
                                            <dt>Buyer</dt>
                                            <dd>
                                                @if ($order->shipment)
                                                    <a target="_blank" href="{{route('customers.show', ['id'=>$order->shipment->buyer_email])}}">{{$order->shipment->buyer_name}}</a>
                                                @else
                                                    {{""}}
                                                @endif
                                            </dd>
                                            <dt>Phone</dt>
                                            <dd>
                                                {{$order->ShippingAddressPhone}}
                                            </dd>
                                            <dt>Email</dt>
                                            <dd>
                                                @if ($order->shipment)
                                                    <a target="_blank" href="{{route('customers.show', ['id'=>$order->shipment->buyer_email])}}">{{$order->shipment->buyer_email}}</a>
                                                @else
                                                    {{""}}
                                                @endif
                                            </dd>

                                            <dt>Recipient</dt>
                                            <dd>
                                                {{$order->ShippingAddressName}}
                                            </dd>
                                            <dt>Address</dt>
                                            <dd>
                                               {!!$order->getShippingAddress(", ")!!}
                                            </dd>
                                            <dt>Country</dt>
                                            <dd>
                                               {{$order->ShippingAddressCountryCode}}
                                            </dd>
                                            <dt>Ship Date</dt>
                                            <dd>
                                                {{$order->shipment?$order->shipment->shipment_date:'...'}}  </dd>
                                            <dt>Est. Arrival Date</dt>
                                            <dd> {{$order->shipment?$order->shipment->estimated_arrival_date:'...'}}</dd>
                                            
                                            <dt>Carrier</dt>
                                            <dd> {{$order->shipment?$order->shipment->carrier:'...'}}</dd>
                                            <dt>Tracking No</dt>
                                            <dd>{{$order->shipment?$order->shipment->tracking_number:'...'}}</dd>
                                            @php
                                            if ($order->shipment) {
                                                $skip = $order->shipment->skip_review_claim;
                                            } else {
                                                $skip = FALSE;
                                            }
                                            @endphp
                                            <dt>Skip Review Claim</dt>
                                            <dd>{{ $skip ? 'YES' : 'NO' }}</dd>
                                            <dt>Skip Review Claim Note</dt>
                                            <dd>{{ $skip ? $order->shipment->note : '' }}</dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
<?php 
$totals = ['total_gross'=>0,'amazon_payout'=>0,'net_profit'=>0,'cost'=>0];
$currency = '';
?>                       
<div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Product Details</h4>
                                    </div>
                                    <div class="panel-body">
                                        <table class="table table-condensed table-bordered table-hover"><tbody><tr class="darkHeader"><th>Product</th><th class="text-center">Qty</th><th class="text-right">Amount</th></tr>
                                       
                                        @foreach($order->items as $item)
                                        <?php $currency = $item->Currency ? config("app.currencySymbol")[$item->Currency] : 'NA'; ?>
                                        <tr class="prodTitle">
                                        <td>{{$item->Title}} <br>
                                        ASIN: <a href="{{route('product.view',[$item->ASIN])}}">{{$item->ASIN}}</a>, SKU: <a href="{{route('listing.view',[$item->SellerSKU,$item->country])}}">{{$item->SellerSKU}}</a>
                                        
                                        </td>
                                        <td class="text-center">{{$item->QuantityOrdered}}</td>
                                        <td class="text-right">{{$currency}}{{$item->ItemPrice}}</td></tr>
                                        <tr><td>Sales Tax</td><td class="text-center"></td><td class="text-right ">{{$currency}}{{$item->tax}}</td></tr>
                                        @if($item->ShippingPrice>0)
                                        <tr><td>Shipping</td><td class="text-center ">1</td><td class="text-right ">{{$currency}}{{$item->ShippingPrice}}</td></tr>
                                        @endif
                                        @if($item->shipment && $item->shipment->ship_promotion_discount + $item->shipment->item_promotion_discount < 0)
                                        <tr><td>Promo Discount</td><td class="text-center ">1</td><td class="text-right  text-danger">({{$currency}}{{number_format(-($item->shipment->ship_promotion_discount + $item->shipment->item_promotion_discount)* $item->shipment->exchange_rate,2)}})</td></tr>
                                        @endif
                                        <tr class="doubleBorder">
                                            <td class="text-right"><strong>Customer Total Paid(including Tax)</strong></td>
                                            <td class="text-right" colspan="2"><strong>{{$currency}}{{$item->totalPaidIncludingTax()}}</strong></td>
                                        </tr>
                                        <tr class="doubleBorder"><td class="text-right"><strong>Customer Paid To Merchant</strong></td>
                                        <td class="text-right" colspan="2"><strong>{{$currency}}{{$item->totalPaid()}}</strong></td>
                                        </tr>
                                        
                                        
                                        
										@if($order->FulfillmentChannel == 'AFN')
                                        <tr><td>FBA  Fee</td><td class="text-center ">{{$item->QuantityOrdered}}</td>
                                        <td class="text-right  text-danger">({{$currency}}{{$item->fbaFee()}})</td></tr>
                                        
                                        @endif
                                        <tr><td>Commission</td><td class="text-center ">{{$item->QuantityOrdered}}</td>
                                        <td class="text-right  text-danger">({{$currency}}{{$item->amzCom()}})</td></tr>
                                        
                                        
                                        @if($item->shippingChargeBack() != 0)
                                        <tr><td>Shipping Chargeback</td><td class="text-center ">1</td><td class="text-right  text-danger">({{$currency}}{{$item->shippingChargeBack()}})</td></tr>
                                        @endif
                                        
                                        <tr class="doubleBorder doubleTotal"><td class="text-right"><strong>Amazon Fees</strong></td>
                                        <td class="text-right text-danger" colspan="2"><strong>({{$currency}}{{$item->amzTotalFee()}})</strong></td></tr>
                                        
                                        

                                       	<tr class="doubleBorder doubleTotal"><td class="text-right"><strong>Total Refunded</strong></td>
                                        <td class="text-right text-danger" colspan="2"><strong>({{$currency}}{{$item->total_refund}})</strong></td></tr>

                                       	
                                       	
                                        <tr class="totalRow"><td class="text-right"><strong>Amazon Payout</strong></td><td class="text-right" colspan="2"><strong>${{$item->totalPayout()}} <?php $totals['amazon_payout'] += $item->totalPayout();?></strong></td></tr>
                                      	<tr><td>Product Cost</td><td class="text-center ">{{$item->QuantityOrdered}}</td><td class="text-right  text-danger">({{$currency}}{{$item->product->cost * $item->QuantityOrdered}})</td></tr>
                                       
                                       
                                      
                                        <?php $totals['cost'] += $item->totalCost();?>
                                        <tr class="doubleBorder doubleTotal"><td class="text-right"><strong>Costs &amp; Expenses</strong></td>
                                        <td class="text-right text-danger" colspan="2"><strong>({{$currency}}{{$item->totalCost()}})</strong></td></tr>
                                        
                                       	
                                        <?php $totals['total_gross'] += $item->totalMerchantGross();?>
                                        <tr class="totalRow"><td class="text-right"><strong>Product Net Profit</strong></td><td class="text-right" colspan="2"><strong>{{$currency}}{{$item->netProfit()}} <?php $totals['net_profit'] += $item->netProfit();?></strong></td></tr>
<!--                                         <tr class="totalRow"><td class="text-right"><strong>Product ROI</strong></td><td class="text-right" colspan="2"><strong>259.48%</strong></td></tr> -->
                                        
                                        
                                        @endforeach
                                      	
                                      	
                                      	<tr class="grandTotalRow"><td class="text-right"><strong>Total Amazon Payout</strong></td><td class="text-right" colspan="2"><strong>{{$currency}}{{number_format($totals['amazon_payout'],2)}}</strong></td></tr>
                                      	<tr class="grandTotalRow"><td class="text-right"><strong>Total Net Profit</strong></td><td class="text-right" colspan="2"><strong>{{$currency}}{{$totals['net_profit']}}</strong></td></tr>
                                      	<tr class="grandTotalRow greenRow"><td class="text-right"><strong>Total ROI</strong></td><td class="text-right" colspan="2"><strong>
                                        @if($totals['cost'] > 0)
                                        {{number_format(100*$totals['net_profit']/$totals['cost'],0)}}%
                                        @else
                                        N/A
                                        @endif
                                        </strong></td></tr>
                                      	<tr class="grandTotalRow greenRow"><td class="text-right"><strong>Total Margin</strong></td><td class="text-right" colspan="2"><strong>
                                        @if($totals['total_gross'] > 0)
                                        {{number_format(100*$totals['net_profit']/$totals['total_gross'],0)}}%
                                        @else
                                        N/A
                                        @endif    
                                        </strong></td></tr></tbody></table>                                    </div>
                                </div>
                            </div>
                        </div>

@endsection