@extends('layouts.master')
@section('title', "Manage Orders")
@section('content')
{!! $grid !!}

<script type="text/javascript">
$(document).ready(function() {
    $('.pUpdate').editable({
        //showbuttons:false,
        //onblur:'submit',
        savenochange:false,
        type: 'text',
        url:'{{route('order.ajaxsave')}}',  
        //title: 'Enter value',
        placement: 'top', 
        send:'auto',
        ajaxOptions: {
            dataType: 'json'
        },
        success: function(response, newValue) {
        } 
    })

    // order status update handler start
    // var selectValues;
    // $('.multiselect-native-select .btn-group').on('hidden.bs.dropdown', function () {
    //     var currentValues = $("select[name='Orders[filters][OrderStatus-in][]']").val();
    //     if (JSON.stringify(selectValues)!=JSON.stringify(currentValues)) {
    //         $(this).closest('form').submit();
    //     }
    // }).on('show.bs.dropdown', function() {
    //     selectValues = $("select[name='Orders[filters][OrderStatus-in][]']").val();
    // });
    // order status update handler end
})

</script>
@endsection