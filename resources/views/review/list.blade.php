@extends('layouts.master')
@section('title', "Review Costs")
@section('content')

<div class="btn-actions">
</div>

<div class="table-responsive">
{!! $grid->render() !!}
</div>


<script type="text/javascript">

$(document).ready(function(){
//edit form style - popup or inline
//$.fn.editable.defaults.mode = 'inline';



$('.pUpdate').editable({
    validate: function(value) {
        if($.trim(value) == '') {
            return 'Value is required.';
        }
	},
	//showbuttons:false,
	//onblur:'submit',
	savenochange:false,
    type: 'text',
    url:'{{route('review.ajaxsave')}}',  
    //title: 'Enter value',
    placement: 'top', 
    send:'auto',
    ajaxOptions: {
   		 dataType: 'json'
    },
    success: function(response, newValue) {
    } 
	})

	$('.pUpdate.unit-cost').on('shown', function(e, editable) {
		var checkbox = $('<div class="input form-group checkbox-success checkbox" style="margin-top: 5px;"><input name="isUnitUpdateOrders" value="1" class="UpdateALLOrders" id="isunitupdateorders" type="checkbox"><label for="isunitupdateorders">Update ALL Records</label></div>');
		
	    editable.input.$input.parents('form').find('.control-group.form-group').append(checkbox);
	})
	
	$('.pUpdate.unit-cost').editable('option', 'params', function (params) {
		
	    var $isUnitUpdateOrders = $('#isunitupdateorders');
	
	    if($isUnitUpdateOrders.is(':checked')) {
	        params.isUnitUpdateOrders = 1;
	    }
	
	    return params;
	});





	 
})

//<div class="input form-group checkbox-success checkbox" style="margin-top: 5px;"><input name="isUnitUpdateOrders" value="1" class="UpdateALLOrders" id="isunitupdateorders" type="checkbox"><label for="isunitupdateorders">Update ALL Orders</label></div>
</script>
@endsection