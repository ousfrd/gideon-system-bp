@extends('layouts.master')
@section('title', "Edit Owned Buyer " . $ownedBuyer->nickname)
@section('content')
<ol class="breadcrumb">
  <li><a href="{{route('owned-buyer.index')}}">Owned Buyer List</a></li>
  <li><a href="{{route('owned-buyer.show', $ownedBuyer)}}">{{$ownedBuyer->nickname}}</a></li>
  <li class="active">edit</li>
</ol>
{{ Form::model($ownedBuyer, ['route' => array('owned-buyer.update', $ownedBuyer), 'method'=> 'put']) }}
  <div class="row">
    <div class="col-xs-12 col-md-6">
      <h4>Buyer Info</h4>
      <table class="table table-striped">
        <tbody>
          <tr>
            <td>Nickname</td>
            <td><input class="form-control" type="text" name="nickname" value="{{$ownedBuyer->nickname}}"></td>
          </tr>
          <tr>
            <td>Device</td>
            <td><input class="form-control" type="text" name="device" value="{{$ownedBuyer->device_name}}"></td>
          </tr>
          <tr>
            <td>Browser</td>
            <td><input class="form-control" type="text" name="browser" value="{{$ownedBuyer->browser}}"></td>
          </tr>
          <tr>
            <td>Buyer Name</td>
            <td><input class="form-control" type="text" name="name" value="{{$ownedBuyer->name}}"></td>
          </tr>
          <tr>
            <td>Email</td>
            <td><input class="form-control" type="email" name="email" value="{{$ownedBuyer->email}}"></td>
          </tr>
          <tr>
            <td>Phone</td>
            <td><input class="form-control" type="text" name="phone" value="{{$ownedBuyer->phone}}"></td>
          </tr>
          <tr>
            <td>Password</td>
            <td><input class="form-control" type="text" name="password" value="{{$ownedBuyer->password}}"></td>
          </tr>
          <tr>
            <td>Status</td>
            <td>{{ Form::select('status', array('0' => 'Inactive', '1' => 'Active'), $ownedBuyer->status, ['class' => 'form-control']) }}
            </td>
          </tr>
          <tr>
            <td>Auxiliary Email</td>
            <td><input class="form-control" type="email" name="auxiliary_email" value="{{$ownedBuyer->auxiliary_email}}"></td>
          </tr>
          <tr>
            <td>Prime</td>
            <td>{{$ownedBuyer->is_prime ? "Yes" : "No"}}</td>
          </tr>
          <tr>
            <td>Prime Purchased At</td>
            <td><input class="form-control date" type="text" name="prime_purchased_at" value="{{$ownedBuyer->prime_purchased_at}}" placeholder="leave it blank if not apply"></td>
          </tr>
          <tr>
            <td>Prime Expired At</td>
            <td><input class="form-control date" type="text" name="prime_end_at" value="{{$ownedBuyer->prime_end_at}}" placeholder="leave it blank if not apply"></td>
          </tr>
          <tr>
            <td>Created By / At</td>
            <td>{{$ownedBuyer->user->name}} / {{$ownedBuyer->created_at}}</td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="col-xs-12 col-md-6">
      <h4>Shipping Address</h4>
      @if ($shippingAddress)
      <table class="table table-striped">
        <tr>
          <td>Address 1</td>
          <td>{{$shippingAddress->ship_address_1}}</td>
        </tr>
        <tr>
          <td>Address 2</td>
          <td>{{$shippingAddress->ship_address_2}}</td>
        </tr>
        <tr>
          <td>Address 3</td>
          <td>{{$shippingAddress->ship_address_3}}</td>
        </tr>
        <tr>
          <td>City, State, Zip</td>
          <td>{{$shippingAddress->ship_city}},{{$shippingAddress->ship_state}}, {{$shippingAddress->ship_postal_code}}</td>
        </tr>
        <tr>
          <td>Country</td>
          <td>{{$shippingAddress->ship_country}}</td>
        </tr>
        <tr>
          <td>Phone</td>
          <td>{{$shippingAddress->shipping_phone}}</td>
        </tr>
      </table>
      @endif
      <hr>
      <h4>Payment Method</h4>
      @if ($paymentMethod)
        <table class="table table-striped">
          <tr>
            <td>Card Number</td>
            <td><a class="editable" href="#" id="card_number" data-type="text" data-pk="{{$paymentMethod->id}}" data-url="{{route('owned-buyer.update-payment-method', ['id' => $ownedBuyer->id])}}" data-title="Enter Card Number">{{$paymentMethod->card_number}}</a></td>
          </tr>
          <tr>
            <td>Bank Name</td>
            <td>{{$paymentMethod->bank_name}}</td>
          </tr>
          <tr>
            <td>Status</td>
            <td>{{$paymentMethod->status}}</td>
          </tr>
          <tr>
            <td>Available Balance</td>
            <td>{{$paymentMethod->available_balance}}</td>
          </tr>
          <tr>
            <td>Total Spend</td>
            <td>{{$paymentMethod->total_spent}}</td>
          </tr>
          <tr>
            <td>Last Added Credit</td>
            <td>{{$paymentMethod->add_credit}}</td>
          </tr>
          <tr>
            <td>Last Added Credit At</td>
            <td>{{$paymentMethod->last_add_credit_at}}</td>
          </tr>
          <tr>
            <td>Last Added Credit By</td>
            <td>{{$paymentMethod->last_add_credit_by}}</td>
          </tr>
        </table>
      @else
      <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add-payment">Add<i class="fa fa-plus"></i></button>
      @endif
    </div>

    
  </div>
  <div class="row">
    <button class="btn btn-primary center-block btn-lg">Submit</button>
  </div>
  {{ Form::close() }}

  @include('owned-buyer._new-payment-modal', ['ownedBuyer' => $ownedBuyer])

<script>
  $(document).ready(function() {

    $('input.date').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      autoUpdateInput: false,
      locale: {
        format: "YYYY-MM-DD"
      }
    }, function(start, end, label) {
      this.element.val(start.format('YYYY-MM-DD'));
    });

    $('.editable').editable();
  
});
</script>

@endsection