@extends('layouts.master')
@section('title', "Owned Buyer " . $ownedBuyer->nickname)
@section('content')
<ol class="breadcrumb">
  <li><a href="{{route('owned-buyer.index')}}">Owned Buyer List</a></li>
  <li class="active">{{$ownedBuyer->nickname}}</li>
</ol>

<div class="btn-actions">
  <a href="{{route('owned-buyer.edit', $ownedBuyer)}}" class="btn btn-primary pull-right"><span class="" >Edit</span></a>
</div>
@if(Session::has('message.success'))
  <p class="text-success">{{Session::get('message.success')}}</p>
@endif
@if(Session::has('message.alert'))
  <p class="text-danger">{{Session::get('message.alert')}}</p>
@endif
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-warning">
    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#add-to-cart">
      Add To Cart Form
    </div>
    <div class="panel-body collapse" id="add-to-cart">
      {{ Form::open(array('route'=> array('owned-buyer-cart-item.store'), 'method'=>'post', 'class'=>'form-inline')) }}
        <div class="form-group">
          <input class="form-control" type="text" name="asin" placeholder="ASIN" />
        </div>
        <input type="text" class="hidden" name="owned_buyer_id" value="{{$ownedBuyer->id}}">
        <input type="text" class="hidden" name="redirect_to" value="{{route('owned-buyer.show', ['ownedBuyer' => $ownedBuyer])}}">
        <button class="btn btn-primary">Add To Cart</button>
      {{ Form::close() }}
    </div>
  </div>

  <div class="panel panel-success">
    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#add-order">
      Add Order Form
    </div>
    <div class="panel-body collapse" id="add-order">
      {{ Form::open(array('route'=> array('managed-review-order.store'), 'method'=>'post', 'class'=>'form-inline')) }}
        <input type="text" class="hidden" name="owned_buyer_id" value="{{$ownedBuyer->id}}">
        <div class="form-group">
          <select required name="order_type" id="order_type" class="form-control">
            <option value="">Select Order Type</option>
            @foreach($orderTypes as $type => $text)
              <option value="{{$type}}">{{$text}}</option>
            @endforeach
          </select>
        </div>
        <div id="self-order" class="form-group">
          <div class="form-group">
            <input required pattern="\d{3}-\d{7}-\d{7}" class="form-control" type="text" name="amazon_order_id" placeholder="Amazon Order Id" title="amazon order should look like xxx-xxxxxxx-xxxxxxx"/>
          </div>
          <div class="form-group">
            <input required type="text" class="form-control" name="order_date">
          </div>
          <div class="form-group">
            <input type="number" step="0.01" name="order_cost" class="form-control" placeholder="Order Cost" id="order_cost">
          </div>
          <div class="checkbox">
            <label for=""><input type="checkbox" name="need_review" value="1"> Need Review?</label>
          </div>
        </div>
        <div id="other-order" class="form-group">
          <div class="form-group">
            <input required type="text" class="form-control" name="order_date">
          </div>
          <div class="form-group">
            <input type="number" step="0.01" name="order_cost" class="form-control" placeholder="Order Cost" id="order_cost">
          </div>
        </div>
        <button id="submit-btn" class="btn btn-primary">Add Order</button>
      {{ Form::close() }}
    </div>
  </div>
</div>

  <div class="row">
    <div class="col-xs-12 col-md-5">
      <h4>Buyer Info</h4>
      <table class="table table-striped">
        <tbody>
          <tr>
            <td>Nickname</td>
            <td>{{$ownedBuyer->nickname}}</td>
          </tr>
          <tr>
            <td>Device</td>
            <td>{{$ownedBuyer->device_name}}</td>
          </tr>
          <tr>
            <td>Browser</td>
            <td>{{$ownedBuyer->browser}}</td>
          </tr>
          <tr>
            <td>Buyer Name</td>
            <td>{{$ownedBuyer->name}}</td>
          </tr>
          <tr>
            <td>Email / Phone</td>
            <td>{{$ownedBuyer->email}}</td>
          </tr>
          <tr>
            <td>Password</td>
            <td>{{$ownedBuyer->password}}</td>
          </tr>
          <tr>
            <td>Status</td>
            <td>{{$ownedBuyer->status ? "Active" : "Inactive"}}</td>
          </tr>
          <tr>
            <td>Auxiliary Email</td>
            <td>{{$ownedBuyer->auxiliary_email}}</td>
          </tr>
          <tr>
            <td>Prime</td>
            <td>{{$ownedBuyer->is_prime ? "Yes" : "No"}}</td>
          </tr>
          <tr>
            <td>Prime Purchased At</td>
            <td>{{$ownedBuyer->prime_purchased_at}}</td>
          </tr>
          <tr>
            <td>Prime Expired At</td>
            <td>{{$ownedBuyer->prime_end_at}}</td>
          </tr>
          <tr>
            <td>Created By / At</td>
            <td>{{$ownedBuyer->user->name}} / {{$ownedBuyer->created_at}}</td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="col-xs-12 col-md-4">
      <h4>Activity Summary</h4>
      <table class="table table-striped">
        <tr>
          <td>Total Orders</td>
          <td>{{$ownedBuyer->total_orders}}</td>
        </tr>
        <tr>
          <td>Own Seller Orders</td>
          <td>{{$ownedBuyer->own_orders}}</td>
        </tr>
        <tr>
          <td>Other Seller Orders</td>
          <td>{{$ownedBuyer->other_orders}}</td>
        </tr>
        <tr>
          <td>Own/Other Seller Orders Rate</td>
          <td>{{$ownedBuyer->own_other_orders_rate}}</td>
        </tr>
        <tr>
          <td>Leave Review Orders</td>
          <td>{{$ownedBuyer->reviewed_orders}}</td>
        </tr>
        <tr>
          <td>Review Rate</td>
          <td>{{$ownedBuyer->review_rate}}</td>
        </tr>
        <tr>
          <td>Last Order At</td>
          <td>{{$ownedBuyer->last_placed_order_at}}</td>
        </tr>
        <tr>
          <td>Last Left Review At</td>
          <td>{{$ownedBuyer->last_left_review_at}}</td>
        </tr>
      </table>
      <hr>
      <h4>Shipping Address</h4>
      @if ($shippingAddress)
      <table class="table table-striped">
        <tr>
          <td>Address 1</td>
          <td>{{$shippingAddress->ship_address_1}}</td>
        </tr>
        <tr>
          <td>Address 2</td>
          <td>{{$shippingAddress->ship_address_2}}</td>
        </tr>
        <tr>
          <td>Address 3</td>
          <td>{{$shippingAddress->ship_address_3}}</td>
        </tr>
        <tr>
          <td>City, State, Zip</td>
          <td>{{$shippingAddress->ship_city}},{{$shippingAddress->ship_state}}, {{$shippingAddress->ship_postal_code}}</td>
        </tr>
        <tr>
          <td>Country</td>
          <td>{{$shippingAddress->ship_country}}</td>
        </tr>
        <tr>
          <td>Phone</td>
          <td>{{$shippingAddress->shipping_phone}}</td>
        </tr>
      </table>
      @endif
    </div>

    <div class="col-xs-12 col-md-3">
      <h4>Payment Method</h4>
      @if ($paymentMethod)
      <table class="table table-striped">
        <tr>
          <td>Card Number</td>
          <td>{{$paymentMethod->card_number}}</td>
        </tr>
        <tr>
          <td>Bank Name</td>
          <td>{{$paymentMethod->bank_name}}</td>
        </tr>
        <tr>
          <td>Status</td>
          <td>{{$paymentMethod->status}}</td>
        </tr>
        <tr>
          <td>Available Balance</td>
          <td>{{$paymentMethod->available_balance}}</td>
        </tr>
        <tr>
          <td>Total Spend</td>
          <td>{{$paymentMethod->total_spent}}</td>
        </tr>
        <tr>
          <td>Last Added Credit</td>
          <td>{{$paymentMethod->add_credit}}</td>
        </tr>
        <tr>
          <td>Last Added Credit At</td>
          <td>{{$paymentMethod->last_add_credit_at}}</td>
        </tr>
        <tr>
          <td>Last Added Credit By</td>
          <td>{{$paymentMethod->last_add_credit_by}}</td>
        </tr>
      </table>
      @else
        <button class="btn btn-success" data-toggle="modal" data-target="#add-payment">Add<i class="fa fa-plus"></i></button>
      @endif
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">
      Added To Shopping Cart Items
    </div>
    <div class="panel-body">
      @if (count($addedToCartItems) > 0)
        <table class="table">
          <thead>
            <tr>
              <th>ASIN</th>
              <th>Add To Cart At</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($addedToCartItems as $cartItem)
            <tr>
              <td>{{$cartItem->asin}}</td>
              <td>{{$cartItem->created_at}}</td>
              <td><button class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete" data-url="{{route('owned-buyer-cart-item.destroy', ['ownedBuyerCartItem'=>$cartItem]) }}" data-key-name="ASIN" data-key-value="{{$cartItem->asin}}">Remove</button></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      @else
        <p>No Added To Cart Item</p>
      @endif
    </div>
    
  </div>

  {!! $orderGrid !!}
  @include('parts.delete-confirmation')

  @include('owned-buyer._new-payment-modal', ['ownedBuyer' => $ownedBuyer])


<script>
  $(document).ready(function() {
    $('input[name="order_date"]').daterangepicker({
      singleDatePicker: true,
      maxDate: moment(),
      locale:{
        format:'YYYY-MM-DD'
      },
    }, ((start, end, label) => {
    }));

    $('.pUpdate').editable({
      ajaxOptions: {
        type: 'put'
      }
    });

    let selfOrder = $('#self-order').detach();
    let otherOrder = $('#other-order').detach();

    $('#order_type').on('change', function(){
      console.log('order type change');
      const value = $(this).val();
      console.log(value);
      if (value == "OWNED_BUYER_SELF_ORDER") {
        selfOrder.insertBefore('#submit-btn');
        if ($.contains(document, otherOrder[0])) {
          otherOrder = $('#other-order').detach();
        }
      } else if (value == "OWNED_BUYER_OTHER_ORDER") {
        otherOrder.insertBefore('#submit-btn');
        if ($.contains(document, selfOrder[0])) {
          selfOrder = $('#self-order').detach();
        }
      } else {
        if ($.contains(document, otherOrder[0])) {
          otherOrder = $('#other-order').detach();
        }
        if ($.contains(document, selfOrder[0])) {
          selfOrder = $('#self-order').detach();
        }
      }
    })

  })

</script>
@endsection