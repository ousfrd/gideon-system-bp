@extends('layouts.master')
@if ($asin)
  @section('title', "Choose Eligible Buyers For " . $asin)
@else
  @section('title', "Choose Eligible Buyers")
@endif
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">Find Eligible Buyers For ASIN</div>
    <div class="panel-body">
      {{ Form::open(array('method' => 'post', 'url' => route('owned_buyer.do-find-eligible-buyers'), 'class'=> ['form-inline'])) }}
      <div class="form-group">
        <label for="">ASIN</label>
        <input required type="text" name="asin" class="form-control" placeHolder="ASIN" value="{{$asin}}">
      </div>
      <div class="form-group">
        <label for="">Last Order Before</label>
        <input type="text" name="lastOrderBefore" class="form-control date" placeHolder="Last Order At" value="{{$lastOrderBefore}}">
      </div>
      <div class="form-group">
        <label for="">Last Review Before </label>
        <input type="text" name="lastReviewBefore" class="form-control date" placeHolder="Last Review At" value="{{$lastReviewBefore}}">
      </div>
      <div class="form-group">
        <label for="">Review Rate <=</label>
        <input required type="number" step="0.01" max="1.00" min="0.00" name="reviewRateLessThan" class="form-control" value="{{$reviewRateLessThan}}">
      </div>
      <button class="btn btn-primary">Search</button>
      {{ Form::close() }}
    </div>
  </div>
  @if ($grid)
    {!! $grid !!}
  @endif

<script>
  $(document).ready(function() {

    $('input.date').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        format: "YYYY-MM-DD"
      },
      maxDate: new Date()
    }, function(start, end, label) {
    }
    );
  
});
</script>
@endsection