@extends('layouts.master')
@section('title', "Owned Buyers")
@section('content')
<!-- <div class="btn-actions">
  <a href="{{route('owned-buyer.create')}}" class="btn btn-primary pull-right"><span class="" >Create Owned Buyer</span></a>
</div> -->
<ul class="nav nav-tabs" role="tablist">
  <li role="presentation" class="{{$tab == 'all' ? 'active' : ''}}"><a href="{{route('owned-buyer.index')}}">All</a></li>
  <li role="presentation" class="{{$tab == 'prime' ? 'active' : ''}}"><a href="{{route('owned-buyer.prime-buyers')}}">Prime Buyers</a></li>
  <li role="presentation" class="{{$tab == 'new' ? 'active' : ''}}"><a href="{{route('owned-buyer.new-buyers')}}">New Buyers</a></li>
</ul>
<div class="panel panel-default" id="file-upload">
  <div class="panel-heading">Import Buyer Info CSV File</div>
  <div class="panel-body">
    <p> CSV File columns </p>
    <div style="overflow: scroll">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Account Status (0:inactive, 1:active)</th>
            <th>Account Nickname</th>
            <th>Country</th>
            <th>Device Name</th>
            <th>Browser</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Password</th>
            <th>Auxiliary Email</th>
            <th>Shipping Address 1</th>
            <th>Shipping Address 2</th>
            <th>Shipping Address 3</th>
            <th>Shipping City</th>
            <th>Shipping State</th>
            <th>Shipping Zip</th>
            <th>Shipping Country</th>
            <th>Shipping Phone</th>
            <th>Payment Card Number</th>
            <th>Available Balance</th>
            <th>Prime Expired Date(YYYY-MM-DD)</th>
          </tr>
        </thead>
      </table>
    </div>
    @if($request->session()->has('success'))
      <p class="text-success">{{$request->session()->get('success')}}</p>
    @endif
    {{ Form::open(array('route' => array('owned_buyer.import'), 'method' => 'post', 'class'=> ['form-inline'], 'files' => true)) }}
    <div class="form-group">
      <input type="file" class="form-control" name="buyer_csv" accept=".csv">
      <button class="btn btn-primary">Upload</button>
    </div>
    {{ Form::close() }}
  </div>
</div>
<div class="nayjest-grid">
  {!! $grid !!}
</div>

@include('parts.delete-confirmation')
@endsection