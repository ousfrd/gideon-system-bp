<!-- Modal -->
<div class="modal fade" id="add-payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Payment Method</h4>
      </div>
      <div class="modal-body">
        {{ Form::open(['url'=> route('owned-buyer.add-payment-method', ['id'=>$ownedBuyer->id]), 'method'=>'post']) }}
        <div class="form-group">
          <label for="">Bank Name</label>
          <input type="text" name="bank_name" class="form-control">
        </div>
        <div class="form-group">
          <label for="">Card Number *</label>
          <input required type="text" name="card_number" class="form-control">
        </div>
        <div class="form-group">
          <label for="">Available Balance</label>
          <input type="number" min="0.0"  name="available_balance" class="form-control" value="100.00">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Add</button>
        </div>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>