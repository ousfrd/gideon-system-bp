@extends('layouts.master')
@section('title', "Divvy Transaction Records")
@section('content')
<div class="panel panel-default" id="file-upload">
  <div class="panel-heading">Import Divvy Transaction CSV File</div>
  <div class="panel-body">
    @if($request->session()->has('success'))
      <p class="text-success">{{$request->session()->get('success')}}</p>
    @endif
    {{ Form::open(array('route' => array('divvy-transaction-record.import'), 'method' => 'post', 'class'=> ['form-inline'], 'files' => true)) }}
    <div class="form-group">
      <p>Export cleared transactions as -> CSV - New (Clean Merchants Added)</p>
      <input type="file" class="form-control" name="divvy_transaction_csv" accept=".csv">
      <button class="btn btn-primary">Upload</button>
    </div>
    {{ Form::close() }}
  </div>
</div>
{!! $grid !!}
@endsection