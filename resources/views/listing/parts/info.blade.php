<div class="mb20"></div>
<h4>Listing Basic Information</h4>
<div class="row">
        <div class="col-xs-6  col-sm-4  ">
          <table class="table table-striped" style="table-layout: fixed;">
            <tbody>
              <tr>
                <th>ASIN</th>
                <td class="text-right"><a target="_blank" href="{{$listing->amzLink()}}">{{$listing->asin}}</a></td>
              </tr>
              <tr>
                <th>SKU</th>
                <td class="text-right"><a href="{{route('listing.view',[$listing->id])}}">{{$listing->sku}}</a></td>
              </tr>
              
              <tr>
                <th>Title</th>
                <td class="text-right">{{$listing->product->name}}</td>
              </tr>
              
              <tr>
                <th>Weight</th>
                <td class="text-right">{{$listing->product->weight}}</td>
              </tr>
              
              <tr>
                <th>Size</th>
                <td class="text-right">{{$listing->product->size()}}</td>
              </tr>
              
              
               <tr>
                <th>Peak Season Mode</th>
                <td class="text-right"> @if($listing->product->peak_season_mode) Yes @else No @endif</td>
              </tr>
           	
           	 <tr>
                <th>Discontinued</th>
                <td class="text-right"><a    data-type="select"  data-value="{{$listing->status==1?'No':'Yes'}}" data-source='[{value: 0, text: "No"}, {value: 1, text: "Yes"}]' data-pk="discontinued-{{$listing->id}}" data-url="{{route('listing.ajaxsave')}}">{{$listing->status==1?'No':'Yes'}}</a></td>
               </tr>
              
           	  <tr>
           	  	<th>Manager</th>
           	  	<td class="text-right"><a href="#" @if(auth()->user()->can('manage-product-manager')) data-type="select" class="pmanagers"  data-pk="manager-{{$listing->product->id}}" data-title="" @endif>{{implode(', ', $managers)}}</a></td>
           	  </tr>
             
        </tbody>
        </table>
       </div>
       
       <div class="col-xs-6  col-sm-4  ">

          <table class="table table-striped" style="table-layout: fixed;">
            <tbody>

              <tr>
                <th>Daily Average Orders</th>
                <td class="text-right">{{$listing->dailyAverageOrders()}}</td>
              </tr>
              
              <tr>
                <th>ManufactureDays</th>
                <td class="text-right">
                Normal: {{$listing->product->manufacture_days}} days<br>
                Peak Season: {{$listing->product->ps_manufacture_days}} days</td>
              </tr>
              
              <tr>
                <th>ToAMZShippingDays</th>
                <td class="text-right">
                Normal: {{$listing->product->cn_to_amz_shipping_days}} days<br>
                Peak Season:{{$listing->product->ps_cn_to_amz_shipping_days}} days
                </td>
              </tr>
              
              <tr>
                <th>Cost Per Unit</th>
                <td class="text-right">{{$listing->product->cost}}</td>
              </tr>
              
              <tr>
                <th>Shipping Cost Per Unit</th>
                <td class="text-right">{{$listing->product->shipping_cost}}</td>
              </tr>
             
              <tr>
                <th>Replenishment Cycle</th>
                <td class="text-right">{{$listing->totalDayToReinstock()}} days</td>
              </tr>
               
               <tr>
                <th>Est. Stock Days</th>
                <td class="text-right">{{$listing->daysToOutofStock()}} days</td>
              </tr>
               <tr>
                <th>Replenishment Needed</th>
                <td class="text-right">{!! ($listing->replenishNeeded() ? '<span class="text-danger alert-qty">Yes</span>':'<span  class="text-success">No</span>') !!}</td>
              </tr>
               <tr>
                <th>Days to Replenish</th>
                <td class="text-right">
                <span class="@if($listing->daysToReplenish() > 7) text-success @elif($listing->daysToReplenish() > 1) text-warning @else text-danger @endif">
                {{ $listing->daysToReplenish()}} days</span>
                
                <br/>
                
                
                </td>
              </tr> 
              
             
             
        </tbody>
        </table>
       </div>
       
       <div class="col-xs-6  col-sm-4  ">
          <table class="table table-striped" style="table-layout: fixed;">
            <tbody>
              <tr>
                <th>Fulfillment Channel</th>
                <td class="text-right">{{$listing->fulfillment_channel}}</td>
              </tr>
           
											
              <tr>
                <th>Fulfillable Qty</th>
                <td class="text-right">{{$listing->inventory?$listing->inventory->fulfillable_quantity:0}}</td>
              </tr>
              
              <tr>
                <th>Unsellable Qty</th>
                <td class="text-right">{{$listing->inventory?$listing->inventory->unsellable_quantity:0}}</td>
              </tr>
              
              <tr>
                <th>Reserved Qty</th>
                <td class="text-right">{{$listing->inventory?$listing->inventory->reserved_quantity:0}}</td>
              </tr>
              
              <tr>
                <th>Warehouse Total Qty</th>
                <td class="text-right">{{$listing->inventory?$listing->inventory->warehouse_quantity:0}}</td>
              </tr>
              
              <tr>
                <th>Inbound Working Qty</th>
                <td class="text-right">{{$listing->inventory?$listing->inventory->inbound_working_quantity:0}}</td>
              </tr>
              
              <tr>
                <th>Inbound Shipped Qty</th>
                <td class="text-right">{{$listing->inventory?$listing->inventory->inbound_shipped_quantity:0}}</td>
              </tr>
              
              <tr>
                <th>Inbound Receiving Qty</th>
                <td class="text-right">{{$listing->inventory?$listing->inventory->inbound_receiving_quantity:0}}</td>
              </tr>
              @if($listing->inventory)
              @if($listing->inventory && $listing->inventory->inbound_working_quantity>0 || $listing->inventory->inbound_shipped_quantity>0 || $listing->inventory->inbound_receiving_quantity > 0) 
				 <tr>
                <th>Total Qty</th>
                <td class="text-right">{{$listing->inventory->total_quantity}}</th>
              </tr>							
			  @endif
			  @endif
			  
			   <tr>
                <th>Safe Stock</th>
                <td class="text-right">{{$listing->safeStockQty()}}</td>
              </tr>
              
        </tbody>
        </table>
       </div>
       
       
 </div>
 <script>
$(document).ready(function(){
	 $('.pUpdate').editable();

	$('.pUpdate.unit-cost').on('shown', function(e, editable) {
		var checkbox = $('<div class="input form-group checkbox-success checkbox" style="margin-top: 5px;"><input name="isUnitUpdateOrders" value="1" class="UpdateALLOrders" id="isunitupdateorders" type="checkbox"><label for="isunitupdateorders">Update ALL Orders</label></div>');
		
	    editable.input.$input.parents('form').find('.control-group.form-group').append(checkbox);
	})
	
	$('.pUpdate.unit-cost').editable('option', 'params', function (params) {
		console.log(params)
	    var $isUnitUpdateOrders = $('#isunitupdateorders');
	
	    if($isUnitUpdateOrders.is(':checked')) {
	        params.isUnitUpdateOrders = 1;
	    }
	
	    return params;
	});


	 var dataSource = {!!json_encode($managerSources)!!}
		var sourceArray = {}
		var tags = []
		$.each(dataSource,function(id,t){
			sourceArray[parseInt(t.id)] = t.text
			tags.push(t.text)
		 })
   $('.pmanagers').editable({
            inputclass: 'input-large',
            type: 'select',
            url:'{{route('product.ajaxsave')}}',
            send:'auto',
            width: '250px',
            source: tags

      });
})
 </script>