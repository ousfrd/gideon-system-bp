<div class="row">
	 <div style="width:90%;height:300px;">
        <div id="inventory-chart" style="width: 100%;height:100%" ></div>
    </div>
 </div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">

	
	google.charts.load('current', {packages: ['corechart', 'line']});
	//google.charts.setOnLoadCallback(drawInventoryHistoryCurveTypes);

	$("a[href='#inventory']").on('shown.bs.tab', function (e) {
		google.charts.setOnLoadCallback(drawInventoryHistoryCurveTypes);
	})
	function drawInventoryHistoryCurveTypes() {
	      var data = new google.visualization.DataTable();
	      data.addColumn('date', 'X');
	      data.addColumn('number', 'Total Qty');
	      data.addColumn('number', 'Fulfillable Qty');
	      data.addColumn('number', 'Inbound  Qty');
// 	      data.addColumn('number', 'Inbound Shipped Qty');
// 	      data.addColumn('number', 'Inbound Receiving Qty');
	      //data.addColumn('number', 'Gross Sales');
// 	      data.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
	     var dates = []

	      
		  @foreach($inventoryHistory as $date => $row)
		  dates.push([new Date({{date('Y',strtotime($date))}},{{date('n',strtotime($date))-1}},{{date('j',strtotime($date))}}) ,
			  {{$row['total_quantity']}},
			  {{$row['fulfillable_quantity']}},
			  {{$row['inbound_working_quantity']+$row['inbound_shipped_quantity']+$row['inbound_receiving_quantity']}}])
		 
		  @endforeach

		  data.addRows(dates);
		  
	      var options = {
	    		  title: 'Daily Inventory Trend',
	    		  hAxes : {
	    			  0: {title:'Date'}
		  			},
		  		  vAxes:{
			  		  0: {baseline:{{$listing->safeStockQty()}},baselineColor:'red'}
			  	  },
	    		  
			  	focusTarget: 'category',
	    		tooltip: {isHtml: true, textStyle: { fontSize: 13 }}
	    		  
	      };

	      var chart = new google.visualization.LineChart(document.getElementById('inventory-chart'));
	      chart.draw(data, options);

	}


	
</script>
<style>
.tooltip-wrap{padding:10px;font-size:11px}

</style>