 <div class="form-group">
		    <label for="" class="col-sm-2 control-label">Category</label>
		    <div class="col-sm-10">
		      <input class="form-control" autocomplete="off" value="{{isset($expense)?$expense->category:''}}" required placeholder="category" id="category" name="data[category]" type="text">
		    </div>
		  </div>
		  
		  <div class="form-group">
		    <label for="" class="col-sm-2 control-label">Amount</label>
		    <div class="col-sm-10 ">
		      <div class="input-group">
		      <div class="input-group-addon">$</div>
		      <input class="form-control" autocomplete="off"  value="{{isset($expense)?$expense->amount:''}}" required placeholder="Amount" id="" name="data[amount]" type="number"> 
		      </div>
		    </div>
		  </div>
		  
		  <div class="form-group">
		    <label for="" class="col-sm-2 control-label">Posted Date</label>
		    <div class="col-sm-10">
		      <input class="form-control" autocomplete="off" required  value="{{isset($expense)?$expense->posted_date:''}}" placeholder="Date" id="" name="data[date]" type="date">
		    </div>
		  </div>
		  
		  <div class="form-group">
		    <label for="" class="col-sm-2 control-label">Spent By</label>
		    <div class="col-sm-10">
		      <input class="form-control" autocomplete="off" required value="{{isset($expense)?$expense->by:auth()->user()->name}}" id="" name="data[by]" type="text">
		    </div>
		  </div>
		  
		  <div class="form-group">
		    <label for="" class="col-sm-2 control-label">Notes</label>
		    <div class="col-sm-10">
		      <textarea class="form-control" autocomplete="off"  name="data[notes]" >{{isset($expense)?$expense->notes:''}}</textarea>
		    </div>
		  </div>