<div class="mb20"></div>
<div class="row">
<div class="col-sm-10"><h4>Expensess</h4></div>
<div class="col-sm-2"><button type="button" class="btn btn-default" data-toggle="modal" data-target="#expenseModal">  Add Expense</button>
</div>
</div>



 <table class="table table-striped" style="table-layout: fixed;"><tbody>
 <tr>
 <th>Posted Date</th><th>Posted By</th><th>Category</th>
 <th>Amount</th><th>Notes</th>
 <th>Spent By</th>
 
 </tr>
 @foreach($listing->expenses as $expense)
 <tr>
 <td>{{$expense->posted_date}}</td>
 <td>{{$expense->created_by}}</td>
 <td>{{$expense->category}}</td>
 <td>${{$expense->amount}}</td>
 <td>{{$expense->notes}}</td>
 <td>{{$expense->by}}</td>
 
 </tr>
 @endforeach
 </tbody></table>
 
 
 
 