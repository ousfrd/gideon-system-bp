<!-- Modal -->
<form class="form-horizontal" id="expenseForm">
  
<div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Expense</h4>
      </div>
<div class="modal-body">

  
		 @include('listing.parts.expense_form')

	</div>

	<div class="modal-footer">
      	
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" id="newuserModelBtn" class="btn btn-primary">Submit</button>
      </div>
      
  <div class="modal-spinner"><div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div></div> 	
      
  </form>


<script>
 	  var availableTags = ["Product Cost","Shipping","Ads","Reviews"];
// 	  $('#expenseModal').on('shown.bs.modal', function (e) {
// 		  console.log(1)
		  $( "#category" ).autocomplete({
		      source: availableTags,
		      minLength: 0,
		      delay:0
		    }).focus(function(){            
	            $(this).data("uiAutocomplete").search($(this).val());
	        });
		//})
	    
  		$( "#category" ).autocomplete( "option", "appendTo", "#expenseForm" );
  		$( "#category" ).autocomplete( "option", "minLength", 0 );
  		$( "#category" ).autocomplete( "option", "autoFocus", true );
  		$( "#category" ).autocomplete( "option", "delay", 0 );

  		$('#expenseForm').submit(function(){
			$(this).attr('disabled',true)
			$('.modal-spinner').show()
			$.post('{{route('listing.editexpense',[$expense->id])}}',$('#expenseForm').serialize(),function(){
				window.location.reload()
			})

			return false
  	  	})
  </script>
  
 