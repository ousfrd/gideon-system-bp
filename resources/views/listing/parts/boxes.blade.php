<div class="mb20"></div>

<div class="row" id="product-general-stats">
                
                
                <div class="col-md-3 col-sm-6 box" data-box="boxOne"
                data-href="{{$box_route}}"
                 data-range="{{$date_range}}"
                 data-range-date="{{$start_date}}"
                 data-end-date="{{$end_date}}"
                 data-options='{!! json_encode($params) !!}'
                >
                    <div class="panel panel-success dark-bottom panel-stat">
                        <div class="panel-heading border-bottom-0">
                            {{$date_range}}
                        </div>
                        
                        <div class="panel-body">
                       		<div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                        </div>
                    </div>
                </div>
              <div class="col-md-3 col-sm-6 box" data-box="boxTwo"
             	 data-href="{{$box_route}}"
                 data-range="Same Time Prev Week"
                 data-range-date="{{date('Y-m-d',strtotime($start_date) - 7 * 24 * 3600)}}"
                 data-end-date="{{date('Y-m-d',strtotime($end_date) - 7 * 24 * 3600)}}"
                 data-options='{!! json_encode($params) !!}'
              >
                    <div class="panel panel-primary dark-bottom panel-stat">
                        <div class="panel-heading border-bottom-0">
                        	Same Time Prev Week
                            
                        </div>
                        <div class="panel-body">
                       		<div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                        </div>
                    </div>
                </div>
               <div class="col-md-3 col-sm-6 box" data-box="boxThree"
               data-href="{{$box_route}}"
                 data-range="Same Time Prev Month"
                  data-range-date="{{date('Y-m-d',strtotime($start_date) - 30 * 24 * 3600)}}"
                 data-end-date="{{date('Y-m-d',strtotime($end_date) - 30 * 24 * 3600)}}"
                 data-options='{!! json_encode($params) !!}'
               >
                    <div class="panel panel-warning dark-bottom panel-stat">
                        <div class="panel-heading border-bottom-0">
                            Same Time Prev Month
                        </div>
                        <div class="panel-body">
                       		<div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                        </div>
                    </div>
                </div>
                
                
                
                <div class="col-md-3 col-sm-6 box" data-box="boxTwo"
             	 data-href="{{$box_route}}"
                 data-range="Lifetime"
                 data-range-date="2010-01-01"
                 data-end-date="{{date('Y-m-d')}}"
                 data-options='{!! json_encode($params) !!}'
                >
                    <div class="panel panel-primary dark-bottom panel-stat">
                        <div class="panel-heading border-bottom-0">
                        	Lifetime 
                        </div>
                        <div class="panel-body">
                       		<div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                        </div>
                    </div>
                </div>
                
                
                </div>
                                            
                                            
<style>
.periodic-gross-sales .panel-heading .preloader, #merchant-products .preloader{width:44px;height:44px;top:50%;left:50%;margin-top:-22px;margin-left:-22px}
.range_title{display:none;}
</style>

<script>
$(document).ready(function(){
	$('#product-general-stats .box').each(function(){
		if ($(this).attr('data-href') != null) {
			var params = $.parseJSON($(this).attr('data-options'));
			params['start_date'] = $(this).attr('data-range-date');
			params['end_date'] = $(this).attr('data-end-date');
			params['data_range'] = $(this).attr('data-range');
			$(this).find('.panel-body').load($(this).attr('data-href'),params,function(){})
		}

	})
})
</script>