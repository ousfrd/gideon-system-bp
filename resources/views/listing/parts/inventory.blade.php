<div class="mb20"></div>
@if($start_date != $end_date)  
@include('listing.parts.inventoryhistory')
@endif

<h4>Current Inventory Summary</h4>
<div class="row">
<div class="col-xs-12 ">
          <table class="table table-striped" style="table-layout: fixed;">
<!--             <thead> -->
<!--             	<tr> -->
<!--             	<th width=1></th> -->
<!--             	<th></th> -->
<!--             	<th width=1></th> -->
<!--             	<th></th> -->
<!--             	<th width=1></th> -->
<!--             	<th></th> -->
<!--             	</tr> -->
<!--             </thead> -->
            <tbody>
              <tr>
                <th>Fulfillment Channel</th>
                <td class="text-right">{{$listing->fulfillment_channel}}</td>
                <th>Fulfillable Qty</th>
                <td class="text-right">{{$listing->afn_fulfillable_quantity}}</td>
             	  <th>Unsellable Qty</th>
                <td class="text-right">{{$listing->afn_unsellable_quantity}}</td>
              </tr>
           
											
             
              <tr>
                <th>Reserved Qty</th>
                <td class="text-right">{{$listing->afn_reserved_quantity}}</td>
                 <th>Warehouse Total Qty</th>
                <td class="text-right">{{$listing->afn_warehouse_quantity }}</td>
                <th>Inbound Working Qty</th>
                <td class="text-right">{{$listing->afn_inbound_working_quantity }}</td>
              </tr>
              
              
              <tr>
                <th>Inbound Shipped Qty</th>
                <td class="text-right">{{$listing->afn_inbound_shipped_quantity }}</td>
                <th>Inbound Receiving Qty</th>
                <td class="text-right">{{$listing->afn_inbound_receiving_quantity }}</td>
                
                <th>Total Qty</th>
                <td class="text-right">{{$listing->afn_total_quantity }}</td>
                
              </tr>
              
              
        </tbody>
        </table>
       </div>
</div>

@if($listing->health_report)
<h4>Listing Health Report</h4>
<div class="row">
<ul class="list-unstyled">
      @foreach($listing->health_report->toArray() as $k=>$v)
      @if(in_array($k,['id','seller_id','listing_id'])) @continue @endif
      <li class="col-sm-3"> <strong>{{ ucwords(str_replace('_',' ',$k))}}: </strong>{{$v}}
      @endforeach
</ul>
 </div>
@endif