<div class="row">
	 <div style="width:90%;height:300px;">
        <div id="order-chart" style="width: 100%;height:100%" ></div>
    </div>
 </div>
 
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">

	
	google.charts.load('current', {packages: ['corechart', 'line']});
	
	google.charts.setOnLoadCallback(drawCurveTypes);
	
	$("a[href='#charts']").on('shown.bs.tab', function (e) {
		google.charts.setOnLoadCallback(drawCurveTypes);
	})
	
	function drawCurveTypes() {
	      var data = new google.visualization.DataTable();
	      @if($start_date == $end_date)
	    	  data.addColumn('datetime', 'X');
    	  @else    
	      	data.addColumn('date', 'X');
	      @endif


	      data.addColumn('number', 'Current');
	      data.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
	      data.addColumn('number', 'Same time prev week');
	      data.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
	      data.addColumn('number', 'Same time 30 days ago');
	      data.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});


	      
	     var dates = []

	      
		  @foreach($orders as $date => $order)
		 
			  @if($start_date == $end_date)
				  dates.push([new Date({{date('Y',strtotime($start_date))}},{{date('n',strtotime($start_date))-1}},{{date('j',strtotime($start_date))}},{{$date}}) ,
					  {{$order->total_units}},
					  '<div class="tooltip-wrap"><b>{{date('g A',strtotime($start_date.' '.$date.':00:00'))}}:</b><br> Units Sold {{$order->total_units}}<br>Gross Sales: ${{number_format($order->total_gross,2)}}</div>',

					  {{$orderReportLastWeek[$date]->total_units}},

					  '<div class="tooltip-wrap">Units Sold: {{$orderReportLastWeek[$date]->total_units}}<br/>Gross Sales: ${{number_format($orderReportLastWeek[$date]->total_gross,2)}}</div>',
					  {{$orderReport30DayAgo[$date]->total_units}},
					  '<div class="tooltip-wrap">Units Sold: {{$orderReport30DayAgo[$date]->total_units}}<br/>Gross Sales: ${{number_format($orderReport30DayAgo[$date]->total_gross,2)}}</div>'
					

					  ])
		  @else
			  dates.push([new Date({{date('Y',strtotime($date))}},{{date('n',strtotime($date))-1}},{{date('j',strtotime($date))}}) ,
				  {{$order->total_units}},
				  '<div class="tooltip-wrap">Units Sold: {{$order->total_units}}<br/>Gross Sales: ${{@number_format($order->total_gross,2)}}</div>',
				  {{@$orderReportLastWeek[date('Y-m-d',strtotime($date)-7*24*3600 )]->total_units}},
				  '<div class="tooltip-wrap">Units Sold: {{@$orderReportLastWeek[date('Y-m-d',strtotime($date)-7*24*3600 )]->total_units}}<br/>Gross Sales: ${{number_format(@$orderReportLastWeek[date('Y-m-d',strtotime($date)-7*24*3600 )]->total_gross,2)}}</div>',
				  {{@$orderReport30DayAgo[date('Y-m-d',strtotime($date)-30*24*3600 )]->total_units}},
				  '<div class="tooltip-wrap">Units Sold: {{@$orderReport30DayAgo[date('Y-m-d',strtotime($date)-30*24*3600 )]->total_units}}<br/>Gross Sales: ${{number_format(@$orderReport30DayAgo[date('Y-m-d',strtotime($date)-30*24*3600 )]->total_gross,2)}}</div>'
				
				  ])
		  @endif
		  @endforeach

		  data.addRows(dates);
		  
	      var options = {
	    		  title: 'Units Sold/Gross Sales',
 	    		  focusTarget: 'category',
	    		  tooltip: {isHtml: true, textStyle: { fontSize: 13 }}
	    		 
	      };

	      var chart = new google.visualization.LineChart(document.getElementById('order-chart'));
	      chart.draw(data, options);

// 	      var materialChart = new google.charts.Line(document.getElementById('order-chart'));
// 	       materialChart.draw(data, options);
	}


	
</script>
<style>
.tooltip-wrap{padding:10px;font-size:11px}

</style>