@if($listing->inventory && $listing->daysToReplenish() <= 7)

<div class="bs-callout bs-callout-sm bs-callout-danger bg-danger"  role="alert">
<h4 style="display: inline-block;">Alert: </h4> 
@if($listing->inventory->fulfillable_quantity == 0)
Listing is out of stock.
@elseif($listing->daysToReplenish()>0)
Listing is considered to replesish in {{$listing->daysToReplenish()}} day(s)
@else
Listing stocking is running out soon.
@endif

<a  class="pull-right btn btn-default"  data-toggle="modal" data-target="#inboundshipmentModal">Create Inbound Shipment Plan</a>


</div>
@endif
