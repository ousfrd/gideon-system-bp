<div class="mb20"></div>
<div class="row">
<div class="col-sm-10"><h4>Daily Ads Report</h4></div>
<div class="col-sm-2">
</div>
</div>



 <table class="table table-striped" style="table-layout: fixed;"><tbody>
 <tr>
 <th>Date</th>
 <th>Clicks</th>
 <th>Impressions</th>
 <th>CTR</th>
 <th>Total Spend</th>
 <th>Avg. CPC</th>
 
 </tr>
 @foreach($listing->ad_report($start_date,$end_date) as $date=>$ad)
 <tr>
 <th> {{$date}} {{date('l',strtotime($date))}} </th>
 @if(empty($ad))
 <td>0</td>
 <td>0</td>
 <td>0</td>
 <td>0</td>
 <td>0</td>
 
 @else
 <td>{{$ad->clicks}}</td>
 <td>{{$ad->impressions}}</td>
 <td>{{$ad->clicks == 0 ? 0 : number_format($ad->clicks*100/$ad->impressions,2)}}%</td>
 <td>${{number_format($ad->spend * $ad->exchange_rate,2)}}</td>
 <td>${{$ad->clicks ? number_format($ad->spend * $ad->exchange_rate/$ad->clicks,2) : 0}}</td>
 @endif
 
 </tr>
 @endforeach
 </tbody></table>
 
 
 
 