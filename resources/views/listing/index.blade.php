@extends('layouts.master')
@section('title', $title)
@section('content')
{!! $grid !!}

<script type="text/javascript">

$(document).ready(function(){
//edit form style - popup or inline
//$.fn.editable.defaults.mode = 'inline';

  $('.alert-qty').parents('tr').addClass('bg-warning');
  $('.editable').editable();
	$('.pUpdate').editable();


	$('.pUpdate.unit-cost').on('shown', function(e, editable) {
		var checkbox = $('<div class="input form-group checkbox-success checkbox" style="margin-top: 5px;"><input name="isUnitUpdateOrders" value="1" class="UpdateALLOrders" id="isunitupdateorders" type="checkbox"><label for="isunitupdateorders">Update ALL Orders</label></div>');
		
	    editable.input.$input.parents('form').find('.control-group.form-group').append(checkbox);
	})
	
	$('.pUpdate.unit-cost').editable('option', 'params', function (params) {
		
	    var $isUnitUpdateOrders = $('#isunitupdateorders');
	
	    if($isUnitUpdateOrders.is(':checked')) {
	        params.isUnitUpdateOrders = 1;
	    }
	
	    return params;
	});


	
  var dataSource = {!!json_encode($managerSources)!!}
  var sourceArray = {}
  var tags = []
  $.each(dataSource,function(id,t){
    sourceArray[parseInt(t.id)] = t.text
    tags.push(t.text)
   })

  $('.pmanagers').each(function(){
    $(this).editable({
          inputclass: 'input-large',
          type: 'select',
          url:'{{route('product.ajaxsave')}}',
          send:'auto',
          width: '250px',
          source: tags
      })

    }); 
	   

})
</script>
@endsection