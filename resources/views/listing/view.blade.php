@extends('layouts.master')
@section('title', $listing->product->name)
@section('content')
<div class="btn-actions">
<button class="back-btn" onclick="window.location='{{url()->previous()}}'"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true">Back</span></button>


</div>
<div class="product dashboard">

@include('listing.parts.alert')


<br/>

<!-- Button trigger modal -->


<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#charts"><i class="fa fa-line-chart" aria-hidden="true"></i> Sales Report</a></li>
  <li><a data-toggle="tab" href="#info"><i class="fa " aria-hidden="true"></i>Listing Info</a></li>
  <li><a data-toggle="tab" href="#inventory"><i class="fa " aria-hidden="true"></i>Inventory Report</a></li>
  <li><a data-toggle="tab" href="#ads"><i class="fa " aria-hidden="true"></i>Ads Report</a></li>
  <li><a data-toggle="tab" href="#expenses"><i class="fa " aria-hidden="true"></i>Expenses</a></li>
  <li><a data-toggle="tab" href="#inbound"><i class="fa " aria-hidden="true"></i>Inbound Shipments</a></li>
 
   <li class="pull-right" style="padding-left:10px">@include('listing.parts.filter')</li>
    <li class="pull-right" style="padding-left:10px"><button type="button" class="btn btn-default" data-toggle="modal" data-target="#expenseModal">  Add Expense</button></li>
     <li class="pull-right"><button type="button" class="btn btn-default" data-toggle="modal" data-target="#inboundshipmentModal"> Create Shipment</button></li>
 </ul>
 
<div class="tab-content">
	<div id="charts" class="tab-pane active">
		@include('listing.parts.boxes')
		@include('index.parts.charts') 
		
	</div>
	<div id="info" class="tab-pane fade">
		@include('listing.parts.info')
	</div>
	
	<div id="inbound" class="tab-pane fade">
		@include('listing.parts.inboundshipments')
	</div>
	
	<div id="inventory" class="tab-pane fade">
		@include('listing.parts.inventory')
	</div>
	
	<div id="expenses" class="tab-pane fade">
		@include('listing.parts.expenses')
	</div>
	
	<div id="ads" class="tab-pane fade">
		@include('listing.parts.ads')
	</div>
</div>
 
 
 @include('listing.parts.expensemodal')
@include('listing.parts.inboundshipment_form')



         
            



</div>



@endsection