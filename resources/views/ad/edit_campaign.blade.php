@extends('layouts.master')
@section('title', $title)
@section('content')

<h5 class="page-header"><a href="{{route('ad.manage')}}">Ads Management</a> > <a href="{{route('ad.manage')}}?AdCampaigns%5Bfilters%5D%5Bseller_id-eq%5D={{$campaign->seller_id}}">{{$campaign->account->name}} Campaigns</a> > <a href="{{route('ad.campaigns.view', [$campaign->campaignId])}}">{{$campaign->name}}</a>  >  Edit</h5>

<!-- .page-section -->
<div class="page-section" style="max-width:500px">


  	<form id="editCampaign" class="form-horizontal" method="Post" action="{{route('ad.campaigns.edit', $campaign->campaignId)}}">
        
        {{Form::token()}}


    	<div class="form-group row">
          <label class="col-sm-4 col-form-label">*Name:</label>
          <div class="col-sm-8">
            {{ Form::text('data[name]', $campaign->name, array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
          </div>
        </div>	

    	<div class="form-group row">
          <label class="col-sm-4 col-form-label">*State:</label>
          <div class="col-sm-8">
          	{{ Form::select('data[state]', ['enabled'=>'Enabled', 'paused'=>'Paused', 'archived'=>'Archived'], $campaign->state,['class' => 'form-control','required']) }}
          </div>
        </div>	

<!--     	<div class="form-group row">
          <label class="col-sm-4 col-form-label">*Targeting Type:</label>
          <div class="col-sm-8">
          	{{ Form::select('data[targetingType]', ['auto'=>'Auto', 'manual'=>'Manual'], $campaign->targetingType,['class' => 'form-control','required']) }}
          </div>
        </div> -->	

    	<div class="form-group row">
          <label class="col-sm-4 col-form-label">*Daily Budget:</label>
          <div class="col-sm-8">
            {{ Form::text('data[dailyBudget]', $campaign->dailyBudget, array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
          </div>
        </div>	


                        <div class="form-actions">
                          <button class="btn btn-primary" type="submit" id="submit">Submit</button>
                        </div>



	</form>


</div>



@endsection