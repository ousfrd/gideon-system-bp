@extends('layouts.master')
@section('title', $title)
@section('content')



<!-- .page-section -->

<script type="text/javascript" src="{!! asset('js/wickedpicker.min.js') !!}"></script>
<link href="{!! asset('css/wickedpicker.min.css') !!}" media="all" rel="stylesheet" type="text/css" />

              <!-- .page-title-bar -->
              <header class="page-title-bar">

                <!-- title and toolbar -->
                <div class="d-sm-flex align-items-sm-center">
                  <!-- .btn-toolbar -->
                  <div id="dt-buttons" class="btn-toolbar"><a href="{{route('ad.rules')}}"><button type="button" class="btn btn-primary">Rule List</button></a></div>

                  <!-- /.btn-toolbar -->
                </div>
                <!-- /title and toolbar -->
              </header>
              <!-- /.page-title-bar -->


<div class="page-section" style="max-width:500px;margin-top:20px;">


  	<form id="newCampaign" class="form-horizontal" method="Post" action="{{route('ad.rules.new')}}">
        
        {{Form::token()}}
    
        
      <div class="form-group row">
          <label class="col-sm-4 col-form-label">Account:</label>
          <div class="col-sm-8">        
            {{ Form::select('seller_id', $accounts, '',['class' => 'form-control account']) }}
          </div>
        </div>  

    	<div class="form-group row">
          <label class="col-sm-4 col-form-label">Contained Word:</label>
          <div class="col-sm-8">
            {{ Form::text('containedword', '', array_merge(['class' => 'form-control','placeholder'=>'Contained Word'])) }}
          </div>
        </div>	
    	<div class="form-group row">
          <label class="col-sm-4 col-form-label">State:</label>
          <div class="col-sm-8">
          	{{ Form::select('state', [''=>'Any','enabled'=>'Enabled', 'paused'=>'Paused', 'archived'=>'Archived'], '',['class' => 'form-control']) }}
          </div>
        </div>	        

        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Targeting Type:</label>
          <div class="col-sm-8">
          	{{ Form::select('targetingType', [''=>'Any','auto'=>'Auto', 'manual'=>'Manual'], '',['class' => 'form-control']) }}
          </div>
        </div>        

        <div class="form-group row">
          <label class="col-sm-4 col-form-label">ASIN:</label>
          <div class="col-sm-8">
          {{ Form::text('asin', '', array_merge(['class' => 'form-control','placeholder'=>'ASIN'])) }}
          </div>
        </div>        

<!--
        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Manager:</label>
          <div class="col-sm-8">        
            {{ Form::select('user_id', $users, '',['class' => 'form-control account']) }}
          </div>
        </div>  
-->
    	<div class="form-group row">
          <label class="col-sm-4 col-form-label">*Scheduled Time (Daily):</label>
          
          <div class="col-sm-8">
          
          {{ Form::text('exec_time', '', array_merge(['class' => 'form-control exec_time','placeholder'=>'ASIN'])) }}
    
          </div>
        </div>	    

        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Rule Active:</label>
          <div class="col-sm-8">
          	{{ Form::select('rule_active', [TRUE=>'Active', FALSE=>'Non-Active'], '',['class' => 'form-control']) }}
          </div>
        </div>           

        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Change Campaign State to:</label>
          <div class="col-sm-8">
          	{{ Form::select('change_campaign_open', [TRUE=>'Enabled', FALSE=>'Paused'], '',['class' => 'form-control']) }}
          </div>
        </div>  

        <div class="form-actions">
          <button class="btn btn-primary" type="submit" id="submit">Submit</button>
        </div>



	</form>


</div>

<script>
var options = { 
  
  twentyFour: false,
  title: 'Scheduled Time(PST)'

}
  //hh:mm 24 hour format only, defaults to current time twentyFour: false, 
  //Display 24 hour format, defaults to false upArrow: 'wickedpicker__controls__control-up', 
  //The up arrow class selector to use, for custom CSS downArrow: 'wickedpicker__controls__control-down', 
  //The down arrow class selector to use, for custom CSS close: 'wickedpicker__close', 
  //The close class selector to use, for custom CSS hoverState: 'hover-state', 
  //The hover state class to use, for custom CSS title: 'Timepicker', 
  //The Wickedpicker's title, showSeconds: false, 
  //Whether or not to show seconds, secondsInterval: 1, //Change interval for seconds, defaults to 1  , minutesInterval: 1,
  //Change interval for minutes, defaults to 1 beforeShow: null,
  //A function to be called before the Wickedpicker is shown show: null, 
  //A function to be called when the Wickedpicker is shown clearable: false,
   //Make the picker's input clearable (has clickable "x")  };

   $('.exec_time').wickedpicker(options);
</script>


@endsection