@extends('layouts.master')
@section('title', $title)
@section('content')




              <!-- .page-title-bar -->
              <header class="page-title-bar">

                <!-- title and toolbar -->
                <div class="d-sm-flex align-items-sm-center">
                  <!-- .btn-toolbar -->
                  <div id="dt-buttons" class="btn-toolbar"><a href="{{route('ad.campaigns.new')}}" target="_blank"><button type="button" class="btn btn-primary">Create Campaign</button></a></div>

                  <!-- /.btn-toolbar -->
                </div>
                <!-- /title and toolbar -->
              </header>
              <!-- /.page-title-bar -->

              
		{!! $campaigns_grid !!}
  


@endsection