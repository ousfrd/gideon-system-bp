@extends('layouts.master')
@section('title', $title)
@section('content')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.5.2/flatpickr.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.5.2/themes/material_blue.css"/>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.5.2/flatpickr.min.js"></script>

  <div id="container"></div>

  <script>
        let accounts = <?php echo json_encode($accounts); ?>;
        let marketplaces = <?php echo json_encode($marketplaces); ?>;
        let fetchrows_url = '<?php echo route('ad.fetchrows'); ?>';
        let fetchcampaigns_url = '<?php echo route('ad.agfetchcampaigns'); ?>';
        let fetchcampaignstotal_url = '<?php echo route('ad.agfetchcampaignstotal'); ?>';
        let fetchadgroups_url = '<?php echo route('ad.agfetchadgroups'); ?>';
        let fetchkeywords_url = '<?php echo route('ad.agfetchkeywords'); ?>';
        let fetchsearchterms_url = '<?php echo route('ad.agfetchsearchterms'); ?>';
        let fetchproducts_url = '<?php echo route('ad.agfetchproducts'); ?>';
        let updateCampaignState_url = '<?php echo route('ad.updateCampaignState'); ?>';
        let updateAutoSchedulerState_url = '<?php echo route('ad.updateAutoSchedulerState'); ?>';
        let updateCampaignDailyBudget_url = '<?php echo route('ad.updateCampaignDailyBudget'); ?>';
        let updateAdGroupDefaultBid_url = '<?php echo route('ad.updateAdGroupDefaultBid'); ?>';
        let updateAdGroupsDefaultBid_url = '<?php echo route('ad.updateAdGroupsDefaultBid'); ?>';
        let updateAdGroupState_url = '<?php echo route('ad.updateAdGroupState'); ?>';
        let updateKeywordBid_url = '<?php echo route('ad.updateKeywordBid'); ?>';
        let updateKeywordState_url = '<?php echo route('ad.updateKeywordState'); ?>';
        let updateKeywordGroupBid_url = '<?php echo route('ad.updateKeywordGroupBid'); ?>';
        let beginning = '<?php echo $beginning; ?>';
  </script>

  <!-- Load our React component. -->
  {{ Html::script(mix('js/ag_react_ads.js')) }}
  
  


  <style type="text/css">
  	.color0 { color: rgb(91,195,76); }
  	.color1 { color: rgb(242,104,95); }
  	.color2 { color: rgb(250,172,94); }
  	.color3 { color: rgb(177,116,227); }
  	.color4 { color: rgb(93,157,252); }
  	.color5 { color: rgb(76,215,200); }
  	.color6 { color: rgb(234,82,171); }
  	.color7 { color: rgb(255,218,89); }
  	.color8 { color: rgb(155,170,182); }
  	.color9 { color: rgb(107,142,35); }
    .box-inlay { text-align: right; padding-bottom: 20px; }
    #series1 { margin-right: 10px; }
    .google_charts { margin-bottom: 35px; }
    .adImage { width: 75px; height: 75px; display: inline-block; margin-right: 25px; text-align: center }
    .adASIN { display: inline-block; vertical-align: top; }
    span.editable, span.editing { text-decoration: none; border-bottom: dashed 1px #0088cc; }
    span.editable:hover, span.editing { background-color: #FFFFC0; cursor: text; }
    .range-filter { width: 60px; margin-bottom: 5px; }
    .bid-adjust { width: 60px; }
    .checkbox-control { width: 100%; display: block; height: 16px; }
    .ag-theme-balham .ag-header {font-size: 14.5px!important;}
    .ag-theme-balham {font-size: 14px!important;}
    .ag-pinned-left-floating-top, .ag-floating-top-container { font-weight: 500; color: #1CAF9A;}
    .ag-font-style {
      user-select: initial;
      -moz-user-select: text;
      -ms-user-select: text;
      -webkit-user-select: text;
    }
    .ag-selection-checkbox {float: left;}
    .custom-date-filter a {
        position: relative;
        right: 30px;
        color: rgba(0, 0, 0, 0.54);
        cursor: pointer;
    }
    .custom-date-filter:after {
        content: '\f073';
        font-weight: 400;
        position: relative;
        right: 28px;
        pointer-events: none;
        color: rgba(0, 0, 0, 0.54);
    }
  </style>


@endsection