@extends('layouts.master')
@section('title', $title)
@section('content')


<h5 class="page-header"><a href="{{route('ad.manage')}}">Ads Management</a> > <a href="{{route('ad.manage')}}?AdCampaigns%5Bfilters%5D%5Bseller_id-eq%5D={{$campaign->seller_id}}">{{$campaign->account->name}} Campaigns</a> > <a href="{{route('ad.campaigns.view', [$campaign->campaignId])}}">{{$campaign->name}}</a>  >  Ad Groups</h5>


              <!-- .page-title-bar -->
              <header class="page-title-bar">
                
                <!-- title and toolbar -->
                <div class="d-sm-flex align-items-sm-center">
                  <!-- .btn-toolbar -->
                  <div id="dt-buttons" class="btn-toolbar"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#adgroupsModal">Create Ad Group</button></div>

                  <!-- /.btn-toolbar -->
                </div>
                <!-- /title and toolbar -->
              </header>
              <!-- /.page-title-bar -->

              
		{!! $adgroups_grid !!}
  

    @include('ad.parts.adgroups_modal')


@endsection