 <div class="modal fade" id="newCampaignsModal" tabindex="-1" role="dialog" aria-labelledby="newCampaignsModal">


  	<form id="newCampaign" class="" method="Post" action="{{route('ad.campaigns.new')}}">
        
        {{Form::token()}}

        <div class="modal-dialog" role="document">
              <div class="modal-content"  style="background: #fff">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Create New Campaign</h4>
              </div>
              <div class="modal-body">


      <div class="form-group row">
          <label class="col-sm-4 col-form-label">*Account:</label>
          <div class="col-sm-8">
            {{ Form::select('data[seller_id]', $accounts, '',['class' => 'form-control account','required']) }}
          </div>
        </div>  

      <div class="form-group row">
          <label class="col-sm-4 col-form-label">*Marketplace:</label>
          <div class="col-sm-8">
            {{ Form::select('data[marketplace]', $marketplaces, '',['class' => 'form-control marketplace','required']) }}
          </div>
        </div>  



    	<div class="form-group row">
          <label class="col-sm-4 col-form-label">*Name:</label>
          <div class="col-sm-8">
            {{ Form::text('data[name]', '', array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
          </div>
        </div>	

    	<div class="form-group row">
          <label class="col-sm-4 col-form-label">*State:</label>
          <div class="col-sm-8">
          	{{ Form::select('data[state]', ['enabled'=>'Enabled', 'paused'=>'Paused', 'archived'=>'Archived'], '',['class' => 'form-control','required']) }}
          </div>
        </div>	

      <div class="form-group row">
          <label class="col-sm-4 col-form-label">*Targeting Type:</label>
          <div class="col-sm-8">
          	{{ Form::select('data[targetingType]', ['auto'=>'Auto', 'manual'=>'Manual'], '',['class' => 'form-control','required']) }}
          </div>
        </div>

    	<div class="form-group row">
          <label class="col-sm-4 col-form-label">*Daily Budget:</label>
          <div class="col-sm-8">
            {{ Form::text('data[dailyBudget]', '', array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
          </div>
        </div>	

        </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" id="expenseModelBtn" class="btn btn-primary">Submit</button>
      </div>
      
      <div class="modal-spinner"><div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div></div>


    </div>
  </div>
  
	</form>



<script type="text/javascript">

  let account_marketplaces = <?php echo json_encode($account_marketplaces); ?>;

    $('.account').on('change', function() {

      var seller_id = $(this).val();
      var marketplaces = account_marketplaces[seller_id];
      var marketplaces_options = marketplaces.split(',').map(m => '<option value="' + m + '">' + m + '</option>');

      $('.marketplace').html(marketplaces_options.join(""));
    })

</script>

</div>
