<div class="box-inlay">
                    <span class="fa fa-line-chart headline" style="margin-right: 3px"></span> <span class="headline">Trend</span> <select id="series2" class="series-select" onchange="series_selected.call(this);" style="color: rgb(250, 172, 94);">
                    <option value="0" style="color: rgb(91, 195, 76); display: none;">Sales</option>
                    <option value="1" style="color: rgb(242, 104, 95);">Ad Spend</option>
                    <option value="2" selected="selected" style="color: rgb(250, 172, 94);">ACoS</option>
                    <option value="3" style="color: rgb(177, 116, 227);">CPC</option>
                    <option value="4" style="color: rgb(93, 157, 252);">Impressions</option>
                    <option value="5" style="color: rgb(76, 215, 200);">CTR</option>
                    <option value="6" style="color: rgb(234, 82, 171);">Clicks</option>
                    <option value="7" style="color: rgb(255, 218, 89);">CR</option>
                    <option value="8" style="color: rgb(155, 170, 182);">Orders</option>
                </select> <select id="series1" class="series-select" onchange="series_selected.call(this);" style="color: rgb(91, 195, 76);">
                    <option value="0" selected="selected" style="color: rgb(91, 195, 76);">Sales</option>
                    <option value="1" style="color: rgb(242, 104, 95);">Ad Spend</option>
                    <option value="2" style="color: rgb(250, 172, 94); display: none;">ACoS</option>
                    <option value="3" style="color: rgb(177, 116, 227);">CPC</option>
                    <option value="4" style="color: rgb(93, 157, 252);">Impressions</option>
                    <option value="5" style="color: rgb(76, 215, 200);">CTR</option>
                    <option value="6" style="color: rgb(234, 82, 171);">Clicks</option>
                    <option value="7" style="color: rgb(255, 218, 89);">CR</option>
                    <option value="8" style="color: rgb(155, 170, 182);">Orders</option>
                </select>



</div>
                

<div class="box-inlay box-content" style="height: 235px; margin-top: 10px;">
	
	<div id="main_chart"></div>

</div>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);

  var main_data, main_options, main_chart;
  var show_series = [0, 2];

  var main_colors = [
                'rgb(91,195,76)',  // green
                'rgb(242,104,95)',    // red
                'rgb(250,172,94)',   // orange
                'rgb(177,116,227)',  // violet
                'rgb(93,157,252)',   // blue
                'rgb(76,215,200)',   // turquoise
                'rgb(234,82,171)',   // pink
                'rgb(255,218,89)',   // yellow
                'rgb(155,170,182)',  // grey
                'rgb(107,142,35)'   // dark olive
  ];

    function filter_main_series(show) {
        // store global variables
        show_series = show;

        // filter series
        var v = new google.visualization.DataView(main_data);
        var hide = [];

        /* console.log('v.columns '+v.getNumberOfColumns()); */

        for (var i = 0; i < v.getNumberOfColumns() - 1; i++) {
            if (($.inArray(Math.floor(i), show) == -1)) {
                hide.push(i + 1); // first col is the annotation label
            }
        }
        v.hideColumns(hide);

        // adjust main options (colors and axes)
        // switch axes and colors if show[0] > show[1] to maintain legend order and colors
        var c1, c2;
        if (show[0] > show[1]) {
            c1 = main_colors[show[1]];
            c2 = main_colors[show[0]];
            main_options.series[0].targetAxisIndex = 1;
            main_options.series[1].targetAxisIndex = 0;
        } else {
            c1 = main_colors[show[0]];
            c2 = main_colors[show[1]];
            main_options.series[0].targetAxisIndex = 0;
            main_options.series[1].targetAxisIndex = 1;
        }

        main_options.colors = [c1, c2];

        main_options.vAxes[0].textStyle.color = main_colors[show[0]];
        main_options.vAxes[1].textStyle.color = main_colors[show[1]];

        // re-draw chart
        main_chart.draw(v, main_options);

    }


  function load_main_chart() {
		main_data = new google.visualization.DataTable();

        main_data.addColumn({"type":"date","id":"x","label":"x"});
		main_data.addColumn({"type":"number","id":"revenue","label":"revenue"});
		main_data.addColumn({"type":"number","id":"cost","label":"cost"});
		main_data.addColumn({"type":"number","id":"acos","label":"acos"});
		main_data.addColumn({"type":"number","id":"cpc","label":"cpc"});
		main_data.addColumn({"type":"number","id":"impressions","label":"impressions"});
		main_data.addColumn({"type":"number","id":"ctr","label":"ctr"});
		main_data.addColumn({"type":"number","id":"clicks","label":"clicks"});
		main_data.addColumn({"type":"number","id":"cr","label":"cr"});
		main_data.addColumn({"type":"number","id":"orders","label":"orders"});
		main_data.addColumn({"type":"number","id":"starting bid","label":"starting bid"});

		var rows = [];
		<?php
			foreach($ad_daily_data as $date => $data) {
		?>
			rows.push([new Date(<?php echo date("Y, n - 1, d, H, i, s", strtotime($date)) ?>), <?php echo $data['total_sales']?>, <?php echo $data['total_cost']?>, <?php echo $data['acos']?>, <?php echo $data['cpc']?>, <?php echo $data['total_impressions']?>, <?php echo $data['ctr']?>, <?php echo $data['total_clicks']?>, <?php echo $data['cr']?>, <?php echo $data['total_orders']?>, <?php echo isset($data['bid']) ? $data['bid'] : 'null' ?>]);
		<?php
			}
		?>
		main_data.addRows(rows);


		main_options = {
            height: 235,
            colors: main_colors,
            curveType: 'function',
            interpolateNulls: false,
            legend: 'none',
            pointSize: 4,

            chartArea: {left: 80, top: 8, right: 80, height: '205'},

            hAxis: {
                gridlines: {color: 'transparent'},
                format: 'M/d/yyyy',
                textStyle: {color: 'rgb(155,170,182)'},
                viewWindow: {
                    min: new Date(<?php echo date("Y, n - 1, d, H, i, s", strtotime($start_date)) ?>),
                    max: new Date(<?php echo date("Y, n - 1, d, H, i, s", strtotime($end_date)) ?>)
                }
            },
            vAxes: {
                0: {
                    baselineColor: 'rgb(233,233,233)',
                    textStyle: {color: 'grey'},
                    gridlines: {color: 'transparent'},
                    minValue: 0,
                    viewWindow: {min: 0},
                },
                1: {
                    baselineColor: 'rgb(233,233,233)',
                    textStyle: {color: 'grey'},
                    gridlines: {color: 'transparent'},
                    minValue: 0,
                    viewWindow: {min: 0},
                }
            },
            series: {
                0: {targetAxisIndex: 0},
                1: {targetAxisIndex: 1},
            }

        };


    	main_chart = new google.visualization.LineChart(document.getElementById('main_chart'));

    
  }

	function drawChart() {
	    load_main_chart();
	    filter_main_series(show_series);
	}


	function series_selected() {
        // set color
        /* console.log($(this).find('option:selected').css('color')); */
        $(this).css('color', $(this).find('option:selected').css('color'));
        // show all
        /* console.log('showing all '+ $('.series-select option')); */
        $('.series-select option').show();
        // hide selected from all others respectively
        $('.series-select').each(function () {
            var selected = $(this).find('option:selected').val();
            /* console.log('hiding '+selected); */
            $('.series-select').not(this).each(function () {
                $(this).find('option[value=' + selected + ']').hide();
            });
        });

        filter_main_series([Number($('#series1 option:selected').val()), Number($('#series2 option:selected').val())]);
    }

</script>


<style type="text/css">
.series-select {
    float: right;
    border: none;
    background-color: rgba(249, 249, 249, 1);
    margin-left: 3px;
}	
</style>