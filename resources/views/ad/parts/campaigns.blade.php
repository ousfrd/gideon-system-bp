<table id="campaign_id" class="display">
    <thead>
        <tr>
        	<th>Status</th>
            <th>Name</th>
            <th>Targeting Type</th>
            
            <th>Ad Spend <span>USD</span></th>
            <th>Sales <span>USD</span></th>

            <th>Profit <span>USD</span></th>
            <th>ACoS <span>%</span></th>
            <th>CPC <span>USD</span></th>

            <th>Impr.</th>
            <th>CTR <span>%</span></th>
            <th>Clicks</th>
            
            <th>CR <span>%</span></th>
            <th>Orders</th>

            <th>Daily Budget</th>
        </tr>
    </thead>
    <tbody>
    	@foreach ($campaigns as $campaign) 
    	@if(isset($campaign['data']))
        <tr>
        	<td>{{ $campaign['data']->state }}</td>
            <td><a href="{{route('ad.view_campaign', $campaign['data']->id)}}?start_date={{$start_date}}&end_date={{$end_date}}&date_range={{$date_range}}">{{ $campaign['data']->name }}</a></td>
            <td>{{ $campaign['data']->targetingType }}</td>

            <td>{{ isset($campaign['total_cost']) ? $campaign['total_cost'] : 0 }}</td>
            <td>{{ isset($campaign['total_sales']) ? $campaign['total_sales'] : 0 }}</td>
    
            <td></td>
            <td>{{ (isset($campaign['total_sales']) && $campaign['total_sales'] > 0) ? round(($campaign['total_cost'] / $campaign['total_sales']) * 100, 2) : 0.0 }}</td>
            <td>{{ (isset($campaign['total_clicks']) && $campaign['total_clicks'] > 0) ? round($campaign['total_cost'] / $campaign['total_clicks'], 2) : 0 }}</td>

            <td>{{ isset($campaign['total_impressions']) ? $campaign['total_impressions'] : 0 }}</td>

            <td>{{ (isset($campaign['total_impressions']) && $campaign['total_impressions'] > 0) ? round(($campaign['total_clicks'] / $campaign['total_impressions']) * 100, 2) : 0 }}</td>
            <td>{{ isset($campaign['total_clicks']) ? $campaign['total_clicks'] : 0 }}</td>
            
            <td>{{ (isset($campaign['total_clicks']) && $campaign['total_clicks'] > 0) ? round(($campaign['total_orders'] / $campaign['total_clicks']) * 100, 2) : 0 }}</td>
            <td>{{ isset($campaign['total_orders']) ? $campaign['total_orders'] : 0 }}</td>

            <td>{{ $campaign['data']->dailyBudget }}</td>
        </tr>
        @endif
        @endforeach
    </tbody>
</table>
