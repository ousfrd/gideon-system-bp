<table id="adgroup_id">
    <thead>
        <tr>
        	<th>Status</th>
            <th>Campaign</th>
            <th>Ad Group</th>
            
            
            <th>Ad Spend <span>USD</span></th>
            <th>Sales <span>USD</span></th>

            <th>Profit <span>USD</span></th>
            <th>ACoS <span>%</span></th>
            <th>CPC <span>USD</span></th>

            <th>Impr.</th>
            <th>CTR <span>%</span></th>
            <th>Clicks</th>
            
            <th>CR <span>%</span></th>
            <th>Orders</th>

            <th>Default Bid</th>
        </tr>
    </thead>
    <tbody>
    	@foreach ($adgroups as $adgroup) 
    	@if(isset($adgroup['data']))
        <tr>
        	<td>{{ $adgroup['data']->state }}</td>
            <td>{{ isset($adgroup['campaign_name']) ? $adgroup['campaign_name'] : '' }}</td>
            <td>{{ $adgroup['data']->name }}</td>
            

            <td>{{ isset($adgroup['total_cost']) ? $adgroup['total_cost'] : 0 }}</td>
            <td>{{ isset($adgroup['total_sales']) ? $adgroup['total_sales'] : 0 }}</td>
    
            <td></td>
            <td>{{ (isset($adgroup['total_sales']) && $adgroup['total_sales'] > 0) ? round(($adgroup['total_cost'] / $adgroup['total_sales']) * 100, 2) : 0.0 }}</td>
            <td>{{ (isset($adgroup['total_clicks']) && $adgroup['total_clicks'] > 0) ? round($adgroup['total_cost'] / $adgroup['total_clicks'], 2) : 0 }}</td>

            <td>{{ isset($adgroup['total_impressions']) ? $adgroup['total_impressions'] : 0 }}</td>

            <td>{{ (isset($adgroup['total_impressions']) && $adgroup['total_impressions'] > 0) ? round(($adgroup['total_clicks'] / $adgroup['total_impressions']) * 100, 2) : 0 }}</td>
            <td>{{ isset($adgroup['total_clicks']) ? $adgroup['total_clicks'] : 0 }}</td>
            
            <td>{{ (isset($adgroup['total_clicks']) && $adgroup['total_clicks'] > 0) ? round(($adgroup['total_orders'] / $adgroup['total_clicks']) * 100, 2) : 0 }}</td>
            <td>{{ isset($adgroup['total_orders']) ? $adgroup['total_orders'] : 0 }}</td>

            <td>{{ $adgroup['data']->defaultBid }}</td>
        </tr>
        @endif
        @endforeach
    </tbody>
</table>
