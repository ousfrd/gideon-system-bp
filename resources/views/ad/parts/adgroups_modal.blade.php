 <div class="modal fade" id="adgroupsModal" tabindex="-1" role="dialog" aria-labelledby="adgroupsModal">
<form class="" method="Post" id="adgroupsForm">
{{Form::token()}}  
  <div class="modal-dialog" role="document">
    <div class="modal-content"  style="background: #fff">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Create Negative Keywords</h4>
      </div>
      <div class="modal-body">
       
      <div class="form-group row">
          {{ Form::label("Match Type", null, ['class' => 'col-sm-3 control-label']) }}
          <div class="col-sm-9">
             {{ Form::select('data[matchType]', ['negativePhrase'=>'negativePhrase','negativeExact'=>'negativeExact'], array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
          </div>
      </div>

      <div class="form-group row">
          {{ Form::label("State", null, ['class' => 'col-sm-3 control-label']) }}
          <div class="col-sm-9">
             {{ Form::select('data[state]', ['enabled'=>'Enabled','paused'=>'Paused','archived'=>'Archived'], array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
          </div>
      </div>

      <div class="form-group row">
          {{ Form::label("Keywords", null, ['class' => 'col-sm-3 control-label']) }}
          <div class="col-sm-9">
             {{ Form::textarea('data[keywords]', '', array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
          </div>
      </div>

		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" id="expenseModelBtn" class="btn btn-primary">Submit</button>
      </div>
      
      <div class="modal-spinner"><div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div></div>
      
    </div>
  </div>
</form>
</div>