@extends('layouts.master')
@section('title', $title)
@section('content')

@include('ad.parts.filters')

<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#campaigns"> <i class="fa fa-line-chart" aria-hidden="true"></i> Campaigns</a></li>
  <li><a data-toggle="tab" href="#adgroups"><i class="fa fa-table" aria-hidden="true"></i> Ad Groups</a></li>
  <li><a data-toggle="tab" href="#keywords"><i class="fa fa-table" aria-hidden="true"></i> Keywords</a></li>
  <li><a data-toggle="tab" href="#searchterms"><i class="fa fa-table" aria-hidden="true"></i> Searchterms</a></li>
  <li><a data-toggle="tab" href="#products"><i class="fa fa-table" aria-hidden="true"></i> Products</a></li>
  <li><a data-toggle="tab" href="#negative-keywords"><i class="fa fa-table" aria-hidden="true"></i> Negative Keywords</a></li>
  
 </ul>
<div class="tab-content">
	<div id="campaigns" class="tab-pane active">
		{!! $campaigns_grid !!}
	</div>
	<div id="adgroups" class="tab-pane">
		{!! $adgroups_grid !!}
	</div>
	<div id="keywords" class="tab-pane">
		{!! $adkeywords_grid !!}
	</div>
	<div id="searchterms" class="tab-pane">
		{!! $adsearchterms_grid !!}
	</div>
	<div id="products" class="tab-pane">
		{!! $adproducts_grid !!}
	</div>
	<div id="negative-keywords" class="tab-pane">
		{!! $negative_adkeywords_grid !!}
	</div>
</div>

<script type="text/javascript">
	$('.tab-content form').on( "submit", function( event ) {
		$('<input />').attr('type', 'hidden').attr('name', "account").attr('value', '{{$account_id}}').appendTo($(this));
		$('<input />').attr('type', 'hidden').attr('name', "marketplace").attr('value', '{{$marketplace}}').appendTo($(this));
		$('<input />').attr('type', 'hidden').attr('name', "start_date").attr('value', '{{$start_date}}').appendTo($(this));
		$('<input />').attr('type', 'hidden').attr('name', "end_date").attr('value', '{{$end_date}}').appendTo($(this));
		$('<input />').attr('type', 'hidden').attr('name', "date_range").attr('value', '{{$date_range}}').appendTo($(this));
      	return true;
	});

</script>


@endsection