@extends('layouts.master')
@section('title', $title)
@section('content')


@include('ad.parts.filters')


@include('ad.parts.line_charts')


<ul class="nav nav-tabs" style="padding-top: 40px;">
  <li class="active"><a data-toggle="tab" href="#campaigns"> <i class="fa fa-line-chart" aria-hidden="true"></i> Campaigns</a></li>
  <li><a data-toggle="tab" href="#adgroups"><i class="fa fa-table" aria-hidden="true"></i> Ad Groups</a></li>
</ul>
<div class="tab-content">
	<div id="campaigns" class="tab-pane active">
		{!! $campaigns_grid !!}
	</div>
	<div id="adgroups" class="tab-pane">
		{!! $adgroups_grid !!}
	</div>
</div>

<script type="text/javascript">
	$('.tab-content form').on( "submit", function( event ) {
		$('<input />').attr('type', 'hidden').attr('name', "account").attr('value', '{{$account_id}}').appendTo($(this));
		$('<input />').attr('type', 'hidden').attr('name', "marketplace").attr('value', '{{$marketplace}}').appendTo($(this));
		$('<input />').attr('type', 'hidden').attr('name', "start_date").attr('value', '{{$start_date}}').appendTo($(this));
		$('<input />').attr('type', 'hidden').attr('name', "end_date").attr('value', '{{$end_date}}').appendTo($(this));
		$('<input />').attr('type', 'hidden').attr('name', "date_range").attr('value', '{{$date_range}}').appendTo($(this));
      	return true;
	});

</script>
  


@endsection