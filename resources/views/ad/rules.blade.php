@extends('layouts.master')
@section('title', $title)
@section('content')

<script src="https://unpkg.com/ag-grid-community/dist/ag-grid-community.min.noStyle.js"></script>
<link rel="stylesheet" href="https://unpkg.com/ag-grid-community/dist/styles/ag-grid.css">
<link rel="stylesheet" href="https://unpkg.com/ag-grid-community/dist/styles/ag-theme-balham.css">



              <!-- .page-title-bar -->
              <header class="page-title-bar">

                <!-- title and toolbar -->
                <div class="d-sm-flex align-items-sm-center">
                  <!-- .btn-toolbar -->
                  <div id="dt-buttons" class="btn-toolbar"><a href="{{route('ad.rules.new')}}"><button type="button" class="btn btn-primary">Create New Rule</button></a></div>

                  <!-- /.btn-toolbar -->
                </div>
                <!-- /title and toolbar -->
              </header>
              <!-- /.page-title-bar -->

              
              <div id="myGrid" style="height: 500px;width:100%;margin-top:20px" class="ag-theme-balham"></div>

                <script type="text/javascript" charset="utf-8">
                // specify the columns
                var columnDefs = [
                    {headerName: "ID", field: "id"},
                    {headerName: "Active", field: "rule_active"},
                    {headerName: "Action", field: "change_campaign_open"},
                    {headerName: "Scheduled Time", field: "exec_time"},
                    {headerName: "Edit", field: "action_links", 
                        cellRenderer:function (params) {  
                            
                            return '<a href="'+ params.value.edit +'" >Edit</a>'}
                    
                    },
                    {headerName: "Delete", field: "action_links",
                        cellRenderer:function (params) {  
                            var message = "Do you want to delete this Rule?";
                            return '<a href="'+ params.value.delete +'" onClick="return confirmDel()" >Delete</a>'}
                    
                    },                    
                    {headerName: "Account", field: "seller_name"},
                    {headerName: "Word", field: "containedword"},
                    {headerName: "State", field: "state"},
                    {headerName: "Type", field: "targetingType"},
                    {headerName: "ASIN", field: "asin"},
//                    {headerName: "Manager", field: "manager_name"},
//                    {headerName: "user_id", field: "user_id"},
                   
                    
                    
                ];
                
                // specify the data
                var rowData = [];
                
                // let the grid know which columns and what data to use
                var gridOptions = {
                    columnDefs: columnDefs,
                    rowData: rowData
                };

                // lookup the container we want the Grid to use
                var eGridDiv = document.querySelector('#myGrid');

                // create the grid passing in the div to use together with the columns & data we want to use
                new agGrid.Grid(eGridDiv, gridOptions);

                fetch('/ads/rules/fetchrules').then(function(response) {
                    return response.json();
                }).then(function(data) {

                     gridOptions.api.setRowData(data);
                })                

                function confirmDel(){

                  if(confirm("Do you want to delete this Ad Rule")){

                    return true

                  }else{

                    return false;
                  }

                }
                </script>

                
  


@endsection