@extends('layouts.master')
@section('title', $title)
@section('content')
{!! $grid !!}
<div class="btn-actions">
  <button class="btn btn-primary pull-right" onclick="window.location='{{ route('product-category.create') }}'" data-toggle="modal" data-target="#newRedeemModal">New</button>
</div>
<!-- Modal -->
<div class="modal fade" id="confirmDelete" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Are you sure you want to delete <b id="categoryName"></b>?</h4>
      </div>
      <div class="modal-body">
        <p class="text-default">Please input the category name below and then click delete if you really want to delete this category</p>
        <input type="text" id="confimName" class="form-control">
        <p id="wrong-brand-name" class="text-danger">Your input is not exactly the same as the category name</p>
      </div>
      <div class="modal-footer">
        <i id="spinner" class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>
        <div id="btn-group">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger" id="deleteBtn">Delete</button>
        </div>
      </div>
    </div>
    
  </div>
</div>

<script type="text/javascript">

$(document).ready(function(){

    $('.column-status').each(function(){
        if($(this).find('.low-stock').length>0){
            $(this).parents('tr').addClass('bg-warning')
        }
    })
    $('.pUpdate').editable({
        savenochange:false,
        type: 'text',
        url:'{{route('product-category.ajaxsave')}}',  
        //title: 'Enter value',
        placement: 'top', 
        send:'auto',
        ajaxOptions: {
          dataType: 'json'
        },
        success: function(response, newValue) {
          console.log(response);
        },
        error: function(response, newValue) {
          if(response.status === 500) {
            return 'Service unavailable. Please try later.';
          } else {
            return response.responseText;
          }
        } 
    })


    $('#confirmDelete').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        let categoryId = button.data('categoryId'); // Extract info from data-* attributes
        let categoryName = button.data('categoryName');
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        let modal = $(this);
        modal.find('#wrong-campaign-name').hide();
        modal.find('#btn-group').show();
        modal.find('#spinner').hide();
        modal.find('#categoryName').text(categoryName);
        let input = modal.find('#confimName');
        $('#deleteBtn').on('click', function() {
        if (input.val().replace(/\s/g, "") == categoryName.replace(/\s/g, "")) {
            modal.find('#wrong-category-name').hide();
            modal.find('#btn-group').hide();
            modal.find('#spinner').show();
            $.ajax({
            url: "/product_categories/"+categoryId,
            type: 'DELETE',
            success: function(res){
                button.closest('tr').remove();
                modal.modal('hide');
            },
            error: function(err) {
                modal.find('#btn-group').show();
                modal.find('#spinner').hide();
                console.log(err);
            }
            });
            
        } else {
            modal.find('#wrong-category-name').show();
        }
        })
    });

    $("#confirmDelete").on('hide.bs.modal' , function (event) {
        let modal = $(this);
        modal.find('#confimName').val("");
    })
})
</script>
@endsection