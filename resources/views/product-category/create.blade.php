@extends('layouts.master')
@section('title', 'New Product Category')
@section('content')
<div class="row">
	<div class="col-md-4 mx-auto">
        <form role="form" action="{{route('product-category.store')}}" method="post">
            {{Form::token()}}
            <div class="form-group">
                <label for="category">Category:</label>
                <input required type="text" name="category" class="form-control" id="category">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
@endsection
