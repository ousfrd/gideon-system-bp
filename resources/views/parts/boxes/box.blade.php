<div class="stat">
    <div class="row">
        <div class="col-xs-12">
            <small class="stat-label" style="font-size: 13px;">
                <span class="range_title">{{$data_range}}:</span>
                @if($data_range == 'Lifetime')
                Lifetime
                @else
                {{date('m/d/Y',strtotime($start_date))}} <span style="text-transform: none;">to</span> {{date('m/d/Y',strtotime($end_date))}}
                @endif
                </small>
            <div class="mb10"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-8">
            <small class="stat-label">Gross Sales</small>
            <?php $gross_sales = $orders->total_gross+$orders->tax+$orders->shipping_fee ?>
            <h1>${{number_format($gross_sales,2)}}</h1>
            <hr>
            <small class="stat-label">Est. Profit
                <span class="fa fa-info-circle" tabindex="0" role="button" data-toggle="popover" data-html="true" data-container=".panel.panel-success" data-cb-function="popoverContent" data-trigger="focus" data-placement="bottom" data-element="#{{date('Y-m-d',strtotime($start_date))}}Pop" data-original-title="" title=""></span>            </small>
            <div id="{{date('Y-m-d',strtotime($start_date))}}Pop" class="hidden">
			    <table class="table stats-popover">
			        <tbody>
			        
			        <tr>
			            <td><strong>Total:</strong></td>
			            <td class="text-right text-success">${{number_format($orders->total_gross,2)}}</td>
			        </tr>
			        <tr>
			            <td><strong>Tax:</strong></td>
			            <td class="text-right text-muted">${{number_format($orders->tax,2)}}</td>
			        </tr>		
			        <tr>
			            <td><strong>Shipping:</strong></td>
			            <td class="text-right text-success">${{number_format($orders->shipping_fee,2)}}</td>
					</tr>
			        <tr>
			            <td><strong>VAT Fees:</strong></td>
			            <td class="text-right text-danger">(${{number_format(abs($orders->vat_fee),2)}})</td>
			        </tr>						       
			        <tr>
			            <td><strong>FBA Fees:</strong></td>
			            <td class="text-right text-danger">(${{number_format(abs($orders->fba_fee),2)}})</td>
			        </tr>
			        <tr>
			            <td><strong>Refer Fees:</strong></td>
			            <td class="text-right text-danger">(${{number_format(abs($orders->commission),2)}})</td>
			        </tr>
			        <tr>
			            <td><strong>Promo Value:</strong></td>
			            <td class="text-right text-danger">(${{number_format(abs($orders->total_promo),2)}})</td>
			        </tr>
			        <tr>
			            <td><strong>Product Refunds:</strong></td>
			            <td class="text-right text-danger">(${{number_format(abs($orders->total_refund),2)}})</td>
			        </tr>
			        @if($orders->giftwrap_fee)
					<tr>
			            <td><strong>GiftWrap Fees:</strong></td>
			            <td class="text-right text-danger">(${{number_format(abs($orders->giftwrap_fee),2)}})</td>
			        </tr>
			        @endif
			        @if($orders->other_fee)
					<tr>
			            <td><strong>Other Fees:</strong></td>
			            <td class="text-right text-danger">(${{number_format(abs($orders->other_fee),2)}})</td>
			        </tr>
			        @endif
					@if($adExpenses)
			        <tr>
			                <td><strong>Sponsored Ads:</strong></td>
			                <td class="text-right text-danger">(${{number_format($adExpenses,2)}})</td>
			        </tr>
			        @endif

			        
			        
			        @if($otherExpenses)
			        <tr>
			                <td><strong>Other Expenses:</strong></td>
			                <td class="text-right text-danger">(${{number_format($otherExpenses,2)}})</td>
			        </tr>
			        @endif

			        @if($dealExpenses)
			        <tr>
			                <td><strong>Lightning Deals:</strong></td>
			                <td class="text-right text-danger">(${{number_format($dealExpenses,2)}})</td>
			        </tr>
			        @endif

			        @if($couponExpenses)
			        <tr>
			                <td><strong>Coupon Expenses:</strong></td>
			                <td class="text-right text-danger">(${{number_format($couponExpenses,2)}})</td>
			        </tr>
			        @endif			        

			        @if($otherFees)
			        <tr>
			                <td><strong>Other Fees:</strong></td>
			                @if($otherFees < 0)
			                <td class="text-right text-success">${{number_format(abs($otherFees),2)}}</td>
			                @else
			                <td class="text-right text-danger">(${{number_format($otherFees,2)}})</td>
			                @endif
			        </tr>
			        @endif	

			        <tr>
			            <td class="doubleBrdr text-right"><strong>Est. Payout:</strong></td>
			            <?php 
			            $amazon_cost = $otherExpenses + $adExpenses + $dealExpenses + $couponExpenses + $otherFees;
			            $payout = $orders->total_gross + $orders->shipping_fee + $orders->vat_fee + $orders->fba_fee + $orders->commission + $orders->total_promo + $orders->total_refund - $amazon_cost;
			            ?>
			            <td class="text-right doubleBrdr text-success"><strong>${{number_format($payout,2)}}</strong></td>
			        </tr>
			        <tr>
			            <td><strong>Product/Ship Costs:</strong></td>
			            <td class="text-right text-danger">(${{number_format($orders->total_cost,2)}})</td>
			        </tr>
			        			        
			        <?php 
			        $product_cost = $orders->total_cost;
			        $profit = $payout-$product_cost;
			        ?>
			        <tr>
			            <td class="doubleBrdr text-right"><strong>Profit:</strong></td>
			            <td class="text-right doubleBrdr text-success">
			                <strong>${{number_format($profit,2)}}</strong>
			            </td>
			        </tr>
			
			        <tr>
			            <td class="doubleBrdr text-right"><strong>Margin:</strong></td>
			            <td class="text-right doubleBrdr text-success">
			                <strong>
			                @if($orders->total_gross > 0)
				            {{number_format(100*$profit/$gross_sales,2)}}%
				            @else
				            N/A
				            @endif
            				</strong>
			            </td>
			        </tr>
			    </tbody></table></div> 
					<h1>${{number_format($profit,2)}} </h1>
					<hr>
        </div>
        <div class="col-xs-4">
            <small class="stat-label">ROI</small>
            <h1>
 			@if($product_cost > 0)
            {{number_format(100*$profit/$product_cost,0)}}%
            @else
            N/A
            @endif
            </h1>

            <hr>

            <small class="stat-label">Margin</small>
            <h1>
            @if($orders->total_gross > 0)
            {{number_format(100*$profit/$gross_sales,0)}}%
            @else
            N/A
            @endif
            </h1>
						<hr>
        </div>
    </div>
    <div class="mb20"></div>
		<div class="row">
			<div class="col-xs-4">
				<small class="stat-label">EST. REVENUE</small>
				<h4>${{ number_format($payout, 2) }}</h4>
				<hr>
			</div>
			<div class="col-xs-4">
				<small class="stat-label">Sponsored Ads</small>
				<h4>${{number_format($adExpenses,2)}}</h4>
				<hr>
			</div>
			<div class="col-xs-4">
				<small class="stat-label">Promo Value</small>
				<h4>${{number_format(abs($orders->total_promo),2)}}</h4>
				<hr>
			</div>
		</div>
		<div class="mb20"></div>
    <div class="row">
        <div class="col-xs-12 text-center">
            <table class="table table_stats">
                <tbody><tr>
                    <td colspan="3" class="padding-bottom-5"><small class="stat-label stat-heading">Orders</small></td>
                    <td colspan="3" class="padding-bottom-5" style="border-left: 1px solid #f1f1f1"><small class="stat-label stat-heading">Units</small></td>
                </tr>
                <tr>
                    <td><small class="stat-label">Tot</small></td>
                    <td><small class="stat-label">Org</small></td>
                    <td><small class="stat-label">PPC</small></td>
                    <td style="border-left: 1px solid #f1f1f1"><small class="stat-label">Tot</small></td>
                    <td><small class="stat-label">Promos</small></td>
                    <td><small class="stat-label">Rfds</small></td>
                </tr>
                <tr>
                    <td><h4>{{number_format($orders->total_orders)}}</h4></td>
                    <td><h4>{{$orders->total_orders - $orders->ppc_count}}</h4></td>
                    <td><h4>{{$orders->ppc_count}}</h4></td>
                    <td style="border-left: 1px solid #f1f1f1"><h4>{{number_format($orders->total_units)}}</h4></td>
                    <td><h4>{{$orders->promo_count}}</h4></td>
                    <td><h4>{{$orders->refund_count}}</h4></td>
                </tr>
            </tbody></table>
        </div>
    </div>

		@if ($product)
		<div class="mb20"></div>
		<div class="row">
        <div class="col-xs-12 text-center">
            <table class="table table_stats">
                <tbody><tr>
                    <!-- <td colspan="3" class="padding-bottom-5"><small class="stat-label stat-heading">Mkt Orders</small></td> -->
                    <td colspan="3" class="padding-bottom-5" style="border-left: 1px solid #f1f1f1"><small class="stat-label stat-heading">Mkt Cost</small></td>
                </tr>
                <tr>
                    <td><small class="stat-label">SRO</small></td>
                    <td><small class="stat-label">IRO</small></td>
                    <td><small class="stat-label">CRO</small></td>
                    <td style="border-left: 1px solid #f1f1f1"><small class="stat-label">SRC</small></td>
                    <td><small class="stat-label">IRC</small></td>
                    <td><small class="stat-label">CRC</small></td>
                </tr>
                <tr>
                    <td><h4>{{number_format($product->selfReviewCount['reviews'])}}/{{number_format($product->selfReviewCount['total'])}}</h4></td>
                    <td><h4>{{number_format($product->inviteReviewCount['reviews'])}}/{{number_format($product->inviteReviewCount['total'])}}</h4></td>
                    <td>
											<h4>
												{{number_format($product->claimReviewCount['reviews'])}}/{{number_format($product->claimReviewCount['total'])}}
												<br>
												{{number_format($product->claimReviewCount['insert_card'])}}
											</h4>
										</td>
                    <td style="border-left: 1px solid #f1f1f1"><h4>{{number_format($product->selfReviewCount['reviews'] * $product->self_review_order_cost) }}</h4></td>
                    <td><h4>{{number_format($product->inviteReviewCount['reviews'] * $product->invite_review_order_cost) }}</h4></td>
                    <td><h4>{{number_format($product->claimReviewCount['reviews'] * $product->claim_review_order_cost) }}</h4></td>
                </tr>
            </tbody></table>
        </div>
    	</div>
		@endif
   

</div>
<script type="text/javascript">
$('[data-toggle="popover"]').popover({
	content:function(){
		var elmt = $(this).attr('data-element')
		return $(elmt).html()

	}
})
</script>
        