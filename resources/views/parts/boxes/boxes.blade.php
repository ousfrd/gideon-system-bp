<div class="mb20"></div>

<div class="row merchant-dash-ajax" id="periodic-gross-sales">
                
                @if ($start_date === $end_date)
                <div class="col-md-3 col-sm-6 periodic-gross-sales" data-box="boxOne"
                data-href="{{route('order.call_backs.periodicGrossSales')}}"
                 data-range="{{$date_range}}"
                 data-range-date="{{$start_date}}"
                 data-end-date="{{$end_date}}"
                 data-options='{!! json_encode($params) !!}'
                >
                    <div class="panel panel-success dark-bottom panel-stat">
                        <div class="panel-heading border-bottom-0">
                            <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 periodic-gross-sales" data-box="boxTwo"
              data-href="{{route('order.call_backs.periodicGrossSales')}}"
                 data-range="1 Day"
                 data-range-date="{{date('Y-m-d',strtotime($start_date) - 24 * 3600)}}"
                 data-end-date="{{date('Y-m-d',strtotime($end_date) - 24 * 3600)}}"
                 data-options='{!! json_encode($params) !!}'
              >
                    <div class="panel panel-primary dark-bottom panel-stat">
                        <div class="panel-heading border-bottom-0">
                            <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 periodic-gross-sales" data-box="boxThree"
              data-href="{{route('order.call_backs.periodicGrossSales')}}"
                 data-range="7 DAY"
                 data-range-date="{{date('Y-m-d',strtotime($start_date) - 7 * 24 * 3600)}}"
                 data-end-date="{{date('Y-m-d',strtotime($end_date) - 1 * 24 * 3600)}}"
                 data-options='{!! json_encode($params) !!}'
              >
                    <div class="panel panel-warning dark-bottom panel-stat">
                        <div class="panel-heading border-bottom-0">
                            <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 periodic-gross-sales" data-box="boxFour"
               data-href="{{route('order.call_backs.periodicGrossSales')}}"
                 data-range="30 DAY"
                  data-range-date="{{date('Y-m-d',strtotime($start_date) - 30 * 24 * 3600)}}"
                 data-end-date="{{date('Y-m-d',strtotime($end_date) - 1 * 24 * 3600)}}"
                 data-options='{!! json_encode($params) !!}'
               >
                    <div class="panel panel-danger dark-bottom panel-stat">
                        <div class="panel-heading border-bottom-0">
                            <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                        </div>
                    </div>
                </div>

                @else
                <div class="col-md-4 col-sm-6 periodic-gross-sales" data-box="boxOne"
                data-href="{{route('order.call_backs.periodicGrossSales')}}"
                 data-range="{{$date_range}}"
                 data-range-date="{{$start_date}}"
                 data-end-date="{{$end_date}}"
                 data-options='{!! json_encode($params) !!}'
                >
                    <div class="panel panel-success dark-bottom panel-stat">
                        <div class="panel-heading border-bottom-0">
                            <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 periodic-gross-sales" data-box="boxTwo"
              data-href="{{route('order.call_backs.periodicGrossSales')}}"
                 data-range="Same Time Prev Week"
                 data-range-date="{{date('Y-m-d',strtotime($start_date) - 7 * 24 * 3600)}}"
                 data-end-date="{{date('Y-m-d',strtotime($end_date) - 7 * 24 * 3600)}}"
                 data-options='{!! json_encode($params) !!}'
              >
                    <div class="panel panel-primary dark-bottom panel-stat">
                        <div class="panel-heading border-bottom-0">
                            <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 periodic-gross-sales" data-box="boxThree"
               data-href="{{route('order.call_backs.periodicGrossSales')}}"
                 data-range="Same Time Prev Month"
                  data-range-date="{{date('Y-m-d',strtotime($start_date) - 30 * 24 * 3600)}}"
                 data-end-date="{{date('Y-m-d',strtotime($end_date) - 30 * 24 * 3600)}}"
                 data-options='{!! json_encode($params) !!}'
               >
                    <div class="panel panel-warning dark-bottom panel-stat">
                        <div class="panel-heading border-bottom-0">
                            <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                        </div>
                    </div>
                </div>
                @endif
</div>
                                            
                                            
<style>
.periodic-gross-sales .panel-heading .preloader, #merchant-products .preloader{width:44px;height:44px;top:50%;left:50%;margin-top:-22px;margin-left:-22px}
</style>

<script>
$(document).ready(function(){
	$('.merchant-dash-ajax .periodic-gross-sales').each(function(){
		if ($(this).attr('data-href') != null) {
			var params = $.parseJSON($(this).attr('data-options'));
			params['start_date'] = $(this).attr('data-range-date');
			params['end_date'] = $(this).attr('data-end-date');
			params['data_range'] = $(this).attr('data-range');
			$(this).find('.panel-heading').load($(this).attr('data-href'),params,function(){})
		}

	})
})
</script>