@foreach ($bestsellers as $bestseller)

        <div class="col-md-4">
                    <div class="panel panel-default panel-dark product{{$bestseller->product_id}}">
                        <div class="panel-heading">
                            <h4 class="panel-title clearfix">
                                <span class="tip col-md-10 col-xs-10 nopadding" data-toggle="tooltip" data-placement="bottom" style="cursor:pointer;" title="{{ $bestseller->product_name }}">
                                      <small>{{$bestseller->asin}}</small>: {{ $bestseller->product->alias ? $bestseller->product->alias : str_limit($bestseller->product->name, $limit = 50, $end = '..') }} </span>
                               
                                <div class="pull-right dash-icons col-md-2 col-xs-2 nopadding">
                                    <a class="dropdown-toggle" id="dashboard-dropdown-toggle" href="#" data-toggle="dropdown" title="View Dashboard" style="margin-right: 5px;"><i class="fa fa-list-alt"></i></a>  
                                            <ul class="dropdown-menu">
                                                    <li>
                                                        <a target="_blank" data-toggle="tooltip" data-placement="bottom" href="{{ route ( 'product.view', [ 'id' => $bestseller->product_id ] ) }}">View Dashboard</a>
                                                    </li>
                                            
                                            </ul>
                                    <div class="pull-right">
                                            <a class="dropdown-toggle" id="user-dropdown-toggle" href="#" data-toggle="dropdown" title="View On Amazon">
                                                <i class="fa fa-external-link"></i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                    <li>
                                                        <a target="_blank" data-toggle="tooltip" data-placement="bottom" href="{{$bestseller->product->amazonLink()}}">{{ucfirst($bestseller->marketplace)}}</a>
                                                    </li>
                                            
                                            </ul>
                                    </div>
                                                                
                                </div>
                            </h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4 col-xs-4">
                                <?php $all = $bestseller->total_gross_sales + $bestseller->total_promo_value + abs($bestseller->total_refund); ?>
                                <h5 class="inverse">Gross Sales</h5>
                                    <span class="sublabel">
                                        <strong>Full</strong>
                                        <small>(${{number_format($bestseller->total_gross_sales,2)}})</small>
                                    </span>
                                <?php $rate = round($bestseller->total_gross_sales / $all * 100, 3);?>
                                <div class="progress-sm progress"><div aria-valuenow="<?php echo $rate ?>" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-success"></div></div>                        <span class="sublabel">
                                        <strong>Promos</strong><br>
                                        <small>(${{number_format($bestseller->total_promo_value,2)}})</small>
                                    </span>
                                <?php $rate = round($bestseller->total_promo_value / $all * 100, 3)?>
                                <div class="progress-sm progress"><div aria-valuenow="<?php echo $rate ?>" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-hpgreen"></div></div>                        <span class="sublabel">
                                        <strong>RFDS</strong>
                                        <small class="text-danger">(${{number_format(abs($bestseller->total_refund),2)}})</small>
                                    </span>
                                <?php $rate = round(abs($bestseller->total_refund) / $all * 100, 3)?>
                                <div class="progress-sm progress"><div aria-valuenow="<?php echo $rate ?>" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-danger"></div></div>
                            </div>


                            <div class="col-md-4 col-xs-4">
                                <?php $full = $bestseller->total_units - $bestseller->total_promo_orders - $bestseller->total_refunded_units ?>
                                <h5 class="inverse">Units</h5>
                                    <span class="sublabel">
                                        <strong>Full</strong>
                                        <small>({{ $full }})</small>
                                    </span>
                                <?php $rate = round($full / $bestseller->total_units * 100, 3)?>
                                <div class="progress-sm progress"><div aria-valuenow="<?php echo $rate ?>" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-primary"></div></div>
                                <span class="sublabel">
                                        <strong>Promos</strong>
                                        <small>({{ $bestseller->total_promo_orders }})</small>
                                    </span>
                                <?php $rate = round($bestseller->total_promo_orders /$bestseller->total_units * 100, 3)?>
                                <div class="progress-sm progress"><div aria-valuenow="<?php echo $rate ?>" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-info"></div></div>
                                <span class="sublabel">
                                        <strong>RFDS</strong>
                                        <small>({{$bestseller->total_refunded_units}})</small>
                                    </span>
                                <?php $rate = round($bestseller->total_refunded_units / $bestseller->total_units * 100, 3)?>
                                <div class="progress-sm progress"><div aria-valuenow="<?php echo $rate ?>" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-warning"></div></div>
                            </div>
                            
                            <div class="col-md-4 col-xs-4">
                                <h5 class="inverse">Orders</h5>
                                <span class="sublabel">
                                        <strong>TOT</strong>
                                        <small>({{$bestseller->total_orders}})</small>
                                    </span>
                                <div class="progress-sm progress"><div aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: 100%;" class="progress-bar progress-bar-hpgreen-dark"></div></div>
                                <span class="sublabel">
                                        <strong>ORG</strong>
                                        <small>({{$bestseller->organic_orders}})</small>
                                    </span>
                                    <?php $rate = ($bestseller->total_orders == 0) ? 0 : round(($bestseller->total_orders - $bestseller->ad_orders) / $bestseller->total_orders * 100, 3)?>
                                <div class="progress-sm progress"><div aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-hpgreen-med"></div></div>
                                <span class="sublabel">
                                        <strong>PPC</strong>
                                        <small>({{$bestseller->ad_orders}})</small>
                                    </span>
                                    <?php $rate = ($bestseller->total_orders == 0) ? 0 : round($bestseller->ad_orders / $bestseller->total_orders * 100, 3)?>
                                <div class="progress-sm progress"><div aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-hpgreen-lgt"></div></div>                    </div>
                            <div class="clearfix"></div>

                        </div>
                        <!--<div class="panel-footer panel-stat">
                            <div class="row">
                                <div class="col-md-12 col-xs-12 mp-latest-bsr">
                                                            </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>-->
                        <div class="panel-footer panel-stat">
                            <ul class="list-unstyled nomargin clearfix">
                                <li class="col-md-4 col-xs-4 text-center">
                                    <small class="stat-label">Gross</small>
                                    <h4 style="font-size: 16px;">${{number_format($bestseller->total_gross_sales,2)}}</h4>
                                </li>
                                <li class="col-md-3 col-xs-3 text-center">
                                    <small class="stat-label">
                                        Profit
                                        <span class="fa fa-info-circle" tabindex="0" role="button" data-toggle="popover" data-html="true" data-container=".panel.panel-default.product{{$bestseller->product_id}}" data-cb-function="popoverContent" data-trigger="focus" data-placement="bottom" data-element="#product{{$bestseller->product_id}}" data-original-title="" title=""></span>                            </small>
                                    <div id="product{{$bestseller->product_id}}" class="hidden">
            <table class="table stats-popover">
                <tbody><tr>
                    <td><strong>Total:</strong></td>
                    <td class="text-right text-success">${{number_format($bestseller->total_gross_sales,2)}}</td>
                </tr>
                <tr>
                    <td><strong>Tax:</strong></td>
                    <td class="text-right text-muted">${{number_format($bestseller->total_tax,2)}}</td>
                </tr>
                <tr>
                    <td><strong>Shipping:</strong></td>
                    <td class="text-right">${{number_format($bestseller->total_shipping,2)}}</td>
                </tr>
                <tr>
                    <td><strong>FBA Fees:</strong></td>
                    <td class="text-right text-danger">(${{number_format(abs($bestseller->fba_fee),2)}})</td>
                </tr>
                <tr>
                    <td><strong>Refer Fees:</strong></td>
                    <td class="text-right text-danger">(${{number_format(abs($bestseller->total_refer_fee),2)}})</td>
                </tr>
                <tr>
                    <td><strong>Promo Value:</strong></td>
                    <td class="text-right text-danger">(${{number_format(abs($bestseller->total_promo_value),2)}})</td>
                </tr>
                <tr>
                    <td><strong>Product Refunds:</strong></td>
                    <td class="text-right text-danger">(${{number_format(abs($bestseller->total_refund),2)}})</td>
                </tr>

                <tr>
                    <td><strong>Sponsored Ads:</strong></td>
                    <td class="text-right text-danger">(${{number_format($bestseller->ads_cost,2)}})</td>
                </tr>
                <tr>
                    <td class="doubleBrdr text-right"><strong>Est. Payout:</strong></td>
                    
                    <td class="text-right doubleBrdr text-success"><strong>${{number_format($bestseller->total_revenue,2)}}</strong></td>
                </tr>
                <tr>
                    <td><strong>Product/Ship Costs:</strong></td>
                    <td class="text-right text-danger">(${{number_format($bestseller->total_cost,2)}})</td>
                </tr>

                
                
                <tr>
                    <td class="doubleBrdr text-right"><strong>Profit:</strong></td>
                    <td class="text-right doubleBrdr text-success">
                        <strong>${{number_format($bestseller->total_profit,2)}}</strong>
                    </td>
                </tr>

                <tr>
                    <td class="doubleBrdr text-right"><strong>Margin:</strong></td>
                    <td class="text-right doubleBrdr text-success">
                        <strong>
                        @if($bestseller->total_gross_sales > 0)
                        {{number_format(100*$bestseller->total_profit/$bestseller->total_gross_sales,2)}}%
                        @else
                        N/A
                        @endif
                        </strong>
                    </td>
                </tr>
            </tbody></table>
        </div>                            <h4 style="font-size: 16px;" class="text-success">${{number_format($bestseller->total_profit,2)}}</h4>
                                </li>
                                <li class="col-md-2 col-xs-2 text-center">
                                    <small class="stat-label">ROI</small>
                                    <h4 style="font-size: 16px;" class="text-success">
                                    @if($bestseller->total_cost > 0)
                                    {{number_format(100*$bestseller->total_profit/$bestseller->total_cost,0)}}%
                                    @else
                                    N/A
                                    @endif                           
                                    </h4>
                                </li>
                                <li class="col-md-3 col-xs-3 text-center">
                                    <small class="stat-label">Units</small>
                                    <h4 style="font-size: 16px;">{{number_format($bestseller->total_units)}}</h4>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>


        @endforeach


<script type="text/javascript">
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
    
$('[data-toggle="popover"]').popover({
	content:function(){
		var elmt = $(this).attr('data-element')
		return $(elmt).html()

	}
})
</script>