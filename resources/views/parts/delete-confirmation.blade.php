<!-- Modal -->
<div class="modal fade" id="confirmDelete" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Are you sure you want to delete <b id="keyValue"></b>?</h4>
      </div>
      <div class="modal-body">
        <p class="text-default">Please input the <span id="keyName"></span> below and then click delete if you really want to delete it. </p>
        <input type="text" id="confimKeyValue" class="form-control">
        <p id="wrongKeyValue" class="text-danger">Your input is not exactly the same as <b id="keyValue"></b></p>
      </div>
      <div class="modal-footer">
        <i id="spinner" class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>
        <div id="btn-group">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger" id="deleteBtn">Delete</button>
        </div>
      </div>
    </div>
    
  </div>
</div>

<script>
  $('#confirmDelete').on('show.bs.modal', function (event) {
    const modal = $(this);
    const button = $(event.relatedTarget);
    const url = button.data('url');
    const keyName = button.data('key-name');
    const keyValue = button.data('key-value');
    modal.find('#keyValue').text(keyValue);
    modal.find('#keyName').text(keyName);
    modal.find('#wrongKeyValue').hide();
    modal.find('#btn-group').show();
    modal.find('#spinner').hide();
    let input = modal.find('#confimKeyValue');
    $('#deleteBtn').off('click');
    $('#deleteBtn').on('click', function() {
      if (input.val().replace(/\s/g, "") == keyValue.replace(/\s/g, "")) {
        modal.find('#wrongKeyValue').hide();
        modal.find('#btn-group').hide();
        modal.find('#spinner').show();
        $.ajax({
          url: url,
          type: 'DELETE',
          success: function(res){
            button.closest('tr').remove();
            modal.modal('hide');
            input.val("");
          },
          error: function(err) {
            modal.find('#btn-group').show();
            modal.find('#spinner').hide();
          }
        });

      } else {
        modal.find('#wrongKeyValue').show();
      }
    })

  })
</script>