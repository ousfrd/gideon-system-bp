<div id="reportDateRange" style="background: #fff; cursor: pointer; padding: 2px 10px; border: 1px solid #ccc;" class="date-range-picker pull-left"
	                             data-limit="{months: 1}"
	                             data-opens="left"
	                             data-container-class="dateRangePickerCont"
	                             data-max-date="{{date('m/d/Y')}}"
	                             data-on-apply="reportDateRange">
	
	                            <i class="fa fa-calendar"></i>
	                            <span>
	                                <span class="rangeText">Today</span>:
	                                <span class="startText">Aug 6, 2017</span>  -
	                                <span class="endText">Aug 6, 2017</span>
	                            </span> <b class="caret"></b>
	                        </div> 
<script>
$(function() {

    var start = moment('{{$start_date}}');
    var end = moment('{{$end_date}}');
    var label = '{{$date_range}}'
        
        function cb(start, end, label) {
        	$('#reportDateRange span.startText').html(start.format('MMM D, Y'))
            $('#reportDateRange span.endText').html(end.format('MMM D, Y'))
            $('#reportDateRange span.rangeText').html(label)
        }
  
    $('#reportDateRange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
        	'Today': [moment(), moment()],
        	'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        	 'Last 7 Days': [moment().subtract(7, 'days'), moment().subtract(1, 'days')],
        	 'Last 30 Days': [moment().subtract(30, 'days'), moment().subtract(1, 'days')],
        	 'Last 60 Days': [moment().subtract(60, 'days'), moment().subtract(1, 'days')],
        	 'Last 90 Days': [moment().subtract(90, 'days'), moment().subtract(1, 'days')],
        	 'This Month': [moment().startOf('month'), moment().endOf('month')],
        	 'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        	 
             'This Week': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
             'Last Week': [moment().subtract(1, 'weeks').startOf('isoWeek'), moment().subtract(1, 'weeks').endOf('isoWeek')],
             "Year To Date":[moment().startOf('year'),moment()]
             
        }
    }, cb).on('apply.daterangepicker', function(ev, picker) {
    	  //do something, like clearing an input
    	 $('input[name="start_date"]').val(picker.startDate.format('YYYY-MM-DD'))
    	 $('input[name="end_date"]').val(picker.endDate.format('YYYY-MM-DD'))
    	 $('input[name="date_range"]').val(picker.chosenLabel)
    	 // $(this).parents('form').submit()
    });

    cb(start, end,label);
})
</script>	                        
	                        