@extends('layouts.master')
@section('title', $title)
@section('content')
{!! $grid !!}
<!-- Modal -->
<div class="modal fade" id="confirmDelete" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Are you sure you want to delete <b id="brandName"></b>?</h4>
      </div>
      <div class="modal-body">
        <p class="text-default">Please input the brand name below and then click delete if you really want to delete this brand</p>
        <input type="text" id="confimName" class="form-control">
        <p id="wrong-brand-name" class="text-danger">Your input is not exactly the same as the brand name</p>
      </div>
      <div class="modal-footer">
        <i id="spinner" class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>
        <div id="btn-group">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger" id="deleteBtn">Delete</button>
        </div>
      </div>
    </div>
    
  </div>
</div>
<script type="text/javascript">

$(document).ready(function(){
//edit form style - popup or inline
//$.fn.editable.defaults.mode = 'inline';


$('.column-status').each(function(){
	if($(this).find('.low-stock').length>0){
		$(this).parents('tr').addClass('bg-warning')
	}
	
	
})

$('#confirmDelete').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget);
    let brandId = button.data('brandId'); // Extract info from data-* attributes
    let brandName = button.data('brandName');
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    let modal = $(this);
    modal.find('#wrong-campaign-name').hide();
    modal.find('#btn-group').show();
    modal.find('#spinner').hide();
    modal.find('#brandName').text(brandName);
    let input = modal.find('#confimName');
    $('#deleteBtn').on('click', function() {
      if (input.val().replace(/\s/g, "") == brandName.replace(/\s/g, "")) {
        modal.find('#wrong-brand-name').hide();
        modal.find('#btn-group').hide();
        modal.find('#spinner').show();
        $.ajax({
          url: "/brands/"+brandId,
          type: 'DELETE',
          success: function(res){
            button.closest('tr').remove();
            modal.modal('hide');
          },
          error: function(err) {
            modal.find('#btn-group').show();
            modal.find('#spinner').hide();
            console.log(err);
          }
        });
        
      } else {
        modal.find('#wrong-brand-name').show();
      }
    })
  });

  $("#confirmDelete").on('hide.bs.modal' , function (event) {
    let modal = $(this);
    modal.find('#confimName').val("");
  })


$('.editable').editable();

$('.pUpdate').editable({
    validate: function(value) {
        if($.trim(value) == '') {
            return 'Value is required.';
        }
	},
	//showbuttons:false,
	//onblur:'submit',
	savenochange:false,
    type: 'text',
    url:'{{route('product.ajaxsave')}}',  
    //title: 'Enter value',
    placement: 'top', 
    send:'auto',
    ajaxOptions: {
   		 dataType: 'json'
    },
    success: function(response, newValue) {
    } 
	})

	$('.pUpdate.unit-cost').on('shown', function(e, editable) {
		var checkbox = $('<div class="input form-group checkbox-success checkbox" style="margin-top: 5px;"><input name="isUnitUpdateOrders" value="1" class="UpdateALLOrders" id="isunitupdateorders" type="checkbox"><label for="isunitupdateorders">Update ALL Orders</label></div>');
		
	    editable.input.$input.parents('form').find('.control-group.form-group').append(checkbox);
	})
	
	$('.pUpdate.unit-cost').editable('option', 'params', function (params) {
		
	    var $isUnitUpdateOrders = $('#isunitupdateorders');
	
	    if($isUnitUpdateOrders.is(':checked')) {
	        params.isUnitUpdateOrders = 1;
	    }
	
	    return params;
	});


var dataSource = {!!json_encode($managerSources)!!}
var sourceArray = {}
var tags = []
$.each(dataSource,function(id,t){
	sourceArray[parseInt(t.id)] = t.text
	tags.push(t.text)
 })


$('.pmanagers').each(function(){
	$(this).editable({
        inputclass: 'input-large',
        type: 'select',
        url:'{{route('product.ajaxsave')}}',
        send:'auto',
        width: '250px',
        source: tags
        // select2: {
        // 	placeholder: 'Select Managers',
            
        //     minimumInputLength: 1,
        	
        // 	tags: tags,
        // 	width: '250px',
           
        // },

        // display: function(value) {
	        
        //     if(value!= null && value.constructor === Array) {
	       //  	$.each(value,function(i){
		        	
	       //          $.each(dataSource,function(id,t){
	                	
	       //              if (t.id.toString() == value[i]) {
	       //              	value[i] = t.text;
	       //              	return false
	       //              } 
	                    
	       //          })
	                
	       //       });
	       //       $(this).html(value.join(", "));
        //     }
        // } 
    })

	}); 

// $('thead .column-managers input').autocomplete({
// 	source: "{{route('utils.ajax.product-managers')}}",
// 	select: function( event, ui ) {
//         $( this ).val( ui.item.text );
       
 
//         return false;
//       }
// }).autocomplete( "instance" )._renderItem = function( ul, item ) {

//       return $( "<li>" )
//         .append( "<div>" + item.text + "</div>" )
//         .appendTo( ul );
//     };


	 
})

//<div class="input form-group checkbox-success checkbox" style="margin-top: 5px;"><input name="isUnitUpdateOrders" value="1" class="UpdateALLOrders" id="isunitupdateorders" type="checkbox"><label for="isunitupdateorders">Update ALL Orders</label></div>
</script>
@endsection