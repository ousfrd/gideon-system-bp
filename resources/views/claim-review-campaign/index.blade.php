@extends('layouts.master')
@section('title', "Claim Review Campaign")
@section('content')
<div class="btn-actions">
  <a href="{{route('claim-review-campaign.create')}}" class="btn btn-primary pull-right"><span class="" >Create New Claim Review Campaign</span></a>
</div>
{!! $grid !!}

<!-- Modal -->
<div class="modal fade" id="confirmDelete" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Are you sure you want to delete <b id="compaignName"></b>?</h4>
      </div>
      <div class="modal-body">
        <p class="text-default">Please input the campaign name below and then click delete if you really want to delete this campaign</p>
        <input type="text" id="confimName" class="form-control">
        <p id="wrong-campaign-name" class="text-danger">Your input is not exactly the same as the campaign name</p>
      </div>
      <div class="modal-footer">
        <i id="spinner" class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>
        <div id="btn-group">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger" id="deleteBtn">Delete</button>
        </div>
      </div>
    </div>
    
  </div>
</div>

<div class="modal fade" id="comfirm-send" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Are you ready to send?</h4>
      </div>
      <div class="modal-body">
        <p>Note: after send, you are not able to edit this campaign any more. please make sure everything is right.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger" id="confirmSendBtn">Send</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

  $('#confirmDelete').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget);
    let campaignId = button.data('campaignId'); // Extract info from data-* attributes
    let campaignName = button.data('campaignName');
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    let modal = $(this);
    modal.find('#wrong-campaign-name').hide();
    modal.find('#btn-group').show();
    modal.find('#spinner').hide();
    modal.find('#compaignName').text(campaignName);
    let input = modal.find('#confimName');
    $('#deleteBtn').on('click', function() {
      if (input.val().replace(/\s/g, "") == campaignName.replace(/\s/g, "")) {
        modal.find('#wrong-campaign-name').hide();
        modal.find('#btn-group').hide();
        modal.find('#spinner').show();
        $.ajax({
          url: "/claim-review-campaign/"+campaignId,
          type: 'DELETE',
          success: function(res){
            button.closest('tr').remove();
            modal.modal('hide');
          },
          error: function(err) {
            modal.find('#btn-group').show();
            modal.find('#spinner').hide();
            console.log(err);
          }
        });
        
      } else {
        modal.find('#wrong-campaign-name').show();
      }
    })
  });

  $("#confirmDelete").on('hide.bs.modal' , function (event) {
    let modal = $(this);
    modal.find('#confimName').val("");
  })

  $('.pUpdate').editable();

  $('#comfirm-send').on('show.bs.modal',  function (event) {
    const button = $(event.relatedTarget);
    const parentTr = button.closest('tr');
    const actualSentAmount = parentTr.find('.column-actual_sent_amount');
    const modal = $(this);
    $('#confirmSendBtn').off('click');
    $('#confirmSendBtn').on('click', () => {
      $.ajax({
        url: button.data('remote-url'),
        type: button.data('remote-method'),
        success: (res) => {
          actualSentAmount.html(actualSentAmount.text());
          parentTr.find('button.send').remove();
          parentTr.find('a.edit').remove();
          parentTr.find('button.delete').remove();
          modal.modal('hide');
        },
        error: (err) => {
          console.log(err);
          alert(err);
          modal.modal('hide');
        }
      })
    })
  })

</script>

@endsection