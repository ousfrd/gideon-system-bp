@extends('layouts.master')
@section('title', "Claim Review Campaign - " . $claimReviewCampaign->name)
@section('info_content')
<ol class="breadcrumb">
  <li><a href="{{route('claim-review-campaign.index')}}">Claim Review Campaign List</a></li>
  <li class="active">{{$claimReviewCampaign->name}}</li>
</ol>
<div class="panel panel-default">
  <div class="panel-heading">
    Campaign Info
    
    @if ($claimReviewCampaign->sent_at)
      <span class="btn btn-default">Sent At : {{$claimReviewCampaign->sent_at}}</span>
    @else
      {{ Form::open(array('url' => route('claim-review-campaign.send', ['id'=>$claimReviewCampaign->id]), 'method' => 'PUT', 'class' => 'hidden', 'id'=>'sendForm')) }}
        <button type="submit" id="sendFormSubmitBtn">Submit</button>
      {{ Form::close() }}
      @if ($claimReviewCampaign->cs_return_address_id)
        @if ($claimReviewCampaign->sent_at)
          <a class="btn btn-default pull-right" href="{{route('claim-review-campaign.review', ['id' => $claimReviewCampaign->id])}}">API Response</a>
        @else
          <a class="btn btn-primary pull-right" href="{{route('claim-review-campaign.review', ['id' => $claimReviewCampaign->id])}}">Send Via API</a>
        @endif
      @endif
      @if (!$claimReviewCampaign->sent_at)
        <button class="btn btn-info pull-right" data-toggle="modal" data-target="#send-via-api">config</button>
      @endif
      <button type="button" class="btn btn-danger delete pull-right" data-toggle="modal" data-target="#confirmDelete" data-campaign-id="{{$claimReviewCampaign->id}}" data-campaign-name="{{$claimReviewCampaign->name}}">Delete</button>
      <a href="{{route('claim-review-campaign.edit', ['claimReviewCampaign' => $claimReviewCampaign])}}" class="btn btn-success pull-right"><span class="" >Edit</span></a>
      <button class="btn pull-right" data-toggle="modal" data-target="#comfirm-send">Mark as Send</button>
    @endif
    
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-xs-12  col-sm-6">
        <table class="table table-striped">
          <tr>
            <th>Name</th>
            <td>{{$claimReviewCampaign->name}}</td>
          </tr>
          <tr>
            <th>Supplier</th>
            <td>
              {{$claimReviewCampaign->platform}} 
              
              @if ($claimReviewCampaign->cs_return_address_id)
              <br />
              <ul class="list-group" style="margin-top: 10px">
                <li class="list-group-item">
                  <div>
                    return address: <br>
                    {!! $returnAddress ? $returnAddress->format() : 'N/A' !!}
                  </div>
                </li>
                @if ($giftCardTemplate->template_type == "letter")
                <li class="list-group-item">
                  <span class="badge">{{$claimReviewCampaign->cs_duplex ? "Yes" : "No"}}</span>
                  duplex: 
                </li>
                <li class="list-group-item">
                  <span class="badge">{{$claimReviewCampaign->cs_colour ? "Yes" : "No"}}</span>
                  colorful: 
                </li>
                <li class="list-group-item">
                  <span class="badge">{{$claimReviewCampaign->cs_priority_post ? "Yes" : "No"}}</span>
                  priority: 
                </li>
                @endif
                <li class="list-group-item">
                  <span class="badge">{{$claimReviewCampaign->cs_delay_hours}}</span>
                  deplay hours: 
                </li>
                <li class="list-group-item">
                  Exclude Promo Discount: {{$claimReviewCampaign->cs_exclude_promotion_discounts}}
                </li>
                <li class="list-group-item">
                  
                </li>
              </ul>
              @endif
            </td>
          </tr>
          <tr>
            <th>Gift Card Amount</th>
            <td>{{$claimReviewCampaign->cashback}}</td>
          </tr>
          <tr>
            <th>Marketplace</th>
            <td>{{$claimReviewCampaign->marketplace}}</td>
          </tr>
          <tr>
            <th>ASIN</th>
            <td>{{$claimReviewCampaign->asin}}</td>
          </tr>
          <tr>
            <th>Date Range</th>
            <td>{{$claimReviewCampaign->start_date}} To {{$claimReviewCampaign->end_date}}</td>
          </tr>
          @if ($giftCardTemplate)
          <tr>
            <th>Gift Card Template</th>
            <td><a target="_blank" href="{{route('gift-card-template.show', ['giftCardTemplate' => $giftCardTemplate])}}">{{$giftCardTemplate->name}}</a></td>
          </tr>
          @endif
          
        </table>
      </div>
      <div class="col-xs-12  col-sm-3">
        <table class="table table-striped">
          <tr>
              <th>Channel</th>
              <td>{{$claimReviewCampaign->channel}}</td>
          </tr>
          <tr>
              <th>Orders Quantity</th>
              <td>{{$claimReviewCampaign->order_quantity}}</td>
          </tr>
          <tr>
              <th>Actual Sent Amount</th>
              <td>
                @if ($claimReviewCampaign->sent_at)
                  {{$claimReviewCampaign->actual_sent_amount}}
                @else
                  <a data-type="text" class="pUpdate"
                     data-pk="actual_sent_amount"
                     data-url="{{route('claim-review-campaign.ajaxUpdate', ['id' => $claimReviewCampaign->id])}}"
                     data-value="{{$claimReviewCampaign->actual_sent_amount}}"
                     data-title="">{{$claimReviewCampaign->actual_sent_amount}}</a>
                @endif
                
              </td>
          </tr>
          <tr>
              <th>Before Send Balance</th>
              <td>{{$claimReviewCampaign->balance_before}}</td>
          </tr>
          <tr>
              <th>After Send Balance</th>
              <td>{{$claimReviewCampaign->balance_after}}</td>
          </tr>
          <tr>
              <th>Cost</th>
              <td>{{$claimReviewCampaign->total_cost}}</td>
          </tr>
          <tr>
              <th>Unit Price</th>
              <td>{{$claimReviewCampaign->unit_price}}</td>
          </tr>
          <tr>
              <th>Promoted Orders Quantity</th>
              <td>{{$claimReviewCampaign->promoted_order_quantity}}</td>
          </tr>
          <tr>
              <th>Responses</th>
              <td>{{$claimReviewCampaign->response_quantity}}</td>
          </tr>
          <tr>
            <th>Response Rate</th>
            <td>{{$claimReviewCampaign->response_rate * 100}}%</td>
          </tr>
          <tr>
            <th>Positive Response Rate</th>
            <td>{{$claimReviewCampaign->positive_response_rate * 100}}%</td>
          </tr>
          <tr>
            <th>Positive Review Rate</th>
            <td>{{$claimReviewCampaign->positive_review_rate * 100}}%</td>
          </tr>
          <tr>
            <th>Free Product Response</th>
            <td><a target="_blank" href="{{route('redeems.claimReviewCampaignFreeProduct', ['claim_review_campaign_id' => $claimReviewCampaign->id])}}">{{$claimReviewCampaign->free_product_quantity}}</a></td>
          </tr>
          <tr>
            <th>Gift Card Response</th>
            <td><a target="_blank" href="{{route('redeems.claimReviewCampaignGiftCard', ['claim_review_campaign_id' => $claimReviewCampaign->id])}}">{{$claimReviewCampaign->gift_card_quantity}}</a></td>
          </tr>
          <tr>
            <th>Negative Response</th>
            <td><a target="_blank" href="{{route('redeems.claimReviewCampaignOther', ['claim_review_campaign_id' => $claimReviewCampaign->id ])}}">{{$claimReviewCampaign->negtive_review_quantity}}</a></td>
          </tr>
          <tr>
            <th>Negative To Positive Reviews</th>
            <td>{{$claimReviewCampaign->negtive_to_positive_quantity}}</td>
          </tr>
        </table>
      </div>
      @if ($giftCardTemplate)
        <div class="col-xs-12  col-sm-3">
          <label for="">Front</label>
          <a href="javascript:void(0)" class="pop">
            <img class="thumbnail" src="{{$giftCardTemplate->front_design_url}}" alt="">
          </a>
          <label for="">Back</label>
          <a href="javascript:void(0)" class="pop">
            <img class="thumbnail" src="{{$giftCardTemplate->back_design_url}}" alt="">
          </a>
        </div>
      @endif
    </div>
    
  </div>
</div>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="comfirm-send" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Are you ready to send?</h4>
      </div>
      <div class="modal-body">
        <p>Note: after send, you are not able to edit this campaign any more. please make sure everything is right.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger" id="confirmSendBtn">Send</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="send-via-api" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Click Send Configuration</h4>
      </div>
      <div class="modal-body">
      {{ Form::open(array('url' => route('claim-review-campaign.createConfig', ['id'=>$claimReviewCampaign->id]), 'method' => 'POST', 'id'=>'config-clicksend')) }}
        <div class="form-group">
          <label for="">Return Address</label>
          <select required name="cs_return_address_id" id="cs_return_address_id" class="form-control">
            <option value="">-- select --</option>
            @foreach($returnAddresses as $address)
              @php ($selected = $address->return_address_id == $claimReviewCampaign->cs_return_address_id || $address->default)
              <option value="{{$address->return_address_id}}" {{ $selected ? "selected" : "" }}>{{$address->formatOneLine()}}</option>
              }
            @endforeach
          </select>
        </div>
        @if ($giftCardTemplate->template_type == "letter")
          <div class="form-group">
            <label for="">Duplex?</label>
            <label class="radio-inline"><input type="radio" name="cs_duplex" value="1" {{$claimReviewCampaign->cs_duplex ? "checked" : ""}}>Yes</label>
            <label class="radio-inline"><input type="radio" name="cs_duplex" value="0" {{!$claimReviewCampaign->cs_duplex ? "checked" : ""}}>No</label>
          </div>
          <div class="form-group">
            <label for="">Color?</label>
            <label class="radio-inline"><input type="radio" name="cs_colour" value="1" {{$claimReviewCampaign->cs_colour ? "checked" : ""}}>Yes</label>
            <label class="radio-inline"><input type="radio" name="cs_colour" value="0" {{!$claimReviewCampaign->cs_colour ? "checked" : ""}}>No</label>
          </div>
          <div class="form-group">
            <label for="">Priority?</label>
            <label class="radio-inline"><input type="radio" name="cs_priority_post" value="1" {{$claimReviewCampaign->cs_priority_post ? "checked" : ""}}>Yes</label>
            <label class="radio-inline"><input type="radio" name="cs_priority_post" value="0" {{!$claimReviewCampaign->cs_priority_post ? "checked" : ""}}>No</label>
          </div>
        @endif
        <div class="form-group">
          <label for= "">Delay hours</label>
          <input required id="delay" type="number" name="cs_delay_hours" class="form-control" min="0" step="1" value="{{$claimReviewCampaign->cs_delay_hours}}">
        </div>
        @if ($claimReviewCampaign->promoted_order_quantity > 0)
        <div class="form-group">
          <label for="">Exclude Promo Discounts</label>
          @foreach($claimReviewCampaign->promotion_discounts_array as $discount)
            <div class="checkbox">
              <label>
                <input type="checkbox" value="{{$discount}}" name="cs_exclude_promotion_discounts[]" {{in_array($discount, $claimReviewCampaign->exclude_promotion_discounts_array) ? "checked" : ""}}>  {{$discount}}
              </label>
            </div>
          @endforeach
        </div>
        @endif
        <button type="submit" class="btn btn-primary" id="sendFormSubmitBtn">Submit</button>
      {{ Form::close() }}
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="confirmDelete" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Are you sure you want to delete <b id="compaignName"></b>?</h4>
      </div>
      <div class="modal-body">
        <p class="text-default">Please input the campaign name below and then click delete if you really want to delete this campaign</p>
        <input type="text" id="confimName" class="form-control">
        <p id="wrong-campaign-name" class="text-danger">Your input is not exactly the same as the campaign name</p>
      </div>
      <div class="modal-footer">
        <i id="spinner" class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>
        <div id="btn-group">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger" id="deleteBtn">Delete</button>
        </div>
      </div>
    </div>
    
  </div>
</div>



<script>
  $(function() {
      $('.pop').on('click', function() {
        $('.imagepreview').attr('src', $(this).find('img').attr('src'));
        $('#imagemodal').modal('show');   
      });		

      $('.pUpdate').editable();
      $('[data-toggle="collapse"]').collapse()
  });

  $('#comfirm-send').on('show.bs.modal',  (event) => {
    const button = $(event.relatedTarget);
    $('#confirmSendBtn').on('click', () => {
      $('#sendFormSubmitBtn').click();
    })
  })

  $('#confirmDelete').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget);
    let campaignId = button.data('campaignId'); // Extract info from data-* attributes
    console.log(campaignId)
    let campaignName = button.data('campaignName');
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    let modal = $(this);
    modal.find('#wrong-campaign-name').hide();
    modal.find('#btn-group').show();
    modal.find('#spinner').hide();
    modal.find('#compaignName').text(campaignName);
    let input = modal.find('#confimName');
    $('#deleteBtn').on('click', function() {
      if (input.val().replace(/\s/g, "") == campaignName.replace(/\s/g, "")) {
        modal.find('#wrong-campaign-name').hide();
        modal.find('#btn-group').hide();
        modal.find('#spinner').show();
        $.ajax({
          url: "/claim-review-campaign/"+campaignId,
          type: 'DELETE',
          success: function(res){
            // button.closest('tr').remove();
            modal.modal('hide');
            window.location.href = "/claim-review-campaign";
          },
          error: function(err) {
            modal.find('#btn-group').show();
            modal.find('#spinner').hide();
            console.log(err);
          }
        });
        
      } else {
        modal.find('#wrong-campaign-name').show();
      }
    })
  });

  $("#confirmDelete").on('hide.bs.modal' , function (event) {
    let modal = $(this);
    modal.find('#confimName').val("");
  });

  $(function() {
    const defaultDelayHoure = 2;
    $("#delay").val(defaultDelayHoure);
  });
  
</script>
@endsection
@section('content')
{!! $grid !!}
@endsection