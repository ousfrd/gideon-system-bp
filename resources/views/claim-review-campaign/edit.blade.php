@extends('layouts.master')
@section('title', "New Review Campaign")
@section('content')
<ol class="breadcrumb">
  <li><a href="{{route('claim-review-campaign.index')}}">Claim Review Campaign List</a></li>
  <li><a href="{{route('claim-review-campaign.show', ['claimReviewCampaign' => $claimReviewCampaign])}}">{{$claimReviewCampaign->name}}</a></li>
  <li class="active">Edit</li>
</ol>
<div class="row">
	 <div class="col-md-4 mx-auto">
    <form role="form" action="{{route('claim-review-campaign.update', ['claimReviewCampaign'=>$claimReviewCampaign])}}" method="POST">
      <input name="_method" type="hidden" value="PUT">
      {{Form::token()}}
      <div class="form-group">
        <label for="name">Name:</label>
        <input type="text" name="name" class="form-control" id="name" value="{{$claimReviewCampaign->name}}">
      </div>
      <div class="form-group">
        <label for="platform">Supplier:</label>
        <select required name="platform" class="form-control" id="platform">
          <option value="">-- Select --</option>
          @foreach($suppliers as $val=>$text)
            <option value="{{$val}}" {{$claimReviewCampaign->platform == $val ? "selected" : "" }}>{{$text}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="channel">Channel *:</label>
        <select required name="channel" id="channel" class="form-control">
          <option value="">--select--</option>
          @foreach (App\ClaimReviewCampaign::CHANNELS as $val => $text)
            <option value="{{$val}}" {{$claimReviewCampaign->channel == $val ? "selected" : ""}}>{{$text}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="actual_sent_amount">Actual Sent Amount:</label>
        <input type="number" name="actual_sent_amount" class="form-control" id="actual_sent_amount" value="{{$claimReviewCampaign->actual_sent_amount}}">
      </div>
      <div class="form-group">
        <label for="gift-card">Gift Card Amount</label>
        <p>{{$claimReviewCampaign->cashback}}</p>
      </div>
      <div class="form-group">
        <label for="asin">ASIN</label>
        <input type="hidden" id="asin" value="{{$claimReviewCampaign->asin}}">
        <p>{{$claimReviewCampaign->asin}}</p>
      </div>
      <div class="form-group">
        <label for="gift_card_template">Gift Card Template</label>
        <p id="gift-card-template-hint">gift card template will show after you input valid ASIN</p>
        <select name="gift_card_template_id" id="gift-card-template" data-value="{{$claimReviewCampaign->gift_card_template_id}}">

        </select>

      </div>
      <div class="form-group">
        <label for="marketplace">Marketplace</label>
        <p>{{$claimReviewCampaign->marketplace}}</p>
      </div>
      <div class="form-group">
        <label for="dateRange">Date Range</label>
        <p>{{$claimReviewCampaign->start_date}} - {{$claimReviewCampaign->end_date}}</p>
      </div>
      <div class="form-group">
            <label for="">Insert Card Orders Filter?</label>
            <label class="radio-inline"><input type="radio" name="insert_card_orders_filter" value="1" {{$claimReviewCampaign->insert_card_orders_filter ? "checked" : ""}} disabled>Yes</label>
            <label class="radio-inline"><input type="radio" name="insert_card_orders_filter" value="0" {{!$claimReviewCampaign->insert_card_orders_filter ? "checked" : ""}} disabled>No</label>
          </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

<script>
  $(function() {
    function setDate(start, end){
      let startDate = start.format('YYYY-MM-DD');
      let endDate = end.format('YYYY-MM-DD');
      $("#startDate").val(startDate);
      $("#endDate").val(endDate);
    }
    const defaultStartDate = moment().subtract(7, 'days');
    const defaultEndDate = moment().subtract(0, 'days');
    setDate(defaultStartDate, defaultEndDate);
    $('input[name="dateRange"]').daterangepicker({
      maxDate: moment(),
      startDate: defaultStartDate,
      endDate: defaultEndDate
    }, ((start, end, label) => {
      setDate(start, end);
    }));
  });

  $(document).ready(() => {
    let giftCardTemplateSelect = $('#gift-card-template');

    function manageGiftCardTemplateField(){
      let asin = $('#asin').val().trim();
      if (asin.length != 10) {
        giftCardTemplateSelect.hide()
      } else {
        $('#gift-card-template-hint').hide();
        getGiftCardTemplateByAsin(asin);
      }
    }

    function getGiftCardTemplateByAsin(asin) {
      $.get(`/gift-card-template/asin/${asin}`, (templates) => { 
        if (templates.length > 0) {
          let defaultOption = `<option>Select</option>`;
          giftCardTemplateSelect.append(defaultOption);
          for(template of templates) {
            let dataContent = `<img class="small-thumbnail" src="${template.front_design_url}"/> - <div>${template.name} <br>${template.asin} <br> version: ${template.version} <br> amount: ${template.amount}</div> - <img class="small-thumbnail" src="${template.back_design_url}"/>`;
            let option = `<option value="${template.id}" data-asin="${template.asin}" data-content='${dataContent}'></option>`;
            giftCardTemplateSelect.append(option);
          }
          giftCardTemplateSelect.show();
          giftCardTemplateSelect.val(giftCardTemplateSelect.data('value'));
          giftCardTemplateSelect.selectpicker();
        }
      })
    }


    $('#asin').on('blur', ()=>{
      console.log('blur');
      manageGiftCardTemplateField();
    });

    manageGiftCardTemplateField();
  });
</script>
@endsection