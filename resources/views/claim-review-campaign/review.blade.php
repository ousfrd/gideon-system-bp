@extends('layouts.master')
@section('title', "Claim Review Campaign - " . $claimReviewCampaign->name)
@section('info_content')
<ol class="breadcrumb">
  <li><a href="{{route('claim-review-campaign.index')}}">Claim Review Campaign List</a></li>
  <li><a href="{{route('claim-review-campaign.show', ['claimReviewCampaign' => $claimReviewCampaign])}}"> {{$claimReviewCampaign->name}} </a></li>
  <li class="active">Review</li>
</ol>
<div class="panel panel-default">
  <div class="panel-heading">
    Configuration
    @if ($claimReviewCampaign->sent_at)
      <span class="btn btn-default">Sent At : {{$claimReviewCampaign->sent_at}}</span>
    @else
      {{ Form::open(array('url' => route('claim-review-campaign.send-via-api', ['id'=>$claimReviewCampaign->id]), 'method' => 'PUT', 'class' => 'hidden', 'id'=>'sendForm')) }}
        <button type="submit" id="sendFormSubmitBtn">Submit</button>
      {{ Form::close() }}
      <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#comfirm-send">Send Via API</button>
    @endif
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-xs-12  col-sm-6">
        <table class="table table-striped">
          <tr>
            <th>Return Address</th>
            <td>{!! $returnAddress->format() !!}</td>
          </tr>
          @if ($giftCardTemplate->template_type == "letter")
          <tr>
            <th>Is Duplex? </th>
            <td>{{$claimReviewCampaign->cs_duplex ? "Yes" : "No"}}</td>
          </tr>
          <tr>
            <th>Is Colorful?</th>
            <td>{{$claimReviewCampaign->cs_colour ? "Yes" : "No"}}</td>
          </tr>
          <tr>
            <th>Is Priority?</th>
            <td>{{$claimReviewCampaign->cs_priority_post ? "Yes" : "No"}}</td>
          </tr>
          @endif
          <tr>
            <th>Deplay Hours</th>
            <td>{{$claimReviewCampaign->cs_delay_hours}}</td>
          </tr>
          
          <tr>
            <th>Exclude Promo Discount:</th>
            <td>{{$claimReviewCampaign->cs_exclude_promotion_discounts}}</td>
          </tr>
          <tr>
            <th>Country</th>
            <td>{{$claimReviewCampaign->marketplace}}</td>
          </tr>
          <tr>
            <th>Other Filter</th>
            <td>recipient name doesn't contain "Amazon" </td>
          </tr>
        </table>
      </div>
      @if ($giftCardTemplate)
        <div class="col-xs-12  col-sm-3">
          <label for="">Front</label>
          <a href="javascript:void(0)" class="pop">
            <img class="thumbnail" src="{{$giftCardTemplate->front_design_url}}" alt="">
          </a>
          <a target="_blank" href="{{$giftCardTemplate->attachment1_url}}">Attatchment Front Link</a><br/><br/>
          @if ($giftCardTemplate->attachment2_url != 'https://storage.googleapis.com/300gedion/')
            <label for="">Back</label>
            <a href="javascript:void(0)" class="pop">
              <img class="thumbnail" src="{{$giftCardTemplate->back_design_url}}" alt="">
            </a>
            <a target="_blank" href="{{$giftCardTemplate->attachment2_url}}">Attatchment Back Link</a>
          @endif
        </div>
      @endif
    </div>
    
  </div>
</div>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="comfirm-send" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Are you ready to send?</h4>
      </div>
      <div class="modal-body">
        <p>Note: after send, you are not able to edit this campaign any more. please make sure everything is right.</p>
        <p class="text-danger">Expense will occur</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger" id="confirmSendBtn">Send</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<script>
  $(function() {
      $('.pop').on('click', function() {
        $('.imagepreview').attr('src', $(this).find('img').attr('src'));
        $('#imagemodal').modal('show');   
      });		

      $('.pUpdate').editable();
  });

  $('#comfirm-send').on('show.bs.modal',  (event) => {
    const button = $(event.relatedTarget);
    $('#confirmSendBtn').on('click', () => {
      $('#confirmSendBtn').prop('disabled', true);
      $('#sendFormSubmitBtn').click();
    })
  })
</script>
@endsection
@section('content')
{!! $grid !!}
@endsection