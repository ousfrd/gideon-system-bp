@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Filter Orders</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('claim-review-campaign.filter-shipments') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <p>Filter Orders that would not like to include in claim review campaign.</p>
                            <p>
                                <a href="{{ route('download-file', ['file' => 'shipments-filter-template.csv']) }}" target="_blank">
                                    shipments-filter-template.csv
                                </a>
                            </p>
                            <br />

                            <div class="form-group{{ $errors->has('orders_file') ? ' has-error' : '' }}">
                                <label for="orders_file" class="col-md-4 control-label">File to import</label>

                                <div class="col-md-6">
                                    <input id="orders_file" type="file" class="form-control" name="orders_file" required />

                                    @if ($errors->has('orders_file'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('orders_file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection