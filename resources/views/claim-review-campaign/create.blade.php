@extends('layouts.master')
@section('title', "New Review Campaign")
@section('content')
<div class="row">
	 <div class="col-md-4 mx-auto">
    <form role="form" action="{{route('claim-review-campaign.store')}}" method="POST">
      {{Form::token()}}
      <div class="form-group">
        <label for="platform">Supplier *:</label>
        <select required name="platform" class="form-control" id="platform">
          <option value="">-- Select --</option>
          <?php
          foreach($suppliers as $val=>$text)
            if ($val == "click2mail") {
              echo('<option selected="selected" value='.$val.'>'.$text.'</option>');
            } else {
              echo('<option value='.$val.'>'.$text.'</option>');
            }
          ?>
        </select>
      </div>
      <div class="form-group">
        <label for="asin">ASIN *</label>
        <input required type="text" name="asin" class="form-control" id="asin">
      </div>
      <div class="form-group">
        <label for="gift_card_template">Gift Card Template</label>
        <p id="gift-card-template-hint">gift card template will show after you input valid ASIN</p>
        <select name="gift_card_template_id" id="gift-card-template">

        </select>

      </div>
      <div class="form-group">
        <label for="marketplace">Marketplace *</label>
        <select required class="form-control" name="marketplace" id="marketplace">
          <option value="">--select *--</option>
          <?php
          foreach($marketplaces as $value=>$text)
            if ($value == "US") {
              echo('<option selected="selected" value='.$value.'>'.$text.'</option>');
            } else {
              echo('<option value='.$value.'>'.$text.'</option>');
            }
          ?>
        </select>
      </div>
      <div class="form-group">
        <label for="dateRange">Date Range</label>
        <input type="text" class="form-control" name="dateRange" id="dateRange" />
        <input type="hidden" class="form-control" id="startDate" name="startDate">
        <input type="hidden" class="form-control" id="endDate" name="endDate">
      </div>
      <div class="form-group">
        <label for="">Insert Card Orders Filter?</label>
        <label class="radio-inline"><input type="radio" name="insert_card_orders_filter" value="1" checked >Yes</label>
        <label class="radio-inline"><input type="radio" name="insert_card_orders_filter" value="0" >No</label>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

<script>
  $(function() {
    function setDate(start, end){
      let startDate = start.format('YYYY-MM-DD');
      let endDate = end.format('YYYY-MM-DD');
      $("#startDate").val(startDate);
      $("#endDate").val(endDate);
    }
    const defaultStartDate = moment().subtract(7, 'days');
    const defaultEndDate = moment().subtract(2, 'days');
    setDate(defaultStartDate, defaultEndDate);
    $('input[name="dateRange"]').daterangepicker({
      maxDate: moment(),
      startDate: defaultStartDate,
      endDate: defaultEndDate
    }, ((start, end, label) => {
      setDate(start, end);
    }));
  });

  $(function() {
    const defaultGiftCardValue = 20;
    $("#gift-card").val(defaultGiftCardValue);
  });

  $(document).ready(() => {
    let giftCardTemplateSelect = $('#gift-card-template');

    function manageGiftCardTemplateField(){
      let asin = $('#asin').val().trim();
      if (asin.length != 10) {
        giftCardTemplateSelect.hide()
      } else {
        $('#gift-card-template-hint').hide();
        getGiftCardTemplateByAsin(asin);
      }
    }

    function getGiftCardTemplateByAsin(asin) {
      $.get(`/gift-card-template/asin/${asin}`, (templates) => { 
        if (templates.length > 0) {
          let defaultOption = `<option>Select</option>`;
          giftCardTemplateSelect.append(defaultOption);
          for(template of templates) {
            let dataContent = `<img class="small-thumbnail" src="${template.front_design_url}"/> - <div>${template.name} <br> ${template.asin} <br> channel: ${template.channel} <br> ${template.channel_detail} <br> version: ${template.version} <br> amount: ${template.amount}</div> - <img class="small-thumbnail" src="${template.back_design_url}"/>`;
            let option = `<option value="${template.id}" data-asin="${template.asin}" data-content='${dataContent}'></option>`;
            giftCardTemplateSelect.append(option);
          }
          giftCardTemplateSelect.show();
          giftCardTemplateSelect.selectpicker();
        }
      })
    }

    $('#asin').on('blur', ()=>{
      console.log('blur');
      manageGiftCardTemplateField();
    });

    manageGiftCardTemplateField();
  });
</script>
@endsection