@extends('layouts.master')
@section('title', "Managed Review Orders")
@section('content')
<ul class="nav nav-tabs" role="tablist">
  <li role="presentation" class="{{$tab == 'all' ? 'active' : ''}}"><a href="{{route('managed-review-order.index')}}">All</a></li>
  <li role="presentation" class="{{$tab == 'self-orders' ? 'active' : ''}}"><a href="{{route('managed-review-order.own-orders')}}">Self Orders</a></li>
  <li role="presentation" class="{{$tab == 'need-review-orders' ? 'active' : ''}}"><a href="{{route('managed-review-order.need-review-orders')}}">Need Review</a></li>
  <li role="presentation" class="{{$tab == 'check-review-orders' ? 'active' : ''}}"><a href="{{route('managed-review-order.check-review-orders')}}">Wait For Review Link</a></li>
</ul>

{!! $grid !!}

<script>
$(document).ready(function() {
  $('.pUpdate').editable({
    ajaxOptions: {
      type: 'put'
    }
  });
})
</script>
@endsection