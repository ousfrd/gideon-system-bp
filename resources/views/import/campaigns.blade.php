@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">


                <div class="panel panel-default">
                    <div class="panel-heading">Import Campaigns Bulk Report</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('import.campaigns.parse') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <p>Bulk Operations -> Create & download a custom spreadsheet -> Yesterday -> Create Speadsheet for download</p>
                            <br>

                            <div class="form-group">
                                
                                    <label for="csv_file" class="col-md-4 control-label">Seller Account</label>
                                        
                                    <div class="col-md-6">
                                        <select name="seller_id">
                                            @foreach($accounts as $account)
<option value="{{$account->seller_id}}" >{{$account->name}} ({{$account->code}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                            </div>


                            <div class="form-group">
                                
                                    <label for="country" class="col-md-4 control-label">Country</label>
                                        
                                    <div class="col-md-6">
                                        <select name="country">
                                            
                                            <option value="US">US</option>
                                            <option value="CA">CA</option>
                                            <option value="UK">UK</option>
                                            <option value="DE">DE</option>
                                            <option value="FR">FR</option>
                                            <option value="ES">ES</option>
                                            <option value="IT">IT</option>
                                            <option value="JP">JP</option>
                                            <option value="AU">AU</option>
                                            
                                        </select>
                                    </div>
                            </div>

                            <div class="form-group">
                                
                                    <label for="csv_file" class="col-md-4 control-label">Report Date</label>
                                        
                                    <div class="col-md-6">
                                     <input type="date" name="report_date">   
                                    </div>
                            </div>

                            <div class="form-group{{ $errors->has('campaigns_file') ? ' has-error' : '' }}">
                                <label for="campaigns_file" class="col-md-4 control-label">xlsx file to import</label>

                                <div class="col-md-6">
                                    <input id="campaigns_file" type="file" class="form-control" name="campaigns_file" required>

                                    @if ($errors->has('campaigns_file'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('campaigns_file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Import
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Import Campaigns Search Term</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('import.searchterms.parse') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <p>File location: Report -> Advertising Reports -> Report Type(Search Term) -> Daily
</p>
                            <br>

                            <div class="form-group">
                                
                                    <label for="csv_file" class="col-md-4 control-label">Seller Account</label>
                                        
                                    <div class="col-md-6">
                                        <select name="seller_id">
                                            @foreach($accounts as $account)
                                            <option value="{{$account->seller_id}}"  >{{$account->name}} ({{$account->code}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                            </div>

                            <div class="form-group">
                                
                                    <label for="country" class="col-md-4 control-label">Country</label>
                                        
                                    <div class="col-md-6">
                                        <select name="country">
                                            
                                            <option value="US">US</option>
                                            <option value="CA">CA</option>
                                            <option value="UK">UK</option>
                                            <option value="DE">DE</option>
                                            <option value="FR">FR</option>
                                            <option value="ES">ES</option>
                                            <option value="IT">IT</option>
                                            <option value="JP">JP</option>
                                            <option value="AU">AU</option>
                                            
                                        </select>
                                    </div>
                            </div>

                            <div class="form-group{{ $errors->has('searchterms_file') ? ' has-error' : '' }}">
                                <label for="searchterms_file" class="col-md-4 control-label">xlsx file to import</label>

                                <div class="col-md-6">
                                    <input id="searchterms_file" type="file" class="form-control" name="searchterms_file" required>

                                    @if ($errors->has('searchterms_file'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('searchterms_file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Import
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>                


            </div>
        </div>


    </div>
@endsection