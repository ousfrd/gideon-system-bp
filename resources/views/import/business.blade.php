@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Import Business Report</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('import.business.parse') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <p>File location: Report -> Business Reports -> by ASIN -> Detail Sales</p>
                            <br>
                            
                            <div class="form-group">
                                
                                    <label for="csv_file" class="col-md-4 control-label">Seller Account</label>
                                        
                                    <div class="col-md-6">
                                        <select name="seller_id">
                                            @foreach($accounts as $account)
                                            <option value="{{$account->seller_id}}">{{$account->name}} ({{$account->code}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                            </div>

                                      
                            <div class="form-group">
                                
                                    <label for="csv_file" class="col-md-4 control-label">Report Date</label>
                                        
                                    <div class="col-md-6">
                                     <input type="date" name="report_date">   
                                    </div>
                            </div>

                                         

                            <div class="form-group">
                                
                                    <label for="country" class="col-md-4 control-label">Country</label>
                                        
                                    <div class="col-md-6">
                                        <select name="country">
                                            
                                            <option value="US">US</option>
                                            <option value="CA">CA</option>
                                            <option value="UK">UK</option>
                                            <option value="DE">DE</option>
                                            <option value="FR">FR</option>
                                            <option value="ES">ES</option>
                                            <option value="IT">IT</option>
                                            <option value="JP">JP</option>
                                            <option value="AU">AU</option>
                                            
                                        </select>
                                    </div>
                            </div>


                            <div class="form-group{{ $errors->has('business_file') ? ' has-error' : '' }}">
                                <label for="business_file" class="col-md-4 control-label">CSV file to import</label>

                                <div class="col-md-6">
                                    <input id="business_file" type="file" class="form-control" name="business_file" required>

                                    @if ($errors->has('business_file'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('business_file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Import
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection