@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Import Orders</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('import.orders.parse') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <p>File location: Reports -> Fulfillment -> Sales -> All Orders -> Last Updated Date -> Request Download</p>
                            <br>
                            <div class="form-group">
                                
                                    <label for="csv_file" class="col-md-4 control-label">Seller Account</label>
                                        
                                    <div class="col-md-6">
                                        <select name="seller_id">
                                            @foreach($accounts as $account)
                                            <option value="{{$account->seller_id}}">{{$account->name}} ({{$account->code}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                            </div>

                            <div class="form-group{{ $errors->has('orders_file') ? ' has-error' : '' }}">
                                <label for="orders_file" class="col-md-4 control-label">TXT file to import</label>

                                <div class="col-md-6">
                                    <input id="orders_file" type="file" class="form-control" name="orders_file" required>

                                    @if ($errors->has('orders_file'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('orders_file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Import
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Import Order Shipments</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('import.order_shipments.parse') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <p>File location: Reports -> Fulfillment -> Sales -> Amazon Fulfilled Shipments -> Request .txt Download</p>
                            <br>
                            <div class="form-group">
                                
                                    <label for="csv_file" class="col-md-4 control-label">Seller Account</label>
                                        
                                    <div class="col-md-6">
                                        <select name="seller_id">
                                            @foreach($accounts as $account)
                                            <option value="{{$account->seller_id}}">{{$account->name}} ({{$account->code}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                            </div>

                            <div class="form-group{{ $errors->has('order_shipments_file') ? ' has-error' : '' }}">
                                <label for="order_shipments_file" class="col-md-4 control-label">TXT file to import</label>

                                <div class="col-md-6">
                                    <input id="order_shipments_file" type="file" class="form-control" name="order_shipments_file" required>

                                    @if ($errors->has('order_shipments_file'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('order_shipments_file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Import
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection