@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Import Listings</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('import.listings.parse') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <p>File location: Inventory -> Inventory Reports -> All Listings Report -> Request Report</p>
                            <br>
                            
                            <div class="form-group">
                                
                                    <label for="csv_file" class="col-md-4 control-label">Seller Account</label>
                                        
                                    <div class="col-md-6">
                                        <select name="seller_id">
                                            @foreach($accounts as $account)
                                            <option value="{{$account->seller_id}}">{{$account->name}} ({{$account->code}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                            </div>

                            <div class="form-group">
                                
                                    <label for="country" class="col-md-4 control-label">Country</label>
                                        
                                    <div class="col-md-6">
                                        <select name="country">
                                            
                                            <option value="US">US</option>
                                            <option value="CA">CA</option>
                                            <option value="UK">UK</option>
                                            <option value="DE">DE</option>
                                            <option value="FR">FR</option>
                                            <option value="ES">ES</option>
                                            <option value="IT">IT</option>
                                            <option value="JP">JP</option>
                                            <option value="AU">AU</option>
                                            
                                        </select>
                                    </div>
                            </div>

                            <div class="form-group{{ $errors->has('listings_file') ? ' has-error' : '' }}">
                                <label for="listings_file" class="col-md-4 control-label">TXT file to import</label>

                                <div class="col-md-6">
                                    <input id="listings_file" type="file" class="form-control" name="listings_file" required>

                                    @if ($errors->has('listings_file'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('listings_file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Import
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection