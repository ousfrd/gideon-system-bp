@extends('layouts.master')
@section('title', "Dashboard")
@section('content')

@if(count($nocost_products) > 0)
<div class="bs-callout bs-callout-sm bs-callout-danger bg-danger" role="alert" style="margin-top: 10px;">
<h4 style="display: inline-block;">Alert: </h4> 
{{count($nocost_products)}} of your products need to add Unit Cost and Unit Shipping Cost.

<a class="pull-right btn btn-primary" href="{{ route ( 'index.addproductcost' ) }}" target="_blank">Add Product Cost</a>

</div>
@endif

@include('index.parts.filters')

<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#scharts"> <i class="fa fa-line-chart" aria-hidden="true"></i> Charts</a></li>
  <li><a data-toggle="tab" href="#stables"><i class="fa fa-table" aria-hidden="true"></i> Tabular Data</a></li>
 </ul>
<div class="tab-content">
	<div id="scharts" class="tab-pane active">
	@include('index.parts.boxes')

	@include('index.parts.charts')
	</div>
	<div id="stables" class="tab-pane fade">
	@include('index.parts.table')
	</div>

	@if($bestsellers_count > 0)
	<div id="bestsellers" class="tab-pan">
		@include('index.parts.bestsellers')	
	</div>
	@endif
</div>


@endsection