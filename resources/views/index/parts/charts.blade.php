<div class="row">
	 <div style="width:100%;height:450px;margin:0 auto">
        <div id="order-chart" style="width: 100%;height:100%" ></div>
    </div>
</div>

<?php
	$columns = [];
	$xline = ['x'];
	$json_data = [];

	if((strtotime($end_date) - strtotime($start_date)) == 0) {
		if($start_date == date('Y-m-d')) {
			$today = ['Today'];
			$yesterday = ['Yesterday'];
		} else {
			$today = ['Current'];
			$yesterday = ['Day Before'];
		}
		
		$prev_week = ['Same Time Prev Week'];
		$prev_month = ['Same Time Prev Month'];			

		foreach($orders as $date => $order) {
			
			$xline[] = $start_date . " " . sprintf("%02s", $date) . ":00:00";

			$timestamp_last_week = strtotime($start_date_last_week) + 12 * 3600;
			$timestamp_last_month = strtotime($start_date_last_month) + 12 * 3600;

	  		$total_1 = $orderReportYesterday[is_int($date)?$date:date('Y-m-d',strtotime($date)-1*24*3600 )]->total_orders;
		  	$gross_1 = $orderReportYesterday[is_int($date)?$date:date('Y-m-d',strtotime($date)-1*24*3600 )]->revenue;
		  	$aggregate_orders_1 = $orderReportYesterday[is_int($date)?$date:date('Y-m-d',strtotime($date)-1*24*3600 )]->aggregate_orders;
		  	$aggregate_revenue_1 = $orderReportYesterday[is_int($date)?$date:date('Y-m-d',strtotime($date)-1*24*3600 )]->aggregate_revenue;

		  	$total_7 = $orderReportLastWeek[is_int($date)?$date:date('Y-m-d', $timestamp_last_week)]->total_orders;
		  	$gross_7 = $orderReportLastWeek[is_int($date)?$date:date('Y-m-d', $timestamp_last_week)]->revenue;
		  	$aggregate_orders_7 = $orderReportLastWeek[is_int($date)?$date:date('Y-m-d', $timestamp_last_week)]->aggregate_orders;
		  	$aggregate_revenue_7 = $orderReportLastWeek[is_int($date)?$date:date('Y-m-d', $timestamp_last_week)]->aggregate_revenue;

		  	$total_30 = $orderReportLastMonth[is_int($date)?$date:date('Y-m-d',$timestamp_last_month )]->total_orders;
			$gross_30 = $orderReportLastMonth[is_int($date)?$date:date('Y-m-d',$timestamp_last_month )]->revenue;
			$aggregate_orders_30 = $orderReportLastMonth[is_int($date)?$date:date('Y-m-d',$timestamp_last_month )]->aggregate_orders;
			$aggregate_revenue_30 = $orderReportLastMonth[is_int($date)?$date:date('Y-m-d',$timestamp_last_month )]->aggregate_revenue;

			$today[] = $order->total_units;
			$yesterday[] = $total_1;
			$prev_week[] = $total_7;
			$prev_month[] = $total_30;

			if($start_date == date('Y-m-d')) {
				$json_data[] =  array(
					'Today' => array(
						'date' => $start_date,
						'orders' => $order->total_orders,
						'aggregate_orders' => $order->aggregate_orders,
						'revenue' => '$'.number_format($order->revenue, 2),
						'aggregate_revenue' => '$'.number_format($order->aggregate_revenue, 2)
					),
					'Yesterday' => array(
						'date' => date('Y-m-d',strtotime($start_date)-1*24*3600 ),
						'orders' => $total_1,
						'aggregate_orders' => $aggregate_orders_1,
						'revenue' => '$'.number_format($gross_1, 2),
						'aggregate_revenue' => '$'.number_format($aggregate_revenue_1, 2)
					),
					'Same Time Prev Week' => array(
						'date' => date('Y-m-d', $timestamp_last_week),
						'orders' => $total_7,
						'aggregate_orders' => $aggregate_orders_7,
						'revenue' => '$'.number_format($gross_7, 2),
						'aggregate_revenue' => '$'.number_format($aggregate_revenue_7, 2)
					),
					'Same Time Prev Month' => array(
						'date' => date('Y-m-d',$timestamp_last_month ),
						'orders' => $total_30,
						'aggregate_orders' => $aggregate_orders_30,
						'revenue' => '$'.number_format($gross_30, 2),
						'aggregate_revenue' => '$'.number_format($aggregate_revenue_30, 2)
					)
				);
			} else {
				$json_data[] =  array(
					'Current' => array(
						'date' => $start_date,
						'orders' => $order->total_orders,
						'aggregate_orders' => $order->aggregate_orders,
						'revenue' => '$'.number_format($order->revenue, 2),
						'aggregate_revenue' => '$'.number_format($order->aggregate_revenue, 2)
					),
					'Day Before' => array(
						'date' => date('Y-m-d',strtotime($start_date)-1*24*3600 ),
						'orders' => $total_1,
						'aggregate_orders' => $aggregate_orders_1,
						'revenue' => '$'.number_format($gross_1, 2),
						'aggregate_revenue' => '$'.number_format($aggregate_revenue_1, 2)
					),
					'Same Time Prev Week' => array(
						'date' => date('Y-m-d', $timestamp_last_week),
						'orders' => $total_7,
						'aggregate_orders' => $aggregate_orders_7,
						'revenue' => '$'.number_format($gross_7, 2),
						'aggregate_revenue' => '$'.number_format($aggregate_revenue_7, 2)
					),
					'Same Time Prev Month' => array(
						'date' => date('Y-m-d',$timestamp_last_month ),
						'orders' => $total_30,
						'aggregate_orders' => $aggregate_orders_30,
						'revenue' => '$'.number_format($gross_30, 2),
						'aggregate_revenue' => '$'.number_format($aggregate_revenue_30, 2)
					)
				);

			}
		} 

	} else {
		
		$current = ['Current'];

		if((strtotime($end_date) - strtotime($start_date)) <= 7 * 24 * 3600) {
			$last_7 = ['Same Time Prev Week'];
		}

		if ((strtotime($end_date) - strtotime($start_date)) <= 31 * 24 * 3600) {
			$last_30 = ['Same Time Prev Month'];	
		}
		
		if ((strtotime($end_date) - strtotime($start_date)) > 7 * 24 * 3600) {
			$last_365 = ['Same Time Prev Year'];
		}

		foreach($orders as $date => $order) {
			$current[] = $order->total_units;

			$new_json_data = [];

			if($date <= $end_date) {
				
				$xline[] = $date;

				$new_json_data['Current'] = array(
						'date' => date('Y-m-d',strtotime($date) ),
						'orders' => $order->total_orders, 
						'revenue' => '$'.number_format($order->revenue, 2)
					);
			}


			if((strtotime($end_date) - strtotime($start_date)) <= 7 * 24 * 3600) {
				$date_last_week = date('Y-m-d',strtotime("-1 week",strtotime ($date) ));
				if($date_last_week <= $end_date_last_week) {
					$total_7 = $orderReportLastWeek[$date_last_week]->total_orders;
			  		$gross_7 = $orderReportLastWeek[$date_last_week]->revenue;

			  		$last_7[] = $total_7;

			  		$new_json_data['Same Time Prev Week'] = array(
			  			'date' => $date_last_week,
						'orders' => $total_7, 
						'revenue' => '$'.number_format($gross_7, 2)
					);
			  	}
			}
			

			if ((strtotime($end_date) - strtotime($start_date)) <= 31 * 24 * 3600) {
				$date_last_month = date('Y-m-d',strtotime("-1 month",strtotime ($date) ));
				if($date_last_month <= $end_date_last_month) {
				  $total_30 = $orderReportLastMonth[$date_last_month]->total_orders;
					$gross_30 = $orderReportLastMonth[$date_last_month]->revenue;

					$last_30[] = $total_30;

					$new_json_data['Same Time Prev Month'] = array(
						'date' => $date_last_month,
						'orders' => $total_30, 
						'revenue' => '$'.number_format($gross_30, 2)
					);
				}
			}

			if ((strtotime($end_date) - strtotime($start_date)) > 7 * 24 * 3600) {
				$date_last_year = date('Y-m-d',strtotime ("-1 year",strtotime ($date)));
				if($date_last_year <= $end_date_last_year && array_key_exists($date_last_year, $orderReportLastYear)) {
				  $total_365 = $orderReportLastYear[$date_last_year]->total_orders;
					$gross_365 = $orderReportLastYear[$date_last_year]->revenue;

					$last_365[] = $total_365;

					$new_json_data['Same Time Prev Year'] = array(
						'date' => $date_last_year,
						'orders' => $total_365, 
						'revenue' => '$'.number_format($gross_365, 2)
					);
				}
			}

			

			$json_data[] =  $new_json_data;

		}

	}
?>


<script type="text/javascript">


	var columns = [];
	console.log(columns);
	@if((strtotime($end_date) - strtotime($start_date)) == 0)
	
		columns.push(<?php echo json_encode($xline); ?>, <?php echo json_encode($today); ?>, <?php echo json_encode($yesterday); ?>, <?php echo json_encode($prev_week); ?>, <?php echo json_encode($prev_month); ?>);

	@elseif ((strtotime($end_date) - strtotime($start_date)) <= 7 * 24 * 3600)

		columns.push(<?php echo json_encode($xline); ?>, <?php echo json_encode($current); ?>, <?php echo json_encode($last_7); ?>, <?php echo json_encode($last_30); ?>);

	@elseif ((strtotime($end_date) - strtotime($start_date)) <= 31 * 24 * 3600)

		columns.push(<?php echo json_encode($xline); ?>, <?php echo json_encode($current); ?>, <?php echo json_encode($last_30); ?>, <?php echo json_encode($last_365); ?>);

	@else 

		columns.push(<?php echo json_encode($xline); ?>, <?php echo json_encode($current); ?>, <?php echo json_encode($last_365); ?>);

	@endif

	var json_data = <?php echo json_encode($json_data) ?>
	
	var chart = c3.generate({
	    bindto: '#order-chart',
	    data: {
	      x: 'x',
	      @if($start_date == $end_date)
	      	xFormat: '%Y-%m-%d %H:%M:%S',
          @else
    		xFormat: '%Y-%m-%d',
    	  @endif
	      columns: columns,
	      type: 'spline'
	    },
	    axis: {
	        x: {
	            type: 'timeseries',
	            @if($start_date == $end_date)
	            tick: {
	                format: '%-I%p'
	            }
	            @else
	        	tick: {
	                format: '%b %d,%Y'
	            }
	        	@endif
	        },
	        y: {
	        	padding: {bottom:0}
	        }
	    },

	    tooltip: {
		    contents: function(d, defaultTitleFormat, defaultValueFormat, color) {

			  var $$ = this;

		      @if($start_date == $end_date)
			      var format = d3.time.format("%-I%p");
			      title = "{{ $start_date }} " + format(d[0]['x']);
		      @else
			      var format = d3.time.format("%b %d,%Y");
			      title = "{{ $start_date }} - {{ $end_date }}";
		      @endif

		      var x_header = '<tr><th colspan="5">' + title + '</th></tr>';
		      @if($start_date == $end_date)
		      var value_header = '<tr><td class="name"> Date </td><td class="value"> Orders </td><td class="value"> Revenue </td><td class="value"> Aggregate Orders </td><td class="value"> Aggregate Revenue </td></tr>';
		      @else
		      var value_header = '<tr><td class="name"> Date </td><td class="value"> Orders </td><td class="value"> Revenue </td></tr>';
		      @endif

		      var index = d[0]['index'];
		      var text = '';
		      // console.log(d);
			    for (i = 0; i < d.length; i++) {
			    	if (!(d[i] && (d[i].value || d[i].value === 0))) {
			            continue;
			        }
			        
			        bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

			    	text += "<tr>";
		            text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + d[i]['name'] + " ("+json_data[index][d[i]['name']]['date']+")</td>";
		            text += "<td class='value'>" + d[i]['value'] + "</td>";
		            text += "<td class='value'>" + json_data[index][d[i]['name']]['revenue'] + "</td>";

		            @if($start_date == $end_date)
		            text += "<td class='value'>" + json_data[index][d[i]['name']]['aggregate_orders'] + "</td>";
		            text += "<td class='value'>" + json_data[index][d[i]['name']]['aggregate_revenue'] + "</td>";
		            @endif
		            text += "</tr>";
			  	}

		      return '<table class="c3-tooltip"><tbody>' + x_header + value_header + text + '</tbody></table>';
		    }

		},

	});


	

	
</script>
<style>


</style>