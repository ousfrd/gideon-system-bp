<div class="mb20"></div>

<?php $pageSize = 10000; ?>
<?php $pages = ceil($bestsellers_count / $pageSize); ?>
<div class="row merchant-dash-ajax periodic-best-sellers" id="merchant-products" data-href="{{route('listing.call_backs.periodicBestSellers')}}"
                 data-range="{{$date_range}}"
                 data-range-date="{{$start_date}}"
                 data-end-date="{{$end_date}}"
                 data-options='{!! json_encode($params) !!}'>
	
    <div class="panel-content">
    	@foreach ($bestsellers as $key => $bestseller)
		
    	<?php if($key == 0) { ?>
    	<div id="page-1">
    	<?php } ?>

    	<?php if($key != 0 && $key % $pageSize == 0 ) { ?>
    	</div>
    	<div id="page-{{$key / $pageSize + 1}}" style="display: none;">
    	<?php } ?>
		<div class="col-md-4">
		            <div class="panel panel-default panel-dark product{{$bestseller->product_id}}">
		                <div class="panel-heading">
		                    <h4 class="panel-title clearfix">
		                        <span class="tip col-md-10 col-xs-10 nopadding" style="cursor:pointer;" data-toggle="tooltip" data-placement="bottom" title="{{ $bestseller->alias ?? $bestseller->product_name }}">
		                        	<small>
		                        		{{$bestseller->ASIN}}
		                        	</small>: {{ $bestseller->alias ? $bestseller->alias : str_limit($bestseller->product_name, $limit = 50, $end = '..') }}</span>
		                        	<!-- }
		                        	} -->
		                       
                                <div class="pull-right dash-icons col-md-2 col-xs-2 nopadding">
                                    <a class="dropdown-toggle" id="dashboard-dropdown-toggle" href="#" data-toggle="dropdown" title="View Dashboard" style="margin-right: 5px;"><i class="fa fa-list-alt"></i></a>  
                                            <ul class="dropdown-menu">
                                                    <li>
                                                        <a target="_blank" data-toggle="tooltip" data-placement="bottom" href="{{ route ( 'product.view', [ 'id' => $bestseller->product_id ] ) }}">View Dashboard</a>
                                                    </li>
                                            
                                            </ul>
                                	<a target="_blank" href="{{$bestseller->amazon_link}}">{{$bestseller->country}}</a>
                                    <!-- <div class="pull-right">
                                            <a class="dropdown-toggle" id="user-dropdown-toggle" href="#" data-toggle="dropdown" title="View On Amazon">
                                                <i class="fa fa-external-link"></i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                    <li>
                                                        <a target="_blank" data-toggle="tooltip" data-placement="bottom" href="{{$bestseller->amazon_link}}">{{ucfirst($bestseller->marketplace)}}</a>
                                                    </li>
                                            
                                            </ul>
                                    </div> -->
                                                                
                                </div>
		                    </h4>
		                </div>
		                <div class="panel-body">
											<div class="row">
		                    <div class="col-md-4 col-xs-4">
		                    	<?php $all = $bestseller->total_gross + abs($bestseller->total_refund);
		                    	?>
		                        <h5 class="inverse">Gross Sales</h5>
		                            <span class="sublabel">
		                                <strong>Full</strong>
		                                <small>(${{number_format($bestseller->total_gross_full,2)}})</small>
		                            </span>
		                        <?php $rate = ($all == 0) ? 0 : round($bestseller->total_gross_full / $all * 100, 3);?>
														<div class="progress-sm progress">
															<div aria-valuenow="<?php echo $rate ?>" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-success">
															</div>
														</div>                        
														<span class="sublabel">
		                          <strong>Promos</strong>
		                          <small>(${{number_format($bestseller->total_gross_promo,2)}})</small>
		                        </span>
		                        <?php $rate = ($all == 0) ? 0 : round($bestseller->total_gross_promo / $all * 100, 3)?>
		                        <div class="progress-sm progress"><div aria-valuenow="<?php echo $rate ?>" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-hpgreen"></div></div>                        <span class="sublabel">
		                                <strong>RFDS</strong>
		                                <small class="text-danger">(${{number_format(abs($bestseller->total_refund),2)}})</small>
		                            </span>
		                        <?php $rate = ($all == 0) ? 0 : round(abs($bestseller->total_refund) / $all * 100, 3)?>
		                        <div class="progress-sm progress"><div aria-valuenow="<?php echo $rate ?>" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-danger"></div></div>
													</div>
		                    <div class="col-md-4 col-xs-4">
		                    	<?php $all = $bestseller->total_units_full + $bestseller->total_units_promo +$bestseller->refund_count ?>
		                        <h5 class="inverse">Units</h5>
		                            <span class="sublabel">
		                                <strong>Full</strong>
		                                <small>({{ $bestseller->total_units_full }})</small>
		                            </span>
		                        <?php $rate = ($all == 0) ? 0 : round($bestseller->total_units_full / $all * 100, 3)?>
		                        <div class="progress-sm progress"><div aria-valuenow="<?php echo $rate ?>" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-primary"></div></div>
		                        <span class="sublabel">
		                                <strong>Promos</strong>
		                                <small>({{ $bestseller->total_units_promo }})</small>
		                            </span>
		                        <?php $rate = ($all == 0) ? 0 : round($bestseller->total_units_promo / $all * 100, 3)?>
		                        <div class="progress-sm progress"><div aria-valuenow="<?php echo $rate ?>" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-info"></div></div>
		                        <span class="sublabel">
		                                <strong>RFDS</strong>
		                                <small>({{$bestseller->refund_count ? $bestseller->refund_count : 0}})</small>
		                            </span>
		                        <?php $rate = ($all == 0) ? 0 : round($bestseller->refund_count / $all * 100, 3)?>
		                        <div class="progress-sm progress"><div aria-valuenow="<?php echo $rate ?>" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-warning"></div></div>
												</div>
		                    
		                    <div class="col-md-4 col-xs-4">
		                        <h5 class="inverse">Orders</h5>
		                        <span class="sublabel">
		                                <strong>TOT</strong>
		                                <small>({{$bestseller->total_orders}})</small>
		                            </span>
		                            <?php $rate = ($bestseller->total_orders == 0) ? 0 : 100 ?>
		                        <div class="progress-sm progress"><div aria-valuenow="<?php echo $rate ?>" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-hpgreen-dark"></div></div>
		                        <span class="sublabel">
		                                <strong>ORG</strong>
		                                <small>({{$bestseller->total_orders - $bestseller->ppc_count}})</small>
		                            </span>
		                            <?php $rate = ($bestseller->total_orders == 0) ? 0 : round(($bestseller->total_orders - $bestseller->ppc_count) / $bestseller->total_orders * 100, 3)?>
		                        <div class="progress-sm progress"><div aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-hpgreen-med"></div></div>
		                        <span class="sublabel">
		                                <strong>PPC</strong>
		                                <small>({{$bestseller->ppc_count ? $bestseller->ppc_count : 0}})</small>
		                            </span>
		                            <?php $rate = ($bestseller->total_orders == 0) ? 0 : round($bestseller->ppc_count / $bestseller->total_orders * 100, 3)?>
														<div class="progress-sm progress"><div aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: <?php echo $rate ?>%;" class="progress-bar progress-bar-hpgreen-lgt"></div></div>
													</div>
													<div class="clearfix"></div>
														
												</div>
										
										
											<div class="row">
												<div class="col-md-4 col-xs-4">
													<h5 class="inverse">AI ADs</h5>
													<?php $all = $bestseller->total_gross + abs($bestseller->total_refund);
		                    	?>
													<p>{{$bestseller->ai_ads_open ? "ON" : "OFF"}}</p>
												</div>

												<div class="col-md-4 col-xs-4">
													<h5 class="inverse">AIAD C Rate</h5>
													<P>{{$bestseller->ai_ads_commission_rate ? $bestseller->ai_ads_commission_rate : 0}}%</P>
												</div>

												<div class="col-md-4 col-xs-4">
													<?php $commission = number_format(0.01 * $bestseller->ai_ads_commission_rate * $bestseller->ads_sales, 2);?>
													<h5 class="inverse">AIAD CMSion</h5>
													<p>${{$commission}}</p>
												</div>

											</div>
										
											<div class="row">
												<div class="col-md-4 col-xs-4">
													<h5 class="inverse">Insert Card Orders</h5>
													<?php $all = $bestseller->total_gross + abs($bestseller->total_refund);
		                    						?>
													<p>{{$bestseller->fba_insert_card_orders?? 0}}</p>
												</div>

												<div class="col-md-4 col-xs-4">
													<h5 class="inverse">Insert Card Reviews</h5>
													<P>{{$bestseller->insert_card_reviews?? 0}}</P>
												</div>
											</div>
										</div>
		                <!--<div class="panel-footer panel-stat">
		                    <div class="row">
		                        <div class="col-md-12 col-xs-12 mp-latest-bsr">
		                                                    </div>
		                    </div>
		                    <div class="clearfix"></div>
		                </div>-->
		                <div class="panel-footer panel-stat">
		                    <ul class="list-unstyled nomargin clearfix">
		                        <li class="col-md-4 col-xs-4 text-center">
		                            <small class="stat-label">Gross</small>
		                            <h4>${{number_format($bestseller->gross_sales,2)}}</h4>
		                        </li>
		                        <li class="col-md-3 col-xs-3 text-center">
		                            <small class="stat-label">
		                                Profit
		                                <span class="fa fa-info-circle" tabindex="0" role="button" data-toggle="popover" data-html="true" data-container=".panel.panel-default.product{{$bestseller->product_id}}" data-cb-function="popoverContent" data-trigger="focus" data-placement="bottom" data-element="#product{{$bestseller->product_id}}" data-original-title="" title=""></span>                            </small>
		                            <div id="product{{$bestseller->product_id}}" class="hidden">
		    <table class="table stats-popover">
		        <tbody><tr>
		            <td><strong>Total:</strong></td>
		            <td class="text-right text-success">${{number_format($bestseller->total_gross,2)}}</td>
		        </tr>
		        <tr>
		            <td><strong>Tax:</strong></td>
		            <td class="text-right text-muted">${{number_format($bestseller->tax,2)}}</td>
		        </tr>
		        <tr>
		            <td><strong>Shipping:</strong></td>
		            <td class="text-right text-success">${{number_format($bestseller->shipping_fee,2)}}</td>
		        </tr>
		        <tr>
		            <td><strong>VAT Fees:</strong></td>
		            <td class="text-right text-danger">(${{number_format(abs($bestseller->vat_fee),2)}})</td>
		        </tr>
				<tr>
		            <td><strong>FBA Fees:</strong></td>
		            <td class="text-right text-danger">(${{number_format(abs($bestseller->fba_fee),2)}})</td>
		        </tr>
		        <tr>
		            <td><strong>Refer Fees:</strong></td>
		            <td class="text-right text-danger">(${{number_format(abs($bestseller->commission),2)}})</td>
		        </tr>
		        <tr>
		            <td><strong>Promo Value:</strong></td>
		            <td class="text-right text-danger">(${{number_format(abs($bestseller->total_promo),2)}})</td>
		        </tr>
		        <tr>
		            <td><strong>Product Refunds:</strong></td>
		            <td class="text-right text-danger">(${{number_format(abs($bestseller->total_refund),2)}})</td>
		        </tr>

                <tr>
	                <td><strong>Sponsored Ads:</strong></td>
	                <td class="text-right text-danger">(${{number_format($bestseller->adExpenses,2)}})</td>
	            </tr>
						<tr>
							<td><strong>AI ADs CR:</strong></td>
							<td class="text-right text-danger">({{$bestseller->ai_ads_commission_rate}}%)</td>
						</tr>
						<tr>
							<td><strong>AI ADS CMSion:</strong></td>
							<td class="text-right text-danger">({{number_format(0.01 * $bestseller->ai_ads_commission_rate * $bestseller->ads_sales, 2)}}%)</td>
						</tr>
		        <tr>
		            <td class="doubleBrdr text-right"><strong>Est. Payout:</strong></td>
		            
		            <td class="text-right doubleBrdr text-success"><strong>${{number_format($bestseller->payout,2)}}</strong></td>
		        </tr>
		        <tr>
		            <td><strong>Product/Ship Costs:</strong></td>
		            <td class="text-right text-danger">(${{number_format($bestseller->total_cost,2)}})</td>
		        </tr>
		        
		        
		        <tr>
		            <td class="doubleBrdr text-right"><strong>Profit:</strong></td>
		            <td class="text-right doubleBrdr text-success">
		                <strong>${{number_format($bestseller->profit,2)}}</strong>
		            </td>
		        </tr>

		        <tr>
		            <td class="doubleBrdr text-right"><strong>Margin:</strong></td>
		            <td class="text-right doubleBrdr text-success">
		                <strong>
						@if($bestseller->total_gross > 0)
			            {{number_format(100*$bestseller->profit/$bestseller->gross_sales,2)}}%
			            @else
			            N/A
						@endif
		                </strong>
		            </td>
		        </tr>
		    </tbody></table>
		</div>                            <h4 class="text-success">${{number_format($bestseller->profit,2)}}</h4>
		                        </li>
		                        <li class="col-md-2 col-xs-2 text-center">
		                            <small class="stat-label">ROI</small>
		                            <h4 class="text-success">
						            @if($bestseller->total_cost > 0)
						            {{number_format(100*$bestseller->profit/$bestseller->total_cost,0)}}%
						            @else
						            N/A
						            @endif                           
		        					</h4>
		                        </li>
		                        <li class="col-md-3 col-xs-3 text-center">
		                            <small class="stat-label">Units</small>
		                            <h4>{{number_format($bestseller->total_units)}}</h4>
		                        </li>
		                    </ul>
		                </div>
		            </div>
		        </div>

		<?php if(($key + 1) == $bestsellers_count) { ?>
			</div>
		<?php } ?>
		@endforeach

		</div>
		<!-- <div class="clearfix removeClearFix"></div>

		@if($pages > 1)
		<div class="col-md-12 text-center" style="position: relative; float: none;">
			<button id="loadMoreProducts" class="btn btn-lg btn-success" data-page="2">Load Page 2 of {{$pages}}</button>


            <div class="preloader" style="display: none;"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
        </div>
        @endif -->
</div>
            
<script type="text/javascript">

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

$('[data-toggle="popover"]').popover({
	content:function(){
		var elmt = $(this).attr('data-element')
		return $(elmt).html()

	}
})


// $('.merchant-dash-ajax #loadMoreProducts').click(function(){

// 	var page = $(this).attr('data-page');
// 	$('#page-'+page).show();

// 	var new_page = parseInt(page) + 1;
// 	if(new_page > {{$pages}}) {
// 		$('.merchant-dash-ajax #loadMoreProducts').hide();
// 	}

// 	$('.merchant-dash-ajax #loadMoreProducts').attr('data-page', new_page);
// 	$('.merchant-dash-ajax #loadMoreProducts').text('Load page ' + new_page + ' of {{$pages}}');
	

// })

</script>