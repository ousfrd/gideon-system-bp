<div class="mb20"></div>

<?php $pages = ceil($bestsellers_count / 24);
 ?>
<div class="row merchant-dash-ajax periodic-best-sellers" id="merchant-products">

    <div class="panel-content">

    	<div class="row">


		</div>

    </div>


		<div class="clearfix removeClearFix"></div>

		<div class="col-md-12 text-center" style="position: relative; float: none;">
			<button id="loadMoreProducts" class="btn btn-lg btn-success" data-page="2" data-href="{{route('listing.call_backs.periodicBestSellers')}}"
                 data-range="{{$date_range}}"
                 data-start-date="{{$start_date}}"
                 data-end-date="{{$end_date}}"
                 data-options='{!! json_encode($params) !!}' data-offset=0>Load Page 1 of {{$pages}}</button>


            <div class="preloader" style="display: none;"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
        </div>

                
</div>
                                            
            
<script type="text/javascript">
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
	
$('[data-toggle="popover"]').popover({
	content:function(){
		var elmt = $(this).attr('data-element')
		return $(elmt).html()

	}
})




$(document).ready(function() {

	$('.merchant-dash-ajax #loadMoreProducts').click(function(){
		if ($(this).attr('data-href') != null) {
			$(this).hide();
			$('.merchant-dash-ajax .preloader').show();
			var params = $.parseJSON($(this).attr('data-options'));
			params['start_date'] = $(this).attr('data-start-date');
			params['end_date'] = $(this).attr('data-end-date');
			params['data_range'] = $(this).attr('data-range');
			params['offset'] = $(this).attr('data-offset');
			$('.merchant-dash-ajax .panel-content>.row').append($('<div>').load($(this).attr('data-href'),params,function(){
				var new_page = (parseInt(params['offset']) + 24) / 24 + 1;

				if( new_page <= {{$pages}}) {
					$('.merchant-dash-ajax #loadMoreProducts').attr('data-offset', parseInt(params['offset']) + 24);
					$('.merchant-dash-ajax #loadMoreProducts').text('Load page ' + new_page + ' of {{$pages}}');
					$('.merchant-dash-ajax #loadMoreProducts').show();
				}

				$('.merchant-dash-ajax .preloader').hide();
			}))
		}


	})


	$('.merchant-dash-ajax #loadMoreProducts').click();
})

</script>