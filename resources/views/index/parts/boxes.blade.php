<div class="mb20"></div>

<div class="row merchant-dash-ajax" id="periodic-gross-sales">
                
                @if((strtotime($end_date) - strtotime($start_date)) == 0)
                    <?php 
                        $box_data_range = $date_range;
                        $box_start_date = $start_date;
                        $box_end_date = $end_date;
                    ?>
                    <div class="col-md-3 col-sm-6 periodic-gross-sales" data-box="boxOne"
                    data-href="{{$box_route}}"
                     data-range="{{$box_data_range}}"
                     data-start-date="{{$box_start_date}}"
                     data-end-date="{{$box_end_date}}"
                     data-options='{!! json_encode($params) !!}'
                    >
                        <div class="panel panel-success dark-bottom panel-stat">
                            <div class="panel-heading border-bottom-0">
                                <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                            </div>
                        </div>
                    </div>

                    <?php 
                        $box_data_range = "1 Day";
                        $box_start_date = date('Y-m-d',strtotime($start_date) - 1 * 24 * 3600);
                        $box_end_date = date('Y-m-d',strtotime($start_date) - 1 * 24 * 3600);
                    ?>
                    <div class="col-md-3 col-sm-6 periodic-gross-sales" data-box="boxTwo"
                    data-href="{{$box_route}}"
                     data-range="{{$box_data_range}}"
                     data-start-date="{{$box_start_date}}"
                     data-end-date="{{$box_end_date}}"
                     data-options='{!! json_encode($params) !!}'
                    >
                        <div class="panel panel-primary dark-bottom panel-stat">
                            <div class="panel-heading border-bottom-0">
                                <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                            </div>
                        </div>
                    </div>

                    <?php 
                        $box_data_range = "7 Day";
                        $box_start_date = date('Y-m-d',strtotime($start_date) - 7 * 24 * 3600);
                        $box_end_date = date('Y-m-d',strtotime($start_date) - 1 * 24 * 3600);
                    ?>
                    <div class="col-md-3 col-sm-6 periodic-gross-sales" data-box="boxThree"
                    data-href="{{$box_route}}"
                     data-range="{{$box_data_range}}"
                     data-start-date="{{$box_start_date}}"
                     data-end-date="{{$box_end_date}}"
                     data-options='{!! json_encode($params) !!}'
                    >
                        <div class="panel panel-warning dark-bottom panel-stat">
                            <div class="panel-heading border-bottom-0">
                                <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                            </div>
                        </div>
                    </div>

                    <?php 
                        $box_data_range = "30 Day";
                        $box_start_date = date('Y-m-d',strtotime($start_date) - 30 * 24 * 3600);
                        $box_end_date = date('Y-m-d',strtotime($start_date) - 1 * 24 * 3600);
                    ?>
                    <div class="col-md-3 col-sm-6 periodic-gross-sales" data-box="boxFour"
                    data-href="{{$box_route}}"
                     data-range="{{$box_data_range}}"
                     data-start-date="{{$box_start_date}}"
                     data-end-date="{{$box_end_date}}"
                     data-options='{!! json_encode($params) !!}'
                    >
                        <div class="panel panel-danger dark-bottom panel-stat">
                            <div class="panel-heading border-bottom-0">
                                <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                            </div>
                        </div>
                    </div>

                @else

                    <?php 
                        $box_data_range = $date_range;
                        $box_start_date = $start_date;
                        $box_end_date = $end_date;
                    ?>
                    <div class="col-md-4 col-sm-12 periodic-gross-sales" data-box="boxOne"
                    data-href="{{$box_route}}"
                     data-range="{{$box_data_range}}"
                     data-start-date="{{$box_start_date}}"
                     data-end-date="{{$box_end_date}}"
                     data-options='{!! json_encode($params) !!}'
                    >
                        <div class="panel panel-success dark-bottom panel-stat">
                            <div class="panel-heading border-bottom-0">
                                <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                            </div>
                        </div>
                    </div>


                    @if ((strtotime($end_date) - strtotime($start_date)) <= 7 * 24 * 3600)


                        <?php 
                            $box_data_range = "Same Time Prev Week";
                            $box_start_date = $start_date_last_week;
                            $box_end_date = $end_date_last_week;
                        ?>
                        <div class="col-md-4 col-sm-12 periodic-gross-sales" data-box="boxTwo"
                        data-href="{{$box_route}}"
                         data-range="{{$box_data_range}}"
                         data-start-date="{{$box_start_date}}"
                         data-end-date="{{$box_end_date}}"
                         data-options='{!! json_encode($params) !!}'
                        >
                            <div class="panel panel-primary dark-bottom panel-stat">
                                <div class="panel-heading border-bottom-0">
                                    <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                                </div>
                            </div>
                        </div>

                    @endif

                    @if ((strtotime($end_date) - strtotime($start_date)) <= 31 * 24 * 3600)

                        <?php 
                            $box_data_range = "Same Time Prev Month";
                            $box_start_date = $start_date_last_month;
                            $box_end_date = $end_date_last_month;
                        ?>
                        <div class="col-md-4 col-sm-12 periodic-gross-sales" data-box="boxThree"
                        data-href="{{$box_route}}"
                         data-range="{{$box_data_range}}"
                         data-start-date="{{$box_start_date}}"
                         data-end-date="{{$box_end_date}}"
                         data-options='{!! json_encode($params) !!}'
                        >
                            <div class="panel panel-warning dark-bottom panel-stat">
                                <div class="panel-heading border-bottom-0">
                                    <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                                </div>
                            </div>
                        </div>

                    @endif


                    @if ((strtotime($end_date) - strtotime($start_date)) > 7 * 24 * 3600)


                        <?php 
                            $box_data_range = "Same Time Prev Year";
                            $box_start_date = $start_date_last_year;
                            $box_end_date = $end_date_last_year;
                        ?>
                        <div class="col-md-4 col-sm-12 periodic-gross-sales" data-box="boxTwo"
                        data-href="{{$box_route}}"
                         data-range="{{$box_data_range}}"
                         data-start-date="{{$box_start_date}}"
                         data-end-date="{{$box_end_date}}"
                         data-options='{!! json_encode($params) !!}'
                        >
                            <div class="panel panel-danger dark-bottom panel-stat">
                                <div class="panel-heading border-bottom-0">
                                    <div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div>
                                </div>
                            </div>
                        </div>


                    @endif

                @endif
</div>
                                            
                                            
<style>
.periodic-gross-sales .panel-heading .preloader, #merchant-products .preloader{width:44px;height:44px;top:50%;left:50%;margin-top:-22px;margin-left:-22px}
</style>

<script>
$(document).ready(function(){
    $('.merchant-dash-ajax .periodic-gross-sales').each(function(){
        if ($(this).attr('data-href') != null) {
            var params = $.parseJSON($(this).attr('data-options'));
            params['start_date'] = $(this).attr('data-start-date');
            params['end_date'] = $(this).attr('data-end-date');
            params['data_range'] = $(this).attr('data-range');
            $(this).find('.panel-heading').load($(this).attr('data-href'),params,function(){})
        }

    })
})
</script>