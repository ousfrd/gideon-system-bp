<div class="row">
 <div class="col-xs-12  col-sm-12  ">
 <div class="table-responsive">
 <table class="table table-striped">

 <thead>
 <tr>
 <th width=150>Date</th>
 <th>Units Sold</th>
 <th>Gross Sales</th>
  @if((strtotime($end_date) - strtotime($start_date)) == 0)
  <th>Units Sold Yesterday</th>
 <th>Gross Sales Yesterday</th>
 @endif

 @if ((strtotime($end_date) - strtotime($start_date)) <= 7 * 24 * 3600)
 <th>Units Sold Prev Week</th>
 <th>Gross Sales Prev Week</th>
 @endif

  @if ((strtotime($end_date) - strtotime($start_date)) <= 31 * 24 * 3600)
 <th>Units Sold Prev Month</th>
 <th>Gross Sales Prev Month</th>
  @endif

@if ((strtotime($end_date) - strtotime($start_date)) > 7 * 24 * 3600)
 <th>Units Sold Prev Year</th>
 <th>Gross Sales Prev Year</th>
 @endif

 </tr>
 </thead>


<tbody>
@foreach($orders as $date => $order)
<?php if($date <= $end_date) { ?>
<tr>
<th>{{$date}}</th>
<td>{{$order->total_units}}</td>
<td>${{number_format($order->revenue,2)}}</td>

@if((strtotime($end_date) - strtotime($start_date)) == 0)

<?php $total_1 = $orderReportYesterday[is_int($date)?$date:date('Y-m-d',strtotime($date)-1*24*3600 )]->total_units;?>
<td>{{$total_1}}
@if($total_1>0)
<span @if($order->total_units > $total_1) class="text-success" @else class="text-danger" @endif>({{number_format(100*($order->total_units - $total_1)/$total_1,1)}}%)</span>
@endif
</td>
<?php $gross_1 = $orderReportYesterday[is_int($date)?$date:date('Y-m-d',strtotime($date)-1*24*3600 )]->revenue;?>
<td>${{number_format($gross_1,2)}}
@if($gross_1>0)
<span @if($order->revenue > $gross_1) class="text-success" @else class="text-danger" @endif>({{number_format(100*($order->revenue - $gross_1)/$gross_1, 1)}}%)</span>
@endif
</td>
@endif


 @if ((strtotime($end_date) - strtotime($start_date)) <= 7 * 24 * 3600)


<?php 
$date_last_week = date('Y-m-d',strtotime("-1 week",strtotime ($date) ));
if($date_last_week <= $end_date_last_week) {
$total_7 = $orderReportLastWeek[is_int($date)?$date:$date_last_week]->total_units;?>
<td>{{$total_7}}
@if($total_7>0)
<span @if($order->total_units > $total_7) class="text-success" @else class="text-danger" @endif>({{number_format(100*($order->total_units - $total_7)/$total_7,1)}}%)</span>
@endif
</td>
<?php $gross_7 = $orderReportLastWeek[is_int($date)?$date:$date_last_week]->revenue;?>
<td>${{number_format($gross_7,2)}}
@if($gross_7>0)
<span @if($order->revenue > $gross_7) class="text-success" @else class="text-danger" @endif>({{number_format(100*($order->revenue - $gross_7)/$gross_7, 1)}}%)</span>
@endif
</td>
<?php } ?>
@endif


  @if ((strtotime($end_date) - strtotime($start_date)) <= 31 * 24 * 3600)

<?php 
$date_last_month = date('Y-m-d',strtotime("-1 month",strtotime ($date) ));
if($date_last_month <= $end_date_last_month) {
$total_30 = $orderReportLastMonth[is_int($date)?$date:$date_last_month]->total_units;?>
<td>{{$total_30}}
@if($total_30>0)
<span @if($order->total_units > $total_30) class="text-success" @else class="text-danger" @endif>({{number_format(100*($order->total_units - $total_30)/$total_30, 1)}}%)</span>
@endif
</td>
<?php $gross_30 = $orderReportLastMonth[is_int($date)?$date:$date_last_month]->revenue;?>
<td>${{number_format($gross_30,2)}}
@if($gross_30>0)
<span @if($order->revenue > $gross_30) class="text-success" @else class="text-danger" @endif>({{number_format(100*($order->revenue - $gross_30)/$gross_30, 1)}}%)</span>
@endif
</td>
<?php } ?>
@endif


@if ((strtotime($end_date) - strtotime($start_date)) > 7 * 24 * 3600)

<?php 
$date_last_year = date('Y-m-d',strtotime ("-1 year",strtotime ($date)));
if($date_last_year <= $end_date_last_year) {
$total_365 = $orderReportLastYear[is_int($date)?$date:$date_last_year]->total_units;?>
<td>{{$total_365}}
@if($total_365>0)
<span @if($order->total_units > $total_365) class="text-success" @else class="text-danger" @endif>({{number_format(100*($order->total_units - $total_365)/$total_365, 1)}}%)</span>
@endif
</td>
<?php $gross_365 = $orderReportLastYear[is_int($date)?$date:$date_last_year]->revenue;?>
<td>${{number_format($gross_365,2)}}
@if($gross_365>0)
<span @if($order->revenue > $gross_365) class="text-success" @else class="text-danger" @endif>({{number_format(100*($order->revenue - $gross_365)/$gross_365, 1)}}%)</span>
@endif
</td>
<?php } ?>
@endif

</tr>
<?php } ?>
@endforeach



</tbody>

</table>
</div></div>
</div>            
            
            
            
