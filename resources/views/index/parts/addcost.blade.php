@if(count($nocost_products) > 0)
<h3>{{count($nocost_products)}} products need to input Cost and Shipping cost (in USD)</h3>
<ul class="list-group">
@foreach($nocost_products as $product)

	<li class="list-group-item product-{{$product->id}}">
		<a href="{{ route ( 'product.view', [ 'id' => $product->id ] ) }}" target="_blank">{{$product->asin}}</a> - {{str_limit($product->name, 70)}} : <input type="text" value="" name="cost" class="cost" placeholder="Cost">&nbsp;&nbsp;&nbsp;<input type="text" value="" name="shipping_cost" class="shipping_cost" placeholder="Shipping Cost">&nbsp;&nbsp;&nbsp;
		<button type="button" class="btn btn-success" data-pid="{{$product->id}}">Save</button>
		<i class="fa fa-spinner fa-spin" style="display: none;"></i>
	</li>

@endforeach
</ul>


<script type="text/javascript">
	$(document).ready(function() {

		$('.list-group button').click(function(el) {
				var id = $(this).data('pid');
				var cost = $('.product-'+id).find('.cost').val()
				var shipping_cost = $('.product-'+id).find('.shipping_cost').val()
				
				$('.product-'+id).find('.btn').hide()
				$('.product-'+id).find('.fa-spinner').show()
				// console.log(cost, shipping_cost, id);

				$.ajax({
					url: '{{ route("utils.ajax.product-addcost") }}',
					data: { id: id, cost: cost, shipping_cost: shipping_cost },
					dataType: 'json'
				}).success(function(data) {
					if(data.success) {
						$('.product-'+data.id).remove()
					} else {
						$("#alerts .bg-danger ul").append('<li>'+data.error_msg+'</li>');
						$('.product-'+id).find('.btn').show()
						$('.product-'+id).find('.fa-spinner').hide()
					}
				})
		})


	})



</script>

@endif