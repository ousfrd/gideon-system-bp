<div class="form-group row">
    {{ Form::label("Shipment Name", null, ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
   		 {{ Form::text('data[shipment_name]', '', array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label("Quantity", null, ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
   		 {{ Form::number('data[qty]', '', array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label("Cost Per Unit (USD$)", null, ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
   		 {{ Form::number('data[cost]', '', array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label("Shipping Per Unit (USD$)", null, ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
   		 {{ Form::number('data[shipping_cost]', '', array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label("Shipment Status", null, ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
   		 {{ Form::select('data[shipment_status]',['WORKING'=>'WORKING','SHIPPED'=>'SHIPPED'], 'WORKING', array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
    </div>
</div>


 <h4>Ship From</h4>
<div class="form-group row">
    {{ Form::label("Name", null, ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
   		 {{ Form::text('data[shipFromAddress][Name]', '', array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
    </div>
</div>    
<div class="form-group row">
    {{ Form::label("Address", null, ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
   		 {{ Form::text('data[shipFromAddress][AddressLine1]', '', array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
    </div>
</div>  
<div class="form-group row">
    {{ Form::label("City", null, ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
   		 {{ Form::text('data[shipFromAddress][City]', '', array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
    </div>
</div>  
<div class="form-group row">
    {{ Form::label("StateOrProvinceCode", null, ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
   		 {{ Form::text('data[shipFromAddress][StateOrProvinceCode]', '', array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
    </div>
</div>  
<div class="form-group row">
    {{ Form::label("PostalCode", null, ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
   		 {{ Form::text('data[shipFromAddress][PostalCode]', '', array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
    </div>
</div>  
<div class="form-group row">
    {{ Form::label("CountryCode", null, ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
   		 {{ Form::text('data[shipFromAddress][CountryCode]', '', array_merge(['class' => 'form-control','required','placeholder'=>''])) }}
    </div>
</div> 