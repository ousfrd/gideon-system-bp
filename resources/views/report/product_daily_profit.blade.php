@extends('layouts.master')
@section('title', $title)
@section('content')


  <div id="container"></div>

  <script>
    let accounts = <?php echo json_encode($accounts); ?>;
    let marketplaces = <?php echo json_encode($marketplaces); ?>;
    let fetchdailyprofits_url = '<?php echo route('report.fetchdailyprofits'); ?>';
    let fetchdailyprofitsrows_url = '<?php echo route('report.fetchdailyprofitsrows'); ?>';
    let updateProduct_url = '<?php echo route('product.ajaxsave'); ?>';
    let beginning = '<?php echo $beginning ?>';
  </script>

  <!-- Load our React component. -->
  {{ Html::script(mix('js/react_daily_profit.js')) }}


  <style type="text/css">
  	.color0 { color: rgb(91,195,76); }
  	.color1 { color: rgb(242,104,95); }
  	.color2 { color: rgb(250,172,94); }
  	.color3 { color: rgb(177,116,227); }
  	.color4 { color: rgb(93,157,252); }
  	.color5 { color: rgb(76,215,200); }
  	.color6 { color: rgb(234,82,171); }
  	.color7 { color: rgb(107,142,35); }
    .color8 { color: rgb(255,218,89); }
    .color9 { color: rgb(155,170,182); }
    .color10 { color: rgb(107,142,35); }
    .box-inlay { text-align: right; padding-bottom: 20px; }
    #series1 { margin-right: 10px; }
    .google_charts { margin-bottom: 35px; }
    .adImage { width: 75px; height: 75px; display: inline-block; margin-right: 25px; text-align: center }
    .adASIN { display: inline-block; vertical-align: top; }
    span.editable, span.editing { text-decoration: none; border-bottom: dashed 1px #0088cc; }
    span.editable:hover, span.editing { background-color: #FFFFC0; cursor: text; }
    .range-filter { width: 60px; margin-bottom: 5px; }
    .bid-adjust { width: 60px; }
    .checkbox-control { width: 100%; display: block; height: 16px; }
    .ag-theme-balham .ag-header {font-size: 14.5px!important;}
    .ag-theme-balham {font-size: 14px!important;}
    .ag-pinned-left-floating-top, .ag-floating-top-container { font-weight: 500; color: #1CAF9A;}
    .ag-theme-balham .ag-icon-expanded {background: transparent url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTIiIGhlaWdodD0iMTIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48cmVjdCBzdHJva2Utb3BhY2l0eT0iLjUiIHN0cm9rZT0iIzAwMCIgeD0iMS41IiB5PSIxLjUiIHdpZHRoPSI5IiBoZWlnaHQ9IjkiIHJ4PSIxIi8+PHBhdGggZmlsbD0iIzAwMCIgZD0iTTUgM2gydjZINXoiLz48cGF0aCBmaWxsPSIjMDAwIiBkPSJNOSA1djJIM1Y1eiIvPjwvZz48L3N2Zz4=) center no-repeat; cursor: pointer; }
    .ag-theme-balham .ag-icon-contracted {background: transparent url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTIiIGhlaWdodD0iMTIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48cmVjdCBzdHJva2Utb3BhY2l0eT0iLjUiIHN0cm9rZT0iIzAwMCIgeD0iMS41IiB5PSIxLjUiIHdpZHRoPSI5IiBoZWlnaHQ9IjkiIHJ4PSIxIi8+PHBhdGggZmlsbD0iIzAwMCIgZD0iTTkgNXYySDNWNXoiLz48L2c+PC9zdmc+) center no-repeat; cursor: pointer; }
  </style>


@endsection