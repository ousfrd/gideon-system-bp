@extends('layouts.master')
@section('title', $title)
@section('content')


  <div id="container"></div>

  <script>
    let fetchmonthlyprofits_url = '<?php echo route('report.fetchmonthlyprofits'); ?>';
    let updatePmGoal_url = '<?php echo route('report.updatePmGoal'); ?>'
  </script>

  <!-- Load our React component. -->
  {{ Html::script(mix('js/react_seller_profit.js')) }}


  <style type="text/css">
  	.color0 { color: rgb(91,195,76); }
  	.color1 { color: rgb(242,104,95); }
  	.color2 { color: rgb(250,172,94); }
  	.color3 { color: rgb(177,116,227); }
  	.color4 { color: rgb(93,157,252); }
  	.color5 { color: rgb(76,215,200); }
  	.color6 { color: rgb(234,82,171); }
  	.color7 { color: rgb(107,142,35); }
    .box-inlay { text-align: right; padding-bottom: 20px; }
    #series1 { margin-right: 10px; }
    .google_charts { margin-bottom: 35px; }
    .adImage { width: 75px; height: 75px; display: inline-block; margin-right: 25px; text-align: center }
    .adASIN { display: inline-block; vertical-align: top; }
    span.editable, span.editing { text-decoration: none; border-bottom: dashed 1px #0088cc; }
    span.editable:hover, span.editing { background-color: #FFFFC0; cursor: text; }
    .range-filter { width: 60px; margin-bottom: 5px; }
    .bid-adjust { width: 60px; }
    .checkbox-control { width: 100%; display: block; height: 16px; }
    .rc-switch {margin-left: 5px;}
  </style>


@endsection