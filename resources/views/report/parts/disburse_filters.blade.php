<div class="panel panel-default product-filter-form filter-form">
  <form>
    <div class="panel-heading clearfix">
      <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter padding-sm">
          <div class="input form-group select">
            <select  name="account" style="width: 100%" id="select-account" class="reportFilterField" data-minimum-input-length = 0 data-placeholder="Select Account"  removeDefaultClass="1">
              @if ($account == 'all' || $account == '')
              <option value="all" selected>All Accounts</option>
              @else
              <option value="all">All Accounts</option>
              @endif
              @foreach ($accounts as $acc)
                @if ($account == $acc->seller_id)
                <option value="{{$acc->seller_id}}" selected>{{$acc->code}} {{$acc->name}}</option>
                @else
                <option value="{{$acc->seller_id}}">{{$acc->code}} {{$acc->name}}</option>
                @endif
              @endforeach
            </select>
            </div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter salesChannelCont padding-sm">
          <div class="input form-group select">
            <select  name="marketplace" style="width: 100%" id="select-marketplace" class="reportFilterField">
              <option @if ($marketplace == 'all' || $marketplace == '') selected @endif value="all">All Marketplaces</option>
              @foreach($marketplaces as $code => $marketplace_name)
              <option value="{{$code}}" @if($marketplace == $code) selected @endif>{{ucfirst($marketplace_name)}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12 mobile-padding-filter padding-sm">
          @include('parts.filters.daterange')  
        </div>

        <div class="col-md-2 col-sm-2 col-xs-12">
          <div class="submit">
            <input type="submit" class="btn btn-success btn-block btn" id="reportFilterBtn" data-url="/merchant/dashboard" value="Filter" />
          </div>
        </div>
      </div>
    </div>
    <input name="start_date" type="hidden" value='{{$start_date}}'> 
    <input name="end_date" type="hidden" value='{{$end_date}}'> 
    <input name="date_range" type="hidden" value='{{$date_range}}'>  
  </form>               
</div>

<script>
$(document).ready(function(){
	$('.filter-form select').select2()
})
</script>