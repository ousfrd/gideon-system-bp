<div class="panel-group product-filter-form filter-form">

    <div class="panel panel-default">

      <form id = "reportForm">
      <div class="panel-heading clearfix">

                      <div class="row">
                          
                           <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter padding-sm">
                            <h5>Select Account:</h5>

                                <div class="input form-group select">
                                  <select  name="account" style="width: 100%" id="select-account" class="reportFilterField" data-minimum-input-length = 0 data-placeholder="Select Account"  removeDefaultClass="1">
                                    <option value="all" selected>All Accounts</option>
                                    @foreach ($accounts as $acc)
                                      <option value="{{$acc->seller_id}}">{{$acc->name}}</option>
                                    @endforeach
                                  </select>
                                  </div>
                           </div>
                           
                          <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter salesChannelCont padding-sm">
                            <h5>Select Marketplace:</h5>
                                <div class="input form-group select">
                                  <select  name="marketplace" style="width: 100%" id="select-marketplace" class="reportFilterField" data-minimum-input-length = 0 data-placeholder="Select Marketplace"  removeDefaultClass="1">
                                  <option value="all" selected>All Marketplaces</option>
                                  @foreach($marketplaces as $marketplace_name) 
                                  <option value="{{$marketplace_name}}">{{ucfirst($marketplace_name)}}</option>
                                  @endforeach
                                  </select>
                                  </div>
                          </div>


                          

                          <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter padding-sm">
                              <h5>Select Date Range:</h5>
                              @include('parts.filters.daterange')
                          </div>

<!--                            <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter salesChannelCont padding-sm">
                                <div class="input form-group select">
                                  <select  name="fulfillment" style="width: 100%" id="select-fulfillment" class="reportFilterField" data-minimum-input-length = 0 data-placeholder="Select Fulfillment"  removeDefaultClass="1">
                                  <option value="all" selected>All Fulfillment</option>
                                  <option value="afn">FBA Only</option>
                                  <option value="mfn">MFN Only</option>
                                  </select>
                                  </div>
                          </div> -->
                      </div>
    


                        <div class="row">

<!--                            <div class="col-md-6 col-sm-6 col-xs-12 mobile-padding-filter padding-sm">
                                <h5>Select Products:</h5>
                                <div class="input form-group">
                                  <input name="products" type="hidden" class="bigdrop" value='' id="select-asin" placeholder="All Products"> 
                                  </div>
                           </div> -->

<!--                            <div class="col-md-2 col-sm-2 col-xs-6 mobile-padding-filter padding-sm">
                                <div class="input form-group select">
                                  <select  name="order_status" style="width: 100%" id="select-marketplace" class="reportFilterField" data-minimum-input-length = 0 data-placeholder="Order Status"  removeDefaultClass="1" multiple="multiple">
                                  <option selected>All Status</option>
                                  <option value="Pending">Pending</option>
                                  <option value="Shipped">Shipped</option>
                                  <option value="Unshipped">Unshipped</option>
                                  </select>
                                  </div>
                           </div> -->

                        </div>



                      <div class="row">

                           <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter padding-sm">
                                <h5>Select Report Type:</h5>
                                <div class="input form-group select">
                                  <select  name="report_type" style="width: 100%" id="select-type" class="reportFilterField" data-minimum-input-length = 0 data-placeholder="Order Status"  removeDefaultClass="1">
                                  <option value="Products Profit Report" selected>Products Profit Report</option>
                                  <option value="Sellers Profit Report">Sellers Profit Report</option>
                                  <!-- <option value="Groups Profit Report">Groups Profit Report</option> -->
                                  <!-- <option value="Orders Report">Orders Report</option> -->
                                  <option value="Ads Report">Ads Report</option>
                                  <option value="Products Daily Profit Report">Products Daily Profit Report</option>
                                  <option value="Listing Inventory Report">Listing Inventory Report</option>
                                  <option value="Other Fees Report">Other Fees Report</option>

                                  </select>
                                  </div>
                           </div>

                      </div>



                      <div class="row">
                           <div class="col-md-3 col-sm-3 col-xs-12 mobile-padding-filter padding-sm">
                                <h5>Select Export File Type:</h5>
                                <div class="input form-group select">
                                  <select  name="file_type" style="width: 100%" id="select-account" class="reportFilterField" data-minimum-input-length = 0 data-placeholder="File Type"  removeDefaultClass="1">
                                    <option value="xls" selected>xls</option>
                                    <option value="xlsx">xlsx</option>
                                    <option value="csv">csv</option>
                                    <!-- <option value="pdf">pdf</option> -->
                                  </select>
                                  </div>
                           </div>
                      </div>


                        <div class="row" style="padding: 20px 0 0;">

                           <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="submit"><input type="button" class="btn btn-success btn-block" id="generateReportBtn" data-url="/reports/generate" value="Generate Report" /></div>                        </div>
                        
                        

                        </div>

      </div>


          <input name="start_date" type="hidden" value='{{$start_date}}'> 
          <input name="end_date" type="hidden" value='{{$end_date}}'> 
          <input name="date_range" type="hidden" value='{{$date_range}}'>  

   
    </form>
   
  </div>



</div>


<script>
$(document).ready(function(){
  $('.filter-form select').select2()
  

  $('#select-asin').select2({
    width: '100%',
    allowClear: true,
    multiple: true,
    minimumInputLength: 3,
    placeholder: "Search by Product ASIN",
    ajax: {
          url: "/utils/ajax/products",
          dataType: 'json',
          quietMillis: 250,
          data: function(term, page) {
              return {
                  q: term,
                  t: 'asin',
                  page: page
              };
          },
          results: function(data, page) {
              var more = (page * 15) < data.total_count;

              return {results: data.results, more: more};
          },
          cache: true
      },

      dropdownCssClass: "bigdrop",

      formatResult: function(element){
          return "<div>" + element.text + "</div>";
      },
      formatSelection: function(element){
          return element.id;
      },
      escapeMarkup: function(m) {
          return m;
      }
  });



  $('#generateReportBtn').click(function (evt) {
    evt.preventDefault(); 

    var url = $(this).data('url');
    var params = $('form').serialize();

    $.ajax({url: url,data: params,dataType: "json"}).done(function(msg) { console.log(msg); })

    $("#alerts").append('<div class="bs-callout bs-callout-sm bs-callout-success bg-success"><p class="text-success">Your data has been queued for processing.</p></div>');
    // console.log(params);

  })
  
})
</script>