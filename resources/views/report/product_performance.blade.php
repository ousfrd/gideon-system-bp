@extends('layouts.master')
@section('title', $title)
@section('content')

<div>
	<h3 class="bg-primary">Product Performance Formulas:</h3>
	<p class="text-info">ROI = Profit / Cost</p>
	<p class="text-info">ROI2 = Profit / (Cost + AD Cost)</p>
	<p class="text-info">Revenue = Gross + Shipping + VAT + FBA Fee + Promo + Refund + Tax + Commission + Giftwrap Fee + Other Fee</p>
	<p class="text-info">Cost = Product Cost + Shipping Cost</p>
	<p class="text-info">Payout = Revenue - Ad Cost - Tax</p>
	<p class="text-info">Profit = Payout - Cost</p>
	<p class="text-info">PPC Cnt = AD Orders</p>
	<p class="text-info">Price = Average price of all listings that sale price greater than 0</p>
</div>

@include('report.parts.product_performance_filters')
{!! $grid !!}

@endsection