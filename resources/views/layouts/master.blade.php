<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title> @yield('title')</title>
<meta name="referrer" content="never">
<meta name="referrer" content="no-referrer">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script><link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

{{ Html::Style(mix('css/all.css'))}}

{{ Html::script('js/multiselect/js/bootstrap-multiselect.js') }}
{{ Html::script('js/js.cookie.js') }}

<!-- Include Required Prerequisites -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
 
{{ Html::script('js/select2.js') }}

<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
 
 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
{{ Html::script('js/animatedModal.min.js') }}
  
<!--   <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script> -->
<!--  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
<!--   {{ Html::script('js/utils.js') }} -->
  
<meta name="csrf-token" content="{{ csrf_token() }}">

{{ Html::script('colorbox/jquery.colorbox-min.js') }}
{{ Html::script('js/d3.v3.min.js') }}
{{ Html::script('js/c3.min.js') }}
{{ Html::script(mix('js/all-app.js')) }}

</head><body>
<nav class="navbar navbar-inverse navbar-fixed-top no-margin">
  <div class="navbar-header fixed-brand">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle"> <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span> </button>
    <a class="navbar-brand" href="{{Request::root()}}">{{config('tenant.app.name')}}</a> </div>
  <div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
      <li class="active" >
        <button class="navbar-toggle collapse in" data-toggle="collapse" id="menu-toggle-2"> <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span></button>
      </li>
      
    </ul>
   
           <ul class="nav navbar-nav navbar-right">
          <ul class="nav navbar-nav">
            <li class="header-dropdown menu-default dropdown">
                <a id="navDropdown" role="button" class="menu-default dropdown-toggle" aria-haspopup="true" aria-expanded="false" href="">
                  <span class="user-name">Hi, {{ auth()->user()->first_name() }}&nbsp;<span class="glyphicon-user glyphicon"></span>
                  <span class="caret"></span>
                </a>
                <ul role="menu" class="menu-default dropdown-menu" aria-labelledby="navDropdown">
                  @can('view-own-dashboard')
                  <li role="presentation" class=""><a href="{{route('user.view', ['id' => auth()->user()->id ])}}" role="menuitem" tabindex="-1"><span class="glyphicon glyphicon-signal"></span> Dashboard</a></li>
                  <li role="separator" class="divider"></li>
                  @endcan
                  <li role="presentation" class=""><a href="{{route('user.change-password')}}" role="menuitem" tabindex="-1"><span class="glyphicon-cog glyphicon"></span> Change Password</a></li>
                  <li role="separator" class="divider"></li>
                  <li role="presentation" class=""><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="glyphicon-off glyphicon"></span> Logout</a></li>
                </ul>
            </li>
          </ul>
        </ul>
        
    <form class="navbar-form navbar-right" action = "{{route('search')}}">
      <input type="text" class="search form-control" required placeholder="Search Product Name, ASIN, SKU or Order Id" name="q">
      

    </form>

    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
  </div>
</nav>
<div  id="wrapper" class="toggled-2">
    @include('layouts.sidemenu')
    <div id="page-content-wrapper" style="">

        @include('layouts.parts.loader')

        <div id="alerts"></div>

        <div id="main-content" style="position:relative;padding-top:20px">

            <h1 class="page-header">@yield('title')</h1>


            @if(Session::has('msg'))

              <p class="text-danger">{{Session::get('msg')}}</p>

            @endif

            @if(Session::has('errors'))

              @foreach (Session::get('errors')->all() as $message)

                  <p class="text-danger">{{$message}}</p>

              @endforeach

            @endif
            @yield('info_content')
            @yield('content')
        </div>
    </div>
</div>



<!--DEMO01-->
    <div id="animatedModal">
        <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID  class="close-animatedModal" -->
        <div class="close-animatedModal close-animated-modal"> 
            CLOSE <i class="fa fa-times-circle-o"></i>
        </div>
            
        <div class="modal-content">
                 
        </div>
    </div>  
</body>
</html>
