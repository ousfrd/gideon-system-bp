<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
    
        <title> @yield('title')</title>
        
        <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>


{{ Html::style('js/multiselect/css/bootstrap-multiselect.css') }}
{{ Html::script('js/multiselect/js/bootstrap-multiselect.js') }}


 <meta name="csrf-token" content="{{ csrf_token() }}"> 
{{ Html::style('css/dashboard.css') }}
{{ Html::style('colorbox/colorbox.css') }}
{{ Html::script('colorbox/jquery.colorbox-min.js') }}
<script>
$(document).ready(function(){
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	})
	
	$(".cbox").colorbox();

	 $('select[multiple="multiple"]').multiselect();
})

</script>
<style>
.column-daily_ave_orders a {
  display: inline-block;
  width: 50px;
}

</style>
    </head>
    <body>
    
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{Request::root()}}"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
			
         	
        </div>
      </div>
    </nav>
    <div class="container-fluid">
      <div class="row">
       
        
		<div class="col-sm-12 main">
        <h1 class="page-header">@yield('title')</h1>
       	
       	@if(Session::has('msg'))
  <p class="text-danger">{{Session::get('msg')}}</p>
  @endif
  
            @yield('content')
        
        </div>
     </div></div>
        
    </body>
</html>