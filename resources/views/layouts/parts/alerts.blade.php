@if($low_stock_count>0 || $excess_count>0)
<div class="bs-callout bs-callout-sm bs-callout-danger bg-danger"  role="alert">
<ul style="margin-bottom: 0">
@if($low_stock_count)
<li>{{$low_stock_count}} of your @if($low_stock_count > 1) products are @else product is @endif out of stock or runing out of stock.
<a href="{{route('products.low')}}?Products[filters][status-eq]=1">View</a></li>
@endif

@if($excess_count)
<li>{{$excess_count}} of your @if($excess_count > 1) listings are @else listing is @endif excess and action is required.
<a href="{{route('listings.excess')}}">View</a></li>
@endif
</ul>
</div>
@endif