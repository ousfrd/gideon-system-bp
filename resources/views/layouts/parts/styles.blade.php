<style>
 *{ font-size:12.5px;font-family:Helvetica;}
  a{ color:#369;}
  a:hover{  color:#CC6600; text-decoration:none;}
 
#page-content-wrapper{position:relative}
.btn-actions{position:absolute;right: 20px;
    top: 10px;}
.back-btn{margin-top:8px}
.qty{max-width:100px}

.btn-actions .btn{margin-left:10px}
#wait-loader{z-index:1001}


.column-cost,.column-last_30_days_orders,.column-total_qty,.column-last_7_days_orders,.daily_ave_orders,.column-sku,.column-manufacture_days{white-space: nowrap;}
.dl-horizontal.orderItemDetail dt {
    float: left;
    width: 70px;
    overflow: hidden;
    clear: left;
    text-align: right;
    text-overflow: ellipsis;
    white-space: nowrap;
}
.dl-horizontal.orderItemDetail dd {
    margin-left: 80px;
    margin-right:10px;
}

.table tr.doubleBorder td {
    border-top: 1px dotted #bbb;
    border-bottom: 1px dotted #bbb;
}

.table tr.doubleBorder.doubleTotal td {
    border-bottom: 3px double #bbb;
}

.bg-danger {
    background-color: #f2dede !important;
}

.bg-warning {
    background-color: #fcf8e3 !important;
}

.bs-callout {
    padding: 10px;
    margin: 20px 0 0;
    margin-top:0;
    border: 1px solid #eee;
    border-left-width: 5px;
    border-radius: 3px;
}
.bs-callout h4 {
    margin-top: 0;
    margin-bottom: 5px;
}
.bs-callout p:last-child {
    margin-bottom: 0;
}
.bs-callout code {
    border-radius: 3px;
}
.bs-callout+.bs-callout {
    margin-top: -5px;
}
.bs-callout-default {
    border-left-color: #777;
}
.bs-callout-default h4 {
    color: #777;
}
.bs-callout-primary {
    border-left-color: #428bca;
}
.bs-callout-primary h4 {
    color: #428bca;
}
.bs-callout-success {
    border-left-color: #5cb85c;
}
.bs-callout-success h4 {
    color: #5cb85c;
}
.bs-callout-danger {
    border-left-color: #d9534f;
}
.bs-callout-danger h4 {
    color: #d9534f;
}
.bs-callout-warning {
    border-left-color: #f0ad4e;
}
.bs-callout-warning h4 {
    color: #f0ad4e;
}
.bs-callout-info {
    border-left-color: #5bc0de;
}
.bs-callout-info h4 {
    color: #5bc0de;
}

.mb20 {
    margin-bottom: 20px;
}

.panel-success.dark-bottom{
box-shadow:none;
-webkit-box-shadow:none;
}

/* @group Buttons */

.btn{
	font-size: 12px;
    border-radius: 2px;
}

.btn:focus, .btn:active:focus, .btn.active:focus {
    outline: none;
    outline-offset: 0;
}

/*.btn-default {
    background-color: #c7cbd6;
    border-color: #c7cbd6;
    color: #32426B;
}

.btn-group > .btn.btn-default:last-child:not(:first-child), 
.btn-group > .btn-default.dropdown-toggle:not(:first-child){
	border-left: 1px solid #9BA2B7;
}

.btn-default:hover, .btn-default:focus, .btn-default:active, .btn-default.active, .open .dropdown-toggle.btn-default {
    background-color: #b0b5b9;
    border-color: #b0b5b9;
    color: #32426B;
}

.btn-default.disabled, .btn-default[disabled], fieldset[disabled] .btn-default, .btn-default.disabled:hover, .btn-default[disabled]:hover, fieldset[disabled] .btn-default:hover, .btn-default.disabled:focus, .btn-default[disabled]:focus, fieldset[disabled] .btn-default:focus, .btn-default.disabled:active, .btn-default[disabled]:active, fieldset[disabled] .btn-default:active, .btn-default.disabled.active, .btn-default.active[disabled], fieldset[disabled] .btn-default.active{
	background-color: #c7cbd6;
	border-color: #c7cbd6;
}*/

.btn-primary {
    background-color: #1fb5ad;
    border-color: #1fb5ad;
    color: #FFFFFF;
}

.btn-group > .btn.btn-primary:last-child:not(:first-child), 
.btn-group > .btn-primary.dropdown-toggle:not(:first-child){
	border-left: 1px solid #00ABA4;
}

.btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open .dropdown-toggle.btn-primary {
    background-color: #1ca59e;
    border-color: #1ca59e;
    color: #FFFFFF;
}

.btn-primary.disabled, .btn-primary[disabled], fieldset[disabled] .btn-primary, .btn-primary.disabled:hover, .btn-primary[disabled]:hover, fieldset[disabled] .btn-primary:hover, .btn-primary.disabled:focus, .btn-primary[disabled]:focus, fieldset[disabled] .btn-primary:focus, .btn-primary.disabled:active, .btn-primary[disabled]:active, fieldset[disabled] .btn-primary:active, .btn-primary.disabled.active, .btn-primary.active[disabled], fieldset[disabled] .btn-primary.active{
	background-color: #1fb5ad;
	border-color: #1fb5ad;
}

.btn-success {
    background-color: #95b75d;
    border-color: #95b75d;
    color: #FFFFFF;
}

.btn-group > .btn.btn-success:last-child:not(:first-child), 
.btn-group > .btn-success.dropdown-toggle:not(:first-child){
	border-left: 1px solid #729638;
}

.btn-success:hover, .btn-success:focus, .btn-success:active, .btn-success.active, .open .dropdown-toggle.btn-success {
    background-color: #88a755;
    border-color: #88a755;
    color: #FFFFFF;
}

.btn-success.disabled, .btn-success[disabled], fieldset[disabled] .btn-success, .btn-success.disabled:hover, .btn-success[disabled]:hover, fieldset[disabled] .btn-success:hover, .btn-success.disabled:focus, .btn-success[disabled]:focus, fieldset[disabled] .btn-success:focus, .btn-success.disabled:active, .btn-success[disabled]:active, fieldset[disabled] .btn-success:active, .btn-success.disabled.active, .btn-success.active[disabled], fieldset[disabled] .btn-success.active{
	background-color: #95b75d;
	border-color: #95b75d;
}

.btn-info {
    background-color: #57c8f1;
    border-color: #57c8f1;
    color: #FFFFFF;
}

.btn-info:hover, .btn-info:focus, .btn-info:active, .btn-info.active, .open .dropdown-toggle.btn-info {
    background-color: #53bee6;
    border-color: #53bee6;
    color: #FFFFFF;
}

/*.btn-warning {
    background-color: #57c8f1;
    border-color: #57c8f1;
    color: #FFFFFF;
}

.btn-warning:hover, .btn-warning:focus, .btn-warning:active, .btn-warning.active, .open .dropdown-toggle.btn-warning {
    background-color: #53bee6;
    border-color: #53bee6;
    color: #FFFFFF;
}*/

.btn-danger {
    background-color: #DF040A;
    border-color: #DF040A;
    color: #FFFFFF;
}

.btn-danger:hover, .btn-danger:focus, .btn-danger:active, .btn-danger.active, .open .dropdown-toggle.btn-danger {
    background-color: #FD2C31;
    border-color: #FD2C31;
    color: #FFFFFF;
}

.btn-inverse {
    background-color: #193048;
    border-color: #193048;
    color: #fff;
}

.btn-inverse:hover, .btn-inverse:focus, .btn-inverse:active, .btn-inverse.active, .open .dropdown-toggle.btn-inverse {
    background-color: #294058;
    border-color: #294058;
    color: #fff;
}

.btn-orange {
    background-color: #FF5A05;
    border-color: #FF5A05;
    color: #fff;
}

.btn-orange:hover, .btn-orange:focus, .btn-orange:active, .btn-orange.active, .open .dropdown-toggle.btn-orange {
    background-color: #FF7C38;
    border-color: #FF7C38;
    color: #fff;
}

.btn-inverse-light {
    background-color: #5f738a;
    border-color: #5f738a;
    color: #fff;
}

.btn-inverse-light:hover, .btn-inverse-light:focus, .btn-inverse-light:active, .btn-inverse-light.active, .open .dropdown-toggle.btn-inverse-light {
    background-color: #5f738a;
    border-color: #5f738a;
    color: #fff;
}

.btn-white {
    box-shadow: none !important;
}

.btn-xs, 
.btn-group-xs > .btn,
.btn-group-xs > .btn + .dropdown-toggle{
	padding: 2px 5px;	
}

.input-group-btn .btn{
	padding-bottom: 5px;
}
/* @end */

/* @group jQuery Overrides */

.ui-state-default, 
.ui-widget-content .ui-state-default, 
.ui-widget-header .ui-state-default {
	outline: none;	
}

/* @end */

/* @group Select 2 Overrides */

.modal .select2-container {
	z-index: 2001;
}

#container .select2-container--default .select2-selection--single {
	border-color: #cccccc;
	border-radius: 3px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
}

#container .select2-container--default.select2-container--open.select2-container--below .select2-selection--single, 
#container .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
	border-bottom-left-radius: 0;
	border-bottom-right-radius: 0;
}

#container .select2-container--default .select2-selection--single .select2-selection__arrow b {
	border-color: #333 transparent transparent;
	margin-top: 0;
}

#container .select2-container .select2-selection--single {
	height: 31px;
}

#container .select2-container--default .select2-selection--single .select2-selection__rendered {
	color: #333;
	height: 100%;
}

body .select2-dropdown {
	border: 1px solid #cccccc;
}

body .select2-container--default .select2-search--dropdown .select2-search__field {
	border: 1px solid #cccccc;
}

/* @end */

/* @group Button Group */

.btn-group-sm .dropdown-menu{
	font-size: 12px;
}

.btn-group-xs .dropdown-menu{
	font-size: 12px;
	padding: 2px 0;
}

/* @group Default */

/*.btn-group.b-default .dropdown-menu{
	background: #c7cbd6;
}

.btn-group.b-default .dropdown-menu > li{
	border-bottom: 1px solid #b0b5b9;
	border-top: 1px solid #E0E1E2;
}

.btn-group.b-default .dropdown-menu > li:first-child{
	border-top: none;
}

.btn-group.b-default .dropdown-menu > li:last-child{
	border-bottom: none;
}*/

.btn-group.b-default .dropdown-menu > li > a{
	/*color: #32426B;*/
	padding: 4px 12px;
}

.btn-group.b-default .dropdown-menu > li > a:hover,
.btn-group.b-default .dropdown-menu > li > a:focus {
  /*color: #fff;
  background-color: #707C9B;*/
}

/* @end */

/* @group Primary */

.btn-group.b-primary .dropdown-menu{
	background: #1fb5ad;
}

.btn-group.b-primary .dropdown-menu > li{
	border-top: 1px solid #40C4BF;
	border-bottom: 1px solid #00ABA4;
}

.btn-group.b-primary .dropdown-menu > li:first-child{
	border-top: none;
}

.btn-group.b-primary .dropdown-menu > li:last-child{
	border-bottom: none;
}

.btn-group.b-primary .dropdown-menu > li > a{
	color: #fff;
	padding: 4px 12px;
	font-size: 12px;
}

.btn-group.b-primary .dropdown-menu > li > a:hover,
.btn-group.b-primary .dropdown-menu > li > a:focus {
  color: #fff;
  background-color: #1ca59e;
}

/* @end */

/* @group Info */

.btn-group.b-info .dropdown-menu{
	background: #57c8f1;
}

.btn-group.b-info .dropdown-menu > li{
	border-top: 1px solid #32B6E9;
	border-bottom: 1px solid #80D6F6;
}

.btn-group.b-info .dropdown-menu > li:first-child{
	border-top: none;
}

.btn-group.b-info .dropdown-menu > li:last-child{
	border-bottom: none;
}

.btn-group.b-info .dropdown-menu > li > a{
	color: #fff;
	padding: 4px 12px;
	font-size: 12px;
}

.btn-group.b-info .dropdown-menu > li > a:hover,
.btn-group.b-info .dropdown-menu > li > a:focus {
  color: #fff;
  background-color: #53bee6;
}

/* @end */

/* @group Inverse */

.btn-group.b-inverse .dropdown-menu{
	background: #193048;
}

.btn-group.b-inverse .dropdown-menu > li{
	border-top: 1px solid #111D2E;
	border-bottom: 1px solid #303B49;
}

.btn-group.b-inverse .dropdown-menu > li:first-child{
	border-top: none;
}

.btn-group.b-inverse .dropdown-menu > li:last-child{
	border-bottom: none;
}

.btn-group.b-inverse .dropdown-menu > li > a{
	color: #fff;
	padding: 4px 12px;
	font-size: 12px;
}

.btn-group.b-inverse .dropdown-menu > li > a:hover,
.btn-group.b-inverse .dropdown-menu > li > a:focus {
	color: #fff;
	background-color: #4A586B;
}

/* @end */

/* @group Orange */

.btn-group.b-orange .dropdown-menu{
	background: #FF5A05;
}

.btn-group.b-orange .dropdown-menu > li{
	border-top: 1px solid #FF9F53;
	border-bottom: 1px solid #FF7C38;
}

.btn-group.b-orange .dropdown-menu > li:first-child{
	border-top: none;
}

.btn-group.b-orange .dropdown-menu > li:last-child{
	border-bottom: none;
}

.btn-group.b-orange .dropdown-menu > li > a{
	color: #fff;
	padding: 4px 12px;
	font-size: 12px;
}

.btn-group.b-orange .dropdown-menu > li > a:hover,
.btn-group.b-orange .dropdown-menu > li > a:focus {
  color: #fff;
  background-color: #FF7C38;
}

/* @end */

/* @end */

/* @group Modals */

.modal{
	z-index: 1047;
}

.right-sidebar .modal{
	z-index: 1049;
}

.modal-dialog {
	margin: 60px auto;
}

/* @end */

/* @group Popover */

.popover {
	max-width: 650px;
}

.popover.editable-container.editable-popup {
	z-index: 1046;
}

.popover-footer {
  margin: 0;
  padding: 8px 14px;
  font-size: 14px;
  font-weight: 400;
  line-height: 18px;
  background-color: #F7F7F7;
  border-top: 1px solid #EBEBEB;
  border-radius: 5px 5px 0 0;
}

.popover-footer ul.pagination.popover-navigation {
	margin: 0;
	padding: 0;
}

.popover-footer ul.pagination > li > a, 
.popover-footer ul.pagination > li > span {
	background: #ffffff;
	border: 1px solid #ddd;
	padding: 5px 10px;
}

.popover-footer ul.pagination > li.active > a, 
.popover-footer ul.pagination > li > a:hover {
	background: #1fb5ad;
}

/* @end */

/* @group Alerts */

.alert{
	border-radius: 2px;
}

.alert-primary{
	background-color: #1fb5ad;
	border: none;
	color: #FFFFFF;
}

.alert-success{
	background-color: #95b75d;
	border: none;
	color: #FFFFFF;
}

.alert-danger{
	background-color: #DF040A;
	background-color: rgba(217,83,79,0.80);
	border: none;
	color: #FFFFFF;
}

.growl-animated.alert{
	width:301px;
	padding: 10px;
}

.growl-animated.alert{
	z-index: 10003 !important;
}

.growl-animated.alert.alert-success{
	background: rgba(149,183,93,0.80);
}

.growl-animated.alert.alert-danger{
	background: rgba(217,83,79,0.80);
}

.growl-animated.alert .close{
	font-size: 14px;
}

.growl-animated .fa,
.growl-animated .growl-title,
.growl-animated .growl-message,
.growl-animated .growl-url{
	display: block;
}

.growl-animated .growl-title,
.growl-animated .growl-message,
.growl-animated .growl-url{
	margin-left: 58px;
}

.growl-animated .fa {
	float: left;
	width: 48px;
	height: 48px;
	font-size: 48px;
	font-size: 48px;
	margin-top: 5px;
}

.growl-animated .growl-title {
	font-weight: 700;
	font-size: 16px;
}

.growl-animated .growl-message,
.growl-animated .growl-url {
	line-height: 18px;
}
/* @end */

/* @group Callouts */

.bs-callout {
    padding: 20px;
    margin: 0 0 0px 0;
    border: 1px solid #eee;
    border-left-width: 5px;
    border-radius: 3px;
    background: #fff;
}

.bs-callout-sm {
	padding: 10px;
}

.bs-callout h4 {
    margin-top: 0;
    margin-bottom: 5px;
}

.bs-callout-sm h4 {
    font-size: 16px;
}
.bs-callout p:last-child {
    margin-bottom: 0;
}
.bs-callout code {
    border-radius: 3px;
}
.bs-callout+.bs-callout {
    margin-top: -5px;
}
.bs-callout-default {
    border-left-color: #777;
}
.bs-callout-default h4 {
    color: #777;
}
.bs-callout-primary {
    border-left-color: #297685;
}
.bs-callout-primary h4 {
    color: #297685;
}
.bs-callout-success {
    border-left-color: #5cb85c;
}
.bs-callout-success h4 {
    color: #5cb85c;
}
.bs-callout-danger {
    border-left-color: #d9534f;
}
.bs-callout-danger h4 {
    color: #d9534f;
}
.bs-callout-warning {
    border-left-color: #f0ad4e;
}
.bs-callout-warning h4 {
    color: #f0ad4e;
}
.bs-callout-info {
    border-left-color: #5bc0de;
}
.bs-callout-info h4 {
    color: #5bc0de;
}

/* @end */

/* @group Forms */

::-webkit-input-placeholder,
.form-control::-webkit-input-placeholder {
	 color: #555555 !important;
	 opacity: 1 !important;
}
:-moz-placeholder,
.form-control:-moz-placeholder { /* older Firefox*/
	color: #555555 !important;
	opacity: 1 !important;
}
::-moz-placeholder,
.form-control::-moz-placeholder { /* Firefox 19+ */ 
	color: #555555 !important;
	opacity: 1 !important;
} 
:-ms-input-placeholder,
.form-control:-ms-input-placeholder { 
	color: #555555 !important;
	opacity: 1 !important;
}

label{
	/*font-weight: normal;*/
	margin-bottom: 5px;
}

.has-error .help-block, 
.has-error .control-label, 
.has-error .radio, 
.has-error .checkbox, 
.has-error .radio-inline, 
.has-error .checkbox-inline{
	color: #df040a;
}

.has-error .help-block.alert-danger{
	font-size: 12px;
    line-height: 12px;
    margin: 5px 0;
    padding: 8px;
    color: #fff;
}

.row.well .form-group{
	margin-bottom: 5px;
}

.addFormCont .row.well .form-group{
	margin-bottom: 0;
}

.form-control{
	height: 30px;
	border-radius: 2px;
	-moz-border-radius: 2px;
}

.input-group-addon{
	border-radius: 2px;
}

.form-control:focus {
    border: 1px solid #95b75d;
    		box-shadow: none;
    -webkit-box-shadow: none;
       -moz-box-shadow: none;
}

.sidebarForm div.input input.form-control:disabled, 
li.sidebar-form div.input input.form-control:disabled {
    /*background: #bbbbbb;*/
}

/* @group Nested Related Forms */

.addFormCont{
	margin-bottom: 20px;
}

.row-inner .addFormCont{
	margin-bottom: 0;
}

.btn-xs.getForm{
	margin-left: 10px;
}

/* @end */

/* @group Checkboxes//Radio */

.radioCheckPadding .form-group {
	padding-top: 20px;
}

.tight-forms .form-group{
	margin-bottom: 5px;
}

.form-group .alert.alert-danger{
	padding: 5px;
	margin-top: 3px;
}

.form-group .alert.alert-danger .close{
	font-size: 15px;
}

.form-group.checkbox,
.form-group.radio,
.form-group.radio .rdio {
	position: relative;
}

.form-group.checkbox input[type="checkbox"],
.form-group.radio .rdio input[type="radio"] {
	opacity: 0;
	margin-left: 0;
	position: inherit;
}

.form-group.checkbox label,
.form-group .rdio label {
	padding-left: 10px;
	cursor: pointer;
	margin-bottom: 7px !important;
	font-weight: bold;
}

.form-group .rdio label{
	font-weight: normal;
}

.form-group.radio > label{
	font-weight: bold;
	margin-right: 10px;	
	padding-left: 0;
}

.form-group.checkbox label:before {
	width: 18px;
	height: 18px;
	position: absolute;
	top: 1px;
	left: 0;
	content: '';
	display: inline-block;
	-moz-border-radius: 2px;
	-webkit-border-radius: 2px;
	border-radius: 2px;
	border: 1px solid #bbb;
	background: #fff;
}

.form-group.checkbox input[type="checkbox"]:disabled + label  {
	color: #999;
}

.form-group.checkbox input[type="checkbox"]:disabled + label:before  {
	background-color: #eee;
}

.form-group.checkbox input[type="checkbox"]:checked + label::after  {
	font-family: 'FontAwesome';
	content: "\F00C";
	position: absolute;
	top: 0;
	left: 3.5px;
	display: inline-block;
	font-size: 11px;
	width: 16px;
	height: 16px;
	color: #fff;
}

.form-group.checkbox input[type="checkbox"]:checked + label:before {
	border-color: #999;
}

.form-group.checkbox input[type="checkbox"]:checked + label::after {
	color: #333;
}

.form-group.checkbox-primary input[type="checkbox"]:checked + label:before {
	border-color: #357EBD;
	background-color: #428BCA;
}

.form-group.checkbox-warning input[type="checkbox"]:checked + label:before {
	border-color: #EEA236;
	background-color: #F0AD4E;
}

.form-group.checkbox-success input[type="checkbox"]:checked + label:before {
	border-color: #1CAF9A;
	background-color: #1CAF9A;
}

.form-group.checkbox-danger input[type="checkbox"]:checked + label:before {
	border-color: #D43F3A;
	background-color: #D9534F;
}

.form-group.checkbox-primary input[type="checkbox"]:checked + label::after,
.form-group.checkbox-warning input[type="checkbox"]:checked + label::after,
.form-group.checkbox-success input[type="checkbox"]:checked + label::after,
.form-group.checkbox-danger input[type="checkbox"]:checked + label::after {
	color: #fff;
}

.form-group.radio .rdio{
	display: inline-block;
	margin-right: 15px;
	padding: 0;
}

.form-group .rdio label:before {
	width: 18px;
	height: 18px;
	position: absolute;
	top: 1px;
	left: 0;
	content: '';
	display: inline-block;
	-moz-border-radius: 50px;
	-webkit-border-radius: 50px;
	border-radius: 50px;
	border: 1px solid #bbb;
	background: #fff;
}

.form-group .rdio input[type="radio"]:disabled + label  {
	color: #999;
}

.form-group .rdio input[type="radio"]:disabled + label:before  {
	background-color: #eee;
}

.form-group .rdio input[type="radio"]:checked + label::after  {
	content: '';
	position: absolute;
	top: 5px;
	left: 4px;
	display: inline-block;
	font-size: 11px;
	width: 10px;
	height: 10px;
	background-color: #444;
	-moz-border-radius: 50px;
	-webkit-border-radius: 50px;
	border-radius: 50px;
}

.form-group .rdio.radio-default input[type="radio"]:checked + label:before {
	border-color: #999;
}

.form-group .rdio.radio-primary input[type="radio"]:checked + label:before {
	border-color: #428BCA;
}

.form-group .rdio.radio-primary input[type="radio"]:checked + label::after {
	background-color: #428BCA;
}

.form-group .rdio.radio-warning input[type="radio"]:checked + label:before {
	border-color: #F0AD4E;
}

.form-group .rdio.radio-warning input[type="radio"]:checked + label::after {
	background-color: #F0AD4E;
}

.form-group .rdio.radio-success input[type="radio"]:checked + label:before {
	border-color: #1CAF9A;
}

.form-group .rdio.radio-success input[type="radio"]:checked + label::after {
	background-color: #1CAF9A;
}

.form-group .rdio.radio-danger input[type="radio"]:checked + label:before {
	border-color: #D9534F;
}

.form-group .rdio.radio-danger input[type="radio"]:checked + label::after {
	background-color: #D9534F;
}


/* @end */

/* @end */

/* @group Panels */

.panel {
    border: none;
    box-shadow: none;
    background: transparent;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    -moz-box-shadow: 0 3px 0 rgba(12,12,12,0.10);
    -webkit-box-shadow: 0 3px 0 rgba(12,12,12,0.10);
    box-shadow: 0 3px 0 rgba(12,12,12,0.10);
}

.panel.no-bkg {
    -moz-box-shadow: none;
    -webkit-box-shadow: none;
    box-shadow: none;
}

.panel.no-bkg > .panel-heading,
.panel.no-bkg > .panel-footer {
    background: transparent;
    border-color: transparent;
}

.panel .panel-heading {
	transition: all 0.3s ease-in-out 0s;
	-webkit-transition: all 0.3s ease-in-out 0s;
	   -moz-transition: all 0.3s ease-in-out 0s;
}

.rounded-list div.round.panel .panel-heading{
	padding-left: 20px;
}

.panel-default .panel-heading,
.panel-default .panel-footer {
    background: #f8f8f8;
    border-color: #e7e7e7;
    font-size: 13px;
    font-weight: 400;
    padding: 10px;		
}

.panel.fixed .panel-heading.main-heading{
    position: fixed;
    background: #fff;
    top: 52px;
    z-index: 1048;
    left: 0;
    right: 0;
}

.left-sidebar .panel.fixed .panel-heading.main-heading{
	left: 240px;
}

.fixed-table-toolbar .btn{
	margin: 0 0 0 5px;
}

.panel.fixed .panel-footer.main-footer{
	display: none;
}

.panel-title {
    color: #32323A;
    font-size: 16px;
    font-weight: 400;
    margin-bottom: 0;
    margin-top: 0;
    font-family: 'Lato', sans-serif;
}

.panel .panel-body{
	background: #fcfcfc;
}

.panel.no-bkg .panel-body {
    background: transparent;
}

.panel-primary .tools a {
    color: #fff;
}

.panel-heading .brand{
	font-size: 22px;
	line-height: 32px;
	letter-spacing: -0.5px;
}

.panel-dark .panel-heading {
   background-color: #193048;
   color: #fff;
}

.panel-dark .panel-title,
.panel-dark .panel-title a {
   color: #fff;
}

.panel-dark .panel-title .dropdown-menu > li > a{
    color: #333;
}

.panel.panel-dark .panel-body{
	background: #ffffff;
}

.panel-light .panel-heading {
   background-color: #f6f6f6;
   border: 1px solid #e7e7e7;
   color: #1d2939;
}

.panel-light .panel-title {
   color: #1d2939;
}

.panel-primary {
    -moz-box-shadow: 0 3px 0 rgba(11,87,91,0.30);
    -webkit-box-shadow: 0 3px 0 rgba(0,118,102,0.30);
    box-shadow: 0 3px 0 rgba(0,118,102,0.30);
}

.panel-primary.dark-bottom {
    -moz-box-shadow: 0 3px 0 #0B575B;
    -webkit-box-shadow: 0 3px 0 #0B575B;
    box-shadow: 0 3px 0 #0B575B;
}

.panel-primary .panel-heading {
    background-color: #307F83;
    color: #ffffff;
}

.panel-primary .panel-heading .panel-title{
    color: #ffffff;
}

.panel-success {
    -moz-box-shadow: 0 3px 0 rgba(0,118,102,0.30);
    -webkit-box-shadow: 0 3px 0 rgba(0,118,102,0.30);
    box-shadow: 0 3px 0 rgba(0,118,102,0.30);
}

.panel-success.dark-bottom {
    -moz-box-shadow: 0 3px 0 #007666;
    -webkit-box-shadow: 0 3px 0 #007666;
    box-shadow: 0 3px 0 #007666;
}

.panel-success .panel-heading {
    background-color: #1CAF9A;
    color: #ffffff;
}

.panel-success .panel-heading .panel-title{
    color: #ffffff;
}

.panel-warning {
    -moz-box-shadow: 0 3px 0 rgba(173,109,17,0.30);
    -webkit-box-shadow: 0 3px 0 rgba(173,109,17,0.30);
    box-shadow: 0 3px 0 rgba(173,109,17,0.30);
}

.panel-warning.dark-bottom {
    -moz-box-shadow: 0 3px 0 #AD6D11;
    /* -webkit-box-shadow: 0 3px 0 #AD6D11; */
    /* box-shadow: 0 3px 0 #AD6D11; */
}

.panel-warning .panel-heading {
    background-color: #F0AD4E;
    color: #ffffff;
}

.panel-warning .panel-heading .panel-title {
    color: #ffffff;
}

.panel-danger {
    -moz-box-shadow: 0 3px 0 rgba(150,21,18,0.30);
    -webkit-box-shadow: 0 3px 0 rgba(150,21,18,0.30);
    box-shadow: 0 3px 0 rgba(150,21,18,0.30);
}

.panel-danger.dark-bottom {
    -moz-box-shadow: 0 3px 0 #961512;
    -webkit-box-shadow: 0 3px 0 #961512;
    box-shadow: 0 3px 0 #961512;
}

.panel-danger .panel-heading {
    background-color: #D9534F;
    color: #ffffff;
}

.panel-danger .panel-heading .panel-title {
    color: #ffffff;
}

.panel-info {
    -moz-box-shadow: 0 3px 0 rgba(25,147,182,0.30);
    -webkit-box-shadow: 0 3px 0 rgba(25,147,182,0.30);
    box-shadow: 0 3px 0 rgba(25,147,182,0.30);
}

.panel-info.dark-bottom {
    -moz-box-shadow: 0 3px 0 #1993B6;
    -webkit-box-shadow: 0 3px 0 #1993B6;
    box-shadow: 0 3px 0 #1993B6;
}

.panel-info .panel-heading {
    background-color: #5BC0DE;
    color: #ffffff;
}

.panel-info .panel-heading .panel-title {
    color: #ffffff;
}

.panel-light .panel-title {
    color: #1d2939;
}

.panel.panel-light .panel-body{
	background: #fcfcfc;
	border-left: 1px solid #e7e7e7;
	border-right: 1px solid #e7e7e7;
}

.panel.panel-light .panel-heading .control{
	color: #1d2939;
	display: inline-block;
	float: right;
	margin-left: 8px;
	line-height: 16px;
}

.panel-btns a {
   margin-left: 8px;
   float: right;
   color: #000 !important;
   display: inline-block;
   font-weight: bold;
   opacity: 0.5;
   font-size: 18px;
   line-height: 14px;
   padding-left: 10px;
   border-left: 1px solid #bbb;
   -moz-transition: all 0.2s ease-out 0s;
	-webkit-transition: all 0.2s ease-out 0s;
	transition: all 0.2s ease-out 0s;
}

.panel-btns a:hover {
   text-decoration: none;
   opacity: 0.5;
   cursor: pointer;
}

.panel-btns a.dynaDelete {
	border-left: none;
}

.panel-btns a.minimize {
	line-height: 18px;
}

.panel-btns a.minimize:active,
.panel-btns a.minimize:focus {
   text-decoration: none;
}

.panel-success .panel-btns a,
.panel-danger .panel-btns a,
.panel-dark .panel-btns a {
   color: #fff;
}

/* @end */

/* @group Progress Bars */

.progress {
   -moz-border-radius: 3px;
   -webkit-border-radius: 3px;
   border-radius: 3px;
   height: 15px;
   margin-bottom: 15px;
}

.progress-bar {
   -moz-box-shadow: none;
   -webkit-box-shadow: none;
   box-shadow: none;
}

.progress-sm {
    border-radius: 2px;
    height: 10px;
}

.progress-bar-orange {
	background-color: #FF5A05;
}

.progress-bar-hpgreen {
	  background-color: #1ca59e;
}

.progress-bar-hpgreen-dark {
	background-color: #0B575B;
}

.progress-bar-hpgreen-med {
	background-color: #4D9598;
}

.progress-bar-hpgreen-lgt {
	background-color: #81BDC0;
}

/* @end */

/* @group Well */

.row.well{
	margin-left: 0;
	margin-right: 0;
}

.row.well.well-sm{
	padding: 9px 0;
}

/* @end */

/* @group Pagination */

.fixed-table-pagination {
	font-size: 12px;
}

.pagination li a {
    color: #32323a;
}

.pagination > li > a, .pagination > li > span {
    background-color: #EFF2F7;
    border: 1px solid #EFF2F7;
    float: left;
    line-height: 1.42857;
    /*margin-left: 1px;*/
    padding: 6px 12px;
    position: relative;
    text-decoration: none;
}

.pagination > li > a:hover, .pagination > li > span:hover, .pagination > li > a:focus, .pagination > li > span:focus, .pagination > li.active > a, .pagination > li.active > a:hover {
    background-color: #297685;
    border-color: #297685;
    color: #fff;
}

.index-table .fixed-table-pagination .pagination a{
	padding: 4px 10px;
	font-size: 11px;
}

.fixed-table-pagination .page-list .btn{
	padding: 4px 10px;
}

/* @end */

/* @group Nav Tabs  */

.nav.nav-tabs.tab-alt > li.has-error > a{
	color: #DF040A !important;
}

/* @end */

/* @group Nav Tabs Alt */

.view .tab-content, 
.dashboard .tab-content {
	min-height: 400px;	
}

.nav.nav-tabs.tab-alt{
	border-bottom: 3px solid #ddd;
	margin-bottom: 20px;
}

.nav.nav-tabs.tab-alt > li > a{
	text-transform: uppercase;
	font-weight: 500;
	color: #777;
	border: none;
}

.nav.nav-tabs.tab-alt > li > a:hover{
	background: transparent;
	border: none;
	color: #1D2939;
}

.nav.nav-tabs.tab-alt > li.active > a{
	border: none;
	border-bottom: 3px solid #1CAF9A;
	color: #1D2939;
	background: none;
}

.sidebarForm .nav.nav-tabs.tab-alt > li.active > a{
	color: #f1f1f1;
}

.nav.nav-tabs.tab-alt.tab-alt-inverse > li.active > a{
	background: #1CAF9A;
	color: #ffffff;
}

.nav.nav-tabs.tab-alt > li{
	margin-bottom: -3px;
}

/* @end */

/* @group Nav Tab Dark */

.nav.nav-tabs.nav-dark {
   background: #193048;
}

.nav.nav-tabs.nav-dark > li > a {
   color: #999;
}

.nav.nav-tabs.nav-dark > li > a:hover {
   color: #fff;
}

.nav.nav-tabs.nav-dark > li.active > a{
	border: none;
	border-bottom: 3px solid #1CAF9A;
	background: #1CAF9A;
	color: #ffffff;
	-moz-border-radius: 0;
	-webkit-border-radius: 0;
	border-radius: 0;
}

/* @end */

/* @group Wizard */

.nav.block-wizard-steps{
	margin-bottom: 20px;
}

.nav-tabs.block-wizard-steps > li.active > a .label.label-default{
	background: #ffffff;
	color: #1CAF9A;
}

/* @end */

/* @group Collaspible */

.panel-group .panel .panel-heading{
	padding: 0;	
}

.panel-group .panel .panel-heading .panel-title a{
	display: block;
	text-decoration: none;
	padding: 10px;
}

.panel-group .panel-title a:after{
	font-family: FontAwesome;
	content: "\f0d8";
	float: right;
	color: #32323a;
	font-weight: 500;
	font-size: 1.2em;
}

.panel-group .panel-title a.collapsed:after{
	content: "\f0d7";
}

.panel .panel-tools a{
	font-weight: normal;
	font-size: 12px;
	line-height: 17px;
	float: right;
	color: #000;
	opacity: 0.4;
	margin-left: 10px;
}

.panel.panel-dark .panel-tools a{
	color: #ffffff;
	opacity: 1;
}

/* @end */

/* @group Tables */

.sidebarForm .bootstrap-table{
	color: #333333;
}
.table.noBorder{
	border: none;
}
.table.noBorder td{
	border: none;
}

.highcharts-tooltip .table tr td {
	padding: 2px 5px;
}

.bootstrap-table th.bs-checkbox {
	padding: 0 8px !important;
}

.bootstrap-table.index-table .fixed-table-container{
    /*padding-top: 0;
    border: none;
    border-radius: 0;
    -webkit-border-radius: 0;
    -moz-border-radius: 0;*/
}

.tableScroll.panel {
	background: #fcfcfc;
}

.tableScroll .bootstrap-table.index-table .fixed-table-container {
	border: none;
}

.fixed-table-toolbar.panel-heading {
	transition: all 0.3s ease-in-out 0s;
}

.allowFixed .fixed-table-toolbar.panel-heading.fixedHeader {
    position: fixed;
    top: 52px;
    z-index: 9;
    left: 0;
    right: 0;
    /*transition: all 0.3s ease-in-out 0s;*/
}

.allowFixed .fixed-table-container .fixed-table-header.fixedTableHeader {
    position: fixed;
    top: 105px;
    z-index: 9;
    left: 0;
    right: 0;
    /*transition: all 0.3s ease-in-out 0s;*/
}

.bootstrap-table.index-table .fixed-table-toolbar .bars,
.bootstrap-table.index-table .fixed-table-toolbar .search,
.bootstrap-table.index-table .fixed-table-toolbar .columns {
    position: relative;
    margin-top: 0;
    margin-bottom: 0;
}

.bootstrap-table.index-table .panel-body{
	padding: 0;
}

.tableScroll .bootstrap-table.index-table .panel-body{
	padding: 0;
	height: 100%;
}

.bootstrap-table.index-table .fixed-table-pagination{
    padding-left: 15px;
    padding-right: 15px;
}

.tableScroll .bootstrap-table.index-table .fixed-table-pagination{
	border-top: 1px solid #dddddd;
}

.index-table .fixed-table-body tbody td,
.index-table .fixed-table-body thead th .th-inner{
	/*border-left: none;*/
}

.tableScroll .fixed-table-container thead th,
.table tr.darkHeader th,
.table tr.totalRow td{
    border-left: 1px solid #dddddd;
    background: #f1f1f1;
}

.tableScroll .fixed-table-container thead tr th:first-child{
    border-left: none;
}

.fixed-table-container.hasFilterControl {
	padding-bottom: 74px !important;
}

.hasFilterControl .fixed-table-header {
	height: 74px;
}

/* @end */

/* @group Calendar */

/*calender*/

.fc-toolbar h2{
	font-weight: 500 !important;
}

.fc-header-title {
    display:inline-block;
    margin-top:-45px;
    vertical-align:top;
}
.fc-header-center {
    text-align:left;
}
.fc-header-left {
    text-align:left;
    width:18%;
}
.fc-view {
    /*margin-top:-50px;*/
    overflow:hidden;
    width:100%;
}

.fc button{
	padding: 3px 10px !important;
}

.fc-state-default,
.fc-state-default .fc-button-inner {
    background:#F3F3F3 !important;
    border-color:#DDDDDD;
    border-style:none solid;
    color:#646464;
    text-shadow: none !important;
}

.fc-state-default.fc-corner-left,
.fc-state-default.fc-corner-right{
	border-radius: 0 !important;
}

.fc-state-active,.fc-state-active .fc-button-inner,.fc-state-active,.fc-button-today .fc-button-inner,.fc-state-hover,.fc-state-hover .fc-button-inner {
    background:#32323A !important;
    color:#fff !important;
}
.fc-event-skin {
    background-color:#1FB5AD !important;
    border-color:#1FB5AD !important;
    color:#FFFFFF !important;
}
.fc-grid th {
    height:50px;
    line-height:50px;
    text-align:center;
    background:#e4e4e4 !important;
}
.fc-header-title h2 {
    font-size:18px !important;
    color:#474752;
    font-weight:300;
    padding:0 10px;
}
.external-event {
    cursor:move;
    display:inline-block !important;
    margin-bottom:6px !important;
    margin-right:6px !important;
    padding:8px;
}
#external-events p input[type="checkbox"] {
    margin:0;
}
.drg-event-title {
    font-weight:300;
    margin-top:0;
    margin-bottom:15px;
    border-bottom:1px solid #ddd;
    padding-bottom:10px;
}
.fc-content .fc-event {
    border-radius:4px;
    webkit-border-radius:4px;
    padding: 4px 6px;
}
.fc-corner-left {
    /*border-radius:4px 0 0 4px;*/
    /*-webkit-border-radius:4px 0 0 4px;*/
}
.fc-corner-right {
    /*border-radius:0 4px 4px 0;*/
    /*-webkit-border-radius:0 4px 4px 0;*/
}
.drp-rmv {
    padding-top:10px;
    margin-top: 10px;
}


/* @end */

/* @group Date Range Picker */

.date-range-picker {
	background: #fff; 
	cursor: pointer; 
	padding: 5px 10px; 
	border: 1px solid #ccc;
	/*margin-bottom: 3px;*/
	border-radius: 3px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
}

.daterangepicker.dropdown-menu .ranges li {
	color: #193048;
	font-size: 11px;
	padding: 1px 12px;
	margin-bottom: 5px;
	border-radius: 2px;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
}

.daterangepicker.dropdown-menu .ranges li.active, 
.daterangepicker.dropdown-menu .ranges li:hover {
	background: #193048;
	border-color: #193048;
}

.panel-warning>.panel-heading{background-image:none}
.mb10 {
    margin-bottom: 10px;
}

.page-header{padding-right:120px}
/* @end */

.google-visualization-tooltip-item,.tooltip-wrap {
  white-space: nowrap;
}
.tooltip-wrap{padding:10px;font-size:11px}


 .modal-spinner{position:absolute;top:0;bottom:0;left:0;right:0;background:#fff;z-index: 10001;display:none}
  .modal-spinner .preloader{position:absolute;width:100px;height:100px;margin-left:-50px;margin-top:-50px;left: 50%;
    top: 50%;}
   .modal-spinner .preloader .fa-spin{font-size:80px}
 
 .select2-container.input-large  {min-width:150px}
</style>