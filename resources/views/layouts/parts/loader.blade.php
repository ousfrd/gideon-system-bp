<div id="wait-loader" style="display:none;position: fixed;top:0;left:0;bottom:0;right:0; background:#fff">

<p class="text-center" style="position: absolute; left:50%;top:50%;width: 400px; margin-left: -200px;height:100px; margin-top:-200px;overflow-y:scroll">
<img src="{{ URL::asset('images/ajax_loader.gif') }}" ></p>

<div id="wait-loader-info" style="position: absolute; left:50%;top:50%;width: 400px; margin-left: -200px;margin-top:-110px;height:300px;overflow-y:scroll" >

<p>Please dont close this window, It may take a while to finish...</p>
</div>
</div>