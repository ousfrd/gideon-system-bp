<div id="sidebar-wrapper">
	
	
    <ul class="sidebar-nav nav-pills nav-stacked" id="menu" style="">
      	
		 
		 
		  
   
  	@can('manage-orders')
			@if(config('tenant.gateway.orders_page'))
			<li class="menu-level-1">
				<a href="{{route('orders')}}"><span class="fa-stack fa-lg pull-left"><i class="fa fa-dollar  fa-stack-1x "></i></span> Orders</a>
			</li> 
			@endif
		@endcan
		
		
		
		
	@can('manage-products')
		@if(config('tenant.gateway.products_page'))
		<li class="menu-level-1">
		  <a href="{{route('products')}}?Products[filters][status-eq]=1"><span class="fa-stack fa-lg pull-left"><i class="fa fa-product-hunt  fa-stack-1x "></i></span> Products</a>     
				<ul>
					<li><a href="{{route('products')}}?Products[filters][status-eq]=1&Products[filters][qty-eq]=1"> Active Products</a></li>
					<li><a href="{{route('products.low')}}?Products[filters][status-eq]=1"> Low Stocking Products</a></li>
					<li><a href="{{route('products.lowsales')}}?Products[filters][status-eq]=1&Products[filters][qty-eq]=1"> Low Sales Products</a></li>
					<li><a href="{{route('products.newreleases')}}">New Releases</a></li>					  
					<li><a href="{{route('products')}}"> All Products</a></li>
					<li><a href="{{route('brands')}}"> All Brands</a></li>
					<li><a href="{{route('product-category.index')}}">Product Category</a></li>
				</ul>       
		</li>   
		@endif

		@if(config('tenant.gateway.products_page'))
		<li class="menu-level-1">
		  <a href="{{route('reviews')}}"><span class="fa-stack fa-lg pull-left"><i class="fa fa-comments  fa-stack-1x "></i></span> Reviews </a>     
		</li>   
		@endif

		@if(config('tenant.gateway.listings_page'))
		<li class="menu-level-1">
			<a href=""><span class="fa-stack fa-lg pull-left"><i class="fa fa-list  fa-stack-1x "></i></span> Listings</a>
			<ul>
				<li><a href="{{route('listings')}}?Listing[filters][status-eq]=1"> Active Listings</a></li>
				<!-- <li><a href="{{route('listings.low')}}?Listing[filters][status-eq]=1"> Low Stocking Listings</a></li> -->
				<!-- <li><a href="{{route('listings.lowsales')}}?Listing[filters][status-eq]=1&Listing[filters][qty-eq]=1"> Low Sales Listings</a></li> -->
				<!-- <li><a href="{{route('listings.excess')}}"> Excess Inventory!!!</a></li> -->
				
				<li><a href="{{route('listings')}}"> All Listings</a></li>
			</ul>
		</li>
		@endif     
	@endcan

	@can('view-inventory-management')
		<li class="menu-level-1">
			<a href=""><span class="fa-stack fa-lg pull-left"><i class="fa fa-archive fa-stack-1x "></i></span> Inventory </a>
			<ul>
				<li><a href="{{route('inventory-management.summary')}}"> FBA Inventory</a></li>
			</ul>
		</li>
	@endcan
	

	@can('manage-inbound-shipments')
<!-- 			       <li class="menu-level-1">
		      	  <a href="{{route('inbound_shipments')}}"><span class="fa-stack fa-lg pull-left"><i class="fa fa-cubes  fa-stack-1x "></i></span> Inbound Shipments</a>
		          
		       </li>  -->
	@endcan

	@if(config('tenant.gateway.customers_page'))
		@can('view-customers')
		<li class="menu-level-1">
			<a href=""><span class="fa-stack fa-lg pull-left"><i class="fa fa-users  fa-stack-1x "></i></span> Customers</a>
			<ul>
				<li><a href="{{route('customers.dashboard')}}">Dashboard</a></li>
				<li><a href="{{route('customers.export-form')}}">Export</a></li>
			</ul>
		</li>
		@endcan
	@endif
	<!-- @can('manage-emails')
		<li class="menu-level-1">
      	  <a href=""><span class="fa-stack fa-lg pull-left"><i class="fa fa-envelope  fa-stack-1x "></i></span> Emails</a>
          <ul>

          			<li><a href="{{route('email.lists')}}"> Email Lists</a></li>
		          	<li><a href="{{route('email.templates')}}"> Email Templates</a></li>
		          	<li><a href="{{route('email.profiles')}}"> Reviewer Profiles</a></li>
		          	
		  </ul>
       </li>  

	@endcan -->
	
	@can('manage-reports')
		@if(config('tenant.gateway.reports_page'))
		<li class="menu-level-1">
			<a href=""><span class="fa-stack fa-lg pull-left"><i class="fa fa-table  fa-stack-1x "></i></span> Reports</a>
			<ul>
				@can('view-performance-report')
				<li><a href="{{route('report.product-performance')}}">Product Performance Report</a></li>
				@endcan
				<li><a href="{{route('report.productDailyProfit')}}"> Product Daily Profit Report</a></li>
				@can('view-pm-monthly-report')
				<li><a href="{{route('report.monthlyProfit')}}"> PM Monthly Profit Report</a></li>
				@endcan
				@can('view-disbursement-report')
				<li><a href="{{route('report.disburse')}}"> Disburse Report</a></li>
				@endcan
						<!-- <li><a href="{{route('business.list')}}"> Business Report</a></li> -->
				@can('generate-report')
				<li><a href="{{route('reports.new')}}"> Generate Report</a></li>
				<li><a href="{{route('reports.lists')}}"> Download Report</a></li>
				@endcan
			</ul>
    </li>
		@endif
	@endcan


		<!-- @can('manage-ads')
		<li class="menu-level-1">
      	  <a href=""><span class="fa-stack fa-lg pull-left"><i class="fa fa-bullhorn fa-stack-1x "></i></span> Ads</a>
          <ul>

          			<li><a href="{{route('ad.manage')}}"> Ads Manage</a></li>								
          			<li><a href="{{route('ad.analyze')}}"> Ads Analyze</a></li>
								<li><a href="{{route('ad.rules')}}"> Ads Rules</a></li>

		          	
		          </ul>
       </li>
       @endcan -->


		<!-- @can('manage-reviews')
  	
			<li class="menu-level-1">
				<a href="{{route('reviews')}}"><span class="fa-stack fa-lg pull-left"><i class="fa fa-comments  fa-stack-1x "></i></span> Reviews</a>		
			</li>  
		@endcan -->

		@can('view-marketing')
			@if(config('tenant.gateway.marketing_page'))
			<li class="menu-level-1">
				<a href=""><span class="fa-stack fa-lg pull-left"><i class="fa fa-line-chart fa-stack-1x"></i></span> Marketing</a>
				<ul>
					@can('manage-outsource-orders')	
					<li class="menu-level-2">				
					<a href="{{route('csorders')}}"> Search Orders</a>
					</li>
					@endcan	
					@can('access-managed-review')
						@if(config('tenant.gateway.mrs_page'))
						<li class="menu-level-2">
							<a href="">Self Review</a>
							<ul>
								<li><a href="{{route('leave-review-task.index')}}">Tasks</a></li>
								<li><a href="{{route('owned-buyer.index')}}">Owned Buyers</a></li>
								<li><a href="{{route('owned_buyer.find-eligible-buyers')}}">Find Eligible Buyers</a></li>
								<li><a href="{{route('managed-review-order.index')}}">Orders</a></li>
								<li><a href="{{route('owned-buyer-cart-item.index')}}">Shopping Cart Items</a></li>
								<li><a href="{{route('divvy-transaction-record.index')}}">Divvy Transaction</a></li>
							</ul>
						</li>
						@endif
					@endcan

					@can('invite-review')
						<li class="menu-level-2"><a href="">Invite Review</a>
							<ul>
								<li><a href="{{route('invite-review-letter-template.index')}}">Letter Templates</a></li>
							</ul>
						</li>
					@endcan
					
					@can('manage-claim-review')
						<li class="menu-level-2"><a href="">Claim Review</a>
							<ul>
								<li><a href="{{route('claim-review-campaign.products-overview')}}">Products Campaign</a></li>
								<li><a href="{{route('claim-review-campaign.filter-shipments')}}">Filter Orders</a></li>
								<li><a href="{{route('claim-review-campaign.index')}}">Campaigns</a></li>
								<li><a href="{{route('clicksend-history.index')}}">Click Send History</a></li>
								<li><a href="{{route('gift-card-template.index')}}">Gift Card Template</a></li>
							</ul>
						</li>
					@endcan
					
					
					@can('manage-redeems')
						@if(config('tenant.gateway.redeems_page'))
						<li class="menu-level-2">
							
							<a href="">Redeems</a>
								<ul>
									<li><a href="{{route('redeems.freeProduct')}}"> Free Product</a></li>
									<li><a href="{{route('redeems.giftcard')}}"> Gift Card</a></li>
									@can('redeems.other')
									<li><a href="{{route('redeems.other')}}"> Negtive Review</a></li>	
									@endcan	
									@can('redeems.list-invite-reviews')
									<li><a href="{{route('redeems.list-invite-reviews')}}">Invite Review</a></li>
									@endcan
									@can('manage-redeems.list-follow-up')
									<li><a href="{{route('redeems.followup')}}"> Follow Up</a></li>
									@endcan
									@can('redeems.invite-reviews-followup')
									<li><a href="{{route('redeems.invite-reviews-followup')}}">Invite FollowUp</a></li>
									@endcan
									@can('manage-redeems.list-warehouse-requests')
									<li><a href="{{route('redeems.warehouseRequest')}}"> Warehouse Requests</a></li>
									@endcan
									
									@can('manage-redeems.delete-test-data')
									<li><a target="_blank" href="{{route('redeems.deleteTestData')}}">Delete Test Data</a></li>
									@endcan
									<!-- <li><a href="{{route('redeems.outofstockRequest')}}"> Out of Stock</a></li>		          			 -->
									
									<!-- <li><a href="{{route('redeems.refund')}}"> Full Refund</a></li> -->
									<!-- <li><a href="{{route('redeems.replacement')}}"> Free Replacement</a></li> -->
								</ul>
						</li>
						@endif
					@endcan
					
				</ul>
			</li>
			@endif
		@endcan

		


		@can('manage-imports')
			@if(config('tenant.gateway.imports_page'))
			<li class="menu-level-1">
				<a href=""><span class="fa-stack fa-lg pull-left"><i class="fa fa-cloud-upload  fa-stack-1x "></i></span> Imports</a>
				<ul>
					<li><a href="{{route('imports')}}"> All Imports</a></li>
					<li><a href="{{route('import.stats')}}">Import Stats</a></li>						
					<li><a href="{{route('import.orders')}}"> Import Orders</a></li>
					<li><a href="{{route('import.finances')}}"> Import Finances</a></li>
					<li><a href="{{route('import.ads')}}"> Import Product Ads</a></li> 
					<li><a href="{{route('import.campaigns')}}"> Import Campaigns</a></li>
					<li><a href="{{route('import.listings')}}"> Import Listings</a></li>
					<li><a href="{{route('import.inventory')}}"> Import FBA Inventory</a></li>
					<li><a href="{{route('import.business')}}"> Import Business</a></li>	
				</ul>
			</li>  
			@endif
		@endcan


		@can('manage-users')
			@if(config('tenant.gateway.users_page'))
			<li class="menu-level-1">
				<a href="{{route('users')}}"><span class="fa-stack fa-lg pull-left"><i class="fa fa-users  fa-stack-1x "></i></span> Users</a>
			</li>
			@endif
		@endcan
		

		 

		@can('manage-accounts')
			@if(config('tenant.gateway.seller_acounts'))
			<li class="menu-level-1">
				<a href="{{route('accounts')}}"><span class="fa-stack fa-lg pull-left"><i class="fa fa-book  fa-stack-1x "></i></span> Seller Accounts</a>
			</li>  
			@endif
		@endcan
  </ul>    
</div>