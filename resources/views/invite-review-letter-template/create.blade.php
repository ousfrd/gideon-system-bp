@extends('layouts.master')
@section('title', "Create Invite Review Letter Templates")
@section('content')
<div class="row">
	 <div class="col-md-6 mx-auto">
    <div class="alert alert-info" role="alert">
      <p> variables available to inject to template: </p>
      <ul>
        <li>customer_name</li>
        <li>bonus</li>
        <li>sender_name</li>
        <li>order_number</li>
        <li>product_name</li>
        <li>products</li>
      </ul>
      <p>Using [[variable]] to inject variable, eg: [[products]]</p>
    </div>
    <form role="form" action="{{route('invite-review-letter-template.store')}}" method="POST">
      {{Form::token()}}
      <div class="form-group">
        <label for="name">Template Name *:</label>
        <input required type="text" name="name" class="form-control" id="name">
      </div>
      <div class="form-group">
        <label for="marketplace">Country *</label>
        <select required class="form-control" name="country" id="country">
          <option value="">--select *--</option>
          @foreach(config('app.countries') as $value=>$text)
            <option value="{{$value}}">{{$text}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="name">Subject *:</label>
        <input required type="text" name="subject" class="form-control" id="subject">
      </div>
      <div class="form-group">
        <label for="platform">Body *:</label>
        <textarea name="body" class="form-control" id="body" cols="50" rows="15"></textarea>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
@endsection