@extends('layouts.master')
@section('title', "Invite Review Letter Templates")
@section('content')
<div class="btn-actions">
  <a href="{{route('invite-review-letter-template.create')}}" class="btn btn-primary pull-right"><span class="" >Create Template</span></a>
</div>
{!! $grid !!}
@include('parts.delete-confirmation')
<!-- Modal -->
<div class="modal fade" id="letter-modal" tabindex="-1" role="dialog" aria-labelledby="letter-modal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="letter-name"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><label>Country: </label> <sapn class="letter-country"></span></p>
        <hr>
        <p><label>Subject: </label> <span class="letter-subject"></span></p>
        <hr>
        <label>Body: </label>
        <div class="letter-body"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
  (function(){
    $('#letter-modal').on('show.bs.modal', function (event) {
      const modal = $(this);
      const button = $(event.relatedTarget);
      const body = button.data('body');
      const name = button.data('name');
      const subject = button.data('subject');
      const country = button.data('country');
      modal.find('.letter-name').text(name);
      modal.find('.letter-country').text(country);
      modal.find('.letter-subject').text(subject);
      modal.find('.letter-body').html(body);
    })
  })()
</script>
@endsection