@extends('layouts.master')
@section('title', "Add New Email Template ")
@section('content')

<div class="btn-actions">
<a class="btn btn-default pull-right" onclick="return confirm('Are you sure to go back without saving?')" href="{{url()->previous()}}"><span class="" >Back</span></a>
</div>

<div class="">
	<div class="row">
	@include('email.parts.shortcuts')
	@include('email.form.new')
	</div>
</div>

@endsection

