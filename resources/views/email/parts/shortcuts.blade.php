<div class="col-md-2">
<h5>Available Shortcuts:</h5>
<p>[[order-id]]</p>
<p>[[order-link]]</p>
<p>[[order-link:Your order]]</p>
<p>[[order-link-with-id]]</p>
<p>[[customer-name]]</p>
<p>[[first-name]]</p>
<p>[[seller-name]]</p>
<p>[[product-name]]</p>
<p>[[product-link]]</p>
<p>[[product-link:Your product]]</p>
<p>[[product-link-with-name]]</p>
<p>[[product-image]]</p>
<p>[[product-image-url]]</p>
<p>[[contact-us-link]]</p>
<p>[[contact-us-link:Contact us]]</p>
<p>[[product-review-link]]</p>
<p>[[product-review-link:Write your review here]]</p>
<p>[[seller-feedback-link]]</p>
<p>[[seller-feedback-link:Feedback]]</p>
</div>