<!-- Modal -->
<div class="modal fade" id="addTemplateModal" tabindex="-1" role="dialog" aria-labelledby="addTemplateModal">
  <form class="form-horizontal" id="addTemplateForm">
  <div class="modal-dialog" role="document">
    <div class="modal-content"  style="background: #fff">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Email Template</h4>
      </div>
      <div class="modal-body">
      	<p class="text-danger msg" style="display:none"></p>

        <div class="form-group row">
                      <label for="Role" class="col-sm-4 control-label">Email Types</label>
                      <div class="col-sm-6">
                        <select name="type" class="form-control role-field" ">
                              <option value="Greeting" selected>Greeting</option>
                              <option value="Delivered">Delivered</option>
                              <option value="Product Review">Product Review</option>
                          </select>
                      </div>
                </div>

       				 <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Subject</label>

                            <div class="col-md-6">
                                <input id="subject" type="text" class="form-control" name="subject" value="{{ old('subject') }}" required autofocus>
                            </div>
                        </div>
					
              <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Message</label>

                            <div class="col-md-6">
                                <textarea id="message" class="form-control" name="message" value="{{ old('message') }}"></textarea>
                            </div>
                        </div>

					
            <div class="form-group row">
              <label for="Role" class="col-md-4 control-label">Assigned Products</label>
              <div class="col-md-6">
                <!-- <select multiple="multiple" name="products[]" class="products-managed form-control"></select> -->
                <input type="hidden" name="asin" class="bigdrop" id="products-managed" style="width:600px" value="" />

              </div>
            </div>

	
<!-- 					<div class="form-group"> -->
<!--                             <div class="col-md-6 col-md-offset-4"> -->
<!--                                 <button type="submit" class="btn btn-primary"> -->
<!--                                     Submit -->
<!--                                 </button> -->
<!--                             </div> -->
<!--                     </div> -->
					
					
      <div class="modal-footer">
      	
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" id="newuserModelBtn" class="btn btn-primary">Submit</button>
      </div>
      
      <div class="modal-spinner"><div class="preloader"><div class="status"><i class="fa fa-spinner fa-spin"></i></div></div></div>
      
    </div>
  </div>
  </form>
		 
</div>

<script>
//   		$( "#category" ).autocomplete( "option", "appendTo", "#expenseForm" );
//   		$( "#category" ).autocomplete( "option", "minLength", 0 );
//   		$( "#category" ).autocomplete( "option", "autoFocus", true );
//   		$( "#category" ).autocomplete( "option", "delay", 0 );

  		$('#addTemplateForm').submit(function(){
  			$('.msg').html('').hide()
			$(this).attr('disabled',true)
			$('.modal-spinner').show()
			$.post('{{route('email.templates.add')}}',$('#addTemplateForm').serialize(),function(response){
				// alert(response);
        if(response == 1) {
					window.location.reload()
				}
			}).fail(function(xhr, status, error) {
		        var ul = $('<ul>')
				$.each($.parseJSON(xhr.responseText), function(k, v) {
					ul.append('<li>'+k+': '+v[0]+'</li>')
				});
				
				$('.msg').append(ul).show()
		        $(this).attr('disabled',false)
				$('.modal-spinner').hide()

				
		    })

			return false
  	  	})
  	  	
        $("#products-managed").select2({
          width: '100%',
          allowClear: true,
          multiple: true,
          minimumInputLength: 3,
          ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
              placeholder: "Select a Product",
              url: "{{route('utils.ajax.products')}}",
              dataType: 'json',
              quietMillis: 250,
              data: function (term, page) {
                  return {
                      q: term, // search term
                      page: page
                  };
              },
              results: function(data, page) {
                    var more = (page * 15) < data.total_count;

                    return {results: data.results, more: more};
              },
              cache: true
          },



          dropdownCssClass: "bigdrop",

          formatResult: function(element){
              return "<div>" + element.text + "</div>";
          },
          formatSelection: function(element){
              return element.text;
          },
          escapeMarkup: function(m) {
              return m;
          }

      })

  </script>
 