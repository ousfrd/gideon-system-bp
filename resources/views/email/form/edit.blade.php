
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>

    
        <div class="col-md-8">

                
                    <div>
          
          {!! Form::model($email_template, ['route' => ['email.template.edit', $email_template->id], 'class' => 'form-horizontal']) !!}
          

          <div class="form-group row">
            {{ Form::label("Type", null, ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">
            {{ Form::select('type', ['Greeting' => 'Greeting', 'Delivered' => 'Delivered', 'Product Review' => 'Product Review'], $email_template->type,['class' => 'form-control','required']) }}
            </div>
        </div>




<div class="form-group row">
    {{ Form::label("Subject", null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
    {{ Form::text('subject', $email_template->subject, array_merge(['class' => 'form-control','required','placeholder'=>'Subject'])) }}
    </div>
</div>
                    
              <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Message</label>

                            <div class="col-sm-10">
                                <textarea id="message" class="form-control summernote" name="message" value="{{ $email_template->message }}" style="height: 300px;">{{ $email_template->message }}</textarea>
                            </div>
                        </div>
                    
                              <div class="form-group row">
            {{ Form::label("Is General Template", null, ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">
              {{ Form::select('general', ['1' => 'Yes', '0' => 'No'], $email_template->general,['class' => 'form-control', 'id' => 'generalCheckbox', 'required']) }}
            </div>
        </div>

                    
            <div class="form-group row" id="assignedProducts">
              <label for="Role" class="col-sm-2 control-label">Assigned Products</label>
              <div class="col-sm-10">
                <!-- <select multiple="multiple" name="products[]" class="products-managed form-control"></select> -->
                <input type="hidden" name="products" id="products-managed" style="width:600px" value="{{count($email_template->products) > 0 ? $email_template->products[0]->id : ''}}" />

              </div>
            </div>
 
             <div class="form-group row">
              <label for="Role" class="col-sm-2 control-label">Active</label>
              <div class="col-sm-10">
                <!-- <select multiple="multiple" name="products[]" class="products-managed form-control"></select> -->
                {{ Form::select('active', ['1' => 'Yes', '0' => 'No'], $email_template->active,['class' => 'form-control', 'id' => 'activeCheckbox', 'required']) }}
              </div>
            </div>
          

          
          <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>

                                <button type="button" class="btn btn-warning send-test">
                                    Send Test Email
                                </button>
                            </div>
                    </div>
          
          {!! Form::close() !!}

            </div>
        </div>

<script>

    $(document).ready(function() {

     $('.summernote').summernote({

           height: 300,

      });

     @if($email_template->general)
      $('#assignedProducts').hide();
     @else
      $('#assignedProducts').show();
     @endif

     $('#generalCheckbox').on('change', function() {

      if($(this).val() == 0) {
        $('#assignedProducts').fadeIn();
      } else {
        $('#assignedProducts').fadeOut();
        // $("#products-managed").val(null).trigger('change');
      }

     });

     $('.send-test').click(function(e) {
        e.preventDefault();

        $.ajax({
          url: "{{route('utils.ajax.send-test-email')}}",
          data: {"template_id": "{{$email_template->id}}"},
          success: function() {
            alert('Test email was send to you, please check the email!');
          }
        })

     })


     $("#products-managed").select2({
        width: '100%',
        allowClear: true,
        multiple: true,
        minimumInputLength: 3,
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            placeholder: "Select a Product",
            url: "{{route('utils.ajax.products')}}",
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page: page
                };
            },
            results: function(data, page) {
                  var more = (page * 15) < data.total_count;

                  return {results: data.results, more: more};
            },
            cache: true
        },

        initSelection : function (element, callback) {
            var data = [];
            @foreach($email_template->products as $p)
              data.push({id: '{{$p->id}}', text: '{{$p->asin}} - {{$p->country}} - {{$p->name}}', short_text: '{{$p->asin}} - {{$p->country}}'});
            @endforeach

            callback(data);
        },

        formatResult: function(element){
            return "<div>" + element.text + "</div>";
        },
        formatSelection: function(element){
            return element.short_text;
        },
        escapeMarkup: function(m) {
            return m;
        }

    })

   });

  </script>