
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>

        <div class="col-md-8">

                
					<div>
					
					{!! Form::model($email_template, ['route' => ['email.template.add', $email_template->id], 'class' => 'form-horizontal']) !!}
					
					         <div class="form-group row">
                      <label for="Role" class="col-sm-2 control-label">Email Type</label>
                      <div class="col-sm-10">
                        <select name="type" class="form-control role-field" ">
                              <option value="Greeting" selected>Greeting</option>
                              <option value="Delivered">Delivered</option>
                              <option value="Product Review">Product Review</option>
                          </select>
                      </div>
                </div>


                     <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Subject</label>

                            <div class="col-sm-10">
                                <input id="subject" type="text" class="form-control" name="subject" value="{{ old('subject') }}" required autofocus>
                            </div>
                        </div>
                    
              <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Message</label>

                            <div class="col-sm-10">
                                <textarea id="message" class="form-control summernote" name="message" value="{{ old('message') }}" style="height: 300px;"></textarea>
                            </div>
                        </div>
                    
                                                  <div class="form-group row">
            {{ Form::label("Is General Template", null, ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">
              {{ Form::select('general', ['0' => 'No', '1' => 'Yes'], 0,['class' => 'form-control', 'id' => 'generalCheckbox', 'required']) }}
            </div>
        </div>

                    
            <div class="form-group row" id="assignedProducts">
              <label for="Role" class="col-sm-2 control-label">Assigned Products</label>
              <div class="col-sm-10">
                <!-- <select multiple="multiple" name="products[]" class="products-managed form-control"></select> -->
                <input type="hidden" name="products" id="products-managed" style="width:600px" value="" />

              </div>
            </div>
					
					             <div class="form-group row">
              <label for="Role" class="col-sm-2 control-label">Active</label>
              <div class="col-sm-10">
                <!-- <select multiple="multiple" name="products[]" class="products-managed form-control"></select> -->
                {{ Form::select('active', ['1' => 'Yes', '0' => 'No'], $email_template->active,['class' => 'form-control', 'id' => 'activeCheckbox', 'required']) }}
              </div>
            </div>


					<div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                    </div>
					
					{!! Form::close() !!}
					</div>
					
        </div>


<script>

  $(document).ready(function() {

     $('.summernote').summernote({

           height: 300,

      });



     $('#generalCheckbox').on('change', function() {

      if($(this).val() == 0) {
        $('#assignedProducts').fadeIn();
      } else {
        $('#assignedProducts').fadeOut();
      }

     });



        $("#products-managed").select2({
          width: '100%',
          allowClear: true,
          multiple: true,
          minimumInputLength: 3,
          ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
              placeholder: "Select a Product",
              url: "{{route('utils.ajax.products')}}",
              dataType: 'json',
              quietMillis: 250,
              data: function (term, page) {
                  return {
                      q: term, // search term
                      page: page
                  };
              },
              results: function(data, page) {
                    var more = (page * 15) < data.total_count;

                    return {results: data.results, more: more};
              },
              cache: true
          },



          formatResult: function(element){
              return "<div>" + element.text + "</div>";
          },
          formatSelection: function(element){
              return element.short_text;
          },
          escapeMarkup: function(m) {
              return m;
          }

      })

  });
  
  </script>