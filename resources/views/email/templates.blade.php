@extends('layouts.master')
@section('title', $title)
@section('content')
<div class="btn-actions">
<a class="btn btn-default pull-right" href="{{ route('email.template.add') }}"><span class="">Create New Email Template</span></a>
</div>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$("#Emails tr").each(function(){
		$(this).addClass($(this).find(".column-status").text())
	})
})
//-->
</script>

<div class="table-responsive">
{!! $grid !!}
</div>
@endsection