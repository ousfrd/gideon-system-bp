@extends('layouts.master')
@section('title', $title)
@section('content')

    <div class="table-responsive">
        {!! $grid->render() !!}
    </div>

@endsection