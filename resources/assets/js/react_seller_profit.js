import React from 'react';
import ReactDOM from 'react-dom';

// import FilterBar from './components/FilterBar'
import SellerFilterBar from './components/SellerFilterBar'
import SellerProfitChart from './components/SellerProfitChart'
// import SellerList from './components/SellerList'

class SellerProfitBox extends React.Component {
  constructor(props) {
    super(props);

    var d = new Date();
    var month = d.getMonth();
    var year = d.getFullYear();
    
    this.state = {
      year: year,
      month: month+1,
      type: 'profit',
    };

    this.handleChangeType = this.handleChangeType.bind(this);
    this.handleChangeYear = this.handleChangeYear.bind(this);
    this.handleChangeMonth = this.handleChangeMonth.bind(this);
  }

  handleChangeType(type) {
    this.setState({
      type: type,
    });
  }

  handleChangeYear(year) {
    this.setState({
      year: year,
    });
  }

  handleChangeMonth(month) {
    this.setState({
      month: month,
    });
  }


  render() {
    return (
      <div>
        <SellerFilterBar
          onChangeType={this.handleChangeType}
          onChangeYear={this.handleChangeYear}
          onChangeMonth={this.handleChangeMonth}
        />
        <SellerProfitChart 
          filterState={this.state}
        />
      </div>
    );
  }
}




ReactDOM.render(
  <SellerProfitBox />,
  document.getElementById('container')
);