import React from 'react';
import ReactDOM from 'react-dom';

import FilterBar from './components/FilterBar'
import ProfitChart from './components/ProfitChart'
import AgProductList from './components/AgProductList'

class ProfitBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      account: '',
      marketplace: '',
      startDate: moment().subtract(14, 'days'),
      endDate: moment(),
      asin_params: ''
    };

    this.handleFilterSubmit = this.handleFilterSubmit.bind(this);
    this.handleAsinFilter = this.handleAsinFilter.bind(this);
  }

  handleFilterSubmit(state) {
    this.setState({
      account: state.account,
      marketplace: state.marketplace,
      startDate: state.startDate,
      endDate: state.endDate
    });
  }

  handleAsinFilter(asin_params) {
    this.setState({
      asin_params: asin_params
    })
  }

  render() {
    return (
      <div>
        <FilterBar
          accounts={accounts}
          beginning={beginning}
          marketplaces={marketplaces}
          startDate={this.state.startDate}
          endDate={this.state.endDate}
          onSubmit={this.handleFilterSubmit}
        />
        <ProfitChart 
          filterState={this.state}
        />
        <AgProductList
          filterState={this.state}
          onAsinFilter={this.handleAsinFilter}
        />
      </div>
    );
  }
}




ReactDOM.render(
  <ProfitBox />,
  document.getElementById('container')
);