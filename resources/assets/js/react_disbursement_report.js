import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import "regenerator-runtime/runtime";
import formateParamsDate from './components/formateParamsDate'
import SettlementDate from './components/SettlementDate'
import AccountList from './components/AccountList'
import DepositDateList from './components/DepositDateList'
import AgSettlementList from './components/AgSettlementList'
import SettlementChart from './components/SettlementChart'
import SettlementAccountChart from './components/SettlementAccountChart'




class DisbursementBox extends React.Component {
  constructor(props) {

    super(props);
    
    this.state = {
      accountList: [],
      arrayValue: [],
      depositList: [],
      depositarrayValue: [],
      startDate: moment().subtract(13, 'days'),
      endDate: moment(),
      SettlementDateShow:true,
      DepositDateListShow:true,
      AccountListShow:true,
      AgSettlementListShow:true,
      DataLoading : false,
      TotalAmountShow:true,
      SettlementChartShow:false,
      SettlementAccountChartShow:false,
      TotalAmount : "0",
      ChartData:[],
      AccountChartData:[]
      
,    };


    this.handleAccountStateOnly = this.handleAccountStateOnly.bind(this);
    this.handleDepositStateOnly = this.handleDepositStateOnly.bind(this);
    this.SettlementDateSelectionChange = this.SettlementDateSelectionChange.bind(this);
    this.AccountListSelectionChange = this.AccountListSelectionChange.bind(this);
    this.DepositDateListSelectionChange = this.DepositDateListSelectionChange.bind(this);
    this.updateAccountBox = this.updateAccountBox.bind(this);
    this.updateDepositBox = this.updateDepositBox.bind(this);
    this.updateGridList = this.updateGridList.bind(this);
    this.checkAccountBoxAllSeleted = this.checkAccountBoxAllSeleted.bind(this);
    this.checkDepositBoxAllSeleted = this.checkDepositBoxAllSeleted.bind(this);
    this.updateAccountState = this.updateAccountState.bind(this);
    this.updateDepositState = this.updateDepositState.bind(this);
    this.updateDateState = this.updateDateState.bind(this);
    this.updateChart = this.updateChart.bind(this);
    this.updateAccountChart = this.updateAccountChart.bind(this);

    
  }

  //handle only account state
  handleAccountStateOnly(){

    this.setState({      
      arrayValue: [],
      accountAll: true

   })  

  }

 //handle only deposit state
  handleDepositStateOnly(){

    this.setState({      
      depositarrayValue: [],
      depositAll: true

    })      
  }

  async componentDidMount(){

    this.setState({DataLoading : true});
    this.setState({AccountListShow : false});
    this.setState({DepositDateListShow : false});
    this.setState({AgSettlementListShow : false});
    this.setState({TotalAmountShow : false});  
    this.setState({SettlementChartShow : false});  
    this.setState({SettlementAccountChartShow : false});  
    
    

    //get accountList
    axios.post(fetchdisbursementbyaccount_url, formateParamsDate(this.state))
    .then(res => {
      
      this.setState({
          accountList: res.data,
          arrayValue: res.data,
      })                  

      }).catch(error => {
        console.log(error.response)
      }); 

      
    //get depositDate List
    axios.post(fetchdisbursementbydepositdate_url, formateParamsDate(this.state))
    .then(res => {
      
      this.setState({
          depositList: res.data,
          depositarrayValue: res.data,
      })                  

      }).catch(error => {
        console.log('error', error);
      });       


    await this.updateGridList();
    this.setState({AgSettlementListShow : true});
    this.setState({TotalAmountShow : true}); 
    await this.updateChart();
    this.setState({SettlementChartShow : true});
    await this.updateAccountChart();
    this.setState({SettlementAccountChartShow : true});  

    
    this.setState({AccountListShow : true});
    this.setState({DepositDateListShow : true});        
    this.setState({DataLoading : false});



  } 


   checkAccountBoxAllSeleted(){

    return new Promise((resolve, reject) => {
      
      if(this.state.arrayValue.length == this.state.accountList.length || this.state.arrayValue.length == 0){

        resolve(true);

      }else{

        resolve(false);

      }

    })
  
  }

  
  checkDepositBoxAllSeleted(){

    return new Promise((resolve, reject) => {
      
      if(this.state.depositarrayValue.length == this.state.depositList.length || this.state.depositarrayValue.length == 0){

        resolve(true);

      }else{

        resolve(false);

      }

    })

  }

   updateDepositState(val){

    return new Promise((resolve, reject) => {

      this.setState({
        depositarrayValue : val
      },()=>{
        resolve(true);
      })      
      
    })

  }

   updateAccountState(val){

    return new Promise((resolve, reject) => {

      this.setState({
        arrayValue : val
      },()=>{
        resolve(true);
      })      
      
    })
  }


  updateDateState(state){

     return new Promise((resolve) => {

      this.setState({
        startDate : (state.startDate === undefined) ? this.state.startDate : state.startDate,
        endDate : (state.endDate === undefined) ? this.state.endDate : state.endDate,
      },()=>{
          resolve(1)
      })    

    });



  }

  async SettlementDateSelectionChange(state){

    this.setState({DataLoading : true});
    this.setState({AccountListShow : false});
    this.setState({DepositDateListShow : false});
    this.setState({AgSettlementListShow : false});  
    this.setState({TotalAmountShow : false});
    this.setState({SettlementChartShow : false});
    this.setState({SettlementAccountChartShow : false});  
    //update settlement date state
    await this.updateDateState(state);

    //check if it got selected all or not for account box
    if(await this.checkAccountBoxAllSeleted()) await this.updateAccountState([]);
    //check if it got selected all or not for deposit box
    if(await this.checkDepositBoxAllSeleted()) await this.updateDepositState([]);          
    
    if(!await this.checkAccountBoxAllSeleted()) {      
      this.updateAccountBox(this.state.arrayValue);//if there is a selection on account box
     }else{
      
       this.updateAccountBox(); 
     }
     
     
     if(!await this.checkDepositBoxAllSeleted()) {
      this.updateDepositBox(this.state.depositarrayValue);//if there is a selection on depositbox
    }else{
      this.updateDepositBox();
    }

    
    await this.updateGridList();
    this.setState({AgSettlementListShow : true});   
    this.setState({TotalAmountShow : true});
    await this.updateChart();
    this.setState({SettlementChartShow : true});
    await this.updateAccountChart();
    this.setState({SettlementAccountChartShow : true});  
    this.setState({AccountListShow : true});
    this.setState({DepositDateListShow : true});       
    this.setState({DataLoading : false});
         


  }

  async AccountListSelectionChange(value){

    this.setState({DataLoading : true});
    this.setState({SettlementDateShow : false});
    this.setState({DepositDateListShow : false});
    this.setState({AgSettlementListShow : false});  
    this.setState({TotalAmountShow : false});
    this.setState({SettlementChartShow : false});
    this.setState({SettlementAccountChartShow : false});  

    
    //update account state only
    await this.updateAccountState(value);
    
    if(await this.checkDepositBoxAllSeleted()) await this.updateDepositState([]);

    if(!await this.checkDepositBoxAllSeleted()) {
      this.updateDepositBox(this.state.depositarrayValue);//if there is a selection on depositbox
    }else{
      this.updateDepositBox();
    }

    
    await this.updateGridList();
    this.setState({AgSettlementListShow : true});      
    this.setState({TotalAmountShow : true});
    await this.updateChart();
    this.setState({SettlementChartShow : true});
    await this.updateAccountChart();
    this.setState({SettlementAccountChartShow : true});  
    this.setState({DepositDateListShow : true});    
    this.setState({SettlementDateShow : true});
    this.setState({DataLoading : false});

  }




  async DepositDateListSelectionChange(value){

    this.setState({DataLoading : true}); 
    this.setState({SettlementDateShow : false});
    this.setState({AccountListShow : false});  
    this.setState({AgSettlementListShow : false});     
    this.setState({TotalAmountShow : false});
    this.setState({SettlementChartShow : false});
    this.setState({SettlementAccountChartShow : false});  

    
  //update deposit date selection state only    
    await this.updateDepositState(value);
    
    if(await this.checkAccountBoxAllSeleted()) await this.updateAccountState([]);

    if(!await this.checkAccountBoxAllSeleted()) {
       this.updateAccountBox(this.state.arrayValue);  //if there is a selection on account box    
      }else{  
        this.updateAccountBox();  
      }

    await this.updateGridList();
    this.setState({AgSettlementListShow : true});    
    this.setState({TotalAmountShow : true});
    await this.updateChart();
    this.setState({SettlementChartShow : true});
    await this.updateAccountChart();
    this.setState({SettlementAccountChartShow : true});  
    this.setState({AccountListShow : true});  
    
    this.setState({SettlementDateShow : true});
    
    
    
    this.setState({DataLoading : false});
  }  

  // fetch account box data
  async updateAccountBox(val){

    let updated_val;
    
    return axios.post(fetchdisbursementbyaccount_url, formateParamsDate(this.state))
    .then(res => {
          
      if(val){//sync selected values (id, name, value)
        
        let temp = [];

        val.forEach(val_element => {

          res.data.forEach(data_element => {
    
              if(val_element.id == data_element.id){ 
    
                temp.push(data_element);
              
              }
    
          });
    
        });        
        
        val = temp;
      }

      this.setState({
        accountList: res.data,
        arrayValue: val ? val : res.data
      })      

      }).catch(error => {
        console.log('error', error);
      }); 

  }

  // fetch deposit box data
  async updateDepositBox(val){


    let updated_val;

    return axios.post(fetchdisbursementbydepositdate_url, formateParamsDate(this.state))
    .then(res => {
          
      let temp = [];

      if(val){//sync selected values (date, deposit, value)

        let temp = [];

        val.forEach(val_element => {

          res.data.forEach(data_element => {
    
              if(val_element.date == data_element.date){ 
    
                temp.push(data_element);
              
              }
    
          });
    
        });        
        
        val = temp;
      }

      this.setState({
        depositList: res.data,
        depositarrayValue: val ? val : res.data
        //depositarrayValue: res.data
      })      

      }).catch(error => {
        console.log('error', error);
      }); 
      
    
  }

  async updateChart(){

    return axios.post(fetchdisbursement_url, formateParamsDate(this.state))
    .then(res => {

      this.setState({ChartData:res.data});

    }).then(()=>{

      console.log(this.state.ChartData)
    })

  }

  async updateAccountChart(){

    return axios.post(fetchdisbursementbyaccountchart_url, formateParamsDate(this.state))
    .then(res => {
//      console.log(res.data);
      this.setState({AccountChartData:res.data});

    }).then(()=>{
      console.log(this.state.AccountChartData)
    })

  }
    
  

  //fetch Data Grid List
  async updateGridList(){

    return new Promise((resolve, reject) => {

      axios.post(fetchdisbursementrows_url, formateParamsDate(this.state))
      .then(res => {
          
        this.setState({
            columnDefs: res.data.columnDefs, 
            rowData: res.data.rowData,
            topRowData: res.data.topRowData      
        },()=>{

          //console.log(res.data.topRowData[0].total_amount_usd);

          this.setState({ TotalAmount : res.data.topRowData[0].total_amount_usd.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')});
          
          resolve(true);
          

        });

        }).catch(error => {
          
          console.log('error', error);
        });   

    });



  }


  render() {
    return (
      <div>
      <div className="panel panel-default product-filter-form filter-form">
      <div className={this.state.DataLoading ? 'loader':''}></div>
          <div className="panel-heading clearfix">
          <span><a href={disbursement_url}>Reset All</a></span>
          
            <div className="row">
        
        <span  className={this.state.SettlementDateShow ? '':'cloading'}   >
        
        <SettlementDate 
          beginning={beginning}
          startDate={this.state.startDate}
          endDate={this.state.endDate}
          onChange={this.SettlementDateSelectionChange}
          
        />
        </span>
        

        <span  className={this.state.AccountListShow ? '':'cloading'}   >
        <AccountList 
          accountList={this.state.accountList}
          arrayValue={this.state.arrayValue}
          accountAll={this.state.accountAll}
          onChange={this.AccountListSelectionChange}
          

        />
        </span>

        
        <span  className={this.state.DepositDateListShow ? '':'cloading'}   >
        <DepositDateList 
          depositarrayValue={this.state.depositarrayValue}
          depositList={this.state.depositList}
          depositAll={this.state.depositAll}
          onChange={this.DepositDateListSelectionChange}          

        />
        </span>  

        
        
        
        <div className={this.state.TotalAmountShow ? '':'cloading'}><h3><b>${this.state.TotalAmount}</b></h3></div>
        
            </div>
          </div>
      </div>
      
        <div>
    
       
          <div className="disbursmentchartbox" >

          <div className={this.state.SettlementChartShow ? '':'cloading'}   >
              <SettlementChart 
                ChartData={this.state.ChartData}
              />  
          </div> 
          
          </div>

          <div className="disbursmentchartbox">

          <div className={this.state.SettlementAccountChartShow ? '':'cloading'}   >
              <SettlementAccountChart 
                AccountChartData={this.state.AccountChartData}
              />   
            </div>
          </div>   
     
          

          <div className={this.state.AgSettlementListShow ? '':'cloading'}   >
          <AgSettlementList
            columnDefs={this.state.columnDefs}
            rowData={this.state.rowData}
            topRowData={this.state.topRowData}   
            
          />  
          </div>
        </div>
        

        

      </div>

      
    );
  }
}




ReactDOM.render(
  <DisbursementBox />,
  document.getElementById('container')
);