import React from 'react';
import ReactDOM from 'react-dom';

import FilterBar from './components/FilterBar'
import GoogleChart from './components/GoogleChart'
import AgCampaignList from './components/AgCampaignList'

class AdBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      account: '',
      marketplace: '',
      startDate: moment().subtract(30, 'days'),
      endDate: moment().subtract(1, 'days'),
      campaignIds: '',
      adGroupIds: '',
      keywordIds: '',
      asin_countries: ''
    };
    
    this.handleFilterSubmit = this.handleFilterSubmit.bind(this);
    this.handleCampaignFilter = this.handleCampaignFilter.bind(this);
    this.handleAdGroupFilter = this.handleAdGroupFilter.bind(this);
    this.handleKeywordFilter = this.handleKeywordFilter.bind(this);
    this.handleProductFilter = this.handleProductFilter.bind(this);
  }

  handleFilterSubmit(state) {
   
    this.setState({
    	account: state.account,
    	marketplace: state.marketplace,
    	startDate: state.startDate,
    	endDate: state.endDate,
      campaignIds: '',
      adGroupIds: '',
      keywordIds: '',
      asin_countries: ''
    });
  }

  handleCampaignFilter(campaignIds) {
    this.setState({
      campaignIds: campaignIds
    })
  }

  handleAdGroupFilter(adGroupIds) {
    this.setState({
      adGroupIds: adGroupIds
    })
  }

  handleKeywordFilter(keywordIds) {
    this.setState({
      keywordIds: keywordIds
    })
  }

  handleProductFilter(asin_countries) {
    this.setState({
      asin_countries: asin_countries
    })
  }

  render() {
    return (
      <div>
        <FilterBar
          accounts={accounts}
          beginning={beginning}
          marketplaces={marketplaces}
          startDate={this.state.startDate}
          endDate={this.state.endDate}
          onSubmit={this.handleFilterSubmit}
        />
        <GoogleChart 
          filterState={this.state}
        />
        <AgCampaignList 
          filterState={this.state}
          onCampaignFilter={this.handleCampaignFilter}
          onAdGroupFilter={this.handleAdGroupFilter}
          onKeywordFilter={this.handleKeywordFilter}
          onProductFilter={this.handleProductFilter}
        />
      </div>
    );
  }
}




ReactDOM.render(
  <AdBox />,
  document.getElementById('container')
);