import React from 'react';
import ReactDOM from 'react-dom';

import axios from 'axios';

class ImageBox extends React.Component {
	constructor(props) {
		super(props);

		this.state = {name: '', image: '', imagePreviewUrl: '', hideButton: false};

		this.handleClick = this.handleClick.bind(this);
		this.handleFilePreview = this.handleFilePreview.bind(this);
		this.handleFileUpload = this.handleFileUpload.bind(this);
	}

	handleClick(e) {
		if(this.state.imagePreviewUrl == '') {
			this.refs.fileUploader.click();
		} else {
			this.handleFileUpload();
		}
	}

	handleFilePreview(e) {
		let files = e.target.files || e.dataTransfer.files;

		if (!files.length) {
			return;
		}

		let reader = new FileReader();
		let file = files[0];
		
	    reader.onloadend = (e) => {
	      this.setState({
	        image: e.target.result,
	        imagePreviewUrl: reader.result
	      });
	    }

	    reader.readAsDataURL(file)
	}

	async handleFileUpload() {
		await axios.post(insertImage_url, {product_id: product_id, image: this.state.image}).then((res) => {
			if(res.data.success == 1){
				this.setState({
					imagePreviewUrl: res.data.product_image_url,
					hideButton: true
				})
			} else{
				console.log('failed insert');					
			}
		}).catch((e)=>{
			console.log(e);
		})
	}


	render() {
		let {imagePreviewUrl, hideButton} = this.state;
		let $imagePreview = null;
		let $uploadText = "Choose Image";
	    if (imagePreviewUrl) {
	      $imagePreview = (<img src={imagePreviewUrl} height="80" />);
	      $uploadText = "Upload Image";
	    }

	    let button;
	    if (hideButton) {
	      button = ''
	    } else {
	      button = <button id="img_upload" onClick={this.handleClick}>{$uploadText}</button>
	    }

	    return (
	      <div>
	      	<input type="file" id="file" ref="fileUploader" style={{display: "none"}} onChange={ (e) => this.handleFilePreview(e) }/>
	      	<div className="imgPreview">
	          {$imagePreview}
	        </div>
	        {button}
	      </div>
	    );
	}
}

ReactDOM.render(
  <ImageBox />,
  document.getElementById('imageBox')
);