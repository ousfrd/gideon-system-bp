import React from 'react';
import ReactDOM from 'react-dom';
import ImportStatsList from './components/ImportStatsList'
import ImportStatsFilterBar from './components/ImportStatsFilterBar'

class ImportStatsBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: moment().subtract(2, 'days'),
      endDate: moment(),     
    };

    this.handleFilterSubmit = this.handleFilterSubmit.bind(this);    

  }


  handleFilterSubmit(state) {
    this.setState({
      startDate: state.startDate,
      endDate: state.endDate
    });
  }

  render() {
    return (
      <div>      
        <ImportStatsFilterBar
          startDate={this.state.startDate}
          endDate={this.state.endDate}
          onSubmit={this.handleFilterSubmit}
        />        
        <ImportStatsList
          filterState={this.state}
        />
      </div>
    );
  }
}




ReactDOM.render(
  <ImportStatsBox />,
  document.getElementById('container')
);