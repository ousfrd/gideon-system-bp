import React from 'react';
import ReactDOM from 'react-dom';

import FilterBar from './components/FilterBar'
import GoogleChart from './components/GoogleChart'
import CampaignList from './components/CampaignList'

class AdBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      account: '',
      marketplace: '',
      startDate: moment().subtract(30, 'days'),
      endDate: moment().subtract(1, 'days'),
      campaign_query: null,
      campaignId: '',
      campaignState: 'all',
      campaignTargetingType: 'all',
      adGroupId: '',
      keywordId: '',
      asin: '',
      country: '',
    };
    
    this.handleFilterSubmit = this.handleFilterSubmit.bind(this);
    this.handleSearchCampaign = this.handleSearchCampaign.bind(this);
    this.handleSelectCampaign = this.handleSelectCampaign.bind(this);
    this.handleClearCampaign = this.handleClearCampaign.bind(this);
    this.handleFilterCampaignState = this.handleFilterCampaignState.bind(this);
    this.handleFilterCampaignTargetingType = this.handleFilterCampaignTargetingType.bind(this);
    this.handleSelectAdGroup = this.handleSelectAdGroup.bind(this);
    this.handleClearAdGroup = this.handleClearAdGroup.bind(this);
    this.handleSelectKeyword = this.handleSelectKeyword.bind(this);
    this.handleClearKeyword = this.handleClearKeyword.bind(this);
    this.handleSelectProduct = this.handleSelectProduct.bind(this);
    this.handleClearProduct = this.handleClearProduct.bind(this);
  }

  handleFilterSubmit(state) {
    this.setState({
    	account: state.account,
    	marketplace: state.marketplace,
    	startDate: state.startDate,
    	endDate: state.endDate
    });
  }

  handleSearchCampaign(campaign_query) {
    this.setState({
      campaign_query: campaign_query,
    })
  }

  handleSelectCampaign(campaignId) {
    this.setState({
      campaignId: campaignId
    })
  }

  handleClearCampaign(campaignId) {
    this.setState({
      campaignId: ''
    })
  }

  handleFilterCampaignState(state) {
    this.setState({
      campaignState: state
    })
  }

  handleFilterCampaignTargetingType(targetingType) {
    this.setState({
      campaignTargetingType: targetingType
    })    
  }

  handleSelectAdGroup(adGroupId) {
    this.setState({
      adGroupId: adGroupId
    })    
  }

  handleClearAdGroup(adGroupId) {
    this.setState({
      adGroupId: ''
    })
  }

  handleSelectKeyword(keywordId) {
    this.setState({
      keywordId: keywordId
    })
  }

  handleClearKeyword() {
    this.setState({
      keywordId: ''
    })
  }

  handleSelectProduct(asin, country) {
    this.setState({
      asin: asin,
      country: country
    })
  }

  handleClearProduct() {
    this.setState({
      asin: '',
      country: ''
    })
  }

  render() {
    return (
      <div>
        <FilterBar
          accounts={accounts}
          marketplaces={marketplaces}
          startDate={this.state.startDate}
          endDate={this.state.endDate}
          onSubmit={this.handleFilterSubmit}
        />
        <GoogleChart 
          filterState={this.state}
        />
        <CampaignList 
          filterState={this.state} 
          onSearchCampaign={this.handleSearchCampaign}
          onSelectCampaign={this.handleSelectCampaign}
          onClearCampaign={this.handleClearCampaign}
          onFilterCampaignState={this.handleFilterCampaignState}
          onFilterCampaignTargetingType={this.handleFilterCampaignTargetingType}
          onSelectAdGroup={this.handleSelectAdGroup}
          onClearAdGroup={this.handleClearAdGroup}
          onSelectKeyword={this.handleSelectKeyword}
          onClearKeyword={this.handleClearKeyword}
          onSelectProduct={this.handleSelectProduct}
          onClearProduct={this.handleClearProduct}
        />
      </div>
    );
  }
}




ReactDOM.render(
  <AdBox />,
  document.getElementById('container')
);