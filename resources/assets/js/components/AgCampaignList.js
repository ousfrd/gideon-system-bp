import React from 'react';
import axios from 'axios';
import formateParamsDate from './formateParamsDate'

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';

import RangeFloatingFilter from "./filters/RangeFloatingFilter";
import ImageCellRenderer from "./cellRenderers/ImageCellRenderer";
import AsinCellRenderer from "./cellRenderers/AsinCellRenderer";
import PriceCellRenderer from "./cellRenderers/PriceCellRenderer";
import PercentageCellRenderer from "./cellRenderers/PercentageCellRenderer";
import InventoryCellRenderer from "./cellRenderers/InventoryCellRenderer";
import CampaignStateCellRenderer from "./cellRenderers/CampaignStateCellRenderer";
import CampaignBudgetEditableCellRenderer from "./cellRenderers/CampaignBudgetEditableCellRenderer";
import CampaignNameCellRenderer from "./cellRenderers/CampaignNameCellRenderer";
import CampaignStateFilter from "./filters/campaignStateFilter";
import CampaignStatusFilter from "./filters/campaignStatusFilter";
import CampaignTargetingTypeFilter from "./filters/campaignTargetingTypeFilter";
import AdGroupNameCellRenderer from "./cellRenderers/AdGroupNameCellRenderer";
import AdGroupStateCellRenderer from "./cellRenderers/AdGroupStateCellRenderer";
import AdGroupBidEditableCellRenderer from "./cellRenderers/AdGroupBidEditableCellRenderer";
import KeywordBidEditableCellRenderer from "./cellRenderers/KeywordBidEditableCellRenderer";
import KeywordTextCellRenderer from "./cellRenderers/KeywordTextCellRenderer";
import ViewSearchtermsCellRenderer from "./cellRenderers/ViewSearchtermsCellRenderer";
import CampaignStatusCellRenderer from "./cellRenderers/CampaignStatusCellRenderer";
import SearchtermCellRenderer from "./cellRenderers/SearchtermCellRenderer";
import KeywordStateCellRenderer from "./cellRenderers/KeywordStateCellRenderer";
import CustomDateComponent from "./filters/customDateComponent";
import AutoSchedulerStateFilter from "./filters/AutoSchedulerStateFilter";
import AutoSchedulerStateCellRenderer from "./cellRenderers/AutoSchedulerStateCellRenderer";




 
import { Tabs, Tab, Table } from 'react-bootstrap';

class AgCampaignList extends React.Component {

    constructor(props) {
        super(props);

        this.handleSelect = this.handleSelect.bind(this);

        this.state = {						
        	key: 1,
	        tab1_title: 'Campaigns',
	        tab2_title: 'Ad Groups',
	        tab3_title: 'Keywords',
	        tab4_title: 'Products',
	        tab5_title: 'Searchterms',
            campaignColumnDefs: [],
            campaignRowData: [],
            topCampaignRowData: [],

            adGroupColumnDefs: [],
            adGroupRowData: [],
            topAdGroupRowData: [],

            keywordColumnDefs: [],
            keywordRowData: [],
            topKeywordRowData: [],

            productColumnDefs: [],
            productRowData: [],
            topProductRowData: [],

            searchtermColumnDefs: [],
            searchtermRowData: [],
            topSearchtermRowData: [],

            campaignIds: '',
            adGroupIds: '',
            keywordIds: '',

            frameworkComponents: { rangeFloatingFilter: RangeFloatingFilter, 
							imageCellRenderer: ImageCellRenderer, 
              asinCellRenderer: AsinCellRenderer,
              priceCellRenderer: PriceCellRenderer,
              percentageCellRenderer: PercentageCellRenderer,
              inventoryCellRenderer: InventoryCellRenderer,
              campaignStateCellRenderer: CampaignStateCellRenderer,
              campaignBudgetEditableCellRenderer: CampaignBudgetEditableCellRenderer,
              campaignNameCellRenderer: CampaignNameCellRenderer,
							campaignStateFilter: CampaignStateFilter,
							autoSchedulerStateFilter: AutoSchedulerStateFilter,
							autoSchedulerStateCellRenderer: AutoSchedulerStateCellRenderer,
              campaignStatusFilter: CampaignStatusFilter,
              campaignTargetingTypeFilter: CampaignTargetingTypeFilter,
              adGroupNameCellRenderer: AdGroupNameCellRenderer,
              adGroupStateCellRenderer: AdGroupStateCellRenderer,
              adGroupBidEditableCellRenderer: AdGroupBidEditableCellRenderer,
              keywordBidEditableCellRenderer: KeywordBidEditableCellRenderer,
              keywordTextCellRenderer: KeywordTextCellRenderer,
              viewSearchtermsCellRenderer: ViewSearchtermsCellRenderer,
              campaignStatusCellRenderer: CampaignStatusCellRenderer,
              searchtermCellRenderer: SearchtermCellRenderer,
              keywordStateCellRenderer: KeywordStateCellRenderer,
							agDateInput: CustomDateComponent
							
            },
            sortingOrder: ["desc", "asc", null],
						rowSelection: "multiple"

        }

        
        this.getRowHeight = this.getRowHeight.bind(this);
        this.getProductRowHeight = this.getProductRowHeight.bind(this);
        this.onCampaignsGridReady = this.onCampaignsGridReady.bind(this);
        this.onAdGroupsGridReady = this.onAdGroupsGridReady.bind(this);
        this.onKeywordsGridReady = this.onKeywordsGridReady.bind(this);
        this.onProductsGridReady = this.onProductsGridReady.bind(this);
        this.onSearchtermsGridReady = this.onSearchtermsGridReady.bind(this);
        this.loadCampaignsTab = this.loadCampaignsTab.bind(this);
        this.loadAdGroupsTab = this.loadAdGroupsTab.bind(this);
        this.loadKeywordsTab = this.loadKeywordsTab.bind(this);
        this.loadProductsTab = this.loadProductsTab.bind(this);
        this.loadSearchtermsTab = this.loadSearchtermsTab.bind(this);

        this.onCampaignFilterChanged = this.onCampaignFilterChanged.bind(this);
    }	

	onCampaignsGridReady(params) {
	  this.campaignsGridApi = params.api;
	}

	onAdGroupsGridReady(params) {
	  this.adGroupsGridApi = params.api;
	}

	onKeywordsGridReady(params) {
	  this.keywordsGridApi = params.api;
	}

	onProductsGridReady(params) {
	  this.productsGridApi = params.api;
	}

	onSearchtermsGridReady(params) {
	  this.searchtermsGridApi = params.api;
	}

	getRowHeight(params) {
      if (params.node.rowPinned == 'top') {
          return 30;
      } else {
          return 50;
      }
    }

    getProductRowHeight(params) {
      if (params.node.rowPinned == 'top') {
          return 30;
      } else {
          return 80;
      }    	
    }

    componentDidMount() {

      	this.loadCampaignsTab();

    }

    componentDidUpdate(prevProps) {

    	if( this.props.filterState !== prevProps.filterState ) {

    		if(this.props.filterState.account != prevProps.filterState.account
    			|| this.props.filterState.marketplace != prevProps.filterState.marketplace
    			|| this.props.filterState.startDate != prevProps.filterState.startDate
    			|| this.props.filterState.endDate != prevProps.filterState.endDate) {
    			this.setState({ key: 1 });
				this.loadCampaignsTab(true);
    		}

    		if(this.props.filterState.campaignIds != prevProps.filterState.campaignIds) {
    			axios.post(fetchcampaignstotal_url, formateParamsDate(this.props.filterState))
	              .then(res => {

	              	this.setState({
	                  	topCampaignRowData: res.data.topRowData,
	              	})

	            })
    		}
        }
    }

	selectCampaign(campaignId, name) {
		this.setState({
			key: 2,
			tab1_title: name,
			tab2_title: "Ad Groups",
			tab3_title: "Keywords"
		}, () => {
		    this.loadAdGroupsTab(true);
		});

		this.props.onCampaignFilter(JSON.stringify([campaignId]));
		this.props.onAdGroupFilter(JSON.stringify([]));
		this.props.onKeywordFilter(JSON.stringify([]));
		this.props.onProductFilter(JSON.stringify([]));
	}

	onCampaignFilterChanged(params) {
	  this.gridApi = params.api;
      var campaignIds = [];
      this.gridApi.forEachNodeAfterFilterAndSort(function(rowNode, index) {
        var campaignId = rowNode.data.campaignId;
        campaignIds.push(campaignId);
      })

      this.props.onCampaignFilter(JSON.stringify(campaignIds));
	}

	selectAdGroup(adGroupId, name, targetingType) {
		if(targetingType == "auto") {
			this.setState({
				key: 5,
				tab2_title: name
			}, () => {
			    this.loadSearchtermsTab(true);
			});	
		} else {
			this.setState({
				key: 3,
				tab2_title: name
			}, () => {
			    this.loadKeywordsTab(true);
			});	
		}

		this.props.onAdGroupFilter(JSON.stringify([adGroupId]));
		this.props.onKeywordFilter(JSON.stringify([]));
		this.props.onProductFilter(JSON.stringify([]));
	}

	selectKeyword(keywordId, name) {
		this.setState({
			key: 5,
			tab3_title: name
		}, () => {
		    this.loadSearchtermsTab(true);
		});

		this.props.onKeywordFilter(JSON.stringify([keywordId]));
		this.props.onProductFilter(JSON.stringify([]));
	}

	selectProduct(asin, country) {
		this.setState({
			key: 5
		}, () => {
		    this.loadSearchtermsTab(true);
		});

		this.props.onProductFilter(JSON.stringify([[asin, country]]));		
	}

	dateComparator(date1, date2) {
		  var date1Number = this.monthToComparableNumber(date1);
		  var date2Number = this.monthToComparableNumber(date2);
		  if (date1Number === null && date2Number === null) {
		    return 0;
		  }
		  if (date1Number === null) {
		    return -1;
		  }
		  if (date2Number === null) {
		    return 1;
		  }
		  return date1Number - date2Number;
	}

	dateFilterComparator(filterLocalDateAtMidnight, cellValue) {
		  var dateAsString = cellValue;
	      var dateParts = dateAsString.split("/");
	      var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));
	      if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
	        return 0;
	      }
	      if (cellDate < filterLocalDateAtMidnight) {
	        return -1;
	      }
	      if (cellDate > filterLocalDateAtMidnight) {
	        return 1;
	      }
	}

	monthToComparableNumber(date) {
	  if (date === undefined || date === null || date.length !== 10) {
	    return null;
	  }
	  var yearNumber = date.substring(6, 10);
	  var monthNumber = date.substring(3, 5);
	  var dayNumber = date.substring(0, 2);
	  var result = yearNumber * 10000 + monthNumber * 100 + dayNumber;
	  return result;
	}

		loadCampaignsTab(reload=false) {
			if(this.state.campaignRowData.length == 0 || reload) {
			
				axios.post(fetchcampaigns_url, formateParamsDate(this.props.filterState))
							.then(res => {
								
								var columnDefs = res.data.columnDefs;
							columnDefs[3].cellRendererParams = {
							onClick: this.selectCampaign.bind(this)
				};

				columnDefs[7].comparator = this.dateComparator.bind(this);
				columnDefs[7].filterParams = { comparator: this.dateFilterComparator.bind(this) };

								this.setState({
										campaignColumnDefs: columnDefs, 
										campaignRowData: res.data.rowData,
										topCampaignRowData: res.data.topRowData,
								})

						})

		}

		if(this.state.tab1_title != 'Campaigns') {
					this.setState({ key: 1, tab1_title: 'Campaigns' });
		} else {
			this.setState({ key: 1 });
		}
		}

    loadAdGroupsTab(reload=false) {
	  	if(this.state.adGroupRowData.length == 0 || reload) {
	  		var params = formateParamsDate(this.props.filterState);
	    	axios.post(fetchadgroups_url, params)
              .then(res => {
                
               var columnDefs = res.data.columnDefs;
	          	columnDefs[1].cellRendererParams = {
				      onClick: this.selectAdGroup.bind(this)
			  }

              this.setState({
                  adGroupColumnDefs: columnDefs, 
                  adGroupRowData: res.data.rowData,
              })
            })
      	}

		if(this.state.tab2_title != 'Ad Groups') {
	        this.setState({ key: 2, tab2_title: 'Ad Groups' });
		} else {
			this.setState({ key: 2 });
		}
    }

    loadKeywordsTab(reload=false) {
	  	if(this.state.keywordRowData.length == 0 || reload) {
	    	axios.post(fetchkeywords_url, formateParamsDate(this.props.filterState))
              .then(res => {
               
              var columnDefs = res.data.columnDefs;
	          	columnDefs[1].cellRendererParams = {
				      onClick: this.selectKeyword.bind(this)
			  }

              this.setState({
	               keywordColumnDefs: columnDefs, 
	               keywordRowData: res.data.rowData
              })
            })
      	}

	      if(this.state.tab3_title != 'Keywords') {
	        this.setState({ key: 3, tab3_title: 'Keywords'});
	      } else {
	        this.setState({ key: 3 });
	      }
    }

    loadProductsTab(reload=false) {
		if(this.state.productRowData.length == 0 || reload) {

	    	axios.post(fetchproducts_url, formateParamsDate(this.props.filterState))
              .then(res => {
                
                var columnDefs = res.data.columnDefs;
	          	columnDefs[4].cellRendererParams = {
				      onClick: this.selectProduct.bind(this)
			  }

              this.setState({
                  productColumnDefs: res.data.columnDefs, 
                  productRowData: res.data.rowData,
                  topProductRowData: res.data.topRowData,
              })

            })
        }

	    this.setState({ key: 4 });
    }

    loadSearchtermsTab(reload=false) {
		if(this.state.searchtermRowData.length == 0 || reload) {
	    	axios.post(fetchsearchterms_url, formateParamsDate(this.props.filterState))
              .then(res => {
                
              this.setState({
                  searchtermColumnDefs: res.data.columnDefs, 
                  searchtermRowData: res.data.rowData
              })

            })    		
    	}

	    this.setState({ key: 5 });
    }


    handleSelect(key) {
    	if(key == 1) {
    		
    		this.loadCampaignsTab();
    		
	    } else if(key == 2) {

		    this.loadAdGroupsTab();

	    } else if(key == 3) {

		    this.loadKeywordsTab();
		    
	    } else if(key == 4) {
	    	if(this.state.campaignIds != this.props.filterState.campaignIds || this.state.adGroupIds != this.props.filterState.adGroupIds) {
	    		this.loadProductsTab(true);
	    		this.setState({
	    			campaignIds: this.props.filterState.campaignIds,
	    			adGroupIds: this.props.filterState.adGroupIds
	    		})
	    	} else {
	    		this.loadProductsTab();
	    	}
	    } else if(key == 5) {
	    	if(this.state.campaignIds != this.props.filterState.campaignIds || this.state.adGroupIds != this.props.filterState.adGroupIds) {
		    	this.loadSearchtermsTab(true);
		    	this.setState({
	    			campaignIds: this.props.filterState.campaignIds,
	    			adGroupIds: this.props.filterState.adGroupIds
	    		})
		    } else {
		    	this.loadSearchtermsTab();
		    }
	    }
    }

    onCampaignsBtExport() {
    	this.campaignsGridApi.exportDataAsCsv({
    		fileName: 'Campaigns.csv'
    	});
    }

    onAdGroupsBtExport() {
    	this.adGroupsGridApi.exportDataAsCsv({
    		fileName: 'AdGroups.csv'
    	});
    }

    onKeywordsBtExport() {
    	this.keywordsGridApi.exportDataAsCsv({
    		fileName: 'Keywords.csv'
    	});
    }

    onProductsBtExport() {
    	this.productsGridApi.exportDataAsCsv({
    		fileName: 'Products.csv'
    	});
    }

    onSearchtermsBtExport() {
    	this.searchtermsGridApi.exportDataAsCsv({
    		fileName: 'Searchterms.csv'
    	});
    }

	onAdGroupsQuickFilterChanged() {
	    this.adGroupsGridApi.setQuickFilter(document.getElementById("adGroupsQuickFilter").value);
	}

	onKeywordsQuickFilterChanged() {
	    this.keywordsGridApi.setQuickFilter(document.getElementById("keywordsQuickFilter").value);
	}

	onAdGroupsBidUpdate() {
		let adGroupIds = this.adGroupsGridApi.getSelectedRows().map(row => row.adGroupId);
		let newBid = document.getElementById("adGroupsBidInput").value;
		if(adGroupIds.length > 0 && newBid) {
			axios.get(updateAdGroupsDefaultBid_url, {
		      params: {adGroupIds: adGroupIds, bid: newBid}
		    })
		    .then(res => {
		    	console.log(res);

		    	let noticeNode = document.getElementById("adGroupsUpdateNotice");
		    	noticeNode.style.display = "inline-block";
		      	if(res.data.success) {
			      	var itemsToUpdate = [];
			      	this.adGroupsGridApi.getSelectedNodes().forEach(function(rowNode) {
						  var data = rowNode.data;
					      data.defaultBid = newBid;
					      itemsToUpdate.push(data);
					});
					this.adGroupsGridApi.updateRowData({ update: itemsToUpdate });
					noticeNode.textContent="Update Success!";
				} else {
					noticeNode.textContent="Update Failed!";
				}
				setTimeout(function() {
				    noticeNode.style.display = "none";
				}, 2000);
		    })
		} else {
			alert("Please choose one or more AdGroups and input new default bid value!")
		}

	}

	onKeywordsBidUpdate() {
		let keywordIds = this.keywordsGridApi.getSelectedRows().map(row => row.keywordId);
		let newBid = document.getElementById("keywordsBidInput").value;
		if(keywordIds.length > 0 && newBid) {
			axios.get(updateKeywordGroupBid_url, {
		      params: {keywordIds: keywordIds, bid: newBid}
		    })
		    .then(res => {
		    	console.log(res.success);

		    	let noticeNode = document.getElementById("keywordsUpdateNotice");
		    	noticeNode.style.display = "inline-block";
		      	if(res.data.success) {
			      	var itemsToUpdate = [];
			      	this.keywordsGridApi.getSelectedNodes().forEach(function(rowNode) {
						  var data = rowNode.data;
					      data.bid = newBid;
					      itemsToUpdate.push(data);
					});
					this.keywordsGridApi.updateRowData({ update: itemsToUpdate });
					noticeNode.textContent="Update Success!";
				} else {
					noticeNode.textContent="Update Failed!";
				}
				setTimeout(function() {
				    noticeNode.style.display = "none";
				}, 2000);
		    })
		} else {
			alert("Please choose one or more keywords and input new bid value!")
		}

	}

    render() {
        return (

        	<Tabs activeKey={this.state.key} onSelect={this.handleSelect} id="controlled-tab">

        		<Tab eventKey={1} title={this.state.tab1_title}>

        		  <div style={{ paddingBottom: "4px", display: "inline-block" }}>
		            <label style={{ marginTop: "9px" }}>
		              <button onClick={this.onCampaignsBtExport.bind(this)}>Export to CSV</button>
		            </label>
		          </div>

	                <div 
	                  className="ag-theme-balham"
	                  style={{ 
		                height: '1500px'}} 
			            >
	                    <AgGridReact
	                     	enableSorting={true}
	                     	enableFilter={true}
	                        enableColResize={true}
	                        rowDragManaged={true}
	                     	floatingFilter={true}
	                      	suppressDragLeaveHidesColumns={true}
	                      	pinnedTopRowData={this.state.topCampaignRowData}
													// pagination={true}
													columnDefs={this.state.campaignColumnDefs}
													defaultColDef={this.state.defaultColDef}
	                        rowData={this.state.campaignRowData}
	                        frameworkComponents={this.state.frameworkComponents}
	                        getRowHeight={this.getRowHeight}
	                    	floatingFiltersHeight={60}
	                    	onFilterChanged={this.onCampaignFilterChanged}
	                    	onGridReady={this.onCampaignsGridReady}
	                    	sortingOrder={this.state.sortingOrder}
	                    >
	                    </AgGridReact>
	                </div>
                </Tab>

                <Tab eventKey={2} title={this.state.tab2_title}>

        		  <div style={{ paddingBottom: "4px", marginTop: "9px", display: "inline-block" }}>
		            
		            <button onClick={this.onAdGroupsBtExport.bind(this)}>Export to CSV</button>
		            
			        <input
			            type="text"
			            id="adGroupsBidInput"
			            placeholder="new default bid"
			            style={{marginLeft: "5px", padding: "1px"}}
			        />
			        <input 
			        	type="button" 
			        	onClick={this.onAdGroupsBidUpdate.bind(this)} 
			        	value="Update"
			        />
			        <span id="adGroupsUpdateNotice" style={{marginLeft: "5px", fontWeight: "bold"}}></span>
		          </div>

	                <div 
	                  className="ag-theme-balham"
	                  style={{ 
		                height: '1500px'}} 
			            >
	                    <AgGridReact
	                     	enableSorting={true}
	                     	enableFilter={true}
	                        enableColResize={true}
	                        rowDragManaged={true}
	                     	floatingFilter={true}
	                      	suppressDragLeaveHidesColumns={true}
	                      	pinnedTopRowData={this.state.topAdGroupRowData}
	                      	pagination={true}
	                        columnDefs={this.state.adGroupColumnDefs}
	                        rowData={this.state.adGroupRowData}
	                        frameworkComponents={this.state.frameworkComponents}
	                        getRowHeight={this.getRowHeight}
	                    	floatingFiltersHeight={60}
	                    	onGridReady={this.onAdGroupsGridReady}
	                    	sortingOrder={this.state.sortingOrder}
	                    	rowSelection={this.state.rowSelection}
	                    	rowMultiSelectWithClick={true}
	                    >
	                    </AgGridReact>      
	                </div>
                </Tab>

                <Tab eventKey={3} title={this.state.tab3_title}>

        		  <div style={{ paddingBottom: "4px", marginTop: "9px", display: "inline-block" }}>
		            
		            <button onClick={this.onKeywordsBtExport.bind(this)}>Export to CSV</button>
		            
			        <input
			            type="text"
			            id="keywordsBidInput"
			            placeholder="new bid"
			            style={{marginLeft: "5px", padding: "1px"}}
			        />
			        <input 
			        	type="button" 
			        	onClick={this.onKeywordsBidUpdate.bind(this)} 
			        	value="Update"
			        />
			        <span id="keywordsUpdateNotice" style={{marginLeft: "5px", fontWeight: "bold"}}></span>
		          </div>

                	<div 
	                  className="ag-theme-balham"
	                  style={{ 
		                height: '1500px'}} 
			            >
	                    <AgGridReact
	                     	enableSorting={true}
	                     	enableFilter={true}
	                        enableColResize={true}
	                        rowDragManaged={true}
	                     	floatingFilter={true}
	                      	suppressDragLeaveHidesColumns={true}
	                      	pinnedTopRowData={this.state.topKeywordRowData}
	                      	pagination={true}
	                        columnDefs={this.state.keywordColumnDefs}
	                        rowData={this.state.keywordRowData}
	                        frameworkComponents={this.state.frameworkComponents}
	                        getRowHeight={this.getRowHeight}
	                    	floatingFiltersHeight={60}
	                    	onGridReady={this.onKeywordsGridReady}
	                    	sortingOrder={this.state.sortingOrder}
	                    	rowSelection={this.state.rowSelection}
	                    	rowMultiSelectWithClick={true}
	                    >
	                    </AgGridReact>
	                </div>           	
                </Tab>

                <Tab eventKey={4} title={this.state.tab4_title}>

        		  <div style={{ paddingBottom: "4px", display: "inline-block" }}>
		            <label style={{ marginTop: "9px" }}>
		              <button onClick={this.onProductsBtExport.bind(this)}>Export to CSV</button>
		            </label>
		          </div>

          	        <div 
	                  className="ag-theme-balham"
	                  style={{ 
		                height: '1500px'}} 
			            >
			            <AgGridReact
	                     	enableSorting={true}
	                     	enableFilter={true}
	                        enableColResize={true}
	                        rowDragManaged={true}
	                     	floatingFilter={true}
	                      	suppressDragLeaveHidesColumns={true}
	                      	pinnedTopRowData={this.state.topProductRowData}
	                        columnDefs={this.state.productColumnDefs}
	                        rowData={this.state.productRowData}
	                        frameworkComponents={this.state.frameworkComponents}
	                        getRowHeight={this.getProductRowHeight}
	                    	floatingFiltersHeight={60}
	                    	onGridReady={this.onProductsGridReady}
	                    	sortingOrder={this.state.sortingOrder}
	                    >
	                    </AgGridReact>  
                    </div>
                </Tab>

                <Tab eventKey={5} title={this.state.tab5_title}>

        		  <div style={{ paddingBottom: "4px", display: "inline-block" }}>
		            <label style={{ marginTop: "9px" }}>
		              <button onClick={this.onSearchtermsBtExport.bind(this)}>Export to CSV</button>
		            </label>
		          </div>

                    <div 
	                  className="ag-theme-balham"
	                  style={{ 
		                height: '1500px'}} 
			            >
 						<AgGridReact
	                     	enableSorting={true}
	                     	enableFilter={true}
	                        enableColResize={true}
	                        rowDragManaged={true}
	                     	floatingFilter={true}
	                      	suppressDragLeaveHidesColumns={true}
	                      	pinnedTopRowData={this.state.topSearchtermRowData}
	                        columnDefs={this.state.searchtermColumnDefs}
	                        rowData={this.state.searchtermRowData}
	                        frameworkComponents={this.state.frameworkComponents}
	                        getRowHeight={this.getRowHeight}
	                    	floatingFiltersHeight={60}
	                    	onGridReady={this.onSearchtermsGridReady}
	                    	sortingOrder={this.state.sortingOrder}
	                    >
	                    </AgGridReact>
	                </div>   
                </Tab>

            </Tabs>
            
        );
    }

}

export default AgCampaignList;