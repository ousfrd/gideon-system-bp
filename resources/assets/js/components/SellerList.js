import React from 'react';
import axios from 'axios';
import { Table } from 'react-bootstrap';

class SellerList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            rows: [],
        }

    }


    componentDidMount() {

      axios.post(fetchsellerprofitrows_url, this.props.filterState)
          .then(res => {

          	this.setState({
          		rows: res.data,
          	})

        })



    }

    componentDidUpdate(prevProps) {

        if( this.props.filterState !== prevProps.filterState ) {
          axios.post(fetchsellerprofitrows_url, this.props.filterState)
              .then(res => {

                this.setState({
                    rows: res.data,
                })

            })

        }

    }



    render() {

        return (
                <Table>
                    <thead>
                      <tr>
                        <th>PM</th>
                        <th>Goal</th>
                        <th>Profit</th>
                        <th>Order</th>
                        <th>Ad</th>
                        <th>Refund</th>
                        <th>Payout</th>
                        <th>完成率</th>
                        <th>差额</th>
                      </tr>
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.rows.map((row, index) => (
                      <tr key={index}>
                        <td>{row.pm}</td>
                        <td>{row.goal}</td>
                        <td>{row.profit}</td>
                        <td>{row.orders}</td>
                        <td>{row.ad_expenses}</td>
                        <td>{row.refund}</td>
                        <td>{row.payout}</td>
                        <td>{row.finish_rate}</td>
                        <td>{row.diff}</td>
                      </tr>
                      ))}
                      
                    </tbody>
                </Table>

            );
    }
}

export default SellerList;

