import React from 'react';
import axios from 'axios';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';

class AgSettlementList extends React.Component {
    constructor(props) {
        super(props);

        this.getRowHeight = this.getRowHeight.bind(this);
        this.onGridReady = this.onGridReady.bind(this);
        this.dateFormatTo = this.dateFormatTo.bind(this);

                
    }


    dateFormatTo(value){

        return moment(value).format('YYYY-MM-DD');

    }
    

    getRowHeight(params) {
        if (params.node.rowPinned == 'top') {
            return 30;
        } else {
            return 80;
        }
      }
      

    onGridReady(params) {
	    this.gridApi = params.api;
	    this.gridColumnApi = params.columnApi;
	    params.api.sizeColumnsToFit();
      }    
      



    componentDidMount() {
        



    }

    componentDidUpdate(prevProps) {
        

    }


    render() {
        return (
                <div 
                  className="ag-theme-balham"
                  style={{ 
	                height: '1500px'}} 
		            >
                 <div style={{ paddingBottom: "4px", display: "inline-block" }}>
                  <label style={{ marginTop: "9px" }}>
                    
                  </label>
                 </div>

                    <AgGridReact
                        enableSorting={true}
                        enableFilter={true}
                        enableColResize={true}
                        rowDragManaged={true}
                        suppressDragLeaveHidesColumns={true}
                        onFilterChanged={this.onFilterChanged}
                        onGridReady={this.onGridReady}
                        columnDefs={this.props.columnDefs}
                        pinnedTopRowData={this.props.topRowData}
                        rowData={this.props.rowData}
                        
                    >
                    </AgGridReact>
                </div>
            );
    }
}

export default AgSettlementList;

