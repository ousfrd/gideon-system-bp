function SettlementformateParamsDate(filterState) {
  if(typeof filterState.startDate !== 'string') {
    filterState.startDate = filterState.startDate.format('MMMM D, YYYY');
    filterState.endDate = filterState.endDate.format('MMMM D, YYYY');
  }

  if(typeof filterState.depositstartDate !== 'string') {
    filterState.depositstartDate = filterState.depositstartDate.format('MMMM D, YYYY');
    filterState.depositendDate = filterState.depositendDate.format('MMMM D, YYYY');
  }


	return filterState
}

export default SettlementformateParamsDate;