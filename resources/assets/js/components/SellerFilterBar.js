import React from 'react';

class SellerFilterBar extends React.Component {
  constructor(props) {
    super(props);

    var d = new Date();
    var month = d.getMonth();
    var year = d.getFullYear();

    this.state = {
      year: year,
      month: month+1,
      type: 'profit',
    };

    this.handleChangeType = this.handleChangeType.bind(this);
    this.handleChangeYear = this.handleChangeYear.bind(this);
    this.handleChangeMonth = this.handleChangeMonth.bind(this);
  }



  handleChangeType(e) {
    this.setState({type: e.target.value});
    this.props.onChangeType(e.target.value);
  }

  handleChangeYear(e) {
    this.setState({year: e.target.value});
    this.props.onChangeYear(e.target.value);
  }

  handleChangeMonth(e) {
    this.setState({month: e.target.value});
    this.props.onChangeMonth(e.target.value);
  }


  render() {
      var today = new Date();
      var currentYear = today.getFullYear();
      var yearRange = [0, 1, 2, 3, 4].map(function(i) {
        return currentYear - i;
      });
      var yearOptions = yearRange.map(function(year) {
        return <option value="{year}">{year}</option>;
      });

      return (
      <div className="panel panel-default product-filter-form filter-form">
        <form onSubmit={this.handleSubmit}>
          <div className="panel-heading clearfix">
            <div className="row">


              <div className="col-md-2 col-sm-2 col-xs-12 mobile-padding-filter padding-sm">

                <div className="input form-group select">

                <select name="year" value={this.state.year} onChange={this.handleChangeYear} className="form-control">
                  {yearOptions}
                </select>

                </div>    

              </div>


              <div className="col-md-2 col-sm-2 col-xs-12 mobile-padding-filter padding-sm">

                <div className="input form-group select">

                 <select name="month" value={this.state.month} onChange={this.handleChangeMonth} className="form-control">
                   <option value="1">Jan</option>
                   <option value="2">Feb</option>
                   <option value="3">Mar</option>
                   <option value="4">Apr</option>
                   <option value="5">May</option>
                   <option value="6">Jun</option>
                   <option value="7">Jul</option>
                   <option value="8">Aug</option>
                   <option value="9">Sep</option>
                   <option value="10">Oct</option>
                   <option value="11">Nov</option>
                   <option value="12">Dec</option>
                </select>

                </div>    

              </div>


              <div className="col-md-2 col-sm-2 col-xs-12 mobile-padding-filter padding-sm">

                <div className="input form-group select">

                 <select name="type" value={this.state.type} onChange={this.handleChangeType} className="form-control">
                   <option value="profit">Profit</option>
                   <option value="orders">Orders</option>
                   <option value="total_gross">Total Gross</option>
                   <option value="refund">Refund</option>
                   <option value="payout">Payout</option>
                   <option value="product_cost">Product Cost</option>
                   <option value="ad_expenses">Ad Expenses</option>
                   <option value="total_cost">Total Cost</option>
                </select>

                </div>

              </div>


            </div>

          </div>

        </form>

      </div>
      );
    }


}

export default SellerFilterBar;