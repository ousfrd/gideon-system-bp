import React from 'react';
import axios from 'axios';
import formateParamsDate from './formateParamsDate'
import { Chart } from 'react-google-charts';



class SettlementChart extends React.Component {
	constructor(props) {
    super(props);
  }

	render() {
		return (


      <Chart
      chartType="LineChart"
      rows={this.props.ChartData}
      columns={[
        {
          type: "string",
          label: "Deposit Date"
        },
        {
          type: "number",
          label: "Amount($)"
        }
      ]}
      options={
        // Chart options
        {
          hAxis: {
            title: "Deposit Date",
          },
          vAxis: { title: "Amount($)"}
        }
      }
      width={"100%"}
      height={"350px"}
      legendToggle
    />      

		);
	}
}

export default SettlementChart;