import React from 'react';
import axios from 'axios';
import '../../../sass/switch.scss';

class KeywordStateCellRenderer extends React.Component {

    // did you know that React passes props to your component constructor??
    constructor(props) {
        super(props);
        // from here you can access any of the props!
        // console.log('The value is ' + props.value);
        // we can even call grid API functions, if that was useful
        // props.api.selectAll();

        this.handleKeywordStateChange = this.handleKeywordStateChange.bind(this);
        this.state = {
            keywordState: props.value
        };
    }

    handleKeywordStateChange(e) {

        axios.get(updateKeywordState_url, {
          params: {keywordId: e.target.dataset.id}
        })
        .then(res => {

          console.log(res);

        }) 

        var keyword_state = this.state.keywordState == 'enabled' ? 'paused' : 'enabled';
        this.setState({ keywordState: keyword_state });

    }

    render() {
            // or access props using 'this'
            return (
                <div>
                {this.props.data.keywordId &&
                  <span data-id={this.props.data.keywordId} className={this.state.keywordState == 'enabled' ? 'rc-switch rc-switch-checked' : 'rc-switch'} onClick={this.handleKeywordStateChange}>
                    <span className="rc-switch-inner"></span>
                  </span>
                }
                </div>
            );
        
    }
}

export default KeywordStateCellRenderer;