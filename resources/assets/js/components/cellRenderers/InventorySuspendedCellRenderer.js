import React from 'react';
import { OverlayTrigger, Button, Popover } from 'react-bootstrap';

class InventorySuspendedCellRenderer extends React.Component {

    // did you know that React passes props to your component constructor??
    constructor(props) {
        super(props);
        // from here you can access any of the props!
        // console.log('The value is ' + props.value);
        // we can even call grid API functions, if that was useful
        // console.log(props.data.seller_qty);
    }


    render() {
        if (this.props.data.seller_qty_suspended) {
            
           const popoverHoverFocus2 = (
              <Popover id="popover-trigger-hover-focus-2" title="Suspended Seller Fulfillable Inventory (Reserved Inventory)">
                {this.props.data.seller_qty_suspended.split("<!--!>").map((i,key) => {
                    return <div key={key}>{i}</div>;
                })}
              </Popover>
            );

            return <OverlayTrigger
              trigger={['hover', 'focus']}
              placement="right"
              overlay={popoverHoverFocus2}
            >
              <a>{this.props.value} ({this.props.data.total_reserved_qty_suspended})</a>
            </OverlayTrigger>
           

        } else {
            return this.props.value
        }


    }
}

export default InventorySuspendedCellRenderer;