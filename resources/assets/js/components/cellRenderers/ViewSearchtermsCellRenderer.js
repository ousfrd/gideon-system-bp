import React from 'react';

class ViewSearchtermsCellRenderer extends React.Component {

    // did you know that React passes props to your component constructor??
    constructor(props) {
        super(props);
        // from here you can access any of the props!
        // console.log('The value is ' + props.value);
        // we can even call grid API functions, if that was useful
        // props.api.selectAll();
    }

    _onButtonClick(){
        this.props.onClick(this.props.data.asin, this.props.data.country);
    }

    render() {
        // or access props using 'this'
        return <a href="#" onClick={this._onButtonClick.bind(this)}>View Searchterms</a>;
    }
}

export default ViewSearchtermsCellRenderer;