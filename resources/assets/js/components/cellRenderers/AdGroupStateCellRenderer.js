import React from 'react';
import axios from 'axios';
import '../../../sass/switch.scss';

class AdGroupStateCellRenderer extends React.Component {

    // did you know that React passes props to your component constructor??
    constructor(props) {
        super(props);
        // from here you can access any of the props!
        // console.log('The value is ' + props.value);
        // we can even call grid API functions, if that was useful
        // props.api.selectAll();

        this.handleAdGroupStateChange = this.handleAdGroupStateChange.bind(this);
        this.state = {
            adGroupState: props.value
        };
    }

    handleAdGroupStateChange(e) {

        axios.get(updateAdGroupState_url, {
          params: {adGroupId: e.target.dataset.id}
        })
        .then(res => {

          console.log(res);

        }) 

        var adGroup_state = this.state.adGroupState == 'enabled' ? 'paused' : 'enabled';
        this.setState({ adGroupState: adGroup_state });

    }

    render() {
            // or access props using 'this'
            return (
                <div>
                {this.props.data.adGroupId &&
                  <span data-id={this.props.data.adGroupId} className={this.state.adGroupState == 'enabled' ? 'rc-switch rc-switch-checked' : 'rc-switch'} onClick={this.handleAdGroupStateChange}>
                    <span className="rc-switch-inner"></span>
                  </span>
                }
                </div>
            );
        
    }
}

export default AdGroupStateCellRenderer;