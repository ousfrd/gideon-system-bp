import React from 'react';

class CampaignStatusCellRenderer extends React.Component {

    // did you know that React passes props to your component constructor??
    constructor(props) {
        super(props);
        // from here you can access any of the props!
        // console.log('The value is ' + props.value);
        // we can even call grid API functions, if that was useful
        // props.api.selectAll();
        this.servingStatusClassName = {
            'Delivering': 'color0', 
            'Paused': 'color8', 
            'Out of budget': 'color2',
            'Archived': 'color8',
            'Ended': 'color1',
            'Advertiser Payment Failure': 'color1',
        }
    }

    render() {
        // or access props using 'this'
        return <span className={this.servingStatusClassName[this.props.value]}>{this.props.value}</span>;
    }
}

export default CampaignStatusCellRenderer;