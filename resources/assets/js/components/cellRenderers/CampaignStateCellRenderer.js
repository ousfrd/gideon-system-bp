import React from 'react';
import axios from 'axios';
import '../../../sass/switch.scss';

class CampaignStateCellRenderer extends React.Component {

    // did you know that React passes props to your component constructor??
    constructor(props) {
        super(props);
        // from here you can access any of the props!
        // console.log('The value is ' + props.value);
        // we can even call grid API functions, if that was useful
        // props.api.selectAll();
        
        this.handleCampaignStateChange = this.handleCampaignStateChange.bind(this);
        this.state = {
            campaignState: props.value
        };
    }

    handleCampaignStateChange(e) {

        axios.get(updateCampaignState_url, {
          params: {campaignId: e.target.dataset.id}
        })
        .then(res => {

          console.log(res);

        }) 

        var campaign_state = this.state.campaignState == 'enabled' ? 'paused' : 'enabled';
        this.setState({ campaignState: campaign_state });

    }

    render() {
            // or access props using 'this'
            return (
                <div>
                {this.props.data.campaignId &&
                  <span data-id={this.props.data.campaignId} className={this.state.campaignState == 'enabled' ? 'rc-switch rc-switch-checked' : 'rc-switch'} onClick={this.handleCampaignStateChange}>
                    <span className="rc-switch-inner"></span>
                  </span>
                }
                </div>
            );
        
    }
}

export default CampaignStateCellRenderer;