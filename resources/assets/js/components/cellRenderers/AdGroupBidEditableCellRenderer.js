import React from 'react';
import axios from 'axios';
import contentEditable from '../ContentEditable'

class AdGroupBidEditableCellRenderer extends React.Component {

    // did you know that React passes props to your component constructor??
    constructor(props) {
        super(props);
        // from here you can access any of the props!
        // console.log('The value is ' + props.value);
        // we can even call grid API functions, if that was useful
        // props.api.selectAll();
        this.updateAdGroupDefaultBid = this.updateAdGroupDefaultBid.bind(this);
    }

    updateAdGroupDefaultBid(e) {
        var adGroupId = e.props.adGroupId;
        var originBid = e.props.originBid;
        var value = e.domElm.textContent;

        if(originBid != value) {
            axios.get(updateAdGroupDefaultBid_url, {
              params: {adGroupId: adGroupId, bid: value}
            })
            .then(res => {

              console.log(res);

            })   
        }
    }

    render() {
        let EditableSpan = contentEditable('span');
        // or access props using 'this'
        return (
           <p>$<EditableSpan value={this.props.value} onSave={this.updateAdGroupDefaultBid} adGroupId={this.props.data.adGroupId} originBid={this.props.data.defaultBid}/></p>
        )
    }
}

export default AdGroupBidEditableCellRenderer;