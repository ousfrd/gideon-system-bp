import React from 'react';
import axios from 'axios';
import '../../../sass/switch.scss';

class ManagerCellRenderer extends React.Component {

    // did you know that React passes props to your component constructor??
    constructor(props) {
        super(props);
        // from here you can access any of the props!
        // console.log('The value is ' + props.value);
        // we can even call grid API functions, if that was useful
        // props.api.selectAll();
        
        this.handleIsContinuedChange = this.handleIsContinuedChange.bind(this);
        this.state = {
            discontinued: props.data.discontinued
        };
    }

    handleIsContinuedChange(e) {
        var discontinued = this.state.discontinued ? 0 : 1;

        axios.post(updateProduct_url, {pk: 'discontinued-'+e.target.dataset.id, value: discontinued})
        .then(res => {

          console.log(res);

        }) 

        this.setState({ discontinued: discontinued });
    }

    render() {
            const product_id = this.props.data.product_id;
            if(product_id) {
                // or access props using 'this'
                return (
                    <div>
                        {this.props.value}<br />
                      <span data-id={this.props.data.product_id} className={this.state.discontinued ? 'rc-switch rc-switch-checked' : 'rc-switch'} onClick={this.handleIsContinuedChange}>
                        <span className="rc-switch-inner"></span>
                      </span>
                    </div>
                );
            }
            return "";
    }
}

export default ManagerCellRenderer;