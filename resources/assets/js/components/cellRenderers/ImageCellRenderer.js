import React from 'react';

class ImageCellRenderer extends React.Component {

    // did you know that React passes props to your component constructor??
    constructor(props) {
        super(props);
        // from here you can access any of the props!
        // console.log('The value is ' + props.value);
        // we can even call grid API functions, if that was useful
        // props.api.selectAll();
    }

    render() {
        // or access props using 'this'
        return <a href={this.props.data.amz_link} target='_blank'><img src={this.props.value} style={{height: 80}}/></a>;
    }
}

export default ImageCellRenderer;