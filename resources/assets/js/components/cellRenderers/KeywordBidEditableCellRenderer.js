import React from 'react';
import axios from 'axios';
import contentEditable from '../ContentEditable'

class KeywordBidEditableCellRenderer extends React.Component {

    // did you know that React passes props to your component constructor??
    constructor(props) {
        super(props);
        // from here you can access any of the props!
        // console.log('The value is ' + props.value);
        // we can even call grid API functions, if that was useful
        // props.api.selectAll();
        this.updateKeywordBid = this.updateKeywordBid.bind(this);
    }

    updateKeywordBid(e) {
        var keywordId = e.props.keywordId;
        var originBid = e.props.originBid;
        var value = e.domElm.textContent;

        if(originBid != value) {
            axios.get(updateKeywordBid_url, {
              params: {keywordId: keywordId, bid: value}
            })
            .then(res => {

              console.log(res);

            })   
        }
    }

    render() {
        let EditableSpan = contentEditable('span');
        // or access props using 'this'
        return (
           <p>$<EditableSpan value={this.props.value > 0 ? this.props.value : this.props.data.defaultBid} onSave={this.updateKeywordBid} keywordId={this.props.data.keywordId} originBid={this.props.value > 0 ? this.props.value : this.props.data.defaultBid} editable={this.props.data.keywordText != '(_targeting_auto_)'}/></p>
        )
    }
}

export default KeywordBidEditableCellRenderer;