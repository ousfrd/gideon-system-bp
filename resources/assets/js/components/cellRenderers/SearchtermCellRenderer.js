import React from 'react';

class SearchtermCellRenderer extends React.Component {

    // did you know that React passes props to your component constructor??
    constructor(props) {
        super(props);
        // from here you can access any of the props!
        // console.log('The value is ' + props.value);
        // we can even call grid API functions, if that was useful
        // props.api.selectAll();
        this.amzLinks = {
            'US': 'https://www.amazon.com/',
            'CA': 'https://www.amazon.ca/',
            'MX': 'https://www.amazon.com.mx/',
            'UK': 'https://www.amazon.co.uk/',
            'DE': 'https://www.amazon.de/',
            'ES': 'https://www.amazon.es/',
            'FR': 'https://www.amazon.fr/',
            'IT': 'https://www.amazon.it/',
            'JP': 'https://www.amazon.co.jp/',
            'AU': 'https://www.amazon.com.au/',
        }
    }



    render() {
        // or access props using 'this'
        var href = this.amzLinks[this.props.data.country] + 's/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=' + this.props.value;
        return <div><a href={href} target='_blank'>{this.props.value}</a></div>;
    }
}

export default SearchtermCellRenderer;