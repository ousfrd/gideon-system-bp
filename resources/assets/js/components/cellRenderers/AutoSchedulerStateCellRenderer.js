import React from 'react';
import axios from 'axios';
import '../../../sass/switch.scss';

class AutoSchedulerStateCellRenderer extends React.Component {

    // did you know that React passes props to your component constructor??
    constructor(props) {
        super(props);
        // from here you can access any of the props!
        // console.log('The value is ' + props.value);
        // we can even call grid API functions, if that was useful
        // props.api.selectAll();
        
        this.handleAutoSchedulerStateChange = this.handleAutoSchedulerStateChange.bind(this);
        this.state = {
          autoSchedulerState: props.value
        };
    }

    handleAutoSchedulerStateChange(e) {

        axios.get(updateAutoSchedulerState_url, {
          params: {campaignId: e.target.dataset.id}
        })
        .then(res => {

          console.log(res);

        }) 

        console.log(this.state.autoSchedulerState);
        console.log(e.target.dataset.id);

        var auto_scheduler_state = this.state.autoSchedulerState == "enabled" ? "disabled" : "enabled";
        this.setState({ autoSchedulerState: auto_scheduler_state });

    }

    render() {
            // or access props using 'this'
            return (
                <div>
                {this.props.data.campaignId &&
                  <span data-id={this.props.data.campaignId} className={this.state.autoSchedulerState == 'enabled' ? 'rc-switch rc-switch-checked' : 'rc-switch'} onClick={this.handleAutoSchedulerStateChange}>
                    <span className="rc-switch-inner"></span>
                  </span>
                }
                </div>
            );
        
    }
}

export default AutoSchedulerStateCellRenderer;