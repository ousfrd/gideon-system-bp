import React from 'react';
import axios from 'axios';
import contentEditable from '../ContentEditable'

class campaignBudgetEditableCellRenderer extends React.Component {

    // did you know that React passes props to your component constructor??
    constructor(props) {
        super(props);
        // from here you can access any of the props!
        // console.log('The value is ' + props.value);
        // we can even call grid API functions, if that was useful
        // props.api.selectAll();
        this.updateCampaignDailyBudget = this.updateCampaignDailyBudget.bind(this);
    }

    updateCampaignDailyBudget(e) {
        var campaignId = e.props.campaignId;
        var value = e.domElm.textContent;
        var originBudget = e.props.originBudget;

        if(originBudget != value) {
          axios.get(updateCampaignDailyBudget_url, {
            params: {campaignId: campaignId, budget: value}
          }).then(res => {

            console.log(res);

          })   
        }
    }

    render() {
        let EditableSpan = contentEditable('span');
        // or access props using 'this'
        return (
            <div>
            {this.props.data.campaignId ? (
                <p>$<EditableSpan value={this.props.value} onSave={this.updateCampaignDailyBudget} campaignId={this.props.data.campaignId} originBudget={this.props.data.dailyBudget}/></p>
            ) : (
                <p>${this.props.data.dailyBudget}</p>
            )}
            </div>
        )
    }
}

export default campaignBudgetEditableCellRenderer;