import React from 'react';
import { OverlayTrigger, Button, Popover } from 'react-bootstrap';

class AsinCellRenderer extends React.Component {

    // did you know that React passes props to your component constructor??
    constructor(props) {
        super(props);
        // from here you can access any of the props!
        // console.log('The value is ' + props.value);
        // we can even call grid API functions, if that was useful
        // props.api.selectAll();
    }

    render() {
        // or access props using 'this'
        if (this.props.data.seller_qty) {
            const popoverHoverFocus = (
              <Popover id="popover-trigger-hover-focus" title="Seller Fulfillable Inventory (Reserved Inventory)">
                {this.props.data.seller_qty.split("<!--!>").map((i,key) => {
                    return <div key={key}>{i}</div>;
                })}
              </Popover>
            );

            return <div>
                    <a href={this.props.data.product_link} target='_blank'>{this.props.data.asin}</a><br />{this.props.data.country}<br />
                    <OverlayTrigger
                      trigger={['hover', 'focus']}
                      placement="right"
                      overlay={popoverHoverFocus}
                    >
                      <a>{this.props.data.total_qty} ({this.props.data.total_reserved_qty})</a>
                    </OverlayTrigger>
                </div>;

        } else {
            return <div>
                <a href={this.props.data.product_link} target='_blank'>{this.props.data.asin}</a><br />{this.props.data.country}<br />
                this.props.value
                </div>;
        }
    }
}

export default AsinCellRenderer;