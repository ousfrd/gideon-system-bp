import React from 'react';
import axios from 'axios';
import { AgGridReact } from 'ag-grid-react';
import formateParamsDate from './formateParamsDate'
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';


class ImportStatsList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            columnDefs: [],
            rowData: [],
            DataLoading:false
            
            }

            this.onGridReady = this.onGridReady.bind(this);
        }

        onGridReady(params) {
            this.gridApi = params.api;
            this.gridColumnApi = params.columnApi;
            params.api.sizeColumnsToFit();
          }
          
          
        componentDidMount() {
            this.setState({DataLoading : true});

            axios.post(fetchimportstatsrows_url, formateParamsDate(this.props.filterState))
            .then(res => {
                            
                this.setState({
                    columnDefs: res.data.columnDefs, 
                    rowData: res.data.rowData
                })
    
            }).then(()=>{

                this.setState({DataLoading : false});

            })
    
        }
    

        componentDidUpdate(prevProps) {


            if( this.props.filterState !== prevProps.filterState ) {
                this.setState({DataLoading : true});
              axios.post(fetchimportstatsrows_url, formateParamsDate(this.props.filterState))
                  .then(res => {
    
                    this.setState({
                        columnDefs: res.data.columnDefs, 
                        rowData: res.data.rowData                        
                    })  
    
                }).then(()=>{

                    this.setState({DataLoading : false});
                
            })
    
            }
    
        }

    render() {
        return (
            <div>
                <div className={this.state.DataLoading ? 'loader':''}></div>
            
            <div 
            className="ag-theme-balham"
            style={{ 
              height: '1500px'}} 
              >
                    <AgGridReact
                    enableSorting={true}
                    enableColResize={true}
                    onGridReady={this.onGridReady}
                        columnDefs={this.state.columnDefs}
                        rowData={this.state.rowData}                        
                    >
                    </AgGridReact>
            </div>
            </div>
            );
    }
}

export default ImportStatsList;

