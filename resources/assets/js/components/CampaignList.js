import React from 'react';
import axios from 'axios';
import formateParamsDate from './formateParamsDate'
import { Tabs, Tab, Table } from 'react-bootstrap';
import '../../sass/switch.scss';
import contentEditable from './ContentEditable'

const WAIT_INTERVAL = 1000;
const ENTER_KEY = 13;

class CampaignList extends React.Component {
  constructor(props) {
    super(props);

    this.handleSelect = this.handleSelect.bind(this);

    this.state = {
      key: 1,
      tab1_title: 'Campaigns',
      tab2_title: 'Ad Groups',
      tab3_title: 'Keywords',
      tab4_title: 'Products',
      tab5_title: 'Searchterms',
      campaigns: [], 
      origin_campaigns: [], 
      campaignId: '',
      campaignDir: {
        account: 'asc',
        name: 'asc',
        product_manager: 'asc',
        targetingType: 'asc',
        start_date: 'asc',
        total_impressions: 'desc',
        total_clicks: 'desc',
        ctr: 'desc',
        total_cost: 'desc',
        cpc: 'desc',
        total_sales: 'desc',
        acos: 'desc',
        total_orders: 'desc',
        cr: 'desc',
        dailyBudget: 'desc'
      },
      activeCampaignDir: '',
      adgroups: [],
      origin_adgroups: [], 
      adGroupId: '',
      adGroupState: '',
      adGroupDir: {
        total_impressions: 'desc',
        total_clicks: 'desc',
        ctr: 'desc',
        total_cost: 'desc',
        cpc: 'desc',
        total_sales: 'desc',
        acos: 'desc',
        total_orders: 'desc',
        cr: 'desc',
        defaultBid: 'desc'
      },
      activeAdGroupDir: '',
      keywords: [],
      origin_keywords: [], 
      keywordDir: {
        total_impressions: 'desc',
        total_clicks: 'desc',
        ctr: 'desc',
        total_cost: 'desc',
        cpc: 'desc',
        total_sales: 'desc',
        acos: 'desc',
        total_orders: 'desc',
        cr: 'desc'
      },
      activeKeywordDir: '',
      products: [],
      origin_products: [], 
      productDir: {
        total_impressions: 'desc',
        total_clicks: 'desc',
        ctr: 'desc',
        total_cost: 'desc',
        cpc: 'desc',
        total_sales: 'desc',
        acos: 'desc',
        total_orders: 'desc',
        cr: 'desc'
      },
      activeProductDir: '',
      searchterms: [],
      origin_searchterms: [], 
      searchtermDir: {
        total_impressions: 'desc',
        total_clicks: 'desc',
        ctr: 'desc',
        total_cost: 'desc',
        cpc: 'desc',
        total_sales: 'desc',
        acos: 'desc',
        total_orders: 'desc',
        cr: 'desc'
      },
      activeSearchtermDir: '',
      asin: '',
      country: '',
      keywordGroupBid: '',
      keywordQuery: '',
      keywordState: '',
      keywordMatchType: '',
      mins: { 'campaigns': {}, 'adgroups': {}, 'keywords': {}, 'products': {}, 'searchterms': {} },
      maxs: { 'campaigns': {}, 'adgroups': {}, 'keywords': {}, 'products': {}, 'searchterms': {} },
    };

    this.searchCampaign = this.searchCampaign.bind(this);
    this.searchCampaignProductManager = this.searchCampaignProductManager.bind(this);
    this.selectCampaign = this.selectCampaign.bind(this);
    this.filterCampaignState = this.filterCampaignState.bind(this);
    this.filterCampaignStatus = this.filterCampaignStatus.bind(this);
    this.filterAdGroupState = this.filterAdGroupState.bind(this);
    this.filterCampaignTargetingType = this.filterCampaignTargetingType.bind(this);

    this.selectAdGroup = this.selectAdGroup.bind(this);
    this.selectProduct = this.selectProduct.bind(this);
    this.searchProduct = this.searchProduct.bind(this);
    this.searchProductManager = this.searchProductManager.bind(this);

    this.selectKeyword = this.selectKeyword.bind(this);
    this.searchKeyword = this.searchKeyword.bind(this);
    this.checkAllKeyword = this.checkAllKeyword.bind(this);
    this.checkKeyword = this.checkKeyword.bind(this);
    this.updateKeywordGroupBid = this.updateKeywordGroupBid.bind(this);
    this.handleKeywordGroupBidChange = this.handleKeywordGroupBidChange.bind(this);
    this.viewKeywordSearchterms = this.viewKeywordSearchterms.bind(this);
    this.filterKeywordState = this.filterKeywordState.bind(this);
    this.filterKeywordMatchType = this.filterKeywordMatchType.bind(this);

    this.handleCampaignStateChange = this.handleCampaignStateChange.bind(this);
    this.handleKeywordStateChange = this.handleKeywordStateChange.bind(this);

    this.servingStatus = {
        'CAMPAIGN_STATUS_ENABLED': 'Delivering', 
        'CAMPAIGN_PAUSED': 'Paused', 
        'CAMPAIGN_OUT_OF_BUDGET': 'Out of budget',
        'CAMPAIGN_ARCHIVED': 'Archived'
    }

    this.servingStatusClassName = {
        'CAMPAIGN_STATUS_ENABLED': 'color0', 
        'CAMPAIGN_PAUSED': 'color8', 
        'CAMPAIGN_OUT_OF_BUDGET': 'color2',
        'CAMPAIGN_ARCHIVED': 'color8'
    }

    this.getTotalBudget = this.getTotalBudget.bind(this);
    this.getTotalImpressions = this.getTotalImpressions.bind(this);
    this.getTotalClicks = this.getTotalClicks.bind(this);
    this.getTotalCTR = this.getTotalCTR.bind(this);
    this.getTotalCost = this.getTotalCost.bind(this);
    this.getTotalCPC = this.getTotalCPC.bind(this);
    this.getTotalSales = this.getTotalSales.bind(this);
    this.getTotalOrders = this.getTotalOrders.bind(this);
    this.getTotalACoS = this.getTotalACoS.bind(this);
    this.getTotalProfit = this.getTotalProfit.bind(this);
    this.getTotalCR = this.getTotalCR.bind(this);
    this.rangeFilter = this.rangeFilter.bind(this);
  }

  handleSelect(key) {
    if(key == 1) {
      if(this.state.tab1_title != 'Campaigns') {
        this.props.onClearAdGroup();
        this.props.onClearCampaign();
        this.setState({ key: key, tab1_title: 'Campaigns', activeCampaignDir: '', tab2_title: 'Ad Groups', activeAdGroupDir: '', campaignId: '', adGroupId: ''});
      } else {
        this.setState({ key });
      }
      
    } else if(key == 2) {
      if(this.state.tab2_title != 'Ad Groups') {
        this.props.onClearAdGroup();
        this.setState({ key: key, tab2_title: 'Ad Groups', activeAdGroupDir: '', adGroupId: ''});
      } else {
        this.setState({ key });
      }

    } else if(key == 3) {
      if(this.state.tab3_title != 'Keywords') {
        this.props.onClearKeyword();
        this.setState({ key: key, tab3_title: 'Keywords', activeKeywordDir: ''});
      } else {
        this.setState({ key });
      }

    } else if(key == 4) {
      this.setState({ key: key });


    } else if(key == 5) {
      this.setState({ key: key });

    }
  }

  componentDidMount() {

      axios.get(fetchcampaigns_url, {
	       params: formateParamsDate(this.props.filterState)
	    })
      .then(res => {

      	this.setState({
      		campaigns: res.data,
          origin_campaigns: res.data,
      	})

      })

      
      axios.get(fetchadgroups_url, {
        params: formateParamsDate(this.props.filterState)
      })
      .then(res => {

        this.setState({
          adgroups: res.data,
          origin_adgroups: res.data,
        })

      }) 

      axios.get(fetchkeywords_url, {
        params: formateParamsDate(this.props.filterState)
      })
      .then(res => {

        this.setState({
          keywords: res.data,
          origin_keywords: res.data,
        })

      }) 

      axios.get(fetchproducts_url, {
        params: formateParamsDate(this.props.filterState)
      })
      .then(res => {

        this.setState({
          products: res.data,
          origin_products: res.data,
        })

      }) 

  }


  	componentDidUpdate(prevProps) {
	  if (this.props.filterState !== prevProps.filterState) {

      var mainfilter_changed = this.props.filterState.account !== prevProps.filterState.account 
      || this.props.filterState.marketplace !== prevProps.filterState.marketplace
      || this.props.filterState.startDate !== prevProps.filterState.startDate
      || this.props.filterState.endDate !== prevProps.filterState.endDate;

      var campaign_changed = this.props.filterState.campaign_query !== prevProps.filterState.campaign_query
      || this.props.filterState.campaignState !== prevProps.filterState.campaignState
      || this.props.filterState.campaignStatus !== prevProps.filterState.campaignStatus
      || this.props.filterState.campaignTargetingType !== prevProps.filterState.campaignTargetingType;

      var campaign_choosed = this.props.filterState.campaignId !== prevProps.filterState.campaignId;

      var adgroup_choosed = this.props.filterState.adGroupId !== prevProps.filterState.adGroupId;

      var keyword_choosed = this.props.filterState.keywordId !== prevProps.filterState.keywordId;

      var product_choosed = this.props.filterState.asin !== prevProps.filterState.asin || this.props.filterState.country !== prevProps.filterState.country;

      if(mainfilter_changed || campaign_changed) {

          axios.get(fetchcampaigns_url, {
    	      params: formateParamsDate(this.props.filterState)
    	    })
          .then(res => {

          	this.setState({
          		campaigns: res.data,
              origin_campaigns: res.data,
          	})

          })
      }


      if(mainfilter_changed || campaign_changed || campaign_choosed) {
        axios.get(fetchadgroups_url, {
          params: formateParamsDate(this.props.filterState)
        })
        .then(res => {

          this.setState({
            adgroups: res.data,
            origin_adgroups: res.data,
          })

        })    
      }

      if(mainfilter_changed || campaign_changed || campaign_choosed || adgroup_choosed) {
        axios.get(fetchkeywords_url, {
          params: formateParamsDate(this.props.filterState)
        })
        .then(res => {

          this.setState({
            keywords: res.data,
            origin_keywords: res.data,
          })

        }) 
      }

      if(mainfilter_changed || campaign_changed || campaign_choosed || adgroup_choosed || keyword_choosed) {      
        axios.get(fetchproducts_url, {
          params: formateParamsDate(this.props.filterState)
        })
        .then(res => {

          this.setState({
            products: res.data,
            origin_products: res.data,
          })

        }) 
      }


      if(this.props.filterState.startDate != prevProps.filterState.startDate || this.props.filterState.endDate != prevProps.filterState.endDate || keyword_choosed || product_choosed) {
        var filterState = formateParamsDate(this.props.filterState);

        axios.get(fetchsearchterms_url, {
          params: formateParamsDate(this.props.filterState)
        })
        .then(res => {

          this.setState({
            searchterms: res.data,
            origin_searchterms: res.data,
          })

        }) 

      }

	  }
	}

  searchCampaign(e) {
    if (e.keyCode === ENTER_KEY) {
      this.props.onSearchCampaign(e.target.value);
    }
  }

  selectCampaign(e) {

    this.props.onSelectCampaign(e.target.dataset.id);

    this.setState({ key: 2, tab1_title: 'Ad Campaign: ' + e.target.dataset.name, campaignId: e.target.dataset.id });

    e.preventDefault();
  }

  filterCampaignState(e) {
    this.props.onFilterCampaignState(e.target.value);
  }

  filterCampaignStatus(e) {
    var campaigns = this.state.origin_campaigns
    if(e.target.value != 'all') {
      campaigns = campaigns.filter(campaign => campaign['servingStatus'] == e.target.value);
    }
    
    this.setState({campaigns: campaigns})
  }

  filterCampaignTargetingType(e) {
    this.props.onFilterCampaignTargetingType(e.target.value);
  }

  searchCampaignProductManager(e) {
    if (e.keyCode === ENTER_KEY) {
      var campaigns = this.state.origin_campaigns
      var term = e.target.value;

      if(term != '') {
        var filtered_campaigns = campaigns.filter(function(campaign) {
          return campaign.product_manager && campaign.product_manager.toLowerCase().includes(term.toLowerCase()) 
        });   
        
        this.setState({campaigns: filtered_campaigns})   
      } else {
        this.setState({campaigns: campaigns})
      }
    }

  }

  sortCampaign(event, sortKey, currentDir) {
    const campaigns = this.state.campaigns;
    const dir = this.state.campaignDir;
    if(sortKey == 'account' || sortKey == 'name' || sortKey == 'targetingType') {
      if(currentDir == 'desc') {
        campaigns.sort((a,b) => b[sortKey].localeCompare(a[sortKey]));
        dir[sortKey] = 'asc';
      } else {
        campaigns.sort((a,b) => a[sortKey].localeCompare(b[sortKey]));
        dir[sortKey] = 'desc';
      }       
    } else if(sortKey == 'start_date') {
      if(currentDir == 'desc') {
        campaigns.sort(function(a,b) {
          a = a[sortKey].split('/').reverse().join('');
          b = b[sortKey].split('/').reverse().join('');
          return b.localeCompare(a); 
        });
        dir[sortKey] = 'asc';
      } else {
        campaigns.sort(function(a,b) {
          a = a[sortKey].split('/').reverse().join('');
          b = b[sortKey].split('/').reverse().join('');
          return a.localeCompare(b); 
        });
        dir[sortKey] = 'desc';
      }       
    } else {
      if(currentDir == 'desc') {
        campaigns.sort((a,b) => (parseFloat(a[sortKey]) > parseFloat(b[sortKey])) ? -1 : 1);
        dir[sortKey] = 'asc';
      } else {
        campaigns.sort((a,b) => (parseFloat(a[sortKey]) > parseFloat(b[sortKey])) ? 1 : -1);
        dir[sortKey] = 'desc';
      }      
    }


    this.setState({campaigns: campaigns, campaignDir: dir, activeCampaignDir: sortKey});

    event.preventDefault();
  }

  getCampaignActiveDir(sortKey) {
     return this.state.activeCampaignDir == sortKey ? this.state.campaignDir[sortKey] == 'asc' ? '▼' : '▲' : '';
  }

  handleCampaignStateChange(e) {
    var index = e.target.dataset.index;
    var campaigns = this.state.campaigns;

    axios.get(updateCampaignState_url, {
      params: {campaignId: e.target.dataset.id}
    })
    .then(res => {

      console.log(res);

    }) 

    campaigns[index].state = campaigns[index].state == 'enabled' ? 'paused' : 'enabled';

    this.setState({ campaigns: campaigns, origin_campaigns: campaigns });
  }

  handleKeywordStateChange(e) {
    var index = e.target.dataset.index;
    var keywords = this.state.keywords;

    axios.get(updateKeywordState_url, {
      params: {keywordId: e.target.dataset.id}
    })
    .then(res => {

      console.log(res);

    }) 

    keywords[index].state = keywords[index].state == 'enabled' ? 'paused' : 'enabled';

    this.setState({ keywords: keywords, origin_keywords: keywords });
  }

  updateCampaignDailyBudget(e) {
    var campaignId = e.props.campaignId;
    var originBudget = e.props.originBudget;
    var value = e.domElm.textContent;

    if(originBudget != value) {
      axios.get(updateCampaignDailyBudget_url, {
        params: {campaignId: campaignId, budget: value}
      })
      .then(res => {

        console.log(res);

      })       
    }

  }

  updateKeywordBid(e) {
    var keywordId = e.props.keywordId;
    var originBid = e.props.originBid;
    var value = e.domElm.textContent;

    if(originBid != value) {
        axios.get(updateKeywordBid_url, {
          params: {keywordId: keywordId, bid: value}
        })
        .then(res => {

          console.log(res);

        })   
    }


  }


  handleKeywordGroupBidChange(e) {
    this.setState({ keywordGroupBid: e.target.value });
  }

  updateKeywordGroupBid(e) {
    var keywords = this.state.keywords;
    var keywordGroupBid = this.state.keywordGroupBid;

    var keywordIds = [];
    for (var i = 0; i < keywords.length; i++) {
      if(keywords[i]['checked'] && keywords[i]['keywordText'] != '(_targeting_auto_)') {
          keywordIds.push(keywords[i]['keywordId']);
      }
    }

    axios.get(updateKeywordGroupBid_url, {
      params: {keywordIds: keywordIds, bid: keywordGroupBid}
    })
    .then(res => {

      console.log(res);

      for (var i = 0; i < keywords.length; i++) {
        if(keywords[i]['checked'] && keywords[i]['keywordText'] != '(_targeting_auto_)') {
            keywords[i]['bid'] = keywordGroupBid;
        }
      }

      this.setState({keywords: keywords, origin_keywords: keywords});  

    })   

  }

  filterAdGroupState(e) {
    var filterState = formateParamsDate(this.props.filterState);
    filterState.adGroupState = e.target.value;
    
    axios.get(fetchadgroups_url, {
      params: filterState
    })
    .then(res => {

      this.setState({
        adgroups: res.data,
        origin_adgroups: res.data,
      })

    })    

    this.setState({adGroupState: e.target.value});  
  }

  selectAdGroup(e) {

    this.props.onSelectAdGroup(e.target.dataset.id);

    this.setState({ key: 3, tab2_title: 'Ad Group: ' + e.target.dataset.name, adGroupId: e.target.dataset.id });

    e.preventDefault();
  }


  sortAdGroup(event, sortKey, currentDir) {
    const adgroups = this.state.adgroups;
    const dir = this.state.adGroupDir;
    if(currentDir == 'desc') {
      adgroups.sort((a,b) => (parseFloat(a[sortKey]) > parseFloat(b[sortKey])) ? -1 : 1);
      dir[sortKey] = 'asc';
    } else {
      adgroups.sort((a,b) => (parseFloat(a[sortKey]) > parseFloat(b[sortKey])) ? 1 : -1);
      dir[sortKey] = 'desc';
    }

    this.setState({adgroups: adgroups, adGroupDir: dir, activeAdGroupDir: sortKey});

    event.preventDefault();
  }

  getAdGroupActiveDir(sortKey) {
     return this.state.activeAdGroupDir == sortKey ? this.state.adGroupDir[sortKey] == 'asc' ? '▼' : '▲' : '';
  }

  updateAdGroupDefaultBid(e) {
    var adGroupId = e.props.adGroupId;
    var originBid = e.props.originBid;
    var value = e.domElm.textContent;

    if(originBid != value) {
        axios.get(updateAdGroupDefaultBid_url, {
          params: {adGroupId: adGroupId, bid: value}
        })
        .then(res => {

          console.log(res);

        })   
    }
  }

  sortKeyword(event, sortKey, currentDir) {
    const keywords = this.state.keywords;
    const dir = this.state.keywordDir;
    if(currentDir == 'desc') {
      keywords.sort((a,b) => (parseFloat(a[sortKey]) > parseFloat(b[sortKey])) ? -1 : 1);
      dir[sortKey] = 'asc';
    } else {
      keywords.sort((a,b) => (parseFloat(a[sortKey]) > parseFloat(b[sortKey])) ? 1 : -1);
      dir[sortKey] = 'desc';
    }

    this.setState({keywords: keywords, keywordDir: dir, activeKeywordDir: sortKey});

    event.preventDefault();
  }


  getKeywordActiveDir(sortKey) {
     return this.state.activeKeywordDir == sortKey ? this.state.keywordDir[sortKey] == 'asc' ? '▼' : '▲' : '';
  }

  selectKeyword(e) {
    this.props.onClearProduct();
    this.props.onSelectKeyword(e.target.dataset.id);

    this.setState({ key: 4, tab3_title: 'Keyword: ' + e.target.dataset.name, tab4_title: 'Products for: ' + e.target.dataset.name, tab5_title: 'Searchterms for: ' + e.target.dataset.name, keywordId: e.target.dataset.id });

    e.preventDefault();    
  }

  searchKeyword(e) {
    if (e.keyCode === ENTER_KEY) {
        var filterState = formateParamsDate(this.props.filterState);
        filterState.keywordQuery = e.target.value;
        filterState.keywordState = this.state.keywordState;
        filterState.keywordMatchType = this.state.keywordMatchType;

        axios.get(fetchkeywords_url, {
          params: filterState
        })
        .then(res => {

          this.setState({
            keywords: res.data,
            origin_keywords: res.data,
          })

        })     

        this.setState({keywordQuery: e.target.value});
    }
  }

  checkAllKeyword(e) {
    const keywords = this.state.keywords;
    for (var i = 0; i < keywords.length; i++) {
      keywords[i]['checked'] = e.target.checked;
    }
    this.setState({keywords: keywords, origin_keywords: keywords});
  }

  checkKeyword(e) {
    var index = e.target.dataset.index;
    const keywords = this.state.keywords;

    keywords[index]['checked'] = e.target.checked;
    this.setState({keywords: keywords, origin_keywords: keywords});
  }

  viewKeywordSearchterms(e) {
    this.props.onClearProduct();
    this.props.onSelectKeyword(e.target.dataset.id);    
    this.setState({ key: 5, tab4_title: 'Products for: ' + e.target.dataset.name, tab5_title: 'Searchterms for: ' + e.target.dataset.name, keywordId: e.target.dataset.id });
    e.preventDefault();       
  }


  filterKeywordState(e) {
        var filterState = formateParamsDate(this.props.filterState);
        filterState.keywordQuery = this.state.keywordQuery;
        filterState.keywordState = e.target.value;
        filterState.keywordMatchType = this.state.keywordMatchType;
        
        axios.get(fetchkeywords_url, {
          params: filterState
        })
        .then(res => {

          this.setState({
            keywords: res.data,
            origin_keywords: res.data,
          })

        })    

        this.setState({keywordState: e.target.value});  
  }

  filterKeywordMatchType(e) {
        var filterState = formateParamsDate(this.props.filterState);
        filterState.keywordQuery = this.state.keywordQuery;
        filterState.keywordState = this.state.keywordState;
        filterState.keywordMatchType = e.target.value;
        
        axios.get(fetchkeywords_url, {
          params: filterState
        })
        .then(res => {

          this.setState({
            keywords: res.data,
            origin_keywords: res.data,
          })

        })    

        this.setState({keywordMatchType: e.target.value});      
  }


  sortProduct(event, sortKey, currentDir) {
    const products = this.state.products;
    const dir = this.state.productDir;
    if(currentDir == 'desc') {
      products.sort((a,b) => (parseFloat(a[sortKey]) > parseFloat(b[sortKey])) ? -1 : 1);
      dir[sortKey] = 'asc';
    } else {
      products.sort((a,b) => (parseFloat(a[sortKey]) > parseFloat(b[sortKey])) ? 1 : -1);
      dir[sortKey] = 'desc';
    }

    this.setState({products: products, productDir: dir, activeProductDir: sortKey});

    event.preventDefault();
  }

  searchProduct(e) {
    if (e.keyCode === ENTER_KEY) {
      var products = this.state.origin_products;
      var new_products = products.filter(function(product) {
        var name = product.name;
        return name && name.toLowerCase().includes(e.target.value.toLowerCase())
      });
      this.setState({products: new_products})
    }
  }

  searchProductManager(e) {
    if (e.keyCode === ENTER_KEY) {
      var products = this.state.origin_products
      var term = e.target.value;

      if(term != '') {
        var filtered_products = products.filter(function(product) {
          return product.product_manager && product.product_manager.toLowerCase().includes(term.toLowerCase()) 
        });   
        
        this.setState({products: filtered_products})   
      } else {
        this.setState({products: products})
      }
    }

  }

  getProductActiveDir(sortKey) {
     return this.state.activeProductDir == sortKey ? this.state.productDir[sortKey] == 'asc' ? '▼' : '▲' : '';
  }


  selectProduct(e) {
    this.setState({ key: 5, tab5_title: 'Searchterms for: ' + e.target.dataset.asin, asin: e.target.dataset.asin, country: e.target.dataset.country });

    this.props.onClearKeyword();
    this.props.onSelectProduct(e.target.dataset.asin, e.target.dataset.country);

    e.preventDefault();
  }


  sortSearchterm(event, sortKey, currentDir) {
    const searchterms = this.state.searchterms;
    const dir = this.state.searchtermDir;
    if(currentDir == 'desc') {
      searchterms.sort((a,b) => (parseFloat(a[sortKey]) > parseFloat(b[sortKey])) ? -1 : 1);
      dir[sortKey] = 'asc';
    } else {
      searchterms.sort((a,b) => (parseFloat(a[sortKey]) > parseFloat(b[sortKey])) ? 1 : -1);
      dir[sortKey] = 'desc';
    }

    this.setState({searchterms: searchterms, searchtermDir: dir, activeSearchtermDir: sortKey});

    event.preventDefault();
  }


  getSearchtermActiveDir(sortKey) {
     return this.state.activeSearchtermDir == sortKey ? this.state.searchtermDir[sortKey] == 'asc' ? '▼' : '▲' : '';
  }

  getTotalBudget() {
    var sum = 0;
    this.state.campaigns.map((campaign, index) => ( sum += parseFloat(campaign.dailyBudget)))
    return sum
  }

  getTotalImpressions(type) {
    var sum = 0;
    var objects = this.getObjectsByType(type);
    objects.map((obj, index) => ( sum += parseFloat(obj.total_impressions)))
    return sum
  }

  getTotalClicks(type) {
    var sum = 0;
    var objects = this.getObjectsByType(type);
    objects.map((obj, index) => ( sum += parseFloat(obj.total_clicks)))
    return sum
  }

  getTotalCTR(type) {
    return (this.getTotalClicks(type) / this.getTotalImpressions(type) * 100).toFixed(2)
  }

  getTotalCost(type) {
    var sum = 0;
    var objects = this.getObjectsByType(type);
    objects.map((obj, index) => ( sum += parseFloat(obj.total_cost)))
    return sum.toFixed(2)
  }

  getTotalCPC(type) {
    return (this.getTotalCost(type) / this.getTotalClicks(type)).toFixed(2)
  }

  getTotalSales(type) {
    var sum = 0;
    var objects = this.getObjectsByType(type);
    objects.map((obj, index) => ( sum += parseFloat(obj.total_sales)))
    return sum.toFixed(2)
  }

  getTotalACoS(type) {
    return (this.getTotalCost(type) / this.getTotalSales(type) * 100).toFixed(2)
  }

  getTotalOrders(type) {
    var sum = 0;
    var objects = this.getObjectsByType(type);
    objects.map((obj, index) => ( sum += parseFloat(obj.total_orders)))
    return sum
  }


  getTotalProfit() {
    var sum = 0;
    this.state.products.map((product, index) => ( sum += parseFloat(product.profit)))
    return sum.toFixed(2)   
  }

  getTotalCR(type) {
    return (this.getTotalOrders(type) / this.getTotalClicks(type) * 100).toFixed(2)
  }

  getObjectsByType(type) {
    var objects = [];
    switch(type) {
      case 'campaigns':
        objects = this.state.campaigns;
        break;
      case 'adgroups':
        objects = this.state.adgroups;
        break;
      case 'keywords':
        objects = this.state.keywords;
        break;
      case 'products':
        objects = this.state.products;
        break;  
      case 'searchterms':
        objects = this.state.searchterms;
        break; 
    }

    return objects;
  }


  rangeFilter(e, type, term, order) {
    if (e.keyCode === ENTER_KEY) {

      this.rangeFilterExe(e, type, term, order);

    }

  }

  rangeFilterExe(e, type, term, order) {

    var mins = this.state.mins;
    var maxs = this.state.maxs;

    switch(type) {
      case 'campaigns':
        var campaigns = this.state.origin_campaigns;

        if(order == 'min') {
          mins['campaigns'][term] = Number(e.target.value);
        } else if(order == 'max') {
          maxs['campaigns'][term] = Number(e.target.value);
        }

        for (var subterm in mins['campaigns']) {
            campaigns = campaigns.filter(campaign => parseFloat(campaign[subterm]) >= parseFloat(mins['campaigns'][subterm]));
        }

        for (var subterm in maxs['campaigns']) {
          if(maxs['campaigns'][subterm] > 0) {
            campaigns = campaigns.filter(campaign => parseFloat(campaign[subterm]) <= parseFloat(maxs['campaigns'][subterm]));
          }
        }
        
        
        this.setState({mins: mins, maxs: maxs, campaigns: campaigns});
        break;
      case 'adgroups':
        var adgroups = this.state.origin_adgroups;

        if(order == 'min') {
          mins['adgroups'][term] = Number(e.target.value);
        } else if(order == 'max') {
          maxs['adgroups'][term] = Number(e.target.value);
        }

        for (var subterm in mins['adgroups']) {
            adgroups = adgroups.filter(adgroup => parseFloat(adgroup[subterm]) >= parseFloat(mins['adgroups'][subterm]));
        }

        for (var subterm in maxs['adgroups']) {
          if(maxs['adgroups'][subterm] > 0) {
            adgroups = adgroups.filter(adgroup => parseFloat(adgroup[subterm]) <= parseFloat(maxs['adgroups'][subterm]));
          }
        }
        
        this.setState({mins: mins, maxs: maxs, adgroups: adgroups});
        break;
      case 'keywords':
        var keywords = this.state.origin_keywords;

        if(order == 'min') {
          mins['keywords'][term] = Number(e.target.value);
        } else if(order == 'max') {
          maxs['keywords'][term] = Number(e.target.value);
        }

        for (var subterm in mins['keywords']) {
            keywords = keywords.filter(keyword => parseFloat(keyword[subterm]) >= parseFloat(mins['keywords'][subterm]));
        }

        for (var subterm in maxs['keywords']) {
          if(maxs['keywords'][subterm] > 0) {
            keywords = keywords.filter(keyword => parseFloat(keyword[subterm]) <= parseFloat(maxs['keywords'][subterm]));
          }
        } 
        
        this.setState({mins: mins, maxs: maxs, keywords: keywords});
        break;
      case 'products':
        var products = this.state.origin_products;

        if(order == 'min') {
          mins['products'][term] = Number(e.target.value);
        } else if(order == 'max') {
          maxs['products'][term] = Number(e.target.value);
        }

        for (var subterm in mins['products']) {
            products = products.filter(product => parseFloat(product[subterm]) >= parseFloat(mins['products'][subterm]));
        }

        for (var subterm in maxs['products']) {
          if(maxs['products'][subterm] > 0) {
            products = products.filter(product => parseFloat(product[subterm]) <= parseFloat(maxs['products'][subterm]));
          }
        }   
        
        this.setState({mins: mins, maxs: maxs, products: products});
        break;
      case 'searchterms':
        var searchterms = this.state.origin_searchterms;

        if(order == 'min') {
          mins['searchterms'][term] = Number(e.target.value);
        } else if(order == 'max') {
          maxs['searchterms'][term] = Number(e.target.value);
        }

        for (var subterm in mins['searchterms']) {
            searchterms = searchterms.filter(searchterm => parseFloat(searchterm[subterm]) >= parseFloat(mins['searchterms'][subterm]));
        }

        for (var subterm in maxs['searchterms']) {
          if(maxs['searchterms'][subterm] > 0) {
            searchterms = searchterms.filter(searchterm => parseFloat(searchterm[subterm]) <= parseFloat(maxs['searchterms'][subterm]));
          }
        }
        
        this.setState({mins: mins, maxs: maxs, searchterms: searchterms});
        break;
    }


    e.preventDefault();
  }


  render() {

    let EditableSpan = contentEditable('span');

  	return (

  	<Tabs activeKey={this.state.key} onSelect={this.handleSelect} id="controlled-tab">

  		<Tab eventKey={1} title={this.state.tab1_title}>
			<Table>
			  <thead>
			    <tr>
			      <th width="100px">State</th>
            <th width="120px">Status</th>
            <th><a href="#" onClick={e => this.sortCampaign(e, 'account', this.state.campaignDir.account)}>Seller Account { this.getCampaignActiveDir('account') }</a></th>
			      <th width="300px"><a href="#" onClick={e => this.sortCampaign(e, 'name', this.state.campaignDir.name)}>Name { this.getCampaignActiveDir('name') }</a></th>
            <th>Manager</th>
            <th><a href="#" onClick={e => this.sortCampaign(e, 'targetingType', this.state.campaignDir.targetingType)}>Targeting Type { this.getCampaignActiveDir('targetingType') }</a></th>
            <th><a href="#" onClick={e => this.sortCampaign(e, 'start_date', this.state.campaignDir.start_date)}>Start date { this.getCampaignActiveDir('start_date') }</a></th>
            <th><a href="#" onClick={e => this.sortCampaign(e, 'dailyBudget', this.state.campaignDir.dailyBudget)}>Daily Budget { this.getCampaignActiveDir('dailyBudget') }</a></th>
            <th><a href="#" onClick={e => this.sortCampaign(e, 'total_impressions', this.state.campaignDir.total_impressions)}>Impr. { this.getCampaignActiveDir('total_impressions') }</a></th>
            <th><a href="#" onClick={e => this.sortCampaign(e, 'total_clicks', this.state.campaignDir.total_clicks)}>Clicks { this.getCampaignActiveDir('total_clicks') }</a></th>
            <th><a href="#" onClick={e => this.sortCampaign(e, 'ctr', this.state.campaignDir.ctr)}>CTR { this.getCampaignActiveDir('ctr') }</a></th>
			      <th><a href="#" onClick={e => this.sortCampaign(e, 'total_cost', this.state.campaignDir.total_cost)}>Ad Spend { this.getCampaignActiveDir('total_cost') }</a></th>
            <th><a href="#" onClick={e => this.sortCampaign(e, 'cpc', this.state.campaignDir.cpc)}>CPC { this.getCampaignActiveDir('cpc') }</a></th>
			      <th><a href="#" onClick={e => this.sortCampaign(e, 'total_sales', this.state.campaignDir.total_sales)}>Sales { this.getCampaignActiveDir('total_sales') }</a></th>
            <th><a href="#" onClick={e => this.sortCampaign(e, 'acos', this.state.campaignDir.acos)}>ACoS { this.getCampaignActiveDir('acos') }</a></th>
            <th><a href="#" onClick={e => this.sortCampaign(e, 'total_orders', this.state.campaignDir.total_orders)}>Orders { this.getCampaignActiveDir('total_orders') }</a></th>
            <th><a href="#" onClick={e => this.sortCampaign(e, 'cr', this.state.campaignDir.cr)}>CR { this.getCampaignActiveDir('cr') }</a></th>
            
			    </tr>
          <tr>
            <td><select name="state_filter" className="form-control" onChange={this.filterCampaignState}><option value="all">All</option><option value="enabled">Enabled</option><option value="paused">Paused</option><option value="archived">Archived</option></select></td>
            <td><select name="status_filter" className="form-control" onChange={this.filterCampaignStatus}><option value="all">All</option><option value="CAMPAIGN_STATUS_ENABLED">Delivering</option><option value="CAMPAIGN_PAUSED">Paused</option><option value="CAMPAIGN_OUT_OF_BUDGET">Out of budget</option><option value="CAMPAIGN_ARCHIVED">Archived</option></select></td>
            <td></td>
            <td><input type="text" className="form-control" name="name_filter" onKeyDown={this.searchCampaign} /></td>
            <td><input type="text" className="form-control" name="manager_filter" onKeyDown={this.searchCampaignProductManager} /></td>
            <td><select name="targeting_type_filter" className="form-control" onChange={this.filterCampaignTargetingType}><option value="all">All</option><option value="auto">Auto</option><option value="manual">Manual</option></select></td>
            <td></td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'dailyBudget', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'dailyBudget', 'max')} placeholder="max" /><br />${this.getTotalBudget()}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'total_impressions', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'total_impressions', 'max')} placeholder="max" /><br />{this.getTotalImpressions('campaigns')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'total_clicks', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'total_clicks', 'max')} placeholder="max" /><br />{this.getTotalClicks('campaigns')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'ctr', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'ctr', 'max')} placeholder="max" /><br />{this.getTotalCTR('campaigns')}%</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'total_cost', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'total_cost', 'max')} placeholder="max" /><br />${this.getTotalCost('campaigns')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'cpc', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'cpc', 'max')} placeholder="max" /><br />${this.getTotalCPC('campaigns')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'total_sales', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'total_sales', 'max')} placeholder="max" /><br />${this.getTotalSales('campaigns')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'acos', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'acos', 'max')} placeholder="max" /><br />{this.getTotalACoS('campaigns')}%</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'total_orders', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'total_orders', 'max')} placeholder="max" /><br />{this.getTotalOrders('campaigns')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'cr', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'campaigns', 'cr', 'max')} placeholder="max" /><br />{this.getTotalCR('campaigns')}%</td>
          </tr>
			  </thead>
			  <tbody>
			    {this.state.campaigns.map((campaign, index) => (
			    <tr key={index}>
			      <td>
              <span data-index={index} data-id={campaign.campaignId} className={this.state.campaigns[index].state == 'enabled' ? 'rc-switch rc-switch-checked' : 'rc-switch'} onClick={this.handleCampaignStateChange}>
                <span className="rc-switch-inner"></span>
              </span>
            </td>
            <td><span className={this.servingStatusClassName[campaign.servingStatus]}>{this.servingStatus[campaign.servingStatus]}</span></td>
            <td>{campaign.account}</td>
			    	<td><a href="#" data-id={campaign.campaignId} data-name={campaign.name} onClick={this.selectCampaign}>{campaign.name}</a></td>
            <td>{campaign.product_manager}</td>
            <td>{campaign.targetingType}</td>
            <td>{campaign.start_date}</td>
            <td>$<EditableSpan value={campaign.dailyBudget} onSave={this.updateCampaignDailyBudget} campaignId={campaign.campaignId} originBudget={campaign.dailyBudget} /></td>
            <td>{campaign.total_impressions}</td>
            <td>{campaign.total_clicks}</td>
            <td>{campaign.ctr}%</td>
            <td>${campaign.total_cost}</td>
            <td>${campaign.cpc}</td>
            <td>${campaign.total_sales}</td>
            <td>{campaign.acos}%</td>
			    	<td>{campaign.total_orders}</td>
            <td>{campaign.cr}%</td>
			    	
			    </tr>
			    ))}
			  </tbody>
			</Table>
		</Tab>

		<Tab eventKey={2} title={this.state.tab2_title}>
      <Table>
        <thead>
          <tr>
            <th width="100px">State</th>
            <th>Name</th>
            <th><a href="#" onClick={e => this.sortAdGroup(e, 'total_impressions', this.state.adGroupDir.total_impressions)}>Impr. { this.getAdGroupActiveDir('total_impressions') }</a></th>
            <th><a href="#" onClick={e => this.sortAdGroup(e, 'total_clicks', this.state.adGroupDir.total_clicks)}>Clicks { this.getAdGroupActiveDir('total_clicks') }</a></th>
            <th><a href="#" onClick={e => this.sortAdGroup(e, 'ctr', this.state.adGroupDir.ctr)}>CTR { this.getAdGroupActiveDir('ctr') }</a></th>
            <th><a href="#" onClick={e => this.sortAdGroup(e, 'total_cost', this.state.adGroupDir.total_cost)}>Ad Spend { this.getAdGroupActiveDir('total_cost') }</a></th>
            <th><a href="#" onClick={e => this.sortAdGroup(e, 'cpc', this.state.adGroupDir.cpc)}>CPC { this.getAdGroupActiveDir('cpc') }</a></th>
            <th><a href="#" onClick={e => this.sortAdGroup(e, 'total_sales', this.state.adGroupDir.total_sales)}>Sales { this.getAdGroupActiveDir('total_sales') }</a></th>
            <th><a href="#" onClick={e => this.sortAdGroup(e, 'acos', this.state.adGroupDir.acos)}>ACoS { this.getAdGroupActiveDir('acos') }</a></th>
            <th><a href="#" onClick={e => this.sortAdGroup(e, 'total_orders', this.state.adGroupDir.total_orders)}>Orders { this.getAdGroupActiveDir('total_orders') }</a></th>
            <th><a href="#" onClick={e => this.sortAdGroup(e, 'cr', this.state.adGroupDir.cr)}>CR { this.getAdGroupActiveDir('cr') }</a></th>
            <th><a href="#" onClick={e => this.sortAdGroup(e, 'defaultBid', this.state.adGroupDir.defaultBid)}>Default Bid { this.getAdGroupActiveDir('defaultBid') }</a></th>
          </tr>
          <tr>
            <td><select name="state_filter" className="form-control" onChange={this.filterAdGroupState}><option value="all">All</option><option value="enabled">Enabled</option><option value="paused">Paused</option><option value="archived">Archived</option></select></td>
            <td></td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'total_impressions', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'total_impressions', 'max')} placeholder="max" /><br />{this.getTotalImpressions('adgroups')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'total_clicks', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'total_clicks', 'max')} placeholder="max" /><br />{this.getTotalClicks('adgroups')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'ctr', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'ctr', 'max')} placeholder="max" /><br />{this.getTotalCTR('adgroups')}%</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'total_cost', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'total_cost', 'max')} placeholder="max" /><br />${this.getTotalCost('adgroups')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'cpc', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'cpc', 'max')} placeholder="max" /><br />${this.getTotalCPC('adgroups')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'total_sales', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'total_sales', 'max')} placeholder="max" /><br />${this.getTotalSales('adgroups')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'acos', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'acos', 'max')} placeholder="max" /><br />{this.getTotalACoS('adgroups')}%</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'total_orders', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'total_orders', 'max')} placeholder="max" /><br />{this.getTotalOrders('adgroups')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'cr', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'cr', 'max')} placeholder="max" /><br />{this.getTotalCR('adgroups')}%</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'defaultBid', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'adgroups', 'defaultBid', 'max')} placeholder="max" /><br /></td>
          </tr>
        </thead>
        <tbody>
          {this.state.adgroups.map((adgroup, index) => (
          <tr key={index}>
            <td>{adgroup.state}</td>
            <td><a href="#" data-id={adgroup.adGroupId} data-name={adgroup.name} onClick={this.selectAdGroup}>{adgroup.name}</a></td>
            <td>{adgroup.total_impressions}</td>
            <td>{adgroup.total_clicks}</td>
            <td>{adgroup.ctr}%</td>
            <td>${adgroup.total_cost}</td>
            <td>${adgroup.cpc}</td>
            <td>${adgroup.total_sales}</td>
            <td>{adgroup.acos}%</td>
            <td>{adgroup.total_orders}</td>
            <td>{adgroup.cr}%</td>
            <td>
              $<EditableSpan value={adgroup.defaultBid} onSave={this.updateAdGroupDefaultBid} adGroupId={adgroup.adGroupId} originBid={adgroup.defaultBid} />
            </td>
          </tr>
          ))}
        </tbody>
      </Table>
		</Tab>

    <Tab eventKey={3} title={this.state.tab3_title}>
      <Table>
        <thead>
          <tr>
            <th width="40px"></th>
            <th width="100px">State</th>
            <th>Keyword</th>
            <th>Searchterms</th>
            <th>Match Type</th>
            <th><a href="#" onClick={e => this.sortKeyword(e, 'total_impressions', this.state.keywordDir.total_impressions)}>Impr. { this.getKeywordActiveDir('total_impressions') }</a></th>
            <th><a href="#" onClick={e => this.sortKeyword(e, 'total_clicks', this.state.keywordDir.total_clicks)}>Clicks { this.getKeywordActiveDir('total_clicks') }</a></th>
            <th><a href="#" onClick={e => this.sortKeyword(e, 'ctr', this.state.keywordDir.ctr)}>CTR { this.getKeywordActiveDir('ctr') }</a></th>
            <th><a href="#" onClick={e => this.sortKeyword(e, 'total_cost', this.state.keywordDir.total_cost)}>Ad Spend { this.getKeywordActiveDir('total_cost') }</a></th>
            <th><a href="#" onClick={e => this.sortKeyword(e, 'cpc', this.state.keywordDir.cpc)}>CPC { this.getKeywordActiveDir('cpc') }</a></th>
            <th><a href="#" onClick={e => this.sortKeyword(e, 'total_sales', this.state.adGrokeywordDirupDir.total_sales)}>Sales { this.getKeywordActiveDir('total_sales') }</a></th>
            <th><a href="#" onClick={e => this.sortKeyword(e, 'acos', this.state.keywordDir.acos)}>ACoS { this.getKeywordActiveDir('acos') }</a></th>
            <th><a href="#" onClick={e => this.sortKeyword(e, 'total_orders', this.state.keywordDir.total_orders)}>Orders { this.getKeywordActiveDir('total_orders') }</a></th>
            <th><a href="#" onClick={e => this.sortKeyword(e, 'cr', this.state.keywordDir.cr)}>CR { this.getKeywordActiveDir('cr') }</a></th>
            <th><a href="#" onClick={e => this.sortKeyword(e, 'bid', this.state.keywordDir.bid)}>Bid { this.getKeywordActiveDir('bid') }</a></th>
          </tr>
          <tr>
            <td>
            <input type="checkbox" className="checkbox-control" onClick={this.checkAllKeyword} />
            </td>
            <td>
            <select name="state_filter" className="form-control" onChange={this.filterKeywordState}><option value="all">All</option><option value="enabled">Enabled</option><option value="paused">Paused</option><option value="archived">Archived</option></select>
            </td>
            <td><input type="text" className="form-control" name="name_filter" onKeyDown={this.searchKeyword} /></td>
            <td></td>
            <td><select name="match_type_filter" className="form-control" onChange={this.filterKeywordMatchType}><option value="all">All</option><option value="exact">Exact</option><option value="broad">Broad</option><option value="phrase">Phrase</option><option value="negativeExact">negativeExact</option><option value="negativePhrase">negativePhrase</option></select></td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'total_impressions', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'total_impressions', 'max')} placeholder="max" /><br />{this.getTotalImpressions('keywords')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'total_clicks', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'total_clicks', 'max')} placeholder="max" /><br />{this.getTotalClicks('keywords')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'ctr', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'ctr', 'max')} placeholder="max" /><br />{this.getTotalCTR('keywords')}%</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'total_cost', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'total_cost', 'max')} placeholder="max" /><br />${this.getTotalCost('keywords')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'cpc', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'cpc', 'max')} placeholder="max" /><br />${this.getTotalCPC('keywords')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'total_sales', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'total_sales', 'max')} placeholder="max" /><br />${this.getTotalSales('keywords')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'acos', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'acos', 'max')} placeholder="max" /><br />{this.getTotalACoS('keywords')}%</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'total_orders', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'total_orders', 'max')} placeholder="max" /><br />{this.getTotalOrders('keywords')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'cr', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'cr', 'max')} placeholder="max" /><br />{this.getTotalCR('keywords')}%</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'bid', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'keywords', 'bid', 'max')} placeholder="max" /><br />
            { (this.state.campaignId || this.state.adGroupId) && 
              <input type="text" className="bid-adjust" placeholder="New Bid" onChange={ this.handleKeywordGroupBidChange } />
            }
            { (this.state.campaignId || this.state.adGroupId) && 
            <input type="button" value="Submit" onClick={ this.updateKeywordGroupBid } /> 
            }
            </td>
          </tr>
        </thead>
        <tbody>
          {this.state.keywords.map((keyword, index) => (
          <tr key={index}>
            <td><input type="checkbox" data-index={index} data-id={keyword.keywordId} className="checkbox-control" onClick={this.checkKeyword} checked={this.state.keywords[index].checked ? true : false} /></td>
            <td>
              <span data-index={index} data-id={keyword.keywordId} className={this.state.keywords[index].state == 'enabled' ? 'rc-switch rc-switch-checked' : 'rc-switch'} onClick={this.handleKeywordStateChange}>
                <span className="rc-switch-inner"></span>
              </span>
            </td>
            <td><a href="#" data-id={keyword.keywordId} data-name={keyword.keywordText} onClick={this.selectKeyword}>{keyword.keywordText}</a></td>
            <td><a href="#" data-id={keyword.keywordId} data-name={keyword.keywordText} onClick={this.viewKeywordSearchterms}>View Searchterms</a></td>
            <td>{keyword.matchType}</td>
            <td>{keyword.total_impressions}</td>
            <td>{keyword.total_clicks}</td>
            <td>{keyword.ctr}%</td>
            <td>${keyword.total_cost}</td>
            <td>${keyword.cpc}</td>
            <td>${keyword.total_sales}</td>
            <td>{keyword.acos}%</td>
            <td>{keyword.total_orders}</td>
            <td>{keyword.cr}%</td>
            <td>
               $<EditableSpan value={keyword.bid > 0 ? keyword.bid : keyword.defaultBid} onSave={this.updateKeywordBid} keywordId={keyword.keywordId} originBid={keyword.bid > 0 ? keyword.bid : keyword.defaultBid} editable={keyword.keywordText != '(_targeting_auto_)'}/>
            </td>
          </tr>
          ))}
        </tbody>
      </Table>
    </Tab>

    <Tab eventKey={4} title={this.state.tab4_title}>
      <Table>
        <thead>
          <tr>
            <th width='210px'>SKU/ASIN</th>
            <th width='280px'>Name</th>
            <th width="120px">Manager</th>
            <th width='50px'>Searchterms</th>
            <th><a href="#" onClick={e => this.sortProduct(e, 'profit', this.state.productDir.profit)}>Profit { this.getProductActiveDir('profit') }</a></th>
            <th><a href="#" onClick={e => this.sortProduct(e, 'total_impressions', this.state.productDir.total_impressions)}>Impr. { this.getProductActiveDir('total_impressions') }</a></th>
            <th><a href="#" onClick={e => this.sortProduct(e, 'total_clicks', this.state.productDir.total_clicks)}>Clicks { this.getProductActiveDir('total_clicks') }</a></th>
            <th><a href="#" onClick={e => this.sortProduct(e, 'ctr', this.state.productDir.ctr)}>CTR { this.getProductActiveDir('ctr') }</a></th>
            <th><a href="#" onClick={e => this.sortProduct(e, 'total_cost', this.state.productDir.total_cost)}>Ad Spend { this.getProductActiveDir('total_cost') }</a></th>
            <th><a href="#" onClick={e => this.sortProduct(e, 'cpc', this.state.productDir.cpc)}>CPC { this.getProductActiveDir('cpc') }</a></th>
            <th><a href="#" onClick={e => this.sortProduct(e, 'total_sales', this.state.productDir.total_sales)}>Sales { this.getProductActiveDir('total_sales') }</a></th>
            <th><a href="#" onClick={e => this.sortProduct(e, 'acos', this.state.productDir.acos)}>ACoS { this.getProductActiveDir('acos') }</a></th>
            <th><a href="#" onClick={e => this.sortProduct(e, 'total_orders', this.state.productDir.total_orders)}>Orders { this.getProductActiveDir('total_orders') }</a></th>
            <th><a href="#" onClick={e => this.sortProduct(e, 'cr', this.state.productDir.cr)}>CR { this.getProductActiveDir('cr') }</a></th>
          </tr>
          <tr>
            <td></td>
            <td><input type="text" className="form-control" name="name_filter" onKeyDown={this.searchProduct} /></td>
            <td><input type="text" className="form-control" name="manager_filter" onKeyDown={this.searchProductManager} /></td>
            <td></td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'profit', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'profit', 'max')} placeholder="max" /><br />{this.getTotalProfit()}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'total_impressions', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'total_impressions', 'max')} placeholder="max" /><br />{this.getTotalImpressions('products')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'total_clicks', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'total_clicks', 'max')} placeholder="max" /><br />{this.getTotalClicks('products')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'ctr', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'ctr', 'max')} placeholder="max" /><br />{this.getTotalCTR('products')}%</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'total_cost', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'total_cost', 'max')} placeholder="max" /><br />${this.getTotalCost('products')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'cpc', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'cpc', 'max')} placeholder="max" /><br />${this.getTotalCPC('products')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'total_sales', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'total_sales', 'max')} placeholder="max" /><br />${this.getTotalSales('products')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'acos', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'acos', 'max')} placeholder="max" /><br />{this.getTotalACoS('products')}%</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'total_orders', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'total_orders', 'max')} placeholder="max" /><br />{this.getTotalOrders('products')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'cr', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'products', 'cr', 'max')} placeholder="max" /><br />{this.getTotalCR('products')}%</td>
          </tr>
        </thead>
        <tbody>
          {this.state.products.map((product, index) => (
          <tr key={index}>
            <td><div className='adImage'><a href={product.amazon_link} target='_blank'><img src={product.small_image} /></a></div>
            <div className='adASIN'><a href={product.product_link} target='_blank'>{product.asin}</a><br/>{product.country}</div></td>
            <td>{product.name}</td>
            <td>{product.product_manager}</td>
            <td><a href="#" data-asin={product.asin} data-country={product.country} onClick={this.selectProduct}>View Searchterms</a></td>
            <td>${product.profit}</td>
            <td>{product.total_impressions}</td>
            <td>{product.total_clicks}</td>
            <td>{product.ctr}%</td>
            <td>${product.total_cost}</td>
            <td>${product.cpc}</td>
            <td>${product.total_sales}</td>
            <td>{product.acos}%</td>
            <td>{product.total_orders}</td>
            <td>{product.cr}%</td>
          </tr>
          ))}
        </tbody>
      </Table>
    </Tab>

    <Tab eventKey={5} title={this.state.tab5_title}>
      <Table>
        <thead>
          <tr>
            <th width='280px'>Searchterms</th>
            <th><a href="#" onClick={e => this.sortSearchterm(e, 'total_impressions', this.state.searchtermDir.total_impressions)}>Impr. { this.getSearchtermActiveDir('total_impressions') }</a></th>
            <th><a href="#" onClick={e => this.sortSearchterm(e, 'total_clicks', this.state.searchtermDir.total_clicks)}>Clicks { this.getSearchtermActiveDir('total_clicks') }</a></th>
            <th><a href="#" onClick={e => this.sortSearchterm(e, 'ctr', this.state.searchtermDir.ctr)}>CTR { this.getSearchtermActiveDir('ctr') }</a></th>
            <th><a href="#" onClick={e => this.sortSearchterm(e, 'total_cost', this.state.searchtermDir.total_cost)}>Ad Spend { this.getSearchtermActiveDir('total_cost') }</a></th>
            <th><a href="#" onClick={e => this.sortSearchterm(e, 'cpc', this.state.searchtermDir.cpc)}>CPC { this.getSearchtermActiveDir('cpc') }</a></th>
            <th><a href="#" onClick={e => this.sortSearchterm(e, 'total_sales', this.state.searchtermDir.total_sales)}>Sales { this.getSearchtermActiveDir('total_sales') }</a></th>
            <th><a href="#" onClick={e => this.sortSearchterm(e, 'acos', this.state.searchtermDir.acos)}>ACoS { this.getSearchtermActiveDir('acos') }</a></th>
            <th><a href="#" onClick={e => this.sortSearchterm(e, 'total_orders', this.state.searchtermDir.total_orders)}>Orders { this.getSearchtermActiveDir('total_orders') }</a></th>
            <th><a href="#" onClick={e => this.sortSearchterm(e, 'cr', this.state.searchtermDir.cr)}>CR { this.getSearchtermActiveDir('cr') }</a></th>
          </tr>
          <tr>
            <td></td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'total_impressions', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'total_impressions', 'max')} placeholder="max" /><br />{this.getTotalImpressions('searchterms')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'total_clicks', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'total_clicks', 'max')} placeholder="max" /><br />{this.getTotalClicks('searchterms')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'ctr', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'ctr', 'max')} placeholder="max" /><br />{this.getTotalCTR('searchterms')}%</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'total_cost', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'total_cost', 'max')} placeholder="max" /><br />${this.getTotalCost('searchterms')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'cpc', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'cpc', 'max')} placeholder="max" /><br />${this.getTotalCPC('searchterms')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'total_sales', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'total_sales', 'max')} placeholder="max" /><br />${this.getTotalSales('searchterms')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'acos', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'acos', 'max')} placeholder="max" /><br />{this.getTotalACoS('searchterms')}%</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'total_orders', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'total_orders', 'max')} placeholder="max" /><br />{this.getTotalOrders('searchterms')}</td>
            <td><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'cr', 'min')} placeholder="min" /><br /><input type="text" className="range-filter" onKeyDown={e => this.rangeFilter(e, 'searchterms', 'cr', 'max')} placeholder="max" /><br />{this.getTotalCR('searchterms')}%</td>
          </tr>
        </thead>
        <tbody>
          {this.state.searchterms.map((searchterm, index) => (
          <tr key={index}>
            <td><a href={"https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords="+searchterm.query} target="_blank">{searchterm.query}</a></td>
            <td>{searchterm.total_impressions}</td>
            <td>{searchterm.total_clicks}</td>
            <td>{searchterm.ctr}%</td>
            <td>${searchterm.total_cost}</td>
            <td>${searchterm.cpc}</td>
            <td>${searchterm.total_sales}</td>
            <td>{searchterm.acos}%</td>
            <td>{searchterm.total_orders}</td>
            <td>{searchterm.cr}%</td>
          </tr>
          ))}
        </tbody>
      </Table>
    </Tab>

	</Tabs>

  	);
  }

}


export default CampaignList;