import React, {Component} from "react";
import ReactDOM from "react-dom";

export default class CampaignStateFilter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: ''
        };

        this.onChange = this.onChange.bind(this);
    }

    isFilterActive() {
        return this.state.text !== null && this.state.text !== undefined && this.state.text !== '';
    }

    onChange(event) {
        let newValue = event.target.value;
        if (this.state.text !== newValue) {
            this.setState({
                text: newValue
            }, () => {
                this.props.onFloatingFilterChanged({model: this.buildModel()});
            });

        }
    }

    buildModel() {
        if (!this.isFilterActive()) {
            return null;
        }
        return {
            filterType: 'text',
            type: 'equals',
            filter: this.state.text,

        };
    }

    onParentModelChanged(parentModel) {
        // note that the filter could be anything here, but our purposes we're assuming a greater than filter only,
        // so just read off the value and use that
        this.setState({
            text: !parentModel ? 0 : parentModel.filter
        });
    }

    render() {
        return (
            <select name="state_filter" onChange={this.onChange} style={{width: '80px'}}>
                <option value="">All</option>
                <option value="enabled">Enabled</option>
                <option value="paused">Paused</option>
                <option value="archived">Archived</option>
            </select>
        );
    }
};