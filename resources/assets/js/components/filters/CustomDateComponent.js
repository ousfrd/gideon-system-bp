import React, {Component} from 'react';

export default class CustomDateComponent extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            date: null
        }
    }

    render() {
        return (
            <div className="ag-input-text-wrapper custom-date-filter fa" ref="flatpickr">
                <input type='text' data-input />
                <a className='input-button' title='clear' data-clear>
                    <i className='fa fa-times'></i>
                </a>
            </div>
        );
    }

    componentDidMount() {
        this.picker = flatpickr(this.refs.flatpickr, {
            onChange: this.onDateChanged.bind(this),
            dateFormat: 'd/m/Y',
            wrap: true
        });

        this.picker.calendarContainer.classList.add('ag-custom-component-popup');
    }

    getDate() {
        return this.state.date;
    }

    setDate(date) {
        this.setState({date})
        this.picker.setDate(date);
    }

    updateAndNotifyAgGrid(date) {
        this.setState({
                date
            },
            this.props.onDateChanged
        );
    }


    onDateChanged(selectedDates) {
        this.setState({date: selectedDates[0]});
        this.updateAndNotifyAgGrid(selectedDates[0])
    }
}