import React, {Component} from "react";

class RangeFloatingFilter extends Component {
	constructor(props) {
        super(props);

        this.state = {
            currentMinValue: '',
            currentMaxValue: ''
        }

        this.minValueChanged = this.minValueChanged.bind(this);
        this.maxValueChanged = this.maxValueChanged.bind(this);
    }

    minValueChanged(event) {
        this.setState({
            currentMinValue: event.target.value
        },
        () => {
            this.props.onFloatingFilterChanged({model: this.buildModel()});
        })

    }

    maxValueChanged(event) {
    	this.setState({
            currentMaxValue: event.target.value
        },
        () => {
            this.props.onFloatingFilterChanged({model: this.buildModel()});
        })
    }

    onParentModelChanged(parentModel) {
        // note that the filter could be anything here, but our purposes we're assuming a greater than filter only,
        // so just read off the value and use that
        // this.setState({
        //     currentValue: !parentModel ? 0 : parentModel.filter
        // });
    }

    buildModel() {
        if (this.state.currentMinValue === '' && this.state.currentMaxValue === '') {
            return null;
        }
        if (this.state.currentMinValue === '') {
        	return {
	            filterType: 'number',
	            type: 'lessThanOrEqual',
	            filter: this.state.currentMaxValue,
	            filterTo: null
	        };
        }

        if (this.state.currentMaxValue === '') {
        	return {
	            filterType: 'number',
	            type: 'greaterThanOrEqual',
	            filter: this.state.currentMinValue,
	            filterTo: null
	        };
        }

        return {
            filterType: 'number',
            type: 'inRange',
            filter: this.state.currentMinValue,
            filterTo: this.state.currentMaxValue
        };
    }

    render() {
        return (
        	<div>
            	<input type="text"
                   value={this.state.currentMinValue}
                   onChange={this.minValueChanged}
                   placeholder="min"
                />
            	<input type="text" style={{ display: 'block' }} 
                   value={this.state.currentMaxValue}
                   onChange={this.maxValueChanged}
                   placeholder="max"
                />
            </div>
        )
    }

}

export default RangeFloatingFilter;