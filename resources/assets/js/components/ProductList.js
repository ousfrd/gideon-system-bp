import React from 'react';
import axios from 'axios';
import formateParamsDate from './formateParamsDate'
import { Tabs, Tab, Table } from 'react-bootstrap';

class ProductList extends React.Component {
    constructor(props) {
        super(props);

        this.handleSelect = this.handleSelect.bind(this);

        this.state = {
            key: 1,
            tab1_title: 'Daily Profit',
            tab2_title: 'Overall Profit',
            products: [],
            origin_products: [],
            productDir: {
            },
            activeProductDir: '',
            dates: []
        }

         this.searchProduct = this.searchProduct.bind(this);

    }

    handleSelect(key) {
        this.setState({ key });
    }

    componentDidMount() {

      axios.get(fetchdailyprofitsrows_url, {
    	       params: formateParamsDate(this.props.filterState)
    	    })
          .then(res => {

          	this.setState({
          		products: res.data.rows,
                origin_products: res.data.rows,
                dates: res.data.dates,
          	})

        })



    }

    componentDidUpdate(prevProps) {

        if( this.props.filterState !== prevProps.filterState ) {
          axios.get(fetchdailyprofitsrows_url, {
                   params: formateParamsDate(this.props.filterState)
                })
              .then(res => {

                this.setState({
                    products: res.data.rows,
                    origin_products: res.data.rows,
                    dates: res.data.dates,
                })

            })

        }

    }


    searchProduct(e) {
        var products = this.state.origin_products;
        var new_products = products.filter(function(product) {
          var name = product.name;
          return name && name.toLowerCase().includes(e.target.value.toLowerCase())
        });
        this.setState({products: new_products})
    }

    getProductActiveDir(sortKey) {
        return this.state.activeProductDir == sortKey ? this.state.productDir[sortKey] == 'asc' ? '▼' : '▲' : '';
    }


    render() {

        return (
                <Tabs activeKey={this.state.key} onSelect={this.handleSelect} id="controlled-tab">

                    <Tab eventKey={1} title={this.state.tab1_title}>
                        <Table>
                            <thead>
                              <tr>
                                <th width='210px'>ASIN</th>
                                <th width='280px'>Name</th>
                                {this.state.dates.map((date, index) => (
                                    <th>{date}</th>
                                ))}
                              </tr>
                              <tr>
                                <td></td>
                                <td><input type="text" className="form-control" name="name_filter" onBlur={this.searchProduct} /></td>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.products.map((product, index) => (
                              <tr key={index}>
                                <td><div className='adImage'><a href={product.amazon_link} target='_blank'><img src={product.small_image} /></a></div>
                                <div className='adASIN'><a href={product.product_link} target='_blank'>{product.asin}</a><br/>{product.country}</div></td>
                                <td>{product.name}</td>
                                {this.state.dates.map((date, index) => (
                                    <td>{product.dates[index].orders ? product.dates[index].orders : 0} ${product.dates[index].profit ? product.dates[index].profit : 0}</td>
                                ))}
                              </tr>
                              ))}
                              
                            </tbody>
                        </Table>
                    </Tab>

                    <Tab eventKey={2} title={this.state.tab2_title}>
                        <Table>

                        </Table>
                    </Tab>
                </Tabs>

            );
    }
}

export default ProductList;

