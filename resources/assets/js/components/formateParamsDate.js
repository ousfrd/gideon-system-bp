function formateParamsDate(filterState) {
  
  if(typeof filterState.startDate !== 'string') {
    filterState.startDate = filterState.startDate.format('MMMM D, YYYY');
    filterState.endDate = filterState.endDate.format('MMMM D, YYYY');
  }

  
	return filterState
}

export default formateParamsDate;