import React from 'react';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import 'bootstrap-daterangepicker/daterangepicker.css';

class FilterBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      account: 'all',
      marketplace: 'all',
      startDate: this.props.startDate,
      endDate: this.props.endDate,
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(7, 'days'), moment().subtract(1, 'days')],
        'Last 14 Days': [moment().subtract(14, 'days'), moment().subtract(1, 'days')],
        'Last 30 Days': [moment().subtract(30, 'days'), moment().subtract(1, 'days')],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'Lifetime': [moment(this.props.beginning, 'YYYY-MM-DD').startOf('month'), moment().endOf('month')],
      },
      autoUpdateInput: true,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeAccount = this.handleChangeAccount.bind(this);
    this.handleChangeMarketplace = this.handleChangeMarketplace.bind(this);
    this.handleChangePicker = this.handleChangePicker.bind(this);
  }


  handleSubmit(e) {
    this.props.onSubmit(this.state);
    e.preventDefault();
  }

  handleChangeAccount(e) {
    this.setState({account: e.target.value});
  }

  handleChangeMarketplace(e) {
    this.setState({marketplace: e.target.value});
  }

  handleChangePicker(event, picker) {
    this.setState({
      startDate: picker.startDate,
      endDate: picker.endDate,
    });
    event.preventDefault();
  }


  render() {
  	  let start = this.state.startDate.format('MMMM D, YYYY');
	    let end = this.state.endDate.format('MMMM D, YYYY');
	    let label = start + ' - ' + end;
	    if (start === end) {
	      label = start;
	    }
	    let buttonStyle = { 
	    	background: '#fff',
	    	cursor: 'pointer',
	    	padding: '5px 10px',
	    	border: '1px solid #ccc',
	    	width: '100%' 
	    };

      return (
      <div className="panel panel-default product-filter-form filter-form">
        <form onSubmit={this.handleSubmit}>
          <div className="panel-heading clearfix">
            <div className="row">

              <div className="col-md-2 col-sm-2 col-xs-12 mobile-padding-filter padding-sm">

                <div className="input form-group select">

    			       <select name="account" value={this.state.account} onChange={this.handleChangeAccount} className="form-control">
                    <option value="">All Accounts</option>
       				{Object.values(this.props.accounts).map((account, index) => <option key={index} value={account.seller_id}>{account.name} ({account.code})</option>)}
                </select>

                </div>

              </div>

              <div className="col-md-2 col-sm-2 col-xs-12 mobile-padding-filter padding-sm">

                <div className="input form-group select">

                  <select name="marketplace" value={this.state.marketplace} onChange={this.handleChangeMarketplace} className="form-control">
                        <option value="">All Marketplaces</option>
                        {Object.keys(this.props.marketplaces).map((keyName, keyIndex) => <option key={keyIndex} value={keyName}>{this.props.marketplaces[keyName]}</option>)}  
                  </select>
              
                </div>

              </div>


              <div className="col-md-4 col-sm-4 col-xs-12 mobile-padding-filter padding-sm">

          			<DateRangePicker 
          				startDate={this.state.startDate}
          				endDate={this.state.endDate}
          				ranges={this.state.ranges}
          				onEvent={this.handleChangePicker}
          				autoUpdateInput={this.state.autoUpdateInput}
          			>
          			<div id="reportrange" style={buttonStyle}>
          			    <i className="fa fa-calendar"></i>&nbsp;
          			    <span>{label}</span> <i className="fa fa-caret-down"></i>
          			</div>
      	      	</DateRangePicker>

              </div>

              <div className="col-md-2 col-sm-2 col-xs-12">
                  
                  <div className="submit">
                  
                  <input type="submit" value="Submit" className="btn btn-success btn-block btn" />
                
                </div>

              </div>

            </div>

          </div>

        </form>

      </div>
      );
    }


}

export default FilterBar;