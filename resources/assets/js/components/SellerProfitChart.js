import React from 'react';
import axios from 'axios';
import { Table } from 'react-bootstrap';
import contentEditable from './ContentEditable'
import '../../sass/switch.scss';

const getGoogle = (windowAsArg = window) => {
  if (typeof windowAsArg === "undefined") {
    return {};
  }
  if (typeof windowAsArg.google === "undefined") {
    throw new Error("google not in window object. Error in get-google-charts.");
  }
  return windowAsArg.google;
};
const getGoogleCharts = (windowAsArg = window) => {
  if (typeof windowAsArg === "undefined") {
    return {};
  }
  if (typeof windowAsArg.google === "undefined") {
    throw new Error("google not in window object. Error in get-google-charts.");
  }
  return windowAsArg.google.charts;
};

class GoogleChartLoader {
  constructor() {
    this.isLoaded = false;
    this.isLoading = false;
    this.loadScript = null;
    this.destroy = () => {
      this.isLoading = false;
      this.isLoaded = false;
      this.loadScript = null;
    };
    this.init = (packages, version, language, mapsApiKey) => {
      if ((this.isLoading || this.isLoaded) && this.loadScript !== null) {
        return this.loadScript;
      }
      this.isLoading = true;
      const script =
        typeof window !== "undefined"
          ? require("loadjs")
          : (link, { success: callback }) => callback();
      this.loadScript = new Promise(resolve => {
        script("https://www.gstatic.com/charts/loader.js", {
          success: () => {
            const google_charts = getGoogleCharts(window);
            google_charts.load(version || "current", {
              packages: packages || ["corechart"],
              language: language || "en",
              mapsApiKey
            });
            google_charts.setOnLoadCallback(() => {
              this.isLoaded = true;
              this.isLoading = false;
              resolve();
            });
          }
        });
      });
      this.isLoading = true;
      return this.loadScript;
    };
  }
}
const googleChartLoader = new GoogleChartLoader();

class SellerProfitChart extends React.Component {
	constructor(props) {
		super(props);

		this.main_data = null;
		this.main_chart = null;

		this.main_colors = [
		                'rgb(91,195,76)',  // green
		                'rgb(242,104,95)',    // red
		                'rgb(250,172,94)',   // orange
		                'rgb(177,116,227)',  // violet
		                'rgb(93,157,252)',   // blue
		                'rgb(76,215,200)',   // turquoise
		                'rgb(234,82,171)',   // pink
		                'rgb(107,142,35)'   // dark olive
		];

    this.columns = [];
    this.series = {};

		this.main_options = {
            title: 'Monthly PM Performance',
            height: 435,
            colors: this.main_colors,
            curveType: 'function',
            interpolateNulls: false,
            // legend: { position: 'bottom' },
            pointSize: 4,

            chartArea: {left: 80, top: 8, right: 180, height: '405'},

            hAxis: {
                gridlines: {color: 'transparent'},
                format: 'M\/d',
                textStyle: {color: 'rgb(155,170,182)'},
            },
            // vAxes: {
            //     0: {
            //         baselineColor: 'rgb(233,233,233)',
            //         textStyle: {color: 'grey'},
            //         gridlines: {color: 'transparent'},
            //         minValue: 0,
            //         viewWindow: {min: 0},
            //     }
            // },
            focusTarget: 'category',
            series: this.series
        };


		this.state = {
      columns: [],
			rows: [],
      aggrows: [],
      aggtotal: [],
      showSeries: 'enabled'
		}


    this.updatePmGoal = this.updatePmGoal.bind(this)
    this.switchShowSeries = this.switchShowSeries.bind(this)
	}

  transformRowData(aggrows) {
    let aggrowsObj =  aggrows.reduce((memo, item)=>{
      (memo[item['group']] = memo[item['group']] || []).push(item);
      return memo;
    }, {});
    for (const team in aggrowsObj) {
      let membersData = aggrowsObj[team];
      let initData = {
        ad_expenses: 0,
        diff: 0,
        expect_profit: 0,
        goal: 0,
        orders: 0,
        payout: 0,
        ad_expenses: 0,
        profit: 0,
        refund: 0
      };
      let teamData = membersData.reduce((memo, item) => {
        for(const key in memo) {
          memo[key] += item[key];
        }
        return memo;
      }, initData);
      teamData['pm'] = "Group Total";
      teamData['finish_rate'] = (100 * teamData.profit / teamData.goal).toFixed(2) + '%' ;
      membersData.push(teamData);
    }
    return aggrowsObj;
  }

	componentDidMount() {
		if (typeof window === "undefined") {
	      return;
	    }
      
      	googleChartLoader
        .init(
          ["corechart"],
          "current",
          "en",
          ""
        )
        .then(() => {

         axios.post(fetchmonthlyprofits_url, this.props.filterState)
	       .then(res => {
          let aggrows = this.transformRowData(res.data.aggrows)
          this.setState({ columns: res.data.columns, aggrows: aggrows, aggtotal: res.data.aggtotal });
          this.setColumnsSeries();
	      	this.redraw_chart(res.data.rows);
	       })

        });
    	
  	}

  	componentDidUpdate(prevProps) {
      if( this.props.filterState !== prevProps.filterState ) {
  		  axios.post(fetchmonthlyprofits_url, this.props.filterState)
  	      .then(res => {
            let aggrows = this.transformRowData(res.data.aggrows)
            this.setState({ columns: res.data.columns, aggrows: aggrows, aggtotal: res.data.aggtotal });
            this.redraw_chart(res.data.rows)
  	      })
  	  }
  	}

    setColumnsSeries() {
      var i = 0;
      this.state.columns.forEach(column => {
          this.columns.push(i);
          if (i > 0) {
              this.series[i - 1] = {};
          }
          i++;
      });     
    }

    switchShowSeries() {
      var showSeries = this.state.showSeries == 'enabled' ? 'disabled' : 'enabled';

      if(showSeries == "enabled") {
        var i = 0;
        this.columns.forEach(col => {
          if(i > 0) {
              this.columns[i] = i;
              this.series[i - 1].color = null; 
          }
          i++;
        })
      }

      if(showSeries == 'disabled') {
        this.columns.forEach(col => {
          if(col > 0) {
              this.columns[col] = {
                  label: this.state.columns[col].label,
                  type: this.state.columns[col].type,
                  calc: function () {
                      return null;
                  }
              };
              this.series[col - 1].color = '#CCCCCC'; 
          }
        })
      }

      this.setState({ showSeries: showSeries });
      this.draw_chart();
    }

  	redraw_chart(data) {
      	Object.values(data).forEach(function(row) { 
      		row[0] = moment(row[0], 'YYYY-MM-DD').toDate();
      	})

        this.setState({ rows: Object.values(data) });

        this.load_main_chart();
        this.draw_chart();
        this.listen_events();
  	}


	load_main_chart() {
		this.main_data = new window.google.visualization.DataTable();
		this.state.columns.forEach(column => {
	        this.main_data.addColumn(column);
	  });
		this.main_data.addRows(this.state.rows);

		this.main_chart = new window.google.visualization.LineChart(document.getElementById('main_chart'));
	}

	draw_chart() {
        var v = new window.google.visualization.DataView(this.main_data);
        var hide = [];
        v.setColumns(this.columns);
        this.main_chart.draw(v, this.main_options);
	}

  listen_events() {
    var chart = this.main_chart;
    var that = this;

    window.google.visualization.events.addListener(chart, 'select', function () {
        var sel = chart.getSelection();

        // if selection length is 0, we deselected an element
        if (sel.length > 0) {
            // if row is undefined, we clicked on the legend
            if (sel[0].row === null) {
                var col = sel[0].column;
                if (that.columns[col] == col) {
                    // hide the data series
                    that.columns[col] = {
                        label: that.state.columns[col].label,
                        type: that.state.columns[col].type,
                        calc: function () {
                            return null;
                        }
                    };
                    
                    // grey out the legend entry
                    that.series[col - 1].color = '#CCCCCC';
                }
                else {
                    // show the data series
                    that.columns[col] = col;
                    that.series[col - 1].color = null;
                }
                
                that.draw_chart();
            }
        }
    })
  }


  updatePmGoal(e) {
    var uid = e.props.uid;
    var year = this.props.filterState.year;
    var month = this.props.filterState.month;
    var value = e.domElm.textContent;
    var originGoal = e.props.originGoal;

    if(value != originGoal) {
      axios.post(updatePmGoal_url, {uid: uid, month: month, year: year, goal: value }).then(res => {

        console.log(res);

      })  
    }

  }


	render() {

		return (
			<div className="google_charts">
            <div className="box-inlay">
            Show / Hide: 
              <span className={this.state.showSeries == 'enabled' ? 'rc-switch rc-switch-checked' : 'rc-switch'} onClick={this.switchShowSeries}>
                <span className="rc-switch-inner"></span>
              </span>
            </div>
            
				<div id="main_chart"></div>

        <br />
        <br />
        <Table>
              <thead>
                <tr>
                  <th>Group</th>
                  <th>PM</th>
                  <th>Goal</th>
                  <th>Profit</th>
                  <th>Orders</th>
                  <th>Ad</th>
                  <th>Refund</th>
                  <th>Payout</th>
                  <th>完成率</th>
                  <th>差额</th>
                  <th>预计月盈利</th>
                </tr>
                <tr className="total">
                  <td></td>
                  <td></td>
                  <td>${this.state.aggtotal.goal}</td>
                  <td>${this.state.aggtotal.profit}</td>
                  <td>{this.state.aggtotal.orders}</td>
                  <td>${this.state.aggtotal.ad_expenses}</td>
                  <td>${this.state.aggtotal.refund}</td>
                  <td>${this.state.aggtotal.payout}</td>
                  <td>{this.state.aggtotal.finish_rate}</td>
                  <td>${this.state.aggtotal.diff}</td>
                  <td>${this.state.aggtotal.expect_profit}</td>
                </tr>
              </thead>
              <tbody>
                { Object.keys(this.state.aggrows).map((groupName) => {
                  const rows = this.state.aggrows[groupName];
                  return rows.map((row, index) => {
                    if (index == 0) {
                      return (<tr key={groupName+index} className={row.uid ? 'single' : groupName ? 'total' : ''}>
                        <td className="group-name" rowSpan={rows.length}>{groupName}</td>
                        <td><PMRow uid={row.uid} pm={row.pm} /></td>
                        <td><PMGoal uid={row.uid} goal={row.goal} updatePmGoal={this.updatePmGoal}/></td>
                        <td>${row.profit.toLocaleString()}</td>
                        <td>{row.orders.toLocaleString()}</td>
                        <td>${row.ad_expenses.toLocaleString()}</td>
                        <td>${row.refund.toLocaleString()}</td>
                        <td>${row.payout.toLocaleString()}</td>
                        <td>{row.finish_rate}</td>
                        <td>${row.diff.toLocaleString()}</td>
                        <td>${row.expect_profit.toLocaleString()}</td>
                      </tr>)
                    } else {
                      return (<tr key={groupName+index} className={row.uid ? 'single' : groupName ? 'total' : ''}>
                        <td><PMRow uid={row.uid} pm={row.pm} /></td>
                        <td><PMGoal uid={row.uid} goal={row.goal} updatePmGoal={this.updatePmGoal}/></td>
                        <td>${row.profit.toLocaleString()}</td>
                        <td>{row.orders.toLocaleString()}</td>
                        <td>${row.ad_expenses.toLocaleString()}</td>
                        <td>${row.refund.toLocaleString()}</td>
                        <td>${row.payout.toLocaleString()}</td>
                        <td>{row.finish_rate}</td>
                        <td>${row.diff.toLocaleString()}</td>
                        <td>${row.expect_profit.toLocaleString()}</td>
                      </tr>)
                    }
                     
                  })
                })}
                
              </tbody>
          </Table>


			</div>
		);
	}
}

const PMRow = function(props) {
  if (props.uid) {
    return <a target="_blank" href={"/user/" + props.uid}>{ props.pm }</a>;
  } else {
  return <span>{props.pm}</span>;
  }
}

const PMGoal = function(props) {
  const EditableSpan = contentEditable('span');
  if (props.uid) {
    return (
      <React.Fragment>
        $<EditableSpan value={props.goal} onSave={props.updatePmGoal} uid={props.uid} originGoal={props.goal} />
      </React.Fragment>
    )
  } else {
    return (
    <React.Fragment>${props.goal}</React.Fragment>
    )
  }
}

export default SellerProfitChart;