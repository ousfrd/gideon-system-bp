import React from 'react';
import axios from 'axios';
import formateParamsDate from './formateParamsDate'
import { Chart } from 'react-google-charts';



class SettlementAccountChart extends React.Component {
	constructor(props) {
    super(props);
	}



	render() {
		return (


      <Chart
      chartType="ColumnChart"
      rows={this.props.AccountChartData}
      columns={[
        {
          type: "string",
          label: "Account"
        },
        {
          type: "number",
          label: "Amount($)"
        }
      ]}
      options={
        // Chart options
        {
          hAxis: {
            title: "Account",
          },
          vAxis: { title: "Amount($)"}
        }
      }
      width={"100%"}
      height={"350px"}
      
    />      

		);
	}
}

export default SettlementAccountChart;