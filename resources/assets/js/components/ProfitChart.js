import React from 'react';
import axios from 'axios';
import formateParamsDate from './formateParamsDate'

const getGoogle = (windowAsArg = window) => {
  if (typeof windowAsArg === "undefined") {
    return {};
  }
  if (typeof windowAsArg.google === "undefined") {
    throw new Error("google not in window object. Error in get-google-charts.");
  }
  return windowAsArg.google;
};
const getGoogleCharts = (windowAsArg = window) => {
  if (typeof windowAsArg === "undefined") {
    return {};
  }
  if (typeof windowAsArg.google === "undefined") {
    throw new Error("google not in window object. Error in get-google-charts.");
  }
  return windowAsArg.google.charts;
};

class GoogleChartLoader {
  constructor() {
    this.isLoaded = false;
    this.isLoading = false;
    this.loadScript = null;
    this.destroy = () => {
      this.isLoading = false;
      this.isLoaded = false;
      this.loadScript = null;
    };
    this.init = (packages, version, language, mapsApiKey) => {
      if ((this.isLoading || this.isLoaded) && this.loadScript !== null) {
        return this.loadScript;
      }
      this.isLoading = true;
      const script =
        typeof window !== "undefined"
          ? require("loadjs")
          : (link, { success: callback }) => callback();
      this.loadScript = new Promise(resolve => {
        script("https://www.gstatic.com/charts/loader.js", {
          success: () => {
            const google_charts = getGoogleCharts(window);
            google_charts.load(version || "current", {
              packages: packages || ["corechart"],
              language: language || "en",
              mapsApiKey
            });
            google_charts.setOnLoadCallback(() => {
              this.isLoaded = true;
              this.isLoading = false;
              resolve();
            });
          }
        });
      });
      this.isLoading = true;
      return this.loadScript;
    };
  }
}
const googleChartLoader = new GoogleChartLoader();

class ProfitChart extends React.Component {
	constructor(props) {
		super(props);

		this.main_data = null;
		this.main_chart = null;

		this.main_colors = [
		                'rgb(91,195,76)',  // green
		                'rgb(242,104,95)',    // red
		                'rgb(250,172,94)',   // orange
		                'rgb(177,116,227)',  // violet
		                'rgb(93,157,252)',   // blue
		                'rgb(76,215,200)',   // turquoise
		                'rgb(234,82,171)',   // pink
		                'rgb(107,142,35)',   // dark olive
                    'rgb(255,218,89)',   // yellow
                    'rgb(155,170,182)',   // grey
		];

		this.main_options = {
            height: 235,
            colors: this.main_colors,
            curveType: 'function',
            interpolateNulls: false,
            legend: 'none',
            pointSize: 4,

            chartArea: {left: 80, top: 8, right: 80, height: '205'},

            hAxis: {
                gridlines: {color: 'transparent'},
                format: 'M\/d',
                textStyle: {color: 'rgb(155,170,182)'},
            },
            vAxes: {
                0: {
                    baselineColor: 'rgb(233,233,233)',
                    textStyle: {color: 'grey'},
                    gridlines: {color: 'transparent'},
                    minValue: 0,
                    viewWindow: {min: 0},
                },
                1: {
                    baselineColor: 'rgb(233,233,233)',
                    textStyle: {color: 'grey'},
                    gridlines: {color: 'transparent'},
                    minValue: 0,
                    viewWindow: {min: 0},
                }
            },
            series: {
                0: {targetAxisIndex: 0},
                1: {targetAxisIndex: 1},
            },
            seriesType: 'line',
            focusTarget: 'category',
            tooltip: { isHtml: true }
        };

		this.columns = [
		        {"type":"date","id":"x","label":"x"},
            {"type":"number","id":"profit","label":"Profit"},
            {"type":"number","id":"orders","label":"Orders"},
		        {"type":"number","id":"total_gross","label":"Total Gross"},
		        {"type":"number","id":"refund","label":"Refund"},
		        {"type":"number","id":"payout","label":"Payout"},
		        {"type":"number","id":"product_cost","label":"Product Cost"},
		        {"type":"number","id":"ad_expenses","label":"Ad Expenses"},
		        {"type":"number","id":"total_cost","label":"Total Cost"},
            {"type":"number","id":"total_sessions","label":"Total Sessions"},
            {"type":"number","id":"unit_session_percentage","label":"Unit Session Percentage"},
            {'type': 'string', 'role': 'tooltip', 'p': {'html': true}},
		];

		this.state = {
			show_series: [0, 1],
			rows: []
		}

		this.series_selected_left = this.series_selected_left.bind(this);
		this.series_selected_right = this.series_selected_right.bind(this);
	}

	componentDidMount() {
		if (typeof window === "undefined") {
	      return;
	    }
      
      	googleChartLoader
        .init(
          ["corechart"],
          "current",
          "en",
          ""
        )
        .then(() => {

         axios.post(fetchdailyprofits_url, formateParamsDate(this.props.filterState))
	       .then(res => {

	      	this.redraw_chart(res.data)

	       })

        });
    	
  	}

  	componentDidUpdate(prevProps) {
      if( this.props.filterState !== prevProps.filterState ) {
  		  axios.post(fetchdailyprofits_url, formateParamsDate(this.props.filterState))
  	      .then(res => {
  	        this.redraw_chart(res.data)
  	      })
  	  }
  	}


	redraw_chart(data) {
      	Object.values(data).forEach(function(row) { 
      		row[0] = moment(row[0], 'YYYY-MM-DD').toDate();
      	})

        this.setState({ rows: Object.values(data) });

        this.load_main_chart();
        this.draw_chart();
	}


	series_selected_left(e) {
		this.state.show_series[0] = parseInt(e.target.value);
		this.setState({
			show_series: this.state.show_series
		})
		this.draw_chart();
	}

	series_selected_right(e) {
		this.state.show_series[1] = parseInt(e.target.value);
		this.setState({
			show_series: this.state.show_series
		})
		this.draw_chart();
	}

	load_main_chart() {
		this.main_data = new window.google.visualization.DataTable();
		this.columns.forEach(column => {
	        this.main_data.addColumn(column);
	      });
    this.state.rows.forEach(row => {
      var tooltip = "<div style='padding: 10px;'><h4>"+moment(row[0]).format('YYYY-MM-DD')+"</h4><table class='table stats-popover'><tr><td><strong>Profit:</strong></td><td class='text-right text-success'>$"+row[1]+"</td></tr><tr><td><strong>Orders:</strong></td><td class='text-right text-success'>"+row[2]+"</td></tr><tr><td><strong>Total Gross:</strong></td><td class='text-right text-success'>$"+row[3]+"</td></tr><tr><td><strong>Refunds:</strong></td><td class='text-right text-success'>$"+row[4]+"</td></tr><tr><td><strong>Payout:</strong></td><td class='text-right text-success'>$"+row[5]+"</td></tr><tr><td><strong>Ad Expenses:</strong></td><td class='text-right text-success'>$"+row[7]+"</td></tr><tr><td><strong>Sessions:</strong></td><td class='text-right text-success'>"+row[9]+"</td></tr><tr><td><strong>Session Percentage:</strong></td><td class='text-right text-success'>"+row[10]+"%</td></tr></table></div>";
      row.push(tooltip);
      this.main_data.addRow(row);
    })
		// this.main_data.addRows(this.state.rows);

		this.main_chart = new window.google.visualization.LineChart(document.getElementById('main_chart'));
	}

	draw_chart() {
		var show_series = this.state.show_series;
        var v = new window.google.visualization.DataView(this.main_data);
        var columns = [0, v.getNumberOfColumns() - 1];
        var hide = [];

        for (var i = 0; i < v.getNumberOfColumns() - 1; i++) {
            if ($.inArray(Math.floor(i), show_series) == -1) {
              hide.push(i+1);
            } else {
                columns.push(i+1);
            }
        }
        v.hideColumns(hide);
        v.setColumns(columns);
        

        var main_options = this.main_options;
        var main_colors = this.main_colors;
        var c1, c2;
        if (show_series[0] > show_series[1]) {
            c1 = main_colors[show_series[1]];
            c2 = main_colors[show_series[0]];
            main_options.series[0].targetAxisIndex = 1;
            main_options.series[1].targetAxisIndex = 0;
        } else {
            c1 = main_colors[show_series[0]];
            c2 = main_colors[show_series[1]];
            main_options.series[0].targetAxisIndex = 0;
            main_options.series[1].targetAxisIndex = 1;
        }

        main_options.series[0].type = "line";
        main_options.series[1].type = "line";
        if(show_series[0] == 8) {
        	 main_options.series[main_options.series[0].targetAxisIndex].type = "bars";
        }
  
        if(show_series[1] == 8) {
        	main_options.series[main_options.series[1].targetAxisIndex].type = "bars";
        }



        main_options.colors = [c1, c2];

        main_options.vAxes[0].textStyle.color = main_colors[show_series[0]];
        main_options.vAxes[1].textStyle.color = main_colors[show_series[1]];


        var ticks = [];
        for (var i = 0; i < v.getNumberOfRows(); i++) {
          ticks.push(v.getValue(i, 0));
        }
        main_options.hAxis.ticks = ticks;

        this.main_chart.draw(v, main_options);

	}

	render() {
		return (
			<div className="google_charts">
				<div className="box-inlay">
	                <select id="series1" value={this.state.show_series[0]} className={'color' + this.state.show_series[0]} onChange={this.series_selected_left}>
                      <option value="0" className="color0">Profit</option>
                      <option value="1" className="color1">Orders</option>
                      <option value="2" className="color2">Total Gross</option>
                      <option value="3" className="color3">Refund</option>
                      <option value="4" className="color4">Payout</option>
                      <option value="5" className="color5">Product Cost</option>
                      <option value="6" className="color6">Ad Expenses</option>
                      <option value="7" className="color7">Total Cost</option>
                      <option value="8" className="color8">Total Sessions</option>
                      <option value="9" className="color9">Unit Session Percentage</option>
	                </select> 
	                <select id="series2" value={this.state.show_series[1]} className={'color' + this.state.show_series[1]} onChange={this.series_selected_right}>
                      <option value="0" className="color0">Profit</option>
                      <option value="1" className="color1">Orders</option>
                      <option value="2" className="color2">Total Gross</option>
                      <option value="3" className="color3">Refund</option>
                      <option value="4" className="color4">Payout</option>
                      <option value="5" className="color5">Product Cost</option>
                      <option value="6" className="color6">Ad Expenses</option>
                      <option value="7" className="color7">Total Cost</option>
                      <option value="8" className="color8">Total Sessions</option>
                      <option value="9" className="color9">Unit Session Percentage</option>
	                </select>
				</div>
				<div id="main_chart"></div>
			</div>
		);
	}
}

export default ProfitChart;