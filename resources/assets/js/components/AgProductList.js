import React from 'react';
import axios from 'axios';
import formateParamsDate from './formateParamsDate'

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';

import RangeFloatingFilter from "./filters/RangeFloatingFilter";
import ImageCellRenderer from "./cellRenderers/ImageCellRenderer";
import AsinCellRenderer from "./cellRenderers/AsinCellRenderer";
import PriceCellRenderer from "./cellRenderers/PriceCellRenderer";
import PercentageCellRenderer from "./cellRenderers/PercentageCellRenderer";
import InventoryCellRenderer from "./cellRenderers/InventoryCellRenderer";
import InventorySuspendedCellRenderer from "./cellRenderers/InventorySuspendedCellRenderer";
import IsDiscontinuedCellRenderer from "./cellRenderers/IsDiscontinuedCellRenderer";
import IsDiscontinuedFloatingFilter from "./filters/IsDiscontinuedFloatingFilter";
import ManagerCellRenderer from "./cellRenderers/ManagerCellRenderer";

class AgProductList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            columnDefs: [],
            rowData: [],
            topRowData: [],
            frameworkComponents: { 
              rangeFloatingFilter: RangeFloatingFilter, 
              imageCellRenderer: ImageCellRenderer, 
              asinCellRenderer: AsinCellRenderer,
              priceCellRenderer: PriceCellRenderer,
              percentageCellRenderer: PercentageCellRenderer,
              inventoryCellRenderer: InventoryCellRenderer,
              inventorySuspendedCellRenderer: InventorySuspendedCellRenderer, 
              isDiscontinuedCellRenderer: IsDiscontinuedCellRenderer,
              isDiscontinuedFloatingFilter: IsDiscontinuedFloatingFilter,
              managerCellRenderer: ManagerCellRenderer
            },
            sortingOrder: ["desc", "asc", null],
        }

        
        this.getRowHeight = this.getRowHeight.bind(this);
        this.onGridReady = this.onGridReady.bind(this);
        this.onFilterChanged = this.onFilterChanged.bind(this);
    }

	  onGridReady(params) {
	    this.gridApi = params.api;
	    this.gridColumnApi = params.columnApi;
	    params.api.sizeColumnsToFit();
	  }

    onBtExport() {
      this.gridApi.exportDataAsCsv({
        fileName: 'ProductDailyProfit-'+moment(this.props.filterState.startDate).format('MMDDYYYY')+'-'+moment(this.props.filterState.endDate).format('MMDDYYYY')+'.csv',
        columnGroups: true,
        allColumns: true
      });
    }

    onFilterChanged(params) {
      this.gridApi = params.api;
      var asin_params = [];
      this.gridApi.forEachNodeAfterFilterAndSort(function(rowNode, index) {
        var asin = rowNode.data.asin;
        var country = rowNode.data.country;
        asin_params.push([asin, country]);
      })

      this.props.onAsinFilter(JSON.stringify(asin_params));
    }

    getRowHeight(params) {
      if (params.node.rowPinned == 'top') {
          return 30;
      } else {
          return 80;
      }
    }


    componentDidMount() {

      axios.post(fetchdailyprofitsrows_url, formateParamsDate(this.props.filterState))
          .then(res => {

            this.setState({
                columnDefs: res.data.columnDefs, 
                rowData: res.data.rowData,
                topRowData: res.data.topRowData,
            })

        })

    }

    componentDidUpdate(prevProps) {

        if( this.props.filterState !== prevProps.filterState ) {

          axios.post(fetchdailyprofitsrows_url, formateParamsDate(this.props.filterState))
              .then(res => {

                if(this.props.filterState.asin_params != prevProps.filterState.asin_params) {
                  this.setState({
                      topRowData: res.data.topRowData,
                  })                  
                } else {
                  this.setState({
                      columnDefs: res.data.columnDefs, 
                      rowData: res.data.rowData,
                      topRowData: res.data.topRowData,
                  })                  
                }


            })

        }

    }


    render() {
        return (
                <div 
                  className="ag-theme-balham"
                  style={{ 
	                height: '1500px'}} 
		            >
                 <div style={{ paddingBottom: "4px", display: "inline-block" }}>
                  <label style={{ marginTop: "9px" }}>
                    <button onClick={this.onBtExport.bind(this)}>Export to CSV</button>
                  </label>
                 </div>

                    <AgGridReact
                     	enableSorting={true}
                     	enableFilter={true}
                        enableColResize={true}
                        rowDragManaged={true}
                     	floatingFilter={true}
                      suppressDragLeaveHidesColumns={true}
                      pinnedTopRowData={this.state.topRowData}
                        // pagination={true}
                        columnDefs={this.state.columnDefs}
                        rowData={this.state.rowData}
                        frameworkComponents={this.state.frameworkComponents}
                        getRowHeight={this.getRowHeight}
                    floatingFiltersHeight={60}
                    onGridReady={this.onGridReady}
                    onFilterChanged={this.onFilterChanged}
                    sortingOrder={this.state.sortingOrder}
                    >
                    </AgGridReact>
                </div>
            );
    }
}

export default AgProductList;

