import React from 'react';
import axios from 'axios';
import Picky from 'react-picky';
import 'react-picky/dist/picky.css'; // Include CSS


class AccountList extends React.Component {
  constructor(props) {

    super(props);

    this.state = {
        arrayValue: [],
        accountList: [] 
    };    
    

    this.selectMultipleOption = this.selectMultipleOption.bind(this);
   

  }


// after selection in the account box
  selectMultipleOption(value) {

    
    this.setState({
      arrayValue: value,      
    }, () => {

      this.props.onChange(value);

    });

  }


  render() {

      return (

            <div className="col-md-3 col-sm-1 col-xs-12 mobile-padding-filter padding-sm">
              <div>Accounts</div>
            <Picky
              value={(this.props.arrayValue != undefined )? this.props.arrayValue : [] }
              options={this.props.accountList}
              onChange={this.selectMultipleOption}
              open={false}
              valueKey="id"
              labelKey="name"
              multiple={true}
              includeSelectAll={true}
              includeFilter={true}
              
            />

            </div>

      );
    }


}

export default AccountList;