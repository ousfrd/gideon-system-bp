import React from 'react';
import axios from 'axios';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import 'bootstrap-daterangepicker/daterangepicker.css';
import formateParamsDate from './formateParamsDate'



class SettlementDate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: this.props.startDate,
      endDate: this.props.endDate,
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(7, 'days'), moment().subtract(1, 'days')],
        'Last 14 Days': [moment().subtract(14, 'days'), moment().subtract(1, 'days')],
        'Last 30 Days': [moment().subtract(30, 'days'), moment().subtract(1, 'days')],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'Lifetime': [moment(this.props.beginning).startOf('month'), moment().endOf('month')],
      },   
      autoUpdateInput: false,
    };

    this.handleChangePicker = this.handleChangePicker.bind(this);
    this.onApplyEvent = this.onApplyEvent.bind(this);

    
    
  }




  handleChangePicker(event, picker) {
      
    this.setState({

      startDate: picker.startDate,
      endDate: picker.endDate,

    });


  }  


  onApplyEvent(e){

    this.props.onChange(this.state);

  }   


  render() {

  	  let start = this.state.startDate.format('MMMM D, YYYY');
	    let end = this.state.endDate.format('MMMM D, YYYY');
	    let label = start + ' - ' + end;
	    if (start === end) {
	      label = start;
	    }

      let buttonStyle = { 
	    	background: '#fff',
	    	cursor: 'pointer',
	    	padding: '5px 10px',
	    	border: '1px solid #ccc',
	    	width: '100%' 
	    };

      return (

            <div className="col-md-3 col-sm-1 col-xs-12 mobile-padding-filter padding-sm">
              <div>Settlement Date</div>
              <DateRangePicker 
                startDate={this.state.startDate}
                endDate={this.state.endDate}
                ranges={this.state.ranges}
                onEvent={this.handleChangePicker}
                autoUpdateInput={this.state.autoUpdateInput}
                onApply={this.onApplyEvent}
              >
                <div id="reportrange" style={buttonStyle}>
                    <i className="fa fa-calendar"></i>&nbsp;
                    <span>{label}</span> <i className="fa fa-caret-down"></i>
                </div>
              </DateRangePicker>

            </div>

      );
    }


}

export default SettlementDate;