import React from 'react';
import axios from 'axios';
import formateParamsDate from './formateParamsDate'

const getGoogle = (windowAsArg = window) => {
  if (typeof windowAsArg === "undefined") {
    return {};
  }
  if (typeof windowAsArg.google === "undefined") {
    throw new Error("google not in window object. Error in get-google-charts.");
  }
  return windowAsArg.google;
};
const getGoogleCharts = (windowAsArg = window) => {
  if (typeof windowAsArg === "undefined") {
    return {};
  }
  if (typeof windowAsArg.google === "undefined") {
    throw new Error("google not in window object. Error in get-google-charts.");
  }
  return windowAsArg.google.charts;
};

class GoogleChartLoader {
  constructor() {
    this.isLoaded = false;
    this.isLoading = false;
    this.loadScript = null;
    this.destroy = () => {
      this.isLoading = false;
      this.isLoaded = false;
      this.loadScript = null;
    };
    this.init = (packages, version, language, mapsApiKey) => {
      if ((this.isLoading || this.isLoaded) && this.loadScript !== null) {
        return this.loadScript;
      }
      this.isLoading = true;
      const script =
        typeof window !== "undefined"
          ? require("loadjs")
          : (link, { success: callback }) => callback();
      this.loadScript = new Promise(resolve => {
        script("https://www.gstatic.com/charts/loader.js", {
          success: () => {
            const google_charts = getGoogleCharts(window);
            google_charts.load(version || "current", {
              packages: packages || ["corechart"],
              language: language || "en",
              mapsApiKey
            });
            google_charts.setOnLoadCallback(() => {
              this.isLoaded = true;
              this.isLoading = false;
              resolve();
            });
          }
        });
      });
      this.isLoading = true;
      return this.loadScript;
    };
  }
}
const googleChartLoader = new GoogleChartLoader();

class GoogleChart extends React.Component {
	constructor(props) {
		super(props);

		this.main_data = null;
		this.main_chart = null;

		this.main_colors = [
		                'rgb(91,195,76)',  // green
		                'rgb(242,104,95)',    // red
		                'rgb(250,172,94)',   // orange
		                'rgb(177,116,227)',  // violet
		                'rgb(93,157,252)',   // blue
		                'rgb(76,215,200)',   // turquoise
		                'rgb(234,82,171)',   // pink
		                'rgb(255,218,89)',   // yellow
		                'rgb(155,170,182)',  // grey
		                'rgb(107,142,35)'   // dark olive
		];

		this.main_options = {
            height: 235,
            colors: this.main_colors,
            curveType: 'function',
            interpolateNulls: false,
            legend: 'none',
            pointSize: 4,

            chartArea: {left: 80, top: 8, right: 80, height: '205'},

            hAxis: {
                gridlines: {color: 'transparent'},
                format: 'M\/d',
                textStyle: {color: 'rgb(155,170,182)'},
            },
            vAxes: {
                0: {
                    baselineColor: 'rgb(233,233,233)',
                    textStyle: {color: 'grey'},
                    gridlines: {color: 'transparent'},
                    minValue: 0,
                    viewWindow: {min: 0},
                },
                1: {
                    baselineColor: 'rgb(233,233,233)',
                    textStyle: {color: 'grey'},
                    gridlines: {color: 'transparent'},
                    minValue: 0,
                    viewWindow: {min: 0},
                }
            },
            series: {
                0: {targetAxisIndex: 0},
                1: {targetAxisIndex: 1},
            },
            seriesType: 'line',
            focusTarget: 'category',
        };

		this.columns = [
		        {"type":"date","id":"x","label":"x"},
		        {"type":"number","id":"revenue","label":"revenue"},
		        {"type":"number","id":"cost","label":"cost"},
		        {"type":"number","id":"acos","label":"acos"},
		        {"type":"number","id":"cpc","label":"cpc"},
		        {"type":"number","id":"impressions","label":"impressions"},
		        {"type":"number","id":"ctr","label":"ctr"},
		        {"type":"number","id":"clicks","label":"clicks"},
		        {"type":"number","id":"cr","label":"cr"},
		        {"type":"number","id":"orders","label":"orders"},
		        {"type":"number","id":"starting bid","label":"starting bid"}
		];

		this.state = {
			show_series: [0, 2],
			rows: []
		}

		this.series_selected_left = this.series_selected_left.bind(this);
		this.series_selected_right = this.series_selected_right.bind(this);
	}

	componentDidMount() {
		if (typeof window === "undefined") {
	      return;
	    }
      
      	googleChartLoader
        .init(
          ["corechart"],
          "current",
          "en",
          ""
        )
        .then(() => {

         axios.post(fetchrows_url, formateParamsDate(this.props.filterState))
	       .then(res => {

	      	this.redraw_chart(res.data)

	       })

        });
    	
  	}

  	componentDidUpdate(prevProps) {
      if( this.props.filterState !== prevProps.filterState ) {
  		  axios.post(fetchrows_url, formateParamsDate(this.props.filterState))
  	      .then(res => {
  	        this.redraw_chart(res.data)
  	      })
  	  }
  	}


	redraw_chart(data) {
      	Object.values(data).forEach(function(row) { 
      		row[0] = moment(row[0], 'YYYY-MM-DD').toDate();
      	})

        this.setState({ rows: Object.values(data) });

        this.load_main_chart();
        this.draw_chart();
	}


	series_selected_left(e) {
		this.state.show_series[0] = parseInt(e.target.value);
		this.setState({
			show_series: this.state.show_series
		})
		this.draw_chart();
	}

	series_selected_right(e) {
		this.state.show_series[1] = parseInt(e.target.value);
		this.setState({
			show_series: this.state.show_series
		})
		this.draw_chart();
	}

	load_main_chart() {
		this.main_data = new window.google.visualization.DataTable();
		this.columns.forEach(column => {
	        this.main_data.addColumn(column);
	      });
		this.main_data.addRows(this.state.rows);

		this.main_chart = new window.google.visualization.LineChart(document.getElementById('main_chart'));
	}

	draw_chart() {
		var show_series = this.state.show_series;
        var v = new window.google.visualization.DataView(this.main_data);
        var hide = [];

        for (var i = 0; i < v.getNumberOfColumns() - 1; i++) {
            if (($.inArray(Math.floor(i), show_series) == -1)) {
                hide.push(i + 1);
            }
        }
        v.hideColumns(hide);

        var main_options = this.main_options;
        var main_colors = this.main_colors;
        var c1, c2;
        if (show_series[0] > show_series[1]) {
            c1 = main_colors[show_series[1]];
            c2 = main_colors[show_series[0]];
            main_options.series[0].targetAxisIndex = 1;
            main_options.series[1].targetAxisIndex = 0;
        } else {
            c1 = main_colors[show_series[0]];
            c2 = main_colors[show_series[1]];
            main_options.series[0].targetAxisIndex = 0;
            main_options.series[1].targetAxisIndex = 1;
        }

        main_options.series[0].type = "line";
        main_options.series[1].type = "line";
        if(show_series[0] == 4) {
        	 main_options.series[main_options.series[0].targetAxisIndex].type = "bars";
        }
  
        if(show_series[1] == 4) {
        	main_options.series[main_options.series[1].targetAxisIndex].type = "bars";
        }



        main_options.colors = [c1, c2];

        main_options.vAxes[0].textStyle.color = main_colors[show_series[0]];
        main_options.vAxes[1].textStyle.color = main_colors[show_series[1]];


        var ticks = [];
        for (var i = 0; i < v.getNumberOfRows(); i++) {
          ticks.push(v.getValue(i, 0));
        }
        main_options.hAxis.ticks = ticks;

        this.main_chart.draw(v, main_options);

	}

	render() {
		return (
			<div className="google_charts">
				<div className="box-inlay">
	                <select id="series1" value={this.state.show_series[0]} className={'color' + this.state.show_series[0]} onChange={this.series_selected_left}>
	                    <option value="0" className="color0">Sales</option>
	                    <option value="1" className="color1">Ad Spend</option>
	                    <option value="2" className="color2">ACoS</option>
	                    <option value="3" className="color3">CPC</option>
	                    <option value="4" className="color4">Impressions</option>
	                    <option value="5" className="color5">CTR</option>
	                    <option value="6" className="color6">Clicks</option>
	                    <option value="7" className="color7">CR</option>
	                    <option value="8" className="color8">Orders</option>
	                </select> 
	                <select id="series2" value={this.state.show_series[1]} className={'color' + this.state.show_series[1]} onChange={this.series_selected_right}>
	                    <option value="0" className="color0">Sales</option>
	                    <option value="1" className="color1">Ad Spend</option>
	                    <option value="2" className="color2">ACoS</option>
	                    <option value="3" className="color3">CPC</option>
	                    <option value="4" className="color4">Impressions</option>
	                    <option value="5" className="color5">CTR</option>
	                    <option value="6" className="color6">Clicks</option>
	                    <option value="7" className="color7">CR</option>
	                    <option value="8" className="color8">Orders</option>
	                </select>
				</div>
				<div id="main_chart"></div>
			</div>
		);
	}
}

export default GoogleChart;