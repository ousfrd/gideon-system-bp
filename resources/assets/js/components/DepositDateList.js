import React from 'react';
import axios from 'axios';
import Picky from 'react-picky';
import 'react-picky/dist/picky.css'; // Include CSS


class DepositDateList extends React.Component {
  constructor(props) {

    super(props);

    this.state = {
        depositList: [],
        depositarrayValue: []    
    };    
    

    this.selectMultipleOption = this.selectMultipleOption.bind(this);

  }

  
// after selection in the account box
  selectMultipleOption(value) {

    
  this.setState({
    depositarrayValue: value,      
  }, () => {

    this.props.onChange(value);

  });

}

  render() {

      return (

            <div className="col-md-3 col-sm-1 col-xs-12 mobile-padding-filter padding-sm">
            <div>Deposit Date</div>
            <Picky
              value={(this.props.depositarrayValue != undefined )? this.props.depositarrayValue : [] }
              options={this.props.depositList}
              onChange={this.selectMultipleOption}
              open={false}
              valueKey="date"
              labelKey="deposit"
              multiple={true}
              includeSelectAll={true}
              includeFilter={true}
              
            />

            </div>

      );
    }


}

export default DepositDateList;